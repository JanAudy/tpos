﻿
namespace Transware.Data.Enums
{
    public enum ERecordStatus 
    { 
        Unfinished, // 0
        Closed,     // 1
        Printed,    // 2
        Canceled,   // 3
        Created,    // 4
        Unlocked    // 5
    }

}