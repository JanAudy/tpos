﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.Enums
{
    public enum EUsedExchangeRate
    {
        DevizyBuy, 
        DevizySell, 
        DevizyMid, 
        ValutyBuy, 
        ValutySell, 
        ValutyMid, 
        CNBMid
    }
}
