﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.Enums
{
    public enum EConfigFields : long
    {
        None = 0,
        EShopPath,
        OrderMarkName,
        EanPreciseMatch,
        LabelPrintType,
        RememberLastUnit,
        OpenWindowWhenScanned,
        OpenSortimentBeforeItemWindow,
        ForbidSellToMinus,
        SetPositionForNewProductToFirst,
        PrintBillOnA4Printer,
        UseUserID1,
        UserID1Name,
        UseUserID2,
        UserID2Name,
        JidelnaConnected,
        UseOrderMark,
        JidelnaPassword,
        JidelnaAddress,
        JidelnaLastBill,
        EShopUserName,
        EShopPassword,
        PrintRecordSummaryOnXReport,
        UseAdvancedBill,
        UseSimpleReceipt,
        ServiceTabs,
        ServicePrintPos,
        CompaLabel,
        UseEAN,
        EanCountry,
        EanCompany,
        EanNumber,
        OtherMovementsInProfitReport,
        PrintLabelsOnA4,
        UseCustomerSystem,
        BillFirstLine = 1000000000000L,
        BillHeader = 2000000000000L,
        BillFooter = 3000000000000L
    }
}
