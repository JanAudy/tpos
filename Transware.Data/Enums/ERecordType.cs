﻿
namespace Transware.Data.Enums
{
    public enum ERecordType
    {
        Ean,                    //  0
        Receipt,                //  1
        Bill,                   //  2
        AdvInvoice,             //  3
        Invoice,                //  4
        DeliveryNote,           //  5
        CashReceipt,            //  6
        Requisition,            //  7
        PurchaseOrder,          //  8
        Inventory,              //  9
        OtherMovementIncome,    // 10
        OtherMovementOutcome,   // 11
        SellOrder,              // 12
        Transfer,               // 13 
        SellWaranty,            // 14
        PurchaseWaranty,        // 15
        ProductExchange,        // 16
        Contract,               // 17
        Service,                // 18
        Reservation,            // 19
        CreditNote,             // 20
        SellOrderConfirm,       // 21

        CompanyCode = 10000       // 10000
    };

}