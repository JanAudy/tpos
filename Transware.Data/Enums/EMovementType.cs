﻿
namespace Transware.Data.Enums
{
    public enum EMovementType
    {
        Income,         // 0
        Release,        // 1
        Reservation,    // 2
        IncomeLocked,   // 3
        Unlock          // 4
    };

}