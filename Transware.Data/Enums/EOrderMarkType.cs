﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.Enums
{
    public enum EOrderMarkType { 
        String, 
        Numeric, 
        Char, 
        Set, 
        Dash,
        Dot, 
        Plus 
    };

}
