﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.Enums
{
    [Flags]
    public enum ERecordFlags { None = 0, Printed = 1, Emailed = 2 }

    //[Flags]
    //public enum ERecordStatusAndFlags
    //{
    //    None = 0,
    //    Printed = 1,
    //    Emailed = 2,
    //    Created = 1 << 28,
    //    Canceled = 1 << 29,
    //    Closed = 1 << 30,
    //    Unfinished = 1 << 31
    //}
}
