﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RequisitionItemMarkModel : BaseTranswareModel<RequisitionItemMark>
    {
        #region Singleton

        private static RequisitionItemMarkModel instance;

        public static RequisitionItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RequisitionItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected RequisitionItemMarkModel() : base(TranswareDB.Instance.RequisitionItemMarkDao) { }

    }
}
