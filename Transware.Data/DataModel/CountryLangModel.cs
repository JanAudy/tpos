﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CountryLangModel : BaseLongModel<CountryLang>
    {
        #region Singleton

        private static CountryLangModel instance;

        public static CountryLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CountryLangModel();
                }
                return instance;
            }
        }

        #endregion

        protected CountryLangModel() : base(TranswareDB.Instance.CountryLangDao) { }

    }
}
