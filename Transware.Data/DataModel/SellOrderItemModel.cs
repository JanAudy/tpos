﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellOrderItemModel : BaseTranswareModel<SellOrderItem>
    {
        #region Singleton

        private static SellOrderItemModel instance;

        public static SellOrderItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellOrderItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellOrderItemModel() : base(TranswareDB.Instance.SellOrderItemDao) { }

    }
}
