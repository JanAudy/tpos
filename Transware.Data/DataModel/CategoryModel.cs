﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CategoryModel : BaseTranswareModel<Category>
    {
        #region Singleton

        private static CategoryModel instance;

        public static CategoryModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CategoryModel();
                }
                return instance;
            }
        }

        #endregion

        protected CategoryModel() : base(TranswareDB.Instance.CategoryDao) { }

        public override void InitLazy(Category res)
        {
            base.InitLazy(res);
            res.LoadParent += LoadParent;
            res.LoadSubcategories += LoadSubcategories;
            res.LoadProducts += LoadProducts;
        }

        public void LoadParent(object sender, long parentID, ref Category category)
        {
            category = CategoryModel.Instance.Get(parentID);
        }

        public void LoadSubcategories(object sender, long id, ref List<Category> categories)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Parent", ConstraintOperation.Equal, id));
            categories = GetList(constraints);
            categories.Sort(new Category.PositionComparer());
        }

        public void LoadProducts(object sender, long id, ref List<Product> products)
        {
            products = ProductModel.Instance.GetByCategory(id);
        }

        public Category GetTree()
        {
            List<Category> categories = GetList();

            Dictionary<long, Category> catdict = new Dictionary<long, Category>();
            foreach (Category c in categories)
            {
                InitLazy(c);
                catdict.Add(c.ID, c);
                c.Subcategories = new List<Category>();
            }

            foreach (Category c in categories)
            {
                if (c.ParentID != 0 && catdict.ContainsKey(c.ParentID))
                {
                    catdict[c.ParentID].Subcategories.Add(c);
                }
            }

            foreach (Category c in categories)
            {
                c.Subcategories.Sort(new Category.PositionComparer());
            }

            return catdict[1];
        }

        public List<Category> GetAllTree()
        {
            List<Category> categories = GetList();

            List<Category> roots = new List<Category>();
            foreach (Category c in categories)
            {
                if (c.ParentID == 0)
                    roots.Add(c);
            }

            Dictionary<long, Category> catdict = new Dictionary<long, Category>();
            foreach (Category c in categories)
            {
                catdict.Add(c.ID, c);
                c.Subcategories = new List<Category>();
            }

            foreach (Category c in categories)
            {
                if (c.ParentID != 0 && catdict.ContainsKey(c.ParentID))
                {
                    catdict[c.ParentID].Subcategories.Add(c);
                }
            }

            foreach (Category c in categories)
            {
                c.Subcategories.Sort(new Category.PositionComparer());
            }

            return roots;
        }
    }
}
