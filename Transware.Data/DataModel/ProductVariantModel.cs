﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductVariantModel : BaseTranswareModel<ProductVariant>
    {
        #region Singleton

        private static ProductVariantModel instance;

        public static ProductVariantModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductVariantModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductVariantModel() : base(TranswareDB.Instance.ProductVariantDao) { }

        #region OtherCalls

        public List<ProductVariant> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));

            return GetList(constraints);
        }

        #endregion

        public override void InitLazy(ProductVariant res)
        {
            base.InitLazy(res);

            res.LoadProduct += LoadProduct;
            res.LoadProvider += LoadProvider;
            res.LoadProductSetting += LoadProductSetting;
            
            res.LoadProviders += LoadProviders;
            res.LoadPackages += LoadPackages;
            res.LoadImages += LoadImages;
            res.LoadMovements += LoadMovements;
            res.LoadStoreProducts += LoadStoreProducts;
            res.LoadProductAttributes += LoadProductAttributes;
        }

        #region Foreign Objects

        public void LoadProduct(object sender, long objectID, ref Product product)
        {
            product = ProductModel.Instance.Get(objectID);
        }

        public void LoadProvider(object sender, long objectID, ref Provider provider)
        {
            provider = ProviderModel.Instance.Get(objectID);
        }

        public void LoadProductSetting(object sender, long objectID, ref ProductSetting productSetting)
        {
            productSetting = ProductSettingModel.Instance.Get(objectID);
        }

        #endregion

        #region Collections

        public void LoadProviders(object sender, long variantID, ref List<Provider> providers)
        {
            providers = ProviderModel.Instance.GetByVariant(variantID);
        }

        public void LoadPackages(object sender, long variantID, ref List<Package> packages)
        {
            packages = PackageModel.Instance.GetByVariant(variantID);
        }

        public void LoadImages(object sender, long variantID, ref List<ProductImage> images)
        {
            images = ProductImageModel.Instance.GetByVariant(variantID);
        }

        public void LoadMovements(object sender, long variantID, ref List<ProductMovement> movements)
        {
            movements = ProductMovementModel.Instance.GetByVariant(variantID);
        }

        public void LoadStoreProducts(object sender, long variantID, ref Dictionary<long, StoreProduct> storeProducts)
        {
            storeProducts = StoreProductModel.Instance.GetByVariant(variantID);
        }

        public void LoadProductAttributes(object sender, long variantID, ref HashSet<ProductAttribute> productAttributes)
        {
            productAttributes = TranswareDB.Instance.ProductVariantAttributesDao.GetSet(variantID);
            foreach (ProductAttribute pa in productAttributes)
                ProductAttributeModel.Instance.InitLazy(pa);
        }
        
        #endregion

        protected override bool SaveOrUpdateObjects(ProductVariant obj)
        {
            bool res = base.SaveOrUpdateObjects(obj);
            if (obj.productSetting != null)
            {
                res &= ProductSettingModel.Instance.SaveOrUpdate(obj.productSetting);
                obj.SettingID = obj.productSetting.ID;
            }
            //if (obj.provider != null)
            //{
            //    res &= ProviderModel.Instance.SaveOrUpdate(obj.provider);
            //    obj.ProviderID = obj.provider.ID;
            //}
            return res;
        }

        protected override bool SaveOrUpdateChildren(ProductVariant obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.providers != null)
                foreach (Provider p in obj.providers)
                {
                    res &= ProviderModel.Instance.SaveOrUpdate(p);
                }
            if (res && obj.packages != null)
                foreach (Package pkg in obj.packages)
                {
                    res &= PackageModel.Instance.SaveOrUpdate(pkg);
                }
            if (res && obj.images != null)
                foreach (ProductImage o in obj.images)
                {
                    res &= ProductImageModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.movements != null)
                foreach (ProductMovement o in obj.movements)
                {
                    res &= ProductMovementModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.storeProducts != null)
                foreach (StoreProduct o in obj.storeProducts.Values)
                {
                    res &= StoreProductModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.productAttributes != null)
                res = TranswareDB.Instance.ProductVariantAttributesDao.Update(obj.ID, obj.productAttributes);
            
            
            return res;
        }
    }


}
