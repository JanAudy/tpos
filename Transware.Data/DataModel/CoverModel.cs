using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CoverModel : BaseTranswareModel<Cover>
    {
        #region Singleton

        private static CoverModel instance;

        public static CoverModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CoverModel();
                }
                return instance;
            }
        }

        #endregion

        protected CoverModel() : base(TranswareDB.Instance.CoverDao) { }


        internal List<Cover> GetByPackage(long packageID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Package", ConstraintOperation.Equal, packageID));

            return GetList(constraints);
        }


        public override void InitLazy(Cover res)
        {
            base.InitLazy(res);

            res.LoadPackage += LoadPackage;
            res.LoadCover += LoadCover;
        }

        #region Foreign Objects

        public void LoadPackage(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }

        public void LoadCover(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }
        
        #endregion

    }
}
