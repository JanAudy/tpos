﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProviderModel : BaseTranswareModel<Provider>
    {
        #region Singleton

        private static ProviderModel instance;

        public static ProviderModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProviderModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProviderModel() : base(TranswareDB.Instance.ProviderDao) { }


        public List<Provider> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));

            return GetList(constraints);
        }

        public List<Provider> GetByVariant(long variantID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));

            return GetList(constraints);
        }

        public override void InitLazy(Provider res)
        {
            base.InitLazy(res);

            res.LoadProduct += LoadProduct;
            res.LoadVariant += LoadVariant;
            res.LoadCurrency += LoadCurrency;
        }

        #region Foreign Objects

        public void LoadProduct(object sender, long objectID, ref Product product)
        {
            product = ProductModel.Instance.Get(objectID);
        }

        public void LoadVariant(object sender, long objectID, ref ProductVariant variant)
        {
            variant = ProductVariantModel.Instance.Get(objectID);
        }

        public void LoadCurrency(object sender, long objectID, ref Currency currency)
        {
            currency = CurrencyModel.Instance.Get(objectID);
        }

        #endregion

    }
}
