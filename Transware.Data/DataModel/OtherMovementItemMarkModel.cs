﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class OtherMovementItemMarkModel : BaseTranswareModel<OtherMovementItemMark>
    {
        #region Singleton

        private static OtherMovementItemMarkModel instance;

        public static OtherMovementItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OtherMovementItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected OtherMovementItemMarkModel() : base(TranswareDB.Instance.OtherMovementItemMarkDao) { }

    }
}
