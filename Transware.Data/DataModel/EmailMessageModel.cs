﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class EmailMessageModel : BaseTranswareModel<EmailMessage>
    {
        #region Singleton

        private static EmailMessageModel instance;

        public static EmailMessageModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailMessageModel();
                }
                return instance;
            }
        }

        #endregion

        protected EmailMessageModel() : base(TranswareDB.Instance.EmailMessageDao) { }

    }
}
