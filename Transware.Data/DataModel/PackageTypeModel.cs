﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PackageTypeModel : BaseTranswareModel<PackageType>
    {
        #region Singleton

        private static PackageTypeModel instance;

        public static PackageTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PackageTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected PackageTypeModel() : base(TranswareDB.Instance.PackageTypeDao) { }

    }
}
