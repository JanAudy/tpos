﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductAttributeGroupModel : BaseTranswareModel<ProductAttributeGroup>
    {
        #region Singleton

        private static ProductAttributeGroupModel instance;

        public static ProductAttributeGroupModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductAttributeGroupModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductAttributeGroupModel() : base(TranswareDB.Instance.ProductAttributeGroupDao) { }

        public override void InitLazy(ProductAttributeGroup res)
        {
            base.InitLazy(res);

            res.LoadAttributes += LoadAttributes;
        }

        #region Collections

        public void LoadAttributes(object sender, long productAttributeGroupID, ref List<ProductAttribute> attributes)
        {
            attributes = ProductAttributeModel.Instance.GetByGroup(productAttributeGroupID);
        }

        #endregion

        protected override bool SaveOrUpdateChildren(ProductAttributeGroup obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.attributes != null)
                foreach (ProductAttribute p in obj.attributes)
                {
                    res &= ProductAttributeModel.Instance.SaveOrUpdate(p);
                }
            
            return res;
        }
    }
}
