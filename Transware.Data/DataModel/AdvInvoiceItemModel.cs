﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class AdvInvoiceItemModel : BaseTranswareModel<AdvInvoiceItem>
    {
        #region Singleton

        private static AdvInvoiceItemModel instance;

        public static AdvInvoiceItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AdvInvoiceItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected AdvInvoiceItemModel() : base(TranswareDB.Instance.AdvInvoiceItemDao) { }

    }
}
