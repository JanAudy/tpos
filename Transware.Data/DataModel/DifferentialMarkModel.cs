﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class DifferentialMarkModel : BaseTranswareModel<DifferentialMark>
    {
        #region Singleton

        private static DifferentialMarkModel instance;

        public static DifferentialMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DifferentialMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected DifferentialMarkModel() : base(TranswareDB.Instance.DifferentialMarkDao) { }


    }
}
