﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RequisitionModel : BaseTranswareModel<Requisition>
    {
        #region Singleton

        private static RequisitionModel instance;

        public static RequisitionModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RequisitionModel();
                }
                return instance;
            }
        }

        #endregion

        protected RequisitionModel() : base(TranswareDB.Instance.RequisitionDao) { }

    }
}
