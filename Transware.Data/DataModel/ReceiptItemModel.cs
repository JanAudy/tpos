﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ReceiptItemModel : BaseTranswareModel<ReceiptItem>
    {
        #region Singleton

        private static ReceiptItemModel instance;

        public static ReceiptItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReceiptItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected ReceiptItemModel() : base(TranswareDB.Instance.ReceiptItemDao) { }

    }
}
