﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellWarrantyModel : BaseTranswareModel<SellWarranty>
    {
        #region Singleton

        private static SellWarrantyModel instance;

        public static SellWarrantyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellWarrantyModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellWarrantyModel() : base(TranswareDB.Instance.SellWarrantyDao) { }

    }
}
