﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductExchangeModel : BaseTranswareModel<ProductExchange>
    {
        #region Singleton

        private static ProductExchangeModel instance;

        public static ProductExchangeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductExchangeModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductExchangeModel() : base(TranswareDB.Instance.ProductExchangeDao) { }

    }
}
