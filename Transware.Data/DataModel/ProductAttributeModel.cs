﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductAttributeModel : BaseTranswareModel<ProductAttribute>
    {
        #region Singleton

        private static ProductAttributeModel instance;

        public static ProductAttributeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductAttributeModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductAttributeModel() : base(TranswareDB.Instance.ProductAttributeDao) { }


        public override void InitLazy(ProductAttribute res)
        {
            base.InitLazy(res);

            res.LoadGroup += LoadGroup;
        }

        public List<ProductAttribute> GetByGroup(long productAttributeGroupID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Group", ConstraintOperation.Equal, productAttributeGroupID));
            List<ProductAttribute> list = GetList(constraints);
            list.Sort();
            return list;
        }

        #region Foreign Objects

        public void LoadGroup(object sender, long objectID, ref ProductAttributeGroup group)
        {
            group = ProductAttributeGroupModel.Instance.Get(objectID);
        }

        #endregion
    }
}
