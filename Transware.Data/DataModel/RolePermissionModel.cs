﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RolePermissionModel : BaseTranswareModel<RolePermission>
    {
        #region Singleton

        private static RolePermissionModel instance;

        public static RolePermissionModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RolePermissionModel();
                }
                return instance;
            }
        }

        #endregion

        protected RolePermissionModel() : base(TranswareDB.Instance.RolePermissionDao) { }

    }
}
