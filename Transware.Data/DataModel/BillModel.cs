﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillModel : BaseTranswareModel<Bill>
    {
        #region Singleton

        private static BillModel instance;

        public static BillModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillModel() : base(TranswareDB.Instance.BillDao) { }

        public override void InitLazy(Bill res)
        {
            base.InitLazy(res);
            res.LoadItems += LoadItems;
            res.LoadPayments += LoadPayments;
        }

        public override Bill Create()
        {
            Bill o = base.Create();
            return o;
        }

        public void LoadItems(object sender, long recordID, ref List<BillItem> items)
        {
            items = BillItemModel.Instance.GetByRecord(recordID);
        }

        public void LoadPayments(object sender, long recordID, ref List<BillPayment> items)
        {
            items = BillPaymentModel.Instance.GetByRecord(recordID);
        }

        protected override bool SaveOrUpdateChildren(Bill obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.items != null)
            {
                foreach (BillItem o in obj.items)
                    res &= BillItemModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.payments != null)
            {
                foreach (BillPayment o in obj.payments)
                    res &= BillPaymentModel.Instance.SaveOrUpdate(o);
            }

            return res;
        }
    }
}
