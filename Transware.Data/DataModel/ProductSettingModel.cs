﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductSettingModel : BaseLongModel<ProductSetting>
    {
        #region Singleton

        private static ProductSettingModel instance;

        public static ProductSettingModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductSettingModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductSettingModel() : base(TranswareDB.Instance.ProductSettingDao) { }

    }
}
