﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CTaxModel : BaseTranswareModel<CTax>
    {
        #region Singleton

        private static CTaxModel instance;

        public static CTaxModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CTaxModel();
                }
                return instance;
            }
        }

        #endregion

        protected CTaxModel() : base(TranswareDB.Instance.CTaxDao) { }

    }
}
