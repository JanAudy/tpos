﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ContractReturnedItemNameModel : BaseTranswareModel<ContractReturnedItemName>
    {
        #region Singleton

        private static ContractReturnedItemNameModel instance;

        public static ContractReturnedItemNameModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ContractReturnedItemNameModel();
                }
                return instance;
            }
        }

        #endregion

        protected ContractReturnedItemNameModel() : base(TranswareDB.Instance.ContractReturnedItemNameDao) { }

    }
}
