﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class DeliveryNoteItemMarkModel : BaseTranswareModel<DeliveryNoteItemMark>
    {
        #region Singleton

        private static DeliveryNoteItemMarkModel instance;

        public static DeliveryNoteItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DeliveryNoteItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected DeliveryNoteItemMarkModel() : base(TranswareDB.Instance.DeliveryNoteItemMarkDao) { }

    }
}
