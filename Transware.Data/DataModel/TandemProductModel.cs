using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TandemProductModel : BaseTranswareModel<TandemProduct>
    {
        #region Singleton

        private static TandemProductModel instance;

        public static TandemProductModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TandemProductModel();
                }
                return instance;
            }
        }

        #endregion

        protected TandemProductModel() : base(TranswareDB.Instance.TandemProductDao) { }


        internal List<TandemProduct> GetByPackage(long packageID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Package", ConstraintOperation.Equal, packageID));

            return GetList(constraints);
        }

        public override void InitLazy(TandemProduct res)
        {
            base.InitLazy(res);

            res.LoadPackage += LoadPackage;
            res.LoadTandem += LoadTandem;
        }

        #region Foreign Objects

        public void LoadPackage(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }

        public void LoadTandem(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }

        #endregion
    }
}
