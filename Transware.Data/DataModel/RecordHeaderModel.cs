﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RecordHeaderModel : BaseTranswareModel<RecordHeader>
    {
        #region Singleton

        private static RecordHeaderModel instance;

        public static RecordHeaderModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordHeaderModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordHeaderModel() : base(TranswareDB.Instance.RecordHeaderDao) { }

    }
}
