﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseWarrantyNoteModel : BaseTranswareModel<PurchaseWarrantyNote>
    {
        #region Singleton

        private static PurchaseWarrantyNoteModel instance;

        public static PurchaseWarrantyNoteModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseWarrantyNoteModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseWarrantyNoteModel() : base(TranswareDB.Instance.PurchaseWarrantyNoteDao) { }

    }
}
