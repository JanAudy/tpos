﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class UnitLangModel : BaseTranswareModel<UnitLang>
    {
        #region Singleton

        private static UnitLangModel instance;

        public static UnitLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnitLangModel();
                }
                return instance;
            }
        }

        #endregion
        
        protected UnitLangModel() : base(TranswareDB.Instance.UnitLangDao) { }

        public Dictionary<ELanguage, UnitLang> GetByParent(long parentId)
        {
            return ((UnitLangDao)dao).GetByParent(parentId);
        }


    }
}
