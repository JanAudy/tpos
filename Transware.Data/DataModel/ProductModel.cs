﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class ProductModel : BaseTranswareModel<Product>
    {
        #region Singleton

        private static ProductModel instance;

        public static ProductModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductModel() : base(TranswareDB.Instance.ProductDao) { }

        public override void InitLazy(Product res)
        {
            base.InitLazy(res);

            res.LoadCategory += LoadCategory;
            res.LoadUnit += LoadUnit;
            res.LoadVat += LoadVat;
            res.LoadCTax += LoadCTax;
            res.LoadTimePeriod += LoadTimePeriod;
            res.LoadPriceCalculation += LoadPriceCalculation;
            res.LoadProvider += LoadProvider;
            res.LoadProductSetting += LoadProductSetting;
            res.LoadProductGroup += LoadProductGroup;

            res.LoadLanguages += LoadLanguages;
            res.LoadProviders += LoadProviders;
            res.LoadPackages += LoadPackages;
            res.LoadVariants += LoadVariants;
            res.LoadImages += LoadImages;
            res.LoadMovements += LoadMovements;
            res.LoadStoreProducts += LoadStoreProducts;
            res.LoadDifferentialMarks += LoadDifferentialMarks;
        }

        #region Foreign Objects

        public void LoadCategory(object sender, long objectID, ref Category category)
        {
            category = CategoryModel.Instance.Get(objectID);
        }

        public void LoadUnit(object sender, long objectID, ref Unit unit)
        {
            unit = UnitModel.Instance.Get(objectID);
        }

        public void LoadVat(object sender, long objectID, ref Vat vat)
        {
            vat = VatModel.Instance.Get(objectID);
        }

        public void LoadCTax(object sender, long objectID, ref CTax cTax)
        {
            cTax = CTaxModel.Instance.Get(objectID);
        }

        public void LoadTimePeriod(object sender, long objectID, ref TimePeriod timePeriod)
        {
            timePeriod = TimePeriodModel.Instance.Get(objectID);
        }

        public void LoadPriceCalculation(object sender, long objectID, ref PriceCalculation priceCalculation)
        {
            priceCalculation = PriceCalculationModel.Instance.Get(objectID);
        }

        public void LoadProvider(object sender, long objectID, ref Provider provider)
        {
            provider = ProviderModel.Instance.Get(objectID);
        }

        public void LoadProductSetting(object sender, long objectID, ref ProductSetting productSetting)
        {
            productSetting = ProductSettingModel.Instance.Get(objectID);
        }

        public void LoadProductGroup(object sender, long objectID, ref ProductGroup productGroup)
        {
            productGroup = ProductGroupModel.Instance.Get(objectID);
        }        

        #endregion

        #region Collections

        public void LoadLanguages(object sender, long productID, ref Dictionary<ELanguage, ProductNameLang> languages)
        {
            languages = ProductNameLangModel.Instance.GetByParent(productID);
        }

        public void LoadProviders(object sender, long productID, ref List<Provider> providers)
        {
            providers = ProviderModel.Instance.GetByProduct(productID);
        }

        public void LoadPackages(object sender, long productID, ref List<Package> packages)
        {
            packages = PackageModel.Instance.GetByProduct(productID);
        }

        public void LoadVariants(object sender, long productID, ref List<ProductVariant> variants)
        {
            variants = ProductVariantModel.Instance.GetByProduct(productID);
        }

        public void LoadImages(object sender, long productID, ref List<ProductImage> images)
        {
            images = ProductImageModel.Instance.GetByProduct(productID);
        }

        public void LoadMovements(object sender, long productID, ref List<ProductMovement> movements)
        {
            movements = ProductMovementModel.Instance.GetByProduct(productID);
        }

        public void LoadStoreProducts(object sender, long productID, ref Dictionary<long, StoreProduct> storeProducts)
        {
            storeProducts = StoreProductModel.Instance.GetByProduct(productID);
        }

        public void LoadDifferentialMarks(object sender, long productID, ref HashSet<DifferentialMark> differentialMarks)
        {
            differentialMarks = TranswareDB.Instance.UsedDifferentialMarksDao.GetSet(productID);
        }

        #endregion

        protected override bool SaveOrUpdateObjects(Product obj)
        {
            bool res = base.SaveOrUpdateObjects(obj);
            if (obj.productSetting != null)
            {
                res &= ProductSettingModel.Instance.SaveOrUpdate(obj.productSetting);
                obj.SettingID = obj.productSetting.ID;
            }
            if (obj.priceCalculation != null)
            {
                res &= PriceCalculationModel.Instance.SaveOrUpdate(obj.priceCalculation);
                obj.PriceCalculationID = obj.priceCalculation.ID;
            }
            //if (obj.provider != null)
            //{
            //    res &= ProviderModel.Instance.SaveOrUpdate(obj.provider);
            //    obj.ProviderID = obj.provider.ID;
            //}
            return res;
        }
        
        protected override bool SaveOrUpdateChildren(Product obj)
        {
            bool res =  base.SaveOrUpdateChildren(obj);

            if (res && obj.languages != null && obj.languages.Count > 0)
            {
                foreach (ProductNameLang ul in obj.languages.Values)
                {
                    res &= ProductNameLangModel.Instance.SaveOrUpdate(ul);
                }
            }
            if (res && obj.providers != null)
                foreach (Provider p in obj.providers)
                {
                    res &= ProviderModel.Instance.SaveOrUpdate(p);
                }
            
            if (res && obj.variants != null)
                foreach (ProductVariant o in obj.variants)
                {
                    res &= ProductVariantModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.packages != null)
                foreach (Package pkg in obj.packages)
                {
                    res &= PackageModel.Instance.SaveOrUpdate(pkg);
                } 
            if (res && obj.images != null)
                foreach (ProductImage o in obj.images)
                {
                    res &= ProductImageModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.movements != null)
                foreach (ProductMovement o in obj.movements)
                {
                    res &= ProductMovementModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.storeProducts != null)
                foreach (StoreProduct o in obj.storeProducts.Values)
                {
                    res &= StoreProductModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.differentialMarks!=null)
                res = TranswareDB.Instance.UsedDifferentialMarksDao.Update(obj.ID, obj.differentialMarks);
            
            return res;
        }

        internal List<Product> GetByCategory(long id)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Category", ConstraintOperation.Equal, id));
            List<Product> products = GetList(constraints);
            products.Sort(new Product.PlaceNoComparer());
            return products;
        }
    }
}
