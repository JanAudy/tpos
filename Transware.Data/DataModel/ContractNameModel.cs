﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ContractNameModel : BaseTranswareModel<ContractName>
    {
        #region Singleton

        private static ContractNameModel instance;

        public static ContractNameModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ContractNameModel();
                }
                return instance;
            }
        }

        #endregion

        protected ContractNameModel() : base(TranswareDB.Instance.ContractNameDao) { }

    }
}
