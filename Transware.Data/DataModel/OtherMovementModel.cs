﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class OtherMovementModel : BaseTranswareModel<OtherMovement>
    {
        #region Singleton

        private static OtherMovementModel instance;

        public static OtherMovementModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OtherMovementModel();
                }
                return instance;
            }
        }

        #endregion

        protected OtherMovementModel() : base(TranswareDB.Instance.OtherMovementDao) { }

    }
}
