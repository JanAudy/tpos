﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillPaymentModel : BaseTranswareModel<BillPayment>
    {
        #region Singleton

        private static BillPaymentModel instance;

        public static BillPaymentModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillPaymentModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillPaymentModel() : base(TranswareDB.Instance.BillPaymentDao) { }


        public List<BillPayment> GetByRecord(long recordID)
        {
            List<Constraint> c = new List<Constraint>();
            c.Add(new Constraint("Bill", ConstraintOperation.Equal, recordID));
            return GetList(c);

        }
    }
}
