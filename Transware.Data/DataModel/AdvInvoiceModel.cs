﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class AdvInvoiceModel : BaseTranswareModel<AdvInvoice>
    {
        #region Singleton

        private static AdvInvoiceModel instance;

        public static AdvInvoiceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AdvInvoiceModel();
                }
                return instance;
            }
        }

        #endregion

        protected AdvInvoiceModel() : base(TranswareDB.Instance.AdvInvoiceDao) { }

    }
}
