﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InventoryModel : BaseTranswareModel<Inventory>
    {
        #region Singleton

        private static InventoryModel instance;

        public static InventoryModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InventoryModel();
                }
                return instance;
            }
        }

        #endregion

        protected InventoryModel() : base(TranswareDB.Instance.InventoryDao) { }

    }
}
