﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ExchangeModel : BaseTranswareModel<Exchange>
    {
        #region Singleton

        private static ExchangeModel instance;

        public static ExchangeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExchangeModel();
                }
                return instance;
            }
        }

        #endregion

        protected ExchangeModel() : base(TranswareDB.Instance.ExchangeDao) { }

        public override void InitLazy(Exchange res)
        {
            base.InitLazy(res);

            res.LoadCurrency += LoadCurrency;
            res.LoadMainCurrency += LoadMainCurrency;
        }

        #region Foreign Objects

        public void LoadCurrency(object sender, long objectID, ref Currency currency)
        {
            currency = CurrencyModel.Instance.Get(objectID);
        }

        public void LoadMainCurrency(object sender, long objectID, ref Currency currency)
        {
            currency = CurrencyModel.Instance.Get(objectID);
        }

        #endregion
    }
}
