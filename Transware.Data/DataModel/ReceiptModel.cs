﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ReceiptModel : BaseTranswareModel<Receipt>
    {
        #region Singleton

        private static ReceiptModel instance;

        public static ReceiptModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReceiptModel();
                }
                return instance;
            }
        }

        #endregion

        protected ReceiptModel() : base(TranswareDB.Instance.ReceiptDao) { }

    }
}
