using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductImageModel : BaseTranswareModel<ProductImage>
    {
        #region Singleton

        private static ProductImageModel instance;

        public static ProductImageModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductImageModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductImageModel() : base(TranswareDB.Instance.ProductImageDao) { }

        public List<ProductImage> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));
            return GetList(constraints);
        }

        public List<ProductImage> GetByVariant(long variantID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));
           return GetList(constraints);
        }

        public void UpdatePosition(List<ProductImage> productImages)
        {
            ((ProductImageDao)dao).UpdatePosition(productImages);
        }

    }
}
