﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RoleModel : BaseLongModel<Role>
    {
        #region Singleton

        private static RoleModel instance;

        public static RoleModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RoleModel();
                }
                return instance;
            }
        }

        #endregion

        protected RoleModel() : base(TranswareDB.Instance.RoleDao) { }

    }
}
