﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TimePeriodModel : BaseTranswareModel<TimePeriod>
    {
        #region Singleton

        private static TimePeriodModel instance;

        public static TimePeriodModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TimePeriodModel();
                }
                return instance;
            }
        }

        #endregion

        protected TimePeriodModel() : base(TranswareDB.Instance.TimePeriodDao) { }

    }
}
