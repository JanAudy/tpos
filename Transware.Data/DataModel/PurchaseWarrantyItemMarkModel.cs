﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseWarrantyItemMarkModel : BaseTranswareModel<PurchaseWarrantyItemMark>
    {
        #region Singleton

        private static PurchaseWarrantyItemMarkModel instance;

        public static PurchaseWarrantyItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseWarrantyItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseWarrantyItemMarkModel() : base(TranswareDB.Instance.PurchaseWarrantyItemMarkDao) { }

    }
}
