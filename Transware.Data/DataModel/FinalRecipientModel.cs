﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class FinalRecipientModel : BaseLongModel<FinalRecipient>
    {
        #region Singleton

        private static FinalRecipientModel instance;

        public static FinalRecipientModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FinalRecipientModel();
                }
                return instance;
            }
        }

        #endregion

        protected FinalRecipientModel() : base(TranswareDB.Instance.FinalRecipientDao) { }

    }
}
