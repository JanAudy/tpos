﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InvoiceItemModel : BaseTranswareModel<InvoiceItem>
    {
        #region Singleton

        private static InvoiceItemModel instance;

        public static InvoiceItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InvoiceItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected InvoiceItemModel() : base(TranswareDB.Instance.InvoiceItemDao) { }

    }
}
