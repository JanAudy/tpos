using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PrescriptionModel : BaseTranswareModel<Prescription>
    {
        #region Singleton

        private static PrescriptionModel instance;

        public static PrescriptionModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PrescriptionModel();
                }
                return instance;
            }
        }

        #endregion

        protected PrescriptionModel() : base(TranswareDB.Instance.PrescriptionDao) { }

        public List<Prescription> GetByPackage(long packageID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Package", ConstraintOperation.Equal, packageID));

            return GetList(constraints);
        }


        public override void InitLazy(Prescription res)
        {
            base.InitLazy(res);

            res.LoadPackage += LoadPackage;
            res.LoadIngredient += LoadIngredient;
        }

        #region Foreign Objects

        public void LoadPackage(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }

        public void LoadIngredient(object sender, long objectID, ref Package ingredient)
        {
            ingredient = PackageModel.Instance.Get(objectID);
        }

        #endregion


    }
}
