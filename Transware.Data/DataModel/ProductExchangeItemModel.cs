﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductExchangeItemModel : BaseTranswareModel<ProductExchangeItem>
    {
        #region Singleton

        private static ProductExchangeItemModel instance;

        public static ProductExchangeItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductExchangeItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductExchangeItemModel() : base(TranswareDB.Instance.ProductExchangeItemDao) { }

    }
}
