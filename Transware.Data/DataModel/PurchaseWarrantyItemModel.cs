﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseWarrantyItemModel : BaseTranswareModel<PurchaseWarrantyItem>
    {
        #region Singleton

        private static PurchaseWarrantyItemModel instance;

        public static PurchaseWarrantyItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseWarrantyItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseWarrantyItemModel() : base(TranswareDB.Instance.PurchaseWarrantyItemDao) { }

    }
}
