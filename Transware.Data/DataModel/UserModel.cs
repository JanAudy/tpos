﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class UserModel : BaseLongModel<User>
    {
        #region Singleton

        private static UserModel instance;

        public static UserModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserModel();
                }
                return instance;
            }
        }

        #endregion

        protected UserModel() : base(TranswareDB.Instance.UserDao) { }

    }
}
