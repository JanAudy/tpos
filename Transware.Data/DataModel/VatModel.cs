﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class VatModel : BaseTranswareModel<Vat>
    {
        #region Singleton

        private static VatModel instance;

        public static VatModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VatModel();
                }
                return instance;
            }
        }

        #endregion

        protected VatModel() : base(TranswareDB.Instance.VatDao) { }

    }
}
