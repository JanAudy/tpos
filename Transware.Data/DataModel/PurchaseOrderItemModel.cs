﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseOrderItemModel : BaseTranswareModel<PurchaseOrderItem>
    {
        #region Singleton

        private static PurchaseOrderItemModel instance;

        public static PurchaseOrderItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseOrderItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseOrderItemModel() : base(TranswareDB.Instance.PurchaseOrderItemDao) { }

    }
}
