﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SettlementModel : BaseTranswareModel<Settlement>
    {
        #region Singleton

        private static SettlementModel instance;

        public static SettlementModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SettlementModel();
                }
                return instance;
            }
        }

        #endregion

        protected SettlementModel() : base(TranswareDB.Instance.SettlementDao) { }

    }
}
