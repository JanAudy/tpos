﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class OrderTypeModel : BaseTranswareModel<OrderType>
    {
        #region Singleton

        private static OrderTypeModel instance;

        public static OrderTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrderTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected OrderTypeModel() : base(TranswareDB.Instance.OrderTypeDao) { }

        public override void InitLazy(OrderType obj)
        {
            base.InitLazy(obj);
            obj.LoadLanguages += LoadLanguages;
        }

        public void LoadLanguages(object sender, long unitID, ref Dictionary<ELanguage, OrderTypeLang> languages)
        {
            languages = OrderTypeLangModel.Instance.GetByParent(unitID);
        }


        protected override bool SaveOrUpdateChildren(OrderType obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.languages != null && obj.languages.Count > 0)
            {
                foreach (OrderTypeLang ul in obj.languages.Values)
                {
                    res &= OrderTypeLangModel.Instance.SaveOrUpdate(ul);
                }
            }

            return res;
        }
    }
}
