﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ReceiptOtherItemModel : BaseTranswareModel<ReceiptOtherItem>
    {
        #region Singleton

        private static ReceiptOtherItemModel instance;

        public static ReceiptOtherItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReceiptOtherItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected ReceiptOtherItemModel() : base(TranswareDB.Instance.ReceiptOtherItemDao) { }

    }
}
