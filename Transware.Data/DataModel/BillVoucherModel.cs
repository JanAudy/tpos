﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillVoucherModel : BaseTranswareModel<BillVoucher>
    {
        #region Singleton

        private static BillVoucherModel instance;

        public static BillVoucherModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillVoucherModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillVoucherModel() : base(TranswareDB.Instance.BillVoucherDao) { }

    }
}
