﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductMovementModel : BaseTranswareModel<ProductMovement>
    {
        #region Singleton

        private static ProductMovementModel instance;

        public static ProductMovementModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductMovementModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductMovementModel() : base(TranswareDB.Instance.ProductMovementDao) { }

        public List<ProductMovement> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));

            return GetList(constraints);
        }

        public List<ProductMovement> GetByVariant(long variantID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));

            return GetList(constraints);
        }

        public override void InitLazy(ProductMovement res)
        {
            base.InitLazy(res);

            res.LoadProduct += LoadProduct;
            res.LoadVariant += LoadVariant;
            res.LoadStore += LoadStore;
        }

        #region Foreign Objects

        public void LoadProduct(object sender, long objectID, ref Product product)
        {
            product = ProductModel.Instance.Get(objectID);
        }

        public void LoadVariant(object sender, long objectID, ref ProductVariant variant)
        {
            variant = ProductVariantModel.Instance.Get(objectID);
        }

        public void LoadStore(object sender, long objectID, ref Store store)
        {
            store = StoreModel.Instance.Get(objectID);
        }

        #endregion

    }
}
