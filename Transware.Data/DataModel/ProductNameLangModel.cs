﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductNameLangModel : BaseTranswareModel<ProductNameLang>
    {
        #region Singleton

        private static ProductNameLangModel instance;

        public static ProductNameLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductNameLangModel();
                }
                return instance;
            }
        }

        #endregion
        
        protected ProductNameLangModel() : base(TranswareDB.Instance.ProductNameLangDao) { }

        public Dictionary<ELanguage, ProductNameLang> GetByParent(long parentId)
        {
            return ((ProductNameLangDao)dao).GetByParent(parentId);
        }


    }
}
