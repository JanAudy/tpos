﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductSummaryModel : BaseLongModel<ProductSummary>
    {
        #region Singleton

        private static ProductSummaryModel instance;

        public static ProductSummaryModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductSummaryModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductSummaryModel() : base(TranswareDB.Instance.ProductSummaryDao) { }

        public void LoadWaranty(object sender, long id, ref ProductSummary warranty)
        {
            warranty = ProductSummaryModel.Instance.Get(id);
        }

        public List<ProductSummary> GetList(long StoreID, long CategoryID)
        {
            List<ProductSummary> result = null;

            result = (dao as ProductSummaryDao).GetList(StoreID, CategoryID);
            foreach (ProductSummary r in result)
            {
                InitLazy(r);
            }
            
            return result;
        }

        public List<ProductSummary> GetList(long StoreID, ProductFilter filter)
        {
            List<Constraint> constraints = new List<Constraint>();

            if (filter.ID.HasValue)
            {
                constraints.Add(new Constraint("p.ID", ConstraintOperation.Equal, filter.ID.Value));
            }

            if (!string.IsNullOrEmpty(filter.Ean))
            {
                constraints.Add(new Constraint("pkg.Ean", ConstraintOperation.Like, filter.Ean + "%"));
            }

            if (!string.IsNullOrEmpty(filter.Name))
                constraints.Add(new Constraint("p.Name", ConstraintOperation.Like, filter.Name + "%"));

            if (!string.IsNullOrEmpty(filter.ShortName))
                constraints.Add(new Constraint("p.ShortName", ConstraintOperation.Like, filter.ShortName + "%"));

            if (!string.IsNullOrEmpty(filter.Specification))
                constraints.Add(new Constraint("p.Specification", ConstraintOperation.Like, filter.Specification + "%"));
            
            if (!string.IsNullOrEmpty(filter.UserID1))
                constraints.Add(new Constraint("p.UserID1", ConstraintOperation.Like, filter.UserID1 + "%"));

            if (!string.IsNullOrEmpty(filter.UserID2))
                constraints.Add(new Constraint("p.UserID2", ConstraintOperation.Like, filter.UserID2 + "%"));

            if (filter.TimePeriodSelected)
                constraints.Add(new Constraint("p.TimePeriod", ConstraintOperation.Equal, filter.TimePeriod));

            //if (filter.TimePeriodSelected)
            //    constraints.Add(new Constraint("p.TimePeriod", ConstraintOperation.Equal, filter.TimePeriod));

            if (filter.Vat!=0)
                constraints.Add(new Constraint("p.Vat", ConstraintOperation.Equal, filter.Vat));

            if (filter.Unit != 0)
                constraints.Add(new Constraint("p.Unit", ConstraintOperation.Equal, filter.Unit));

            if (filter.CTax != 0)
                constraints.Add(new Constraint("p.CTax", ConstraintOperation.Equal, filter.CTax));

            if (!string.IsNullOrEmpty(filter.OrderMark))
                constraints.Add(new Constraint("pv.OrderMark", ConstraintOperation.Like, filter.OrderMark.Replace('?', '_') + "%"));

            if (filter.Provider != 0)
            {
                CompositeConstraint cc = new CompositeConstraint(ConstraintOperation.Or);
                cc.Constraints.Add(new Constraint("pp.OrganizationID", ConstraintOperation.Equal, filter.Provider));
                cc.Constraints.Add(new Constraint("pvp.OrganizationID", ConstraintOperation.Equal, filter.Provider));
                constraints.Add(cc);
            }

            if (filter.Position != 0)
            {
                constraints.Add(new Constraint("sp.Position", ConstraintOperation.Equal, filter.Position));
            }

            if (!string.IsNullOrEmpty(filter.Description))
            {
                constraints.Add(new Constraint("sp.PlaceDesc", ConstraintOperation.Equal, filter.Description));
            }

            NumericValueComparison(constraints, "sp.RequestedMin", filter.ReserveMinCmp, filter.ReserveMin);
            NumericValueComparison(constraints, "sp.RequestedMax", filter.ReserveMaxCmp, filter.ReserveMax);
            NumericValueComparison(constraints, "sp.RealNoReserved", filter.NoReserveCmp, filter.NoReserve);
            NumericValueComparison(constraints, "sp.RealReserved", filter.ReserveCmp, filter.Reserve);
            NumericValueComparison(constraints, "sp.RealLocked", filter.LockedCmp, filter.Locked);
            NumericValueComparison(constraints, "sp.RealTotal", filter.TotalCmp, filter.Total);

            //    if (DMarks != null)
            //    {
            //        foreach (DifferentialMark dm in DMarks)
            //        {
            //            if (!product.DifferentialMarks.Contains(dm))
            //                return false;
            //        }
            //    }

            return (dao as ProductSummaryDao).GetList(StoreID, constraints);
        }

        private void NumericValueComparison(List<Constraint> constraints, string name, ValueComparison cmp, decimal? value)
        {
            if (value.HasValue)
            {
                constraints.Add(new Constraint(name, ValueComparisonToConstraintOperation(cmp), value.Value));
            }
        }

        public List<ProductSummary> GetListBelowRequested(long StoreID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("RequestedMin", ConstraintOperation.IsNotNull));
            constraints.Add(new Constraint("RequestedMin", ConstraintOperation.NotEqual, 0));
            constraints.Add(new InnerConstraint("RealTotal", ConstraintOperation.Less, "RequestedMin"));

            return (dao as ProductSummaryDao).GetList(StoreID, constraints);
        }

        private ConstraintOperation ValueComparisonToConstraintOperation(ValueComparison cmp)
        {
            switch (cmp)
            {
                case ValueComparison.Equal: return ConstraintOperation.Equal;
                case ValueComparison.Greater: return ConstraintOperation.Greater;
                case ValueComparison.GreaterEq: return ConstraintOperation.GreaterOrEqual;
                case ValueComparison.Less: return ConstraintOperation.Less;
                case ValueComparison.LessEq: return ConstraintOperation.LessOrEqual;
                case ValueComparison.NotEq: return ConstraintOperation.Not;
            }
            return ConstraintOperation.Equal;
        }

        public void UpdatePlaceNo(List<ProductSummary> products)
        {
            (dao as ProductSummaryDao).UpdatePlaceNo(products);
        }

        public void UpdateCategoryAndPlaceNo(List<ProductSummary> products)
        {
            (dao as ProductSummaryDao).UpdateCategoryAndPlaceNo(products);
        }

        public void SetInvalid(List<ProductSummary> products)
        {
            (dao as ProductSummaryDao).SetInvalid(products);
        }

        public void SetInvalid(List<ProductSummary.ProductVariantSummary> products)
        {
            (dao as ProductSummaryDao).SetInvalid(products);
        }

        public void SetInternet(List<ProductSummary> products, bool status)
        {
            (dao as ProductSummaryDao).SetInternet(products, status);
        }

        public void SetInternet(List<ProductSummary.ProductVariantSummary> products, bool status)
        {
            (dao as ProductSummaryDao).SetInternet(products, status);
        }

        public enum ValueComparison { Equal, Less, LessEq, Greater, GreaterEq, NotEq };

        public class ProductFilter
        {
            public long? ID;
            public string Ean;
            public string Name;
            public string UserID1;
            public string UserID2;
            public string ShortName;
            public bool TimePeriodSelected;
            public long TimePeriod;
            public string Specification;
            public string OrderMark;
            public long Provider;
            public long Vat;
            public long CTax;
            public long Unit;
            public List<long> DMarks = new List<long>();
            public string Description;
            public long Position;
            public ValueComparison ReserveMinCmp;
            public decimal? ReserveMin;
            public ValueComparison ReserveMaxCmp;
            public decimal? ReserveMax;
            public ValueComparison NoReserveCmp;
            public decimal? NoReserve;
            public ValueComparison ReserveCmp;
            public decimal? Reserve;
            public ValueComparison TotalCmp;
            public decimal? Total;
            public ValueComparison LockedCmp;
            public decimal? Locked;
        }
    }
}
