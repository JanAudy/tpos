﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TransferItemMarkModel : BaseTranswareModel<TransferItemMark>
    {
        #region Singleton

        private static TransferItemMarkModel instance;

        public static TransferItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransferItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransferItemMarkModel() : base(TranswareDB.Instance.TransferItemMarkDao) { }

    }
}
