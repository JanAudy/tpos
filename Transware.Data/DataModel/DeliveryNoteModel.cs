﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class DeliveryNoteModel : BaseTranswareModel<DeliveryNote>
    {
        #region Singleton

        private static DeliveryNoteModel instance;

        public static DeliveryNoteModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DeliveryNoteModel();
                }
                return instance;
            }
        }

        #endregion

        protected DeliveryNoteModel() : base(TranswareDB.Instance.DeliveryNoteDao) { }

    }
}
