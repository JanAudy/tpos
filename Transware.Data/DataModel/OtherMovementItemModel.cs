﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class OtherMovementItemModel : BaseTranswareModel<OtherMovementItem>
    {
        #region Singleton

        private static OtherMovementItemModel instance;

        public static OtherMovementItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OtherMovementItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected OtherMovementItemModel() : base(TranswareDB.Instance.OtherMovementItemDao) { }

    }
}
