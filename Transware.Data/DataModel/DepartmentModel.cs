﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class DepartmentModel : BaseTranswareModel<Department>
    {
        #region Singleton

        private static DepartmentModel instance;

        public static DepartmentModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DepartmentModel();
                }
                return instance;
            }
        }

        #endregion

        protected DepartmentModel() : base(TranswareDB.Instance.DepartmentDao) { }

    }
}
