﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class UnitModel : BaseTranswareModel<Unit>
    {
        #region Singleton

        private static UnitModel instance;

        public static UnitModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnitModel();
                }
                return instance;
            }
        }

        #endregion

        protected UnitModel() : base(TranswareDB.Instance.UnitDao) { }

        public override void InitLazy(Unit obj)
        {
            base.InitLazy(obj);
            obj.LoadLanguages += LoadLanguages;
        }

        public void LoadLanguages(object sender, long unitID, ref Dictionary<ELanguage, UnitLang> languages)
        {
            languages = UnitLangModel.Instance.GetByParent(unitID);
        }

        
        protected override bool SaveOrUpdateChildren(Unit obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.languages != null && obj.languages.Count > 0)
            {
                foreach (UnitLang ul in obj.languages.Values)
                {
                    res &= UnitLangModel.Instance.SaveOrUpdate(ul);
                }
            }

            return res;
        }
    }
}
