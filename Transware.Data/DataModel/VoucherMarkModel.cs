﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataModel
{
    public class VoucherMarkModel : BaseTranswareModel<VoucherMark>
    {
        #region Singleton

        private static VoucherMarkModel instance;

        public static VoucherMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VoucherMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected VoucherMarkModel() : base(TranswareDB.Instance.VoucherMarkDao) { }

    }
}
