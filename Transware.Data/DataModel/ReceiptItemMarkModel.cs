﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ReceiptItemMarkModel : BaseTranswareModel<ReceiptItemMark>
    {
        #region Singleton

        private static ReceiptItemMarkModel instance;

        public static ReceiptItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReceiptItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected ReceiptItemMarkModel() : base(TranswareDB.Instance.ReceiptItemMarkDao) { }

    }
}
