﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataModel
{
    public class VoucherModel : BaseTranswareModel<Voucher>
    {
        #region Singleton

        private static VoucherModel instance;

        public static VoucherModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VoucherModel();
                }
                return instance;
            }
        }

        #endregion

        protected VoucherModel() : base(TranswareDB.Instance.VoucherDao) { }

    }
}
