﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class ConfigClassModel : BaseLongModel<ConfigClass>
    {
        #region Singleton

        private static ConfigClassModel instance;

        public static ConfigClassModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConfigClassModel();
                }
                return instance;
            }
        }

        #endregion

        protected ConfigClassModel() : base(TranswareDB.Instance.ConfigClassDao) { }

        public ConfigClass GetConfiguration()
        {
            List<ConfigClass> list = GetList();
            if (list.Count == 0)
                return new ConfigClass();
            else
                return list[0];
        }

        public override void InitLazy(ConfigClass obj)
        {
            base.InitLazy(obj);

            obj.LoadMainCurrency += LoadMainCurrency;
            obj.LoadFields += LoadFields;
        }

        public void LoadMainCurrency(object sender, long objectID, ref Currency mainCurrency)
        {
            mainCurrency = CurrencyModel.Instance.Get(objectID);
        }

        public void LoadFields(object sender, long objectID, ref Dictionary<EConfigFields, ConfigClassField> fields)
        {
            List<ConfigClassField> list = ConfigClassFieldModel.Instance.GetByConfig(objectID);

            fields = new Dictionary<EConfigFields,ConfigClassField>();
            foreach (ConfigClassField ccf in list)
                fields.Add(ccf.Name, ccf);
        }

        protected override bool SaveOrUpdateChildren(ConfigClass obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.fields != null)
                foreach (ConfigClassField p in obj.fields.Values)
                {
                    res &= ConfigClassFieldModel.Instance.SaveOrUpdate(p);
                }
            
            return res;
        }

        public string GetNewEan()
        {
            ConfigClass config = GetConfiguration();
            string prefix = config.GetField(EConfigFields.EanCountry) + config.GetField(EConfigFields.EanCompany);
            string code = config.GetField(EConfigFields.EanNumber);
            int number = int.Parse(code);
            number++;
            config.SetField(EConfigFields.EanNumber, number.ToString());
           
            SaveOrUpdate(config);

            code = code.PadLeft(12 - prefix.Length, '0');
            code = prefix + code;

            int sum = 0;
            sum += code[1] - '0';
            sum += code[3] - '0';
            sum += code[5] - '0';
            sum += code[7] - '0';
            sum += code[9] - '0';
            sum += code[11] - '0';
            sum *= 3;
            sum += code[0] - '0';
            sum += code[2] - '0';
            sum += code[4] - '0';
            sum += code[6] - '0';
            sum += code[8] - '0';
            sum += code[10] - '0';
            if ((sum % 10) == 0)
                sum = 0;
            else
                sum = ((sum / 10) + 1) * 10 - sum;
            code = code + (char)(sum + '0');

            return code;
        }
    }
}
