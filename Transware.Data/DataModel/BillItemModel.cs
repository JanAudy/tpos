﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillItemModel : BaseTranswareModel<BillItem>
    {
        #region Singleton

        private static BillItemModel instance;

        public static BillItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillItemModel() : base(TranswareDB.Instance.BillItemDao) { }


        public List<BillItem> GetByRecord(long recordID)
        {
            List<Constraint> c = new List<Constraint>();
            c.Add(new Constraint("Record", ConstraintOperation.Equal, recordID));
            return GetList(c);

        }
    }
}
