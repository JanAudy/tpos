﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PriceListModel : BaseTranswareModel<PriceList>
    {
        #region Singleton

        private static PriceListModel instance;

        public static PriceListModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PriceListModel();
                }
                return instance;
            }
        }

        #endregion

        protected PriceListModel() : base(TranswareDB.Instance.PriceListDao) { }

        public override void InitLazy(PriceList obj)
        {
            base.InitLazy(obj);

            obj.LoadGroups += LoadGroups;
        }

        public void LoadGroups(object sender, long objectID, ref Dictionary<long, PriceListProductGroup> groups)
        {
            List<PriceListProductGroup> list = PriceListProductGroupModel.Instance.GetByPriceList(objectID);
            Dictionary<long, PriceListProductGroup> res = new Dictionary<long, PriceListProductGroup>();
            foreach (PriceListProductGroup pg in list)
                if (!res.ContainsKey(pg.ProductGroup.Value))
                    res.Add(pg.ProductGroup.Value, pg);
        }
    }
}
