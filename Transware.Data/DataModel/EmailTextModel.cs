﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class EmailTextModel : BaseTranswareModel<EmailText>
    {
        #region Singleton

        private static EmailTextModel instance;

        public static EmailTextModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailTextModel();
                }
                return instance;
            }
        }

        #endregion

        protected EmailTextModel() : base(TranswareDB.Instance.EmailTextDao) { }

        public List<EmailText> GetList(ERecordType recordType)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("RecordType", ConstraintOperation.Equal, (int)recordType));
            return GetList(constraints);
        }

    }
}
