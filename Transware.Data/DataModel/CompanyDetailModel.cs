﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CompanyDetailModel : BaseLongModel<CompanyDetail>
    {
        #region Singleton

        private static CompanyDetailModel instance;

        public static CompanyDetailModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CompanyDetailModel();
                }
                return instance;
            }
        }

        #endregion

        protected CompanyDetailModel() : base(TranswareDB.Instance.CompanyDetailDao) { }

    }
}
