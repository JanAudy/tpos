﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class AdvSettlementModel : BaseTranswareModel<AdvSettlement>
    {
        #region Singleton

        private static AdvSettlementModel instance;

        public static AdvSettlementModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AdvSettlementModel();
                }
                return instance;
            }
        }

        #endregion

        protected AdvSettlementModel() : base(TranswareDB.Instance.AdvSettlementDao) { }

    }
}
