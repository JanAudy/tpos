﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SpecificationModel : BaseTranswareModel<Specification>
    {
        #region Singleton

        private static SpecificationModel instance;

        public static SpecificationModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SpecificationModel();
                }
                return instance;
            }
        }

        #endregion

        protected SpecificationModel() : base(TranswareDB.Instance.SpecificationDao) { }

    }
}
