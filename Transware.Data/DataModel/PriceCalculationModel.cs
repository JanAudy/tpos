﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PriceCalculationModel : BaseLongModel<PriceCalculation>
    {
        #region Singleton

        private static PriceCalculationModel instance;

        public static PriceCalculationModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PriceCalculationModel();
                }
                return instance;
            }
        }

        #endregion

        protected PriceCalculationModel() : base(TranswareDB.Instance.PriceCalculationDao) { }

    }
}
