﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TransferItemModel : BaseTranswareModel<TransferItem>
    {
        #region Singleton

        private static TransferItemModel instance;

        public static TransferItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransferItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransferItemModel() : base(TranswareDB.Instance.TransferItemDao) { }

    }
}
