﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataModel
{
    public class VersionModel : BaseTranswareModel<Version>
    {
        #region Singleton

        private static VersionModel instance;

        public static VersionModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VersionModel();
                }
                return instance;
            }
        }

        #endregion

        protected VersionModel() : base(TranswareDB.Instance.VersionDao) { }

    }
}
