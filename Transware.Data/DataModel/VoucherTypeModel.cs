﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataModel
{
    public class VoucherTypeModel : BaseTranswareModel<VoucherType>
    {
        #region Singleton

        private static VoucherTypeModel instance;

        public static VoucherTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VoucherTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected VoucherTypeModel() : base(TranswareDB.Instance.VoucherTypeDao) { }

    }
}
