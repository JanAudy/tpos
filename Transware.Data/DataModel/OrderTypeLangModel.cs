﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class OrderTypeLangModel : BaseTranswareModel<OrderTypeLang>
    {
        #region Singleton

        private static OrderTypeLangModel instance;

        public static OrderTypeLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrderTypeLangModel();
                }
                return instance;
            }
        }

        #endregion

        protected OrderTypeLangModel() : base(TranswareDB.Instance.OrderTypeLangDao) { }

        public Dictionary<ELanguage, OrderTypeLang> GetByParent(long parentId)
        {
            return ((OrderTypeLangDao)dao).GetByParent(parentId);
        }


    }
}
