﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ContractModel : BaseTranswareModel<Contract>
    {
        #region Singleton

        private static ContractModel instance;

        public static ContractModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ContractModel();
                }
                return instance;
            }
        }

        #endregion

        protected ContractModel() : base(TranswareDB.Instance.ContractDao) { }

    }
}
