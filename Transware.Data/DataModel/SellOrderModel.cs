﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellOrderModel : BaseTranswareModel<SellOrder>
    {
        #region Singleton

        private static SellOrderModel instance;

        public static SellOrderModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellOrderModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellOrderModel() : base(TranswareDB.Instance.SellOrderDao) { }

    }
}
