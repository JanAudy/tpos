﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class StoreModel : BaseTranswareModel<Store>
    {
        #region Singleton

        private static StoreModel instance;

        public static StoreModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StoreModel();
                }
                return instance;
            }
        }

        #endregion

        protected StoreModel() : base(TranswareDB.Instance.StoreDao) { }

        public override void InitLazy(Store obj)
        {
            base.InitLazy(obj);
        }

        public void LoadWaranty(object sender, long id, ref Store warranty)
        {
            warranty = StoreModel.Instance.Get(id);
        }
    }
}
