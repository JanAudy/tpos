﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TransferModel : BaseTranswareModel<Transfer>
    {
        #region Singleton

        private static TransferModel instance;

        public static TransferModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransferModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransferModel() : base(TranswareDB.Instance.TransferDao) { }

    }
}
