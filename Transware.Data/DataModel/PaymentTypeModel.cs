﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PaymentTypeModel : BaseTranswareModel<PaymentType>
    {
        #region Singleton

        private static PaymentTypeModel instance;

        public static PaymentTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PaymentTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected PaymentTypeModel() : base(TranswareDB.Instance.PaymentTypeDao) { }

    }
}
