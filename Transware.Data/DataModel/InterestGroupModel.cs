﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InterestGroupModel : BaseTranswareModel<InterestGroup>
    {
        #region Singleton

        private static InterestGroupModel instance;

        public static InterestGroupModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InterestGroupModel();
                }
                return instance;
            }
        }

        #endregion

        protected InterestGroupModel() : base(TranswareDB.Instance.InterestGroupDao) { }

    }
}
