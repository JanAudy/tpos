﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class RecordNumberModel : BaseLongModel<RecordNumber>
    {
        #region Singleton

        private static RecordNumberModel instance;

        public static RecordNumberModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordNumberModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordNumberModel() : base(TranswareDB.Instance.RecordNumberDao) { }


        public List<RecordNumber> Get(ERecordType recordType, long store)
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint("Type", ConstraintOperation.Equal, (int)recordType ));
            cons.Add(new Constraint("Store", ConstraintOperation.Equal, store));

            return GetList(cons);
        }

        public List<RecordNumber> Get(ERecordType recordType)
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint("Type", ConstraintOperation.Equal, (int)recordType));

            return GetList(cons);
        }

        public long GetNextNumber(ERecordType recordType, long store, ref string numberStr)
        {
            List<RecordNumber> rns = Get(recordType, store);

            RecordNumber rn;

            if (rns.Count > 0)
                rn = rns[0];
            else
                rn = new RecordNumber();

            long res = rn.Next;
            rn.Next++;

            numberStr = rn.FormateByPattern(res);

            SaveOrUpdate(rn);

            return res;
        }

        public long GetNextNumber(ERecordType recordType, ref string numberStr)
        {
            List<RecordNumber> rns = Get(recordType);

            RecordNumber rn;

            if (rns.Count > 0)
                rn = rns[0];
            else
                rn = new RecordNumber();

            long res = rn.Next;
            rn.Next++;

            numberStr = rn.FormateByPattern(res);

            SaveOrUpdate(rn);

            return res;
        }

        public long GetNextNumber(ERecordType recordType, long store)
        {
            string r="";

            return GetNextNumber(recordType, store, ref r);
        }

        public long GetNextNumber(ERecordType recordType)
        {
            string r = "";

            return GetNextNumber(recordType, ref r);
        }

    }
}
