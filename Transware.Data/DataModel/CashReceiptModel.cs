﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CashReceiptModel : BaseTranswareModel<CashReceipt>
    {
        #region Singleton

        private static CashReceiptModel instance;

        public static CashReceiptModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CashReceiptModel();
                }
                return instance;
            }
        }

        #endregion

        protected CashReceiptModel() : base(TranswareDB.Instance.CashReceiptDao) { }

    }
}
