﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PriceListProductGroupModel : BaseTranswareModel<PriceListProductGroup>
    {
        #region Singleton

        private static PriceListProductGroupModel instance;

        public static PriceListProductGroupModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PriceListProductGroupModel();
                }
                return instance;
            }
        }

        #endregion

        protected PriceListProductGroupModel() : base(TranswareDB.Instance.PriceListProductGroupDao) { }


        internal List<PriceListProductGroup> GetByPriceList(long parentID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("PriceList", ConstraintOperation.Equal, parentID));
            return GetList(constraints);
        }
    }
}
