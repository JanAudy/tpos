﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductExchangeItemMarkModel : BaseTranswareModel<ProductExchangeItemMark>
    {
        #region Singleton

        private static ProductExchangeItemMarkModel instance;

        public static ProductExchangeItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductExchangeItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductExchangeItemMarkModel() : base(TranswareDB.Instance.ProductExchangeItemMarkDao) { }

    }
}
