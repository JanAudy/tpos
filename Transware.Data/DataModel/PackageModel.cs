﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PackageModel : BaseTranswareModel<Package>
    {
        #region Singleton

        private static PackageModel instance;

        public static PackageModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PackageModel();
                }
                return instance;
            }
        }

        #endregion

        protected PackageModel() : base(TranswareDB.Instance.PackageDao) { }

        public List<Package> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));

            return GetList(constraints);
        }

        public List<Package> GetByVariant(long variantID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));

            return GetList(constraints);
        }

        public List<Package> GetByEanExact(string ean)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Ean", ConstraintOperation.Equal, ean));

            return GetList(constraints);
        }

        public List<Package> GetByEan(string ean)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Ean", ConstraintOperation.Like, ean+"%"));

            return GetList(constraints);
        }


        public override void InitLazy(Package res)
        {
            base.InitLazy(res);

            res.LoadProduct += LoadProduct;
            res.LoadVariant += LoadVariant;
            res.LoadPackageType += LoadPackageType;
            res.LoadPackageLabel += LoadPackageLabel;

            res.LoadCovers += LoadCovers;
            res.LoadPrescriptions += LoadPrescriptions;
            res.LoadTandemProducts += LoadTandemProducts;
            res.LoadStorePackages += LoadStorePackages;
        }

        #region Foreign Objects

        public void LoadProduct(object sender, long objectID, ref Product product)
        {
            product = ProductModel.Instance.Get(objectID);
        }

        public void LoadVariant(object sender, long objectID, ref ProductVariant variant)
        {
            variant = ProductVariantModel.Instance.Get(objectID);
        }

        public void LoadPackageType(object sender, long objectID, ref PackageType packageType)
        {
            packageType = PackageTypeModel.Instance.Get(objectID);
        }

        public void LoadPackageLabel(object sender, long objectID, ref PackageLabel packageLabel)
        {
            packageLabel = PackageLabelModel.Instance.Get(objectID);
        }
        
        #endregion

        #region Collections

        public void LoadCovers(object sender, long packageID, ref List<Cover> covers)
        {
            covers = CoverModel.Instance.GetByPackage(packageID);
        }

        public void LoadPrescriptions(object sender, long packageID, ref List<Prescription> prescriptions)
        {
            prescriptions = PrescriptionModel.Instance.GetByPackage(packageID);
        }

        public void LoadTandemProducts(object sender, long packageID, ref List<TandemProduct> tandemProducts)
        {
            tandemProducts = TandemProductModel.Instance.GetByPackage(packageID);
        }

        public void LoadStorePackages(object sender, long packageID, ref Dictionary<long, StorePackage> storePackages)
        {
            storePackages = StorePackageModel.Instance.GetByPackage(packageID);
        }

        #endregion

        protected override bool SaveOrUpdateChildren(Package obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.covers != null)
                foreach (Cover p in obj.covers)
                {
                    res &= CoverModel.Instance.SaveOrUpdate(p);
                }
            if (res && obj.prescriptions != null)
                foreach (Prescription p in obj.prescriptions)
                {
                    res &= PrescriptionModel.Instance.SaveOrUpdate(p);
                }
            if (res && obj.tandemProducts != null)
                foreach (TandemProduct o in obj.tandemProducts)
                {
                    res &= TandemProductModel.Instance.SaveOrUpdate(o);
                }
            if (res && obj.storePackages != null)
                foreach (StorePackage o in obj.storePackages.Values)
                {
                    res &= StorePackageModel.Instance.SaveOrUpdate(o);
                }

            return res;
        }
    }
}
