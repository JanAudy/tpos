﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SettlementTypeLangModel : BaseTranswareModel<SettlementTypeLang>
    {
        #region Singleton

        private static SettlementTypeLangModel instance;

        public static SettlementTypeLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SettlementTypeLangModel();
                }
                return instance;
            }
        }

        #endregion

        protected SettlementTypeLangModel() : base(TranswareDB.Instance.SettlementTypeLangDao) { }

        public Dictionary<ELanguage, SettlementTypeLang> GetByParent(long parentId)
        {
            return ((SettlementTypeLangDao)dao).GetByParent(parentId);
        }


    }
}
