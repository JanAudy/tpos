﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseOrderModel : BaseTranswareModel<PurchaseOrder>
    {
        #region Singleton

        private static PurchaseOrderModel instance;

        public static PurchaseOrderModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseOrderModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseOrderModel() : base(TranswareDB.Instance.PurchaseOrderDao) { }

    }
}
