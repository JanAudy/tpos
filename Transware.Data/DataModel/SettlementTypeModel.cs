﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class SettlementTypeModel : BaseTranswareModel<SettlementType>
    {
        #region Singleton

        private static SettlementTypeModel instance;

        public static SettlementTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SettlementTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected SettlementTypeModel() : base(TranswareDB.Instance.SettlementTypeDao) { }

        public override void InitLazy(SettlementType obj)
        {
            base.InitLazy(obj);
            obj.LoadLanguages += LoadLanguages;
        }

        public void LoadLanguages(object sender, long unitID, ref Dictionary<ELanguage, SettlementTypeLang> languages)
        {
            languages = SettlementTypeLangModel.Instance.GetByParent(unitID);
        }


        protected override bool SaveOrUpdateChildren(SettlementType obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.languages != null && obj.languages.Count > 0)
            {
                foreach (SettlementTypeLang ul in obj.languages.Values)
                {
                    res &= SettlementTypeLangModel.Instance.SaveOrUpdate(ul);
                }
            }

            return res;
        }
    }
}
