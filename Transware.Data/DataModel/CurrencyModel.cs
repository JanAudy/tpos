﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CurrencyModel : BaseTranswareModel<Currency>
    {
        #region Singleton

        private static CurrencyModel instance;

        public static CurrencyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CurrencyModel();
                }
                return instance;
            }
        }

        #endregion

        protected CurrencyModel() : base(TranswareDB.Instance.CurrencyDao) { }

    }
}
