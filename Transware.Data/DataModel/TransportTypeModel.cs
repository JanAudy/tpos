﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;

namespace Transware.Data.DataModel
{
    public class TransportTypeModel : BaseTranswareModel<TransportType>
    {
        #region Singleton

        private static TransportTypeModel instance;

        public static TransportTypeModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransportTypeModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransportTypeModel() : base(TranswareDB.Instance.TransportTypeDao) { }

        public override void InitLazy(TransportType obj)
        {
            base.InitLazy(obj);
            obj.LoadLanguages += LoadLanguages;
        }

        public void LoadLanguages(object sender, long unitID, ref Dictionary<ELanguage, TransportTypeLang> languages)
        {
            languages = TransportTypeLangModel.Instance.GetByParent(unitID);
        }


        protected override bool SaveOrUpdateChildren(TransportType obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.languages != null && obj.languages.Count > 0)
            {
                foreach (TransportTypeLang ul in obj.languages.Values)
                {
                    res &= TransportTypeLangModel.Instance.SaveOrUpdate(ul);
                }
            }

            return res;
        }
    }
}
