﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RecordSettingModel : BaseLongModel<RecordSetting>
    {
        #region Singleton

        private static RecordSettingModel instance;

        public static RecordSettingModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordSettingModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordSettingModel() : base(TranswareDB.Instance.RecordSettingDao) { }

    }
}
