﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellWarrantyItemModel : BaseTranswareModel<SellWarrantyItem>
    {
        #region Singleton

        private static SellWarrantyItemModel instance;

        public static SellWarrantyItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellWarrantyItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellWarrantyItemModel() : base(TranswareDB.Instance.SellWarrantyItemDao) { }

    }
}
