﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class EmailAttachmentModel : BaseTranswareModel<EmailAttachment>
    {
        #region Singleton

        private static EmailAttachmentModel instance;

        public static EmailAttachmentModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailAttachmentModel();
                }
                return instance;
            }
        }

        #endregion

        protected EmailAttachmentModel() : base(TranswareDB.Instance.EmailAttachmentDao) { }

    }
}
