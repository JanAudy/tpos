using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class StorePackageModel : BaseLongModel<StorePackage>
    {
        #region Singleton

        private static StorePackageModel instance;

        public static StorePackageModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StorePackageModel();
                }
                return instance;
            }
        }

        #endregion

        protected StorePackageModel() : base(TranswareDB.Instance.StorePackageDao) { }

        public override void InitLazy(StorePackage obj)
        {
            base.InitLazy(obj);
            obj.LoadStore += LoadStore;
            obj.LoadPackage += LoadPackage;
            obj.LoadIndividualDiscount += LoadIndividualDiscount;
        }

        internal Dictionary<long, StorePackage> GetByPackage(long packageID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Package", ConstraintOperation.Equal, packageID));

            List<StorePackage> list = GetList(constraints);

            Dictionary<long, StorePackage> res = new Dictionary<long, StorePackage>();
            foreach (StorePackage sp in list)
            {
                res.Add(sp.StoreID.Value, sp);
            }
            return res;
        }


        #region Foreign Objects

        public void LoadStore(object sender, long objectID, ref Store store)
        {
            store = StoreModel.Instance.Get(objectID);
        }

        public void LoadPackage(object sender, long objectID, ref Package package)
        {
            package = PackageModel.Instance.Get(objectID);
        }

        public void LoadIndividualDiscount(object sender, long objectID, ref IndividualDiscount individualDiscount)
        {
            individualDiscount = IndividualDiscountModel.Instance.Get(objectID);
        }

        #endregion
    }
}
