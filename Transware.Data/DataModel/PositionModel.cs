﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PositionModel : BaseTranswareModel<Position>
    {
        #region Singleton

        private static PositionModel instance;

        public static PositionModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PositionModel();
                }
                return instance;
            }
        }

        #endregion

        protected PositionModel() : base(TranswareDB.Instance.PositionDao) { }

    }
}
