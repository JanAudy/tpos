﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RequisitionItemModel : BaseTranswareModel<RequisitionItem>
    {
        #region Singleton

        private static RequisitionItemModel instance;

        public static RequisitionItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RequisitionItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected RequisitionItemModel() : base(TranswareDB.Instance.RequisitionItemDao) { }

    }
}
