﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InventoryItemModel : BaseTranswareModel<InventoryItem>
    {
        #region Singleton

        private static InventoryItemModel instance;

        public static InventoryItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InventoryItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected InventoryItemModel() : base(TranswareDB.Instance.InventoryItemDao) { }

    }
}
