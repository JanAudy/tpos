﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RecordFooterModel : BaseTranswareModel<RecordFooter>
    {
        #region Singleton

        private static RecordFooterModel instance;

        public static RecordFooterModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordFooterModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordFooterModel() : base(TranswareDB.Instance.RecordFooterDao) { }

    }
}
