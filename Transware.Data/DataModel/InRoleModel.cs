﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InRoleModel : BaseLongModel<InRole>
    {
        #region Singleton

        private static InRoleModel instance;

        public static InRoleModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InRoleModel();
                }
                return instance;
            }
        }

        #endregion

        protected InRoleModel() : base(TranswareDB.Instance.InRoleDao) { }

    }
}
