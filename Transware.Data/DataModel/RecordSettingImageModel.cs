﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RecordSettingImageModel : BaseLongModel<RecordSettingImage>
    {
        #region Singleton

        private static RecordSettingImageModel instance;

        public static RecordSettingImageModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordSettingImageModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordSettingImageModel() : base(TranswareDB.Instance.RecordSettingImageDao) { }

    }
}
