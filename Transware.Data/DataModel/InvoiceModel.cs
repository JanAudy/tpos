﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InvoiceModel : BaseTranswareModel<Invoice>
    {
        #region Singleton

        private static InvoiceModel instance;

        public static InvoiceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InvoiceModel();
                }
                return instance;
            }
        }

        #endregion

        protected InvoiceModel() : base(TranswareDB.Instance.InvoiceDao) { }

    }
}
