﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellWarrantyNoteModel : BaseTranswareModel<SellWarrantyNote>
    {
        #region Singleton

        private static SellWarrantyNoteModel instance;

        public static SellWarrantyNoteModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellWarrantyNoteModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellWarrantyNoteModel() : base(TranswareDB.Instance.SellWarrantyNoteDao) { }

    }
}
