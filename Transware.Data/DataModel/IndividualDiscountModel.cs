﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class IndividualDiscountModel : BaseTranswareModel<IndividualDiscount>
    {
        #region Singleton

        private static IndividualDiscountModel instance;

        public static IndividualDiscountModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IndividualDiscountModel();
                }
                return instance;
            }
        }

        #endregion

        protected IndividualDiscountModel() : base(TranswareDB.Instance.IndividualDiscountDao) { }

    }
}
