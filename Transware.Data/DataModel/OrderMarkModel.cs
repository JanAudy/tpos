﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class OrderMarkModel : BaseTranswareModel<OrderMark>
    {
        #region Singleton

        private static OrderMarkModel instance;

        public static OrderMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrderMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected OrderMarkModel() : base(TranswareDB.Instance.OrderMarkDao) { }

    }
}
