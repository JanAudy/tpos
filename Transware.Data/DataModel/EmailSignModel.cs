﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class EmailSignModel : BaseTranswareModel<EmailSign>
    {
        #region Singleton

        private static EmailSignModel instance;

        public static EmailSignModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailSignModel();
                }
                return instance;
            }
        }

        #endregion

        protected EmailSignModel() : base(TranswareDB.Instance.EmailSignDao) { }

    }
}
