﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PaymentTitleModel : BaseTranswareModel<PaymentTitle>
    {
        #region Singleton

        private static PaymentTitleModel instance;

        public static PaymentTitleModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PaymentTitleModel();
                }
                return instance;
            }
        }

        #endregion

        protected PaymentTitleModel() : base(TranswareDB.Instance.PaymentTitleDao) { }

    }
}
