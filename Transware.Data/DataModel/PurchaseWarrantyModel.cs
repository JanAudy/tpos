﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PurchaseWarrantyModel : BaseTranswareModel<PurchaseWarranty>
    {
        #region Singleton

        private static PurchaseWarrantyModel instance;

        public static PurchaseWarrantyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PurchaseWarrantyModel();
                }
                return instance;
            }
        }

        #endregion

        protected PurchaseWarrantyModel() : base(TranswareDB.Instance.PurchaseWarrantyDao) { }

    }
}
