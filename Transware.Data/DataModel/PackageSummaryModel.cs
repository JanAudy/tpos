﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PackageSummaryModel : BaseTranswareModel<PackageSummary>
    {
        #region Singleton

        private static PackageSummaryModel instance;

        public static PackageSummaryModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PackageSummaryModel();
                }
                return instance;
            }
        }

        #endregion

        protected PackageSummaryModel() : base(TranswareDB.Instance.PackageSummaryDao) { }

        public List<PackageSummary> GetList(long categoryID, long storeID)
        {
            return ((PackageSummaryDao)dao).GetList(categoryID, storeID);
        }

        public List<PackageSummary> GetList(long storeID, string ean)
        {
            return ((PackageSummaryDao)dao).GetList(storeID, ean);
        }

        public PackageSummary Get(long id, long storeID=1)
        {
            return ((PackageSummaryDao)dao).Get(id, storeID);
        }

    }
}
