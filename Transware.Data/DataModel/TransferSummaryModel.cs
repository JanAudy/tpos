﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TransferSummaryModel : BaseTranswareModel<TransferSummary>
    {
        #region Singleton

        private static TransferSummaryModel instance;

        public static TransferSummaryModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransferSummaryModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransferSummaryModel() : base(TranswareDB.Instance.TransferSummaryDao) { }

        public override List<TransferSummary> GetList()
        {
            return dao.GetList();
        }
    }
}
