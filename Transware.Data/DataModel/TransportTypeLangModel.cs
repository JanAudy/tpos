﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;
using Transware.Data.Enums;
using Transware.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class TransportTypeLangModel : BaseTranswareModel<TransportTypeLang>
    {
        #region Singleton

        private static TransportTypeLangModel instance;

        public static TransportTypeLangModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransportTypeLangModel();
                }
                return instance;
            }
        }

        #endregion

        protected TransportTypeLangModel() : base(TranswareDB.Instance.TransportTypeLangDao) { }

        public Dictionary<ELanguage, TransportTypeLang> GetByParent(long parentId)
        {
            return ((TransportTypeLangDao)dao).GetByParent(parentId);
        }


    }
}
