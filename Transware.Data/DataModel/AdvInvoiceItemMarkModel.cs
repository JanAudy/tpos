﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class AdvInvoiceItemMarkModel : BaseTranswareModel<AdvInvoiceItemMark>
    {
        #region Singleton

        private static AdvInvoiceItemMarkModel instance;

        public static AdvInvoiceItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AdvInvoiceItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected AdvInvoiceItemMarkModel() : base(TranswareDB.Instance.AdvInvoiceItemMarkDao) { }

    }
}
