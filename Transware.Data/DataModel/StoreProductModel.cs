using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class StoreProductModel : BaseLongModel<StoreProduct>
    {
        #region Singleton

        private static StoreProductModel instance;

        public static StoreProductModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StoreProductModel();
                }
                return instance;
            }
        }

        #endregion

        protected StoreProductModel() : base(TranswareDB.Instance.StoreProductDao) { }

        public decimal StoreAmountByVariant(long variant, long store)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variant));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, store));

            StoreProduct sp = dao.Get(constraints);

            if (sp == null)
                return 0;
            else
                return sp.RealNoReserved;
        }

        public decimal StoreAmountByProducts(long product, long store)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, product));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, store));

            StoreProduct sp = dao.Get(constraints);

            if (sp == null)
                return 0;
            else
                return sp.RealNoReserved;
        }

        public decimal AveragePurchaseUnitPriceNoVatByVariant(long variant, long store)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variant));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, store));

            StoreProduct sp = dao.Get(constraints);

            if (sp == null)
                return 0;
            else
                return sp.AveragePurchaseUnitPriceNoVat;
        }

        public decimal AveragePurchaseUnitPriceNoVatByProducts(long product, long store)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, product));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, store));

            StoreProduct sp = dao.Get(constraints);

            if (sp == null)
                return 0;
            else
                return sp.AveragePurchaseUnitPriceNoVat;
        }

        public Dictionary<long, StoreProduct> GetByProduct(long productID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));

            List<StoreProduct> list = GetList(constraints);

            Dictionary<long, StoreProduct> res = new Dictionary<long, StoreProduct>();
            foreach (StoreProduct sp in list)
            {

                res.Add(sp.StoreID.Value, sp);
            }
            return res;
        }

        public StoreProduct GetByProduct(long productID, long storeID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Product", ConstraintOperation.Equal, productID));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, storeID));

            List<StoreProduct> list = GetList(constraints);

            if (list.Count > 0)
                return list[0];
            return null;
        }

        public Dictionary<long, StoreProduct> GetByVariant(long variantID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));

            List<StoreProduct> list = GetList(constraints);

            Dictionary<long, StoreProduct> res = new Dictionary<long, StoreProduct>();
            foreach (StoreProduct sp in list)
            {
                res.Add(sp.StoreID.Value, sp);
            }
            return res;
        }

        public StoreProduct GetByVariant(long variantID, long storeID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Variant", ConstraintOperation.Equal, variantID));
            constraints.Add(new Constraint("Store", ConstraintOperation.Equal, storeID));

            List<StoreProduct> list = GetList(constraints);

            if (list.Count > 0)
                return list[0];
            return null;
        }

        public override void InitLazy(StoreProduct res)
        {
            base.InitLazy(res);

            res.LoadStore += LoadStore;
            res.LoadProduct += LoadProduct;
            res.LoadVariant += LoadVariant;
            res.LoadPosition += LoadPosition;
        }

        #region Foreign Objects

        public void LoadStore(object sender, long objectID, ref Store store)
        {
            store = StoreModel.Instance.Get(objectID);
        }

        public void LoadProduct(object sender, long objectID, ref Product product)
        {
            product = ProductModel.Instance.Get(objectID);
        }

        public void LoadVariant(object sender, long objectID, ref ProductVariant variant)
        {
            variant = ProductVariantModel.Instance.Get(objectID);
        }

        public void LoadPosition(object sender, long objectID, ref Position position)
        {
            position = PositionModel.Instance.Get(objectID);
        }

        #endregion
    }        
}
