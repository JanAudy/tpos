﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataModel;
using Microdata.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BaseTranswareModel<T> : AbstractValidModel<T> where T:AbstractValidDto, new()
    {
        protected AbstractValidDao<T> dao = null;

        public BaseTranswareModel(AbstractValidDao<T> dao)
        {
            this.dao = dao;
        }

        public override T Create()
        {
            T obj = new T();
            InitLazy(obj);
            return obj;
        }

        protected virtual bool SaveOrUpdateObjects(T obj) { return true; }
        protected virtual bool SaveOrUpdateChildren(T obj) { return true; }

        public override bool SaveOrUpdate(T obj)
        {
            bool res = true;
            
            res = SaveOrUpdateObjects(obj);

            if (res)
                res &= dao.SaveOrUpdate(obj);
            
            if (res)
            {
                res &= SaveOrUpdateChildren(obj);
            }

            return res;
        }

        public virtual void InitLazy(T obj) {}

        public override T Load(long id)
        {
            T t = dao.Get(id);
            InitLazy(t);
            return t;
        }

        public override List<T> GetList(List<Constraint> constraints)
        {
            List<T> result = null;

            result = dao.GetList(constraints);
            foreach (T r in result)
            {
                InitLazy(r);
            }
            return result;
        }


        public override bool UpdateValid(T obj)
        {
            return dao.UpdateValid(obj);
        }
    }
}
