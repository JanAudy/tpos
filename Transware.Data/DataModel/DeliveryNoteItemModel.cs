﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class DeliveryNoteItemModel : BaseTranswareModel<DeliveryNoteItem>
    {
        #region Singleton

        private static DeliveryNoteItemModel instance;

        public static DeliveryNoteItemModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DeliveryNoteItemModel();
                }
                return instance;
            }
        }

        #endregion

        protected DeliveryNoteItemModel() : base(TranswareDB.Instance.DeliveryNoteItemDao) { }

    }
}
