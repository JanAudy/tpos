﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillItemMarkModel : BaseTranswareModel<BillItemMark>
    {
        #region Singleton

        private static BillItemMarkModel instance;

        public static BillItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillItemMarkModel() : base(TranswareDB.Instance.BillItemMarkDao) { }

    }
}
