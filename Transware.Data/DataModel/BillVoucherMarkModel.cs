﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class BillVoucherMarkModel : BaseTranswareModel<BillVoucherMark>
    {
        #region Singleton

        private static BillVoucherMarkModel instance;

        public static BillVoucherMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BillVoucherMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected BillVoucherMarkModel() : base(TranswareDB.Instance.BillVoucherMarkDao) { }

    }
}
