﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class RecordSettingFieldModel : BaseLongModel<RecordSettingField>
    {
        #region Singleton

        private static RecordSettingFieldModel instance;

        public static RecordSettingFieldModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RecordSettingFieldModel();
                }
                return instance;
            }
        }

        #endregion

        protected RecordSettingFieldModel() : base(TranswareDB.Instance.RecordSettingFieldDao) { }

    }
}
