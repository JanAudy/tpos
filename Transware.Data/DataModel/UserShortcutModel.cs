﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class UserShortcutModel : BaseLongModel<UserShortcut>
    {
        #region Singleton

        private static UserShortcutModel instance;

        public static UserShortcutModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserShortcutModel();
                }
                return instance;
            }
        }

        #endregion

        protected UserShortcutModel() : base(TranswareDB.Instance.UserShortcutDao) { }




        public List<UserShortcut> GetActive()
        {
            List<Constraint> constaints = new List<Constraint>();
            constaints.Add(new Constraint("Active", ConstraintOperation.Equal, "1"));
            return GetList(constaints);
        }
    }
}
