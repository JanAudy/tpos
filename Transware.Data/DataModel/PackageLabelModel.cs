﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class PackageLabelModel : BaseTranswareModel<PackageLabel>
    {
        #region Singleton

        private static PackageLabelModel instance;

        public static PackageLabelModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PackageLabelModel();
                }
                return instance;
            }
        }

        #endregion

        protected PackageLabelModel() : base(TranswareDB.Instance.PackageLabelDao) { }

    }
}
