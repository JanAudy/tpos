﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class CredentialModel : BaseTranswareModel<Credential>
    {
        #region Singleton

        private static CredentialModel instance;

        public static CredentialModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CredentialModel();
                }
                return instance;
            }
        }

        #endregion

        protected CredentialModel() : base(TranswareDB.Instance.CredentialDao) { }

    }
}
