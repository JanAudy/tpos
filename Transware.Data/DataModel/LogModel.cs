﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class LogModel : BaseLongModel<Log>
    {
        #region Singleton

        private static LogModel instance;

        public static LogModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LogModel();
                }
                return instance;
            }
        }

        #endregion

        protected LogModel() : base(TranswareDB.Instance.LogDao) { }

    }
}
