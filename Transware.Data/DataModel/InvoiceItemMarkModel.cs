﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class InvoiceItemMarkModel : BaseTranswareModel<InvoiceItemMark>
    {
        #region Singleton

        private static InvoiceItemMarkModel instance;

        public static InvoiceItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InvoiceItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected InvoiceItemMarkModel() : base(TranswareDB.Instance.InvoiceItemMarkDao) { }

    }
}
