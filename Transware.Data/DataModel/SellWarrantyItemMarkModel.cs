﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class SellWarrantyItemMarkModel : BaseTranswareModel<SellWarrantyItemMark>
    {
        #region Singleton

        private static SellWarrantyItemMarkModel instance;

        public static SellWarrantyItemMarkModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SellWarrantyItemMarkModel();
                }
                return instance;
            }
        }

        #endregion

        protected SellWarrantyItemMarkModel() : base(TranswareDB.Instance.SellWarrantyItemMarkDao) { }

    }
}
