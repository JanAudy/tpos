﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ConfigClassFieldModel : BaseLongModel<ConfigClassField>
    {
        #region Singleton

        private static ConfigClassFieldModel instance;

        public static ConfigClassFieldModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConfigClassFieldModel();
                }
                return instance;
            }
        }

        #endregion

        protected ConfigClassFieldModel() : base(TranswareDB.Instance.ConfigClassFieldDao) { }


        internal List<ConfigClassField> GetByConfig(long objectID)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("ConfigID", ConstraintOperation.Equal, objectID));
            return GetList(constraints);
        }
    }
}
