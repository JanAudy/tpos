﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Transware.Data.DataModel
{
    public class ProductGroupModel : BaseTranswareModel<ProductGroup>
    {
        #region Singleton

        private static ProductGroupModel instance;

        public static ProductGroupModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProductGroupModel();
                }
                return instance;
            }
        }

        #endregion

        protected ProductGroupModel() : base(TranswareDB.Instance.ProductGroupDao) { }

    }
}
