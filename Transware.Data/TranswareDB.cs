﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data;
using Transware.Data.DataAccess;

namespace Transware.Data
{
    public class TranswareDB : AbstractDB<TranswareDB>
    {
        public override string DB_Name
        {
            get { return "MDATA_TRANSWARE"; }
        }

        private CategoryDao categoryDao = null;
        public CategoryDao CategoryDao
        {
            get
            {
                if (categoryDao == null)
                    categoryDao = new CategoryDao(Connection);
                return categoryDao;
            }
        }

        private VatDao vatDao = null;
        public VatDao VatDao
        {
            get
            {
                if (vatDao == null)
                    vatDao = new VatDao(Connection);
                return vatDao;
            }
        }

        private UnitDao unitDao = null;
        public UnitDao UnitDao
        {
            get
            {
                if (unitDao == null)
                    unitDao = new UnitDao(Connection);
                return unitDao;
            }
        }

        private UnitLangDao unitLangDao = null;
        public UnitLangDao UnitLangDao
        {
            get
            {
                if (unitLangDao == null)
                    unitLangDao = new UnitLangDao(Connection);
                return unitLangDao;
            }
        }

        private TimePeriodDao timePeriodDao = null;
        public TimePeriodDao TimePeriodDao
        {
            get
            {
                if (timePeriodDao == null)
                    timePeriodDao = new TimePeriodDao(Connection);
                return timePeriodDao;
            }
        }

        private PositionDao positionDao = null;
        public PositionDao PositionDao
        {
            get
            {
                if (positionDao == null)
                    positionDao = new PositionDao(Connection);
                return positionDao;
            }
        }

        private PriceListDao priceListDao = null;
        public PriceListDao PriceListDao
        {
            get
            {
                if (priceListDao == null)
                    priceListDao = new PriceListDao(Connection);
                return priceListDao;
            }
        }

        private PriceListProductGroupDao priceListProductGroupDao = null;
        public PriceListProductGroupDao PriceListProductGroupDao
        {
            get
            {
                if (priceListProductGroupDao == null)
                    priceListProductGroupDao = new PriceListProductGroupDao(Connection);
                return priceListProductGroupDao;
            }
        }

        private CTaxDao ctaxDao = null;
        public CTaxDao CTaxDao
        {
            get
            {
                if (ctaxDao == null)
                    ctaxDao = new CTaxDao(Connection);
                return ctaxDao;
            }
        }

        private PriceCalculationDao priceCalculationDao = null;
        public PriceCalculationDao PriceCalculationDao
        {
            get
            {
                if (priceCalculationDao == null)
                    priceCalculationDao = new PriceCalculationDao(Connection);
                return priceCalculationDao;
            }
        }

        private ProductSettingDao productSettingDao = null;
        public ProductSettingDao ProductSettingDao
        {
            get
            {
                if (productSettingDao == null)
                    productSettingDao = new ProductSettingDao(Connection);
                return productSettingDao;
            }
        }

        private PackageTypeDao packageTypeDao = null;
        public PackageTypeDao PackageTypeDao
        {
            get
            {
                if (packageTypeDao == null)
                    packageTypeDao = new PackageTypeDao(Connection);
                return packageTypeDao;
            }
        }

        private CurrencyDao currencyDao = null;
        public CurrencyDao CurrencyDao
        {
            get
            {
                if (currencyDao == null)
                    currencyDao = new CurrencyDao(Connection);
                return currencyDao;
            }
        }

        private PackageLabelDao packageLabelDao = null;
        public PackageLabelDao PackageLabelDao
        {
            get
            {
                if (packageLabelDao == null)
                    packageLabelDao = new PackageLabelDao(Connection);
                return packageLabelDao;
            }
        }

        private DifferentialMarkDao differentialMarkDao = null;
        public DifferentialMarkDao DifferentialMarkDao
        {
            get
            {
                if (differentialMarkDao == null)
                    differentialMarkDao = new DifferentialMarkDao(Connection);
                return differentialMarkDao;
            }
        }

        private ContractNameDao contractNameDao = null;
        public ContractNameDao ContractNameDao
        {
            get
            {
                if (contractNameDao == null)
                    contractNameDao = new ContractNameDao(Connection);
                return contractNameDao;
            }
        }

        private ContractReturnedItemNameDao contractReturnedItemNameDao = null;
        public ContractReturnedItemNameDao ContractReturnedItemNameDao
        {
            get
            {
                if (contractReturnedItemNameDao == null)
                    contractReturnedItemNameDao = new ContractReturnedItemNameDao(Connection);
                return contractReturnedItemNameDao;
            }
        }

        private IndividualDiscountDao individualDiscountDao = null;
        public IndividualDiscountDao IndividualDiscountDao
        {
            get
            {
                if (individualDiscountDao == null)
                    individualDiscountDao = new IndividualDiscountDao(Connection);
                return individualDiscountDao;
            }
        }

        private StoreDao storeDao = null;
        public StoreDao StoreDao
        {
            get
            {
                if (storeDao == null)
                    storeDao = new StoreDao(Connection);
                return storeDao;
            }
        }

        private AdvInvoiceItemMarkDao advInvoiceItemMarkDao = null;
        public AdvInvoiceItemMarkDao AdvInvoiceItemMarkDao
        {
            get
            {
                if (advInvoiceItemMarkDao == null)
                    advInvoiceItemMarkDao = new AdvInvoiceItemMarkDao(Connection);
                return advInvoiceItemMarkDao;
            }
        }

        private AdvInvoiceItemDao advInvoiceItemDao = null;
        public AdvInvoiceItemDao AdvInvoiceItemDao
        {
            get
            {
                if (advInvoiceItemDao == null)
                    advInvoiceItemDao = new AdvInvoiceItemDao(Connection);
                return advInvoiceItemDao;
            }
        }

        private AdvInvoiceDao advInvoiceDao = null;
        public AdvInvoiceDao AdvInvoiceDao
        {
            get
            {
                if (advInvoiceDao == null)
                    advInvoiceDao = new AdvInvoiceDao(Connection);
                return advInvoiceDao;
            }
        }


        private AdvSettlementDao advSettlementDao = null;
        public AdvSettlementDao AdvSettlementDao
        {
            get
            {
                if (advSettlementDao == null)
                    advSettlementDao = new AdvSettlementDao(Connection);
                return advSettlementDao;
            }
        }

        private BillItemMarkDao billItemMarkDao = null;
        public BillItemMarkDao BillItemMarkDao
        {
            get
            {
                if (billItemMarkDao == null)
                    billItemMarkDao = new BillItemMarkDao(Connection);
                return billItemMarkDao;
            }
        }

        private BillItemDao billItemDao = null;
        public BillItemDao BillItemDao
        {
            get
            {
                if (billItemDao == null)
                    billItemDao = new BillItemDao(Connection);
                return billItemDao;
            }
        }

        private BillPaymentDao billPaymentDao = null;
        public BillPaymentDao BillPaymentDao
        {
            get
            {
                if (billPaymentDao == null)
                    billPaymentDao = new BillPaymentDao(Connection);
                return billPaymentDao;
            }
        }

        private BillDao billDao = null;
        public BillDao BillDao
        {
            get
            {
                if (billDao == null)
                    billDao = new BillDao(Connection);
                return billDao;
            }
        }

        private BillVoucherMarkDao billVoucherMarkDao = null;
        public BillVoucherMarkDao BillVoucherMarkDao
        {
            get
            {
                if (billVoucherMarkDao == null)
                    billVoucherMarkDao = new BillVoucherMarkDao(Connection);
                return billVoucherMarkDao;
            }
        }

        private BillVoucherDao billVoucherDao = null;
        public BillVoucherDao BillVoucherDao
        {
            get
            {
                if (billVoucherDao == null)
                    billVoucherDao = new BillVoucherDao(Connection);
                return billVoucherDao;
            }
        }

        private CashReceiptDao cashReceiptDao = null;
        public CashReceiptDao CashReceiptDao
        {
            get
            {
                if (cashReceiptDao == null)
                    cashReceiptDao = new CashReceiptDao(Connection);
                return cashReceiptDao;
            }
        }

        private CompanyDetailDao companyDetailDao = null;
        public CompanyDetailDao CompanyDetailDao
        {
            get
            {
                if (companyDetailDao == null)
                    companyDetailDao = new CompanyDetailDao(Connection);
                return companyDetailDao;
            }
        }

        private ConfigClassDao configClassDao = null;
        public ConfigClassDao ConfigClassDao
        {
            get
            {
                if (configClassDao == null)
                    configClassDao = new ConfigClassDao(Connection);
                return configClassDao;
            }
        }

        private ConfigClassFieldDao configClassFieldDao = null;
        public ConfigClassFieldDao ConfigClassFieldDao
        {
            get
            {
                if (configClassFieldDao == null)
                    configClassFieldDao = new ConfigClassFieldDao(Connection);
                return configClassFieldDao;
            }
        }

        private ContractDao contractDao = null;
        public ContractDao ContractDao
        {
            get
            {
                if (contractDao == null)
                    contractDao = new ContractDao(Connection);
                return contractDao;
            }
        }

        private CountryLangDao countryLangDao = null;
        public CountryLangDao CountryLangDao
        {
            get
            {
                if (countryLangDao == null)
                    countryLangDao = new CountryLangDao(Connection);
                return countryLangDao;
            }
        }

        private CoverDao coverDao = null;
        public CoverDao CoverDao
        {
            get
            {
                if (coverDao == null)
                    coverDao = new CoverDao(Connection);
                return coverDao;
            }
        }

        private CredentialDao credentialDao = null;
        public CredentialDao CredentialDao
        {
            get
            {
                if (credentialDao == null)
                    credentialDao = new CredentialDao(Connection);
                return credentialDao;
            }
        }

        private DeliveryNoteItemMarkDao deliveryNoteItemMarkDao = null;
        public DeliveryNoteItemMarkDao DeliveryNoteItemMarkDao
        {
            get
            {
                if (deliveryNoteItemMarkDao == null)
                    deliveryNoteItemMarkDao = new DeliveryNoteItemMarkDao(Connection);
                return deliveryNoteItemMarkDao;
            }
        }

        private DeliveryNoteItemDao deliveryNoteItemDao = null;
        public DeliveryNoteItemDao DeliveryNoteItemDao
        {
            get
            {
                if (deliveryNoteItemDao == null)
                    deliveryNoteItemDao = new DeliveryNoteItemDao(Connection);
                return deliveryNoteItemDao;
            }
        }

        private DeliveryNoteDao deliveryNoteDao = null;
        public DeliveryNoteDao DeliveryNoteDao
        {
            get
            {
                if (deliveryNoteDao == null)
                    deliveryNoteDao = new DeliveryNoteDao(Connection);
                return deliveryNoteDao;
            }
        }

        private DepartmentDao departmentDao = null;
        public DepartmentDao DepartmentDao
        {
            get
            {
                if (departmentDao == null)
                    departmentDao = new DepartmentDao(Connection);
                return departmentDao;
            }
        }

        private EmailAttachmentDao emailAttachmentDao = null;
        public EmailAttachmentDao EmailAttachmentDao
        {
            get
            {
                if (emailAttachmentDao == null)
                    emailAttachmentDao = new EmailAttachmentDao(Connection);
                return emailAttachmentDao;
            }
        }

        private EmailMessageDao emailMessageDao = null;
        public EmailMessageDao EmailMessageDao
        {
            get
            {
                if (emailMessageDao == null)
                    emailMessageDao = new EmailMessageDao(Connection);
                return emailMessageDao;
            }
        }

        private EmailSignDao emailSignDao = null;
        public EmailSignDao EmailSignDao
        {
            get
            {
                if (emailSignDao == null)
                    emailSignDao = new EmailSignDao(Connection);
                return emailSignDao;
            }
        }

        private EmailTextDao emailTextDao = null;
        public EmailTextDao EmailTextDao
        {
            get
            {
                if (emailTextDao == null)
                    emailTextDao = new EmailTextDao(Connection);
                return emailTextDao;
            }
        }

        private ExchangeDao exchangeDao = null;
        public ExchangeDao ExchangeDao
        {
            get
            {
                if (exchangeDao == null)
                    exchangeDao = new ExchangeDao(Connection);
                return exchangeDao;
            }
        }

        private FinalRecipientDao finalRecipientDao = null;
        public FinalRecipientDao FinalRecipientDao
        {
            get
            {
                if (finalRecipientDao == null)
                    finalRecipientDao = new FinalRecipientDao(Connection);
                return finalRecipientDao;
            }
        }

        private InRoleDao inRoleDao = null;
        public InRoleDao InRoleDao
        {
            get
            {
                if (inRoleDao == null)
                    inRoleDao = new InRoleDao(Connection);
                return inRoleDao;
            }
        }

        private InterestGroupDao interestGroupDao = null;
        public InterestGroupDao InterestGroupDao
        {
            get
            {
                if (interestGroupDao == null)
                    interestGroupDao = new InterestGroupDao(Connection);
                return interestGroupDao;
            }
        }

        private InventoryDao inventoryDao = null;
        public InventoryDao InventoryDao
        {
            get
            {
                if (inventoryDao == null)
                    inventoryDao = new InventoryDao(Connection);
                return inventoryDao;
            }
        }

        private InventoryItemDao inventoryItemDao = null;
        public InventoryItemDao InventoryItemDao
        {
            get
            {
                if (inventoryItemDao == null)
                    inventoryItemDao = new InventoryItemDao(Connection);
                return inventoryItemDao;
            }
        }

        private InvoiceItemMarkDao invoiceItemMarkDao = null;
        public InvoiceItemMarkDao InvoiceItemMarkDao
        {
            get
            {
                if (invoiceItemMarkDao == null)
                    invoiceItemMarkDao = new InvoiceItemMarkDao(Connection);
                return invoiceItemMarkDao;
            }
        }

        private InvoiceItemDao invoiceItemDao = null;
        public InvoiceItemDao InvoiceItemDao
        {
            get
            {
                if (invoiceItemDao == null)
                    invoiceItemDao = new InvoiceItemDao(Connection);
                return invoiceItemDao;
            }
        }

        private InvoiceDao invoiceDao = null;
        public InvoiceDao InvoiceDao
        {
            get
            {
                if (invoiceDao == null)
                    invoiceDao = new InvoiceDao(Connection);
                return invoiceDao;
            }
        }

        private LogDao logDao = null;
        public LogDao LogDao
        {
            get
            {
                if (logDao == null)
                    logDao = new LogDao(Connection);
                return logDao;
            }
        }

        private OrderMarkDao orderMarkDao = null;
        public OrderMarkDao OrderMarkDao
        {
            get
            {
                if (orderMarkDao == null)
                    orderMarkDao = new OrderMarkDao(Connection);
                return orderMarkDao;
            }
        }

        private OrderTypeDao orderTypeDao = null;
        public OrderTypeDao OrderTypeDao
        {
            get
            {
                if (orderTypeDao == null)
                    orderTypeDao = new OrderTypeDao(Connection);
                return orderTypeDao;
            }
        }

        private OrderTypeLangDao orderTypeLangDao = null;
        public OrderTypeLangDao OrderTypeLangDao
        {
            get
            {
                if (orderTypeLangDao == null)
                    orderTypeLangDao = new OrderTypeLangDao(Connection);
                return orderTypeLangDao;
            }
        }

        private OtherMovementItemMarkDao otherMovementItemMarkDao = null;
        public OtherMovementItemMarkDao OtherMovementItemMarkDao
        {
            get
            {
                if (otherMovementItemMarkDao == null)
                    otherMovementItemMarkDao = new OtherMovementItemMarkDao(Connection);
                return otherMovementItemMarkDao;
            }
        }

        private OtherMovementItemDao otherMovementItemDao = null;
        public OtherMovementItemDao OtherMovementItemDao
        {
            get
            {
                if (otherMovementItemDao == null)
                    otherMovementItemDao = new OtherMovementItemDao(Connection);
                return otherMovementItemDao;
            }
        }

        private OtherMovementDao otherMovementDao = null;
        public OtherMovementDao OtherMovementDao
        {
            get
            {
                if (otherMovementDao == null)
                    otherMovementDao = new OtherMovementDao(Connection);
                return otherMovementDao;
            }
        }

        private PackageDao packageDao = null;
        public PackageDao PackageDao
        {
            get
            {
                if (packageDao == null)
                    packageDao = new PackageDao(Connection);
                return packageDao;
            }
        }

        private PrescriptionDao prescriptionDao = null;
        public PrescriptionDao PrescriptionDao
        {
            get
            {
                if (prescriptionDao == null)
                    prescriptionDao = new PrescriptionDao(Connection);
                return prescriptionDao;
            }
        }

        private PackageSummaryDao packageSummaryDao = null;
        public PackageSummaryDao PackageSummaryDao
        {
            get
            {
                if (packageSummaryDao == null)
                    packageSummaryDao = new PackageSummaryDao(Connection);
                return packageSummaryDao;
            }
        }

        private PaymentTitleDao paymentTitleDao = null;
        public PaymentTitleDao PaymentTitleDao
        {
            get
            {
                if (paymentTitleDao == null)
                    paymentTitleDao = new PaymentTitleDao(Connection);
                return paymentTitleDao;
            }
        }

        private PaymentTypeDao paymentTypeDao = null;
        public PaymentTypeDao PaymentTypeDao
        {
            get
            {
                if (paymentTypeDao == null)
                    paymentTypeDao = new PaymentTypeDao(Connection);
                return paymentTypeDao;
            }
        }

        private ProductExchangeItemMarkDao productExchangeItemMarkDao = null;
        public ProductExchangeItemMarkDao ProductExchangeItemMarkDao
        {
            get
            {
                if (productExchangeItemMarkDao == null)
                    productExchangeItemMarkDao = new ProductExchangeItemMarkDao(Connection);
                return productExchangeItemMarkDao;
            }
        }

        private ProductExchangeItemDao productExchangeItemDao = null;
        public ProductExchangeItemDao ProductExchangeItemDao
        {
            get
            {
                if (productExchangeItemDao == null)
                    productExchangeItemDao = new ProductExchangeItemDao(Connection);
                return productExchangeItemDao;
            }
        }

        private ProductExchangeDao productExchangeDao = null;
        public ProductExchangeDao ProductExchangeDao
        {
            get
            {
                if (productExchangeDao == null)
                    productExchangeDao = new ProductExchangeDao(Connection);
                return productExchangeDao;
            }
        }

        private ProductMovementDao productMovementDao = null;
        public ProductMovementDao ProductMovementDao
        {
            get
            {
                if (productMovementDao == null)
                    productMovementDao = new ProductMovementDao(Connection);
                return productMovementDao;
            }
        }

        private ProductDao productDao = null;
        public ProductDao ProductDao
        {
            get
            {
                if (productDao == null)
                    productDao = new ProductDao(Connection);
                return productDao;
            }
        }

        private ProductNameLangDao productNameLangDao = null;
        public ProductNameLangDao ProductNameLangDao
        {
            get
            {
                if (productNameLangDao == null)
                    productNameLangDao = new ProductNameLangDao(Connection);
                return productNameLangDao;
            }
        }

        private ProductGroupDao productGroupDao = null;
        public ProductGroupDao ProductGroupDao
        {
            get
            {
                if (productGroupDao == null)
                    productGroupDao = new ProductGroupDao(Connection);
                return productGroupDao;
            }
        }

        private ProductAttributeGroupDao productAttributeGroupDao = null;
        public ProductAttributeGroupDao ProductAttributeGroupDao
        {
            get
            {
                if (productAttributeGroupDao == null)
                    productAttributeGroupDao = new ProductAttributeGroupDao(Connection);
                return productAttributeGroupDao;
            }
        }

        private ProductAttributeDao productAttributeDao = null;
        public ProductAttributeDao ProductAttributeDao
        {
            get
            {
                if (productAttributeDao == null)
                    productAttributeDao = new ProductAttributeDao(Connection);
                return productAttributeDao;
            }
        }

        private ProductVariantDao productVariantDao = null;
        public ProductVariantDao ProductVariantDao
        {
            get
            {
                if (productVariantDao == null)
                    productVariantDao = new ProductVariantDao(Connection);
                return productVariantDao;
            }
        }

        private ProviderDao providerDao = null;
        public ProviderDao ProviderDao
        {
            get
            {
                if (providerDao == null)
                    providerDao = new ProviderDao(Connection);
                return providerDao;
            }
        }

        private PurchaseOrderDao purchaseOrderDao = null;
        public PurchaseOrderDao PurchaseOrderDao
        {
            get
            {
                if (purchaseOrderDao == null)
                    purchaseOrderDao = new PurchaseOrderDao(Connection);
                return purchaseOrderDao;
            }
        }

        private PurchaseOrderItemDao purchaseOrderItemDao = null;
        public PurchaseOrderItemDao PurchaseOrderItemDao
        {
            get
            {
                if (purchaseOrderItemDao == null)
                    purchaseOrderItemDao = new PurchaseOrderItemDao(Connection);
                return purchaseOrderItemDao;
            }
        }

        private PurchaseWarrantyDao purchaseWarrantyDao = null;
        public PurchaseWarrantyDao PurchaseWarrantyDao
        {
            get
            {
                if (purchaseWarrantyDao == null)
                    purchaseWarrantyDao = new PurchaseWarrantyDao(Connection);
                return purchaseWarrantyDao;
            }
        }

        private PurchaseWarrantyItemDao purchaseWarrantyItemDao = null;
        public PurchaseWarrantyItemDao PurchaseWarrantyItemDao
        {
            get
            {
                if (purchaseWarrantyItemDao == null)
                    purchaseWarrantyItemDao = new PurchaseWarrantyItemDao(Connection);
                return purchaseWarrantyItemDao;
            }
        }

        private PurchaseWarrantyNoteDao purchaseWarrantyNoteDao = null;
        public PurchaseWarrantyNoteDao PurchaseWarrantyNoteDao
        {
            get
            {
                if (purchaseWarrantyNoteDao == null)
                    purchaseWarrantyNoteDao = new PurchaseWarrantyNoteDao(Connection);
                return purchaseWarrantyNoteDao;
            }
        }

        private PurchaseWarrantyItemMarkDao purchaseWarrantyItemMarkDao = null;
        public PurchaseWarrantyItemMarkDao PurchaseWarrantyItemMarkDao
        {
            get
            {
                if (purchaseWarrantyItemMarkDao == null)
                    purchaseWarrantyItemMarkDao = new PurchaseWarrantyItemMarkDao(Connection);
                return purchaseWarrantyItemMarkDao;
            }
        }

        private ReceiptDao receiptDao = null;
        public ReceiptDao ReceiptDao
        {
            get
            {
                if (receiptDao == null)
                    receiptDao = new ReceiptDao(Connection);
                return receiptDao;
            }
        }

        private ReceiptItemDao receiptItemDao = null;
        public ReceiptItemDao ReceiptItemDao
        {
            get
            {
                if (receiptItemDao == null)
                    receiptItemDao = new ReceiptItemDao(Connection);
                return receiptItemDao;
            }
        }

        private ReceiptOtherItemDao receiptOtherItemDao = null;
        public ReceiptOtherItemDao ReceiptOtherItemDao
        {
            get
            {
                if (receiptOtherItemDao == null)
                    receiptOtherItemDao = new ReceiptOtherItemDao(Connection);
                return receiptOtherItemDao;
            }
        }

        private ReceiptItemMarkDao receiptItemMarkDao = null;
        public ReceiptItemMarkDao ReceiptItemMarkDao
        {
            get
            {
                if (receiptItemMarkDao == null)
                    receiptItemMarkDao = new ReceiptItemMarkDao(Connection);
                return receiptItemMarkDao;
            }
        }

        private RecordHeaderDao recordHeaderDao = null;
        public RecordHeaderDao RecordHeaderDao
        {
            get
            {
                if (recordHeaderDao == null)
                    recordHeaderDao = new RecordHeaderDao(Connection);
                return recordHeaderDao;
            }
        }

        private RecordFooterDao recordFooterDao = null;
        public RecordFooterDao RecordFooterDao
        {
            get
            {
                if (recordFooterDao == null)
                    recordFooterDao = new RecordFooterDao(Connection);
                return recordFooterDao;
            }
        }

        private RecordNumberDao recordNumberDao = null;
        public RecordNumberDao RecordNumberDao
        {
            get
            {
                if (recordNumberDao == null)
                    recordNumberDao = new RecordNumberDao(Connection);
                return recordNumberDao;
            }
        }

        private RecordSettingFieldDao recordSettingFieldDao = null;
        public RecordSettingFieldDao RecordSettingFieldDao
        {
            get
            {
                if (recordSettingFieldDao == null)
                    recordSettingFieldDao = new RecordSettingFieldDao(Connection);
                return recordSettingFieldDao;
            }
        }

        private RecordSettingImageDao recordSettingImageDao = null;
        public RecordSettingImageDao RecordSettingImageDao
        {
            get
            {
                if (recordSettingImageDao == null)
                    recordSettingImageDao = new RecordSettingImageDao(Connection);
                return recordSettingImageDao;
            }
        }

        private RecordSettingDao recordSettingDao = null;
        public RecordSettingDao RecordSettingDao
        {
            get
            {
                if (recordSettingDao == null)
                    recordSettingDao = new RecordSettingDao(Connection);
                return recordSettingDao;
            }
        }

        private RequisitionDao requisitionDao = null;
        public RequisitionDao RequisitionDao
        {
            get
            {
                if (requisitionDao == null)
                    requisitionDao = new RequisitionDao(Connection);
                return requisitionDao;
            }
        }

        private RequisitionItemDao requisitionItemDao = null;
        public RequisitionItemDao RequisitionItemDao
        {
            get
            {
                if (requisitionItemDao == null)
                    requisitionItemDao = new RequisitionItemDao(Connection);
                return requisitionItemDao;
            }
        }

        private RequisitionItemMarkDao requisitionItemMarkDao = null;
        public RequisitionItemMarkDao RequisitionItemMarkDao
        {
            get
            {
                if (requisitionItemMarkDao == null)
                    requisitionItemMarkDao = new RequisitionItemMarkDao(Connection);
                return requisitionItemMarkDao;
            }
        }

        private RolePermissionDao rolePermissionDao = null;
        public RolePermissionDao RolePermissionDao
        {
            get
            {
                if (rolePermissionDao == null)
                    rolePermissionDao = new RolePermissionDao(Connection);
                return rolePermissionDao;
            }
        }

        private RoleDao roleDao = null;
        public RoleDao RoleDao
        {
            get
            {
                if (roleDao == null)
                    roleDao = new RoleDao(Connection);
                return roleDao;
            }
        }

        private SellOrderDao sellOrderDao = null;
        public SellOrderDao SellOrderDao
        {
            get
            {
                if (sellOrderDao == null)
                    sellOrderDao = new SellOrderDao(Connection);
                return sellOrderDao;
            }
        }

        private SellOrderItemDao sellOrderItemDao = null;
        public SellOrderItemDao SellOrderItemDao
        {
            get
            {
                if (sellOrderItemDao == null)
                    sellOrderItemDao = new SellOrderItemDao(Connection);
                return sellOrderItemDao;
            }
        }

        private SellWarrantyDao sellWarrantyDao = null;
        public SellWarrantyDao SellWarrantyDao
        {
            get
            {
                if (sellWarrantyDao == null)
                    sellWarrantyDao = new SellWarrantyDao(Connection);
                return sellWarrantyDao;
            }
        }

        private SellWarrantyItemDao sellWarrantyItemDao = null;
        public SellWarrantyItemDao SellWarrantyItemDao
        {
            get
            {
                if (sellWarrantyItemDao == null)
                    sellWarrantyItemDao = new SellWarrantyItemDao(Connection);
                return sellWarrantyItemDao;
            }
        }

        private SellWarrantyNoteDao sellWarrantyNoteDao = null;
        public SellWarrantyNoteDao SellWarrantyNoteDao
        {
            get
            {
                if (sellWarrantyNoteDao == null)
                    sellWarrantyNoteDao = new SellWarrantyNoteDao(Connection);
                return sellWarrantyNoteDao;
            }
        }

        private SellWarrantyItemMarkDao sellWarrantyItemMarkDao = null;
        public SellWarrantyItemMarkDao SellWarrantyItemMarkDao
        {
            get
            {
                if (sellWarrantyItemMarkDao == null)
                    sellWarrantyItemMarkDao = new SellWarrantyItemMarkDao(Connection);
                return sellWarrantyItemMarkDao;
            }
        }

        private SettlementDao settlementDao = null;
        public SettlementDao SettlementDao
        {
            get
            {
                if (settlementDao == null)
                    settlementDao = new SettlementDao(Connection);
                return settlementDao;
            }
        }

        private SettlementTypeDao settlementTypeDao = null;
        public SettlementTypeDao SettlementTypeDao
        {
            get
            {
                if (settlementTypeDao == null)
                    settlementTypeDao = new SettlementTypeDao(Connection);
                return settlementTypeDao;
            }
        }

        private SettlementTypeLangDao settlementTypeLangDao = null;
        public SettlementTypeLangDao SettlementTypeLangDao
        {
            get
            {
                if (settlementTypeLangDao == null)
                    settlementTypeLangDao = new SettlementTypeLangDao(Connection);
                return settlementTypeLangDao;
            }
        }

        private SpecificationDao specificationDao = null;
        public SpecificationDao SpecificationDao
        {
            get
            {
                if (specificationDao == null)
                    specificationDao = new SpecificationDao(Connection);
                return specificationDao;
            }
        }

        private StorePackageDao storePackageDao = null;
        public StorePackageDao StorePackageDao
        {
            get
            {
                if (storePackageDao == null)
                    storePackageDao = new StorePackageDao(Connection);
                return storePackageDao;
            }
        }

        private StoreProductDao storeProductDao = null;
        public StoreProductDao StoreProductDao
        {
            get
            {
                if (storeProductDao == null)
                    storeProductDao = new StoreProductDao(Connection);
                return storeProductDao;
            }
        }

        private TandemProductDao tandemProductDao = null;
        public TandemProductDao TandemProductDao
        {
            get
            {
                if (tandemProductDao == null)
                    tandemProductDao = new TandemProductDao(Connection);
                return tandemProductDao;
            }
        }

        private TransferDao transferDao = null;
        public TransferDao TransferDao
        {
            get
            {
                if (transferDao == null)
                    transferDao = new TransferDao(Connection);
                return transferDao;
            }
        }

        private TransferSummaryDao transferSummaryDao = null;
        public TransferSummaryDao TransferSummaryDao
        {
            get
            {
                if (transferSummaryDao == null)
                    transferSummaryDao = new TransferSummaryDao(Connection);
                return transferSummaryDao;
            }
        }

        private TransferItemDao transferItemDao = null;
        public TransferItemDao TransferItemDao
        {
            get
            {
                if (transferItemDao == null)
                    transferItemDao = new TransferItemDao(Connection);
                return transferItemDao;
            }
        }

        private TransferItemMarkDao transferItemMarkDao = null;
        public TransferItemMarkDao TransferItemMarkDao
        {
            get
            {
                if (transferItemMarkDao == null)
                    transferItemMarkDao = new TransferItemMarkDao(Connection);
                return transferItemMarkDao;
            }
        }

        private TransportTypeDao transportTypeDao = null;
        public TransportTypeDao TransportTypeDao
        {
            get
            {
                if (transportTypeDao == null)
                    transportTypeDao = new TransportTypeDao(Connection);
                return transportTypeDao;
            }
        }

        private TransportTypeLangDao transportTypeLangDao = null;
        public TransportTypeLangDao TransportTypeLangDao
        {
            get
            {
                if (transportTypeLangDao == null)
                    transportTypeLangDao = new TransportTypeLangDao(Connection);
                return transportTypeLangDao;
            }
        }

        private UserDao userDao = null;
        public UserDao UserDao
        {
            get
            {
                if (userDao == null)
                    userDao = new UserDao(Connection);
                return userDao;
            }
        }

        private UserShortcutDao userShortcutDao = null;
        public UserShortcutDao UserShortcutDao
        {
            get
            {
                if (userShortcutDao == null)
                    userShortcutDao = new UserShortcutDao(Connection);
                return userShortcutDao;
            }
        }

        private VersionDao versionDao = null;
        public VersionDao VersionDao
        {
            get
            {
                if (versionDao == null)
                    versionDao = new VersionDao(Connection);
                return versionDao;
            }
        }

        private VoucherDao voucherDao = null;
        public VoucherDao VoucherDao
        {
            get
            {
                if (voucherDao == null)
                    voucherDao = new VoucherDao(Connection);
                return voucherDao;
            }
        }

        private VoucherMarkDao voucherMarkDao = null;
        public VoucherMarkDao VoucherMarkDao
        {
            get
            {
                if (voucherMarkDao == null)
                    voucherMarkDao = new VoucherMarkDao(Connection);
                return voucherMarkDao;
            }
        }

        private VoucherTypeDao voucherTypeDao = null;
        public VoucherTypeDao VoucherTypeDao
        {
            get
            {
                if (voucherTypeDao == null)
                    voucherTypeDao = new VoucherTypeDao(Connection);
                return voucherTypeDao;
            }
        }

        private ProductSummaryDao productSummaryDao = null;
        public ProductSummaryDao ProductSummaryDao
        {
            get
            {
                if (productSummaryDao == null)
                    productSummaryDao = new ProductSummaryDao(Connection);
                return productSummaryDao;
            }
        }

        private ProductImageDao productImageDao = null;
        public ProductImageDao ProductImageDao
        {
            get
            {
                if (productImageDao == null)
                    productImageDao = new ProductImageDao(Connection);
                return productImageDao;
            }
        }

        private StoreTransactionDao storeTransactionDao = null;
        public StoreTransactionDao StoreTransactionDao
        {
            get
            {
                if (storeTransactionDao == null)
                    storeTransactionDao = new StoreTransactionDao(Connection);
                return storeTransactionDao;
            }
        }

        private UsedDifferentialMarksDao usedDifferentialMarksDao = null;
        public UsedDifferentialMarksDao UsedDifferentialMarksDao
        {
            get
            {
                if (usedDifferentialMarksDao == null)
                    usedDifferentialMarksDao = new UsedDifferentialMarksDao(Connection, DifferentialMarkDao);
                return usedDifferentialMarksDao;
            }
        }

        private ProductVariantAttributesDao productVariantAttributesDao = null;
        public ProductVariantAttributesDao ProductVariantAttributesDao
        {
            get
            {
                if (productVariantAttributesDao == null)
                    productVariantAttributesDao = new ProductVariantAttributesDao(Connection, ProductAttributeDao);
                return productVariantAttributesDao;
            }
        }
    }
}
