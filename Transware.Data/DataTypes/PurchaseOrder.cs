﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PurchaseOrder : AbstractValidDto
    {
        public long Store { get; set; }
        public long SupplierID { get; set; }
        public string SupplierName { get; set; }
        public long SupplierAddressID { get; set; }
        public string SupplierAddress { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public long CustomerAddressID { get; set; }
        public string CustomerAddress { get; set; }
        public long TransportType { get; set; }
        public long OrderType { get; set; }
        public long Number { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string DeliveryDateText { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public long Clerk { get; set; }
        public string Text { get; set; }
        public string Specification { get; set; }
        public string AdditionalText { get; set; }
        public bool ShowUnitPrice { get; set; }        
    }
}
