﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class CashReceipt : AbstractValidDto
    {
        public long Store { get; set; }
        public long SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAddress { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public decimal Amount { get; set; }
        public long Currency { get; set; }
        public string Purpose { get; set; }
        public string ApprovedBy { get; set; }
        public string Sheet { get; set; }
        public string SheetNumber { get; set; }
        public string Text { get; set; }
        public string Budget { get; set; }
        public string ReceivedBy { get; set; }
    }
}
