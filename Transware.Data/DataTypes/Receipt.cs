﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Receipt : AbstractValidDto
    {
        public long Store { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime ExposureDate { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public long ProviderID { get; set; }
        public string InvoiceNumber { get; set; }
        public string DeliveryNumber { get; set; }
        public string Description { get; set; }
        public long ItemsCurrency { get; set; }
        public decimal ItemsRateUnit { get; set; }
        public decimal ItemsRate { get; set; }
        public long OtherCurrency { get; set; }
        public decimal OtherRateUnit { get; set; }
        public decimal OtherRate { get; set; }        
    }
}
