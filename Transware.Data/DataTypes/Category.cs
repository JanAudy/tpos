﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Category : AbstractValidDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long ParentID { get; set; }
        public int Position { get; set; }
        public byte[] Image { get; set; }
        public bool Internet { get; set; }
        public ECategoryType Type { get; set; }

        public Category parent = null;
        internal event LazyLoadObjectLong<Category> LoadParent = null;
        public Category Parent
        {
            get
            {
                if (parent == null && ParentID!=0)
                    if (LoadParent != null)
                        LoadParent(this, ParentID, ref parent);

                return parent;
            }
            set { parent = value; ParentID = value.ID; }
        }

        public List<Category> subcategories = null;
        internal event LazyLoadListLong<Category> LoadSubcategories = null;
        public List<Category> Subcategories
        {
            get
            {
                if (subcategories == null)
                    if (LoadSubcategories != null)
                        LoadSubcategories(this, ID, ref subcategories);

                return subcategories;
            }
            set { subcategories = value;}
        }

        public List<Product> products = null;
        internal event LazyLoadListLong<Product> LoadProducts = null;
        public List<Product> Products
        {
            get
            {
                if (products == null)
                    if (LoadProducts != null)
                        LoadProducts(this, ID, ref products);

                return products;
            }
            set { products = value; }
        }


        public class PositionComparer : IComparer<Category>
     
        {
            #region IComparer<Category> Members

            public int Compare(Category x, Category y)
            {
                return x.Position.CompareTo(y.Position);
            }

            #endregion
        }
    }

    public enum ECategoryType
    {
        Normal = 0,
        Jidelna_DayMenu = 1,
        Jidelna_Menu = 2
    }
}
