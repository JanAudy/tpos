﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class Product : AbstractValidDto
    {
        #region Fields

        public string Name { get; set; }
        public string ShortName { get; set; }
        public long? CategoryID { get; set; }
        public long? UnitID { get; set; }
        public long? VatID { get; set; }
        public long? CTaxID { get; set; }
        public long? TimePeriodID { get; set; }
        public int PlaceNo { get; set; }
        public string Specification { get; set; }
        public long? PriceCalculationID { get; set; }
        public long? ProviderID { get; set; }
        public long Number { get; set; }
        public string Note { get; set; }
        public byte[] Image { get; set; }
        public long? OrderMark { get; set; }
        public long? SettingID { get; set; }
        public string UserID1 { get; set; }
        public string UserID2 { get; set; }
        public long? ProductGroupID { get; set; }
        public string DetailedSpecification { get; set; }

        #endregion

        #region ForeignObjects

        internal Category category = null;
        internal event LazyLoadObjectLong<Category> LoadCategory = null;
        public Category Category
        {
            get
            {
                if (category == null && CategoryID != 0)
                    if (LoadCategory != null && CategoryID.HasValue)
                        LoadCategory(this, CategoryID.Value, ref category);

                return category;
            }
            set { category = value; CategoryID = (value != null) ? value.ID : (long?)null; }
        }

        internal Unit unit = null;
        internal event LazyLoadObjectLong<Unit> LoadUnit = null;
        public Unit Unit
        {
            get
            {
                if (unit == null && UnitID != 0)
                    if (LoadUnit != null && UnitID.HasValue)
                        LoadUnit(this, UnitID.Value, ref unit);

                return unit;
            }
            set { unit = value; UnitID = (value != null) ? value.ID : (long?)null; }
        }

        internal Vat vat = null;
        internal event LazyLoadObjectLong<Vat> LoadVat = null;
        public Vat Vat
        {
            get
            {
                if (vat == null && VatID != 0)
                    if (LoadVat != null && VatID.HasValue)
                        LoadVat(this, VatID.Value, ref vat);

                return vat;
            }
            set { vat = value; VatID = (value != null) ? value.ID : (long?)null; }
        }

        internal CTax cTax = null;
        internal event LazyLoadObjectLong<CTax> LoadCTax = null;
        public CTax CTax
        {
            get
            {
                if (cTax == null && CTaxID != 0)
                    if (LoadCTax != null && CTaxID.HasValue)
                        LoadCTax(this, CTaxID.Value, ref cTax);

                return cTax;
            }
            set { cTax = value; CTaxID = (value != null) ? value.ID : (long?)null; }
        }

        internal TimePeriod timePeriod = null;
        internal event LazyLoadObjectLong<TimePeriod> LoadTimePeriod = null;
        public TimePeriod TimePeriod
        {
            get
            {
                if (timePeriod == null && TimePeriodID != 0)
                    if (LoadTimePeriod != null && TimePeriodID.HasValue)
                        LoadTimePeriod(this, TimePeriodID.Value, ref timePeriod);

                return timePeriod;
            }
            set { timePeriod = value; TimePeriodID = (value != null) ? value.ID : (long?)null; }
        }

        internal PriceCalculation priceCalculation = null;
        internal event LazyLoadObjectLong<PriceCalculation> LoadPriceCalculation = null;
        public PriceCalculation PriceCalculation
        {
            get
            {
                if (priceCalculation == null && PriceCalculationID != 0)
                    if (LoadPriceCalculation != null && PriceCalculationID.HasValue)
                        LoadPriceCalculation(this, PriceCalculationID.Value, ref priceCalculation);

                return priceCalculation;
            }
            set { priceCalculation = value; PriceCalculationID = (value != null) ? value.ID : (long?)null; }
        }

        internal Provider provider = null;
        internal event LazyLoadObjectLong<Provider> LoadProvider = null;
        public Provider Provider
        {
            get
            {
                if (provider == null && ProviderID != 0)
                    if (LoadProvider != null && ProviderID.HasValue)
                        LoadProvider(this, ProviderID.Value, ref provider);

                return provider;
            }
            set { provider = value; ProviderID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductSetting productSetting = null;
        internal event LazyLoadObjectLong<ProductSetting> LoadProductSetting = null;
        public ProductSetting Setting
        {
            get
            {
                if (productSetting == null && SettingID != 0)
                    if (LoadProductSetting != null && SettingID.HasValue)
                        LoadProductSetting(this, SettingID.Value, ref productSetting);

                return productSetting;
            }
            set { productSetting = value; SettingID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductGroup productGroup = null;
        internal event LazyLoadObjectLong<ProductGroup> LoadProductGroup = null;
        public ProductGroup ProductGroup
        {
            get
            {
                if (productGroup == null && ProductGroupID != 0)
                    if (LoadProductGroup != null && ProductGroupID.HasValue)
                        LoadProductGroup(this, ProductGroupID.Value, ref productGroup);

                return productGroup;
            }
            set { productGroup = value; ProductGroupID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region Collections

        internal Dictionary<ELanguage, ProductNameLang> languages;
        internal event LazyLoadDictionary<ELanguage, ProductNameLang> LoadLanguages;
        public Dictionary<ELanguage, ProductNameLang> Languages
        {
            get
            {
                if (languages == null)
                    if (LoadLanguages != null)
                        LoadLanguages(this, this.ID, ref languages);
                    else
                        languages = new Dictionary<ELanguage, ProductNameLang>();

                return languages;
            }
            internal set { languages = value; }
        }

        internal List<Provider> providers = null;
        internal event LazyLoadListLong<Provider> LoadProviders = null;
        public List<Provider> Providers
        {
            get
            {
                if (providers == null)
                    if (LoadProviders != null)
                        LoadProviders(this, ID, ref providers);

                return providers;
            }
            internal set { providers = value; }
        }

        internal List<Package> packages = null;
        internal event LazyLoadListLong<Package> LoadPackages = null;
        public List<Package> Packages
        {
            get
            {
                if (packages == null)
                    if (LoadPackages != null)
                        LoadPackages(this, ID, ref packages);

                return packages;
            }
            internal set { packages = value; }
        }

        internal List<ProductVariant> variants = null;
        internal event LazyLoadListLong<ProductVariant> LoadVariants = null;
        public List<ProductVariant> Variants
        {
            get
            {
                if (variants == null)
                    if (LoadVariants != null)
                        LoadVariants(this, ID, ref variants);

                return variants;
            }
            internal set { variants = value; }
        }


        internal List<ProductImage> images = null;
        internal event LazyLoadListLong<ProductImage> LoadImages = null;
        public List<ProductImage> Images
        {
            get
            {
                if (images == null)
                    if (LoadImages != null)
                        LoadImages(this, ID, ref images);

                return images;
            }
            internal set { images = value; }
        }

        internal List<ProductMovement> movements = null;
        internal event LazyLoadListLong<ProductMovement> LoadMovements = null;
        public List<ProductMovement> Movements
        {
            get
            {
                if (movements == null)
                    if (LoadMovements != null)
                        LoadMovements(this, ID, ref movements);

                return movements;
            }
            internal set { movements = value; }
        }

        internal Dictionary<long, StoreProduct> storeProducts = null;
        internal event LazyLoadDictionary<long, StoreProduct> LoadStoreProducts = null;
        public Dictionary<long, StoreProduct> StoreProducts
        {
            get
            {
                if (storeProducts == null)
                    if (LoadStoreProducts != null)
                        LoadStoreProducts(this, ID, ref storeProducts);

                return storeProducts;
            }
            internal set { storeProducts = value; }
        }

        internal HashSet<DifferentialMark> differentialMarks = null;
        internal event LazyLoadHashSet<DifferentialMark> LoadDifferentialMarks = null;
        public HashSet<DifferentialMark> DifferentialMarks
        {
            get
            {
                if (differentialMarks == null)
                    if (LoadDifferentialMarks != null)
                        LoadDifferentialMarks(this, ID, ref differentialMarks);

                return differentialMarks;
            }
            internal set { differentialMarks = value; }
        }

        #endregion

        #region Constructors

        public Product()
        {

        }

        public Product(Category category): this()
        {
            this.Category = category;
        }

        #endregion

        #region Override

        public override void AssignIDToChildren()
        {
            if (languages != null)
            {
                foreach (ProductNameLang l in languages.Values)
                    l.ProductID = ID;
            }
            if (providers != null)
            {
                foreach (Provider o in providers)
                    o.ProductID = ID;
            }

            if (packages != null)
            {
                foreach (Package o in packages)
                    o.ProductID = ID;
            }

            if (variants != null)
            {
                foreach (ProductVariant o in variants)
                    o.ProductID = ID;
            }

            if (images != null)
            {
                foreach (ProductImage o in images)
                    o.Product = ID;
            }

            if (movements != null)
            {
                foreach (ProductMovement o in movements)
                    o.ProductID = ID;
            }

            if (storeProducts != null)
            {
                foreach (StoreProduct o in storeProducts.Values)
                    o.ProductID = ID;
            }
        }

        #endregion


        #region Methods

        public Provider GetProvider(long organizationID)
        {
            foreach (Provider p in Providers)
            {
                if (p.OrganizationID == organizationID)
                    return p;
            }
            return null;
        }

        #endregion

        public class PlaceNoComparer : IComparer<Product>
        {
            #region IComparer<Product> Members

            public int Compare(Product x, Product y)
            {
                return x.PlaceNo.CompareTo(y.PlaceNo);
            }

            #endregion
        }
    }
}
