﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class StoreTransaction : AbstractIDLongDto
    {
        public long Store { get; set; }
        public DateTime DateTime { get; set; }
        public EMovementType MovementType { get; set; }
        public ERecordType RecordType { get; set; }
        public long RecordID { get; set; }
        public long RecordNumber { get; set; }
        public string RecordNumberStr { get; set; }
        public long RecordItemID { get; set; }
        public long Product { get; set; }
        public long Variant { get; set; }
        public decimal Amount { get; set; }
        public decimal PurchasePriceNoVat { get; set; }
        public decimal SellPriceNoVat { get; set; }

        public bool Inverse { get; set; }
        public bool MakeMovementsInvalid { get; set; }
        public bool UpdatePurchasePricesOnRelease { get; set; }


    }
}
