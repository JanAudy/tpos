﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class TandemProduct : AbstractValidDto
    {
        public long? PackageID { get; set; }
        public long? TandemPackageID { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }

        #region ForeignObjects

        internal Package package = null;
        internal event LazyLoadObjectLong<Package> LoadPackage = null;
        public Package Package
        {
            get
            {
                if (package == null && PackageID != 0)
                    if (LoadPackage != null && PackageID.HasValue)
                        LoadPackage(this, PackageID.Value, ref package);

                return package;
            }
            set { package = value; PackageID = (value != null) ? value.ID : (long?)null; }
        }

        internal Package tandem = null;
        internal event LazyLoadObjectLong<Package> LoadTandem = null;
        public Package Tandem
        {
            get
            {
                if (tandem == null && TandemPackageID != 0)
                    if (LoadTandem != null && TandemPackageID.HasValue)
                        LoadTandem(this, TandemPackageID.Value, ref tandem);

                return package;
            }
            set { tandem = value; TandemPackageID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region Constructor

        public TandemProduct()
        {

        }

        public TandemProduct(Package package) : this()
        {
            this.Package = package;
        }

        #endregion

        #region Methods

        public virtual TandemProduct Clone(Package p)
        {
            TandemProduct t = new TandemProduct(p);
            t.Tandem = Tandem;
            t.Amount = Amount;
            t.Price = Price;
            return t;
        }


        #endregion
    }
}
