﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class EmailMessage : AbstractValidDto
    {
        public DateTime DateTime { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int RecordType { get; set; }
        public long RecordID { get; set; }
        public int RecordStatus { get; set; }
    }
}
