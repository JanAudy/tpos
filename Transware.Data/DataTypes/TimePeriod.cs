﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class TimePeriod : AbstractValidDto
    {
        public string Name { get; set; }
        public string Text { get; set; }


        public override string ToString()
        {
            return Name + " " + Text;
        }
    }
}
