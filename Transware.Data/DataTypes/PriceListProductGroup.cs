﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class PriceListProductGroup : AbstractValidDto
    {
        public long? PriceList { get; set; }
        public long? ProductGroup { get; set; }
        public decimal Coefficient { get; set; }
        public decimal Addition { get; set; }

        public virtual decimal GetPrice(decimal price)
        {
            return price * Coefficient + Addition;
        }
    }
}
