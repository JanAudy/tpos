﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class BillVoucherMark : AbstractValidDto
    {
        public long BillVoucher { get; set; }
        public long Mark { get; set; }
        public string Value { get; set; }

    }
}
