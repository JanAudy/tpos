﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Role : AbstractIDLongDto
    {
        public string Name { get; set; }
        public string MenuString { get; set; }
        
    }
}
