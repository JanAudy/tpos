﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Cover : AbstractValidDto
    {
        public long? PackageID { get; set; }
        public long? CoverID { get; set; }
        public decimal Amount { get; set; }

        #region ForeignObjects

        internal Package package = null;
        internal event LazyLoadObjectLong<Package> LoadPackage = null;
        public Package Package
        {
            get
            {
                if (package == null && PackageID != 0)
                    if (LoadPackage != null && PackageID.HasValue)
                        LoadPackage(this, PackageID.Value, ref package);

                return package;
            }
            set { package = value; PackageID = (value != null) ? value.ID : (long?)null; }
        }

        internal Package cover = null;
        internal event LazyLoadObjectLong<Package> LoadCover = null;
        public Package CoverPackage
        {
            get
            {
                if (cover == null && CoverID != 0)
                    if (LoadCover != null && CoverID.HasValue)
                        LoadCover(this, CoverID.Value, ref cover);

                return package;
            }
            set { cover = value; CoverID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region Constructors

        public Cover()
        {
        }

        public Cover(Package package)
            : this()
        {
            Package = package;
        }

        #endregion

        #region Methods

        public virtual Cover Clone(Package p)
        {
            Cover c = new Cover(p);
            c.CoverPackage = CoverPackage;
            c.Amount = Amount;
            return c;
        }

        #endregion
    }
}
