﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.DataTypes
{
    public class TransportTypeLang : TranslatedText
    {
        public long TransportTypeID { get; set; }
    }
}
