﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class Currency : AbstractValidDto
    {
        public string Name { get; set; }
        public string Shortcut { get; set; }
        public ERoundingType Rounding { get; set; }
        public int Places { get; set; }
        public ERoundingType PackageRounding { get; set; }
        public int PackagePlaces { get; set; }

        #region Methods

        public virtual decimal GetRounding(decimal price)
        {
            return Currency.GetRounding(Rounding, Places, price);
        }

        public virtual decimal GetPackageRounding(decimal price)
        {
            return Currency.GetRounding(PackageRounding, PackagePlaces, price);
        }

        #endregion


        #region Static Methods

        public static decimal GetRounding(ERoundingType Rounding, int Places, decimal price)
        {
            price *= (decimal)Math.Pow(10, Places);

            decimal trunc = decimal.Truncate(price);
            price -= trunc;
            decimal rounding = 0;

            switch (Rounding)
            {
                case ERoundingType.Down: rounding = -price; break;
                case ERoundingType.Up: rounding = 1 - price; break;
                case ERoundingType.Math:
                    if (price < 0.5m) rounding = -price; else rounding = 1 - price;
                    break;
            }
            rounding /= (decimal)Math.Pow(10, Places);

            return rounding;
        }

        #endregion

        public override string ToString()
        {
            return Shortcut;
        }
    }
}
