﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class AdvInvoice : AbstractValidDto
    {
        public long Store { get; set; }
        public long SupplierID { get; set; }
        public string SupplierName { get; set; }
        public long SupplierAddressID { get; set; }
        public string SupplierAddress { get; set; }
        public long BankAccount { get; set; }
        public string BankName { get; set; }
        public string BankAccountStr { get; set; }
        public string BankCode { get; set; }
        public string BankIBAN { get; set; }
        public string BankSWIFT { get; set; }
        public long CustomerID { get; set; }
        public int CustomerType { get; set; }
        public string CustomerName { get; set; }
        public long CustomerAddressID { get; set; }
        public string CustomerAddress { get; set; }
        public long TransportType { get; set; }
        public long FinalRecipient { get; set; }
        public string FinalRecipientStr { get; set; }
        public string OrderNo { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public long DeliveryNo { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public DateTime MaturityDate { get; set; }
        public DateTime TaxApplyDate { get; set; }
        public long SettlementType { get; set; }
        public double Penalization { get; set; }
        public bool Settled { get; set; }
        public DateTime SettledDate { get; set; }
        public decimal AmountLeft { get; set; }
        public long ItemCurrency { get; set; }
        public long IndividualDiscount { get; set; }
        public decimal CustomerDiscount { get; set; }
        public decimal Rounding { get; set; }
        public long Clerk { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
        public bool UseExchange { get; set; }
        public decimal RateUnit { get; set; }
        public decimal Rate { get; set; }
        public int RoundingType { get; set; }
        public int RoundingPlaces { get; set; }
        public long CustomerTransaction { get; set; }

        
    }
}
