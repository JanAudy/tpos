﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class SettlementType : AbstractValidDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Actual { get; set; }
        public EUsedExchangeRate Rate { get; set; }
        public bool Rounding { get; set; }
        public bool IsCash { get; set; }
        public bool InstantPayment { get; set; }
        public decimal Fee { get; set; }

        internal Dictionary<ELanguage, SettlementTypeLang> languages;
        internal event LazyLoadDictionary<ELanguage, SettlementTypeLang> LoadLanguages;
        public Dictionary<ELanguage, SettlementTypeLang> Languages
        {
            get
            {
                if (languages == null)
                    if (LoadLanguages != null)
                        LoadLanguages(this, this.ID, ref languages);
                    else
                        languages = new Dictionary<ELanguage, SettlementTypeLang>();

                return languages;
            }
            set { languages = value; }
        }

        public override void AssignIDToChildren()
        {
            if (languages != null && languages.Count > 0)
                foreach (SettlementTypeLang ul in languages.Values)
                    ul.SettlementTypeID = ID;
        }
    }
}
