﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Version : AbstractValidDto
    {
        public long DBver { get; set; }
        public long MinAppVer { get; set; }
        public DateTime Changed { get; set; }
        
    }
}
