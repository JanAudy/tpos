﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class CountryLang : AbstractIDLongDto
    {
        public long Country { get; set; }
        
    }
}
