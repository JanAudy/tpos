﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.DataTypes
{
    public class SettlementTypeLang : TranslatedText
    {
        public long SettlementTypeID { get; set; }
    }
}
