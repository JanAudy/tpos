﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class CTax : AbstractValidDto
    {
        public string Name { get; set; }
        public int Tax { get; set; }


        public override string ToString()
        {
            return Tax.ToString("0.#####") + " %";
        }
    }
}
