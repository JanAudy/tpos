﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class TransportType : AbstractValidDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Actual { get; set; }

        internal Dictionary<ELanguage, TransportTypeLang> languages;
        internal event LazyLoadDictionary<ELanguage, TransportTypeLang> LoadLanguages;
        public Dictionary<ELanguage, TransportTypeLang> Languages
        {
            get
            {
                if (languages == null)
                    if (LoadLanguages != null)
                        LoadLanguages(this, this.ID, ref languages);
                    else
                        languages = new Dictionary<ELanguage, TransportTypeLang>();

                return languages;
            }
            set { languages = value; }
        }

        public override void AssignIDToChildren()
        {
            if (languages != null && languages.Count > 0)
                foreach (TransportTypeLang ul in languages.Values)
                    ul.TransportTypeID = ID;
        }
    }
}
