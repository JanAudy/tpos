﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class DeliveryNote : AbstractValidDto
    {
        public long Store { get; set; }
        public long SupplierID { get; set; }
        public string SupplierName { get; set; }
        public long SupplierAddressID { get; set; }
        public string SupplierAddress { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public long CustomerAddressID { get; set; }
        public string CustomerAddress { get; set; }
        public long TransportType { get; set; }
        public long FinalRecipient { get; set; }
        public string FinalRecipientStr { get; set; }
        public string OrderNo { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public int Creation { get; set; }
        public int PrintPrices { get; set; }
        public long ItemsCurrency { get; set; }
        public long Clerk { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
    }
}
