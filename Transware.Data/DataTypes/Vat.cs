﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Vat : AbstractValidDto
    {
        public string Name { get; set; }
        public decimal Tax { get; set; }

        public static decimal GetPrice(decimal Tax, decimal price)
        {
            decimal f = price;

            f = price * ((100 + Tax) / 100.0m);
            return f;
        }

        public static decimal GetPriceNoVat(decimal Tax, decimal price)
        {
            decimal f = price;

            f = price * (100.0m / (100 + Tax));
            return f;
        }

        public virtual decimal GetPrice(decimal price)
        {
            return Vat.GetPrice(Tax, price);
        }

        public virtual decimal GetPriceNoVat(decimal price)
        {
            return Vat.GetPriceNoVat(Tax, price);
        }

        public override string ToString()
        {
            return Tax.ToString("0.#####") + " %";
        }
    }
}
