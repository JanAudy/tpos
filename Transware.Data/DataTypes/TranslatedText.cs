﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    /// <summary>
    /// Base class for translated text of list items
    /// </summary>
    public class TranslatedText : AbstractValidDto, IComparable<TranslatedText>
    {
        public ELanguage Language { get; set; }
        public string Value { get; set; }

        public override int GetHashCode()
        {
            return Language.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is TranslatedText)
            {
                TranslatedText other = obj as TranslatedText;
                return other.Language == Language;
            }
            return base.Equals(obj);
        }

        #region IComparable<TranslatedText> Members

        public int CompareTo(TranslatedText other)
        {
            return Language.CompareTo(other.Language);
        }

        #endregion
    }
}
