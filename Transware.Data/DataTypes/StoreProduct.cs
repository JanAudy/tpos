﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class StoreProduct : AbstractIDLongDto
    {
        public long? StoreID { get; set; }
        public long? ProductID { get; set; }
        public long? VariantID { get; set; }
        public string PlaceDesc { get; set; }
        public long? PositionID { get; set; }
        public decimal? RequestedMin { get; set; }
        public decimal? RequestedMax { get; set; }
        public decimal RealNoReserved { get; set; }
        public decimal RealReserved { get; set; }
        public decimal RealTotal { get; set; }
        public decimal RealLocked { get; set; }
        public DateTime LastIncome { get; set; }
        public DateTime LastRelease { get; set; }
        public DateTime LastChange { get; set; }
        public decimal AveragePurchaseUnitPriceNoVat { get; set; }


        public StoreProduct()
        {
            LastIncome = ZeroDateTime;
            LastRelease = ZeroDateTime;
            LastChange = ZeroDateTime;
        }

        #region ForeignObjects

        internal Store store = null;
        internal event LazyLoadObjectLong<Store> LoadStore = null;
        public Store Store
        {
            get
            {
                if (store == null && StoreID != 0)
                    if (LoadStore != null && StoreID.HasValue)
                        LoadStore(this, StoreID.Value, ref store);

                return store;
            }
            set { store = value; StoreID = (value != null) ? value.ID : (long?)null; }
        }

        internal Product product = null;
        internal event LazyLoadObjectLong<Product> LoadProduct = null;
        public Product Product
        {
            get
            {
                if (product == null && ProductID != 0)
                    if (LoadProduct != null && ProductID.HasValue)
                        LoadProduct(this, ProductID.Value, ref product);

                return product;
            }
            set { product = value; ProductID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductVariant variant = null;
        internal event LazyLoadObjectLong<ProductVariant> LoadVariant = null;
        public ProductVariant Variant
        {
            get
            {
                if (variant == null && VariantID != 0)
                    if (LoadVariant != null && VariantID.HasValue)
                        LoadVariant(this, VariantID.Value, ref variant);

                return variant;
            }
            set { variant = value; VariantID = (value != null) ? value.ID : (long?)null; }
        }

        internal Position position = null;
        internal event LazyLoadObjectLong<Position> LoadPosition = null;
        public Position Position
        {
            get
            {
                if (position == null && PositionID != 0)
                    if (LoadPosition != null && PositionID.HasValue)
                        LoadPosition(this, PositionID.Value, ref position);

                return position;
            }
            set { position = value; PositionID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion
    }
}
