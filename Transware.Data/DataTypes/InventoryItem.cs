﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class InventoryItem : AbstractValidDto
    {
        public long Inventory { get; set; }
        public long Product { get; set; }
        public decimal StoreAmount { get; set; }
        public decimal RealAmount { get; set; }
    }
}
