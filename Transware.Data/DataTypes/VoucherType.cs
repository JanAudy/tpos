﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class VoucherType : AbstractValidDto
    {
        public string Name { get; set; }
        public bool AllowReturn { get; set; }
        public bool UseValue { get; set; }
        
    }
}
