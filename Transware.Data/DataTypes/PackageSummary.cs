﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PackageSummary : AbstractValidDto
    {
        public long ProductID { get; set; }
        public string Product { get; set; }
        public long VariantID { get; set; }
        public string Variant { get; set; }
        public long TypeID { get; set; }
        public string Type { get; set; }
        public int TypeAmount { get; set; }
        public string Ean { get; set; }
        public decimal Vat { get; set; }
        public string Unit { get; set; }
        public decimal Multiply { get; set; }
        public decimal PriceNoVat { get; set; }
        public decimal Discount { get; set; }

        public string Name
        {
            get
            {
                string name = Product;
                if (!string.IsNullOrEmpty(Variant))
                    name += " " + Variant;
                if (TypeAmount != 1)
                    name += "(" + (TypeAmount).ToString() + ")";
                return name;
            }
        }

        public decimal Price
        {
            get
            {
                return PriceNoVat * (100m + Vat) / 100m;
            }
        }

    }
}
