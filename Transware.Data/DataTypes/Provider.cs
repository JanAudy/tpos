﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Provider : AbstractValidDto
    {
        public long? ProductID { get; set; }
        public long? VariantID { get; set; }
        public long OrganizationID { get; set; }
        public string CatalogNumber { get; set; }
        public string Name { get; set; }
        public long? CurrencyID { get; set; }
        public decimal? Price { get; set; }

        #region ForeignObjects

        internal Product product = null;
        internal event LazyLoadObjectLong<Product> LoadProduct = null;
        public Product Product
        {
            get
            {
                if (product == null && ProductID != 0)
                    if (LoadProduct != null && ProductID.HasValue)
                        LoadProduct(this, ProductID.Value, ref product);

                return product;
            }
            set { product = value; ProductID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductVariant variant = null;
        internal event LazyLoadObjectLong<ProductVariant> LoadVariant = null;
        public ProductVariant Variant
        {
            get
            {
                if (variant == null && VariantID != 0)
                    if (LoadVariant != null && VariantID.HasValue)
                        LoadVariant(this, VariantID.Value, ref variant);

                return variant;
            }
            set { variant = value; VariantID = (value != null) ? value.ID : (long?)null; }
        }

        internal Currency currency = null;
        internal event LazyLoadObjectLong<Currency> LoadCurrency = null;
        public Currency Currency
        {
            get
            {
                if (currency == null && CurrencyID != 0)
                    if (LoadCurrency != null && CurrencyID.HasValue)
                        LoadCurrency(this, CurrencyID.Value, ref currency);

                return currency;
            }
            set { currency = value; CurrencyID = (value != null) ? value.ID : (long?)null; }
        }

        

        #endregion
    }
}
