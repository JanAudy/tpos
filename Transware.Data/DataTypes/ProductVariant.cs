﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductVariant : AbstractValidDto
    {
        #region Fields
        
        public long? ProductID { get; set; }
        public string OrderMark { get; set; }
        public string Specification { get; set; }
        public long? ProviderID { get; set; }
        public long? SettingID { get; set; }
        public virtual string UserID1 { get; set; }
        public virtual string UserID2 { get; set; }
        public virtual string DetailedSpecification { get; set; }
        
        #endregion

        #region ForeignObjects

        internal Product product = null;
        internal event LazyLoadObjectLong<Product> LoadProduct = null;
        public Product Product
        {
            get
            {
                if (product == null && ProductID != 0)
                    if (LoadProduct != null && ProductID.HasValue)
                        LoadProduct(this, ProductID.Value, ref product);

                return product;
            }
            set { product = value; ProductID = (value != null) ? value.ID : (long?)null; }
        }
        
        internal Provider provider = null;
        internal event LazyLoadObjectLong<Provider> LoadProvider = null;
        public Provider Provider
        {
            get
            {
                if (provider == null && ProviderID != 0)
                    if (LoadProvider != null && ProviderID.HasValue)
                        LoadProvider(this, ProviderID.Value, ref provider);

                return provider;
            }
            set { provider = value; ProviderID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductSetting productSetting = null;
        internal event LazyLoadObjectLong<ProductSetting> LoadProductSetting = null;
        public ProductSetting Setting
        {
            get
            {
                if (productSetting == null && SettingID != 0)
                    if (LoadProductSetting != null && SettingID.HasValue)
                        LoadProductSetting(this, SettingID.Value, ref productSetting);

                return productSetting;
            }
            set { productSetting = value; SettingID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region Collections

        internal List<Provider> providers = null;
        internal event LazyLoadListLong<Provider> LoadProviders = null;
        public List<Provider> Providers
        {
            get
            {
                if (providers == null)
                    if (LoadProviders != null)
                        LoadProviders(this, ID, ref providers);

                return providers;
            }
            internal set { providers = value; }
        }

        internal List<Package> packages = null;
        internal event LazyLoadListLong<Package> LoadPackages = null;
        public List<Package> Packages
        {
            get
            {
                if (packages == null)
                    if (LoadPackages != null)
                        LoadPackages(this, ID, ref packages);

                return packages;
            }
            internal set { packages = value; }
        }
        
        internal List<ProductImage> images = null;
        internal event LazyLoadListLong<ProductImage> LoadImages = null;
        public List<ProductImage> Images
        {
            get
            {
                if (images == null)
                    if (LoadImages != null)
                        LoadImages(this, ID, ref images);

                return images;
            }
            internal set { images = value; }
        }

        internal List<ProductMovement> movements = null;
        internal event LazyLoadListLong<ProductMovement> LoadMovements = null;
        public List<ProductMovement> Movements
        {
            get
            {
                if (movements == null)
                    if (LoadMovements != null)
                        LoadMovements(this, ID, ref movements);

                return movements;
            }
            internal set { movements = value; }
        }

        internal Dictionary<long, StoreProduct> storeProducts = null;
        internal event LazyLoadDictionary<long, StoreProduct> LoadStoreProducts = null;
        public Dictionary<long, StoreProduct> StoreProducts
        {
            get
            {
                if (storeProducts == null)
                    if (LoadStoreProducts != null)
                        LoadStoreProducts(this, ID, ref storeProducts);

                return storeProducts;
            }
            internal set { storeProducts = value; }
        }

        internal HashSet<ProductAttribute> productAttributes = null;
        internal event LazyLoadHashSet<ProductAttribute> LoadProductAttributes = null;
        public HashSet<ProductAttribute> ProductAttributes
        {
            get
            {
                if (productAttributes == null)
                    if (LoadProductAttributes != null)
                        LoadProductAttributes(this, ID, ref productAttributes);

                return productAttributes;
            }
            internal set { productAttributes = value; }
        }

        #endregion


        #region Override

        public override void AssignIDToChildren()
        {
            if (providers != null)
            {
                foreach (Provider o in providers)
                    o.VariantID = ID;
            }

            if (packages != null)
            {
                foreach (Package o in packages)
                    o.VariantID = ID;
            }

            if (images != null)
            {
                foreach (ProductImage o in images)
                    o.Variant = ID;
            }

            if (movements != null)
            {
                foreach (ProductMovement o in movements)
                    o.VariantID= ID;
            }

            if (storeProducts != null)
            {
                foreach (StoreProduct o in storeProducts.Values)
                    o.VariantID = ID;
            }
        }

        #endregion

        #region Methods

        public Provider GetProvider(long organizationID)
        {
            foreach (Provider p in Providers)
            {
                if (p.OrganizationID == organizationID)
                    return p;
            }
            return null;
        }

        #endregion
    }
}
