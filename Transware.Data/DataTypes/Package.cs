﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Package : AbstractValidDto
    {
        public long? ProductID { get; set; }
        public long? VariantID { get; set; }
        public long? TypeID { get; set; }
        public string Ean { get; set; }
        public decimal? Weight { get; set; }
        public long? LabelID { get; set; }
        public bool Dividable { get; set; }

        #region ForeignObjects

        internal Product product = null;
        internal event LazyLoadObjectLong<Product> LoadProduct = null;
        public Product Product
        {
            get
            {
                if (product == null && ProductID != 0)
                    if (LoadProduct != null && ProductID.HasValue)
                        LoadProduct(this, ProductID.Value, ref product);

                return product;
            }
            set { product = value; ProductID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductVariant variant = null;
        internal event LazyLoadObjectLong<ProductVariant> LoadVariant = null;
        public ProductVariant Variant
        {
            get
            {
                if (variant == null && VariantID != 0)
                    if (LoadVariant != null && VariantID.HasValue)
                        LoadVariant(this, VariantID.Value, ref variant);

                return variant;
            }
            set { variant = value; VariantID = (value != null) ? value.ID : (long?)null; }
        }

        internal PackageType packageType = null;
        internal event LazyLoadObjectLong<PackageType> LoadPackageType = null;
        public PackageType Type
        {
            get
            {
                if (packageType == null && TypeID != 0)
                    if (LoadPackageType != null && TypeID.HasValue)
                        LoadPackageType(this, TypeID.Value, ref packageType);

                return packageType;
            }
            set { packageType = value; TypeID = (value != null) ? value.ID : (long?)null; }
        }

        internal PackageLabel packageLabel = null;
        internal event LazyLoadObjectLong<PackageLabel> LoadPackageLabel = null;
        public PackageLabel Label
        {
            get
            {
                if (packageLabel == null && LabelID != 0)
                    if (LoadPackageLabel != null && LabelID.HasValue)
                        LoadPackageLabel(this, LabelID.Value, ref packageLabel);

                return packageLabel;
            }
            set { packageLabel = value; LabelID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion
        
        #region Collections

        internal List<Cover> covers = null;
        internal event LazyLoadListLong<Cover> LoadCovers = null;
        public List<Cover> Covers
        {
            get
            {
                if (covers == null)
                    if (LoadCovers != null)
                        LoadCovers(this, ID, ref covers);

                return covers;
            }
            internal set { covers = value; }
        }

        internal List<Prescription> prescriptions = null;
        internal event LazyLoadListLong<Prescription> LoadPrescriptions = null;
        public List<Prescription> Prescriptions
        {
            get
            {
                if (prescriptions == null)
                    if (LoadPrescriptions != null)
                        LoadPrescriptions(this, ID, ref prescriptions);

                return prescriptions;
            }
            internal set { prescriptions = value; }
        }

        internal List<TandemProduct> tandemProducts = null;
        internal event LazyLoadListLong<TandemProduct> LoadTandemProducts = null;
        public List<TandemProduct> TandemProducts
        {
            get
            {
                if (tandemProducts == null)
                    if (LoadTandemProducts != null)
                        LoadTandemProducts(this, ID, ref tandemProducts);

                return tandemProducts;
            }
            internal set { tandemProducts = value; }
        }

        internal Dictionary<long, StorePackage> storePackages = null;
        internal event LazyLoadDictionary<long, StorePackage> LoadStorePackages = null;
        public Dictionary<long, StorePackage> StorePackages
        {
            get
            {
                if (storePackages == null)
                    if (LoadStorePackages != null)
                        LoadStorePackages(this, ID, ref storePackages);

                return storePackages;
            }
            internal set { storePackages = value; }
        }

        public override void AssignIDToChildren()
        {
            if (covers != null)
                foreach (Cover p in covers)
                {
                    p.PackageID = ID;
                }
            if (prescriptions != null)
                foreach (Prescription p in prescriptions)
                {
                    p.PackageID = ID;
                }
            if (tandemProducts != null)
                foreach (TandemProduct o in tandemProducts)
                {
                    o.PackageID = ID;
                }
            if (storePackages != null)
                foreach (StorePackage o in storePackages.Values)
                {
                    o.PackageID = ID;
                }
        }

        #endregion

        #region Constructor

        public Package()
        {
            
        }

        public Package(Product product)
            : this()
        {
            Product = product;
        }

        public Package(ProductVariant variant)
            : this()
        {
            Variant = variant;
        }

        #endregion

        #region Shortcuts Properties

        public string Name
        {
            get
            {
                string productName = "";
                if (Product!=null)
                    productName = Product.Name;
                string variantName = "";
                if (VariantID.HasValue)
                    variantName = Variant.OrderMark;
                string amount = "";
                if (Amount > 1)
                    amount = Type.Name;

                string res = productName;
                if (!string.IsNullOrEmpty(variantName))
                    res += " - " + variantName;

                if (!string.IsNullOrEmpty(amount))
                    res += " (" + amount + ")";
                return res;
            }
        }
        public Unit Unit { get { return Product.Unit; } }
        public Vat Vat { get { return Product.Vat; } }
        public decimal Amount { get { return (Type != null) ? Type.Amount : 0; } }

        #endregion

        #region Shortcut Methods

        public virtual decimal PriceNoVat(Store store)
        {
            if (StorePackages!=null && StorePackages.ContainsKey(store.ID))
                return StorePackages[store.ID].PriceNoVat;
            else
                return 0;
        }

        public virtual decimal Price(Store store)
        {
            if (Vat != null)
                return Vat.GetPrice(PriceNoVat(store));
            return PriceNoVat(store);
        }

        public virtual decimal UnitPriceNoVat(Store store)
        {
            return PriceNoVat(store) / Amount;
        }

        public virtual decimal UnitPrice(Store store)
        {
            if (Vat != null)
                return Price(store) / Amount;
            return UnitPriceNoVat(store);
        }

        public virtual IndividualDiscount IndividualDiscount(Store store)
        {
            if (StorePackages != null && StorePackages.ContainsKey(store.ID))
                return StorePackages[store.ID].IndividualDiscount;
            else
                return null;
        }

        public virtual decimal UnitPriceNoVatWithDiscount(Store store)
        {
            IndividualDiscount d = IndividualDiscount(store);
            if (d != null)
                return d.GetPrice(UnitPriceNoVat(store));
            else
                return UnitPriceNoVat(store);
        }

        public virtual decimal UnitPriceWithDiscount(Store store)
        {
            IndividualDiscount d = IndividualDiscount(store);
            if (d != null)
                return d.GetPrice(UnitPrice(store));
            else
                return UnitPrice(store);
        }

        public virtual decimal PriceNoVatWithDiscount(Store store)
        {
            IndividualDiscount d = IndividualDiscount(store);
            if (d != null)
                return d.GetPrice(PriceNoVat(store));
            else
                return PriceNoVat(store);
        }

        public virtual decimal PriceWithDiscount(Store store)
        {
            IndividualDiscount d = IndividualDiscount(store);
            if (d != null)
                return d.GetPrice(Price(store));
            else
                return Price(store);
        }


        #endregion


        #region Additional Methods

        public virtual Package Clone(Product pc)
        {
            Package p = new Package(pc);
            p.Type = Type;
            p.Ean = Ean;
            p.Weight = Weight;
            foreach (Cover c in Covers)
            {
                if (!c.Valid) continue;
                p.Covers.Add(c.Clone(p));
            }
            foreach (TandemProduct t in TandemProducts)
            {
                if (!t.Valid) continue;
                p.TandemProducts.Add(t.Clone(p));
            }
            return p;
        }


        #endregion
    }
}
