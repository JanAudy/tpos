﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Store : AbstractValidDto
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string City { get; set; }
        public string ZIP { get; set; }
        public long Country { get; set; }
        public long WarrantyID { get; set; }

        private Store warranty = null;
        public event LazyLoadObjectLong<Store> LoadWarranty;
        public Store Warranty
        {
            get
            {
                if (warranty == null)
                    if (LoadWarranty != null)
                        LoadWarranty(this, WarrantyID, ref warranty);

                return warranty;
            }
            set { warranty = value; WarrantyID = value.ID; }
        }
        
    }
}
