﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class PriceList : AbstractValidDto
    {
        public string Name { get; set; }
        public decimal Coefficient { get; set; }
        public decimal Addition { get; set; }
        public bool UseGroups { get; set; }
        public ERoundingPrices RoundingPrices { get; set; }
        public ERoundingType Rounding { get; set; }
        public int Places { get; set; }


        #region Collections
        
        internal Dictionary<long, PriceListProductGroup> groups = null;
        internal event LazyLoadDictionary<long, PriceListProductGroup> LoadGroups = null;
        public Dictionary<long, PriceListProductGroup> Groups
        {
            get
            {
                if (groups == null)
                    if (LoadGroups != null)
                        LoadGroups(this, ID, ref groups);

                return groups;
            }
            internal set { groups = value; }
        }
        
        #endregion


        #region Methods

        public virtual decimal GetPrice(decimal price)
        {
            return price * Coefficient + Addition;
        }

        public virtual decimal GetPriceNoVat(ProductGroup group, decimal vat, decimal priceNoVat)
        {
            decimal actualPrice = GetPrice(priceNoVat);

            if (group != null && Groups != null && Groups.Count != 0 && Groups.ContainsKey(group.ID))
                actualPrice = Groups[group.ID].GetPrice(actualPrice);

            if (RoundingPrices == ERoundingPrices.NoVat)
            {
                actualPrice += Currency.GetRounding(Rounding, Places, actualPrice);
            }
            else if (RoundingPrices == ERoundingPrices.Regular)
            {
                actualPrice = Vat.GetPrice(vat, actualPrice);
                actualPrice += Currency.GetRounding(Rounding, Places, actualPrice);
                actualPrice = Vat.GetPriceNoVat(vat, actualPrice);
            }

            return actualPrice;
        }

        public virtual decimal GetCoefficient(ProductGroup pg)
        {
            if (pg == null || Groups == null || Groups.Count == 0 || !Groups.ContainsKey(pg.ID))
                return Coefficient;
            else
                return Groups[pg.ID].Coefficient;
        }

        #endregion

        public override void AssignIDToChildren()
        {
            foreach (PriceListProductGroup pg in groups.Values)
                pg.PriceList = ID;
        }

    }

}
