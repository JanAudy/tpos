﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class OrderType : AbstractValidDto
    {
        public string Name { get; set; }
        public string Description { get; set; }

        internal Dictionary<ELanguage, OrderTypeLang> languages;
        internal event LazyLoadDictionary<ELanguage, OrderTypeLang> LoadLanguages;
        public Dictionary<ELanguage, OrderTypeLang> Languages
        {
            get
            {
                if (languages == null)
                    if (LoadLanguages != null)
                        LoadLanguages(this, this.ID, ref languages);
                    else
                        languages = new Dictionary<ELanguage, OrderTypeLang>();

                return languages;
            }
            set { languages = value; }
        }

        public override void AssignIDToChildren()
        {
            if (languages != null && languages.Count > 0)
                foreach (OrderTypeLang ul in languages.Values)
                    ul.OrderTypeID = ID;
        }
    }
}
