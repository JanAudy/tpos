﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PackageType : AbstractValidDto
    {
        public string Name { get; set; }
        public int Amount { get; set; }

        public override string ToString()
        {
            return Name + string.Format("({0:0.#####})", Amount);
        }
    }
}
