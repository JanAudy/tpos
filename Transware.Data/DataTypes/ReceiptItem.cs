﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ReceiptItem : AbstractValidDto
    {
        public long Record { get; set; }
        public long Package { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        public decimal AddedPrice { get; set; }
        public string OrderMark { get; set; }
    }
}
