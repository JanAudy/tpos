﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Inventory : AbstractValidDto
    {
        public long Store { get; set; }
        public bool Complete { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public int Status { get; set; }
        public int Flags { get; set; }
        public long Clerk { get; set; }
        public string Note { get; set; }
    }
}
