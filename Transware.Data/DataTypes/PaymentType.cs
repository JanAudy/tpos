﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class PaymentType : AbstractValidDto
    {
        public string Name { get; set; }
        public bool Rounding { get; set; }
        public int NoOfBills { get; set; }
        public EUsedExchangeRate Rate { get; set; }
        public bool IsCash { get; set; }
        public decimal Fee { get; set; }
    }
}
