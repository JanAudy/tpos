﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductSetting : AbstractIDLongDto
    {
        public bool Actual { get; set; }
        public bool Tandem { get; set; }
        public bool Catalog { get; set; }
        public bool UndefinedAmount { get; set; }
        public bool IndividualPrice { get; set; }
        public bool Internet { get; set; }
        public bool OverHead { get; set; }
        public bool Commissional { get; set; }
        public bool ShowPricesOnWeb { get; set; }
        public bool ShowOnTitlePage { get; set; }
        public bool Voucher { get; set; }
        public bool VariableLength { get; set; }
        public bool Action { get; set; }
        public bool Prescription { get; set; }

        public virtual ProductSetting Clone()
        {
            ProductSetting ps = (ProductSetting)this.MemberwiseClone();
            ps.ID = 0;
            return ps;
        }

    }
}
