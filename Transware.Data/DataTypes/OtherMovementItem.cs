﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class OtherMovementItem : AbstractValidDto
    {
        public long Record { get; set; }
        public long Package { get; set; }
        public decimal Amount { get; set; }
        public decimal BuyPriceNoVat { get; set; }
        public decimal PriceNoVat { get; set; }
        public decimal Vat { get; set; }
    }
}
