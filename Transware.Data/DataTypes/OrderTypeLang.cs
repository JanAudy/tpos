﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.DataTypes
{
    public class OrderTypeLang : TranslatedText
    {
        public long OrderTypeID { get; set; }
    }
}
