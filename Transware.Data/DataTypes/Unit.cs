﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class Unit : AbstractValidDto
    {
        public string Name { get; set; }
        public string Shortcut { get; set; }
        public decimal Multiply { get; set; }

        internal Dictionary<ELanguage, UnitLang> languages;
        internal event LazyLoadDictionary<ELanguage, UnitLang> LoadLanguages;
        public Dictionary<ELanguage, UnitLang> Languages
        {
            get
            {
                if (languages == null)
                    if (LoadLanguages != null)
                        LoadLanguages(this, this.ID, ref languages);
                    else
                        languages = new Dictionary<ELanguage, UnitLang>();

                return languages;
            }
            set { languages = value; }
        }

        public override void AssignIDToChildren()
        {
            if (languages != null && languages.Count > 0)
                foreach (UnitLang ul in languages.Values)
                    ul.UnitID = ID;
        }

        public override string ToString()
        {
            return string.Format("{0:0.#####} {1}", Multiply, Shortcut);
        }
    }
}
