﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductSummary : AbstractIDLongDto
    {        
        public string Name { get; set; }
        public long Category { get; set; }
        public int PlaceNo { get; set; }
        public decimal Tax { get; set; }
        public bool Actual { get; set; }
        public bool Internet { get; set; }
        public bool IndividualPrice { get; set; }
        public bool UndefinedAmount { get; set; }
        public int Amount { get; set; }
        public decimal PriceNoVat { get; set; }
        public string UserID1 { get; set; }
        public string UserID2 { get; set; }

        public decimal? RequestedMin { get; set; }
        public decimal? RequestedMax { get; set; }
        public decimal RealNoReserved { get; set; }
        public decimal RealReserved { get; set; }
        public decimal RealTotal { get; set; }
        public decimal RealLocked { get; set; }
        
        public string OrderMark { get; set; }

        public Dictionary<int, decimal> Prices = new Dictionary<int, decimal>();

        public List<ProductVariantSummary> Variants = new List<ProductVariantSummary>();


        public void SetID(long id)
        {
            ID = id;
        }


        public class NameComparer : IComparer<ProductSummary>
        {

            #region IComparer<ProductSummary> Members

            public int Compare(ProductSummary x, ProductSummary y)
            {
                return x.Name.CompareTo(y.Name);
            }

            #endregion
        }

        public class ProductVariantSummary
        {
            public long PID;
            public long ID;

            public decimal Tax;

            public string Name { get { return OrderMark; } }

            public string OrderMark { get; set; }

            public bool Actual { get; set; }
            public bool Internet { get; set; }
            public bool IndividualPrice { get; set; }
            public bool UndefinedAmount { get; set; }
            public int Amount { get; set; }
            public decimal PriceNoVat { get; set; }
            public string UserID1 { get; set; }
            public string UserID2 { get; set; }

            public decimal? RequestedMin { get; set; }
            public decimal? RequestedMax { get; set; }
            public decimal RealNoReserved { get; set; }
            public decimal RealReserved { get; set; }
            public decimal RealTotal { get; set; }
            public decimal RealLocked { get; set; }            

            public Dictionary<int, decimal> Prices = new Dictionary<int, decimal>();

            public override bool Equals(object obj)
            {
                if (this == obj)
                    return true;

                if (obj is ProductVariantSummary)
                {
                    ProductVariantSummary g = (ProductVariantSummary)obj;
                    return ID == g.ID;
                }
                return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return (GetType().ToString() + ID.ToString()).GetHashCode();
            }
        }
    }
}
