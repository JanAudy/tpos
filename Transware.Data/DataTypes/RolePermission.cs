﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class RolePermission : AbstractValidDto
    {
        public long Role { get; set; }
        public long Permission { get; set; }
        
    }
}
