﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.DataTypes
{
    public class ProductNameLang : TranslatedText
    {
        public long ProductID { get; set; }
    }
}
