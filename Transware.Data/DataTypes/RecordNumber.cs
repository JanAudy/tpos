﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class RecordNumber : AbstractIDLongDto
    {
        public long Store { get; set; }
        public int Year { get; set; }
        public int Type { get; set; }
        public string Mask { get; set; }
        public long First { get; set; }
        public long Next { get; set; }


        public virtual string FormateByPattern(long id)
        {
            string res = "";

            string mask = Mask;
            string number = id.ToString();
            int j = number.Length - 1;
            for (int i = mask.Length - 1; i >= 0; i--)
            {
                if (mask[i] == 'n')
                {
                    if (j < 0) res = '0' + res;
                    else
                    {
                        res = number[j--] + res;
                    }
                }
                else
                {
                    res = mask[i] + res;
                }
            }
            if (res.EndsWith("c") && res.Length == 13)
            {
                int sum = 0;
                sum += res[1] - '0';
                sum += res[3] - '0';
                sum += res[5] - '0';
                sum += res[7] - '0';
                sum += res[9] - '0';
                sum += res[11] - '0';
                sum *= 3;
                sum += res[0] - '0';
                sum += res[2] - '0';
                sum += res[4] - '0';
                sum += res[6] - '0';
                sum += res[8] - '0';
                sum += res[10] - '0';
                if ((sum % 10) == 0)
                    sum = 0;
                else
                    sum = ((sum / 10) + 1) * 10 - sum;
                res = res.Substring(0, 12) + (char)(sum + '0');
            }
            return res;
        }
        
    }
}
