﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class Exchange : AbstractValidDto
    {
        public long? CurrencyID { get; set; }
        public long? MainCurrencyID { get; set; }
        public int Unit { get; set; }
        public decimal DevizyBuy { get; set; }
        public decimal DevizySell { get; set; }
        public decimal DevizyMid { get; set; }
        public decimal ValutyBuy { get; set; }
        public decimal ValutySell { get; set; }
        public decimal ValutyMid { get; set; }
        public decimal CNBMid { get; set; }
        public DateTime DateTime { get; set; }

        #region Foreign Objects

        internal Currency currency = null;
        internal event LazyLoadObjectLong<Currency> LoadCurrency = null;
        public Currency Currency
        {
            get
            {
                if (currency == null && CurrencyID != 0)
                    if (LoadCurrency != null && CurrencyID.HasValue)
                        LoadCurrency(this, CurrencyID.Value, ref currency);

                return currency;
            }
            set { currency = value; CurrencyID = (value != null) ? value.ID : (long?)null; }
        }

        internal Currency mainCurrency = null;
        internal event LazyLoadObjectLong<Currency> LoadMainCurrency = null;
        public Currency MainCurrency
        {
            get
            {
                if (mainCurrency == null && MainCurrencyID != 0)
                    if (LoadMainCurrency != null && MainCurrencyID.HasValue)
                        LoadMainCurrency(this, MainCurrencyID.Value, ref mainCurrency);

                return mainCurrency;
            }
            set { mainCurrency = value; MainCurrencyID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region Methods

        public virtual decimal? ConvertToMain(decimal price, EUsedExchangeRate usedRate)
        {
            decimal? rate = null;

            switch (usedRate)
            {
                case EUsedExchangeRate.DevizyBuy: rate = DevizyBuy; break;
                case EUsedExchangeRate.DevizySell: rate = DevizySell; break;
                case EUsedExchangeRate.DevizyMid: rate = DevizyMid; break;
                case EUsedExchangeRate.ValutyBuy: rate = ValutyBuy; break;
                case EUsedExchangeRate.ValutySell: rate = ValutySell; break;
                case EUsedExchangeRate.ValutyMid: rate = ValutyMid; break;
            }
            if (rate.HasValue)
            {
                return Exchange.ConvertToMain(price, Unit, rate);
            }
            else
            {
                return null;
            }
        }

        public virtual decimal? ConvertFromMain(decimal price, EUsedExchangeRate usedRate)
        {
            decimal? rate = null;

            switch (usedRate)
            {
                case EUsedExchangeRate.DevizyBuy: rate = DevizyBuy; break;
                case EUsedExchangeRate.DevizySell: rate = DevizySell; break;
                case EUsedExchangeRate.DevizyMid: rate = DevizyMid; break;
                case EUsedExchangeRate.ValutyBuy: rate = ValutyBuy; break;
                case EUsedExchangeRate.ValutySell: rate = ValutySell; break;
                case EUsedExchangeRate.ValutyMid: rate = ValutyMid; break;
            }
            if (rate.HasValue)
            {
                return Exchange.ConvertFromMain(price, Unit, rate);
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Static methods

        public static Exchange Identity(Currency c)
        {
            Exchange e = new Exchange();
            e.Currency = c;
            e.DateTime = DateTime.Now;
            e.DevizyBuy = e.DevizySell = e.DevizyMid = e.ValutyBuy = e.ValutySell = e.ValutyMid = e.CNBMid = 1;
            e.Unit = 1;
            return e;
        }

        public static decimal? ConvertToMain(decimal price, decimal unit, decimal? rate)
        {
            if (rate.HasValue)
            {
                return ConvertToMain(price, unit, rate.Value);
            }
            else
            {
                return null;
            }
        }

        public static decimal? ConvertFromMain(decimal price, decimal unit, decimal? rate)
        {
            if (rate.HasValue)
            {
                return ConvertFromMain(price, unit, rate.Value);
            }
            else
            {
                return null;
            }
        }

        public static decimal ConvertToMain(decimal price, decimal unit, decimal rate)
        {
            return price * (rate / unit);
        }

        public static decimal ConvertFromMain(decimal price, decimal unit, decimal rate)
        {
            return price * (unit / rate);
        }

        #endregion
    }
}
