﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class ProductMovement : AbstractValidDto
    {
        public long? ProductID { get; set; }
        public long? VariantID { get; set; }
        public long? StoreID { get; set; }
        public DateTime DateTime { get; set; }
        public ERecordType DocumentType { get; set; }
        public EMovementType MovementType { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public long DocumentID { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal SellPrice { get; set; }
        public decimal Amount { get; set; }
        public decimal InStore { get; set; }

        #region ForeignObjects

        internal Product product = null;
        internal event LazyLoadObjectLong<Product> LoadProduct = null;
        public Product Product
        {
            get
            {
                if (product == null && ProductID != 0)
                    if (LoadProduct != null && ProductID.HasValue)
                        LoadProduct(this, ProductID.Value, ref product);

                return product;
            }
            set { product = value; ProductID = (value != null) ? value.ID : (long?)null; }
        }

        internal ProductVariant variant = null;
        internal event LazyLoadObjectLong<ProductVariant> LoadVariant = null;
        public ProductVariant Variant
        {
            get
            {
                if (variant == null && VariantID != 0)
                    if (LoadVariant != null && VariantID.HasValue)
                        LoadVariant(this, VariantID.Value, ref variant);

                return variant;
            }
            set { variant = value; VariantID = (value != null) ? value.ID : (long?)null; }
        }

        internal Store store = null;
        internal event LazyLoadObjectLong<Store> LoadStore = null;
        public Store Store
        {
            get
            {
                if (store == null && StoreID != 0)
                    if (LoadStore != null && StoreID.HasValue)
                        LoadStore(this, StoreID.Value, ref store);

                return store;
            }
            set { store = value; StoreID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion
    }
}
