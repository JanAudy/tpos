﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Contract : AbstractValidDto
    {
        public long Store { get; set; }
        public string Name { get; set; }        
        
    }
}
