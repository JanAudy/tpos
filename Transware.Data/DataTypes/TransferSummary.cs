﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class TransferSummary : AbstractValidDto, IComparable<TransferSummary>
    {
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public ERecordStatus Status { get; set; }
        public ERecordFlags Flags { get; set; }
        public long Source { get; set; }
        public string SourceName { get; set; }
        public long Destination { get; set; }
        public string DestinationName { get; set; }
        public decimal PriceNoVat { get; set; }


        #region IComparable<TransferSummary> Members

        public int CompareTo(TransferSummary other)
        {
            if (Number == 0 && other.Number == 0)
                return -DateTime.CompareTo(other.DateTime);
            if (Number == 0 && other.Number != 0)
                return -1;
            if (Number != 0 && other.Number == 0)
                return +1;

            int res = DateTime.Date.Year.CompareTo(other.DateTime.Date.Year);
            if (res == 0)
                return -Number.CompareTo(other.Number);
            else
                return -DateTime.CompareTo(other.DateTime);
        }

        #endregion

        public string StatusString()
        {
            string s = "";
            switch (Status)
            {
                case ERecordStatus.Closed: s += "Uzavřená, "; break;
                case ERecordStatus.Created: s += "Vytvořená, "; break;
                case ERecordStatus.Unfinished: s += "Rozpracovaná, "; break;
                case ERecordStatus.Canceled: s += "Stornovaná, "; break;
                case ERecordStatus.Unlocked: s += "Uvolněná, "; break;
            }
            if (s.Length > 0)
                s = s.Substring(0, s.Length - 2);
            return s;
        }
    }
}
