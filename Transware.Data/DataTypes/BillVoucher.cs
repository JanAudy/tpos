﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class BillVoucher : AbstractValidDto
    {
        public long Bill { get; set; }
        public bool Voucher { get; set; }
        public decimal Amount { get; set; }
    }
}
