﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class FinalRecipient : AbstractIDLongDto
    {
        public long OrganizationID { get; set; }
        public string Person { get; set; }
        public string Department { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string City { get; set; }
        public string ZIP { get; set; }
        public long Country { get; set; }
        public bool Actual { get; set; }
    }
}
