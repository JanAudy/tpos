﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductAttribute : AbstractValidDto, IComparable<ProductAttribute>
    {
        public long? GroupID { get; set; }
        public string Value { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public int Position { get; set; }

        public override string ToString()
        {
            return Value;
        }

        #region ForeignObjects

        internal ProductAttributeGroup group = null;
        internal event LazyLoadObjectLong<ProductAttributeGroup> LoadGroup = null;
        public ProductAttributeGroup Group
        {
            get
            {
                if (group == null && GroupID != 0)
                    if (LoadGroup != null && GroupID.HasValue)
                        LoadGroup(this, GroupID.Value, ref group);

                return group;
            }
            set { group = value; GroupID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion

        #region 

        public static int CompareAlphabetically(ProductAttribute x, ProductAttribute y)
        {
            return x.Value.CompareTo(y.Value);
        }

        public static int CompareAsNumbers(ProductAttribute x, ProductAttribute y)
        {
            try
            {
                int xi = int.MinValue;
                int yi = int.MinValue;

                int.TryParse(x.Value, out xi);
                int.TryParse(y.Value, out yi);

                return xi.CompareTo(yi);
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        #region IComparable<ProductAttribute> Members

        public int CompareTo(ProductAttribute other)
        {

            int res = Position.CompareTo(other.Position);
            return res;
        }

        #endregion
    }
}
