﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PurchaseOrderItem : AbstractValidDto
    {
        public long Record { get; set; }
        public long Store { get; set; }
        public long Provider { get; set; }
        public decimal Amount { get; set; }
    }
}
