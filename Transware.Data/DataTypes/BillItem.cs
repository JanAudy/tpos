﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.DataModel;

namespace Transware.Data.DataTypes
{
    public class BillItem : AbstractValidDto
    {
        public long Record { get; set; }
        public bool IndividualItem { get; set; }
        public long? Package { get; set; }
        public string Name { get; set; }
        public long? IndividualDiscount { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public decimal Price { get; set; }
        public decimal UnitBuyPriceNoVat { get; set; }


        public decimal PriceNoVat
        {
            get
            {
                return Price * (100m - Vat) / 100m;
            }
        }

        public decimal TotalPriceNoVat
        {
            get
            {
                return Amount * PriceNoVat;
            }
        }


        public decimal TotalPrice
        {
            get
            {
                return Amount * Price;
            }
        }

        public decimal PriceWithDiscount
        {
            get
            {
                decimal price = Price;
                if (Discount!=0)
                {
                    price = Transware.Data.DataTypes.IndividualDiscount.GetPrice(price, Discount);
                }
                return price;
            }
        }

        public decimal TotalPriceWithDiscount
        {
            get
            {
                decimal price = TotalPrice;
                if (Discount != 0)
                {
                    price = Transware.Data.DataTypes.IndividualDiscount.GetPrice(price, Discount);
                }
                return price;
            }
        }

        public decimal TotalAmount
        {
            get
            {
                return Amount;
            }
        }

        public virtual decimal UnitPriceNoVatWithDiscount
        {
            get
            {
                decimal price = Price;
                if (Discount != 0)
                    price = Transware.Data.DataTypes.IndividualDiscount.GetPrice(price, Discount);
                
                //if (!IndividualItem)
                //    price /= Package.Amount;
                return price;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj is BillItem)
            {
                BillItem item = obj as BillItem;
                if (ID == 0 || item.ID == 0)
                {
                    if (IndividualItem && item.IndividualItem)
                    {
                        return IndividualItem == item.IndividualItem;
                    }
                    else
                        return Package.Equals(item.Package);
                }
                else
                    return ID.Equals(item.ID);
            }
            else
                return base.Equals(obj);
        }
    }
}
