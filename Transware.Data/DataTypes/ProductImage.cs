﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductImage : AbstractValidDto, IComparable<ProductImage>
    {
        public long Product { get; set; }
        public long Variant { get; set; }
        public byte[] Thumbnail { get; set; }
        public byte[] Image { get; set; }
        public int Position { get; set; }




        #region IComparable<ProductImage> Members

        public int CompareTo(ProductImage other)
        {
            return Position.CompareTo(other.Position);
        }

        #endregion
    }
}
