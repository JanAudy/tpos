﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class EmailText : AbstractValidDto
    {
        public ERecordType RecordType { get; set; }
        public string Text { get; set; }
    }
}
