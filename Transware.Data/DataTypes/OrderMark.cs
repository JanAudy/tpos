﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class OrderMark : AbstractValidDto
    {
        public string Name { get; set; }
        public int Position { get; set; }
        public int Length { get; set; }
        public EOrderMarkType Type { get; set; }
        public string Value { get; set; }

        
    }
}
