﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    public class ConfigClassField : AbstractIDLongDto
    {
        public long ConfigID { get; set; }
        public EConfigFields Name { get; set; }
        public string Value { get; set; }
        
    }
}
