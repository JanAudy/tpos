﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class DeliveryNoteItem : AbstractValidDto
    {
        public long Record { get; set; }
        public bool IndividualItem { get; set; }
        public long Package { get; set; }
        public string Name { get; set; }
        public long IndividualDiscount { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public decimal Price { get; set; }
        public bool Deletable { get; set; }
        public long Invoice { get; set; }
    }
}
