﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PurchaseWarrantyNote : AbstractValidDto
    {
        public long PurchaseWarranty { get; set; }
        public DateTime DateTime { get; set; }
        public string Text { get; set; }
    }
}
