﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class SellOrderItem : AbstractValidDto
    {
        public long SellOrder { get; set; }
        public bool IndividualItem { get; set; }
        public long Package { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
