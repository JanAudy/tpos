﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class SellWarrantyItemMark : AbstractValidDto
    {
        public long Item { get; set; }
        public long DifferentialMark { get; set; }
        public string Value { get; set; }

    }
}
