﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class PurchaseWarrantyItem : AbstractValidDto
    {
        public long PurchaseWarranty { get; set; }
        public long Package { get; set; }
        public decimal Amount { get; set; }
    }
}
