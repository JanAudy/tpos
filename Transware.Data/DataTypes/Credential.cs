﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Credential : AbstractValidDto
    {
        public long CoreID { get; set; }
        public bool Active { get; set; }
        public int Type { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        
    }
}
