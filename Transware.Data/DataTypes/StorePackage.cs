﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class StorePackage : AbstractIDLongDto
    {
        public long? StoreID { get; set; }
        public long? PackageID { get; set; }
        public decimal PriceNoVat { get; set; }
        public long? IndividualDiscountID { get; set; }


        #region ForeignObjects

        internal Store store = null;
        internal event LazyLoadObjectLong<Store> LoadStore = null;
        public Store Store
        {
            get
            {
                if (store == null && StoreID != 0)
                    if (LoadStore != null && StoreID.HasValue)
                        LoadStore(this, StoreID.Value, ref store);

                return store;
            }
            set { store = value; StoreID = (value != null) ? value.ID : (long?)null; }
        }

        internal Package package = null;
        internal event LazyLoadObjectLong<Package> LoadPackage = null;
        public Package Package
        {
            get
            {
                if (package == null && PackageID != 0)
                    if (LoadPackage != null && PackageID.HasValue)
                        LoadPackage(this, PackageID.Value, ref package);

                return package;
            }
            set { package = value; PackageID = (value != null) ? value.ID : (long?)null; }
        }

        internal IndividualDiscount individualDiscount = null;
        internal event LazyLoadObjectLong<IndividualDiscount> LoadIndividualDiscount = null;
        public IndividualDiscount IndividualDiscount
        {
            get
            {
                if (individualDiscount == null && IndividualDiscountID != 0)
                    if (LoadIndividualDiscount != null && IndividualDiscountID.HasValue)
                        LoadIndividualDiscount(this, IndividualDiscountID.Value, ref individualDiscount);

                return individualDiscount;
            }
            set { individualDiscount = value; IndividualDiscountID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion
    }
}
