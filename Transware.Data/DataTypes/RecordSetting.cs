﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class RecordSetting : AbstractIDLongDto
    {
        public int Type { get; set; }
        
    }
}
