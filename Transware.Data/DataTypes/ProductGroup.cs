﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductGroup : AbstractValidDto, IComparable<ProductGroup>
    {
        public string Name { get; set; }
        
        public override string ToString()
        {
            return Name;
        }

        #region IComparable<ProductGroup> Members

        public int CompareTo(ProductGroup other)
        {
            return Name.CompareTo(other.Name);
        }

        #endregion
    }
}
