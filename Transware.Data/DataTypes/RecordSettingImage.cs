﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class RecordSettingImage : AbstractIDLongDto
    {
        public int Type { get; set; }
        public string Name { get; set; }
        public byte[] Value { get; set; }
        
    }
}
