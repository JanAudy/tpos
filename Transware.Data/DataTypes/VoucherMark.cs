﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class VoucherMark : AbstractValidDto
    {
        public long VoucherType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}
