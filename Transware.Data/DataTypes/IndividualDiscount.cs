﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class IndividualDiscount : AbstractValidDto, IComparable<IndividualDiscount>
    {
        public string Name { get; set; }
        public decimal Discount { get; set; }


        public virtual decimal GetPrice(decimal price)
        {
            return price * (100 - Discount) / 100.0m;
        }

        public static decimal GetPrice(decimal price, decimal discount)
        {
            return price * (100 - discount) / 100.0m;
        }

        public override string ToString()
        {
            return Name + string.Format("({0:0.#####} %)", Discount);
        }
        
        #region IComparable<IndividualDiscount> Members

        public int CompareTo(IndividualDiscount other)
        {
            if ((Discount == decimal.Round(Discount) && other.Discount == decimal.Round(other.Discount)) || (Discount != decimal.Round(Discount) && other.Discount != decimal.Round(other.Discount)))
            {
                return Discount.CompareTo(other.Discount);
            }
            else
                if (Discount == decimal.Round(Discount) && other.Discount != decimal.Round(other.Discount))
                    return -1;
                else
                    return 1;
        }

        #endregion
    }
}
