﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class InRole : AbstractIDLongDto
    {
        public long RoleID { get; set; }
        public long UserID { get; set; }       
    }
}
