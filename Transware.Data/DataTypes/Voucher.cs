﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Voucher : AbstractValidDto
    {
        public long VoucherType { get; set; }
        public string Ean { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        
    }
}
