﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Log : AbstractIDLongDto
    {
        public DateTime Date { get; set; }
        public string Host { get; set; }
        public string Level { get; set; }
        public string LoggedUser { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }      
    }
}
