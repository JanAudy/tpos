﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class Prescription : AbstractValidDto
    {
        public long? PackageID { get; set; }
        public long? IngredientID { get; set; }
        public decimal Amount { get; set; }

        #region ForeignObjects

        internal Package package = null;
        internal event LazyLoadObjectLong<Package> LoadPackage = null;
        public Package Package
        {
            get
            {
                if (package == null && PackageID != 0)
                    if (LoadPackage != null && PackageID.HasValue)
                        LoadPackage(this, PackageID.Value, ref package);

                return package;
            }
            set { package = value; PackageID = (value != null) ? value.ID : (long?)null; }
        }

        internal Package ingredient = null;
        internal event LazyLoadObjectLong<Package> LoadIngredient = null;
        public Package Ingredient
        {
            get
            {
                if (ingredient == null && IngredientID != 0)
                    if (LoadIngredient != null && IngredientID.HasValue)
                        LoadIngredient(this, IngredientID.Value, ref ingredient);

                return package;
            }
            set { ingredient = value; IngredientID = (value != null) ? value.ID : (long?)null; }
        }

        #endregion


        #region Collections

        internal List<Cover> covers = null;
        internal event LazyLoadListLong<Cover> LoadCovers = null;
        public List<Cover> Covers
        {
            get
            {
                if (covers == null)
                    if (LoadCovers != null)
                        LoadCovers(this, ID, ref covers);

                return covers;
            }
            internal set { covers = value; }
        }

        internal List<Prescription> prescriptions = null;
        internal event LazyLoadListLong<Prescription> LoadPrescriptions = null;
        public List<Prescription> Prescriptions
        {
            get
            {
                if (prescriptions == null)
                    if (LoadPrescriptions != null)
                        LoadPrescriptions(this, ID, ref prescriptions);

                return prescriptions;
            }
            internal set { prescriptions = value; }
        }

        internal List<TandemProduct> tandemProducts = null;
        internal event LazyLoadListLong<TandemProduct> LoadTandemProducts = null;
        public List<TandemProduct> TandemProducts
        {
            get
            {
                if (tandemProducts == null)
                    if (LoadTandemProducts != null)
                        LoadTandemProducts(this, ID, ref tandemProducts);

                return tandemProducts;
            }
            internal set { tandemProducts = value; }
        }

        internal Dictionary<long, StorePackage> storePackages = null;
        internal event LazyLoadDictionary<long, StorePackage> LoadStorePackages = null;
        public Dictionary<long, StorePackage> StorePackages
        {
            get
            {
                if (storePackages == null)
                    if (LoadStorePackages != null)
                        LoadStorePackages(this, ID, ref storePackages);

                return storePackages;
            }
            internal set { storePackages = value; }
        }

        #endregion


        public Prescription()
        {
        }

        public Prescription(Package p)
            : this()
        {
            Package = p;
        }
    }
}
