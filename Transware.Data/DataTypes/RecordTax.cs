﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Data.DataTypes
{
    public class RecordTax
    {
        public decimal Percent = 0;
        public decimal PriceNoVat = 0;
        public decimal Price = 0;
        public decimal Tax { get { return Price - PriceNoVat; } }
    }
}
