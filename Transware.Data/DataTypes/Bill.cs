﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;
using Transware.Data.DataModel;

namespace Transware.Data.DataTypes
{
    public class Bill : AbstractValidDto
    {
        public long Store { get; set; }
        public long SupplierID { get; set; }
        public string SupplierName { get; set; }
        public long SupplierAddressID { get; set; }
        public string SupplierAddress { get; set; }
        public string AssociatedCard { get; set; }
        public long Number { get; set; }
        public string NumberStr { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime CloseDate { get; set; }
        public long CloseUser { get; set; }
        public ERecordStatus Status { get; set; }
        public ERecordFlags Flags { get; set; }
        public decimal Rounding { get; set; }
        public long? IndividualDiscount { get; set; }
        public decimal Discount { get; set; }
        public long? PaymentType { get; set; }
        public long ItemsCurrency { get; set; }
        public decimal Paid { get; set; }
        public long CustomerCard { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public long CustomerAddressID { get; set; }
        public string CustomerAddress { get; set; }
        public int CustomerType { get; set; }
        public decimal CustomerDiscount { get; set; }
        public bool CustomerDiscountNonDiscounted { get; set; }
        public long CustomerTransaction { get; set; }
        public long? PriceList { get; set; }
        public long? PaymentTitle { get; set; }

        public bool UseEET { get; set; }
        public string UUID { get; set; }
        public string Pkp { get; set; }
        public string Bkp { get; set; }
        public string Fik { get; set; }

        internal List<BillItem> items = null;
        internal event LazyLoadListLong<BillItem> LoadItems;
        public List<BillItem> Items
        {
            get
            {
                if (items == null)
                    if (LoadItems != null)
                        LoadItems(this, this.ID, ref items);
                    else
                        items = new List<BillItem>();

                return items;
            }
            set { items = value; }
        }

        internal List<BillPayment> payments = null;
        internal event LazyLoadListLong<BillPayment> LoadPayments;
        public List<BillPayment> Payments
        {
            get
            {
                if (payments == null)
                    if (LoadPayments != null)
                        LoadPayments(this, this.ID, ref payments);
                    else
                        payments = new List<BillPayment>();

                return payments;
            }
            set { payments = value; }
        }

        public Bill()
        {
            DateTime = DateTime.Now;
            Status = ERecordStatus.Unfinished;
        }


        public override void AssignIDToChildren()
        {
            base.AssignIDToChildren();

            foreach (BillItem item in Items)
            {
                item.Record = ID;
            }

            foreach (BillPayment item in Payments)
            {
                item.Bill = ID;
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return Items.Sum(n => n.TotalPriceWithDiscount);
            }
        }

        public decimal TotalPriceWithDiscount
        {
            get
            {
                decimal total = TotalPrice;
                if (Discount != 0)
                    total *= (100m - Discount) / 100m;

                return total;
            }
        }

        public decimal TotalPriceWithRounding
        {
            get
            {
                decimal total = TotalPriceWithCustomerDiscount;

                return total + Rounding;
            }
        }

        public decimal TotalPriceWithRoundingAndVouchers
        {
            get
            {
                decimal total = TotalPriceWithRounding;

                return total;
            }
        }

        public decimal TotalPayments
        {
            get
            {
                return Payments.Sum(n => n.Amount);
            }
        }

        public decimal TotalRemains
        {
            get
            {
                return TotalPrice - TotalPayments;
            }
        }

        public decimal TotalRemainsWithDiscount
        {
            get
            {
                return TotalPriceWithDiscount - TotalPayments;
            }
        }

        public decimal TotalRemainsWithRounding
        {
            get
            {
                return TotalPriceWithRounding - TotalPayments;
            }
        }

        

        public void Add(PackageSummary pkg, decimal multiply = 1)
        {
            foreach (BillItem it in Items)
            {
                if (it.Package==pkg.ID)
                {
                    it.Amount += multiply;
                    return;
                }
            }
            Items.Add(new BillItem() { Record = this.ID, Package = pkg.ID, Name = pkg.Name, Amount = multiply, Price = pkg.Price, Vat = pkg.Vat, Discount = pkg.Discount });
        }

        public void Remove(PackageSummary pkg, decimal multiply = 1)
        {
            foreach (BillItem it in Items)
            {
                if (it.Package == pkg.ID)
                {
                    it.Amount -= multiply;
                    return;
                }
            }
            Items.Add(new BillItem() { Record = this.ID, Package = pkg.ID, Name = pkg.Name, Amount = -multiply, Price = pkg.Price, Vat = pkg.Vat, Discount = pkg.Discount });
        }

        public decimal CheckCount(PackageSummary pkg)
        {
            foreach (BillItem it in Items)
            {
                if (it.Package == pkg.ID)
                {
                    return it.Amount;
                }
            }
            return 0;
        }

        public void Add(BillPayment payment)
        {
            foreach (BillPayment it in Payments)
            {
                if (it.PaymentType == payment.PaymentType)
                {
                    it.Amount += payment.Amount;
                    return;
                }
            }
            Payments.Add(payment);
        }


        public void ComputeRounding()
        {
            decimal totalPrice = TotalPriceWithCustomerDiscount;
            totalPrice -= (int)totalPrice;
            if (totalPrice < 0.5m)
                Rounding = -totalPrice;
            else
                Rounding = 1 - totalPrice; 
        }

        public void ClearRounding()
        {
            Rounding = 0;
        }

        public decimal TotalAmount
        {
            get
            {
                decimal d = 0;
                foreach (BillItem it in Items)
                    d += it.TotalAmount;
                return d;
            }
        }

        public virtual decimal TotalPriceWithCustomerDiscount
        {
            get
            {
                decimal totalPrice = TotalPriceWithDiscount;
                if (CustomerDiscount != 0)
                {
                    if (!CustomerDiscountNonDiscounted)
                        return Transware.Data.DataTypes.IndividualDiscount.GetPrice(totalPrice, CustomerDiscount);
                    else
                    {
                        foreach (BillItem item in Items)
                        {
                            IndividualDiscount id = null;
                            if (item.IndividualDiscount != null && item.IndividualDiscount.Value != 0)
                                id = IndividualDiscountModel.Instance.Get(item.IndividualDiscount.Value);

                            Package pkg = null;
                            if (item.Package != 0)
                                pkg = PackageModel.Instance.Get(item.Package.Value);
                            Product p = null;
                            ProductSetting ps = null;

                            if (pkg != null)
                            {
                                p = ProductModel.Instance.Get(pkg.ProductID.Value);
                                ps = p.Setting;
                            }
                            if (id != null || (item.Package != null && p != null && ps.Action))
                                continue;
                            decimal price = item.TotalPriceWithDiscount;
                            if (id != null)
                                price = id.GetPrice(price);
                            totalPrice -= price;
                            totalPrice += Transware.Data.DataTypes.IndividualDiscount.GetPrice(price, CustomerDiscount);
                        }
                    }
                }
                return totalPrice;
            }
        }


        public virtual RecordTax[] Taxes
        {
            get
            {
                decimal totalPrice = TotalPrice;
                Dictionary<int, RecordTax> taxes = new Dictionary<int, RecordTax>();
                foreach (BillItem item in Items)
                {
                    if (!item.Valid) continue;

                    int v = (int)item.Vat;
                    RecordTax tax;
                    if (!taxes.ContainsKey(v))
                    {
                        tax = new RecordTax();
                        tax.Percent = v;
                        taxes.Add(v, tax);
                    }
                    else
                    {
                        tax = taxes[v];
                    }
                    decimal ration = (totalPrice == 0) ? 0 : item.PriceWithDiscount / totalPrice;
                    decimal price = item.PriceWithDiscount + ration * Rounding;
                    if (Discount!=0)
                        price = Transware.Data.DataTypes.IndividualDiscount.GetPrice(price, Discount);
                    //if (CustomerDiscount != 0)
                    //{
                    //    if (CustomerDiscountNonDiscounted)
                    //    {
                    //        if (!(item.IndividualDiscount != null || (item.Package != null && item.Package.Product != null && item.Package.Product.Setting.Action)))
                    //            price = IndividualDiscount.GetPrice(price, CustomerDiscount);
                    //    }
                    //    else
                    //        price = IndividualDiscount.GetPrice(price, CustomerDiscount);
                    //}
                    if (item.IndividualItem)
                    {
                        tax.Price += item.Amount * price;
                        tax.PriceNoVat += item.Amount * Vat.GetPriceNoVat(v, price);
                    }
                    else
                    {
                        tax.Price += item.TotalAmount * price;
                        tax.PriceNoVat += item.TotalAmount * Vat.GetPriceNoVat(v, price);
                    }
                    taxes[v] = tax;
                }

                return taxes.Values.ToArray();                
            }
        }
    }
}
