﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class ProductAttributeGroup : AbstractValidDto
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool IsColor { get; set; }

        public override string ToString()
        {
            return Name;
        }

        
        #region Collections

        internal List<ProductAttribute> attributes = null;
        internal event LazyLoadListLong<ProductAttribute> LoadAttributes = null;
        public List<ProductAttribute> Attributes
        {
            get
            {
                if (attributes == null)
                    if (LoadAttributes != null)
                        LoadAttributes(this, ID, ref attributes);

                return attributes;
            }
            internal set { attributes = value; }
        }

        #endregion

        public override void AssignIDToChildren()
        {
            if (attributes != null)
            {
                foreach (ProductAttribute l in attributes)
                    l.GroupID = ID;
            }
        }
    }
}
