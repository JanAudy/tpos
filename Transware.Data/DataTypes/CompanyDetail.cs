﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class CompanyDetail : AbstractIDLongDto
    {
        public long OrganizationID { get; set; }
        public decimal Penalization { get; set; }
        public int Maturity { get; set; }
        public int RecordLanguage { get; set; }
    }
}
