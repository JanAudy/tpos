﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataTypes
{
    

    public class ConfigClass : AbstractIDLongDto
    {
        public long CompanyID { get; set; }
        public long? MainCurrencyID { get; set; }
        public int StoreReleaseType { get; set; }
        public int BusinessType { get; set; }
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public bool UseCredentials { get; set; }
        public bool SslConnection { get; set; }
        public string FromEmail { get; set; }
        public string NotificationEmail { get; set; }
        public bool SendNotificationEmail { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool SendSystemEmails { get; set; }
        public float MinMoneyInsert { get; set; }
        public bool PrintCardPaidBills { get; set; }
        public string BillsFirstLine { get; set; }
        public string BillsHeader { get; set; }
        public string BillsFooter { get; set; }
        public int MainLanguage { get; set; }
        public string Display1Line { get; set; }
        public string Display2Line { get; set; }

        
        public virtual string[] BillsHeaderStrings
        {
            get
            {
                if (BillsHeader != null)
                {
                    string[] lines = BillsHeader.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < lines.Length; i++)
                        lines[i] = lines[i].Replace('~', ',');
                    return lines;
                }
                else
                    return new string[] { "" };
            }
            set
            {
                bool first = true;
                string result = "";
                foreach (string s in value)
                {
                    string str = s.Replace(',', '~');
                    if (first) first = false;
                    else result += ",";
                    result += str;
                }
                BillsHeader = result;
            }
        }
        
        public virtual string[] BillsFooterStrings
        {
            get
            {
                if (BillsFooter != null)
                {
                    string[] lines = BillsFooter.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < lines.Length; i++)
                        lines[i] = lines[i].Replace('~', ',');
                    return lines;
                }
                else
                    return new string[] { "" };
            }
            set
            {
                bool first = true;
                string result = "";
                foreach (string s in value)
                {
                    string str = s.Replace(',', '~');
                    if (first) first = false;
                    else result += ",";
                    result += str;
                }
                BillsFooter = result;
            }
        }


        internal Currency mainCurrency = null;
        internal event LazyLoadObjectLong<Currency> LoadMainCurrency = null;
        public Currency MainCurrency
        {
            get
            {
                if (mainCurrency == null && MainCurrencyID != 0)
                    if (LoadMainCurrency != null && MainCurrencyID.HasValue)
                        LoadMainCurrency(this, MainCurrencyID.Value, ref mainCurrency);

                return mainCurrency;
            }
            set { mainCurrency = value; MainCurrencyID = (value != null) ? value.ID : (long?)null; }
        }


        internal Dictionary<EConfigFields, ConfigClassField> fields = null;
        internal LazyLoadDictionary<EConfigFields, ConfigClassField> LoadFields = null;
        public Dictionary<EConfigFields, ConfigClassField> Fields
        {
            get
            {
                if (fields == null && LoadFields != null)
                    LoadFields(this, ID, ref fields);
                return fields;
            }
            internal set { fields = value; }
        }


        public string GetField(EConfigFields name)
        {
            if (Fields.ContainsKey(name))
                return Fields[name].Value;
            else 
                return "";
        }

        public void SetField(EConfigFields name, string value)
        {
            if (Fields.ContainsKey(name))
                Fields[name].Value = value;
        }
    }
}
