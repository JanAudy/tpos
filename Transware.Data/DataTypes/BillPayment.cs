﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Transware.Data.DataTypes
{
    public class BillPayment : AbstractValidDto
    {
        public long Bill { get; set; }
        public long PaymentType { get; set; }
        public decimal Amount { get; set; }

        public string Name { get; set; }
    }
}
