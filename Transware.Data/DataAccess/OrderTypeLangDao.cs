﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class OrderTypeLangDao : AbstractTranswareDao<OrderTypeLang>
    {
        public OrderTypeLangDao(SqlConnection connection)
            : base(connection, "OrderTypeLangs") { }

        public Dictionary<ELanguage, OrderTypeLang> GetByParent(long parentID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " WHERE OrderType=" + parentID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                Dictionary<ELanguage, OrderTypeLang> result = new Dictionary<ELanguage, OrderTypeLang>();
                while (reader.Read())
                {
                    OrderTypeLang lang = Interprete(reader, cols);
                    result.Add(lang.Language, lang);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new Dictionary<ELanguage, OrderTypeLang>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override bool SaveOrUpdate(OrderTypeLang obj)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "Update " + tableName + " Set ";
                comm.CommandText += "Language=@language, OrderType=@unit, Value=@value";
                comm.CommandText += " WHERE OrderType=" + obj.OrderTypeID + " AND Language=" + (int)obj.Language;
                comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                comm.Parameters.Add(new SqlParameter("unit", obj.OrderTypeID));
                comm.Parameters.Add(new SqlParameter("value", obj.Value));

                int count = comm.ExecuteNonQuery();

                if (count == 0)
                {
                    comm.Dispose();
                    comm = connection.CreateCommand();
                    comm.CommandText = "INSERT INTO " + tableName + "(OrderType, Language, Value)";
                    comm.CommandText += " Values(@unit, @language, @value)";
                    comm.Parameters.Add(new SqlParameter("unit", obj.OrderTypeID));
                    comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                    comm.Parameters.Add(new SqlParameter("value", obj.Value));
                    count = comm.ExecuteNonQuery();
                }

                bool result = count == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override OrderTypeLang Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            OrderTypeLang obj = new OrderTypeLang();
            obj.Language = (ELanguage)Interprete(reader, cols, "Language", (int)obj.Language);
            obj.OrderTypeID = Interprete(reader, cols, "OrderType", obj.OrderTypeID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            return obj;
        }

    }
}
