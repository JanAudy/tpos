﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class UnitLangDao : AbstractTranswareDao<UnitLang>
    {
        public UnitLangDao(SqlConnection connection)
            : base(connection, "UnitsLangs") { }

        public Dictionary<ELanguage, UnitLang> GetByParent(long parentID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " WHERE Unit=" + parentID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                Dictionary<ELanguage, UnitLang> result = new Dictionary<ELanguage, UnitLang>();
                while (reader.Read())
                {
                    UnitLang lang = Interprete(reader, cols);
                    result.Add(lang.Language, lang);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new Dictionary<ELanguage, UnitLang>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override bool SaveOrUpdate(UnitLang obj)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "Update " + tableName + " Set ";
                comm.CommandText += "Language=@language, Unit=@unit, Value=@value";
                comm.CommandText += " WHERE Unit=" + obj.UnitID + " AND Language=" + (int)obj.Language;
                comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                comm.Parameters.Add(new SqlParameter("unit", obj.UnitID));
                comm.Parameters.Add(new SqlParameter("value", obj.Value));

                int count = comm.ExecuteNonQuery();

                if (count == 0)
                {
                    comm.Dispose();
                    comm = connection.CreateCommand();
                    comm.CommandText = "INSERT INTO " + tableName + "(Language, Unit, Value)";
                    comm.CommandText += " Values(@language, @unit, @value)";
                    comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                    comm.Parameters.Add(new SqlParameter("unit", obj.UnitID));
                    comm.Parameters.Add(new SqlParameter("value", obj.Value));
                    count = comm.ExecuteNonQuery();
                }

                bool result = count == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override UnitLang Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            UnitLang obj = new UnitLang();
            obj.Language = (ELanguage)Interprete(reader, cols, "Language", (int)obj.Language);
            obj.UnitID = Interprete(reader, cols, "Unit", obj.UnitID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            return obj;
        }

    }
}
