using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PackageTypeDao : AbstractTranswareDao<PackageType>
    {
        public PackageTypeDao(SqlConnection connection) : base(connection, "PackageTypes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PackageType obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(PackageType obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
