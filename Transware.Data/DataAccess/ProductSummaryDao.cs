﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class ProductSummaryDao : AbstractIDLongDao<ProductSummary>
    {
        public ProductSummaryDao(SqlConnection connection) : base(connection, "ProductSummarys") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductSummary obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Category = Interprete(reader, cols, "Category", obj.Category);
            obj.PlaceNo = Interprete(reader, cols, "PlaceNo", obj.PlaceNo);
            obj.Tax = Interprete(reader, cols, "Tax", obj.Tax);
            obj.Actual = Interprete(reader, cols, "Actual", obj.Actual);
            obj.Internet = Interprete(reader, cols, "Internet", obj.Internet);
            obj.IndividualPrice = Interprete(reader, cols, "IndividualPrice", obj.IndividualPrice);
            obj.UndefinedAmount = Interprete(reader, cols, "UndefinedAmount", obj.UndefinedAmount);
            if (cols.Contains("Amount") && !(reader["Amount"] is DBNull))
            {
                int amount = Interprete(reader, cols, "Amount", obj.Amount);
                decimal price = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
                obj.Prices.Add(amount, price);
            }
            obj.RealNoReserved = Interprete(reader, cols, "RealNoReserved", obj.RealNoReserved);
            obj.RealReserved = Interprete(reader, cols, "RealReserved", obj.RealReserved);
            obj.RealTotal = Interprete(reader, cols, "RealTotal", obj.RealTotal);
            obj.RealLocked = Interprete(reader, cols, "RealLocked", obj.RealLocked);
            obj.RequestedMin = Interprete(reader, cols, "RequestedMin", obj.RequestedMin);
            obj.RequestedMax = Interprete(reader, cols, "RequestedMax", obj.RequestedMax);
            obj.OrderMark = Interprete(reader, cols, "OrderMark", obj.OrderMark);
            obj.UserID1 = Interprete(reader, cols, "UserID1", obj.UserID1);
            obj.UserID2 = Interprete(reader, cols, "UserID2", obj.UserID2);
        }

        public ProductSummary.ProductVariantSummary InterpreteVariant(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols)
        {
            ProductSummary.ProductVariantSummary obj = new ProductSummary.ProductVariantSummary();

            obj.ID= Interprete(reader, cols, "ID", obj.ID);
            obj.PID= Interprete(reader, cols, "PID", obj.ID);
            obj.Actual = Interprete(reader, cols, "Actual", obj.Actual);
            obj.Internet = Interprete(reader, cols, "Internet", obj.Internet);
            obj.IndividualPrice = Interprete(reader, cols, "IndividualPrice", obj.IndividualPrice);
            obj.UndefinedAmount = Interprete(reader, cols, "UndefinedAmount", obj.UndefinedAmount);
            if (cols.Contains("Amount") && !(reader["Amount"] is DBNull))
            {
                int amount = Interprete(reader, cols, "Amount", obj.Amount);
                decimal price = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
                obj.Prices.Add(amount, price);
            }
            obj.RealNoReserved = Interprete(reader, cols, "RealNoReserved", obj.RealNoReserved);
            obj.RealReserved = Interprete(reader, cols, "RealReserved", obj.RealReserved);
            obj.RealTotal = Interprete(reader, cols, "RealTotal", obj.RealTotal);
            obj.RealLocked = Interprete(reader, cols, "RealLocked", obj.RealLocked);
            obj.RequestedMin = Interprete(reader, cols, "RequestedMin", obj.RequestedMin);
            obj.RequestedMax = Interprete(reader, cols, "RequestedMax", obj.RequestedMax);
            obj.OrderMark = Interprete(reader, cols, "OrderMark", obj.OrderMark);
            obj.UserID1 = Interprete(reader, cols, "UserID1", obj.UserID1);
            obj.UserID2 = Interprete(reader, cols, "UserID2", obj.UserID2);

            return obj;
        }

        public override void FillParams(ProductSummary obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Category", obj.Category);
            AddParam(parameters, "PlaceNo", obj.PlaceNo);
            AddParam(parameters, "Tax", obj.Tax);
            AddParam(parameters, "Actual", obj.Actual);
            AddParam(parameters, "Internet", obj.Internet);
            AddParam(parameters, "IndividualPrice", obj.IndividualPrice);
            AddParam(parameters, "UndefinedAmount", obj.UndefinedAmount);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "PriceNoVat", obj.PriceNoVat);
            AddParam(parameters, "RealNoReserved", obj.RealNoReserved);
            AddParam(parameters, "RealReserved", obj.RealReserved);
            AddParam(parameters, "RealTotal", obj.RealTotal);
            AddParam(parameters, "RealLocked", obj.RealLocked);
            AddParam(parameters, "RequestedMin", obj.RequestedMin);
            AddParam(parameters, "RequestedMax", obj.RequestedMax);
            AddParam(parameters, "OrderMark", obj.OrderMark);
            AddParam(parameters, "UserID1", obj.UserID1);
            AddParam(parameters, "UserID2", obj.UserID2);
        }

        public List<long> GetAllId(long CategoryID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT p.ID " +
                                   "FROM Products p " +
                                   "WHERE p.Valid=1" +
                                   " AND p.Category=" + CategoryID +
                                   " ORDER BY p.PlaceNo";

                
                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<long> result = new List<long>();
                while (reader.Read())
                {
                    result.Add((long)reader[0]);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<long>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual List<ProductSummary> GetList(long StoreID, long CategoryID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT p.ID, p.Category, p.PlaceNo, p.Name, p.UserID1, p.UserID2, v.Tax, s.Actual, s.Internet, s.IndividualPrice, s.UndefinedAmount, pt.Amount, spkg.PriceNoVat, sp.RealNoReserved, sp.RealReserved, sp.RealTotal, sp.RealLocked, sp.RequestedMin, sp.RequestedMax, pv.OrderMark " +
                                   "FROM Products p " +
                                   "LEFT JOIN Vats v on v.ID=p.Vat " +
                                   "LEFT JOIN ProductSettings s on s.ID=p.Setting "+
                                   "LEFT OUTER JOIN Packages pkg ON pkg.Product=p.ID AND pkg.Valid=1 " +
                                   "LEFT OUTER JOIN PackageTypes pt ON pt.ID=pkg.Type " +
                                   "LEFT OUTER JOIN StoreProducts sp ON sp.Product=p.ID " + " AND sp.Store=" + StoreID + " " +
                                   "LEFT OUTER JOIN StorePackages spkg ON spkg.Package=pkg.ID " + " AND spkg.Store=" + StoreID + " " +
                                   "LEFT OUTER JOIN ProductVariant pv ON pv.Product=p.id " +
                                   "WHERE p.Valid=1 " +
                                   " AND s.ID=p.Setting "+
                                   " AND p.Category=" + CategoryID + " " +
                                   " AND sp.Store=" + StoreID +
                                   "ORDER BY p.PlaceNo, p.ID, pt.Amount";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<ProductSummary> result = new List<ProductSummary>();
                ProductSummary lps = null;
                Dictionary<long, ProductSummary> summaries = new Dictionary<long, ProductSummary>();
                while (reader.Read())
                {
                    ProductSummary ps  = Interprete(reader, cols);
                    if (lps==null || ps.ID != lps.ID)
                    {
                        result.Add(ps);
                        if (!summaries.ContainsKey(ps.ID))
                            summaries.Add(ps.ID, ps);
                        lps = ps;
                    }
                    else
                    {
                        foreach (KeyValuePair<int, decimal> kvp in ps.Prices)
                        {
                            if (!lps.Prices.ContainsKey(kvp.Key))
                                lps.Prices.Add(kvp.Key, kvp.Value);
                        }
                    }
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                // variants
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT p.ID as pid, pv.ID, pv.OrderMark, pv.UserID1, pv.UserID2, s.Actual, s.Internet, s.IndividualPrice, s.UndefinedAmount, pt.Amount, spkg.PriceNoVat, sp.RealNoReserved, sp.RealReserved, sp.RealTotal, sp.RealLocked, sp.RequestedMin, sp.RequestedMax " + 
                                   "FROM Products p, ProductVariant pv " +
                                   "LEFT JOIN ProductSettings s on s.ID=pv.Setting "+
                                   "LEFT OUTER JOIN Packages pkg ON pkg.Variant=pv.ID  AND pkg.Valid=1 " +
                                   "LEFT OUTER JOIN PackageTypes pt ON pt.ID=pkg.Type "+
                                   "LEFT OUTER JOIN StoreProducts sp ON sp.Variant=pv.ID "+
                                   "LEFT OUTER JOIN StorePackages spkg ON spkg.Package=pkg.ID "+" AND spkg.Store=" + StoreID + " " +
                                   "WHERE p.ID=pv.Product AND p.Valid=1 AND pv.Valid=1 "+
                                   " AND p.Category=" + CategoryID + " " +
                                   " AND sp.Store=" + StoreID +
                                   "ORDER BY p.ID, pv.ID, pt.Amount";

                reader = comm.ExecuteReader();

                cols = new PresentColumnsCollection(reader);
                ProductSummary.ProductVariantSummary lpvs = null;
                while (reader.Read())
                {
                    ProductSummary.ProductVariantSummary pvs = InterpreteVariant(reader, cols);
                    if (lpvs == null || pvs.ID != lpvs.ID)
                    {
                        if (summaries.ContainsKey(pvs.PID))
                        {
                            ProductSummary ps = summaries[pvs.PID];
                            pvs.Tax = ps.Tax;
                            ps.Variants.Add(pvs);
                        }

                        lpvs = pvs;
                    }
                    else
                    {
                        foreach (KeyValuePair<int, decimal> kvp in pvs.Prices)
                        {
                            if (!lpvs.Prices.ContainsKey(kvp.Key))
                                lpvs.Prices.Add(kvp.Key, kvp.Value);
                        }
                    }
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<ProductSummary>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual List<ProductSummary> GetList(long StoreID, List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT p.ID, p.Category, p.PlaceNo, p.Name, p.UserID1, p.UserID2, v.Tax, s.Actual, s.Internet, s.IndividualPrice, s.UndefinedAmount, pt.Amount, spkg.PriceNoVat, sp.RealNoReserved, sp.RealReserved, sp.RealTotal, sp.RealLocked, sp.RequestedMin, sp.RequestedMax, pv.OrderMark " +
                                   "FROM Products p " +
                                   "LEFT JOIN Vats v on v.ID=p.Vat " +
                                   "LEFT JOIN ProductSettings s on s.ID=p.Setting " +
                                   "LEFT OUTER JOIN Packages pkg ON pkg.Product=p.ID " +
                                   "LEFT OUTER JOIN PackageTypes pt ON pt.ID=pkg.Type " +
                                   "LEFT OUTER JOIN StoreProducts sp ON sp.Product=p.ID " +
                                   "LEFT OUTER JOIN StorePackages spkg ON spkg.Package=pkg.ID " +
                                   "LEFT OUTER JOIN ProductVariant pv ON pv.Product=p.id " +
                                   "LEFT OUTER JOIN Providers pp ON pp.Product=p.ID " +
                                   "LEFT OUTER JOIN Providers pvp ON pvp.Variant=pv.ID "+
                                   "WHERE p.Valid=1 " +
                                   " AND pkg.Valid=1 " +
                                   " AND s.ID=p.Setting " +
                                   " AND spkg.Store=" + StoreID +
                                   " AND sp.Store=" + StoreID;
                if (constraints != null && constraints.Count > 0)
                {
                    comm.CommandText += " AND " + ConstraintsToString(constraints, comm.Parameters);
                }
                comm.CommandText += " ORDER BY p.ID";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<ProductSummary> result = new List<ProductSummary>();
                ProductSummary lps = null;
                while (reader.Read())
                {
                    ProductSummary ps = Interprete(reader, cols);
                    if (lps == null || ps.ID != lps.ID)
                    {
                        result.Add(ps);
                        lps = ps;
                    }
                    else
                    {
                        foreach (KeyValuePair<int, decimal> kvp in ps.Prices)
                        {
                            if (!lps.Prices.ContainsKey(kvp.Key))
                                lps.Prices.Add(kvp.Key, kvp.Value);
                        }
                    }
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<ProductSummary>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void UpdatePlaceNo(List<ProductSummary> products)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary p in products)
                {
                    comm.CommandText += "UPDATE Products SET PlaceNo=" + p.PlaceNo + " WHERE ID=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
               if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void UpdateCategoryAndPlaceNo(List<ProductSummary> products)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary p in products)
                {
                    comm.CommandText += "UPDATE Products SET PlaceNo=" + p.PlaceNo + ", Category=" + p.Category + " WHERE ID=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void SetInvalid(List<ProductSummary> products)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary p in products)
                {
                    comm.CommandText += "UPDATE Products SET Valid=0 WHERE ID=" + p.ID + ";\n";
                    comm.CommandText += "UPDATE Packages SET Valid=0 WHERE Product=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void SetInvalid(List<ProductSummary.ProductVariantSummary> products)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary.ProductVariantSummary p in products)
                {
                    comm.CommandText += "UPDATE ProductVariant SET Valid=0 WHERE ID=" + p.ID + ";\n";
                    comm.CommandText += "UPDATE Packages SET Valid=0 WHERE Variant=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void SetInternet(List<ProductSummary> products, bool status)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary p in products)
                {
                    comm.CommandText += "UPDATE ProductSettings Set ProductSettings.Internet="+((status)?1:0).ToString()+
                        " FROM  ProductSettings INNER JOIN Products on Products.Setting=ProductSettings.ID WHERE Products.ID=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual void SetInternet(List<ProductSummary.ProductVariantSummary> products, bool status)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductSummary.ProductVariantSummary p in products)
                {
                    comm.CommandText += "UPDATE ProductSettings Set ProductSettings.Internet=" + ((status) ? 1 : 0).ToString() +
                        " FROM  ProductSettings INNER JOIN ProductVariant on ProductVariant.Setting=ProductSettings.ID WHERE ProductVariant.ID=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

    }
}
