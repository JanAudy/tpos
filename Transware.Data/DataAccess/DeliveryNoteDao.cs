using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class DeliveryNoteDao : AbstractTranswareDao<DeliveryNote>
    {
        public DeliveryNoteDao(SqlConnection connection) : base(connection, "DeliveryNotes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref DeliveryNote obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.SupplierID = Interprete(reader, cols, "SupplierID", obj.SupplierID);
            obj.SupplierName = Interprete(reader, cols, "SupplierName", obj.SupplierName);
            obj.SupplierAddressID = Interprete(reader, cols, "SupplierAddressID", obj.SupplierAddressID);
            obj.SupplierAddress = Interprete(reader, cols, "SupplierAddress", obj.SupplierAddress);
            obj.CustomerID = Interprete(reader, cols, "CustomerID", obj.CustomerID);
            obj.CustomerName = Interprete(reader, cols, "CustomerName", obj.CustomerName);
            obj.CustomerAddressID = Interprete(reader, cols, "CustomerAddressID", obj.CustomerAddressID);
            obj.CustomerAddress = Interprete(reader, cols, "CustomerAddress", obj.CustomerAddress);
            obj.TransportType = Interprete(reader, cols, "TransportType", obj.TransportType);
            obj.FinalRecipient = Interprete(reader, cols, "FinalRecipient", obj.FinalRecipient);
            obj.FinalRecipientStr = Interprete(reader, cols, "FinalRecipientStr", obj.FinalRecipientStr);
            obj.OrderNo = Interprete(reader, cols, "OrderNo", obj.OrderNo);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Creation = Interprete(reader, cols, "Creation", obj.Creation);
            obj.PrintPrices = Interprete(reader, cols, "PrintPrices", obj.PrintPrices);     
            obj.ItemsCurrency = Interprete(reader, cols, "ItemsCurrency", obj.ItemsCurrency);           
            obj.Clerk = Interprete(reader, cols, "Clerk", obj.Clerk);           
            obj.Header = Interprete(reader, cols, "Header", obj.Header);           
            obj.Footer = Interprete(reader, cols, "Footer", obj.Footer);           
        }

        public override void FillParams(DeliveryNote obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "SupplierID", obj.SupplierID);
            AddParam(parameters, "SupplierName", obj.SupplierName);
            AddParam(parameters, "SupplierAddressID", obj.SupplierAddressID);
            AddParam(parameters, "SupplierAddress", obj.SupplierAddress);
            AddParam(parameters, "CustomerID", obj.CustomerID);
            AddParam(parameters, "CustomerName", obj.CustomerName);
            AddParam(parameters, "CustomerAddressID", obj.CustomerAddressID);
            AddParam(parameters, "CustomerAddress", obj.CustomerAddress);
            AddParam(parameters, "TransportType", obj.TransportType);
            AddParam(parameters, "FinalRecipient", obj.FinalRecipient);
            AddParam(parameters, "FinalRecipientStr", obj.FinalRecipientStr);
            AddParam(parameters, "OrderNo", obj.OrderNo);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Creation", obj.Creation);
            AddParam(parameters, "PrintPrices", obj.PrintPrices);
            AddParam(parameters, "ItemsCurrency", obj.ItemsCurrency);
            AddParam(parameters, "Clerk", obj.Clerk);
            AddParam(parameters, "Header", obj.Header);
            AddParam(parameters, "Footer", obj.Footer);
        }
    }
}
