using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class SellOrderItemDao : AbstractTranswareDao<SellOrderItem>
    {
        public SellOrderItemDao(SqlConnection connection) : base(connection, "SellOrderItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref SellOrderItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.SellOrder = Interprete(reader, cols, "SellOrder", obj.SellOrder);
            obj.IndividualItem = Interprete(reader, cols, "IndividualItem", obj.IndividualItem);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(SellOrderItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "SellOrder", obj.SellOrder);
            AddParam(parameters, "IndividualItem", obj.IndividualItem);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
