using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RecordHeaderDao : AbstractTranswareDao<RecordHeader>
    {
        public RecordHeaderDao(SqlConnection connection) : base(connection, "RecordHeaders") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RecordHeader obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
        }

        public override void FillParams(RecordHeader obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Text", obj.Text);
        }
    }
}
