using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataAccess
{
    public class VoucherMarkDao : AbstractTranswareDao<VoucherMark>
    {
        public VoucherMarkDao(SqlConnection connection) : base(connection, "VoucherMarks") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref VoucherMark obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.VoucherType = Interprete(reader, cols, "VoucherType", obj.VoucherType);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
        }

        public override void FillParams(VoucherMark obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "VoucherType", obj.VoucherType);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Description", obj.Description);
        }
    }
}
