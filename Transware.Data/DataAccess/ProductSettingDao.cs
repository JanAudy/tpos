﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductSettingDao : AbstractIDLongDao<ProductSetting>
    {
        public ProductSettingDao(SqlConnection connection) : base(connection, "ProductSettings") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductSetting obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Actual = Interprete(reader, cols, "Actual", obj.Actual);
            obj.Tandem = Interprete(reader, cols, "Tandem", obj.Tandem);
            obj.Catalog = Interprete(reader, cols, "Catalog", obj.Catalog);
            obj.UndefinedAmount = Interprete(reader, cols, "UndefinedAmount", obj.UndefinedAmount);
            obj.IndividualPrice = Interprete(reader, cols, "IndividualPrice", obj.IndividualPrice);
            obj.Internet = Interprete(reader, cols, "Internet", obj.Internet);
            obj.OverHead = Interprete(reader, cols, "OverHead", obj.OverHead);
            obj.Commissional = Interprete(reader, cols, "Commissional", obj.Commissional);
            obj.ShowPricesOnWeb = Interprete(reader, cols, "ShowPricesOnWeb", obj.ShowPricesOnWeb);
            obj.ShowOnTitlePage = Interprete(reader, cols, "ShowOnTitlePage", obj.ShowOnTitlePage);    
            obj.Voucher = Interprete(reader, cols, "Voucher", obj.Voucher);    
            obj.VariableLength = Interprete(reader, cols, "VariableLength", obj.VariableLength);    
            obj.Action = Interprete(reader, cols, "Action", obj.Action);    
            obj.Prescription = Interprete(reader, cols, "Prescription", obj.Prescription);    
        }

        public override void FillParams(ProductSetting obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Actual", obj.Actual);
            AddParam(parameters, "Tandem", obj.Tandem);
            AddParam(parameters, "Catalog", obj.Catalog);
            AddParam(parameters, "UndefinedAmount", obj.UndefinedAmount);
            AddParam(parameters, "IndividualPrice", obj.IndividualPrice);
            AddParam(parameters, "Internet", obj.Internet);
            AddParam(parameters, "OverHead", obj.OverHead);
            AddParam(parameters, "Commissional", obj.Commissional);
            AddParam(parameters, "ShowPricesOnWeb", obj.ShowPricesOnWeb);
            AddParam(parameters, "ShowOnTitlePage", obj.ShowOnTitlePage);
            AddParam(parameters, "Voucher", obj.Voucher);
            AddParam(parameters, "VariableLength", obj.VariableLength);
            AddParam(parameters, "Action", obj.Action);
            AddParam(parameters, "Prescription", obj.Prescription);
        }
    }
}
