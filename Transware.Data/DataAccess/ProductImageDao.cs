using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductImageDao : AbstractTranswareDao<ProductImage>
    {
        public ProductImageDao(SqlConnection connection) : base(connection, "ProductImages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductImage obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Product = Interprete(reader, cols, "Product", obj.Product);
            obj.Variant = Interprete(reader, cols, "Variant", obj.Variant);
            obj.Thumbnail = Interprete(reader, cols, "Thumbnail", obj.Thumbnail);
            obj.Image = Interprete(reader, cols, "Image", obj.Image);
            obj.Position = Interprete(reader, cols, "Position", obj.Position); 
        }

        public override void FillParams(ProductImage obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            if (obj.Product!=0)
                AddParam(parameters, "Product", obj.Product);
            if (obj.Variant!=0)
                AddParam(parameters, "Variant", obj.Variant); 
            AddParam(parameters, "Thumbnail", obj.Thumbnail); 
            AddParam(parameters, "Image", obj.Image);
            AddParam(parameters, "Position", obj.Position);
        }

        public virtual void UpdatePosition(List<ProductImage> productImages)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "";
                comm.CommandText += "BEGIN TRANSACTION\n";
                foreach (ProductImage p in productImages)
                {
                    comm.CommandText += "UPDATE ProductImages SET Position=" + p.Position.ToString() + " WHERE ID=" + p.ID + ";\n";
                }
                comm.CommandText += "COMMIT TRANSACTION";
                comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;
            }
            catch (Exception e)
            {
                if (comm != null)
                    comm.Dispose();
                comm = connection.CreateCommand();
                comm.CommandText = "ROLLBACK TRANSACTION";
                comm.ExecuteNonQuery();
                comm.Dispose();
                comm = null;
                throw e;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

    }
}
