using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class BillPaymentDao : AbstractTranswareDao<BillPayment>
    {
        public BillPaymentDao(SqlConnection connection) : base(connection, "BillPayments") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref BillPayment obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Bill = Interprete(reader, cols, "Bill", obj.Bill);
            obj.PaymentType = Interprete(reader, cols, "PaymentType", obj.PaymentType);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);

        }

        public override void FillParams(BillPayment obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Bill", obj.Bill);
            AddParam(parameters, "PaymentType", obj.PaymentType);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
