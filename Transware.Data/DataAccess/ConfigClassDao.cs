﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ConfigClassDao : AbstractIDLongDao<ConfigClass>
    {
        protected override string IDColumnName
        {
            get
            {
                return "ConfigClassID";
            }
        }
        public ConfigClassDao(SqlConnection connection) : base(connection, "ConfigClass") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ConfigClass obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.CompanyID = Interprete(reader, cols, "CompanyID", obj.CompanyID);
            obj.MainCurrencyID = Interprete(reader, cols, "MainCurrency", obj.MainCurrencyID);
            obj.StoreReleaseType = Interprete(reader, cols, "StoreRelaseType", obj.StoreReleaseType);
            obj.BusinessType = Interprete(reader, cols, "BusinessType", obj.BusinessType);
            obj.SmtpServer = Interprete(reader, cols, "SmtpServer", obj.SmtpServer);
            obj.SmtpPort = Interprete(reader, cols, "SmtpPort", obj.SmtpPort);
            obj.UseCredentials = Interprete(reader, cols, "UseCreditals", obj.UseCredentials);
            obj.SslConnection = Interprete(reader, cols, "SslConnection", obj.SslConnection);
            obj.FromEmail = Interprete(reader, cols, "FromEmail", obj.FromEmail);
            obj.NotificationEmail = Interprete(reader, cols, "NotificationEmail", obj.NotificationEmail);
            obj.SendNotificationEmail = Interprete(reader, cols, "SendNotificationEmail", obj.SendNotificationEmail);
            obj.UserName = Interprete(reader, cols, "UserName", obj.UserName);
            obj.Password = Interprete(reader, cols, "Password", obj.Password);
            obj.SendSystemEmails = Interprete(reader, cols, "SendSystemEmails", obj.SendSystemEmails);
            obj.MinMoneyInsert = Interprete(reader, cols, "MinMoneyInsert", obj.MinMoneyInsert);
            obj.PrintCardPaidBills = Interprete(reader, cols, "PrintCardPaidBills", obj.PrintCardPaidBills);
            obj.BillsFirstLine = Interprete(reader, cols, "BillsFirstLine", obj.BillsFirstLine);
            obj.BillsHeader = Interprete(reader, cols, "BillsHeader", obj.BillsHeader);
            obj.BillsFooter = Interprete(reader, cols, "BillsFooter", obj.BillsFooter);
            obj.MainLanguage = Interprete(reader, cols, "MainLanguage", obj.MainLanguage);
            obj.Display1Line = Interprete(reader, cols, "Display1Line", obj.Display1Line);
            obj.Display2Line = Interprete(reader, cols, "Display2Line", obj.Display2Line);    
        }

        public override void FillParams(ConfigClass obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "CompanyID", obj.CompanyID);
            AddParam(parameters, "MainCurrency", obj.MainCurrencyID);
            AddParam(parameters, "StoreRelaseType", obj.StoreReleaseType);
            AddParam(parameters, "BusinessType", obj.BusinessType);
            AddParam(parameters, "SmtpServer", obj.SmtpServer);
            AddParam(parameters, "SmtpPort", obj.SmtpPort);
            AddParam(parameters, "UseCreditals", obj.UseCredentials);
            AddParam(parameters, "SslConnection", obj.SslConnection);
            AddParam(parameters, "FromEmail", obj.FromEmail);
            AddParam(parameters, "NotificationEmail", obj.NotificationEmail);
            AddParam(parameters, "SendNotificationEmail", obj.SendNotificationEmail);
            AddParam(parameters, "UserName", obj.UserName);
            AddParam(parameters, "Password", obj.Password);
            AddParam(parameters, "SendSystemEmails", obj.SendSystemEmails);
            AddParam(parameters, "MinMoneyInsert", obj.MinMoneyInsert);
            AddParam(parameters, "PrintCardPaidBills", obj.PrintCardPaidBills);
            AddParam(parameters, "BillsFirstLine", obj.BillsFirstLine);
            AddParam(parameters, "BillsHeader", obj.BillsHeader);
            AddParam(parameters, "BillsFooter", obj.BillsFooter);
            AddParam(parameters, "MainLanguage", obj.MainLanguage);
            AddParam(parameters, "Display1Line", obj.Display1Line);
            AddParam(parameters, "Display2Line", obj.Display2Line);
        }
    }
}
