﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Transware.Data.Enums;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class ConfigClassFieldDao : AbstractIDLongDao<ConfigClassField>
    {
        public ConfigClassFieldDao(SqlConnection connection) : base(connection, "ConfigClassFields") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ConfigClassField obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ConfigID = Interprete(reader, cols, "ConfigID", obj.ConfigID);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(ConfigClassField obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ConfigID", obj.ConfigID);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Value", obj.Value);
        }

        protected EConfigFields Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, EConfigFields def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (EConfigFields)reader[name];
            return def;
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, EConfigFields value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (long)value));
        }
    }
}
