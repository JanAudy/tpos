using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CoverDao : AbstractTranswareDao<Cover>
    {
        public CoverDao(SqlConnection connection) : base(connection, "Covers") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Cover obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.PackageID = Interprete(reader, cols, "Package", obj.PackageID);
            obj.CoverID = Interprete(reader, cols, "Cover", obj.CoverID);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(Cover obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Package", obj.PackageID);
            AddParam(parameters, "Cover", obj.CoverID);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
