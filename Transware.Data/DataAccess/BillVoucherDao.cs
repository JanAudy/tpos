using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class BillVoucherDao : AbstractTranswareDao<BillVoucher>
    {
        public BillVoucherDao(SqlConnection connection) : base(connection, "BillVouchers") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref BillVoucher obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Bill = Interprete(reader, cols, "Bill", obj.Bill);
            obj.Voucher = Interprete(reader, cols, "Voucher", obj.Voucher);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(BillVoucher obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Bill", obj.Bill);
            AddParam(parameters, "Voucher", obj.Voucher);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
