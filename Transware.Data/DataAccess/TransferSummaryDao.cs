﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class TransferSummaryDao : AbstractTranswareDao<TransferSummary>
    {
        public TransferSummaryDao(SqlConnection connection) : base(connection, "TransferSummarys") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref TransferSummary obj)
        {
            base.Interprete(reader, cols, ref obj);
            
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Source = Interprete(reader, cols, "Source", obj.Source);
            obj.SourceName = Interprete(reader, cols, "SourceName", obj.SourceName);
            obj.Destination = Interprete(reader, cols, "Destination", obj.Destination);
            obj.DestinationName = Interprete(reader, cols, "DestinationName", obj.DestinationName);
            obj.PriceNoVat = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
        }

        public override void FillParams(TransferSummary obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Source", obj.Source);
            AddParam(parameters, "SourceName", obj.SourceName);
            AddParam(parameters, "Destination", obj.Destination);
            AddParam(parameters, "DestinationName", obj.DestinationName);
            AddParam(parameters, "PriceNoVat", obj.PriceNoVat);
        }

        public override List<TransferSummary> GetList()
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT t.ID, t.Number, t.NumberStr, t.DateTime, t.Status, t.Flags, t.Source, s.Name as SourceName, t.Destination ,d.Name as DestinationName, "+
                    "(select SUM(ti.BuyPriceNoVat * ti.Amount) from TransferItems ti where ti.Transfer = t.ID) as PriceNoVat, t.Valid "+
                    "FROM Transfers t, Stores s, Stores d "+
                    "WHERE t.Valid=1 AND s.ID=t.Source AND d.ID=t.Destination";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<TransferSummary> result = new List<TransferSummary>();
                TransferSummary lps = null;
                Dictionary<long, TransferSummary> summaries = new Dictionary<long, TransferSummary>();
                while (reader.Read())
                {
                    TransferSummary ps  = Interprete(reader, cols);
                    result.Add(ps);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<TransferSummary>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

    }
}
