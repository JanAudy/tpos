using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class BillItemMarkDao : AbstractTranswareDao<BillItemMark>
    {
        public BillItemMarkDao(SqlConnection connection) : base(connection, "BillItemMarks") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref BillItemMark obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Item = Interprete(reader, cols, "Item", obj.Item);
            obj.DifferentialMark = Interprete(reader, cols, "DifferentialMark", obj.DifferentialMark);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(BillItemMark obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Item", obj.Item);
            AddParam(parameters, "DifferentialMark", obj.DifferentialMark);
            AddParam(parameters, "Value", obj.Value);
        }
    }
}
