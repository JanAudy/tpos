﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class SettlementTypeLangDao : AbstractTranswareDao<SettlementTypeLang>
    {
        public SettlementTypeLangDao(SqlConnection connection)
            : base(connection, "SettlementTypeNameLangs") { }

        public Dictionary<ELanguage, SettlementTypeLang> GetByParent(long parentID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " WHERE SettlementType=" + parentID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                Dictionary<ELanguage, SettlementTypeLang> result = new Dictionary<ELanguage, SettlementTypeLang>();
                while (reader.Read())
                {
                    SettlementTypeLang lang = Interprete(reader, cols);
                    result.Add(lang.Language, lang);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new Dictionary<ELanguage, SettlementTypeLang>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override bool SaveOrUpdate(SettlementTypeLang obj)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "Update " + tableName + " Set ";
                comm.CommandText += "Language=@language, SettlementType=@unit, Value=@value";
                comm.CommandText += " WHERE SettlementType=" + obj.SettlementTypeID + " AND Language=" + (int)obj.Language;
                comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                comm.Parameters.Add(new SqlParameter("unit", obj.SettlementTypeID));
                comm.Parameters.Add(new SqlParameter("value", obj.Value));

                int count = comm.ExecuteNonQuery();

                if (count == 0)
                {
                    comm.Dispose();
                    comm = connection.CreateCommand();
                    comm.CommandText = "INSERT INTO " + tableName + "(SettlementType, Language, Value)";
                    comm.CommandText += " Values(@unit, @language, @value)";
                    comm.Parameters.Add(new SqlParameter("unit", obj.SettlementTypeID));
                    comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                    comm.Parameters.Add(new SqlParameter("value", obj.Value));
                    count = comm.ExecuteNonQuery();
                }

                bool result = count == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override SettlementTypeLang Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            SettlementTypeLang obj = new SettlementTypeLang();
            obj.Language = (ELanguage)Interprete(reader, cols, "Language", (int)obj.Language);
            obj.SettlementTypeID = Interprete(reader, cols, "SettlementType", obj.SettlementTypeID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            return obj;
        }

    }
}
