using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class AdvSettlementDao : AbstractTranswareDao<AdvSettlement>
    {
        public AdvSettlementDao(SqlConnection connection) : base(connection, "AdvSettlements") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref AdvSettlement obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Date = Interprete(reader, cols, "Date", obj.Date);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.Invoice = Interprete(reader, cols, "Invoice", obj.Invoice);
        }

        public override void FillParams(AdvSettlement obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Date", obj.Date);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "Invoice", obj.Invoice);
        }
    }
}
