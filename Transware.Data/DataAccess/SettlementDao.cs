using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class SettlementDao : AbstractTranswareDao<Settlement>
    {
        public SettlementDao(SqlConnection connection) : base(connection, "Settlements") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Settlement obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Date = Interprete(reader, cols, "Date", obj.Date);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.Invoice = Interprete(reader, cols, "Invoice", obj.Invoice);
        }

        public override void FillParams(Settlement obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Date", obj.Date);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "Invoice", obj.Invoice);
        }
    }
}
