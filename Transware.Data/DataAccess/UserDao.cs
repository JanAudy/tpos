﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class UserDao : AbstractIDLongDao<User>
    {
        protected override string IDColumnName
        {
            get
            {
                return "UserID";
            }
        }

        public UserDao(SqlConnection connection) : base(connection, "Users") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref User obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.CoreID = Interprete(reader, cols, "CoreID", obj.CoreID);
            obj.UserName = Interprete(reader, cols, "UserName", obj.UserName);
            obj.Password = Interprete(reader, cols, "Password", obj.Password);
            obj.Active = Interprete(reader, cols, "Active", obj.Active);
        }

        public override void FillParams(User obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "CoreID", obj.CoreID);
            AddParam(parameters, "UserName", obj.UserName);
            AddParam(parameters, "Password", obj.Password);
            AddParam(parameters, "Active", obj.Active);
        }
    }
}
