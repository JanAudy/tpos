using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CategoryDao : AbstractTranswareDao<Category>
    {
        public CategoryDao(SqlConnection connection) : base(connection, "Categories") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Category obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.ParentID = Interprete(reader, cols, "Parent", obj.ParentID);
            obj.Position = Interprete(reader, cols, "Position", obj.Position);
            obj.Image = Interprete(reader, cols, "Image", obj.Image);
            obj.Internet = Interprete(reader, cols, "Internet", obj.Internet);
            obj.Type = (ECategoryType)Interprete(reader, cols, "Type", (int)obj.Type);
        }

        public override void FillParams(Category obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "Parent", obj.ParentID);
            AddParam(parameters, "Position", obj.Position);
            AddParam(parameters, "Image", obj.Image);
            AddParam(parameters, "Internet", obj.Internet);
            AddParam(parameters, "Type", (int)obj.Type);
        }
    }
}
