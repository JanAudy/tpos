using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class DifferentialMarkDao : AbstractTranswareDao<DifferentialMark>
    {
        public DifferentialMarkDao(SqlConnection connection) : base(connection, "DifferentialMarks") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref DifferentialMark obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Shortcut = Interprete(reader, cols, "Shortcut", obj.Shortcut);
        }

        public override void FillParams(DifferentialMark obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Shortcut", obj.Shortcut);
        }
    }
}
