using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class UnitDao : AbstractTranswareDao<Unit>
    {
        public UnitDao(SqlConnection connection) : base(connection, "Units") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Unit obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Shortcut = Interprete(reader, cols, "Shortcut", obj.Shortcut);
            obj.Multiply = Interprete(reader, cols, "Multiply", obj.Multiply);
        }

        public override void FillParams(Unit obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Shortcut", obj.Shortcut);
            AddParam(parameters, "Multiply", obj.Multiply);
        }
    }
}
