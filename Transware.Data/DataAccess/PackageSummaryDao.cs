using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class PackageSummaryDao : AbstractTranswareDao<PackageSummary>
    {
        public PackageSummaryDao(SqlConnection connection) : base(connection, "Packages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PackageSummary obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ProductID = Interprete(reader, cols, "ProductID", obj.ProductID);
            obj.Product = Interprete(reader, cols, "Product", obj.Product);
            obj.VariantID = Interprete(reader, cols, "VariantID", obj.VariantID);
            obj.Variant = Interprete(reader, cols, "Variant", obj.Variant);
            obj.TypeID = Interprete(reader, cols, "TypeID", obj.TypeID);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.TypeAmount = Interprete(reader, cols, "TypeAmount", obj.TypeAmount);
            obj.Ean = Interprete(reader, cols, "Ean", obj.Ean);
            obj.Vat = Interprete(reader, cols, "Vat", obj.Vat);
            obj.Unit = Interprete(reader, cols, "Unit", obj.Unit);
            obj.Multiply = Interprete(reader, cols, "Multiply", obj.Multiply);
            obj.PriceNoVat = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
            obj.Discount = Interprete(reader, cols, "Discount", obj.Discount);
        }

        public override void FillParams(PackageSummary obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ProductID", obj.ProductID);
            AddParam(parameters, "Product", obj.Product);
            AddParam(parameters, "VariantID", obj.VariantID);
            AddParam(parameters, "Variant", obj.Variant);
            AddParam(parameters, "TypeID", obj.TypeID);
            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "TypeAmount", obj.TypeAmount);
            AddParam(parameters, "Ean", obj.Ean);
            AddParam(parameters, "Vat", obj.Vat);
            AddParam(parameters, "Unit", obj.Unit);
            AddParam(parameters, "Multiply", obj.Multiply);
            AddParam(parameters, "PriceNoVat", obj.PriceNoVat);
            AddParam(parameters, "Discount", obj.Discount);
        }

        public virtual List<PackageSummary> GetList(long categoryID, long storeID, string ean = null)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT pkg.ID, pkg.Product as ProductID, p.Name as Product, pkg.Variant as VariantID, pv.OrderMark as Variant, pkg.Type as TypeID, pt.Name as Type, pt.Amount as TypeAmount, pkg.Ean, v.Tax as Vat, u.Shortcut as Unit, u.Multiply as Multiply, sp.PriceNoVat, d.Discount as Discount, pkg.Valid " +
                    "FROM Packages pkg "+
                    "LEFT JOIN PackageTypes pt on pkg.Type=pt.ID "+
                    "LEFT OUTER JOIN Products p on pkg.Product=p.ID AND p.Valid=1 "+
                    "LEFT OUTER JOIN ProductVariant pv on pkg.Variant=pv.ID AND pv.Valid=1 "+
                    "LEFT OUTER JOIN Vats v on p.Vat=v.ID "+
                    "LEFT OUTER JOIN Units u on p.Unit=u.ID "+
                    "LEFT OUTER JOIN StorePackages sp on pkg.ID=sp.Package AND sp.Store="+storeID.ToString()+" "+
                    "LEFT OUTER JOIN IndividualDiscounts d on sp.IndividualDiscount=d.ID " +
                    "WHERE pkg.Valid=1 AND p.Category="+categoryID;
                if (!string.IsNullOrEmpty(ean))
                    comm.CommandText = " AND pkg.Ean='" + ean + "'";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<PackageSummary> result = new List<PackageSummary>();
                while (reader.Read())
                {
                    PackageSummary ps = Interprete(reader, cols);
                    result.Add(ps);                        
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<PackageSummary>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual List<PackageSummary> GetList(long storeID, string ean = null)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT pkg.ID, pkg.Product as ProductID, p.Name as Product, pkg.Variant as VariantID, pv.OrderMark as Variant, pkg.Type as TypeID, pt.Name as Type, pt.Amount as TypeAmount, pkg.Ean, v.Tax as Vat, u.Shortcut as Unit, u.Multiply as Multiply, sp.PriceNoVat, d.Discount as Discount, pkg.Valid " +
                    "FROM Packages pkg " +
                    "LEFT JOIN PackageTypes pt on pkg.Type=pt.ID " +
                    "LEFT OUTER JOIN Products p on pkg.Product=p.ID " +
                    "LEFT OUTER JOIN ProductVariant pv on pkg.Variant=pv.ID " +
                    "LEFT OUTER JOIN Vats v on p.Vat=v.ID " +
                    "LEFT OUTER JOIN Units u on p.Unit=u.ID " +
                    "LEFT OUTER JOIN StorePackages sp on pkg.ID=sp.Package AND sp.Store=" + storeID.ToString() + " " +
                    "LEFT OUTER JOIN IndividualDiscounts d on sp.IndividualDiscount=d.ID " +
                    "WHERE pkg.Valid=1";
                if (!string.IsNullOrEmpty(ean))
                    comm.CommandText += " AND pkg.Ean='" + ean + "'";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<PackageSummary> result = new List<PackageSummary>();
                while (reader.Read())
                {
                    PackageSummary ps = Interprete(reader, cols);
                    result.Add(ps);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<PackageSummary>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }


        public virtual PackageSummary Get(long packageID, long storeID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT pkg.ID, pkg.Product as ProductID, p.Name as Product, pkg.Variant as VariantID, pv.OrderMark as Variant, pkg.Type as TypeID, pt.Name as Type, pt.Amount as TypeAmount, pkg.Ean, v.Tax as Vat, u.Shortcut as Unit, u.Multiply as Multiply, sp.PriceNoVat, d.Discount as Discount, pkg.Valid " +
                    "FROM Packages pkg " +
                    "LEFT JOIN PackageTypes pt on pkg.Type=pt.ID " +
                    "LEFT OUTER JOIN Products p on pkg.Product=p.ID " +
                    "LEFT OUTER JOIN ProductVariant pv on pkg.Variant=pv.ID " +
                    "LEFT OUTER JOIN Vats v on p.Vat=v.ID " +
                    "LEFT OUTER JOIN Units u on p.Unit=u.ID " +
                    "LEFT OUTER JOIN StorePackages sp on pkg.ID=sp.Package AND sp.Store=" + storeID.ToString() + " " +
                    "LEFT OUTER JOIN IndividualDiscounts d on sp.IndividualDiscount=d.ID " +
                    "WHERE pkg.Valid=1 AND pkg.ID=" + packageID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<PackageSummary> result = new List<PackageSummary>();
                if (reader.Read())
                {
                    PackageSummary ps = Interprete(reader, cols);
                    result.Add(ps);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result[0];
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
