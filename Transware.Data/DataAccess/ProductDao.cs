using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductDao : AbstractTranswareDao<Product>
    {
        public ProductDao(SqlConnection connection) : base(connection, "Products") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Product obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.ShortName = Interprete(reader, cols, "ShortName", obj.ShortName);
            obj.CategoryID = Interprete(reader, cols, "Category", obj.CategoryID);
            obj.UnitID = Interprete(reader, cols, "Unit", obj.UnitID);
            obj.VatID = Interprete(reader, cols, "Vat", obj.VatID);
            obj.CTaxID = Interprete(reader, cols, "CTax", obj.CTaxID);
            obj.TimePeriodID = Interprete(reader, cols, "TimePeriod", obj.TimePeriodID);
            obj.PlaceNo = Interprete(reader, cols, "PlaceNo", obj.PlaceNo);
            obj.Specification = Interprete(reader, cols, "Specification", obj.Specification);
            obj.PriceCalculationID = Interprete(reader, cols, "PriceCalculation", obj.PriceCalculationID); 
            obj.ProviderID = Interprete(reader, cols, "Provider", obj.ProviderID); 
            obj.Number = Interprete(reader, cols, "Number", obj.Number); 
            obj.Note = Interprete(reader, cols, "Note", obj.Note); 
            obj.Image = Interprete(reader, cols, "Image", obj.Image); 
            obj.OrderMark = Interprete(reader, cols, "OrderMark", obj.OrderMark); 
            obj.SettingID = Interprete(reader, cols, "Setting", obj.SettingID); 
            obj.UserID1 = Interprete(reader, cols, "UserID1", obj.UserID1);
            obj.UserID2 = Interprete(reader, cols, "UserID2", obj.UserID2);
            obj.ProductGroupID = Interprete(reader, cols, "ProductGroup", obj.ProductGroupID);
            obj.DetailedSpecification = Interprete(reader, cols, "Detailedspecification", obj.DetailedSpecification);
        }

        public override void FillParams(Product obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "ShortName", obj.ShortName);
            AddParam(parameters, "Category", obj.CategoryID);
            AddParam(parameters, "Unit", obj.UnitID);
            AddParam(parameters, "Vat", obj.VatID);
            AddParam(parameters, "CTax", obj.CTaxID);
            AddParam(parameters, "TimePeriod", obj.TimePeriodID);
            AddParam(parameters, "PlaceNo", obj.PlaceNo);
            AddParam(parameters, "Specification", obj.Specification);
            AddParam(parameters, "PriceCalculation", (obj.PriceCalculationID.HasValue)?obj.PriceCalculationID:0);
            AddParam(parameters, "Provider", (obj.ProviderID==0)?null:obj.ProviderID);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "Note", obj.Note);
            AddParam(parameters, "Image", obj.Image);
            AddParam(parameters, "OrderMark", obj.OrderMark);
            AddParam(parameters, "Setting", obj.SettingID);
            AddParam(parameters, "UserID1", obj.UserID1);
            AddParam(parameters, "UserID2", obj.UserID2);
            AddParam(parameters, "ProductGroup", obj.ProductGroupID);
            AddParam(parameters, "Detailedspecification", obj.DetailedSpecification);
        }

    }
}
