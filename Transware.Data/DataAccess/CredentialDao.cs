using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CredentialDao : AbstractTranswareDao<Credential>
    {
        public CredentialDao(SqlConnection connection) : base(connection, "Credentials") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Credential obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.CoreID = Interprete(reader, cols, "CoreID", obj.CoreID);
            obj.Active = Interprete(reader, cols, "Active", obj.Active);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.UserName = Interprete(reader, cols, "UserName", obj.UserName);
            obj.Password = Interprete(reader, cols, "Password", obj.Password);
        }

        public override void FillParams(Credential obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "CoreID", obj.CoreID);
            AddParam(parameters, "Active", obj.Active);
            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "UserName", obj.UserName);
            AddParam(parameters, "Password", obj.Password);
        }
    }
}
