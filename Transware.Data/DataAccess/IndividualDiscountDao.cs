using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class IndividualDiscountDao : AbstractTranswareDao<IndividualDiscount>
    {
        public IndividualDiscountDao(SqlConnection connection) : base(connection, "IndividualDiscounts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref IndividualDiscount obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Discount = Interprete(reader, cols, "Discount", obj.Discount);
        }

        public override void FillParams(IndividualDiscount obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Discount", obj.Discount);
        }
    }
}
