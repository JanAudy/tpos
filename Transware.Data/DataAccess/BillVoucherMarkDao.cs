using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class BillVoucherMarkDao : AbstractTranswareDao<BillVoucherMark>
    {
        public BillVoucherMarkDao(SqlConnection connection) : base(connection, "BillVoucherMarks") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref BillVoucherMark obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.BillVoucher = Interprete(reader, cols, "BillVoucher", obj.BillVoucher);
            obj.Mark = Interprete(reader, cols, "Mark", obj.Mark);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(BillVoucherMark obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "BillVoucher", obj.BillVoucher);
            AddParam(parameters, "Mark", obj.Mark);
            AddParam(parameters, "Value", obj.Value);
        }
    }
}
