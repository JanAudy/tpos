﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class UsedDifferentialMarksDao
    {
        protected SqlConnection connection = null;

        protected string tableName = "UsedDifferentialMarks";
        protected string masterColumn = "ProductID";
        protected string slaveColumn = "DMarkID";
        protected string slaveTable = "DifferentialMarks";
        protected AbstractTranswareDao<DifferentialMark> slaveDao;


        public UsedDifferentialMarksDao(SqlConnection connection, AbstractTranswareDao<DifferentialMark> slaveDao)
        {
            this.connection = connection;
            this.slaveDao = slaveDao;
        }

        public virtual List<DifferentialMark> GetList(long masterID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + slaveTable + " WHERE ID in (SELECT "+slaveColumn+" FROM "+tableName+" WHERE "+masterColumn+"="+masterID.ToString()+")";
                
                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<DifferentialMark> result = new List<DifferentialMark>();
                while (reader.Read())
                {
                    result.Add(slaveDao.Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<DifferentialMark>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual HashSet<DifferentialMark> GetSet(long masterID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + slaveTable + " WHERE ID in (SELECT " + slaveColumn + " FROM " + tableName + " WHERE " + masterColumn + "=" + masterID.ToString() + ")";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                HashSet<DifferentialMark> result = new HashSet<DifferentialMark>();
                while (reader.Read())
                {
                    result.Add(slaveDao.Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new HashSet<DifferentialMark>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }


        internal bool Update(long masterID, HashSet<DifferentialMark> items)
        {
            HashSet<DifferentialMark> actual = GetSet(masterID);
            bool res = true;
            foreach (DifferentialMark dm in items)
            {
                if (!actual.Contains(dm))
                    res &= Save(masterID, dm.ID);
                else
                    actual.Remove(dm);

                if (!res) break;
            }

            foreach (DifferentialMark dm in actual.ToArray())
            {
                res &= Delete(masterID, dm.ID);
                
                if (!res) break;
            }
            return res;
        }

        internal bool Save(long masterID, long slaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO "+tableName+"("+masterColumn+", "+slaveColumn+") Values (@master, @slave)";
                comm.Parameters.Add(new SqlParameter("master", masterID));
                comm.Parameters.Add(new SqlParameter("slave", slaveID));
                return comm.ExecuteNonQuery() == 1;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                
                if (comm != null)
                    comm.Dispose();
            }
        }

        internal bool Delete(long masterID, long slaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "DELETE " + tableName + " WHERE " + masterColumn + "=@master AND  " + slaveColumn + "= @slave";
                comm.Parameters.Add(new SqlParameter("master", masterID));
                comm.Parameters.Add(new SqlParameter("slave", slaveID));
                return comm.ExecuteNonQuery() >= 1;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {

                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
