using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class OrderMarkDao : AbstractTranswareDao<OrderMark>
    {
        public OrderMarkDao(SqlConnection connection) : base(connection, "OrderMarks") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref OrderMark obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Position = Interprete(reader, cols, "Position", obj.Position);
            obj.Length = Interprete(reader, cols, "Length", obj.Length);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(OrderMark obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Position", obj.Position);
            AddParam(parameters, "Length", obj.Length);
            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "Value", obj.Value);
        }
    }
}
