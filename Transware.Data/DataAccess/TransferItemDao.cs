using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class TransferItemDao : AbstractTranswareDao<TransferItem>
    {
        public TransferItemDao(SqlConnection connection) : base(connection, "TransferItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref TransferItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Transfer = Interprete(reader, cols, "Transfer", obj.Transfer);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            
        }

        public override void FillParams(TransferItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Transfer", obj.Transfer);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
