using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PurchaseWarrantyItemDao : AbstractTranswareDao<PurchaseWarrantyItem>
    {
        public PurchaseWarrantyItemDao(SqlConnection connection) : base(connection, "PurchaseWarrantyItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PurchaseWarrantyItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.PurchaseWarranty = Interprete(reader, cols, "PurchaseWarranty", obj.PurchaseWarranty);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(PurchaseWarrantyItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "PurchaseWarranty", obj.PurchaseWarranty);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
