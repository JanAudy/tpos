﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Microdata.Data;
using Transware.Data.Enums;

namespace Transware.Data.DataAccess
{
    public class StoreTransactionDao : AbstractIDLongDao<StoreTransaction>
    {
        public StoreTransactionDao(SqlConnection connection) : base(connection, "StoreTransactions") { }

        public bool ProcessTransaction(List<StoreTransaction> transactions)
        {
            SqlDataReader reader = null;
            SqlTransaction t = null;
            try
            {
                t = connection.BeginTransaction(System.Data.IsolationLevel.Serializable);

                int i = 0;
                foreach (StoreTransaction st in transactions)
                {
                    if (!ProcessCommands(connection, t, st))
                        throw new Exception("Wrong sqls");
                    i++;
                }
                
                t.Commit();
            }
            catch (Exception e)
            {
                t.Rollback();
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                //if (comm != null)
                //    comm.Dispose();
            }
            return true;
        }

        private bool ProcessCommands(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            if (st.MovementType == EMovementType.Reservation)
                if (!st.Inverse)
                    return ProcessReservation(connection, t, st);
                else
                    return ProcessReservationInverse(connection, t, st);

            if (st.MovementType == EMovementType.Release)
                if (!st.Inverse)
                    return ProcessRelease(connection, t, st);
                else
                    return ProcessReleaseInverse(connection, t, st);

            if (st.MovementType == EMovementType.Income)
                return ProcessIncome(connection, t, st);


            if (st.MovementType == EMovementType.IncomeLocked)
                if (!st.Inverse)
                    return ProcessIncomeLock(connection, t, st);
                else
                    return ProcessIncomeLockInverse(connection, t, st);

            if (st.MovementType == EMovementType.Unlock)
                return ProcessUnlock(connection, t, st);

            return false;
        }


        private bool ProcessReservation(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved-@Amount, RealReserved=RealReserved+@Amount WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved-@Amount, RealReserved=RealReserved+@Amount WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                if (comm.ExecuteNonQuery() != 1)
                    return false;
            }

            return true;
        }

        private bool ProcessReservationInverse(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealReserved=RealReserved-@Amount WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealReserved=RealReserved-@Amount WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                if (comm.ExecuteNonQuery() != 1)
                    return false;
            }

            return true;
        }

        
        private bool ProcessRelease(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm;
            
            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved-@Amount, RealTotal=RealTotal-@Amount, LastRelease=@DateTime WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            SqlDataReader reader = comm.ExecuteReader();
            decimal inStore=0, purchasePrice=0;
            if (reader.Read())
            {
                inStore = (decimal)reader[0];
                purchasePrice = (decimal)reader[1];
                reader.Close();
            }
            else
                return false;                

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "INSERT INTO ProductMovements(Product, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                " VALUES (@Product, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
            comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
            comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", purchasePrice));
            comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("InStore", inStore));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            if (st.UpdatePurchasePricesOnRelease && st.Variant == 0)
            {
                string tableName = "";
                if (st.RecordType == ERecordType.Bill)
                    tableName = "BillItems";
                if (st.RecordType == ERecordType.DeliveryNote)
                    tableName = "DeliveryNoteItems";
                if (st.RecordType == ERecordType.Invoice)
                    tableName = "InvoiceItems";
                if (!string.IsNullOrEmpty(tableName))
                {
                    comm = new SqlCommand("", connection, t);
                    comm.CommandText = "UPDATE " + tableName + " Set UnitBuyPriceNoVat=@Price WHERE ID=@Id";
                    comm.Parameters.Add(new SqlParameter("Price", purchasePrice));
                    comm.Parameters.Add(new SqlParameter("Id", st.RecordItemID));
                    if (comm.ExecuteNonQuery() != 1)
                        return false;
                }
            }

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved-@Amount, RealTotal=RealTotal-@Amount, LastRelease=@DateTime WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                reader = comm.ExecuteReader();
                
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    purchasePrice = (decimal)reader[1];
                    reader.Close();
                }
                else
                    return false;
                
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "INSERT INTO ProductMovements(Variant, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                    " VALUES (@Variant, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
                comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
                comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", purchasePrice));
                comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("InStore", inStore));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                if (st.UpdatePurchasePricesOnRelease)
                {
                    string tableName = "";
                    if (st.RecordType == ERecordType.Bill)
                        tableName = "BillItems";
                    if (st.RecordType == ERecordType.DeliveryNote)
                        tableName = "DeliveryNoteItems";
                    if (st.RecordType == ERecordType.Invoice)
                        tableName = "InvoiceItems";
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        comm = new SqlCommand("", connection, t);
                        comm.CommandText = "UPDATE " + tableName + " SET UnitBuyPriceNoVat=@Price WHERE ID=@Id";
                        comm.Parameters.Add(new SqlParameter("Price", purchasePrice));
                        comm.Parameters.Add(new SqlParameter("Id", st.RecordItemID));
                        if (comm.ExecuteNonQuery() != 1)
                            return false;
                    }
                }
            }

            return true;
        }

        private bool ProcessReleaseInverse(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            if (st.MakeMovementsInvalid)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Product=@Product AND Valid=1";
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("Product", st.Product));
                comm.ExecuteNonQuery();
            }

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                if (st.MakeMovementsInvalid)
                {
                    comm = new SqlCommand("", connection, t);
                    comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Variant=@Variant AND Valid=1";
                    comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                    comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                    comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                    comm.ExecuteNonQuery();
                }
            }

            return true;
        }


        private bool ProcessIncome(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            SqlDataReader reader = comm.ExecuteReader();
            decimal inStore=0, purchasePrice=0;
            if (reader.Read())
            {
                inStore = (decimal)reader[0];
                purchasePrice = (decimal)reader[1];
                reader.Close();
            }
            else
                return false;

            decimal storePrice = inStore * purchasePrice;
            storePrice += st.Amount * st.PurchasePriceNoVat;
            if ((st.Amount + inStore)!=0)
                storePrice /= st.Amount + inStore;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount, LastIncome=@DateTime, AveragePurchaseUnitPriceNoVat=@PurchasePrice WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", storePrice));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            inStore += st.Amount;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "INSERT INTO ProductMovements(Product, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                " VALUES (@Product, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
            comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
            comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
            comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("InStore", inStore));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    purchasePrice = (decimal)reader[1];
                    reader.Close();
                }
                else
                    return false;

                storePrice = inStore * purchasePrice;
                storePrice += st.Amount * st.PurchasePriceNoVat;
                if ((st.Amount + inStore) != 0)
                    storePrice /= st.Amount + inStore;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount, LastIncome=@DateTime, AveragePurchaseUnitPriceNoVat=@PurchasePrice WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", storePrice));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                inStore += st.Amount;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "INSERT INTO ProductMovements(Variant, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                    " VALUES (@Variant, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
                comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
                comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
                comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("InStore", inStore));
                if (comm.ExecuteNonQuery() != 1)
                    return false;
            }

            return true;
        }
                
        private bool ProcessIncomeLock(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealLocked=RealLocked+@Amount WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            SqlDataReader reader = comm.ExecuteReader();
            decimal inStore=0, purchasePrice=0;
            if (reader.Read())
            {
                inStore = (decimal)reader[0];
                purchasePrice = (decimal)reader[1];
                reader.Close();
            }
            else
                return false;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "INSERT INTO ProductMovements(Product, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                " VALUES (@Product, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
            comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
            comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
            comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("InStore", inStore));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            
            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealLocked=RealLocked+@Amount WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    purchasePrice = (decimal)reader[1];
                    reader.Close();
                }
                else
                    return false;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "INSERT INTO ProductMovements(Variant, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                    " VALUES (@Variant, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
                comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
                comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
                comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("InStore", inStore));
                if (comm.ExecuteNonQuery() != 1)
                    return false;
            }

            return true;
        }

        private bool ProcessIncomeLockInverse(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealLocked=RealLocked-@Amount WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Product=@Product AND Valid=1";
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.ExecuteNonQuery();

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealLocked=RealLocked-@Amount WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Variant=@Variant AND Valid=1";
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.ExecuteNonQuery();
            }

            return true;
        }


        private bool ProcessUnlock(SqlConnection connection, SqlTransaction t, StoreTransaction st)
        {
            SqlCommand comm;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            SqlDataReader reader = comm.ExecuteReader();
            decimal inStore=0, purchasePrice=0;
            if (reader.Read())
            {
                inStore = (decimal)reader[0];
                purchasePrice = (decimal)reader[1];
                reader.Close();
            }
            else
                return false;

            
            decimal storePrice = inStore * purchasePrice;
            
            storePrice += st.Amount * st.PurchasePriceNoVat;
            if ((st.Amount + inStore) != 0)
            {
                if (purchasePrice == 0 && st.Amount!=0)
                    storePrice /= st.Amount;
                else
                    storePrice /= st.Amount + inStore;
            }


            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount, RealLocked=RealLocked-@Amount, LastIncome=@DateTime, AveragePurchaseUnitPriceNoVat=@PurchasePrice WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", storePrice));
            if (comm.ExecuteNonQuery() != 1)
                return false;

            inStore += st.Amount;

            comm = new SqlCommand("", connection, t);
            comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Product=@Product AND Valid=1";
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.ExecuteNonQuery();
            
            comm = new SqlCommand("", connection, t);
            comm.CommandText = "INSERT INTO ProductMovements(Product, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                " VALUES (@Product, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
            comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
            comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
            comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
            comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
            comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
            comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
            comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
            comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
            comm.Parameters.Add(new SqlParameter("InStore", inStore));
            if (comm.ExecuteNonQuery() != 1)
                return false;


            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "SELECT RealTotal, AveragePurchaseUnitPriceNoVat FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                reader = comm.ExecuteReader();
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    purchasePrice = (decimal)reader[1];
                    reader.Close();
                }
                else
                    return false;

                storePrice = inStore * purchasePrice;
                storePrice += st.Amount * st.PurchasePriceNoVat;
                if ((st.Amount + inStore) != 0)
                    storePrice /= st.Amount + inStore;


                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE StoreProducts Set RealNoReserved=RealNoReserved+@Amount, RealTotal=RealTotal+@Amount, RealLocked=RealLocked-@Amount, LastIncome=@DateTime, AveragePurchaseUnitPriceNoVat=@PurchasePrice WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", storePrice));
                if (comm.ExecuteNonQuery() != 1)
                    return false;

                inStore += st.Amount;
                
                comm = new SqlCommand("", connection, t);
                comm.CommandText = "UPDATE ProductMovements SET Valid = 0 WHERE DocumentType=@DocumentType AND DocumentID=@DocumentID AND Variant=@Variant AND Valid=1";
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.ExecuteNonQuery();

                comm = new SqlCommand("", connection, t);
                comm.CommandText = "INSERT INTO ProductMovements(Variant, Store, DateTime, DocumentType, MovementType, Number, NumberStr, DocumentID, PurchasePrice, SellPrice, Amount, InStore, Valid)" +
                    " VALUES (@Variant, @Store, @DateTime, @DocumentType, @MovementType, @Number, @NumberStr, @DocumentID, @PurchasePrice, @SellPrice, @Amount, @InStore, 1)";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                comm.Parameters.Add(new SqlParameter("DateTime", st.DateTime));
                comm.Parameters.Add(new SqlParameter("DocumentType", st.RecordType));
                comm.Parameters.Add(new SqlParameter("MovementType", st.MovementType));
                comm.Parameters.Add(new SqlParameter("Number", st.RecordNumber));
                comm.Parameters.Add(new SqlParameter("NumberStr", st.RecordNumberStr));
                comm.Parameters.Add(new SqlParameter("DocumentID", st.RecordID));
                comm.Parameters.Add(new SqlParameter("PurchasePrice", st.PurchasePriceNoVat));
                comm.Parameters.Add(new SqlParameter("SellPrice", st.SellPriceNoVat));
                comm.Parameters.Add(new SqlParameter("Amount", st.Amount));
                comm.Parameters.Add(new SqlParameter("InStore", inStore));
                if (comm.ExecuteNonQuery() != 1)
                    return false;               
            }

            return true;
        }



        public bool IsAvailable(List<StoreTransaction> transactions)
        {
            SqlDataReader reader = null;
            try
            {
                foreach (StoreTransaction st in transactions)
                {
                    if (!IsAvailable(st))
                        return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return true;
        }

        private bool IsAvailable(StoreTransaction st)
        {
            //if (st.MovementType == EMovementType.Reservation || )
            //    return IsAvailableReservation(connection, st);
                
            if (st.MovementType == EMovementType.Release)
                return IsAvailableRelease(connection, st);
                

            return true;
        }

        private bool IsAvailableRelease(SqlConnection connection, StoreTransaction st)
        {
            SqlCommand comm;

            comm = new SqlCommand("", connection);
            comm.CommandText = "SELECT RealTotal, RealNoReserved FROM StoreProducts WHERE Product=@Product AND Store=@Store";
            comm.Parameters.Add(new SqlParameter("Product", st.Product));
            comm.Parameters.Add(new SqlParameter("Store", st.Store));
            SqlDataReader reader = comm.ExecuteReader();
            decimal inStore, inStoreNoReserved;
            if (reader.Read())
            {
                inStore = (decimal)reader[0];
                inStoreNoReserved = (decimal)reader[1];
                reader.Close();
            }
            else
                return false;

            if (st.Amount > inStore || st.Amount > inStoreNoReserved)
                return false;

            

            if (st.Variant != 0)
            {
                comm = new SqlCommand("", connection);
                comm.CommandText = "SELECT RealTotal, RealNoReserved FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                reader = comm.ExecuteReader();

                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    inStoreNoReserved = (decimal)reader[1];
                    reader.Close();
                }
                else
                    return false;

                if (st.Amount > inStore || st.Amount > inStoreNoReserved)
                    return false;
            }

            return true;
        }

        public decimal GetRealTotal(StoreTransaction st)
        {
            SqlCommand comm;

            if (st.Variant == 0)
            {
                comm = new SqlCommand("", connection);
                comm.CommandText = "SELECT RealTotal FROM StoreProducts WHERE Product=@Product AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Product", st.Product));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                SqlDataReader reader = comm.ExecuteReader();
                decimal inStore;
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    reader.Close();
                }
                else
                    return 0;

                return inStore;
            }
            else
            {
                comm = new SqlCommand("", connection);
                comm.CommandText = "SELECT RealTotal FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                SqlDataReader reader = comm.ExecuteReader();
                decimal inStore;
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    reader.Close();
                }
                else
                    return 0;

                return inStore;
            }
        }

        public decimal GetRealNoReserved(StoreTransaction st)
        {
            SqlCommand comm;

            if (st.Variant == 0)
            {
                comm = new SqlCommand("", connection);
                comm.CommandText = "SELECT RealNoReserved FROM StoreProducts WHERE Product=@Product AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Product", st.Product));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                SqlDataReader reader = comm.ExecuteReader();
                decimal inStore;
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    reader.Close();
                }
                else
                    return 0;

                return inStore;
            }
            else
            {
                comm = new SqlCommand("", connection);
                comm.CommandText = "SELECT RealNoReserved FROM StoreProducts WHERE Variant=@Variant AND Store=@Store";
                comm.Parameters.Add(new SqlParameter("Variant", st.Variant));
                comm.Parameters.Add(new SqlParameter("Store", st.Store));
                SqlDataReader reader = comm.ExecuteReader();
                decimal inStore;
                if (reader.Read())
                {
                    inStore = (decimal)reader[0];
                    reader.Close();
                }
                else
                    return 0;

                return inStore;
            }
        }
    }
}
