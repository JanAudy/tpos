using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ReceiptDao : AbstractTranswareDao<Receipt>
    {
        public ReceiptDao(SqlConnection connection) : base(connection, "Receipts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Receipt obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.ExposureDate = Interprete(reader, cols, "ExposureDate", obj.ExposureDate);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.ProviderID = Interprete(reader, cols, "ProviderID", obj.ProviderID);
            obj.InvoiceNumber = Interprete(reader, cols, "InvoiceNumber", obj.InvoiceNumber);
            obj.DeliveryNumber = Interprete(reader, cols, "DeliveryNumber", obj.DeliveryNumber);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);   
            obj.ItemsCurrency = Interprete(reader, cols, "ItemsCurrency", obj.ItemsCurrency);
            obj.ItemsRateUnit = Interprete(reader, cols, "ItemsRateUnit", obj.ItemsRateUnit);
            obj.ItemsRate = Interprete(reader, cols, "ItemsRate", obj.ItemsRate);   
            obj.OtherCurrency = Interprete(reader, cols, "OtherCurrency", obj.OtherCurrency);
            obj.OtherRateUnit = Interprete(reader, cols, "OtherRateUnit", obj.OtherRateUnit);
            obj.OtherRate = Interprete(reader, cols, "OtherRate", obj.OtherRate);   
        }

        public override void FillParams(Receipt obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "ExposureDate", obj.ExposureDate);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "ProviderID", obj.ProviderID);
            AddParam(parameters, "InvoiceNumber", obj.InvoiceNumber);
            AddParam(parameters, "DeliveryNumber", obj.DeliveryNumber);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "ItemsCurrency", obj.ItemsCurrency);
            AddParam(parameters, "ItemsRateUnit", obj.ItemsRateUnit);
            AddParam(parameters, "ItemsRate", obj.ItemsRate);
            AddParam(parameters, "OtherCurrency", obj.OtherCurrency);
            AddParam(parameters, "OtherRateUnit", obj.OtherRateUnit);
            AddParam(parameters, "OtherRate", obj.OtherRate);
        }
    }
}
