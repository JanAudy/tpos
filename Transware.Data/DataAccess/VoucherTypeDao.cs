using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataAccess
{
    public class VoucherTypeDao : AbstractTranswareDao<VoucherType>
    {
        public VoucherTypeDao(SqlConnection connection) : base(connection, "VoucherTypes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref VoucherType obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.AllowReturn = Interprete(reader, cols, "AllowReturn", obj.AllowReturn);
            obj.UseValue = Interprete(reader, cols, "UseValue", obj.UseValue);
        }

        public override void FillParams(VoucherType obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "AllowReturn", obj.AllowReturn);
            AddParam(parameters, "UseValue", obj.UseValue);
        }
    }
}
