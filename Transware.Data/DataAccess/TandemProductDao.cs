using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class TandemProductDao : AbstractTranswareDao<TandemProduct>
    {
        public TandemProductDao(SqlConnection connection) : base(connection, "TandemProducts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref TandemProduct obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.PackageID = Interprete(reader, cols, "Package", obj.PackageID);
            obj.TandemPackageID = Interprete(reader, cols, "TandemPackage", obj.TandemPackageID);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Price = Interprete(reader, cols, "Price", obj.Price);
        }

        public override void FillParams(TandemProduct obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Package", obj.PackageID);
            AddParam(parameters, "TandemPackage", obj.TandemPackageID);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Price", obj.Price);
        }
    }
}
