using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataAccess
{
    public class VersionDao : AbstractTranswareDao<Version>
    {
        public VersionDao(SqlConnection connection) : base(connection, "Versions") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Version obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.DBver = Interprete(reader, cols, "DBver", obj.DBver);
            obj.MinAppVer = Interprete(reader, cols, "MinAppVer", obj.MinAppVer);
            obj.Changed = Interprete(reader, cols, "Changed", obj.Changed);
        }

        public override void FillParams(Version obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "DBver", obj.DBver);
            AddParam(parameters, "MinAppVer", obj.MinAppVer);
            AddParam(parameters, "Changed", obj.Changed);
        }
    }
}
