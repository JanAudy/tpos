using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PackageDao : AbstractTranswareDao<Package>
    {
        public PackageDao(SqlConnection connection) : base(connection, "Packages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Package obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.VariantID = Interprete(reader, cols, "Variant", obj.VariantID);
            obj.TypeID = Interprete(reader, cols, "Type", obj.TypeID);
            obj.Ean = Interprete(reader, cols, "Ean", obj.Ean);
            obj.Weight = Interprete(reader, cols, "Weight", obj.Weight);
            obj.LabelID = Interprete(reader, cols, "Label", obj.LabelID);
            obj.Dividable = Interprete(reader, cols, "Dividable", obj.Dividable);
        }

        public override void FillParams(Package obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Product", obj.ProductID);
            AddParam(parameters, "Variant", obj.VariantID);
            AddParam(parameters, "Type", obj.TypeID);
            AddParam(parameters, "Ean", obj.Ean);
            AddParam(parameters, "Weight", obj.Weight);
            AddParam(parameters, "Label", obj.LabelID);
            AddParam(parameters, "Dividable", obj.Dividable);
        }

    }
}
