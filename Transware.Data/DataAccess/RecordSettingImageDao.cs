﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RecordSettingImageDao : AbstractIDLongDao<RecordSettingImage>
    {
        public RecordSettingImageDao(SqlConnection connection) : base(connection, "RecordSettingImages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RecordSettingImage obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(RecordSettingImage obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Value", obj.Value);
        }
    }
}
