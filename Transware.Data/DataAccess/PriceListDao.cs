using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PriceListDao : AbstractTranswareDao<PriceList>
    {
        public PriceListDao(SqlConnection connection) : base(connection, "PriceList") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PriceList obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Coefficient = Interprete(reader, cols, "Coefficient", obj.Coefficient);
            obj.Addition = Interprete(reader, cols, "Addition", obj.Addition);
            obj.UseGroups = Interprete(reader, cols, "UseGroups", obj.UseGroups);
            obj.RoundingPrices = Interprete(reader, cols, "RoundingPrices", obj.RoundingPrices);
            obj.Rounding = Interprete(reader, cols, "Rounding", obj.Rounding);
            obj.Places = Interprete(reader, cols, "Places", obj.Places);
        }

        public override void FillParams(PriceList obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Coefficient", obj.Coefficient);
            AddParam(parameters, "Addition", obj.Addition);
            AddParam(parameters, "UseGroups", obj.UseGroups);
            AddParam(parameters, "RoundingPrices", obj.RoundingPrices);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "Places", obj.Places);
        }
 
    }
}
