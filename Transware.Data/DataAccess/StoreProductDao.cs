﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class StoreProductDao : AbstractIDLongDao<StoreProduct>
    {
        public StoreProductDao(SqlConnection connection) : base(connection, "StoreProducts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref StoreProduct obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.StoreID = Interprete(reader, cols, "Store", obj.StoreID);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.VariantID = Interprete(reader, cols, "Variant", obj.VariantID);
            obj.PlaceDesc = Interprete(reader, cols, "PlaceDesc", obj.PlaceDesc);
            obj.PositionID = Interprete(reader, cols, "Position", obj.PositionID);
            obj.RequestedMin = Interprete(reader, cols, "RequestedMin", obj.RequestedMin);
            obj.RequestedMax = Interprete(reader, cols, "RequestedMax", obj.RequestedMax);
            obj.RealNoReserved = Interprete(reader, cols, "RealNoReserved", obj.RealNoReserved);
            obj.RealReserved = Interprete(reader, cols, "RealReserved", obj.RealReserved);
            obj.RealTotal = Interprete(reader, cols, "RealTotal", obj.RealTotal);
            obj.RealLocked = Interprete(reader, cols, "RealLocked", obj.RealLocked);
            obj.LastIncome = Interprete(reader, cols, "LastIncome", obj.LastIncome);
            obj.LastRelease = Interprete(reader, cols, "LastRelease", obj.LastRelease);
            obj.LastChange = Interprete(reader, cols, "LastChange", obj.LastChange);
            obj.AveragePurchaseUnitPriceNoVat = Interprete(reader, cols, "AveragePurchaseUnitPriceNoVat", obj.AveragePurchaseUnitPriceNoVat);
        }

        public override void FillParams(StoreProduct obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.StoreID);
            AddParam(parameters, "Product", obj.ProductID);
            AddParam(parameters, "Variant", obj.VariantID);
            AddParam(parameters, "PlaceDesc", obj.PlaceDesc);
            AddParam(parameters, "Position", obj.PositionID);
            AddParam(parameters, "RequestedMin", obj.RequestedMin);
            AddParam(parameters, "RequestedMax", obj.RequestedMax);
            AddParam(parameters, "RealNoReserved", obj.RealNoReserved);
            AddParam(parameters, "RealReserved", obj.RealReserved);
            AddParam(parameters, "RealTotal", obj.RealTotal);
            AddParam(parameters, "RealLocked", obj.RealLocked);
            AddParam(parameters, "LastIncome", obj.LastIncome);
            AddParam(parameters, "LastRelease", obj.LastRelease);
            AddParam(parameters, "LastChange", obj.LastChange);
            AddParam(parameters, "AveragePurchaseUnitPriceNoVat", obj.AveragePurchaseUnitPriceNoVat);
        }
    }
}
