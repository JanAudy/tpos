using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PaymentTypeDao : AbstractTranswareDao<PaymentType>
    {
        public PaymentTypeDao(SqlConnection connection) : base(connection, "PaymentType") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PaymentType obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Rounding = Interprete(reader, cols, "Rounding", obj.Rounding);
            obj.NoOfBills = Interprete(reader, cols, "NoOfBills", obj.NoOfBills);
            obj.Rate = Interprete(reader, cols, "Rate", obj.Rate);
            obj.IsCash = Interprete(reader, cols, "IsCash", obj.IsCash);
            obj.Fee = Interprete(reader, cols, "Fee", obj.Fee);
        }

        public override void FillParams(PaymentType obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "NoOfBills", obj.NoOfBills);
            AddParam(parameters, "Rate", obj.Rate);
            AddParam(parameters, "IsCash", obj.IsCash);
            AddParam(parameters, "Fee", obj.Fee);
        }
    }
}
