using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class BillDao : AbstractTranswareDao<Bill>
    {
        public BillDao(SqlConnection connection) : base(connection, "Bills") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Bill obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.SupplierID = Interprete(reader, cols, "SupplierID", obj.SupplierID);
            obj.SupplierName = Interprete(reader, cols, "SupplierName", obj.SupplierName);
            obj.SupplierAddressID = Interprete(reader, cols, "SupplierAddressID", obj.SupplierAddressID);
            obj.SupplierAddress = Interprete(reader, cols, "SupplierAddress", obj.SupplierAddress);
            obj.AssociatedCard = Interprete(reader, cols, "AssociatedCard", obj.AssociatedCard);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Rounding = Interprete(reader, cols, "Rounding", obj.Rounding); 
            obj.IndividualDiscount = Interprete(reader, cols, "IndividualDiscount", obj.IndividualDiscount);
            obj.Discount = Interprete(reader, cols, "Discount", obj.Discount);
            obj.PaymentType = Interprete(reader, cols, "PaymentType", obj.PaymentType);
            obj.ItemsCurrency = Interprete(reader, cols, "ItemsCurrency", obj.ItemsCurrency);
            obj.Paid = Interprete(reader, cols, "Paid", obj.Paid);
            obj.CustomerCard = Interprete(reader, cols, "CustomerCard", obj.CustomerCard);
            obj.CustomerID = Interprete(reader, cols, "CustomerID", obj.CustomerID);
            obj.CustomerName = Interprete(reader, cols, "CustomerName", obj.CustomerName);
            obj.CustomerAddressID = Interprete(reader, cols, "CustomerAddressID", obj.CustomerAddressID);
            obj.CustomerAddress = Interprete(reader, cols, "CustomerAddress", obj.CustomerAddress);
            obj.CustomerType = Interprete(reader, cols, "CustomerType", obj.CustomerType);
            obj.CustomerDiscount = Interprete(reader, cols, "CustomerDiscount", obj.CustomerDiscount);
            obj.CustomerDiscountNonDiscounted = Interprete(reader, cols, "CustomerDiscountNonDiscounted", obj.CustomerDiscountNonDiscounted);
            obj.CustomerTransaction = Interprete(reader, cols, "CustomerTransaction", obj.CustomerTransaction);
            obj.PriceList = Interprete(reader, cols, "PriceList", obj.PriceList);
            obj.PaymentTitle = Interprete(reader, cols, "PaymentTitle", obj.PaymentTitle);
            obj.UseEET = Interprete(reader, cols, "UseEet", obj.UseEET);
            obj.UUID = Interprete(reader, cols, "UUID", obj.UUID);
            obj.Pkp = Interprete(reader, cols, "Pkp", obj.Pkp);
            obj.Bkp = Interprete(reader, cols, "Bkp", obj.Bkp);
            obj.Fik = Interprete(reader, cols, "Fik", obj.Fik);
            
    }

        public override void FillParams(Bill obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "SupplierID", obj.SupplierID);
            AddParam(parameters, "SupplierName", obj.SupplierName);
            AddParam(parameters, "SupplierAddressID", obj.SupplierAddressID);
            AddParam(parameters, "SupplierAddress", obj.SupplierAddress);
            AddParam(parameters, "AssociatedCard", obj.AssociatedCard);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "IndividualDiscount", obj.IndividualDiscount);
            AddParam(parameters, "Discount", obj.Discount);
            AddParam(parameters, "PaymentType", obj.PaymentType);
            AddParam(parameters, "ItemsCurrency", obj.ItemsCurrency);
            AddParam(parameters, "Paid", obj.Paid);
            AddParam(parameters, "CustomerCard", obj.CustomerCard);
            AddParam(parameters, "CustomerID", obj.CustomerID);
            AddParam(parameters, "CustomerName", obj.CustomerName);
            AddParam(parameters, "CustomerAddressID", obj.CustomerAddressID);
            AddParam(parameters, "CustomerAddress", obj.CustomerAddress);
            AddParam(parameters, "CustomerType", obj.CustomerType);
            AddParam(parameters, "CustomerDiscount", obj.CustomerDiscount);
            AddParam(parameters, "CustomerDiscountNonDiscounted", obj.CustomerDiscountNonDiscounted);
            AddParam(parameters, "CustomerTransaction", obj.CustomerTransaction);
            AddParam(parameters, "PriceList", obj.PriceList);
            AddParam(parameters, "PaymentTitle", obj.PaymentTitle);
            AddParam(parameters, "UseEet", obj.UseEET);
            AddParam(parameters, "UUID", obj.UUID);
            AddParam(parameters, "Pkp", obj.Pkp);
            AddParam(parameters, "Bkp", obj.Bkp);
            AddParam(parameters, "Fik", obj.Fik);
        }

    }
}
