using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class EmailAttachmentDao : AbstractTranswareDao<EmailAttachment>
    {
        public EmailAttachmentDao(SqlConnection connection) : base(connection, "EmailAttachments") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref EmailAttachment obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.EmailMessage = Interprete(reader, cols, "EmailMessage", obj.EmailMessage);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.FileName = Interprete(reader, cols, "FileName", obj.FileName);
            obj.Data = Interprete(reader, cols, "Data", obj.Data);
        }

        public override void FillParams(EmailAttachment obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "EmailMessage", obj.EmailMessage);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "FileName", obj.FileName);
            AddParam(parameters, "Data", obj.Data);
        }
    }
}
