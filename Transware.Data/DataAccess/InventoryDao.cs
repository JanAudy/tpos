using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class InventoryDao : AbstractTranswareDao<Inventory>
    {
        public InventoryDao(SqlConnection connection) : base(connection, "Inventories") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Inventory obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.Complete = Interprete(reader, cols, "Complete", obj.Complete);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Clerk = Interprete(reader, cols, "Clerk", obj.Clerk);
            obj.Note = Interprete(reader, cols, "Note", obj.Note);           
        }

        public override void FillParams(Inventory obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "Complete", obj.Complete);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Clerk", obj.Clerk);
            AddParam(parameters, "Note", obj.Note);
        }
    }
}
