using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class SettlementTypeDao : AbstractTranswareDao<SettlementType>
    {
        public SettlementTypeDao(SqlConnection connection) : base(connection, "SettlementTypes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref SettlementType obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.Actual = Interprete(reader, cols, "Actual", obj.Actual);
            obj.Rounding = Interprete(reader, cols, "Rounding", obj.Rounding);
            obj.Rate = Interprete(reader, cols, "Rate", obj.Rate);
            obj.IsCash = Interprete(reader, cols, "IsCash", obj.IsCash);
            obj.InstantPayment = Interprete(reader, cols, "InstantPayment", obj.InstantPayment);
            obj.Fee = Interprete(reader, cols, "Fee", obj.Fee);
            
        }

        public override void FillParams(SettlementType obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "Actual", obj.Actual);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "Rate", obj.Rate);
            AddParam(parameters, "IsCash", obj.IsCash);
            AddParam(parameters, "InstantPayment", obj.InstantPayment);
            AddParam(parameters, "Fee", obj.Fee);
            
        }
    }
}
