using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PurchaseOrderItemDao : AbstractTranswareDao<PurchaseOrderItem>
    {
        public PurchaseOrderItemDao(SqlConnection connection) : base(connection, "PurchaseOrderItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PurchaseOrderItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Record = Interprete(reader, cols, "Record", obj.Record);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.Provider = Interprete(reader, cols, "Provider", obj.Provider);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(PurchaseOrderItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Record", obj.Record);
            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "Provider", obj.Provider);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
