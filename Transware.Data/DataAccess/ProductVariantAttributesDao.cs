﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class ProductVariantAttributesDao
    {
        protected SqlConnection connection = null;

        protected string tableName = "ProductVariantAttributes";
        protected string masterColumn = "Variant";
        protected string slaveColumn = "Attribute";
        protected string slaveTable = "ProductAttribute";
        protected string masterTable = "ProductVariant";
        protected AbstractTranswareDao<ProductAttribute> slaveDao;


        public ProductVariantAttributesDao(SqlConnection connection, AbstractTranswareDao<ProductAttribute> slaveDao)
        {
            this.connection = connection;
            this.slaveDao = slaveDao;
        }

        public virtual List<ProductAttribute> GetList(long masterID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + slaveTable + " WHERE ID in (SELECT "+slaveColumn+" FROM "+tableName+" WHERE "+masterColumn+"="+masterID.ToString()+")";
                
                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<ProductAttribute> result = new List<ProductAttribute>();
                while (reader.Read())
                {
                    result.Add(slaveDao.Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<ProductAttribute>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual HashSet<ProductAttribute> GetSet(long masterID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + slaveTable + " WHERE ID in (SELECT " + slaveColumn + " FROM " + tableName + " WHERE " + masterColumn + "=" + masterID.ToString() + ")";

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                HashSet<ProductAttribute> result = new HashSet<ProductAttribute>();
                while (reader.Read())
                {
                    result.Add(slaveDao.Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new HashSet<ProductAttribute>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }


        internal bool Update(long masterID, HashSet<ProductAttribute> items)
        {
            HashSet<ProductAttribute> actual = GetSet(masterID);
            bool res = true;
            foreach (ProductAttribute dm in items)
            {
                if (!actual.Contains(dm))
                    res &= Save(masterID, dm.ID);
                else
                    actual.Remove(dm);

                if (!res) break;
            }

            foreach (ProductAttribute dm in actual.ToArray())
            {
                res &= Delete(masterID, dm.ID);
                
                if (!res) break;
            }
            return res;
        }

        internal bool Save(long masterID, long slaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO "+tableName+"("+masterColumn+", "+slaveColumn+") Values (@master, @slave)";
                comm.Parameters.Add(new SqlParameter("master", masterID));
                comm.Parameters.Add(new SqlParameter("slave", slaveID));
                return comm.ExecuteNonQuery() == 1;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                
                if (comm != null)
                    comm.Dispose();
            }
        }

        internal bool Delete(long masterID, long slaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "DELETE " + tableName + " WHERE " + masterColumn + "=@master AND  " + slaveColumn + "= @slave";
                comm.Parameters.Add(new SqlParameter("master", masterID));
                comm.Parameters.Add(new SqlParameter("slave", slaveID));
                return comm.ExecuteNonQuery() >= 1;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {

                if (comm != null)
                    comm.Dispose();
            }
        }

        public int CountInUse(long slaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT COUNT(*) FROM "+tableName+", "+masterTable+" WHERE "+masterColumn+"="+masterTable+".ID AND Valid=1 AND "+slaveColumn+"=@slave";
                comm.Parameters.Add(new SqlParameter("slave", slaveID));
                return (int)comm.ExecuteScalar();
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {

                if (comm != null)
                    comm.Dispose();
            }
        }

        public void Replace(long oldSlaveID, long newSlaveID)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "UPDATE "+tableName+" Set "+slaveColumn+"=@newid WHERE "+slaveColumn+"=@oldid";
                comm.Parameters.Add(new SqlParameter("newid", newSlaveID));
                comm.Parameters.Add(new SqlParameter("oldid", oldSlaveID));
                comm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return;
            }
            finally
            {

                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
