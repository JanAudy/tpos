﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RecordNumberDao : AbstractIDLongDao<RecordNumber>
    {
        public RecordNumberDao(SqlConnection connection) : base(connection, "RecordNumbers") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RecordNumber obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.Year = Interprete(reader, cols, "Year", obj.Year);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.Mask = Interprete(reader, cols, "Mask", obj.Mask);
            obj.First = Interprete(reader, cols, "First", obj.First);
            obj.Next = Interprete(reader, cols, "Next", obj.Next);
        }

        public override void FillParams(RecordNumber obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "Year", obj.Year);
            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "Mask", obj.Mask);
            AddParam(parameters, "First", obj.First);
            AddParam(parameters, "Next", obj.Next);
        }


    }
}
