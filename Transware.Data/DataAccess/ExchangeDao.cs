using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ExchangeDao : AbstractTranswareDao<Exchange>
    {
        protected override string IDColumnName
        {
            get
            {
                return "ConfigClassID";
            }
        }
        public ExchangeDao(SqlConnection connection) : base(connection, "Exchanges") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Exchange obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.CurrencyID = Interprete(reader, cols, "Currency", obj.CurrencyID);
            obj.MainCurrencyID = Interprete(reader, cols, "MainCurrency", obj.MainCurrencyID);
            obj.Unit = Interprete(reader, cols, "Unit", obj.Unit);
            obj.DevizyBuy = Interprete(reader, cols, "DevizyBuy", obj.DevizyBuy);
            obj.DevizySell = Interprete(reader, cols, "DevizySell", obj.DevizySell);
            obj.DevizyMid = Interprete(reader, cols, "DevizyMid", obj.DevizyMid);
            obj.ValutyBuy = Interprete(reader, cols, "ValutyBuy", obj.ValutyBuy);
            obj.ValutySell = Interprete(reader, cols, "ValutySell", obj.ValutySell);
            obj.ValutyMid = Interprete(reader, cols, "ValutyMid", obj.ValutyMid);
            obj.CNBMid = Interprete(reader, cols, "CNBMid", obj.CNBMid);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
        }

        public override void FillParams(Exchange obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Currency", obj.CurrencyID);
            AddParam(parameters, "MainCurrency", obj.MainCurrencyID);
            AddParam(parameters, "Unit", obj.Unit);
            AddParam(parameters, "DevizyBuy", obj.DevizyBuy);
            AddParam(parameters, "DevizySell", obj.DevizySell);
            AddParam(parameters, "DevizyMid", obj.DevizyMid);
            AddParam(parameters, "ValutyBuy", obj.ValutyBuy);
            AddParam(parameters, "ValutySell", obj.ValutySell);
            AddParam(parameters, "ValutyMid", obj.ValutyMid);
            AddParam(parameters, "CNBMid", obj.CNBMid);
            AddParam(parameters, "DateTime", obj.DateTime);            
        }
    }
}
