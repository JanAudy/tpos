﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class StorePackageDao : AbstractIDLongDao<StorePackage>
    {
        public StorePackageDao(SqlConnection connection) : base(connection, "StorePackages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref StorePackage obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.StoreID = Interprete(reader, cols, "Store", obj.StoreID);
            obj.PackageID = Interprete(reader, cols, "Package", obj.PackageID);
            obj.PriceNoVat = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
            obj.IndividualDiscountID = Interprete(reader, cols, "IndividualDiscount", obj.IndividualDiscountID);
        }

        public override void FillParams(StorePackage obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.StoreID);
            AddParam(parameters, "Package", obj.PackageID);
            AddParam(parameters, "PriceNoVat", obj.PriceNoVat);
            AddParam(parameters, "IndividualDiscount", obj.IndividualDiscountID);
        }
    }
}
