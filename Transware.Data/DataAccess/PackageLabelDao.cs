using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PackageLabelDao : AbstractTranswareDao<PackageLabel>
    {
        public PackageLabelDao(SqlConnection connection) : base(connection, "PackageLabels") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PackageLabel obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Specification = Interprete(reader, cols, "Specification", obj.Specification);
        }

        public override void FillParams(PackageLabel obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Specification", obj.Specification);
        }
    }
}
