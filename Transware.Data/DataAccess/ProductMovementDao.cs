using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductMovementDao : AbstractTranswareDao<ProductMovement>
    {
        public ProductMovementDao(SqlConnection connection) : base(connection, "ProductMovements") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductMovement obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.VariantID = Interprete(reader, cols, "Variant", obj.VariantID);
            obj.StoreID = Interprete(reader, cols, "Store", obj.StoreID);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.DocumentType = Interprete(reader, cols, "DocumentType", obj.DocumentType);
            obj.MovementType = Interprete(reader, cols, "MovementType", obj.MovementType);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DocumentID = Interprete(reader, cols, "DocumentID", obj.DocumentID);
            obj.PurchasePrice = Interprete(reader, cols, "PurchasePrice", obj.PurchasePrice);
            obj.SellPrice = Interprete(reader, cols, "SellPrice", obj.SellPrice);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.InStore = Interprete(reader, cols, "InStore", obj.InStore);     
        
        }

        public override void FillParams(ProductMovement obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Product", obj.ProductID);
            AddParam(parameters, "Variant", obj.VariantID);
            AddParam(parameters, "Store", obj.StoreID);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "DocumentType", obj.DocumentType);
            AddParam(parameters, "MovementType", obj.MovementType);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DocumentID", obj.DocumentID);
            AddParam(parameters, "PurchasePrice", obj.PurchasePrice);
            AddParam(parameters, "SellPrice", obj.SellPrice);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "InStore", obj.InStore);
        }
    }
}
