using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class SellWarrantyItemDao : AbstractTranswareDao<SellWarrantyItem>
    {
        public SellWarrantyItemDao(SqlConnection connection) : base(connection, "SellWarrantyItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref SellWarrantyItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.SellWarranty = Interprete(reader, cols, "SellWarranty", obj.SellWarranty);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(SellWarrantyItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "SellWarranty", obj.SellWarranty);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
