using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class EmailSignDao : AbstractTranswareDao<EmailSign>
    {
        public EmailSignDao(SqlConnection connection) : base(connection, "EmailSigns") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref EmailSign obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
        }

        public override void FillParams(EmailSign obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Text", obj.Text);
        }
    }
}
