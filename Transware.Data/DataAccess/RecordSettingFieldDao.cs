﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RecordSettingFieldDao : AbstractIDLongDao<RecordSettingField>
    {
        public RecordSettingFieldDao(SqlConnection connection) : base(connection, "RecordSettingFields") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RecordSettingField obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
        }

        public override void FillParams(RecordSettingField obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Type", obj.Type);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Value", obj.Value);
        }
    }
}
