using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductAttributeDao : AbstractTranswareDao<ProductAttribute>
    {
        public ProductAttributeDao(SqlConnection connection) : base(connection, "ProductAttribute") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductAttribute obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.GroupID = Interprete(reader, cols, "Group", obj.GroupID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            obj.Color = Interprete(reader, cols, "Color", obj.Color);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.Position = Interprete(reader, cols, "Position", obj.Position);            
        }

        public override void FillParams(ProductAttribute obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Group", obj.GroupID);
            AddParam(parameters, "Value", obj.Value);
            AddParam(parameters, "Color", obj.Color);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "Position", obj.Position);
        }
    }
}
