﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CountryLangDao : AbstractIDLongDao<CountryLang>
    {
        public CountryLangDao(SqlConnection connection) : base(connection, "CountryLangs") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref CountryLang obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Country = Interprete(reader, cols, "Country", obj.Country);
        }

        public override void FillParams(CountryLang obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Country", obj.Country);
        }
    }
}
