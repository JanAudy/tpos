using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ReceiptOtherItemDao : AbstractTranswareDao<ReceiptOtherItem>
    {
        public ReceiptOtherItemDao(SqlConnection connection) : base(connection, "ReceiptOtherItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ReceiptOtherItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Record = Interprete(reader, cols, "Record", obj.Record);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Price = Interprete(reader, cols, "Price", obj.Price);
        }

        public override void FillParams(ReceiptOtherItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Record", obj.Record);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Price", obj.Price);
        }
    }
}
