﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class UserShortcutDao : AbstractIDLongDao<UserShortcut>
    {
        protected override string IDColumnName
        {
            get
            {
                return "UserID";
            }
        }

        public UserShortcutDao(SqlConnection connection) : base(connection, "Users") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref UserShortcut obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.CoreID = Interprete(reader, cols, "CoreID", obj.CoreID);
            obj.UserName = Interprete(reader, cols, "UserName", obj.UserName);
            obj.Password = Interprete(reader, cols, "Password", obj.Password);
            obj.Active = Interprete(reader, cols, "Active", obj.Active);
            obj.FullName = Interprete(reader, cols, "FULL_NAME", obj.FullName);
        }


        public override List<UserShortcut> GetList(List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " LEFT JOIN MDATA_JADRO.dbo.Osoba o on CoreID=o.ID";
                if (constraints != null && constraints.Count > 0)
                    comm.CommandText += " WHERE " + ConstraintsToString(constraints, comm.Parameters);

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<UserShortcut> result = new List<UserShortcut>();
                while (reader.Read())
                {
                    result.Add(Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<UserShortcut>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
