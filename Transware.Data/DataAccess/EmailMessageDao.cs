using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class EmailMessageDao : AbstractTranswareDao<EmailMessage>
    {
        public EmailMessageDao(SqlConnection connection) : base(connection, "EmailMessages") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref EmailMessage obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.From = Interprete(reader, cols, "From", obj.From);
            obj.To = Interprete(reader, cols, "To", obj.To);
            obj.CC = Interprete(reader, cols, "CC", obj.CC);
            obj.Subject = Interprete(reader, cols, "Subject", obj.Subject);
            obj.Body = Interprete(reader, cols, "Body", obj.Body);
            obj.RecordType = Interprete(reader, cols, "RecordType", obj.RecordType);
            obj.RecordID = Interprete(reader, cols, "RecordID", obj.RecordID);
            obj.RecordStatus = Interprete(reader, cols, "RecordStatus", obj.RecordStatus);
        }

        public override void FillParams(EmailMessage obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "From", obj.From);
            AddParam(parameters, "To", obj.To);
            AddParam(parameters, "CC", obj.CC);
            AddParam(parameters, "Subject", obj.Subject);
            AddParam(parameters, "Body", obj.Body);
            AddParam(parameters, "RecordType", obj.RecordType);           
            AddParam(parameters, "RecordID", obj.RecordID);
            AddParam(parameters, "RecordStatus", obj.RecordStatus);
        }
    }
}
