using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PurchaseWarrantyDao : AbstractTranswareDao<PurchaseWarranty>
    {
        public PurchaseWarrantyDao(SqlConnection connection) : base(connection, "PurchaseWarrantys") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PurchaseWarranty obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Address = Interprete(reader, cols, "Address", obj.Address);
            obj.Email = Interprete(reader, cols, "Email", obj.Email);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Clerk = Interprete(reader, cols, "Clerk", obj.Clerk);
            obj.Note = Interprete(reader, cols, "Note", obj.Note);
            obj.Result = Interprete(reader, cols, "Result", obj.Result);
            obj.ResultComment = Interprete(reader, cols, "ResultComment", obj.ResultComment);                      
        }

        public override void FillParams(PurchaseWarranty obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Address", obj.Address);
            AddParam(parameters, "Email", obj.Email);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Clerk", obj.Clerk);
            AddParam(parameters, "Note", obj.Note);
            AddParam(parameters, "Result", obj.Result);
            AddParam(parameters, "ResultComment", obj.ResultComment);
        }
    }
}
