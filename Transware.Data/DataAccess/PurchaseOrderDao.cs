using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PurchaseOrderDao : AbstractTranswareDao<PurchaseOrder>
    {
        public PurchaseOrderDao(SqlConnection connection) : base(connection, "PurchaseOrders") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PurchaseOrder obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.SupplierID = Interprete(reader, cols, "SupplierID", obj.SupplierID);
            obj.SupplierName = Interprete(reader, cols, "SupplierName", obj.SupplierName);
            obj.SupplierAddressID = Interprete(reader, cols, "SupplierAddressID", obj.SupplierAddressID);
            obj.SupplierAddress = Interprete(reader, cols, "SupplierAddress", obj.SupplierAddress);
            obj.CustomerID = Interprete(reader, cols, "CustomerID", obj.CustomerID);
            obj.CustomerName = Interprete(reader, cols, "CustomerName", obj.CustomerName);
            obj.CustomerAddressID = Interprete(reader, cols, "CustomerAddressID", obj.CustomerAddressID);
            obj.CustomerAddress = Interprete(reader, cols, "CustomerAddress", obj.CustomerAddress);
            obj.TransportType = Interprete(reader, cols, "TransportType", obj.TransportType);
            obj.OrderType = Interprete(reader, cols, "OrderType", obj.OrderType);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.DeliveryDate = Interprete(reader, cols, "DeliveryDate", obj.DeliveryDate);
            obj.DeliveryDateText = Interprete(reader, cols, "DeliveryDateText", obj.DeliveryDateText);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Clerk = Interprete(reader, cols, "Clerk", obj.Clerk);           
            obj.Text = Interprete(reader, cols, "Text", obj.Text); 
            obj.Specification = Interprete(reader, cols, "Specification", obj.Specification);           
            obj.AdditionalText = Interprete(reader, cols, "AdditionalText", obj.AdditionalText);           
            obj.ShowUnitPrice = Interprete(reader, cols, "ShowUnitPrice", obj.ShowUnitPrice); 
        }

        public override void FillParams(PurchaseOrder obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "SupplierID", obj.SupplierID);
            AddParam(parameters, "SupplierName", obj.SupplierName);
            AddParam(parameters, "SupplierAddressID", obj.SupplierAddressID);
            AddParam(parameters, "SupplierAddress", obj.SupplierAddress);
            AddParam(parameters, "CustomerID", obj.CustomerID);
            AddParam(parameters, "CustomerName", obj.CustomerName);
            AddParam(parameters, "CustomerAddressID", obj.CustomerAddressID);
            AddParam(parameters, "CustomerAddress", obj.CustomerAddress);
            AddParam(parameters, "TransportType", obj.TransportType);
            AddParam(parameters, "OrderType", obj.OrderType);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "DeliveryDate", obj.DeliveryDate);
            AddParam(parameters, "DeliveryDateText", obj.DeliveryDateText);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Clerk", obj.Clerk);
            AddParam(parameters, "Text", obj.Text);
            AddParam(parameters, "Specification", obj.Specification);
            AddParam(parameters, "AdditionalText", obj.AdditionalText);
            AddParam(parameters, "ShowUnitPrice", obj.ShowUnitPrice); 
        }
    }
}
