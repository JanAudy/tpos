using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RolePermissionDao : AbstractTranswareDao<RolePermission>
    {
        public RolePermissionDao(SqlConnection connection) : base(connection, "RolePermissions") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RolePermission obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Role = Interprete(reader, cols, "Role", obj.Role);
            obj.Permission = Interprete(reader, cols, "Permission", obj.Permission);
        }

        public override void FillParams(RolePermission obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Role", obj.Role);
            AddParam(parameters, "Permission", obj.Permission);
        }
    }
}
