﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class InRoleDao : AbstractIDLongDao<InRole>
    {
        protected override string IDColumnName
        {
            get
            {
                return "InRoleID";
            }
        }
        public InRoleDao(SqlConnection connection) : base(connection, "InRoles") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref InRole obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.RoleID = Interprete(reader, cols, "RoleID", obj.RoleID);
            obj.UserID = Interprete(reader, cols, "UserID", obj.UserID);   
        }

        public override void FillParams(InRole obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "RoleID", obj.RoleID);
            AddParam(parameters, "UserID", obj.UserID);
        }
    }
}
