﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PriceCalculationDao : AbstractIDLongDao<PriceCalculation>
    {
        public PriceCalculationDao(SqlConnection connection) : base(connection, "PriceCalculations") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PriceCalculation obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.FixedPrice = Interprete(reader, cols, "FixedPrice", obj.FixedPrice);
        }

        public override void FillParams(PriceCalculation obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "FixedPrice", obj.FixedPrice);
        }
    }
}
