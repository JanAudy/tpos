using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class SellWarrantyNoteDao : AbstractTranswareDao<SellWarrantyNote>
    {
        public SellWarrantyNoteDao(SqlConnection connection) : base(connection, "SellWarrantyNotes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref SellWarrantyNote obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
            obj.SellWarranty = Interprete(reader, cols, "SellWarranty", obj.SellWarranty);
        }

        public override void FillParams(SellWarrantyNote obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "Text", obj.Text);
            AddParam(parameters, "SellWarranty", obj.SellWarranty);
        }
    }
}
