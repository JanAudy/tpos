﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using System.Data.SqlClient;
using Transware.Data.Enums;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public abstract class AbstractTranswareDao<T> : AbstractValidDao<T> where T:AbstractValidDto, new()
    {
        public AbstractTranswareDao(SqlConnection connection, string tableName) : base(connection, tableName) { }


        protected ERecordType Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, ERecordType def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (ERecordType)reader[name];
            return def;
        }

        protected ERoundingType Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, ERoundingType def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (ERoundingType)reader[name];
            return def;
        }

        protected EUsedExchangeRate Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, EUsedExchangeRate def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (EUsedExchangeRate)reader[name];
            return def;
        }

        protected ERecordStatus Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, ERecordStatus def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (ERecordStatus)reader[name];
            return def;
        }

        protected ERecordFlags Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, ERecordFlags def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (ERecordFlags)reader[name];
            return def;
        }

        protected EMovementType Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, EMovementType def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (EMovementType)reader[name];
            return def;
        }

        protected EOrderMarkType Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, EOrderMarkType def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (EOrderMarkType)reader[name];
            return def;
        }

        protected ERoundingPrices Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, ERoundingPrices def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (ERoundingPrices)reader[name];
            return def;
        }


        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, ERecordType value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, ERoundingType value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, EUsedExchangeRate value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, ERecordStatus value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, ERecordFlags value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, EMovementType value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, EOrderMarkType value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, ERoundingPrices value)
        {
            parameters.Add(name, new SqlParameter("@" + name, (int)value));
        }

    }
}
