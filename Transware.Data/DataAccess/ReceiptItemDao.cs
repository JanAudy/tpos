using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ReceiptItemDao : AbstractTranswareDao<ReceiptItem>
    {
        public ReceiptItemDao(SqlConnection connection) : base(connection, "ReceiptItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ReceiptItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Record = Interprete(reader, cols, "Record", obj.Record);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Price = Interprete(reader, cols, "Price", obj.Price);
            obj.AddedPrice = Interprete(reader, cols, "AddedPrice", obj.AddedPrice);
            obj.OrderMark = Interprete(reader, cols, "OrderMark", obj.OrderMark);
        }

        public override void FillParams(ReceiptItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Record", obj.Record);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Price", obj.Price);
            AddParam(parameters, "AddedPrice", obj.AddedPrice);
            AddParam(parameters, "OrderMark", obj.OrderMark);
        }
    }
}
