﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RecordSettingDao : AbstractIDLongDao<RecordSetting>
    {
        public RecordSettingDao(SqlConnection connection) : base(connection, "RecordSettings") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RecordSetting obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Type = Interprete(reader, cols, "Type", obj.Type);
        }

        public override void FillParams(RecordSetting obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Type", obj.Type);
        }
    }
}
