﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class FinalRecipientDao : AbstractIDLongDao<FinalRecipient>
    {
        public FinalRecipientDao(SqlConnection connection) : base(connection, "FinalRecipients") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref FinalRecipient obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.OrganizationID = Interprete(reader, cols, "OrganizationID", obj.OrganizationID);
            obj.Person = Interprete(reader, cols, "Person", obj.Person);
            obj.Department = Interprete(reader, cols, "Department", obj.Department);
            obj.Street = Interprete(reader, cols, "Street", obj.Street);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.City = Interprete(reader, cols, "City", obj.City);
            obj.ZIP = Interprete(reader, cols, "ZIP", obj.ZIP);
            obj.Country = Interprete(reader, cols, "Country", obj.Country);
            obj.Actual = Interprete(reader, cols, "Actual", obj.Actual);
        }

        public override void FillParams(FinalRecipient obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "OrganizationID", obj.OrganizationID);        
            AddParam(parameters, "Person", obj.Person);        
            AddParam(parameters, "Department", obj.Department);        
            AddParam(parameters, "Street", obj.Street);        
            AddParam(parameters, "Number", obj.Number);        
            AddParam(parameters, "City", obj.City);        
            AddParam(parameters, "ZIP", obj.ZIP);        
            AddParam(parameters, "Country", obj.Country);        
            AddParam(parameters, "Actual", obj.Actual); 
        }
    }
}
