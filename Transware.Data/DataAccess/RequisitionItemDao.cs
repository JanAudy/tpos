using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RequisitionItemDao : AbstractTranswareDao<RequisitionItem>
    {
        public RequisitionItemDao(SqlConnection connection) : base(connection, "RequisitionItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref RequisitionItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Record = Interprete(reader, cols, "Record", obj.Record);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.BuyPriceNoVat = Interprete(reader, cols, "BuyPriceNoVat", obj.BuyPriceNoVat);
            obj.PriceNoVat = Interprete(reader, cols, "PriceNoVat", obj.PriceNoVat);
            obj.Vat = Interprete(reader, cols, "Vat", obj.Vat);
        }

        public override void FillParams(RequisitionItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Record", obj.Record);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "BuyPriceNoVat", obj.BuyPriceNoVat);
            AddParam(parameters, "PriceNoVat", obj.PriceNoVat);
            AddParam(parameters, "Vat", obj.Vat);
        }
    }
}
