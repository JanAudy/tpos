using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class AdvInvoiceDao : AbstractTranswareDao<AdvInvoice>
    {
        public AdvInvoiceDao(SqlConnection connection) : base(connection, "AdvInvoices") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref AdvInvoice obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.SupplierID = Interprete(reader, cols, "SupplierID", obj.SupplierID);
            obj.SupplierName = Interprete(reader, cols, "SupplierName", obj.SupplierName);
            obj.SupplierAddressID = Interprete(reader, cols, "SupplierAddressID", obj.SupplierAddressID);
            obj.SupplierAddress = Interprete(reader, cols, "SupplierAddress", obj.SupplierAddress);
            obj.BankAccount = Interprete(reader, cols, "BankAccount", obj.BankAccount);
            obj.BankName = Interprete(reader, cols, "BankName", obj.BankName);
            obj.BankAccountStr = Interprete(reader, cols, "BankAccountStr", obj.BankAccountStr);
            obj.BankCode = Interprete(reader, cols, "BankCode", obj.BankCode);
            obj.BankIBAN = Interprete(reader, cols, "BankIBAN", obj.BankIBAN);
            obj.BankSWIFT = Interprete(reader, cols, "BankSWIFT", obj.BankSWIFT);
            obj.CustomerID = Interprete(reader, cols, "CustomerID", obj.CustomerID);
            obj.CustomerType = Interprete(reader, cols, "CustomerType", obj.CustomerType);
            obj.CustomerName = Interprete(reader, cols, "CustomerName", obj.CustomerName);
            obj.CustomerAddressID = Interprete(reader, cols, "CustomerAddressID", obj.CustomerAddressID);
            obj.CustomerAddress = Interprete(reader, cols, "CustomerAddress", obj.CustomerAddress);
            obj.TransportType = Interprete(reader, cols, "TransportType", obj.TransportType);
            obj.FinalRecipient = Interprete(reader, cols, "FinalRecipient", obj.FinalRecipient);
            obj.FinalRecipientStr = Interprete(reader, cols, "FinalRecipientStr", obj.FinalRecipientStr);
            obj.OrderNo = Interprete(reader, cols, "OrderNo", obj.OrderNo);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.DeliveryNo = Interprete(reader, cols, "DeliveryNo", obj.DeliveryNo);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.MaturityDate = Interprete(reader, cols, "MaturityDate", obj.MaturityDate);
            obj.TaxApplyDate = Interprete(reader, cols, "TaxApplyDate", obj.TaxApplyDate);
            obj.SettlementType = Interprete(reader, cols, "SettlementType", obj.SettlementType);
            obj.Penalization = Interprete(reader, cols, "Penalization", obj.Penalization);
            obj.Settled = Interprete(reader, cols, "Settled", obj.Settled);           
            obj.SettledDate = Interprete(reader, cols, "SettledDate", obj.SettledDate);           
            obj.AmountLeft = Interprete(reader, cols, "AmountLeft", obj.AmountLeft);           
            obj.ItemCurrency = Interprete(reader, cols, "ItemCurrency", obj.ItemCurrency);           
            obj.IndividualDiscount = Interprete(reader, cols, "IndividualDiscount", obj.IndividualDiscount);           
            obj.CustomerDiscount = Interprete(reader, cols, "CustomerDiscount", obj.CustomerDiscount);           
            obj.Rounding = Interprete(reader, cols, "Rounding", obj.Rounding);           
            obj.Clerk = Interprete(reader, cols, "Clerk", obj.Clerk);           
            obj.Header = Interprete(reader, cols, "Header", obj.Header);           
            obj.Footer = Interprete(reader, cols, "Footer", obj.Footer);           
            obj.UseExchange = Interprete(reader, cols, "UseExchange", obj.UseExchange);           
            obj.RateUnit = Interprete(reader, cols, "RateUnit", obj.RateUnit);           
            obj.Rate = Interprete(reader, cols, "Rate", obj.Rate);           
            obj.RoundingType = Interprete(reader, cols, "RoundingType", obj.RoundingType);           
            obj.RoundingPlaces = Interprete(reader, cols, "RoundingPlaces", obj.RoundingPlaces);           
            obj.CustomerTransaction = Interprete(reader, cols, "CustomerTransaction", obj.CustomerTransaction);
        }

        public override void FillParams(AdvInvoice obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "SupplierID", obj.SupplierID);
            AddParam(parameters, "SupplierName", obj.SupplierName);
            AddParam(parameters, "SupplierAddressID", obj.SupplierAddressID);
            AddParam(parameters, "SupplierAddress", obj.SupplierAddress);
            AddParam(parameters, "BankAccount", obj.BankAccount);
            AddParam(parameters, "BankName", obj.BankName);
            AddParam(parameters, "BankAccountStr", obj.BankAccountStr);
            AddParam(parameters, "BankCode", obj.BankCode);
            AddParam(parameters, "BankIBAN", obj.BankIBAN);
            AddParam(parameters, "BankSWIFT", obj.BankSWIFT);
            AddParam(parameters, "CustomerID", obj.CustomerID);
            AddParam(parameters, "CustomerType", obj.CustomerType);
            AddParam(parameters, "CustomerName", obj.CustomerName);
            AddParam(parameters, "CustomerAddressID", obj.CustomerAddressID);
            AddParam(parameters, "CustomerAddress", obj.CustomerAddress);
            AddParam(parameters, "TransportType", obj.TransportType);
            AddParam(parameters, "FinalRecipient", obj.FinalRecipient);
            AddParam(parameters, "FinalRecipientStr", obj.FinalRecipientStr);
            AddParam(parameters, "OrderNo", obj.OrderNo);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "DeliveryNo", obj.DeliveryNo);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "MaturityDate", obj.MaturityDate);
            AddParam(parameters, "TaxApplyDate", obj.TaxApplyDate);
            AddParam(parameters, "SettlementType", obj.SettlementType);
            AddParam(parameters, "Penalization", obj.Penalization);
            AddParam(parameters, "Settled", obj.Settled);
            AddParam(parameters, "SettledDate", obj.SettledDate);
            AddParam(parameters, "AmountLeft", obj.AmountLeft);
            AddParam(parameters, "ItemCurrency", obj.ItemCurrency);
            AddParam(parameters, "IndividualDiscount", obj.IndividualDiscount);
            AddParam(parameters, "CustomerDiscount", obj.CustomerDiscount);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "Clerk", obj.Clerk);
            AddParam(parameters, "Header", obj.Header);
            AddParam(parameters, "Footer", obj.Footer);
            AddParam(parameters, "UseExchange", obj.UseExchange);
            AddParam(parameters, "RateUnit", obj.RateUnit);
            AddParam(parameters, "Rate", obj.Rate);
            AddParam(parameters, "RoundingType", obj.RoundingType);
            AddParam(parameters, "RoundingPlaces", obj.RoundingPlaces);
            AddParam(parameters, "CustomerTransaction", obj.CustomerTransaction);
        }
    }
}
