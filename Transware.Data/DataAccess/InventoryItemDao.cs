using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class InventoryItemDao : AbstractTranswareDao<InventoryItem>
    {
        public InventoryItemDao(SqlConnection connection) : base(connection, "InventoryItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref InventoryItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Inventory = Interprete(reader, cols, "Inventory", obj.Inventory);
            obj.Product = Interprete(reader, cols, "Product", obj.Product);
            obj.StoreAmount = Interprete(reader, cols, "StoreAmount", obj.StoreAmount);
            obj.RealAmount = Interprete(reader, cols, "RealAmount", obj.RealAmount);          
        }

        public override void FillParams(InventoryItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Inventory", obj.Inventory);
            AddParam(parameters, "Product", obj.Product);
            AddParam(parameters, "StoreAmount", obj.StoreAmount);
            AddParam(parameters, "RealAmount", obj.RealAmount);
        }
    }
}
