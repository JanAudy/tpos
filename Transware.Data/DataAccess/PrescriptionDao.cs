using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PrescriptionDao : AbstractTranswareDao<Prescription>
    {
        public PrescriptionDao(SqlConnection connection) : base(connection, "Prescriptions") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Prescription obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.PackageID = Interprete(reader, cols, "Package", obj.PackageID);
            obj.IngredientID = Interprete(reader, cols, "Ingredient", obj.IngredientID);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
        }

        public override void FillParams(Prescription obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Package", obj.PackageID);
            AddParam(parameters, "Ingredient", obj.IngredientID);
            AddParam(parameters, "Amount", obj.Amount);
        }
    }
}
