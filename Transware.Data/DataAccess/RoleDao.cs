﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class RoleDao : AbstractIDLongDao<Role>
    {
        protected override string IDColumnName
        {
            get
            {
                return "RoleID";
            }
        }

        public RoleDao(SqlConnection connection) : base(connection, "Roles") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Role obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.MenuString = Interprete(reader, cols, "MenuString", obj.MenuString);
        }

        public override void FillParams(Role obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "MenuString", obj.MenuString);
        }
    }
}
