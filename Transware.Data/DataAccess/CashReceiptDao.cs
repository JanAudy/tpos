using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CashReceiptDao : AbstractTranswareDao<CashReceipt>
    {
        public CashReceiptDao(SqlConnection connection) : base(connection, "CashReceipts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref CashReceipt obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Store = Interprete(reader, cols, "Store", obj.Store);
            obj.SupplierID = Interprete(reader, cols, "SupplierID", obj.SupplierID);
            obj.SupplierName = Interprete(reader, cols, "SupplierName", obj.SupplierName);
            obj.SupplierAddress = Interprete(reader, cols, "SupplierAddress", obj.SupplierAddress);
            obj.CustomerID = Interprete(reader, cols, "CustomerID", obj.CustomerID);
            obj.CustomerName = Interprete(reader, cols, "CustomerName", obj.CustomerName);
            obj.CustomerAddress = Interprete(reader, cols, "CustomerAddress", obj.CustomerAddress);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.NumberStr = Interprete(reader, cols, "NumberStr", obj.NumberStr);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.CloseDate = Interprete(reader, cols, "CloseDate", obj.CloseDate);
            obj.CloseUser = Interprete(reader, cols, "CloseUser", obj.CloseUser);
            obj.Status = Interprete(reader, cols, "Status", obj.Status);
            obj.Flags = Interprete(reader, cols, "Flags", obj.Flags);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Currency = Interprete(reader, cols, "Currency", obj.Currency);
            obj.Purpose = Interprete(reader, cols, "Purpose", obj.Purpose);
            obj.ApprovedBy = Interprete(reader, cols, "ApprovedBy", obj.ApprovedBy);
            obj.Sheet = Interprete(reader, cols, "Sheet", obj.Sheet);
            obj.SheetNumber = Interprete(reader, cols, "SheetNumber", obj.SheetNumber);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
            obj.Budget = Interprete(reader, cols, "Budget", obj.Budget);
            obj.ReceivedBy = Interprete(reader, cols, "ReceivedBy", obj.ReceivedBy);
        }

        public override void FillParams(CashReceipt obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Store", obj.Store);
            AddParam(parameters, "SupplierID", obj.SupplierID);
            AddParam(parameters, "SupplierName", obj.SupplierName);
            AddParam(parameters, "SupplierAddress", obj.SupplierAddress);
            AddParam(parameters, "CustomerID", obj.CustomerID);
            AddParam(parameters, "CustomerName", obj.CustomerName);
            AddParam(parameters, "CustomerAddress", obj.CustomerAddress);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "NumberStr", obj.NumberStr);
            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "CloseDate", obj.CloseDate);
            AddParam(parameters, "CloseUser", obj.CloseUser);
            AddParam(parameters, "Status", obj.Status);
            AddParam(parameters, "Flags", obj.Flags);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Currency", obj.Currency);
            AddParam(parameters, "Purpose", obj.Purpose);
            AddParam(parameters, "ApprovedBy", obj.ApprovedBy);
            AddParam(parameters, "Sheet", obj.Sheet);
            AddParam(parameters, "SheetNumber", obj.SheetNumber);
            AddParam(parameters, "Text", obj.Text);
            AddParam(parameters, "Budget", obj.Budget);
            AddParam(parameters, "ReceivedBy", obj.ReceivedBy);            
        }
    }
}
