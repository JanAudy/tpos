using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataAccess
{
    public class EmailTextDao : AbstractTranswareDao<EmailText>
    {
        public EmailTextDao(SqlConnection connection) : base(connection, "EmailTexts") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref EmailText obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.RecordType = (ERecordType)Interprete(reader, cols, "RecordType", obj.RecordType);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
        }

        public override void FillParams(EmailText obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "RecordType", obj.RecordType);
            AddParam(parameters, "Text", obj.Text);
        }
    }
}
