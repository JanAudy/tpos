using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductExchangeItemDao : AbstractTranswareDao<ProductExchangeItem>
    {
        public ProductExchangeItemDao(SqlConnection connection) : base(connection, "ProductExchangeItems") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductExchangeItem obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Record = Interprete(reader, cols, "Record", obj.Record);
            obj.IndividualItem = Interprete(reader, cols, "IndividualItem", obj.IndividualItem);
            obj.Package = Interprete(reader, cols, "Package", obj.Package);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.IndividualDiscount = Interprete(reader, cols, "IndividualDiscount", obj.IndividualDiscount);
            obj.Amount = Interprete(reader, cols, "Amount", obj.Amount);
            obj.Vat = Interprete(reader, cols, "Vat", obj.Vat);
            obj.Price = Interprete(reader, cols, "Price", obj.Price);
            obj.Returned = Interprete(reader, cols, "FromAdvProductExchange", obj.Returned);
        }

        public override void FillParams(ProductExchangeItem obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Record", obj.Record);
            AddParam(parameters, "IndividualItem", obj.IndividualItem);
            AddParam(parameters, "Package", obj.Package);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "IndividualDiscount", obj.IndividualDiscount);
            AddParam(parameters, "Amount", obj.Amount);
            AddParam(parameters, "Vat", obj.Vat);
            AddParam(parameters, "Price", obj.Price);
            AddParam(parameters, "Returned", obj.Returned);
        }
    }
}
