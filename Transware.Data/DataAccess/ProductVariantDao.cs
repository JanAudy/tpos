using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductVariantDao : AbstractTranswareDao<ProductVariant>
    {
        public ProductVariantDao(SqlConnection connection) : base(connection, "ProductVariant") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductVariant obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.OrderMark = Interprete(reader, cols, "OrderMark", obj.OrderMark);
            obj.Specification = Interprete(reader, cols, "Specification", obj.Specification);
            obj.ProviderID = Interprete(reader, cols, "Provider", obj.ProviderID); 
            obj.SettingID = Interprete(reader, cols, "Setting", obj.SettingID);
            obj.UserID1 = Interprete(reader, cols, "UserID1", obj.UserID1);
            obj.UserID2 = Interprete(reader, cols, "UserID2", obj.UserID2);
            obj.DetailedSpecification = Interprete(reader, cols, "DetailedSpecification", obj.DetailedSpecification);      
        }

        public override void FillParams(ProductVariant obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Product", obj.ProductID);
            AddParam(parameters, "OrderMark", obj.OrderMark);
            AddParam(parameters, "Specification", obj.Specification);
            AddParam(parameters, "Provider", (obj.ProviderID == 0) ? null : obj.ProviderID);
            AddParam(parameters, "Setting", obj.SettingID);
            AddParam(parameters, "UserID1", obj.UserID1);
            AddParam(parameters, "UserID2", obj.UserID2);
            AddParam(parameters, "DetailedSpecification", obj.DetailedSpecification);
        }
    }
}
