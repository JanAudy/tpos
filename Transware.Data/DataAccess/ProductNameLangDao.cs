﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class ProductNameLangDao : AbstractTranswareDao<ProductNameLang>
    {
        public ProductNameLangDao(SqlConnection connection)
            : base(connection, "ProductNameLangs") { }

        public Dictionary<ELanguage, ProductNameLang> GetByParent(long parentID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " WHERE Product=" + parentID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                Dictionary<ELanguage, ProductNameLang> result = new Dictionary<ELanguage, ProductNameLang>();
                while (reader.Read())
                {
                    ProductNameLang lang = Interprete(reader, cols);
                    result.Add(lang.Language, lang);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new Dictionary<ELanguage, ProductNameLang>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override bool SaveOrUpdate(ProductNameLang obj)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "Update " + tableName + " Set ";
                comm.CommandText += "Language=@language, Product=@product, Value=@value";
                comm.CommandText += " WHERE ProductName=" + obj.ProductID + " AND Language=" + (int)obj.Language;
                comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                comm.Parameters.Add(new SqlParameter("product", obj.ProductID));
                comm.Parameters.Add(new SqlParameter("value", obj.Value));

                int count = comm.ExecuteNonQuery();

                if (count == 0)
                {
                    comm.Dispose();
                    comm = connection.CreateCommand();
                    comm.CommandText = "INSERT INTO " + tableName + "(Language, Product, Value)";
                    comm.CommandText += " Values(@language, @product, @value)";
                    comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                    comm.Parameters.Add(new SqlParameter("product", obj.ProductID));
                    comm.Parameters.Add(new SqlParameter("value", obj.Value));
                    count = comm.ExecuteNonQuery();
                }

                bool result = count == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override ProductNameLang Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            ProductNameLang obj = new ProductNameLang();
            obj.Language = (ELanguage)Interprete(reader, cols, "Language", (int)obj.Language);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            return obj;
        }

    }
}
