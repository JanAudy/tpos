using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PriceListProductGroupDao : AbstractTranswareDao<PriceListProductGroup>
    {
        public PriceListProductGroupDao(SqlConnection connection) : base(connection, "PriceListProductGroup") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PriceListProductGroup obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.PriceList = Interprete(reader, cols, "PriceList", obj.PriceList);
            obj.ProductGroup = Interprete(reader, cols, "ProductGroup", obj.ProductGroup);
            obj.Coefficient = Interprete(reader, cols, "Coefficient", obj.Coefficient);
            obj.Addition = Interprete(reader, cols, "Addition", obj.Addition);
        }

        public override void FillParams(PriceListProductGroup obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "PriceList", obj.PriceList);
            AddParam(parameters, "ProductGroup", obj.ProductGroup);
            AddParam(parameters, "Coefficient", obj.Coefficient);
            AddParam(parameters, "Addition", obj.Addition);
        }
    }
}
