using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Version = Transware.Data.DataTypes.Version;

namespace Transware.Data.DataAccess
{
    public class VoucherDao : AbstractTranswareDao<Voucher>
    {
        public VoucherDao(SqlConnection connection) : base(connection, "Vouchers") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Voucher obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.VoucherType = Interprete(reader, cols, "VoucherType", obj.VoucherType);
            obj.Ean = Interprete(reader, cols, "Ean", obj.Ean);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
        }

        public override void FillParams(Voucher obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "VoucherType", obj.VoucherType);
            AddParam(parameters, "Ean", obj.Ean);
            AddParam(parameters, "Value", obj.Value);
            AddParam(parameters, "Description", obj.Description);
        }
    }
}
