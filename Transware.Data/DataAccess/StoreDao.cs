using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class StoreDao : AbstractTranswareDao<Store>
    {
        public StoreDao(SqlConnection connection) : base(connection, "Stores") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Store obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Street = Interprete(reader, cols, "Street", obj.Street);
            obj.Number = Interprete(reader, cols, "Number", obj.Number);
            obj.City = Interprete(reader, cols, "City", obj.City);
            obj.ZIP = Interprete(reader, cols, "ZIP", obj.ZIP);
            obj.Country = Interprete(reader, cols, "Country", obj.Country);
            obj.WarrantyID = Interprete(reader, cols, "Warranty", obj.WarrantyID);

        }

        public override void FillParams(Store obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Street", obj.Street);
            AddParam(parameters, "Number", obj.Number);
            AddParam(parameters, "City", obj.City);
            AddParam(parameters, "ZIP", obj.ZIP);
            AddParam(parameters, "Country", obj.Country);
            AddParam(parameters, "Warranty", obj.WarrantyID);
        }
    }
}
