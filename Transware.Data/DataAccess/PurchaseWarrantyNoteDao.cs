using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class PurchaseWarrantyNoteDao : AbstractTranswareDao<PurchaseWarrantyNote>
    {
        public PurchaseWarrantyNoteDao(SqlConnection connection) : base(connection, "PurchaseWarrantyNotes") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref PurchaseWarrantyNote obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.DateTime = Interprete(reader, cols, "DateTime", obj.DateTime);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
            obj.PurchaseWarranty = Interprete(reader, cols, "PurchaseWarranty", obj.PurchaseWarranty);
        }

        public override void FillParams(PurchaseWarrantyNote obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "DateTime", obj.DateTime);
            AddParam(parameters, "Text", obj.Text);
            AddParam(parameters, "PurchaseWarranty", obj.PurchaseWarranty);
        }
    }
}
