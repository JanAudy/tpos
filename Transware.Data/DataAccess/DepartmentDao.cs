using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class DepartmentDao : AbstractTranswareDao<Department>
    {
        public DepartmentDao(SqlConnection connection) : base(connection, "Departments") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Department obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
        }

        public override void FillParams(Department obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Description", obj.Description);
        }
    }
}
