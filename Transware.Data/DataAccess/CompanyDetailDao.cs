﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class CompanyDetailDao : AbstractIDLongDao<CompanyDetail>
    {
        public CompanyDetailDao(SqlConnection connection) : base(connection, "CompanyDetails") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref CompanyDetail obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.OrganizationID = Interprete(reader, cols, "OrganizationID", obj.OrganizationID);
            obj.Penalization = Interprete(reader, cols, "Penalization", obj.Penalization);
            obj.Maturity = Interprete(reader, cols, "Maturity", obj.Maturity);
            obj.RecordLanguage = Interprete(reader, cols, "RecordLanguage", obj.RecordLanguage);
        }

        public override void FillParams(CompanyDetail obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "OrganizationID", obj.OrganizationID);
            AddParam(parameters, "Penalization", obj.Penalization);
            AddParam(parameters, "Maturity", obj.Maturity);
            AddParam(parameters, "RecordLanguage", obj.RecordLanguage);
        }
    }
}
