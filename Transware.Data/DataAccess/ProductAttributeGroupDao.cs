using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProductAttributeGroupDao : AbstractTranswareDao<ProductAttributeGroup>
    {
        public ProductAttributeGroupDao(SqlConnection connection) : base(connection, "ProductAttributeGroup") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref ProductAttributeGroup obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.DisplayName = Interprete(reader, cols, "DisplayName", obj.DisplayName);
            obj.Description = Interprete(reader, cols, "Description", obj.Description);
            obj.IsColor = Interprete(reader, cols, "IsColor", obj.IsColor);
            
        }

        public override void FillParams(ProductAttributeGroup obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "DisplayName", obj.DisplayName);
            AddParam(parameters, "Description", obj.Description);
            AddParam(parameters, "IsColor", obj.IsColor);
        }

       
    }
}
