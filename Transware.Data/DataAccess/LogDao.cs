﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class LogDao : AbstractIDLongDao<Log>
    {
        protected override string IDColumnName
        {
            get
            {
                return "LoggerObjectID";
            }
        }
        public LogDao(SqlConnection connection) : base(connection, "Log") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Log obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Date = Interprete(reader, cols, "Date", obj.Date);
            obj.Host = Interprete(reader, cols, "Host", obj.Host);
            obj.Level = Interprete(reader, cols, "Level", obj.Level);
            obj.LoggedUser = Interprete(reader, cols, "LoggedUser", obj.LoggedUser);
            obj.Message = Interprete(reader, cols, "Message", obj.Message);
            obj.Exception = Interprete(reader, cols, "Exception", obj.Exception);
        }

        public override void FillParams(Log obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Date", obj.Date);
            AddParam(parameters, "Host", obj.Host);
            AddParam(parameters, "Level", obj.Level);
            AddParam(parameters, "LoggedUser", obj.LoggedUser);
            AddParam(parameters, "Message", obj.Message);
            AddParam(parameters, "Exception", obj.Exception);
        }
    }
}
