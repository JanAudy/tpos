using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;

namespace Transware.Data.DataAccess
{
    public class ProviderDao : AbstractTranswareDao<Provider>
    {
        public ProviderDao(SqlConnection connection) : base(connection, "Providers") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Provider obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.ProductID = Interprete(reader, cols, "Product", obj.ProductID);
            obj.VariantID = Interprete(reader, cols, "Variant", obj.VariantID);
            obj.OrganizationID = Interprete(reader, cols, "OrganizationID", obj.OrganizationID);
            obj.CatalogNumber = Interprete(reader, cols, "CatalogNumber", obj.CatalogNumber);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.CurrencyID = Interprete(reader, cols, "Currency", obj.CurrencyID);
            obj.Price = Interprete(reader, cols, "Price", obj.Price);
        }

        public override void FillParams(Provider obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Product", obj.ProductID);
            AddParam(parameters, "Variant", obj.VariantID);
            AddParam(parameters, "OrganizationID", obj.OrganizationID);
            AddParam(parameters, "CatalogNumber", obj.CatalogNumber);
            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Currency", obj.CurrencyID);
            AddParam(parameters, "Price", obj.Price);
        }
    }
}
