using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;
using Microdata.Data.DataTypes;
using Transware.Data.DataTypes;
using Transware.Data.Enums;

namespace Transware.Data.DataAccess
{
    public class CurrencyDao : AbstractTranswareDao<Currency>
    {
        public CurrencyDao(SqlConnection connection) : base(connection, "Currencies") { }

        public override void Interprete(SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Currency obj)
        {
            base.Interprete(reader, cols, ref obj);
            obj.Name = Interprete(reader, cols, "Name", obj.Name);
            obj.Shortcut = Interprete(reader, cols, "Shortcut", obj.Shortcut);
            obj.Rounding = (ERoundingType)Interprete(reader, cols, "Rounding", obj.Rounding);
            obj.Places = Interprete(reader, cols, "Places", obj.Places);
            obj.PackageRounding = (ERoundingType)Interprete(reader, cols, "PackageRounding", obj.PackageRounding);
            obj.PackagePlaces = Interprete(reader, cols, "PackagePlaces", obj.PackagePlaces);
        }

        public override void FillParams(Currency obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Name", obj.Name);
            AddParam(parameters, "Shortcut", obj.Shortcut);
            AddParam(parameters, "Rounding", obj.Rounding);
            AddParam(parameters, "Places", obj.Places);
            AddParam(parameters, "PackageRounding", obj.PackageRounding);
            AddParam(parameters, "PackagePlaces", obj.PackagePlaces);
        }
    }
}
