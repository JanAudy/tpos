﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.Enums;
using Transware.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data;

namespace Transware.Data.DataAccess
{
    public class TransportTypeLangDao : AbstractTranswareDao<TransportTypeLang>
    {
        public TransportTypeLangDao(SqlConnection connection)
            : base(connection, "TransportTypeNameLangs") { }

        public Dictionary<ELanguage, TransportTypeLang> GetByParent(long parentID)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                comm.CommandText += " WHERE TransportType=" + parentID;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                Dictionary<ELanguage, TransportTypeLang> result = new Dictionary<ELanguage, TransportTypeLang>();
                while (reader.Read())
                {
                    TransportTypeLang lang = Interprete(reader, cols);
                    result.Add(lang.Language, lang);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new Dictionary<ELanguage, TransportTypeLang>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override bool SaveOrUpdate(TransportTypeLang obj)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "Update " + tableName + " Set ";
                comm.CommandText += "Language=@language, TransportType=@unit, Value=@value";
                comm.CommandText += " WHERE TransportType=" + obj.TransportTypeID + " AND Language=" + (int)obj.Language;
                comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                comm.Parameters.Add(new SqlParameter("unit", obj.TransportTypeID));
                comm.Parameters.Add(new SqlParameter("value", obj.Value));

                int count = comm.ExecuteNonQuery();

                if (count == 0)
                {
                    comm.Dispose();
                    comm = connection.CreateCommand();
                    comm.CommandText = "INSERT INTO " + tableName + "(TransportType, Language, Value)";
                    comm.CommandText += " Values(@unit, @language, @value)";
                    comm.Parameters.Add(new SqlParameter("unit", obj.TransportTypeID));
                    comm.Parameters.Add(new SqlParameter("language", (int)obj.Language));
                    comm.Parameters.Add(new SqlParameter("value", obj.Value));
                    count = comm.ExecuteNonQuery();
                }

                bool result = count == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public override TransportTypeLang Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            TransportTypeLang obj = new TransportTypeLang();
            obj.Language = (ELanguage)Interprete(reader, cols, "Language", (int)obj.Language);
            obj.TransportTypeID = Interprete(reader, cols, "TransportType", obj.TransportTypeID);
            obj.Value = Interprete(reader, cols, "Value", obj.Value);
            return obj;
        }

    }
}
