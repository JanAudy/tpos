﻿namespace CustomerSys.TestApp
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGiftInsert = new System.Windows.Forms.Button();
            this.bGiftSelect = new System.Windows.Forms.Button();
            this.bGiftSelectAll = new System.Windows.Forms.Button();
            this.bGiftSelectAllValid = new System.Windows.Forms.Button();
            this.bActionInsert = new System.Windows.Forms.Button();
            this.bActionSelect = new System.Windows.Forms.Button();
            this.bActionAll = new System.Windows.Forms.Button();
            this.bTransaction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bGiftInsert
            // 
            this.bGiftInsert.Location = new System.Drawing.Point(22, 21);
            this.bGiftInsert.Name = "bGiftInsert";
            this.bGiftInsert.Size = new System.Drawing.Size(75, 23);
            this.bGiftInsert.TabIndex = 0;
            this.bGiftInsert.Text = "Gift Insert";
            this.bGiftInsert.UseVisualStyleBackColor = true;
            this.bGiftInsert.Click += new System.EventHandler(this.bGiftInsert_Click);
            // 
            // bGiftSelect
            // 
            this.bGiftSelect.Location = new System.Drawing.Point(24, 63);
            this.bGiftSelect.Name = "bGiftSelect";
            this.bGiftSelect.Size = new System.Drawing.Size(75, 23);
            this.bGiftSelect.TabIndex = 1;
            this.bGiftSelect.Text = "Gift Select";
            this.bGiftSelect.UseVisualStyleBackColor = true;
            this.bGiftSelect.Click += new System.EventHandler(this.bGiftSelect_Click);
            // 
            // bGiftSelectAll
            // 
            this.bGiftSelectAll.Location = new System.Drawing.Point(22, 101);
            this.bGiftSelectAll.Name = "bGiftSelectAll";
            this.bGiftSelectAll.Size = new System.Drawing.Size(75, 23);
            this.bGiftSelectAll.TabIndex = 2;
            this.bGiftSelectAll.Text = "GetSelectAll";
            this.bGiftSelectAll.UseVisualStyleBackColor = true;
            this.bGiftSelectAll.Click += new System.EventHandler(this.bGiftSelectAll_Click);
            // 
            // bGiftSelectAllValid
            // 
            this.bGiftSelectAllValid.Location = new System.Drawing.Point(26, 136);
            this.bGiftSelectAllValid.Name = "bGiftSelectAllValid";
            this.bGiftSelectAllValid.Size = new System.Drawing.Size(115, 23);
            this.bGiftSelectAllValid.TabIndex = 3;
            this.bGiftSelectAllValid.Text = "Gift Select AllValid";
            this.bGiftSelectAllValid.UseVisualStyleBackColor = true;
            this.bGiftSelectAllValid.Click += new System.EventHandler(this.bGiftSelectAllValid_Click);
            // 
            // bActionInsert
            // 
            this.bActionInsert.Location = new System.Drawing.Point(165, 21);
            this.bActionInsert.Name = "bActionInsert";
            this.bActionInsert.Size = new System.Drawing.Size(75, 23);
            this.bActionInsert.TabIndex = 4;
            this.bActionInsert.Text = "Action Insert";
            this.bActionInsert.UseVisualStyleBackColor = true;
            this.bActionInsert.Click += new System.EventHandler(this.bActionInsert_Click);
            // 
            // bActionSelect
            // 
            this.bActionSelect.Location = new System.Drawing.Point(165, 63);
            this.bActionSelect.Name = "bActionSelect";
            this.bActionSelect.Size = new System.Drawing.Size(88, 23);
            this.bActionSelect.TabIndex = 5;
            this.bActionSelect.Text = "Action Select";
            this.bActionSelect.UseVisualStyleBackColor = true;
            this.bActionSelect.Click += new System.EventHandler(this.bActionSelect_Click);
            // 
            // bActionAll
            // 
            this.bActionAll.Location = new System.Drawing.Point(178, 101);
            this.bActionAll.Name = "bActionAll";
            this.bActionAll.Size = new System.Drawing.Size(75, 23);
            this.bActionAll.TabIndex = 6;
            this.bActionAll.Text = "ActionSelectAll";
            this.bActionAll.UseVisualStyleBackColor = true;
            this.bActionAll.Click += new System.EventHandler(this.bActionAll_Click);
            // 
            // bTransaction
            // 
            this.bTransaction.Location = new System.Drawing.Point(97, 212);
            this.bTransaction.Name = "bTransaction";
            this.bTransaction.Size = new System.Drawing.Size(75, 23);
            this.bTransaction.TabIndex = 7;
            this.bTransaction.Text = "Transaction";
            this.bTransaction.UseVisualStyleBackColor = true;
            this.bTransaction.Click += new System.EventHandler(this.bTransaction_Click);
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.bTransaction);
            this.Controls.Add(this.bActionAll);
            this.Controls.Add(this.bActionSelect);
            this.Controls.Add(this.bActionInsert);
            this.Controls.Add(this.bGiftSelectAllValid);
            this.Controls.Add(this.bGiftSelectAll);
            this.Controls.Add(this.bGiftSelect);
            this.Controls.Add(this.bGiftInsert);
            this.Name = "TestForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bGiftInsert;
        private System.Windows.Forms.Button bGiftSelect;
        private System.Windows.Forms.Button bGiftSelectAll;
        private System.Windows.Forms.Button bGiftSelectAllValid;
        private System.Windows.Forms.Button bActionInsert;
        private System.Windows.Forms.Button bActionSelect;
        private System.Windows.Forms.Button bActionAll;
        private System.Windows.Forms.Button bTransaction;
    }
}

