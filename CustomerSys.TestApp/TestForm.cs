﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CustomerSys.Data;

namespace CustomerSys.TestApp
{
    public partial class TestForm : Form
    {
        
        public TestForm()
        {
            InitializeComponent();

            CustomerSysDB.Instance.DB_Server = @"PLA06-NB\SQLEXPRESS";
            CustomerSysDB.Instance.DB_UserName = "sa";
            CustomerSysDB.Instance.DB_Password = "cdlist";
        }

        private void bGiftInsert_Click(object sender, EventArgs e)
        {
            Gift g = new Gift();
            g.Name = "Darek";
            g.Description = "Popis darku";

            CustomerSysDB.Instance.GiftDao.Save(g);

            g.Description = "opraveny description";

            CustomerSysDB.Instance.GiftDao.Update(g);
        }

        private void bGiftSelect_Click(object sender, EventArgs e)
        {
            Gift g = CustomerSysDB.Instance.GiftDao.Get(6);
        }

        private void bGiftSelectAll_Click(object sender, EventArgs e)
        {
            List<Gift> gifts = CustomerSysDB.Instance.GiftDao.GetAll();
        }

        private void bGiftSelectAllValid_Click(object sender, EventArgs e)
        {
            List<Gift> gifts = CustomerSysDB.Instance.GiftDao.GetAllValid();
        }

        private void bActionInsert_Click(object sender, EventArgs e)
        {
            CardAction g = new CardAction();
            g.CardType = 2;
            g.DateTime = DateTime.Now;
            g.Arrival = CardAction.ArrivalType.Every;
            g.ArrivalData = 234;
            g.Period = CardAction.PeriodType.AtDay;
            g.PeriodFrom = new DateTime(2000, 1, 1);
            g.PeriodTo = DateTime.Now;
            g.PeriodData = 123;
            g.Expense = CardAction.ExpenseType.TotalPerCard;
            g.ExpenseFrom = 100;
            g.ExpenseTo = null;// 200;
            g.Points = CardAction.PointsType.PerCard;
            g.PointsData = 200;
            g.Reward = CardAction.RewardType.Points;
            g.RewardValue = CardAction.RewardValueType.Discount;
            g.RewardValueData = 100;
            g.RewardPoint = CardAction.RewardPointType.OnePointPer;
            g.RewardPointData = 100;
            
            CustomerSysDB.Instance.CardActionDao.Save(g);

            g.ExpenseTo = 1002;
            CustomerSysDB.Instance.CardActionDao.Update(g);
        }

        private void bActionSelect_Click(object sender, EventArgs e)
        {
            CardAction g = CustomerSysDB.Instance.CardActionDao.Get(3);
        }

        private void bActionAll_Click(object sender, EventArgs e)
        {
            List<CardAction> g = CustomerSysDB.Instance.CardActionDao.GetAll();
        }

        private void bTransaction_Click(object sender, EventArgs e)
        {
            Transaction tr = new Transaction();
            tr.Card = 1;
            tr.Person = 1;
            tr.Department = 0;
            tr.Organization = 1;
            tr.DateTime = DateTime.Now;
            tr.RecordType = Transaction.ERecordType.Bill;
            tr.RecordID = 2;
            tr.RecordNumber = 32;
            tr.RecordNumberStr = "32";
            tr.Amount = 100;
            tr.Discount = 10;
            tr.AmountWithDiscount = 90;
            tr.Points = 100;
            tr.Certificate = 433;
            tr.Gift = 1;

            CustomerSysDB.Instance.TransactionDao.SaveOrUpdate(tr);

            tr.Gift = 2;

            CustomerSysDB.Instance.TransactionDao.SaveOrUpdate(tr);

            List<Transaction> trs = CustomerSysDB.Instance.TransactionDao.GetAll();

            List<Transaction> trsv = CustomerSysDB.Instance.TransactionDao.GetAllValid();

        }
    }
}
