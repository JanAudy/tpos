﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Microdata.Util;
using Transware.Data.DataTypes;
using Transware.Data;
using Transware.Data.DataModel;
using EETLib;

namespace Transware.TouchPOS
{
    public partial class ShortcutConfigForm : Form
    {
        private List<ShortcutInfo[]> pages = new List<ShortcutInfo[]>();
        private int actualPage = 0;
        private int actualShortcut = -1;
        private bool editMode = false;
        private List<TouchButton> shortcutsButtons = new List<TouchButton>();
        private List<TouchButton> colorButtons = new List<TouchButton>();
        private int selectedColor = Color.White.ToArgb();
        private PackageSummary selectedPackage = null;
        public bool ShutdownAction { get; set; }

        public ShortcutConfigForm()
        {
            InitializeComponent();

            LoadData();

            shortcutsButtons.Add(bItem1);
            shortcutsButtons.Add(bItem2);
            shortcutsButtons.Add(bItem3);
            shortcutsButtons.Add(bItem4);
            shortcutsButtons.Add(bItem5);
            shortcutsButtons.Add(bItem6);
            shortcutsButtons.Add(bItem7);
            shortcutsButtons.Add(bItem8);
            shortcutsButtons.Add(bItem9);
            shortcutsButtons.Add(bItem10);
            shortcutsButtons.Add(bItem11);
            shortcutsButtons.Add(bItem12);
            shortcutsButtons.Add(bItem13);
            shortcutsButtons.Add(bItem14);
            shortcutsButtons.Add(bItem15);
            shortcutsButtons.Add(bItem16);
            shortcutsButtons.Add(bItem17);
            shortcutsButtons.Add(bItem18);

            colorButtons.Add(bColor1);
            colorButtons.Add(bColor2);
            colorButtons.Add(bColor3);
            colorButtons.Add(bColor4);
            colorButtons.Add(bColor5);
            colorButtons.Add(bColor6);
            colorButtons.Add(bColor7);
            colorButtons.Add(bColor8);
            colorButtons.Add(bColor9);
            colorButtons.Add(bColor10);
            colorButtons.Add(bColor11);
            colorButtons.Add(bColor12);
            colorButtons.Add(bColor13);
            colorButtons.Add(bColor14);



            if (Setting.Instance.LoginRequired)
                tbExit.Text = "ODHLÁSIT";
            else
                tbExit.Text = "VYPNOUT";

            DrawData();
        }

        private void DrawData()
        {
            DrawShortcuts();
            DrawColors();
            DrawPackageInfo();

            Setting ls = Setting.Instance;
            cbEetEnabled.Checked = ls.EetEnabled;
            cbEetTesting.Checked = ls.EetTesting;

            EnableForm();
        }

        private void EnableForm()
        {
            gbShortcuts.Enabled = !editMode;
            bOK.Enabled = bCancel.Enabled = !editMode;
            gbSetting.Enabled = editMode;
        }

        private void LoadData()
        {
            pages = ShortcutIO.Load();
        }

        private void SaveData()
        {
            ShortcutIO.Save(pages);
            
        }

        private void DrawShortcuts()
        {
            ShortcutInfo[] array = pages[actualPage];
            for (int i = 0; i < ShortcutIO.ItemsPerPage; i++)
            {
                ShortcutInfo p = array[i];
                if (p.Package == null)
                {
                    shortcutsButtons[i].Text = "<nepřiřazeno>";
                    shortcutsButtons[i].Tag = i;
                    shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = true;
                }
                else
                {
                    shortcutsButtons[i].Text = p.Package.Name;
                    shortcutsButtons[i].Tag = i;
                    shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = true;
                }
            }
            
            bLeft.Visible = actualPage > 0;
            bRight.Visible = actualPage < pages.Count - 1;

            lPage.Text = "Strana " + (actualPage + 1);
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            //if (paymentMode)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        if (e.Control)
            //            bCloseRecord_Click(sender, e);
            //        else
            //            ConfirmProduct(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.Escape || e.KeyCode==Keys.Delete)
            //    {
            //        ClearCodeClick(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.Back)
            //    {
            //        DeleteLastClick(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.F4 && e.Alt)
            //        return;
            //    else
            //    {
            //        char c = KeyboardUtil.GetKey(e.KeyCode);
            //        if (c >= 32)
            //        {
            //            tbPaid.Text += c;

            //            e.Handled = true;
            //        }
            //    }
            //}
            //else
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        ConfirmProduct(sender, e);
            //    }
            //}
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bItem_Click(object sender, EventArgs e)
        {
            actualShortcut = (int)(sender as Control).Tag;
            selectedPackage = pages[actualPage][actualShortcut].Package;
            selectedColor = pages[actualPage][actualShortcut].ShortcutColor;
            editMode = true;

            DrawData();
        }

        private void bLeft_Click(object sender, EventArgs e)
        {
            actualPage--;
            DrawData();
        }

        private void bRight_Click(object sender, EventArgs e)
        {
            actualPage++;
            DrawData();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            SaveData();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void bColor_Click(object sender, EventArgs e)
        {
            selectedColor = (sender as TouchButton).FillColorBottom.ToArgb();
            DrawColors();
        }

        private void DrawColors()
        {
            for (int i = 0; i < colorButtons.Count; i++)
            {
                colorButtons[i].Text = "";
                if (colorButtons[i].FillColorBottom.ToArgb() == selectedColor)
                {
                    colorButtons[i].BorderColor = Color.Black;
                    colorButtons[i].BorderWidth = 2;
                }
                else
                {
                    colorButtons[i].BorderColor = Color.White;
                    colorButtons[i].BorderWidth = 1;
                }
            }
        }

        private void bStoreCard_Click(object sender, EventArgs e)
        {
            StoreCardForm form = new StoreCardForm();
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                selectedPackage = form.Package;
                DrawPackageInfo();
            }
        }

        private void DrawPackageInfo()
        {
            tbPackage.Text = (selectedPackage != null) ? selectedPackage.Name : "";
        }

        private void bClearPackage_Click(object sender, EventArgs e)
        {
            selectedPackage = null;
            DrawPackageInfo();
        }

        private void bCancelShortcut_Click(object sender, EventArgs e)
        {
            editMode = false;
            DrawData();
        }

        private void bSaveShortcut_Click(object sender, EventArgs e)
        {
            pages[actualPage][actualShortcut].Package = selectedPackage;
            pages[actualPage][actualShortcut].ShortcutColor = selectedColor;
            
            editMode = false;
            DrawData();
        }

        private void tbExit_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            ShutdownAction = true;
            DialogResult = System.Windows.Forms.DialogResult.Ignore;
            Close();
        }

        private void tbEetTest_Click(object sender, EventArgs e)
        {
            EetRegisterRequest eet = new EetRegisterRequest();

            try
            {
                if (eet.SendTestRequest("1", 1210, DateTime.Now, 1000, 210))
                    MessageBox.Show("Odeslání testovací zprávy bylo úspěšné", "Odeslání testovací zprávy", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Odeslání testovací zprávy selhalo", "Odeslání testovací zprávy", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Došlo k neočekávané chybě", "Odeslání testovací zprávy", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tbEetVerify_Click(object sender, EventArgs e)
        {
            Setting ls = Setting.Instance;
            string error = "";
            if (string.IsNullOrEmpty(ls.EetVat))
                error = "Není zadáno DIČ";
            else if (string.IsNullOrEmpty(ls.EetShopName))
                error = "Není zadání ID provozovny";
            else if (string.IsNullOrEmpty(ls.EetDeviceName))
                error = "Není zadání ID pokladny";
            else if (string.IsNullOrEmpty(ls.EetCertificatePassword))
                error = "Není zadání heslo k certifikatu";
            else if (string.IsNullOrEmpty(ls.EetCertificate))
                error = "Není zadání certifikát";
            if (string.IsNullOrEmpty(error))
            {
                EetRegisterRequest eet = new EetRegisterRequest();
                eet.DICPoplatnika = ls.EetVat;
                eet.IdProvozovny = ls.EetShopName;
                eet.IdPokladny = ls.EetDeviceName;
                string dataStr = ls.EetCertificate;
                
                if (string.IsNullOrEmpty(dataStr))
                {
                    MessageBox.Show("Certifikát a nebo heslo nejsou zadané!", "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                byte[] data = Convert.FromBase64String(dataStr);
                
                bool certSet = eet.SetCertificate(data, ls.EetCertificatePassword);
                if (!certSet)
                {
                    MessageBox.Show("Certifikát a nebo heslo nejsou platné!", "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        if (eet.CheckFullRequest())
                        {
                            MessageBox.Show("Ověření proběhlo v pořádku", "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Ověření SELHALO. Prosím ověřte zadané údaje!", "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Došlo k neočekávané chybě.", "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Nelze ověřit údaje: " + error, "Ověření certifikátu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void touchButton1_Click(object sender, EventArgs e)
        {

            if (!Setting.Instance.Printer) {
                MessageBox.Show("Není nastavena tiskárna!!!");
                return;
            }
            string res = Printing.Instance.PrintTest();
            if (res !="")
                MessageBox.Show(res);
        }
    }
}
