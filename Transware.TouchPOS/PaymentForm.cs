﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Microdata.Util;
using Transware.Data.DataTypes;
using Transware.Data;
using Jadro.Data.DataTypes;
using Transware.Data.DataModel;
using Transware.Data.Enums;
using CustomerSys.Data;
using Jadro.Data.DataModel;
using Jadro.Data;
using Transware.Core;
using EETLib;


namespace Transware.TouchPOS
{
    public partial class PaymentForm : Form
    {
        private enum ESelectButtonsDraw { Clear, Payment, Discount };

        private Bill bill = new Bill();
        private int payment = 0;
        private bool paymentMode = true;
        private bool SplitPayments = false;
        private int selectItem = -1;
        private decimal? toReturn = null;
        private List<PaymentType> paymentTypes = new List<PaymentType>();
        private List<IndividualDiscount> discounts = new List<IndividualDiscount>();
        private List<TouchButton> selectButtons = new List<TouchButton>();
        private ESelectButtonsDraw drawMode = ESelectButtonsDraw.Clear;
        public string ActualPaymentName { get; set; }
        public bool rounding = false;
        public Evaluator.CustomerDiscountType customerDiscount = new Evaluator.CustomerDiscountType() { Discount = 0, OnlyNonDiscounted = false };
        private IDMedia media = null;
        private int selectButtonIndex = 0;
        double[] zd = new double[] { 0, 0, 0 };
        double[] dan = new double[] { 0, 0, 0 };

        double[] vat = new double[] { 0, 0, 0 };
        public PaymentForm(Bill bill)
        {
            this.bill = bill;

            InitializeComponent();

            tbPaid.BackColor = Color.White;
            tbPaid.ForeColor = Color.Black;

            paymentTypes = TranswareDB.Instance.PaymentTypeDao.GetList();
            discounts = TranswareDB.Instance.IndividualDiscountDao.GetList();
            discounts.Sort();

            selectButtons.Add(bSelect1); selectButtons.Add(bSelect2); selectButtons.Add(bSelect3); selectButtons.Add(bSelect4); 

            payment = 0;
            PaymentChanged();

            paymentMode = true;
            
            if (bill.CustomerCard != 0)
            {
                IDMedia media = IDMedialModel.Instance.Get(bill.CustomerCard);

                if (media!=null)
                    customerDiscount = Evaluator.Instance.GetDiscount(bill.CustomerCard, (long)media.Typ_Media, bill.CustomerID, 0, DateTime.Now, bill.TotalPrice);

                bill.CustomerDiscount = customerDiscount.Discount;
                bill.CustomerDiscountNonDiscounted = customerDiscount.OnlyNonDiscounted;
            }

            DrawData();
        }

        private void DrawSelectButtons(ESelectButtonsDraw drawType = ESelectButtonsDraw.Clear)
        {
            this.drawMode = drawType;

            foreach (TouchButton tb in selectButtons)
                tb.Visible = false;

            
            if (drawType==ESelectButtonsDraw.Discount)
            {
                for (int i = selectButtonIndex; i <= discounts.Count; i++)
                {
                    if (i == 0)
                    {
                        selectButtons[i-selectButtonIndex].Text = "bez slevy";
                    }
                    else
                    {
                        selectButtons[i - selectButtonIndex].Text = discounts[i - 1].Discount.ToString("0.#####") + " %";
                    }
                    selectButtons[i - selectButtonIndex].Visible = true;
                    selectButtons[i - selectButtonIndex].Tag = i;
                    
                    if ((i==0 && bill.Discount==0) || (i!=0 && bill.Discount == discounts[i-1].Discount))
                        MakeSelectEnable(selectButtons[i - selectButtonIndex]);
                    else
                        MakeSelectDisable(selectButtons[i - selectButtonIndex]);
                    
                    if ((i-selectButtonIndex) == selectButtons.Count-1)
                        break;
                }
                bLeft.Visible = selectButtonIndex > 0;
                bRight.Visible = selectButtonIndex + selectButtons.Count < discounts.Count+1;
            }
            else if(drawType==ESelectButtonsDraw.Payment)
            {
                for (int i = selectButtonIndex; i < paymentTypes.Count; i++)
                {
                    selectButtons[i - selectButtonIndex].Text = paymentTypes[i].Name;
                    selectButtons[i - selectButtonIndex].Visible = true;
                    selectButtons[i - selectButtonIndex].Tag = i;

                    if (selectItem == i)
                        MakeSelectEnable(selectButtons[i - selectButtonIndex]);
                    else
                        MakeSelectDisable(selectButtons[i - selectButtonIndex]);
                    if ((i - selectButtonIndex) == selectButtons.Count - 1)
                        break;
                }
                bLeft.Visible = selectButtonIndex > 0;
                bRight.Visible = selectButtonIndex + selectButtons.Count < paymentTypes.Count;
            }            
        }

        private void bCash_Click(object sender, EventArgs e)
        {
            payment = 0;
            PaymentChanged();
        }

        private void bCard_Click(object sender, EventArgs e)
        {
            payment = 1;
            PaymentChanged();
        }

        private void bOther_Click(object sender, EventArgs e)
        {
            payment = 2;
            PaymentChanged();
        }

        private void bOther1_Click(object sender, EventArgs e)
        {
            payment = 3;
            PaymentChanged();
        }

        private void MakeDisable(TouchButton button)
        {
            button.ForeColor = Color.Black;
            button.FillColorBottom = button.FillColorTop = Color.FromArgb(192, 192, 255);
        }
        

        private void MakeEnable(TouchButton button)
        {
            button.ForeColor = Color.White;
            button.FillColorBottom = button.FillColorTop = Color.Blue;
        }

        private void MakeSelectDisable(TouchButton button)
        {
            button.ForeColor = Color.Black;
            button.FillColorBottom = button.FillColorTop = Color.FromArgb(192, 255, 255);
        }


        private void MakeSelectEnable(TouchButton button)
        {
            button.ForeColor = Color.White;
            button.FillColorBottom = button.FillColorTop = Color.Blue;
        }


        private void PaymentChanged()
        {
            switch (payment)
            {
                case 0:
                    MakeEnable(bCash);
                    MakeDisable(bCard);
                    MakeDisable(bSplitPayment);
                    MakeDisable(bDiscount);
                    DrawSelectButtons(ESelectButtonsDraw.Clear);
                    pItems.Visible = false;
                    ActualPaymentName = "Hotovost";
                    rounding = paymentTypes[(int)Setting.Instance.PaymentCash-1].Rounding;
                    RoundingChanged();
                    tbPaid.Text = "";
                    tbPaid.Focus();
                    bLeft.Visible = bRight.Visible = false;
                    break;
                case 1:
                    MakeDisable(bCash);
                    MakeEnable(bCard);
                    MakeDisable(bSplitPayment);
                    MakeDisable(bDiscount);
                    DrawSelectButtons(ESelectButtonsDraw.Clear);
                    pItems.Visible = false;
                    ActualPaymentName = "Kartou";
                    rounding = paymentTypes[(int)Setting.Instance.PaymentCard-1].Rounding;
                    RoundingChanged();
                    tbPaid.Text = bill.TotalRemainsWithRounding.ToString("0.00");
                    tbPaid.Focus();
                    tbPaid.SelectionStart = 0;
                    tbPaid.SelectionLength = 0;
                    bLeft.Visible = bRight.Visible = false;
                    break;
                case 2:
                    bLeft.Visible = bRight.Visible = false;
                    selectButtonIndex = 0;
                    MakeDisable(bCash);
                    MakeDisable(bCard);
                    MakeEnable(bSplitPayment);
                    MakeDisable(bDiscount);
                    DrawSelectButtons(ESelectButtonsDraw.Payment);
                    pItems.Visible = true;
                    rounding = true;
                    RoundingChanged();
                    
                    tbPaid.Text = "";
                    tbPaid.Focus();

                    break;
                case 3:
                    bLeft.Visible = bRight.Visible = false;
                    selectButtonIndex = 0;
                    MakeDisable(bCash);
                    MakeDisable(bCard);
                    MakeDisable(bSplitPayment);
                    MakeEnable(bDiscount);
                    DrawSelectButtons(ESelectButtonsDraw.Discount);
                    pItems.Visible = true;
                    tbPaid.Text = "";
                    tbPaid.Focus();
                    break;
            }
        }

        private void RoundingChanged()
        {
            if (rounding)
            {
                bill.ComputeRounding();
            }
            else
            {
                bill.ClearRounding();
            }
            DrawData();
        }
        

        private void NumberButtonClick(object sender, EventArgs e)
        {
            tbPaid.Text += (sender as TouchButton).Tag.ToString();
        }

        private void ClearCodeClick(object sender, EventArgs e)
        {
            tbPaid.Text = "";
        }

        private void DeleteLastClick(object sender, EventArgs e)
        {
            if (tbPaid.Text.Length > 0)
                tbPaid.Text = tbPaid.Text.Substring(0, tbPaid.Text.Length - 1);
        }

        private void ConfirmProduct(object sender, EventArgs e)
        {
            if (tbPaid.Text.Length > 6)
            {
                if (MessageBox.Show("Zadaná částka je velmi velká, opravdu ji přijmout?", "Podezřelá částka", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    return;
            }

            if (paymentMode)
                if (payment <= 1)
                    bCloseRecord_Click(sender, e);
                //bAddPayment_Click(sender, e);
                else
                    bAddSelectPayment_Click(sender, e);
            else
                bClose_Click(sender, e);
        }

        private void DrawData()
        {
            if (paymentMode)
            {
                if (bill.Discount != 0)
                {
                    tbTotal.Text = bill.TotalPrice.ToString("0.00");
                }
                else
                {
                    tbTotal.Text = bill.TotalPriceWithRounding.ToString("0.00");
                }
                tbDiscount.Text = (bill.Discount > 0) ? bill.Discount.ToString("0.####") + " %" : "";
                tbTotalWithDiscount.Text = bill.TotalPriceWithRounding.ToString("0.00");
                tbRemains.Text = (bill.TotalRemainsWithRounding >= 0) ? bill.TotalRemainsWithRounding.ToString("0.00") : "0";
                bCash.Enabled = bCard.Enabled = bSplitPayment.Enabled = bDiscount.Enabled = true;
                bCloseRecord.Visible = true;
                bCancel.Visible = true;
                bClose.Visible = false;
                pKeys.Visible = true;
                pPayment.Visible = true;
                lReturnLabel.Visible = true;
                tbReturn.Enabled = false;
                tbCustomer.Visible = true;
                bSelectCustomer.Visible = true;
                lCustomer.Visible = true;
                bAddPayment.Visible = false;

                pKeys.Enabled = true;
                pItems.Enabled = true;

                tbCustomer.Enabled = true;
                bSelectCustomer.Enabled = true;
                lCustomer.Enabled = true;
                lError.Visible = true;
                bAddPayment.Enabled = true;
                //cbDiscount.Enabled = true;

                Display.Instance.DrawParts("Celkem:", bill.TotalPriceWithRounding.ToString("0.00"), "", "");
            }
            else
            {
                if (bill.Discount != 0)
                {
                    tbTotal.Text = bill.TotalPrice.ToString("0.00");
                }
                else
                {
                    tbTotal.Text = bill.TotalPriceWithRounding.ToString("0.00");
                } 
                tbRemains.Text = "";
                tbReturn.Text = (-bill.TotalRemainsWithRounding).ToString("0.00");
                bCash.Enabled = bCard.Enabled = bSplitPayment.Enabled = bDiscount.Enabled = false;
                bCloseRecord.Visible = false;
                bCancel.Visible = false;
                bClose.Visible = true;

                pKeys.Visible = true;
                pPayment.Visible = true;
                
                pKeys.Enabled = false;
                pItems.Enabled = false;

                lReturnLabel.Visible = true;
                tbReturn.Enabled = true;

                tbCustomer.Enabled = false;
                bSelectCustomer.Enabled = false;
                lCustomer.Enabled = false;
                lError.Visible = false;
                bAddPayment.Visible = false;
                //cbDiscount.Enabled = false;

                Display.Instance.DrawParts("Celkem:", bill.TotalPriceWithRounding.ToString("0.00"), "Vrátit:", (-bill.TotalRemainsWithRounding).ToString("0.00"));
            }

            DrawCustomer();

            bCash.Enabled = bCard.Enabled = !SplitPayments && paymentMode;

            tbDiscount.Visible = lDiscount.Visible = lTotalWithDiscount.Visible = tbTotalWithDiscount.Visible = bill.Discount != 0;

            lvPayments.Objects = bill.Payments;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (paymentMode)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (e.Control)
                        bCloseRecord_Click(sender, e);
                    else
                        ConfirmProduct(sender, e);
                }
                else if (e.KeyCode == Keys.Escape || e.KeyCode==Keys.Delete)
                {
                    ClearCodeClick(sender, e);
                }
                else if (e.KeyCode == Keys.Back)
                {
                    DeleteLastClick(sender, e);
                }
                else if (e.KeyCode == Keys.F4 && e.Alt)
                    return;
                else if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
                {
                    return;
                }
                else
                {
                    char c = KeyboardUtil.GetKey(e.KeyCode);
                    if (c >= 32)
                    {
                        tbPaid.Text += c;

                        e.Handled = true;
                    }
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ConfirmProduct(sender, e);
                }
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }


        private void bCancel_Click(object sender, EventArgs e)
        {
            bill.Payments.Clear();
            bill.Discount = 0;
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bCloseRecord_Click(object sender, EventArgs e)
        {
            try
            {
                bool paymentAdded = false;
                if (!string.IsNullOrEmpty(tbPaid.Text))
                {
                    int count = bill.Payments.Count;
                    if (payment <= 1)
                        bAddPayment_Click(sender, e);
                    else
                        bAddSelectPayment_Click(sender, e);
                    paymentAdded = count != bill.Payments.Count;
                }
                else
                {
                    if (bill.Payments.Count == 0)
                    {
                        tbPaid.Text = bill.TotalRemainsWithRounding.ToString("0.00");
                        if (payment <= 1)
                            bAddPayment_Click(sender, e);
                        else
                            //bAddSelectPayment_Click(sender, e);
                            if (selectItem >= 0)
                                bill.Add(new BillPayment() { PaymentType = paymentTypes[selectItem].ID, Amount = bill.TotalRemainsWithRounding, Name = paymentTypes[selectItem].Name });
                            else
                            {
                                tbPaid.Text = "";
                                lError.Text = "Není vybrán typ platby";
                                return;
                            }
                    }
                }

                //if (bill.TotalRemains==0)
                //{
                //    DialogResult = DialogResult.OK;
                //    Close();
                //}
                //else
                {
                    if (bill.TotalRemainsWithRounding > 0.009m)
                    {
                        lError.Text = "Zaplaceno je méně, než hodnota nákupu";
                        if (paymentAdded)
                        {
                            //tbPaid.Text = bill.Payments[bill.Payments.Count - 1].Amount.ToString();
                            bill.Payments.RemoveAt(bill.Payments.Count - 1);
                        }
                        DrawData();
                        tbPaid.Focus();
                        return;
                    }
                    else
                    {
                        CloseRecord();

                        tbPaid.Text = bill.TotalPayments.ToString("0.00");
                        paymentMode = false;
                        DrawData();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                    lError.Text = "Není zadáno číslo";
            }
        }

        private void CloseRecord()
        {
            bill.Status = Data.Enums.ERecordStatus.Closed;
            bill.CloseDate = DateTime.Now;
            bill.Paid = bill.TotalPayments;
            bill.CloseUser = AppInfo.Instance.LoggedUser;

            List<RecordNumber> rns = RecordNumberModel.Instance.Get(ERecordType.Bill, Setting.Instance.Store);
            if (rns.Count > 0)
            {
                RecordNumber rn = rns[0];
                bill.Number = rn.Next++;
                bill.NumberStr = rn.FormateByPattern(bill.Number);
                RecordNumberModel.Instance.SaveOrUpdate(rn);
            }

            Setting ls = Setting.Instance;
            if (ls.EetEnabled)
            {
                if (ls.EetTesting)
                {
                    try
                    {
                        EetRegisterRequest eet = new EetRegisterRequest();
                        if (eet.SendTestRequest(bill.NumberStr, (double)bill.TotalPriceWithRounding, bill.CloseDate))
                            bill.Fik = eet.Fik;
                        bill.Pkp = eet.PkpStr;
                        bill.Bkp = eet.BkpFormatted;
                        bill.UseEET = true;
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        EetRegisterRequest eet = new EetRegisterRequest();
                        bill.UseEET = true;
                        eet.DICPoplatnika = ls.EetVat;
                        eet.IdProvozovny = ls.EetShopName;
                        eet.IdPokladny = ls.EetDeviceName;
                        eet.DatumOdeslani = DateTime.Now;
                        string dataStr = ls.EetCertificate;
                        byte[] data = Convert.FromBase64String(dataStr);
                        int koeficient = 1;
                        int baseVat = 21;
                        eet.SetCertificate(data, ls.EetCertificatePassword);
                        if (ls.EetTaxPayer)
                        {
                            int vatCount = bill.Taxes.Length;
                            List<RecordTax> tx = bill.Taxes.ToList();
                            tx.Sort((x, y) => y.Percent.CompareTo(x.Percent));

                            int index = 0;


                            for (int i = 0; i < tx.Count; i++)

                            {

                               

                                if (tx[0].Percent < baseVat)
                                    index += 1;

                                zd[index] = (double)Math.Round((koeficient * tx[i].PriceNoVat), 2);
                                dan[index] = (double)Math.Round((koeficient * (tx[i].Price - tx[i].PriceNoVat)), 2);
                                vat[index] = (double)tx[i].Percent;

                                index += 1;


                            }


                        }
                        if (eet.SendRequest(bill.NumberStr, (double)bill.TotalPriceWithRounding, bill.CloseDate, zd[0], dan[0], zd[1], dan[1], zd[2], dan[2]))
                            bill.Fik = eet.Fik;
                        bill.Pkp = eet.PkpStr;
                        bill.Bkp = eet.BkpFormatted;
                    }
                    catch { }
                }
            }

            BillModel.Instance.SaveOrUpdate(bill);

            List<StoreTransaction> storeTransactions = new List<StoreTransaction>();
            foreach (BillItem item in bill.Items)
            {
                if (!item.Valid) continue;
                if (item.IndividualItem)
                    continue;

                StoreTransaction st = new StoreTransaction();
                st.Store = bill.Store;
                Package pkg = PackageModel.Instance.Get(item.Package.Value);
                st.Product = pkg.ProductID.Value;
                st.Variant = (pkg.VariantID.HasValue) ? pkg.VariantID.Value : 0;
                st.DateTime = bill.DateTime;
                st.MovementType = Data.Enums.EMovementType.Release;
                st.RecordType = Data.Enums.ERecordType.Bill;
                st.RecordID = bill.ID;
                st.RecordItemID = item.ID;
                st.RecordNumber = bill.Number;
                st.RecordNumberStr = bill.NumberStr ?? "";
                st.Amount = item.Amount;
                st.PurchasePriceNoVat = 0;
                st.SellPriceNoVat = item.UnitPriceNoVatWithDiscount;
                st.UpdatePurchasePricesOnRelease = true;
                storeTransactions.Add(st);

                //if (item.Package != null)
                //{
                //    foreach (Prescription p in item.Package.Prescriptions)
                //    {
                //        st = new StoreTransaction();
                //        st.Store = record.Store.ID;
                //        st.Product = p.Ingredient.Product.ID;
                //        if (p.Ingredient.Variant != null)
                //            st.Variant = p.Ingredient.Variant.ID;
                //        st.DateTime = record.DateTime;
                //        st.MovementType = Data.Enums.EMovementType.Release;
                //        st.RecordType = Data.Enums.ERecordType.Bill;
                //        st.RecordID = record.ID;
                //        st.RecordItemID = item.ID;
                //        st.RecordNumber = record.Number;
                //        st.RecordNumberStr = record.NumberStr ?? "";
                //        st.Amount = p.Amount;
                //        st.PurchasePriceNoVat = 0;
                //        st.SellPriceNoVat = 0;
                //        st.UpdatePurchasePricesOnRelease = updatePurchasePrice;
                //        storeTransactions.Add(st);
                //    }
                //}
            }
            TranswareDB.Instance.StoreTransactionDao.ProcessTransaction(storeTransactions);

            Printing.Instance.Print(bill);
        }
       

        private void bAddPayment_Click(object sender, EventArgs e)
        {
            toReturn = null;

            if (!string.IsNullOrEmpty(tbPaid.Text))
            {
                decimal d = decimal.Parse(tbPaid.Text);
                //if (d > bill.TotalRemainsWithDiscount)
                //{
                long index = paymentTypes[payment].ID;
                if (payment == 0)
                    index = Setting.Instance.PaymentCash;
                if (payment == 1)
                    index = Setting.Instance.PaymentCard;
                    
                    bill.Payments.Add(new BillPayment() { PaymentType = index, Amount = d, Name = ActualPaymentName });
                //}
            }
            
            tbPaid.Text = "";
            DrawData();
        }

        private void bAddSelectPayment_Click(object sender, EventArgs e)
        {
            if (selectItem < 0)
            {
                lError.Text = "Není zvolen typ rozdělené platby";
                return;
            }

            if (!string.IsNullOrEmpty(tbPaid.Text) && selectItem>=0)
            {
                decimal d = decimal.Parse(tbPaid.Text);
                bill.Add(new BillPayment() { PaymentType = paymentTypes[selectItem].ID, Amount = d, Name = paymentTypes[selectItem].Name });
                SplitPayments = true;
            }

            tbPaid.Text = "";
            DrawData();
        }

        
        private void bSelect_Click(object sender, EventArgs e)
        {
            if (drawMode == ESelectButtonsDraw.Discount)
            {
                int index = (int)(sender as TouchButton).Tag;
                if (index==0)
                    bill.Discount = 0;
                else
                    bill.Discount = discounts[index-1].Discount;

                if (SplitPayments)
                {
                    payment = 2;
                    DrawSelectButtons(ESelectButtonsDraw.Payment);
                    PaymentChanged();
                }
                else
                {
                    payment = 0;
                    PaymentChanged();
                }

                //DrawSelectButtons(ESelectButtonsDraw.Payment);
                DrawData();
            }
            else if (drawMode == ESelectButtonsDraw.Payment)
            {
                selectItem = (int)(sender as TouchButton).Tag;
                DrawSelectButtons(ESelectButtonsDraw.Payment);
            }
        }

        private void bSelectCustomer_Click(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (form.Media != null)
                {
                    IDMedia id = form.Media;
                    if (id.OsobaID != 0)
                    {
                        bill.CustomerID = (long)id.OsobaID;
                        bill.CustomerType = 0;
                        bill.CustomerName = id.Osoba.Full_Name ?? (id.Osoba.Jmeno + " " + id.Osoba.Prijmeni);
                    }
                    else if (id.OrganizaceID != 0)
                    {
                        bill.CustomerID = (long)id.OrganizaceID;
                        bill.CustomerType = 1;
                        bill.CustomerName = id.Organizace.Nazev;
                    }
                    bill.CustomerCard = (long)id.ID;

                    if (bill.CustomerCard != 0)
                    {
                        if (id != null)
                            customerDiscount = Evaluator.Instance.GetDiscount(bill.CustomerCard, (long)id.Typ_Media, bill.CustomerID, 0, DateTime.Now, bill.TotalPrice);

                        bill.CustomerDiscount = customerDiscount.Discount;
                        bill.CustomerDiscountNonDiscounted = customerDiscount.OnlyNonDiscounted;
                    }
                }
                else if (form.Person != null)
                {
                    Osoba id = form.Person;
                    bill.CustomerID = (long)id.ID;
                    bill.CustomerType = 0;
                    bill.CustomerName = id.Full_Name ?? (id.Prijmeni + " " + id.Jmeno);
                    bill.CustomerCard = 0;
                }
                else if (form.Company != null)
                {
                    Organizace id = form.Company;
                    bill.CustomerID = (long)id.ID;
                    bill.CustomerType = 1;
                    bill.CustomerName = id.Nazev;
                    bill.CustomerCard = 0;
                }
                DrawCustomer();
                DrawData();
            }
        }


        private void DrawCustomer()
        {
            tbCustomer.Text = bill.CustomerName;
            if (bill.CustomerCard == 0)
                media = null;
            else
                if (media == null || (media != null && media.ID != bill.CustomerCard))
                {
                    media = IDMedialModel.Instance.Get(bill.CustomerCard);
                }
            if (media != null && media.Typ!=null)
                tbCustomer.Text += "(" + media.Typ.Popis + ")";
        }

        private void bLeft_Click(object sender, EventArgs e)
        {
            selectButtonIndex -= selectButtons.Count;
            if (payment == 2)
                DrawSelectButtons(ESelectButtonsDraw.Payment);
            else
                DrawSelectButtons(ESelectButtonsDraw.Discount);
        }

        private void bRight_Click(object sender, EventArgs e)
        {
            selectButtonIndex += selectButtons.Count;
            if (payment == 2)
                DrawSelectButtons(ESelectButtonsDraw.Payment);
            else
                DrawSelectButtons(ESelectButtonsDraw.Discount);
        }

    }
}
