﻿namespace Transware.TouchPOS
{
    partial class ShortcutConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMain = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbEetVerify = new Transware.Controls.TouchButton();
            this.tbEetTest = new Transware.Controls.TouchButton();
            this.cbEetTesting = new System.Windows.Forms.CheckBox();
            this.cbEetEnabled = new System.Windows.Forms.CheckBox();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.bColor14 = new Transware.Controls.TouchButton();
            this.bColor13 = new Transware.Controls.TouchButton();
            this.bColor12 = new Transware.Controls.TouchButton();
            this.bColor11 = new Transware.Controls.TouchButton();
            this.bSaveShortcut = new Transware.Controls.TouchButton();
            this.bCancelShortcut = new Transware.Controls.TouchButton();
            this.bColor10 = new Transware.Controls.TouchButton();
            this.bColor8 = new Transware.Controls.TouchButton();
            this.bColor6 = new Transware.Controls.TouchButton();
            this.bColor4 = new Transware.Controls.TouchButton();
            this.bColor5 = new Transware.Controls.TouchButton();
            this.bColor7 = new Transware.Controls.TouchButton();
            this.bColor9 = new Transware.Controls.TouchButton();
            this.bColor2 = new Transware.Controls.TouchButton();
            this.bColor3 = new Transware.Controls.TouchButton();
            this.bColor1 = new Transware.Controls.TouchButton();
            this.label3 = new System.Windows.Forms.Label();
            this.bClearPackage = new Transware.Controls.TouchButton();
            this.bStoreCard = new Transware.Controls.TouchButton();
            this.tbPackage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbShortcuts = new System.Windows.Forms.GroupBox();
            this.pShortCuts = new System.Windows.Forms.Panel();
            this.lPage = new System.Windows.Forms.Label();
            this.bRight = new Transware.Controls.TouchButton();
            this.bLeft = new Transware.Controls.TouchButton();
            this.bItem16 = new Transware.Controls.TouchButton();
            this.bItem17 = new Transware.Controls.TouchButton();
            this.bItem18 = new Transware.Controls.TouchButton();
            this.bItem13 = new Transware.Controls.TouchButton();
            this.bItem14 = new Transware.Controls.TouchButton();
            this.bItem15 = new Transware.Controls.TouchButton();
            this.bItem10 = new Transware.Controls.TouchButton();
            this.bItem11 = new Transware.Controls.TouchButton();
            this.bItem12 = new Transware.Controls.TouchButton();
            this.bItem7 = new Transware.Controls.TouchButton();
            this.bItem8 = new Transware.Controls.TouchButton();
            this.bItem9 = new Transware.Controls.TouchButton();
            this.bItem4 = new Transware.Controls.TouchButton();
            this.bItem5 = new Transware.Controls.TouchButton();
            this.bItem6 = new Transware.Controls.TouchButton();
            this.bItem1 = new Transware.Controls.TouchButton();
            this.bItem2 = new Transware.Controls.TouchButton();
            this.bItem3 = new Transware.Controls.TouchButton();
            this.label1 = new System.Windows.Forms.Label();
            this.bCancel = new Transware.Controls.TouchButton();
            this.bOK = new Transware.Controls.TouchButton();
            this.tbExit = new Transware.Controls.TouchButton();
            this.touchButton1 = new Transware.Controls.TouchButton();
            this.gbMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbSetting.SuspendLayout();
            this.gbShortcuts.SuspendLayout();
            this.pShortCuts.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMain
            // 
            this.gbMain.Controls.Add(this.touchButton1);
            this.gbMain.Controls.Add(this.groupBox1);
            this.gbMain.Controls.Add(this.gbSetting);
            this.gbMain.Controls.Add(this.gbShortcuts);
            this.gbMain.Controls.Add(this.label1);
            this.gbMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbMain.Location = new System.Drawing.Point(0, 0);
            this.gbMain.Name = "gbMain";
            this.gbMain.Size = new System.Drawing.Size(1024, 688);
            this.gbMain.TabIndex = 0;
            this.gbMain.TabStop = false;
            this.gbMain.Text = "Konfigurace PLU";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbEetVerify);
            this.groupBox1.Controls.Add(this.tbEetTest);
            this.groupBox1.Controls.Add(this.cbEetTesting);
            this.groupBox1.Controls.Add(this.cbEetEnabled);
            this.groupBox1.Location = new System.Drawing.Point(17, 388);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(421, 157);
            this.groupBox1.TabIndex = 52;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "EET";
            // 
            // tbEetVerify
            // 
            this.tbEetVerify.BorderColor = System.Drawing.Color.Black;
            this.tbEetVerify.BorderWidth = 2;
            this.tbEetVerify.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbEetVerify.FillColorBottom = System.Drawing.Color.Fuchsia;
            this.tbEetVerify.FillColorTop = System.Drawing.Color.Purple;
            this.tbEetVerify.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbEetVerify.ForeColor = System.Drawing.Color.Black;
            this.tbEetVerify.Image = null;
            this.tbEetVerify.Location = new System.Drawing.Point(182, 103);
            this.tbEetVerify.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tbEetVerify.Name = "tbEetVerify";
            this.tbEetVerify.RoundedCorners.BottomLeft = true;
            this.tbEetVerify.RoundedCorners.BottomRight = true;
            this.tbEetVerify.RoundedCorners.TopLeft = true;
            this.tbEetVerify.RoundedCorners.TopRight = true;
            this.tbEetVerify.RoundingDiameter = 20;
            this.tbEetVerify.Size = new System.Drawing.Size(162, 47);
            this.tbEetVerify.TabIndex = 79;
            this.tbEetVerify.TabStop = false;
            this.tbEetVerify.Text = "Ověřit plný režim";
            this.tbEetVerify.Click += new System.EventHandler(this.tbEetVerify_Click);
            // 
            // tbEetTest
            // 
            this.tbEetTest.BorderColor = System.Drawing.Color.Black;
            this.tbEetTest.BorderWidth = 2;
            this.tbEetTest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbEetTest.FillColorBottom = System.Drawing.Color.Fuchsia;
            this.tbEetTest.FillColorTop = System.Drawing.Color.Fuchsia;
            this.tbEetTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbEetTest.ForeColor = System.Drawing.Color.Black;
            this.tbEetTest.Image = null;
            this.tbEetTest.Location = new System.Drawing.Point(8, 103);
            this.tbEetTest.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tbEetTest.Name = "tbEetTest";
            this.tbEetTest.RoundedCorners.BottomLeft = true;
            this.tbEetTest.RoundedCorners.BottomRight = true;
            this.tbEetTest.RoundedCorners.TopLeft = true;
            this.tbEetTest.RoundedCorners.TopRight = true;
            this.tbEetTest.RoundingDiameter = 20;
            this.tbEetTest.Size = new System.Drawing.Size(162, 47);
            this.tbEetTest.TabIndex = 78;
            this.tbEetTest.TabStop = false;
            this.tbEetTest.Text = "Ověřit testovací zprávu";
            this.tbEetTest.Click += new System.EventHandler(this.tbEetTest_Click);
            // 
            // cbEetTesting
            // 
            this.cbEetTesting.AutoSize = true;
            this.cbEetTesting.Enabled = false;
            this.cbEetTesting.Location = new System.Drawing.Point(6, 58);
            this.cbEetTesting.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbEetTesting.Name = "cbEetTesting";
            this.cbEetTesting.Size = new System.Drawing.Size(182, 29);
            this.cbEetTesting.TabIndex = 1;
            this.cbEetTesting.Text = "Testovací režim";
            this.cbEetTesting.UseVisualStyleBackColor = true;
            // 
            // cbEetEnabled
            // 
            this.cbEetEnabled.AutoSize = true;
            this.cbEetEnabled.Enabled = false;
            this.cbEetEnabled.Location = new System.Drawing.Point(6, 27);
            this.cbEetEnabled.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbEetEnabled.Name = "cbEetEnabled";
            this.cbEetEnabled.Size = new System.Drawing.Size(110, 29);
            this.cbEetEnabled.TabIndex = 0;
            this.cbEetEnabled.Text = "Zapnuto";
            this.cbEetEnabled.UseVisualStyleBackColor = true;
            // 
            // gbSetting
            // 
            this.gbSetting.Controls.Add(this.bColor14);
            this.gbSetting.Controls.Add(this.bColor13);
            this.gbSetting.Controls.Add(this.bColor12);
            this.gbSetting.Controls.Add(this.bColor11);
            this.gbSetting.Controls.Add(this.bSaveShortcut);
            this.gbSetting.Controls.Add(this.bCancelShortcut);
            this.gbSetting.Controls.Add(this.bColor10);
            this.gbSetting.Controls.Add(this.bColor8);
            this.gbSetting.Controls.Add(this.bColor6);
            this.gbSetting.Controls.Add(this.bColor4);
            this.gbSetting.Controls.Add(this.bColor5);
            this.gbSetting.Controls.Add(this.bColor7);
            this.gbSetting.Controls.Add(this.bColor9);
            this.gbSetting.Controls.Add(this.bColor2);
            this.gbSetting.Controls.Add(this.bColor3);
            this.gbSetting.Controls.Add(this.bColor1);
            this.gbSetting.Controls.Add(this.label3);
            this.gbSetting.Controls.Add(this.bClearPackage);
            this.gbSetting.Controls.Add(this.bStoreCard);
            this.gbSetting.Controls.Add(this.tbPackage);
            this.gbSetting.Controls.Add(this.label2);
            this.gbSetting.Location = new System.Drawing.Point(444, 46);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Size = new System.Drawing.Size(563, 337);
            this.gbSetting.TabIndex = 51;
            this.gbSetting.TabStop = false;
            this.gbSetting.Text = "Nastavení PLU";
            // 
            // bColor14
            // 
            this.bColor14.BorderColor = System.Drawing.Color.White;
            this.bColor14.BorderWidth = 1;
            this.bColor14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor14.FillColorBottom = System.Drawing.Color.Magenta;
            this.bColor14.FillColorTop = System.Drawing.Color.Magenta;
            this.bColor14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor14.Image = null;
            this.bColor14.Location = new System.Drawing.Point(404, 125);
            this.bColor14.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor14.Name = "bColor14";
            this.bColor14.RoundedCorners.BottomLeft = false;
            this.bColor14.RoundedCorners.BottomRight = false;
            this.bColor14.RoundedCorners.TopLeft = false;
            this.bColor14.RoundedCorners.TopRight = false;
            this.bColor14.RoundingDiameter = 20;
            this.bColor14.Size = new System.Drawing.Size(40, 40);
            this.bColor14.TabIndex = 81;
            this.bColor14.Tag = "13";
            this.bColor14.Text = "TouchButton";
            this.bColor14.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor13
            // 
            this.bColor13.BorderColor = System.Drawing.Color.White;
            this.bColor13.BorderWidth = 1;
            this.bColor13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor13.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bColor13.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bColor13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor13.Image = null;
            this.bColor13.Location = new System.Drawing.Point(404, 73);
            this.bColor13.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor13.Name = "bColor13";
            this.bColor13.RoundedCorners.BottomLeft = false;
            this.bColor13.RoundedCorners.BottomRight = false;
            this.bColor13.RoundedCorners.TopLeft = false;
            this.bColor13.RoundedCorners.TopRight = false;
            this.bColor13.RoundingDiameter = 20;
            this.bColor13.Size = new System.Drawing.Size(40, 40);
            this.bColor13.TabIndex = 80;
            this.bColor13.Tag = "12";
            this.bColor13.Text = "TouchButton";
            this.bColor13.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor12
            // 
            this.bColor12.BorderColor = System.Drawing.Color.White;
            this.bColor12.BorderWidth = 1;
            this.bColor12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor12.FillColorBottom = System.Drawing.Color.Blue;
            this.bColor12.FillColorTop = System.Drawing.Color.Blue;
            this.bColor12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor12.Image = null;
            this.bColor12.Location = new System.Drawing.Point(352, 125);
            this.bColor12.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor12.Name = "bColor12";
            this.bColor12.RoundedCorners.BottomLeft = false;
            this.bColor12.RoundedCorners.BottomRight = false;
            this.bColor12.RoundedCorners.TopLeft = false;
            this.bColor12.RoundedCorners.TopRight = false;
            this.bColor12.RoundingDiameter = 20;
            this.bColor12.Size = new System.Drawing.Size(40, 40);
            this.bColor12.TabIndex = 79;
            this.bColor12.Tag = "11";
            this.bColor12.Text = "TouchButton";
            this.bColor12.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor11
            // 
            this.bColor11.BorderColor = System.Drawing.Color.White;
            this.bColor11.BorderWidth = 1;
            this.bColor11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor11.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bColor11.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bColor11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor11.Image = null;
            this.bColor11.Location = new System.Drawing.Point(352, 73);
            this.bColor11.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor11.Name = "bColor11";
            this.bColor11.RoundedCorners.BottomLeft = false;
            this.bColor11.RoundedCorners.BottomRight = false;
            this.bColor11.RoundedCorners.TopLeft = false;
            this.bColor11.RoundedCorners.TopRight = false;
            this.bColor11.RoundingDiameter = 20;
            this.bColor11.Size = new System.Drawing.Size(40, 40);
            this.bColor11.TabIndex = 78;
            this.bColor11.Tag = "10";
            this.bColor11.Text = "TouchButton";
            this.bColor11.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bSaveShortcut
            // 
            this.bSaveShortcut.BorderColor = System.Drawing.Color.Black;
            this.bSaveShortcut.BorderWidth = 2;
            this.bSaveShortcut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSaveShortcut.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bSaveShortcut.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bSaveShortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSaveShortcut.ForeColor = System.Drawing.Color.Black;
            this.bSaveShortcut.Image = null;
            this.bSaveShortcut.Location = new System.Drawing.Point(392, 278);
            this.bSaveShortcut.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bSaveShortcut.Name = "bSaveShortcut";
            this.bSaveShortcut.RoundedCorners.BottomLeft = true;
            this.bSaveShortcut.RoundedCorners.BottomRight = true;
            this.bSaveShortcut.RoundedCorners.TopLeft = true;
            this.bSaveShortcut.RoundedCorners.TopRight = true;
            this.bSaveShortcut.RoundingDiameter = 20;
            this.bSaveShortcut.Size = new System.Drawing.Size(162, 47);
            this.bSaveShortcut.TabIndex = 77;
            this.bSaveShortcut.TabStop = false;
            this.bSaveShortcut.Text = "Uložit";
            this.bSaveShortcut.Click += new System.EventHandler(this.bSaveShortcut_Click);
            // 
            // bCancelShortcut
            // 
            this.bCancelShortcut.BorderColor = System.Drawing.Color.Black;
            this.bCancelShortcut.BorderWidth = 2;
            this.bCancelShortcut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancelShortcut.FillColorBottom = System.Drawing.Color.Red;
            this.bCancelShortcut.FillColorTop = System.Drawing.Color.Red;
            this.bCancelShortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancelShortcut.ForeColor = System.Drawing.Color.White;
            this.bCancelShortcut.Image = null;
            this.bCancelShortcut.Location = new System.Drawing.Point(3, 278);
            this.bCancelShortcut.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bCancelShortcut.Name = "bCancelShortcut";
            this.bCancelShortcut.RoundedCorners.BottomLeft = true;
            this.bCancelShortcut.RoundedCorners.BottomRight = true;
            this.bCancelShortcut.RoundedCorners.TopLeft = true;
            this.bCancelShortcut.RoundedCorners.TopRight = true;
            this.bCancelShortcut.RoundingDiameter = 20;
            this.bCancelShortcut.Size = new System.Drawing.Size(162, 47);
            this.bCancelShortcut.TabIndex = 76;
            this.bCancelShortcut.TabStop = false;
            this.bCancelShortcut.Text = "Storno";
            this.bCancelShortcut.Click += new System.EventHandler(this.bCancelShortcut_Click);
            // 
            // bColor10
            // 
            this.bColor10.BorderColor = System.Drawing.Color.White;
            this.bColor10.BorderWidth = 1;
            this.bColor10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor10.FillColorBottom = System.Drawing.Color.Cyan;
            this.bColor10.FillColorTop = System.Drawing.Color.Cyan;
            this.bColor10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor10.Image = null;
            this.bColor10.Location = new System.Drawing.Point(301, 125);
            this.bColor10.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor10.Name = "bColor10";
            this.bColor10.RoundedCorners.BottomLeft = false;
            this.bColor10.RoundedCorners.BottomRight = false;
            this.bColor10.RoundedCorners.TopLeft = false;
            this.bColor10.RoundedCorners.TopRight = false;
            this.bColor10.RoundingDiameter = 20;
            this.bColor10.Size = new System.Drawing.Size(40, 40);
            this.bColor10.TabIndex = 75;
            this.bColor10.Tag = "9";
            this.bColor10.Text = "TouchButton";
            this.bColor10.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor8
            // 
            this.bColor8.BorderColor = System.Drawing.Color.White;
            this.bColor8.BorderWidth = 1;
            this.bColor8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor8.FillColorBottom = System.Drawing.Color.Lime;
            this.bColor8.FillColorTop = System.Drawing.Color.Lime;
            this.bColor8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor8.Image = null;
            this.bColor8.Location = new System.Drawing.Point(252, 125);
            this.bColor8.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor8.Name = "bColor8";
            this.bColor8.RoundedCorners.BottomLeft = false;
            this.bColor8.RoundedCorners.BottomRight = false;
            this.bColor8.RoundedCorners.TopLeft = false;
            this.bColor8.RoundedCorners.TopRight = false;
            this.bColor8.RoundingDiameter = 20;
            this.bColor8.Size = new System.Drawing.Size(40, 40);
            this.bColor8.TabIndex = 74;
            this.bColor8.Tag = "7";
            this.bColor8.Text = "TouchButton";
            this.bColor8.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor6
            // 
            this.bColor6.BorderColor = System.Drawing.Color.White;
            this.bColor6.BorderWidth = 1;
            this.bColor6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor6.FillColorBottom = System.Drawing.Color.Yellow;
            this.bColor6.FillColorTop = System.Drawing.Color.Yellow;
            this.bColor6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor6.Image = null;
            this.bColor6.Location = new System.Drawing.Point(203, 125);
            this.bColor6.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor6.Name = "bColor6";
            this.bColor6.RoundedCorners.BottomLeft = false;
            this.bColor6.RoundedCorners.BottomRight = false;
            this.bColor6.RoundedCorners.TopLeft = false;
            this.bColor6.RoundedCorners.TopRight = false;
            this.bColor6.RoundingDiameter = 20;
            this.bColor6.Size = new System.Drawing.Size(40, 40);
            this.bColor6.TabIndex = 73;
            this.bColor6.Tag = "5";
            this.bColor6.Text = "TouchButton";
            this.bColor6.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor4
            // 
            this.bColor4.BorderColor = System.Drawing.Color.White;
            this.bColor4.BorderWidth = 1;
            this.bColor4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor4.FillColorBottom = System.Drawing.Color.Red;
            this.bColor4.FillColorTop = System.Drawing.Color.Red;
            this.bColor4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor4.Image = null;
            this.bColor4.Location = new System.Drawing.Point(154, 125);
            this.bColor4.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor4.Name = "bColor4";
            this.bColor4.RoundedCorners.BottomLeft = false;
            this.bColor4.RoundedCorners.BottomRight = false;
            this.bColor4.RoundedCorners.TopLeft = false;
            this.bColor4.RoundedCorners.TopRight = false;
            this.bColor4.RoundingDiameter = 20;
            this.bColor4.Size = new System.Drawing.Size(40, 40);
            this.bColor4.TabIndex = 72;
            this.bColor4.Tag = "3";
            this.bColor4.Text = "TouchButton";
            this.bColor4.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor5
            // 
            this.bColor5.BorderColor = System.Drawing.Color.White;
            this.bColor5.BorderWidth = 1;
            this.bColor5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor5.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bColor5.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bColor5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor5.Image = null;
            this.bColor5.Location = new System.Drawing.Point(203, 73);
            this.bColor5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor5.Name = "bColor5";
            this.bColor5.RoundedCorners.BottomLeft = false;
            this.bColor5.RoundedCorners.BottomRight = false;
            this.bColor5.RoundedCorners.TopLeft = false;
            this.bColor5.RoundedCorners.TopRight = false;
            this.bColor5.RoundingDiameter = 20;
            this.bColor5.Size = new System.Drawing.Size(40, 40);
            this.bColor5.TabIndex = 71;
            this.bColor5.Tag = "4";
            this.bColor5.Text = "TouchButton";
            this.bColor5.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor7
            // 
            this.bColor7.BorderColor = System.Drawing.Color.White;
            this.bColor7.BorderWidth = 1;
            this.bColor7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor7.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bColor7.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bColor7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor7.Image = null;
            this.bColor7.Location = new System.Drawing.Point(252, 73);
            this.bColor7.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor7.Name = "bColor7";
            this.bColor7.RoundedCorners.BottomLeft = false;
            this.bColor7.RoundedCorners.BottomRight = false;
            this.bColor7.RoundedCorners.TopLeft = false;
            this.bColor7.RoundedCorners.TopRight = false;
            this.bColor7.RoundingDiameter = 20;
            this.bColor7.Size = new System.Drawing.Size(40, 40);
            this.bColor7.TabIndex = 70;
            this.bColor7.Tag = "6";
            this.bColor7.Text = "TouchButton";
            this.bColor7.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor9
            // 
            this.bColor9.BorderColor = System.Drawing.Color.White;
            this.bColor9.BorderWidth = 1;
            this.bColor9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor9.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bColor9.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bColor9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor9.Image = null;
            this.bColor9.Location = new System.Drawing.Point(301, 73);
            this.bColor9.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor9.Name = "bColor9";
            this.bColor9.RoundedCorners.BottomLeft = false;
            this.bColor9.RoundedCorners.BottomRight = false;
            this.bColor9.RoundedCorners.TopLeft = false;
            this.bColor9.RoundedCorners.TopRight = false;
            this.bColor9.RoundingDiameter = 20;
            this.bColor9.Size = new System.Drawing.Size(40, 40);
            this.bColor9.TabIndex = 69;
            this.bColor9.Tag = "8";
            this.bColor9.Text = "TouchButton";
            this.bColor9.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor2
            // 
            this.bColor2.BorderColor = System.Drawing.Color.White;
            this.bColor2.BorderWidth = 1;
            this.bColor2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor2.FillColorBottom = System.Drawing.Color.Silver;
            this.bColor2.FillColorTop = System.Drawing.Color.Silver;
            this.bColor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor2.Image = null;
            this.bColor2.Location = new System.Drawing.Point(105, 125);
            this.bColor2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor2.Name = "bColor2";
            this.bColor2.RoundedCorners.BottomLeft = false;
            this.bColor2.RoundedCorners.BottomRight = false;
            this.bColor2.RoundedCorners.TopLeft = false;
            this.bColor2.RoundedCorners.TopRight = false;
            this.bColor2.RoundingDiameter = 20;
            this.bColor2.Size = new System.Drawing.Size(40, 40);
            this.bColor2.TabIndex = 68;
            this.bColor2.Tag = "1";
            this.bColor2.Text = "TouchButton";
            this.bColor2.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor3
            // 
            this.bColor3.BorderColor = System.Drawing.Color.White;
            this.bColor3.BorderWidth = 1;
            this.bColor3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor3.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bColor3.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bColor3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor3.Image = null;
            this.bColor3.Location = new System.Drawing.Point(154, 73);
            this.bColor3.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor3.Name = "bColor3";
            this.bColor3.RoundedCorners.BottomLeft = false;
            this.bColor3.RoundedCorners.BottomRight = false;
            this.bColor3.RoundedCorners.TopLeft = false;
            this.bColor3.RoundedCorners.TopRight = false;
            this.bColor3.RoundingDiameter = 20;
            this.bColor3.Size = new System.Drawing.Size(40, 40);
            this.bColor3.TabIndex = 67;
            this.bColor3.Tag = "2";
            this.bColor3.Text = "TouchButton";
            this.bColor3.Click += new System.EventHandler(this.bColor_Click);
            // 
            // bColor1
            // 
            this.bColor1.BorderColor = System.Drawing.Color.White;
            this.bColor1.BorderWidth = 1;
            this.bColor1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bColor1.FillColorBottom = System.Drawing.Color.White;
            this.bColor1.FillColorTop = System.Drawing.Color.White;
            this.bColor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bColor1.Image = null;
            this.bColor1.Location = new System.Drawing.Point(105, 73);
            this.bColor1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bColor1.Name = "bColor1";
            this.bColor1.RoundedCorners.BottomLeft = false;
            this.bColor1.RoundedCorners.BottomRight = false;
            this.bColor1.RoundedCorners.TopLeft = false;
            this.bColor1.RoundedCorners.TopRight = false;
            this.bColor1.RoundingDiameter = 20;
            this.bColor1.Size = new System.Drawing.Size(40, 40);
            this.bColor1.TabIndex = 66;
            this.bColor1.Tag = "0";
            this.bColor1.Text = "TouchButton";
            this.bColor1.Click += new System.EventHandler(this.bColor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 25);
            this.label3.TabIndex = 65;
            this.label3.Text = "Barva:";
            // 
            // bClearPackage
            // 
            this.bClearPackage.BorderColor = System.Drawing.Color.Black;
            this.bClearPackage.BorderWidth = 2;
            this.bClearPackage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bClearPackage.FillColorBottom = System.Drawing.Color.Red;
            this.bClearPackage.FillColorTop = System.Drawing.Color.Red;
            this.bClearPackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bClearPackage.ForeColor = System.Drawing.Color.White;
            this.bClearPackage.Image = null;
            this.bClearPackage.Location = new System.Drawing.Point(455, 32);
            this.bClearPackage.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bClearPackage.Name = "bClearPackage";
            this.bClearPackage.RoundedCorners.BottomLeft = true;
            this.bClearPackage.RoundedCorners.BottomRight = true;
            this.bClearPackage.RoundedCorners.TopLeft = true;
            this.bClearPackage.RoundedCorners.TopRight = true;
            this.bClearPackage.RoundingDiameter = 20;
            this.bClearPackage.Size = new System.Drawing.Size(100, 31);
            this.bClearPackage.TabIndex = 64;
            this.bClearPackage.TabStop = false;
            this.bClearPackage.Text = "Vymazat";
            this.bClearPackage.Click += new System.EventHandler(this.bClearPackage_Click);
            // 
            // bStoreCard
            // 
            this.bStoreCard.BorderColor = System.Drawing.Color.Black;
            this.bStoreCard.BorderWidth = 2;
            this.bStoreCard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bStoreCard.FillColorBottom = System.Drawing.Color.Magenta;
            this.bStoreCard.FillColorTop = System.Drawing.Color.Magenta;
            this.bStoreCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bStoreCard.ForeColor = System.Drawing.Color.Black;
            this.bStoreCard.Image = null;
            this.bStoreCard.Location = new System.Drawing.Point(352, 32);
            this.bStoreCard.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bStoreCard.Name = "bStoreCard";
            this.bStoreCard.RoundedCorners.BottomLeft = true;
            this.bStoreCard.RoundedCorners.BottomRight = true;
            this.bStoreCard.RoundedCorners.TopLeft = true;
            this.bStoreCard.RoundedCorners.TopRight = true;
            this.bStoreCard.RoundingDiameter = 20;
            this.bStoreCard.Size = new System.Drawing.Size(100, 31);
            this.bStoreCard.TabIndex = 64;
            this.bStoreCard.TabStop = false;
            this.bStoreCard.Text = "Sortiment";
            this.bStoreCard.Click += new System.EventHandler(this.bStoreCard_Click);
            // 
            // tbPackage
            // 
            this.tbPackage.Location = new System.Drawing.Point(105, 32);
            this.tbPackage.Name = "tbPackage";
            this.tbPackage.Size = new System.Drawing.Size(239, 31);
            this.tbPackage.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Produkt:";
            // 
            // gbShortcuts
            // 
            this.gbShortcuts.Controls.Add(this.pShortCuts);
            this.gbShortcuts.Location = new System.Drawing.Point(17, 46);
            this.gbShortcuts.Name = "gbShortcuts";
            this.gbShortcuts.Size = new System.Drawing.Size(421, 337);
            this.gbShortcuts.TabIndex = 50;
            this.gbShortcuts.TabStop = false;
            this.gbShortcuts.Text = "PLU";
            // 
            // pShortCuts
            // 
            this.pShortCuts.Controls.Add(this.lPage);
            this.pShortCuts.Controls.Add(this.bRight);
            this.pShortCuts.Controls.Add(this.bLeft);
            this.pShortCuts.Controls.Add(this.bItem16);
            this.pShortCuts.Controls.Add(this.bItem17);
            this.pShortCuts.Controls.Add(this.bItem18);
            this.pShortCuts.Controls.Add(this.bItem13);
            this.pShortCuts.Controls.Add(this.bItem14);
            this.pShortCuts.Controls.Add(this.bItem15);
            this.pShortCuts.Controls.Add(this.bItem10);
            this.pShortCuts.Controls.Add(this.bItem11);
            this.pShortCuts.Controls.Add(this.bItem12);
            this.pShortCuts.Controls.Add(this.bItem7);
            this.pShortCuts.Controls.Add(this.bItem8);
            this.pShortCuts.Controls.Add(this.bItem9);
            this.pShortCuts.Controls.Add(this.bItem4);
            this.pShortCuts.Controls.Add(this.bItem5);
            this.pShortCuts.Controls.Add(this.bItem6);
            this.pShortCuts.Controls.Add(this.bItem1);
            this.pShortCuts.Controls.Add(this.bItem2);
            this.pShortCuts.Controls.Add(this.bItem3);
            this.pShortCuts.Location = new System.Drawing.Point(6, 30);
            this.pShortCuts.Name = "pShortCuts";
            this.pShortCuts.Size = new System.Drawing.Size(404, 299);
            this.pShortCuts.TabIndex = 48;
            // 
            // lPage
            // 
            this.lPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPage.Location = new System.Drawing.Point(54, 253);
            this.lPage.Name = "lPage";
            this.lPage.Size = new System.Drawing.Size(296, 42);
            this.lPage.TabIndex = 37;
            this.lPage.Text = "Strana";
            this.lPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bRight
            // 
            this.bRight.BorderColor = System.Drawing.Color.Black;
            this.bRight.BorderWidth = 1;
            this.bRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.bRight.Location = new System.Drawing.Point(356, 253);
            this.bRight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bRight.Name = "bRight";
            this.bRight.RoundedCorners.BottomLeft = true;
            this.bRight.RoundedCorners.BottomRight = true;
            this.bRight.RoundedCorners.TopLeft = true;
            this.bRight.RoundedCorners.TopRight = true;
            this.bRight.RoundingDiameter = 15;
            this.bRight.Size = new System.Drawing.Size(45, 42);
            this.bRight.TabIndex = 36;
            this.bRight.TabStop = false;
            this.bRight.Text = "Položka";
            this.bRight.Click += new System.EventHandler(this.bRight_Click);
            // 
            // bLeft
            // 
            this.bLeft.BorderColor = System.Drawing.Color.Black;
            this.bLeft.BorderWidth = 1;
            this.bLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.bLeft.Location = new System.Drawing.Point(3, 253);
            this.bLeft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bLeft.Name = "bLeft";
            this.bLeft.RoundedCorners.BottomLeft = true;
            this.bLeft.RoundedCorners.BottomRight = true;
            this.bLeft.RoundedCorners.TopLeft = true;
            this.bLeft.RoundedCorners.TopRight = true;
            this.bLeft.RoundingDiameter = 15;
            this.bLeft.Size = new System.Drawing.Size(45, 42);
            this.bLeft.TabIndex = 35;
            this.bLeft.TabStop = false;
            this.bLeft.Text = "Položka";
            this.bLeft.Click += new System.EventHandler(this.bLeft_Click);
            // 
            // bItem16
            // 
            this.bItem16.BorderColor = System.Drawing.Color.Black;
            this.bItem16.BorderWidth = 1;
            this.bItem16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem16.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem16.Image = null;
            this.bItem16.Location = new System.Drawing.Point(2, 205);
            this.bItem16.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem16.Name = "bItem16";
            this.bItem16.RoundedCorners.BottomLeft = true;
            this.bItem16.RoundedCorners.BottomRight = true;
            this.bItem16.RoundedCorners.TopLeft = true;
            this.bItem16.RoundedCorners.TopRight = true;
            this.bItem16.RoundingDiameter = 15;
            this.bItem16.Size = new System.Drawing.Size(130, 35);
            this.bItem16.TabIndex = 32;
            this.bItem16.TabStop = false;
            this.bItem16.Text = "Položka";
            this.bItem16.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem17
            // 
            this.bItem17.BorderColor = System.Drawing.Color.Black;
            this.bItem17.BorderWidth = 1;
            this.bItem17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem17.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem17.Image = null;
            this.bItem17.Location = new System.Drawing.Point(138, 205);
            this.bItem17.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem17.Name = "bItem17";
            this.bItem17.RoundedCorners.BottomLeft = true;
            this.bItem17.RoundedCorners.BottomRight = true;
            this.bItem17.RoundedCorners.TopLeft = true;
            this.bItem17.RoundedCorners.TopRight = true;
            this.bItem17.RoundingDiameter = 15;
            this.bItem17.Size = new System.Drawing.Size(130, 35);
            this.bItem17.TabIndex = 33;
            this.bItem17.TabStop = false;
            this.bItem17.Text = "Položka";
            this.bItem17.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem18
            // 
            this.bItem18.BorderColor = System.Drawing.Color.Black;
            this.bItem18.BorderWidth = 1;
            this.bItem18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem18.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem18.Image = null;
            this.bItem18.Location = new System.Drawing.Point(274, 205);
            this.bItem18.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem18.Name = "bItem18";
            this.bItem18.RoundedCorners.BottomLeft = true;
            this.bItem18.RoundedCorners.BottomRight = true;
            this.bItem18.RoundedCorners.TopLeft = true;
            this.bItem18.RoundedCorners.TopRight = true;
            this.bItem18.RoundingDiameter = 15;
            this.bItem18.Size = new System.Drawing.Size(130, 35);
            this.bItem18.TabIndex = 34;
            this.bItem18.TabStop = false;
            this.bItem18.Text = "Položka";
            this.bItem18.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem13
            // 
            this.bItem13.BorderColor = System.Drawing.Color.Black;
            this.bItem13.BorderWidth = 1;
            this.bItem13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem13.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem13.Image = null;
            this.bItem13.Location = new System.Drawing.Point(2, 164);
            this.bItem13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem13.Name = "bItem13";
            this.bItem13.RoundedCorners.BottomLeft = true;
            this.bItem13.RoundedCorners.BottomRight = true;
            this.bItem13.RoundedCorners.TopLeft = true;
            this.bItem13.RoundedCorners.TopRight = true;
            this.bItem13.RoundingDiameter = 15;
            this.bItem13.Size = new System.Drawing.Size(130, 35);
            this.bItem13.TabIndex = 29;
            this.bItem13.TabStop = false;
            this.bItem13.Text = "Položka";
            this.bItem13.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem14
            // 
            this.bItem14.BorderColor = System.Drawing.Color.Black;
            this.bItem14.BorderWidth = 1;
            this.bItem14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem14.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem14.Image = null;
            this.bItem14.Location = new System.Drawing.Point(138, 164);
            this.bItem14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem14.Name = "bItem14";
            this.bItem14.RoundedCorners.BottomLeft = true;
            this.bItem14.RoundedCorners.BottomRight = true;
            this.bItem14.RoundedCorners.TopLeft = true;
            this.bItem14.RoundedCorners.TopRight = true;
            this.bItem14.RoundingDiameter = 15;
            this.bItem14.Size = new System.Drawing.Size(130, 35);
            this.bItem14.TabIndex = 30;
            this.bItem14.TabStop = false;
            this.bItem14.Text = "Položka";
            this.bItem14.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem15
            // 
            this.bItem15.BorderColor = System.Drawing.Color.Black;
            this.bItem15.BorderWidth = 1;
            this.bItem15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem15.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem15.Image = null;
            this.bItem15.Location = new System.Drawing.Point(274, 164);
            this.bItem15.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem15.Name = "bItem15";
            this.bItem15.RoundedCorners.BottomLeft = true;
            this.bItem15.RoundedCorners.BottomRight = true;
            this.bItem15.RoundedCorners.TopLeft = true;
            this.bItem15.RoundedCorners.TopRight = true;
            this.bItem15.RoundingDiameter = 15;
            this.bItem15.Size = new System.Drawing.Size(130, 35);
            this.bItem15.TabIndex = 31;
            this.bItem15.TabStop = false;
            this.bItem15.Text = "Položka";
            this.bItem15.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem10
            // 
            this.bItem10.BorderColor = System.Drawing.Color.Black;
            this.bItem10.BorderWidth = 1;
            this.bItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem10.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem10.Image = null;
            this.bItem10.Location = new System.Drawing.Point(2, 123);
            this.bItem10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem10.Name = "bItem10";
            this.bItem10.RoundedCorners.BottomLeft = true;
            this.bItem10.RoundedCorners.BottomRight = true;
            this.bItem10.RoundedCorners.TopLeft = true;
            this.bItem10.RoundedCorners.TopRight = true;
            this.bItem10.RoundingDiameter = 15;
            this.bItem10.Size = new System.Drawing.Size(130, 35);
            this.bItem10.TabIndex = 26;
            this.bItem10.TabStop = false;
            this.bItem10.Text = "Položka";
            this.bItem10.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem11
            // 
            this.bItem11.BorderColor = System.Drawing.Color.Black;
            this.bItem11.BorderWidth = 1;
            this.bItem11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem11.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem11.Image = null;
            this.bItem11.Location = new System.Drawing.Point(138, 123);
            this.bItem11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem11.Name = "bItem11";
            this.bItem11.RoundedCorners.BottomLeft = true;
            this.bItem11.RoundedCorners.BottomRight = true;
            this.bItem11.RoundedCorners.TopLeft = true;
            this.bItem11.RoundedCorners.TopRight = true;
            this.bItem11.RoundingDiameter = 15;
            this.bItem11.Size = new System.Drawing.Size(130, 35);
            this.bItem11.TabIndex = 27;
            this.bItem11.TabStop = false;
            this.bItem11.Text = "Položka";
            this.bItem11.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem12
            // 
            this.bItem12.BorderColor = System.Drawing.Color.Black;
            this.bItem12.BorderWidth = 1;
            this.bItem12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem12.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem12.Image = null;
            this.bItem12.Location = new System.Drawing.Point(274, 123);
            this.bItem12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem12.Name = "bItem12";
            this.bItem12.RoundedCorners.BottomLeft = true;
            this.bItem12.RoundedCorners.BottomRight = true;
            this.bItem12.RoundedCorners.TopLeft = true;
            this.bItem12.RoundedCorners.TopRight = true;
            this.bItem12.RoundingDiameter = 15;
            this.bItem12.Size = new System.Drawing.Size(130, 35);
            this.bItem12.TabIndex = 28;
            this.bItem12.TabStop = false;
            this.bItem12.Text = "Položka";
            this.bItem12.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem7
            // 
            this.bItem7.BorderColor = System.Drawing.Color.Black;
            this.bItem7.BorderWidth = 1;
            this.bItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem7.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem7.Image = null;
            this.bItem7.Location = new System.Drawing.Point(2, 82);
            this.bItem7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem7.Name = "bItem7";
            this.bItem7.RoundedCorners.BottomLeft = true;
            this.bItem7.RoundedCorners.BottomRight = true;
            this.bItem7.RoundedCorners.TopLeft = true;
            this.bItem7.RoundedCorners.TopRight = true;
            this.bItem7.RoundingDiameter = 15;
            this.bItem7.Size = new System.Drawing.Size(130, 35);
            this.bItem7.TabIndex = 23;
            this.bItem7.TabStop = false;
            this.bItem7.Text = "Položka";
            this.bItem7.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem8
            // 
            this.bItem8.BorderColor = System.Drawing.Color.Black;
            this.bItem8.BorderWidth = 1;
            this.bItem8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem8.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem8.Image = null;
            this.bItem8.Location = new System.Drawing.Point(138, 82);
            this.bItem8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem8.Name = "bItem8";
            this.bItem8.RoundedCorners.BottomLeft = true;
            this.bItem8.RoundedCorners.BottomRight = true;
            this.bItem8.RoundedCorners.TopLeft = true;
            this.bItem8.RoundedCorners.TopRight = true;
            this.bItem8.RoundingDiameter = 15;
            this.bItem8.Size = new System.Drawing.Size(130, 35);
            this.bItem8.TabIndex = 24;
            this.bItem8.TabStop = false;
            this.bItem8.Text = "Položka";
            this.bItem8.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem9
            // 
            this.bItem9.BorderColor = System.Drawing.Color.Black;
            this.bItem9.BorderWidth = 1;
            this.bItem9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem9.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem9.Image = null;
            this.bItem9.Location = new System.Drawing.Point(274, 82);
            this.bItem9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem9.Name = "bItem9";
            this.bItem9.RoundedCorners.BottomLeft = true;
            this.bItem9.RoundedCorners.BottomRight = true;
            this.bItem9.RoundedCorners.TopLeft = true;
            this.bItem9.RoundedCorners.TopRight = true;
            this.bItem9.RoundingDiameter = 15;
            this.bItem9.Size = new System.Drawing.Size(130, 35);
            this.bItem9.TabIndex = 25;
            this.bItem9.TabStop = false;
            this.bItem9.Text = "Položka";
            this.bItem9.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem4
            // 
            this.bItem4.BorderColor = System.Drawing.Color.Black;
            this.bItem4.BorderWidth = 1;
            this.bItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem4.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem4.Image = null;
            this.bItem4.Location = new System.Drawing.Point(2, 41);
            this.bItem4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem4.Name = "bItem4";
            this.bItem4.RoundedCorners.BottomLeft = true;
            this.bItem4.RoundedCorners.BottomRight = true;
            this.bItem4.RoundedCorners.TopLeft = true;
            this.bItem4.RoundedCorners.TopRight = true;
            this.bItem4.RoundingDiameter = 15;
            this.bItem4.Size = new System.Drawing.Size(130, 35);
            this.bItem4.TabIndex = 20;
            this.bItem4.TabStop = false;
            this.bItem4.Text = "Položka";
            this.bItem4.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem5
            // 
            this.bItem5.BorderColor = System.Drawing.Color.Black;
            this.bItem5.BorderWidth = 1;
            this.bItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem5.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem5.Image = null;
            this.bItem5.Location = new System.Drawing.Point(138, 41);
            this.bItem5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem5.Name = "bItem5";
            this.bItem5.RoundedCorners.BottomLeft = true;
            this.bItem5.RoundedCorners.BottomRight = true;
            this.bItem5.RoundedCorners.TopLeft = true;
            this.bItem5.RoundedCorners.TopRight = true;
            this.bItem5.RoundingDiameter = 15;
            this.bItem5.Size = new System.Drawing.Size(130, 35);
            this.bItem5.TabIndex = 21;
            this.bItem5.TabStop = false;
            this.bItem5.Text = "Položka";
            this.bItem5.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem6
            // 
            this.bItem6.BorderColor = System.Drawing.Color.Black;
            this.bItem6.BorderWidth = 1;
            this.bItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem6.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem6.Image = null;
            this.bItem6.Location = new System.Drawing.Point(274, 41);
            this.bItem6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem6.Name = "bItem6";
            this.bItem6.RoundedCorners.BottomLeft = true;
            this.bItem6.RoundedCorners.BottomRight = true;
            this.bItem6.RoundedCorners.TopLeft = true;
            this.bItem6.RoundedCorners.TopRight = true;
            this.bItem6.RoundingDiameter = 15;
            this.bItem6.Size = new System.Drawing.Size(130, 35);
            this.bItem6.TabIndex = 22;
            this.bItem6.TabStop = false;
            this.bItem6.Text = "Položka";
            this.bItem6.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem1
            // 
            this.bItem1.BorderColor = System.Drawing.Color.Black;
            this.bItem1.BorderWidth = 1;
            this.bItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem1.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem1.Image = null;
            this.bItem1.Location = new System.Drawing.Point(2, 0);
            this.bItem1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem1.Name = "bItem1";
            this.bItem1.RoundedCorners.BottomLeft = true;
            this.bItem1.RoundedCorners.BottomRight = true;
            this.bItem1.RoundedCorners.TopLeft = true;
            this.bItem1.RoundedCorners.TopRight = true;
            this.bItem1.RoundingDiameter = 15;
            this.bItem1.Size = new System.Drawing.Size(130, 35);
            this.bItem1.TabIndex = 17;
            this.bItem1.TabStop = false;
            this.bItem1.Text = "Položka";
            this.bItem1.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem2
            // 
            this.bItem2.BorderColor = System.Drawing.Color.Black;
            this.bItem2.BorderWidth = 1;
            this.bItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem2.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem2.Image = null;
            this.bItem2.Location = new System.Drawing.Point(138, 0);
            this.bItem2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem2.Name = "bItem2";
            this.bItem2.RoundedCorners.BottomLeft = true;
            this.bItem2.RoundedCorners.BottomRight = true;
            this.bItem2.RoundedCorners.TopLeft = true;
            this.bItem2.RoundedCorners.TopRight = true;
            this.bItem2.RoundingDiameter = 15;
            this.bItem2.Size = new System.Drawing.Size(130, 35);
            this.bItem2.TabIndex = 18;
            this.bItem2.TabStop = false;
            this.bItem2.Text = "Položka";
            this.bItem2.Click += new System.EventHandler(this.bItem_Click);
            // 
            // bItem3
            // 
            this.bItem3.BorderColor = System.Drawing.Color.Black;
            this.bItem3.BorderWidth = 1;
            this.bItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem3.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bItem3.Image = null;
            this.bItem3.Location = new System.Drawing.Point(274, 0);
            this.bItem3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bItem3.Name = "bItem3";
            this.bItem3.RoundedCorners.BottomLeft = true;
            this.bItem3.RoundedCorners.BottomRight = true;
            this.bItem3.RoundedCorners.TopLeft = true;
            this.bItem3.RoundedCorners.TopRight = true;
            this.bItem3.RoundingDiameter = 15;
            this.bItem3.Size = new System.Drawing.Size(130, 35);
            this.bItem3.TabIndex = 19;
            this.bItem3.TabStop = false;
            this.bItem3.Text = "Položka";
            this.bItem3.Click += new System.EventHandler(this.bItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 647);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(878, 25);
            this.label1.TabIndex = 49;
            this.label1.Text = "Zvolte tlačítko, vyberte ze sortimentu zvolený produkt a zvolte barvu. Poté změny" +
    " potvrďte.";
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(17, 698);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 62;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.BorderColor = System.Drawing.Color.Black;
            this.bOK.BorderWidth = 2;
            this.bOK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bOK.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bOK.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bOK.ForeColor = System.Drawing.Color.Black;
            this.bOK.Image = null;
            this.bOK.Location = new System.Drawing.Point(804, 698);
            this.bOK.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bOK.Name = "bOK";
            this.bOK.RoundedCorners.BottomLeft = true;
            this.bOK.RoundedCorners.BottomRight = true;
            this.bOK.RoundedCorners.TopLeft = true;
            this.bOK.RoundedCorners.TopRight = true;
            this.bOK.RoundingDiameter = 20;
            this.bOK.Size = new System.Drawing.Size(203, 57);
            this.bOK.TabIndex = 63;
            this.bOK.TabStop = false;
            this.bOK.Text = "OK";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // tbExit
            // 
            this.tbExit.BorderColor = System.Drawing.Color.Black;
            this.tbExit.BorderWidth = 2;
            this.tbExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbExit.FillColorBottom = System.Drawing.Color.Black;
            this.tbExit.FillColorTop = System.Drawing.Color.Black;
            this.tbExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbExit.ForeColor = System.Drawing.Color.White;
            this.tbExit.Image = null;
            this.tbExit.Location = new System.Drawing.Point(406, 698);
            this.tbExit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbExit.Name = "tbExit";
            this.tbExit.RoundedCorners.BottomLeft = true;
            this.tbExit.RoundedCorners.BottomRight = true;
            this.tbExit.RoundedCorners.TopLeft = true;
            this.tbExit.RoundedCorners.TopRight = true;
            this.tbExit.RoundingDiameter = 20;
            this.tbExit.Size = new System.Drawing.Size(203, 57);
            this.tbExit.TabIndex = 64;
            this.tbExit.TabStop = false;
            this.tbExit.Text = "VYPNOUT";
            this.tbExit.Click += new System.EventHandler(this.tbExit_Click);
            // 
            // touchButton1
            // 
            this.touchButton1.BorderColor = System.Drawing.Color.Black;
            this.touchButton1.BorderWidth = 2;
            this.touchButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.touchButton1.FillColorBottom = System.Drawing.Color.Fuchsia;
            this.touchButton1.FillColorTop = System.Drawing.Color.Purple;
            this.touchButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.touchButton1.ForeColor = System.Drawing.Color.Black;
            this.touchButton1.Image = null;
            this.touchButton1.Location = new System.Drawing.Point(447, 491);
            this.touchButton1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.touchButton1.Name = "touchButton1";
            this.touchButton1.RoundedCorners.BottomLeft = true;
            this.touchButton1.RoundedCorners.BottomRight = true;
            this.touchButton1.RoundedCorners.TopLeft = true;
            this.touchButton1.RoundedCorners.TopRight = true;
            this.touchButton1.RoundingDiameter = 20;
            this.touchButton1.Size = new System.Drawing.Size(162, 47);
            this.touchButton1.TabIndex = 79;
            this.touchButton1.TabStop = false;
            this.touchButton1.Text = "Ověřit tisk";
            this.touchButton1.Click += new System.EventHandler(this.touchButton1_Click);
            // 
            // ShortcutConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 593);
            this.Controls.Add(this.tbExit);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.gbMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 558);
            this.Name = "ShortcutConfigForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.gbMain.ResumeLayout(false);
            this.gbMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbSetting.ResumeLayout(false);
            this.gbSetting.PerformLayout();
            this.gbShortcuts.ResumeLayout(false);
            this.pShortCuts.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMain;
        private Controls.TouchButton bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pShortCuts;
        private System.Windows.Forms.Label lPage;
        private Controls.TouchButton bRight;
        private Controls.TouchButton bLeft;
        private Controls.TouchButton bItem16;
        private Controls.TouchButton bItem17;
        private Controls.TouchButton bItem18;
        private Controls.TouchButton bItem13;
        private Controls.TouchButton bItem14;
        private Controls.TouchButton bItem15;
        private Controls.TouchButton bItem10;
        private Controls.TouchButton bItem11;
        private Controls.TouchButton bItem12;
        private Controls.TouchButton bItem7;
        private Controls.TouchButton bItem8;
        private Controls.TouchButton bItem9;
        private Controls.TouchButton bItem4;
        private Controls.TouchButton bItem5;
        private Controls.TouchButton bItem6;
        private Controls.TouchButton bItem1;
        private Controls.TouchButton bItem2;
        private Controls.TouchButton bItem3;
        private Controls.TouchButton bOK;
        private System.Windows.Forms.GroupBox gbShortcuts;
        private System.Windows.Forms.GroupBox gbSetting;
        private Controls.TouchButton bStoreCard;
        private System.Windows.Forms.TextBox tbPackage;
        private System.Windows.Forms.Label label2;
        private Controls.TouchButton bSaveShortcut;
        private Controls.TouchButton bCancelShortcut;
        private Controls.TouchButton bColor10;
        private Controls.TouchButton bColor8;
        private Controls.TouchButton bColor6;
        private Controls.TouchButton bColor4;
        private Controls.TouchButton bColor5;
        private Controls.TouchButton bColor7;
        private Controls.TouchButton bColor9;
        private Controls.TouchButton bColor2;
        private Controls.TouchButton bColor3;
        private Controls.TouchButton bColor1;
        private System.Windows.Forms.Label label3;
        private Controls.TouchButton bClearPackage;
        private Controls.TouchButton bColor14;
        private Controls.TouchButton bColor13;
        private Controls.TouchButton bColor12;
        private Controls.TouchButton bColor11;
        private Controls.TouchButton tbExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbEetTesting;
        private System.Windows.Forms.CheckBox cbEetEnabled;
        private Controls.TouchButton tbEetVerify;
        private Controls.TouchButton tbEetTest;
        private Controls.TouchButton touchButton1;
    }
}

