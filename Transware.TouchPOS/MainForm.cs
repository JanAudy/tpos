﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Transware.Data.DataTypes;
using Transware.Data.DataModel;
using Jadro.Data.DataTypes;
using Jadro.Data;
using Jadro.Data.DataModel;
using Microdata.Util;

namespace Transware.TouchPOS
{
    public partial class MainForm : Form
    {
        private Bill bill;
        private Category root = new Category();
        private List<TouchButton> shortcutsButtons = new List<TouchButton>();
        private List<ShortcutInfo[]> shortcuts = null;
        private int actualPage = 0;
        private ConfigClass config = null;
        private IDMedia media = null;
        private UserSelectForm selectForm = null;
        public MainForm()
        {
            InitializeComponent();

            config = ConfigClassModel.Instance.GetConfiguration();;

            InitBill();

            tbExit.Visible = Setting.Instance.LoginRequired;

            selectForm = new UserSelectForm(true);

            shortcuts = ShortcutIO.Load();

            tbProduct.BackColor = Color.White;
            tbProduct.ForeColor = Color.Black;

            shortcutsButtons.Add(bItem1);
            shortcutsButtons.Add(bItem2);
            shortcutsButtons.Add(bItem3);
            shortcutsButtons.Add(bItem4);
            shortcutsButtons.Add(bItem5);
            shortcutsButtons.Add(bItem6);
            shortcutsButtons.Add(bItem7);
            shortcutsButtons.Add(bItem8);
            shortcutsButtons.Add(bItem9);
            shortcutsButtons.Add(bItem10);
            shortcutsButtons.Add(bItem11);
            shortcutsButtons.Add(bItem12);
            shortcutsButtons.Add(bItem13);
            shortcutsButtons.Add(bItem14);
            shortcutsButtons.Add(bItem15);
            shortcutsButtons.Add(bItem16);
            shortcutsButtons.Add(bItem17);
            shortcutsButtons.Add(bItem18);

            DrawCustomer();

            DrawShortcuts();

            DrawItems();            

            InitDisplay();
        }

        private void InitBill()
        {
            bill = new Bill() { ItemsCurrency = Setting.Instance.ItemsCurrency, Store = Setting.Instance.Store };
        }

        private void InitDisplay()
        {
            Display.Instance.DrawLines(config.Display1Line, config.Display2Line);
        }

        private void DrawShortcuts()
        {
            ShortcutInfo[] array = shortcuts[actualPage];
            for (int i = 0; i < ShortcutIO.ItemsPerPage; i++)
            {
                ShortcutInfo p = array[i];
                if (p.Package == null)
                {
                    shortcutsButtons[i].Text = "";
                    shortcutsButtons[i].Tag = p.Package;
                    shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = false;
                }
                else
                {
                    shortcutsButtons[i].Text = p.Package.Name;
                    shortcutsButtons[i].Tag = p.Package;
                    shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = true;
                }
            }

            bLeft.Visible = actualPage > 0;
            bRight.Visible = actualPage < shortcuts.Count - 1;

            lPage.Text = "Strana " + (actualPage + 1);
        }

        private void NumberButtonClick(object sender, EventArgs e)
        {
            tbProduct.Text += (sender as TouchButton).Tag.ToString();
        }

        private void ClearCodeClick(object sender, EventArgs e)
        {
            tbProduct.Text = "";
        }

        private void DeleteLastClick(object sender, EventArgs e)
        {
            if (tbProduct.Text.Length>0)
                tbProduct.Text = tbProduct.Text.Substring(0, tbProduct.Text.Length - 1);
        }

        private void ConfirmProduct(object sender, EventArgs e)
        {
            string code = tbProduct.Text.Trim();
            if (string.IsNullOrEmpty(code))
                return;

            tbProduct.Text = "";
            bool minus = false;
            if (code.StartsWith("-"))
            {
                minus = true;
                code = code.Substring(1);
            }

            decimal multiple = 1;
            if (code.Contains('*'))
            {
                string[] parts = code.Split(" *".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    if (!decimal.TryParse(parts[0], out multiple))
                    {
                        MessageBox.Show("Množství není číslo");
                    }
                    code = parts[1];
                }
                catch
                {
                    return;
                }
            }

            List<PackageSummary> packages = PackageSummaryModel.Instance.GetList(Setting.Instance.Store, code);
            if (packages.Count>0)
            {
                PackageSummary package = packages[0];
                if (minus)
                    bill.Remove(package, multiple);
                else
                {
                    decimal amount = bill.CheckCount(package);
                    amount += multiple;
                    if (Setting.Instance.ForbidSellToMinus)
                    {
                        Product product = ProductModel.Instance.Get(package.ProductID);
                        if (!product.Setting.UndefinedAmount)
                        {
                            decimal storeAmount = 0;
                            if (package.VariantID != 0)
                                storeAmount = StoreProductModel.Instance.StoreAmountByVariant(package.VariantID, Setting.Instance.Store);
                            else
                                storeAmount = StoreProductModel.Instance.StoreAmountByProducts(package.ProductID, Setting.Instance.Store);
                            if (storeAmount < amount)
                            {
                                MessageBox.Show("Zboží není v dostatečném množství na skladě");
                                return;
                            }
                        }
                    }
                    //int count = bill.Items.Count;
                    bill.Add(package, multiple);
                    //if (count < bill.Items.Count)
                    //{ 
                    //    Product product = ProductModel.Instance.Get(package.ProductID);
                    //    if (product.Setting.IndividualPrice)
                    //    {
                    //        PriceChangeForm form = new PriceChangeForm(bill.Items[bill.Items.Count-1]);

                    //        form.ShowDialog();
                    //    }
                    //}
                }

                UpdateDisplay(package, multiple * ((minus) ? -1 : 1));

                DrawItems();
            }
            else
                MessageBox.Show("Zboží z kódem '" + code + "' nebylo nalezeno");
        }

        private void DrawItems()
        {
            lvItems.Objects = bill.Items;
            tbTotal.Text = bill.TotalPrice.ToString("0.00");
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (e.Control)
                    bPayment_Click(sender, e);
                else
                    ConfirmProduct(sender, e);
            }
            else if (e.KeyCode == Keys.Escape || e.KeyCode==Keys.Delete)
            {
                ClearCodeClick(sender, e);
            }
            else if (e.KeyCode == Keys.Back)
            {
                DeleteLastClick(sender, e);
            }
            else if (e.KeyCode == Keys.F4 && e.Alt)
                return;
            else if (e.KeyCode==Keys.Left || e.KeyCode==Keys.Right || e.KeyCode==Keys.Up || e.KeyCode==Keys.Down)
            {
                return;
            }
            else
            {
                char c = KeyboardUtil.GetKey(e.KeyCode);
                if (c >= 32)
                {
                    if (c == '*')
                        tbProduct.Text = " * ";
                    else
                    tbProduct.Text += c;
                    
                    e.Handled = true;
                }
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            Close();
        }        

        private void bPayment_Click(object sender, EventArgs e)
        {
            PaymentForm form = new PaymentForm(bill);
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                InitBill();
                tbProduct.Text = "";
                DrawCustomer();
                DrawItems();
                InitDisplay();
            }
        }

        private void ShortcutClick(object sender, EventArgs e)
        {
            if (sender is TouchButton)
            {
                PackageSummary p = (PackageSummary)(sender as TouchButton).Tag;

                decimal multiple = 1;
                bool minus = false;
                string code = tbProduct.Text.Trim();
                if (!string.IsNullOrEmpty(code))
                {

                    tbProduct.Text = "";
                    if (code.StartsWith("-"))
                    {
                        minus = true;
                        code = code.Substring(1);
                    }

                    if (code.Contains('*'))
                    {
                        string[] parts = code.Split(" *".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        try
                        {
                            if (!decimal.TryParse(parts[0], out multiple))
                            {
                                
                            }
                            //code = parts[1];
                        }
                        catch
                        {
                            return;
                        }
                    }
                }

                if (!minus)
                {
                    decimal amount = bill.CheckCount(p);
                    amount += multiple;
                    if (Setting.Instance.ForbidSellToMinus)
                    {
                        Product product = ProductModel.Instance.Get(p.ProductID);
                        if (!product.Setting.UndefinedAmount)
                        {
                            decimal storeAmount = 0;
                            if (p.VariantID != 0)
                                storeAmount = StoreProductModel.Instance.StoreAmountByVariant(p.VariantID, Setting.Instance.Store);
                            else
                                storeAmount = StoreProductModel.Instance.StoreAmountByProducts(p.ProductID, Setting.Instance.Store);
                            if (storeAmount < amount)
                            {
                                MessageBox.Show("Zboží není v dostatečném množství na skladě");
                                return;
                            }
                        }
                    }
                }
                bill.Add(p, multiple * ((minus) ? -1 : 1));

                UpdateDisplay(p, multiple * ((minus) ? -1 : 1));

                DrawItems();
            }
        }

        private void bLeft_Click(object sender, EventArgs e)
        {
            actualPage--;
            DrawShortcuts();
        }

        private void bRight_Click(object sender, EventArgs e)
        {
            actualPage++;
            DrawShortcuts();
        }

        private void bCancelItem_Click(object sender, EventArgs e)
        {
            if (lvItems.SelectedObject == null)
            {
                bill.Items.RemoveAt(bill.Items.Count - 1);
                DrawItems();
            }
            else
            {
                bill.Items.Remove((BillItem)lvItems.SelectedObject);
                DrawItems();
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            InitBill();
            InitDisplay();

            DrawCustomer();
            DrawItems();
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            if (!tbProduct.Text.Contains('-'))
                tbProduct.Text = "-" + tbProduct.Text;
        }

        private void bSelectProduct_Click(object sender, EventArgs e)
        {
            StoreCardForm form = new StoreCardForm();
            if (form.ShowDialog()==DialogResult.OK)
            {
                if (string.IsNullOrEmpty(form.Code))
                {
                    decimal multiple = 1;
                    bool minus = false;
                    string code = tbProduct.Text.Trim();
                    if (!string.IsNullOrEmpty(code))
                    {

                        tbProduct.Text = "";
                        if (code.StartsWith("-"))
                        {
                            minus = true;
                            code = code.Substring(1);
                        }

                        if (code.Contains('*'))
                        {
                            string[] parts = code.Split(" *".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                            try
                            {
                                if (!decimal.TryParse(parts[0], out multiple))
                                {

                                }
                                //code = parts[1];
                            }
                            catch
                            {
                                return;
                            }
                        }
                    }

                    if (!minus)
                    {
                        decimal amount = bill.CheckCount(form.Package);
                        amount += multiple;
                        if (Setting.Instance.ForbidSellToMinus)
                        {
                            Product product = ProductModel.Instance.Get(form.Package.ProductID);
                            if (!product.Setting.UndefinedAmount)
                            {
                                decimal storeAmount = 0;
                                if (form.Package.VariantID != 0)
                                    storeAmount = StoreProductModel.Instance.StoreAmountByVariant(form.Package.VariantID, Setting.Instance.Store);
                                else
                                    storeAmount = StoreProductModel.Instance.StoreAmountByProducts(form.Package.ProductID, Setting.Instance.Store);
                                if (storeAmount < amount)
                                {
                                    MessageBox.Show("Zboží není v dostatečném množství na skladě");
                                    return;
                                }
                            }
                        }
                    }
                    bill.Add(form.Package, multiple * ((minus) ? -1 : 1));

                    UpdateDisplay(form.Package, multiple * ((minus) ? -1 : 1));

                    DrawItems();
                }
                else
                {
                    tbProduct.Text += form.Code;
                    ConfirmProduct(this, e);
                }
            };
        }

        private void UpdateDisplay(PackageSummary package, decimal amount)
        {
            string l1 = amount.ToString("0.#####") + "x" + package.Name;
            string l1r = IndividualDiscount.GetPrice(package.Price, package.Discount).ToString("0.00");
            string l2 = "Mezisoučet: ";
            string l2r = bill.TotalPrice.ToString("0.00");

            if ((l1.Length+l1r.Length)>19)
                l1 = l1.Substring(0, 19 - l1r.Length);

            Display.Instance.DrawParts(l1, l1r, l2, l2r);
        }

        private void tbMenu_Click(object sender, EventArgs e)
        {
            ShortcutConfigForm form = new ShortcutConfigForm();
            DialogResult res = form.ShowDialog();
            if (form.ShutdownAction)
            {
                Close();
                return;
            }
            if (res == DialogResult.OK)
            {
                shortcuts = ShortcutIO.Load();
                DrawShortcuts();
            }
            
        }

        private void bSelectCustomer_Click(object sender, EventArgs e)
        {
            CustomerForm form = new CustomerForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (form.Media != null)
                {
                    IDMedia id = form.Media;
                    if (id.OsobaID != 0)
                    {
                        bill.CustomerID = (long)id.OsobaID;
                        bill.CustomerType = 0;
                        bill.CustomerName = id.Osoba.Full_Name ?? (id.Osoba.Jmeno + " " + id.Osoba.Prijmeni);
                        bill.CustomerCard = (long)id.ID;
                    }
                    else if (id.OrganizaceID != 0)
                    {
                        bill.CustomerID = (long)id.OrganizaceID;
                        bill.CustomerType = 1;
                        bill.CustomerName = id.Organizace.Nazev;
                    }
                    bill.CustomerCard = (long)id.ID;
                }
                else if (form.Person != null)
                {
                    Osoba id = form.Person;
                    bill.CustomerID = (long)id.ID;
                    bill.CustomerType = 0;
                    bill.CustomerName = id.Full_Name ?? (id.Prijmeni + " " + id.Jmeno);
                    bill.CustomerCard = 0;
                    List<IDMedia> medias = IDMedialModel.Instance.GetList(id.ID);
                    if (medias.Count > 0)
                        bill.CustomerCard = (long)medias[0].ID;
                }
                else if (form.Company != null)
                {
                    Organizace id = form.Company;
                    bill.CustomerID = (long)id.ID;
                    bill.CustomerType = 1;
                    bill.CustomerName = id.Nazev;
                    bill.CustomerCard = 0;
                }
                DrawCustomer();
            }
        }

        private void DrawCustomer()
        {
            tbCustomer.Text = bill.CustomerName;
            if (bill.CustomerCard == 0)
                media = null;
            else
            if (media==null || (media != null && media.ID != bill.CustomerCard))
            {
                media = IDMedialModel.Instance.Get(bill.CustomerCard);
            }
            if (media != null && media.Typ != null)
                tbCustomer.Text += "(" + media.Typ.Popis + ")";
        }

        private void tbPriceChange_Click(object sender, EventArgs e)
        {
            if (lvItems.SelectedObject == null)
                return;
            PriceChangeForm form = new PriceChangeForm((BillItem)lvItems.SelectedObject);

            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DrawItems();
            }
        }

        private void tbExit_Click(object sender, EventArgs e)
        {
            if (selectForm.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                Application.Exit();
            }
        }


    }
}
