﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.IO.Ports;

namespace Transware.TouchPOS
{
    public class Setting
    {
        private string fileName = "setting.xml";

        #region Singleton

        private static Setting instance = null;

        public static Setting Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Setting();
                    instance.Load();
                }

                return instance;
            }
        }

        #endregion


        #region Property

        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public virtual bool EetEnabled { get; set; }
        public virtual bool EetTaxPayer { get; set; }
        public virtual string EetVat { get; set; }
        public virtual string EetShopName { get; set; }
        public virtual string EetDeviceName { get; set; }
        public virtual string EetCertificateName { get; set; }
        public virtual string EetCertificate { get; set; }
        public virtual bool EetTesting { get; set; }
        public virtual string EetCertificatePassword { get; set; }
        

        public bool Printer { get; set; }
        public string PrinterType { get; set; }
        public string PrinterCom { get; set; }
        public int PrinterSpeed { get; set; }
        public Parity PrinterParity { get; set; }
        public StopBits PrinterStopBits { get; set; }
        public int PrinterDataBits { get; set; }

        public bool Display { get; set; }
        public string DisplayCom { get; set; }
        public int DisplaySpeed { get; set; }
        public Parity DisplayParity { get; set; }
        public StopBits DisplayStopBits { get; set; }
        public int DisplayDataBits { get; set; }

        public long Store { get; set; }

        public long PaymentCash { get; set; }
        public long PaymentCard { get; set; }

        public long ItemsCurrency { get; set; }

        public bool ForbidSellToMinus { get; set; }

        public bool LoginRequired { get; set; }

        #endregion

        protected Setting()
        {
            Server = "";
            User = "";
            Password = "";

            EetEnabled = false;

            Printer = false;
            PrinterType = "PRP-085";
            PrinterCom = "COM1";
            PrinterSpeed = 9600;
            PrinterParity = Parity.None;
            PrinterStopBits = StopBits.One;
            PrinterDataBits = 8;

            Display = false;
            DisplayCom = "COM1";
            DisplaySpeed = 9600;
            DisplayParity = Parity.None;
            DisplayStopBits = StopBits.One;
            DisplayDataBits = 8;

            Store = 1;

            PaymentCash = 1;
            PaymentCard = 2;

            ItemsCurrency = 1;

            LoginRequired = false;
        }

        
        private void Load()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                xml.Load(fs);
                fs.Close();

                ProcessXML(xml);
            }
            catch
            {
                Save();
            }
        }

        public void Save()
        {
            try
            {
                XmlDocument xml = new XmlDocument();

                StoreXML(xml);

                FileStream fs = new FileStream(fileName, FileMode.Create);
                xml.Save(fs);
                fs.Close();               
            }
            catch
            {
            }
        }

        private void ProcessXML(XmlDocument xml)
        {
            XmlElement root = xml.DocumentElement;

            XmlNodeList list = root.ChildNodes;

            foreach (XmlNode node in list)
            {
                if (node.NodeType != XmlNodeType.Element)
                    continue;

                if (node.Name == "database")
                {
                    Server = node.Attributes["server"].InnerText;
                    User = node.Attributes["user"].InnerText;
                    Password = node.Attributes["password"].InnerText;
                }
                if (node.Name == "eet")
                {
                    EetEnabled = node.Attributes["enabled"].InnerText=="1";
                    EetTaxPayer = node.Attributes["taxPayer"].InnerText=="1";
                    EetVat = node.Attributes["vat"].InnerText;
                    EetShopName = node.Attributes["shopName"].InnerText;
                    EetDeviceName = node.Attributes["deviceName"].InnerText;
                    EetCertificateName = node.Attributes["certificateName"].InnerText;
                    EetCertificatePassword = node.Attributes["certificatePassword"].InnerText;
                    EetTesting = node.Attributes["testing"].InnerText=="1";

                    if (EetEnabled && !EetTesting)
                    {
                        if (!string.IsNullOrEmpty(EetCertificateName) && File.Exists(EetCertificateName))
                        {
                            byte[] data = File.ReadAllBytes(EetCertificateName);
                            string dataStr = Convert.ToBase64String(data);
                            EetCertificate = dataStr;
                        }
                        else
                            EetEnabled = false;
                    }
                }
                

                if (node.Name == "printer")
                {
                    Printer = node.Attributes["enabled"].InnerText == "1";
                    PrinterCom = node.Attributes["com"].InnerText;
                    PrinterSpeed = int.Parse(node.Attributes["speed"].InnerText ?? "0");
                    PrinterParity = (Parity)int.Parse(node.Attributes["parity"].InnerText ?? "0");
                    PrinterStopBits = (StopBits)int.Parse(node.Attributes["stopBits"].InnerText ?? "1");
                    PrinterDataBits = int.Parse(node.Attributes["dataBits"].InnerText ?? "8");
                }

                if (node.Name == "display")
                {
                    Display = node.Attributes["enabled"].InnerText == "1";
                    DisplayCom = node.Attributes["com"].InnerText;
                    DisplaySpeed = int.Parse(node.Attributes["speed"].InnerText ?? "0");
                    DisplayParity = (Parity)int.Parse(node.Attributes["parity"].InnerText ?? "0");
                    DisplayStopBits = (StopBits)int.Parse(node.Attributes["stopBits"].InnerText ?? "1");
                    DisplayDataBits = int.Parse(node.Attributes["dataBits"].InnerText ?? "8");
                }

                if (node.Name == "store")
                {
                    Store = int.Parse(node.Attributes["id"].InnerText ?? "1");
                }

                if (node.Name == "paymentCash")
                {
                    PaymentCash = int.Parse(node.Attributes["id"].InnerText ?? "1");
                }

                if (node.Name == "paymentCard")
                {
                    PaymentCard = int.Parse(node.Attributes["id"].InnerText ?? "2");
                }

                if (node.Name == "itemsCurrency")
                {
                    ItemsCurrency = int.Parse(node.Attributes["id"].InnerText ?? "1");
                }

                if (node.Name == "forbidSellToMinus")
                {
                    ForbidSellToMinus = node.Attributes["enabled"].InnerText == "1";
                }

                if (node.Name == "loginRequired")
                {
                    LoginRequired = node.Attributes["enabled"].InnerText == "1";
                }
            }
        }

        private void StoreXML(XmlDocument xml)
        {
            XmlElement root = xml.CreateElement("touchPosSetting");
            xml.AppendChild(root);

            XmlElement el;

            el = xml.CreateElement("database");
            el.Attributes.Append(CreateAttribute(xml, "server", Server));
            el.Attributes.Append(CreateAttribute(xml, "user", User));
            el.Attributes.Append(CreateAttribute(xml, "password", Password));
            root.AppendChild(el);

            el = xml.CreateElement("eet");
            el.Attributes.Append(CreateAttribute(xml, "enabled", EetEnabled ? "1" : "0"));
            el.Attributes.Append(CreateAttribute(xml, "taxPayer", EetTaxPayer ? "1" : "0"));
            el.Attributes.Append(CreateAttribute(xml, "vat", EetVat));
            el.Attributes.Append(CreateAttribute(xml, "shopName", EetShopName));
            el.Attributes.Append(CreateAttribute(xml, "deviceName", EetDeviceName));
            el.Attributes.Append(CreateAttribute(xml, "certificateName", EetCertificateName));
            el.Attributes.Append(CreateAttribute(xml, "certificatePassword", EetCertificatePassword));
            el.Attributes.Append(CreateAttribute(xml, "testing", EetTesting ? "1" : "0"));
            root.AppendChild(el);  

            el = xml.CreateElement("printer");
            el.Attributes.Append(CreateAttribute(xml, "enabled", Printer ? "1" : "0"));
            el.Attributes.Append(CreateAttribute(xml, "com", PrinterCom));
            el.Attributes.Append(CreateAttribute(xml, "speed", PrinterSpeed.ToString()));
            el.Attributes.Append(CreateAttribute(xml, "parity", ((int)PrinterParity).ToString()));
            el.Attributes.Append(CreateAttribute(xml, "stopBits", ((int)PrinterStopBits).ToString()));
            el.Attributes.Append(CreateAttribute(xml, "dataBits", PrinterDataBits.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("display");
            el.Attributes.Append(CreateAttribute(xml, "enabled", Display ? "1" : "0"));
            el.Attributes.Append(CreateAttribute(xml, "com", DisplayCom));
            el.Attributes.Append(CreateAttribute(xml, "speed", DisplaySpeed.ToString()));
            el.Attributes.Append(CreateAttribute(xml, "parity", ((int)DisplayParity).ToString()));
            el.Attributes.Append(CreateAttribute(xml, "stopBits", ((int)DisplayStopBits).ToString()));
            el.Attributes.Append(CreateAttribute(xml, "dataBits", DisplayDataBits.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("store");
            el.Attributes.Append(CreateAttribute(xml, "id", Store.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("paymentCash");
            el.Attributes.Append(CreateAttribute(xml, "id", PaymentCash.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("paymentCard");
            el.Attributes.Append(CreateAttribute(xml, "id", PaymentCard.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("itemsCurrency");
            el.Attributes.Append(CreateAttribute(xml, "id", ItemsCurrency.ToString()));
            root.AppendChild(el);

            el = xml.CreateElement("forbidSellToMinus");
            el.Attributes.Append(CreateAttribute(xml, "enabled", ForbidSellToMinus?"1":"0"));
            root.AppendChild(el);

            el = xml.CreateElement("loginRequired");
            el.Attributes.Append(CreateAttribute(xml, "enabled", LoginRequired ? "1" : "0"));
            root.AppendChild(el);

        }

        private XmlAttribute CreateAttribute(XmlDocument xml, string name, string value)
        {
            XmlAttribute a = xml.CreateAttribute(name);
            a.AppendChild(xml.CreateTextNode(value));
            return a;
        }
    }
}
