﻿namespace Transware.TouchPOS
{
    partial class CustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbStoreCard = new System.Windows.Forms.GroupBox();
            this.pStoreCard = new System.Windows.Forms.Panel();
            this.bCancel = new Transware.Controls.TouchButton();
            this.tbPersons = new Transware.Controls.TouchButton();
            this.tbCompany = new Transware.Controls.TouchButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbClearFilter = new Transware.Controls.TouchButton();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.tbY = new Transware.Controls.TouchButton();
            this.tbZ = new Transware.Controls.TouchButton();
            this.tbX = new Transware.Controls.TouchButton();
            this.tbW = new Transware.Controls.TouchButton();
            this.tbM = new Transware.Controls.TouchButton();
            this.tbS = new Transware.Controls.TouchButton();
            this.tbO = new Transware.Controls.TouchButton();
            this.tbP = new Transware.Controls.TouchButton();
            this.tbT = new Transware.Controls.TouchButton();
            this.tbL = new Transware.Controls.TouchButton();
            this.tbU = new Transware.Controls.TouchButton();
            this.tbV = new Transware.Controls.TouchButton();
            this.tbR = new Transware.Controls.TouchButton();
            this.tbQ = new Transware.Controls.TouchButton();
            this.tbN = new Transware.Controls.TouchButton();
            this.tbJ = new Transware.Controls.TouchButton();
            this.tbK = new Transware.Controls.TouchButton();
            this.tbG = new Transware.Controls.TouchButton();
            this.tbH = new Transware.Controls.TouchButton();
            this.tbI = new Transware.Controls.TouchButton();
            this.tbF = new Transware.Controls.TouchButton();
            this.tbE = new Transware.Controls.TouchButton();
            this.tbC = new Transware.Controls.TouchButton();
            this.tbD = new Transware.Controls.TouchButton();
            this.tbB = new Transware.Controls.TouchButton();
            this.tbA = new Transware.Controls.TouchButton();
            this.tbRight = new Transware.Controls.TouchButton();
            this.tbLeft = new Transware.Controls.TouchButton();
            this.gbStoreCard.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbStoreCard
            // 
            this.gbStoreCard.Controls.Add(this.pStoreCard);
            this.gbStoreCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbStoreCard.Location = new System.Drawing.Point(0, 117);
            this.gbStoreCard.Name = "gbStoreCard";
            this.gbStoreCard.Size = new System.Drawing.Size(1024, 571);
            this.gbStoreCard.TabIndex = 0;
            this.gbStoreCard.TabStop = false;
            this.gbStoreCard.Text = "Odběratelé (vyberte ze seznamu nebo načtěte zákaznickou kartu)";
            // 
            // pStoreCard
            // 
            this.pStoreCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pStoreCard.Location = new System.Drawing.Point(3, 27);
            this.pStoreCard.Name = "pStoreCard";
            this.pStoreCard.Size = new System.Drawing.Size(1018, 541);
            this.pStoreCard.TabIndex = 0;
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(17, 698);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 62;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // tbPersons
            // 
            this.tbPersons.BorderColor = System.Drawing.Color.Black;
            this.tbPersons.BorderWidth = 1;
            this.tbPersons.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbPersons.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbPersons.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbPersons.Image = null;
            this.tbPersons.Location = new System.Drawing.Point(18, 26);
            this.tbPersons.Margin = new System.Windows.Forms.Padding(6);
            this.tbPersons.Name = "tbPersons";
            this.tbPersons.RoundedCorners.BottomLeft = true;
            this.tbPersons.RoundedCorners.BottomRight = true;
            this.tbPersons.RoundedCorners.TopLeft = true;
            this.tbPersons.RoundedCorners.TopRight = true;
            this.tbPersons.RoundingDiameter = 20;
            this.tbPersons.Size = new System.Drawing.Size(136, 37);
            this.tbPersons.TabIndex = 1;
            this.tbPersons.Tag = "0";
            this.tbPersons.Text = "Osoby";
            this.tbPersons.Click += new System.EventHandler(this.CustomerType_Click);
            // 
            // tbCompany
            // 
            this.tbCompany.BorderColor = System.Drawing.Color.Black;
            this.tbCompany.BorderWidth = 1;
            this.tbCompany.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbCompany.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbCompany.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbCompany.Image = null;
            this.tbCompany.Location = new System.Drawing.Point(18, 66);
            this.tbCompany.Margin = new System.Windows.Forms.Padding(12);
            this.tbCompany.Name = "tbCompany";
            this.tbCompany.RoundedCorners.BottomLeft = true;
            this.tbCompany.RoundedCorners.BottomRight = true;
            this.tbCompany.RoundedCorners.TopLeft = true;
            this.tbCompany.RoundedCorners.TopRight = true;
            this.tbCompany.RoundingDiameter = 20;
            this.tbCompany.Size = new System.Drawing.Size(136, 37);
            this.tbCompany.TabIndex = 3;
            this.tbCompany.Tag = "1";
            this.tbCompany.Text = "Organizace";
            this.tbCompany.Click += new System.EventHandler(this.CustomerType_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbCompany);
            this.groupBox1.Controls.Add(this.tbPersons);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 110);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Typ odběratele";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbClearFilter);
            this.groupBox2.Controls.Add(this.tbFilter);
            this.groupBox2.Controls.Add(this.tbY);
            this.groupBox2.Controls.Add(this.tbZ);
            this.groupBox2.Controls.Add(this.tbX);
            this.groupBox2.Controls.Add(this.tbW);
            this.groupBox2.Controls.Add(this.tbM);
            this.groupBox2.Controls.Add(this.tbS);
            this.groupBox2.Controls.Add(this.tbO);
            this.groupBox2.Controls.Add(this.tbP);
            this.groupBox2.Controls.Add(this.tbT);
            this.groupBox2.Controls.Add(this.tbL);
            this.groupBox2.Controls.Add(this.tbU);
            this.groupBox2.Controls.Add(this.tbV);
            this.groupBox2.Controls.Add(this.tbR);
            this.groupBox2.Controls.Add(this.tbQ);
            this.groupBox2.Controls.Add(this.tbN);
            this.groupBox2.Controls.Add(this.tbJ);
            this.groupBox2.Controls.Add(this.tbK);
            this.groupBox2.Controls.Add(this.tbG);
            this.groupBox2.Controls.Add(this.tbH);
            this.groupBox2.Controls.Add(this.tbI);
            this.groupBox2.Controls.Add(this.tbF);
            this.groupBox2.Controls.Add(this.tbE);
            this.groupBox2.Controls.Add(this.tbC);
            this.groupBox2.Controls.Add(this.tbD);
            this.groupBox2.Controls.Add(this.tbB);
            this.groupBox2.Controls.Add(this.tbA);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(178, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(846, 110);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtr";
            // 
            // tbClearFilter
            // 
            this.tbClearFilter.BorderColor = System.Drawing.Color.Black;
            this.tbClearFilter.BorderWidth = 1;
            this.tbClearFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbClearFilter.FillColorBottom = System.Drawing.Color.Red;
            this.tbClearFilter.FillColorTop = System.Drawing.Color.Red;
            this.tbClearFilter.Image = null;
            this.tbClearFilter.Location = new System.Drawing.Point(807, 24);
            this.tbClearFilter.Margin = new System.Windows.Forms.Padding(592, 535, 592, 535);
            this.tbClearFilter.Name = "tbClearFilter";
            this.tbClearFilter.RoundedCorners.BottomLeft = true;
            this.tbClearFilter.RoundedCorners.BottomRight = true;
            this.tbClearFilter.RoundedCorners.TopLeft = true;
            this.tbClearFilter.RoundedCorners.TopRight = true;
            this.tbClearFilter.RoundingDiameter = 20;
            this.tbClearFilter.Size = new System.Drawing.Size(32, 32);
            this.tbClearFilter.TabIndex = 29;
            this.tbClearFilter.Text = "X";
            this.tbClearFilter.Click += new System.EventHandler(this.tbClearFilter_Click);
            // 
            // tbFilter
            // 
            this.tbFilter.Location = new System.Drawing.Point(7, 26);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(768, 29);
            this.tbFilter.TabIndex = 28;
            // 
            // tbY
            // 
            this.tbY.BorderColor = System.Drawing.Color.Black;
            this.tbY.BorderWidth = 1;
            this.tbY.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbY.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbY.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbY.Image = null;
            this.tbY.Location = new System.Drawing.Point(775, 71);
            this.tbY.Margin = new System.Windows.Forms.Padding(323, 290, 323, 290);
            this.tbY.Name = "tbY";
            this.tbY.RoundedCorners.BottomLeft = true;
            this.tbY.RoundedCorners.BottomRight = true;
            this.tbY.RoundedCorners.TopLeft = true;
            this.tbY.RoundedCorners.TopRight = true;
            this.tbY.RoundingDiameter = 20;
            this.tbY.Size = new System.Drawing.Size(32, 32);
            this.tbY.TabIndex = 27;
            this.tbY.Text = "Y";
            this.tbY.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbZ
            // 
            this.tbZ.BorderColor = System.Drawing.Color.Black;
            this.tbZ.BorderWidth = 1;
            this.tbZ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbZ.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbZ.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbZ.Image = null;
            this.tbZ.Location = new System.Drawing.Point(807, 71);
            this.tbZ.Margin = new System.Windows.Forms.Padding(323, 290, 323, 290);
            this.tbZ.Name = "tbZ";
            this.tbZ.RoundedCorners.BottomLeft = true;
            this.tbZ.RoundedCorners.BottomRight = true;
            this.tbZ.RoundedCorners.TopLeft = true;
            this.tbZ.RoundedCorners.TopRight = true;
            this.tbZ.RoundingDiameter = 20;
            this.tbZ.Size = new System.Drawing.Size(32, 32);
            this.tbZ.TabIndex = 26;
            this.tbZ.Text = "Z";
            this.tbZ.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbX
            // 
            this.tbX.BorderColor = System.Drawing.Color.Black;
            this.tbX.BorderWidth = 1;
            this.tbX.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbX.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbX.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbX.Image = null;
            this.tbX.Location = new System.Drawing.Point(743, 71);
            this.tbX.Margin = new System.Windows.Forms.Padding(176, 157, 176, 157);
            this.tbX.Name = "tbX";
            this.tbX.RoundedCorners.BottomLeft = true;
            this.tbX.RoundedCorners.BottomRight = true;
            this.tbX.RoundedCorners.TopLeft = true;
            this.tbX.RoundedCorners.TopRight = true;
            this.tbX.RoundingDiameter = 20;
            this.tbX.Size = new System.Drawing.Size(32, 32);
            this.tbX.TabIndex = 25;
            this.tbX.Text = "X";
            this.tbX.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbW
            // 
            this.tbW.BorderColor = System.Drawing.Color.Black;
            this.tbW.BorderWidth = 1;
            this.tbW.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbW.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbW.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbW.Image = null;
            this.tbW.Location = new System.Drawing.Point(711, 71);
            this.tbW.Margin = new System.Windows.Forms.Padding(96, 85, 96, 85);
            this.tbW.Name = "tbW";
            this.tbW.RoundedCorners.BottomLeft = true;
            this.tbW.RoundedCorners.BottomRight = true;
            this.tbW.RoundedCorners.TopLeft = true;
            this.tbW.RoundedCorners.TopRight = true;
            this.tbW.RoundingDiameter = 20;
            this.tbW.Size = new System.Drawing.Size(32, 32);
            this.tbW.TabIndex = 24;
            this.tbW.Text = "W";
            this.tbW.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbM
            // 
            this.tbM.BorderColor = System.Drawing.Color.Black;
            this.tbM.BorderWidth = 1;
            this.tbM.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbM.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbM.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbM.Image = null;
            this.tbM.Location = new System.Drawing.Point(391, 71);
            this.tbM.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbM.Name = "tbM";
            this.tbM.RoundedCorners.BottomLeft = true;
            this.tbM.RoundedCorners.BottomRight = true;
            this.tbM.RoundedCorners.TopLeft = true;
            this.tbM.RoundedCorners.TopRight = true;
            this.tbM.RoundingDiameter = 20;
            this.tbM.Size = new System.Drawing.Size(32, 32);
            this.tbM.TabIndex = 23;
            this.tbM.Text = "M";
            this.tbM.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbS
            // 
            this.tbS.BorderColor = System.Drawing.Color.Black;
            this.tbS.BorderWidth = 1;
            this.tbS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbS.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbS.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbS.Image = null;
            this.tbS.Location = new System.Drawing.Point(583, 71);
            this.tbS.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbS.Name = "tbS";
            this.tbS.RoundedCorners.BottomLeft = true;
            this.tbS.RoundedCorners.BottomRight = true;
            this.tbS.RoundedCorners.TopLeft = true;
            this.tbS.RoundedCorners.TopRight = true;
            this.tbS.RoundingDiameter = 20;
            this.tbS.Size = new System.Drawing.Size(32, 32);
            this.tbS.TabIndex = 22;
            this.tbS.Text = "S";
            this.tbS.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbO
            // 
            this.tbO.BorderColor = System.Drawing.Color.Black;
            this.tbO.BorderWidth = 1;
            this.tbO.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbO.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbO.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbO.Image = null;
            this.tbO.Location = new System.Drawing.Point(455, 71);
            this.tbO.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbO.Name = "tbO";
            this.tbO.RoundedCorners.BottomLeft = true;
            this.tbO.RoundedCorners.BottomRight = true;
            this.tbO.RoundedCorners.TopLeft = true;
            this.tbO.RoundedCorners.TopRight = true;
            this.tbO.RoundingDiameter = 20;
            this.tbO.Size = new System.Drawing.Size(32, 32);
            this.tbO.TabIndex = 21;
            this.tbO.Text = "O";
            this.tbO.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbP
            // 
            this.tbP.BorderColor = System.Drawing.Color.Black;
            this.tbP.BorderWidth = 1;
            this.tbP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbP.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbP.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbP.Image = null;
            this.tbP.Location = new System.Drawing.Point(487, 71);
            this.tbP.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbP.Name = "tbP";
            this.tbP.RoundedCorners.BottomLeft = true;
            this.tbP.RoundedCorners.BottomRight = true;
            this.tbP.RoundedCorners.TopLeft = true;
            this.tbP.RoundedCorners.TopRight = true;
            this.tbP.RoundingDiameter = 20;
            this.tbP.Size = new System.Drawing.Size(32, 32);
            this.tbP.TabIndex = 20;
            this.tbP.Text = "P";
            this.tbP.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbT
            // 
            this.tbT.BorderColor = System.Drawing.Color.Black;
            this.tbT.BorderWidth = 1;
            this.tbT.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbT.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbT.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbT.Image = null;
            this.tbT.Location = new System.Drawing.Point(615, 71);
            this.tbT.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbT.Name = "tbT";
            this.tbT.RoundedCorners.BottomLeft = true;
            this.tbT.RoundedCorners.BottomRight = true;
            this.tbT.RoundedCorners.TopLeft = true;
            this.tbT.RoundedCorners.TopRight = true;
            this.tbT.RoundingDiameter = 20;
            this.tbT.Size = new System.Drawing.Size(32, 32);
            this.tbT.TabIndex = 19;
            this.tbT.Text = "T";
            this.tbT.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbL
            // 
            this.tbL.BorderColor = System.Drawing.Color.Black;
            this.tbL.BorderWidth = 1;
            this.tbL.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbL.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbL.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbL.Image = null;
            this.tbL.Location = new System.Drawing.Point(359, 71);
            this.tbL.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbL.Name = "tbL";
            this.tbL.RoundedCorners.BottomLeft = true;
            this.tbL.RoundedCorners.BottomRight = true;
            this.tbL.RoundedCorners.TopLeft = true;
            this.tbL.RoundedCorners.TopRight = true;
            this.tbL.RoundingDiameter = 20;
            this.tbL.Size = new System.Drawing.Size(32, 32);
            this.tbL.TabIndex = 18;
            this.tbL.Text = "L";
            this.tbL.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbU
            // 
            this.tbU.BorderColor = System.Drawing.Color.Black;
            this.tbU.BorderWidth = 1;
            this.tbU.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbU.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbU.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbU.Image = null;
            this.tbU.Location = new System.Drawing.Point(647, 71);
            this.tbU.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbU.Name = "tbU";
            this.tbU.RoundedCorners.BottomLeft = true;
            this.tbU.RoundedCorners.BottomRight = true;
            this.tbU.RoundedCorners.TopLeft = true;
            this.tbU.RoundedCorners.TopRight = true;
            this.tbU.RoundingDiameter = 20;
            this.tbU.Size = new System.Drawing.Size(32, 32);
            this.tbU.TabIndex = 17;
            this.tbU.Text = "U";
            this.tbU.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbV
            // 
            this.tbV.BorderColor = System.Drawing.Color.Black;
            this.tbV.BorderWidth = 1;
            this.tbV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbV.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbV.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbV.Image = null;
            this.tbV.Location = new System.Drawing.Point(679, 71);
            this.tbV.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbV.Name = "tbV";
            this.tbV.RoundedCorners.BottomLeft = true;
            this.tbV.RoundedCorners.BottomRight = true;
            this.tbV.RoundedCorners.TopLeft = true;
            this.tbV.RoundedCorners.TopRight = true;
            this.tbV.RoundingDiameter = 20;
            this.tbV.Size = new System.Drawing.Size(32, 32);
            this.tbV.TabIndex = 16;
            this.tbV.Text = "V";
            this.tbV.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbR
            // 
            this.tbR.BorderColor = System.Drawing.Color.Black;
            this.tbR.BorderWidth = 1;
            this.tbR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbR.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbR.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbR.Image = null;
            this.tbR.Location = new System.Drawing.Point(551, 71);
            this.tbR.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbR.Name = "tbR";
            this.tbR.RoundedCorners.BottomLeft = true;
            this.tbR.RoundedCorners.BottomRight = true;
            this.tbR.RoundedCorners.TopLeft = true;
            this.tbR.RoundedCorners.TopRight = true;
            this.tbR.RoundingDiameter = 20;
            this.tbR.Size = new System.Drawing.Size(32, 32);
            this.tbR.TabIndex = 15;
            this.tbR.Text = "R";
            this.tbR.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbQ
            // 
            this.tbQ.BorderColor = System.Drawing.Color.Black;
            this.tbQ.BorderWidth = 1;
            this.tbQ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbQ.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbQ.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbQ.Image = null;
            this.tbQ.Location = new System.Drawing.Point(519, 71);
            this.tbQ.Margin = new System.Windows.Forms.Padding(48, 44, 48, 44);
            this.tbQ.Name = "tbQ";
            this.tbQ.RoundedCorners.BottomLeft = true;
            this.tbQ.RoundedCorners.BottomRight = true;
            this.tbQ.RoundedCorners.TopLeft = true;
            this.tbQ.RoundedCorners.TopRight = true;
            this.tbQ.RoundingDiameter = 20;
            this.tbQ.Size = new System.Drawing.Size(32, 32);
            this.tbQ.TabIndex = 14;
            this.tbQ.Text = "Q";
            this.tbQ.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbN
            // 
            this.tbN.BorderColor = System.Drawing.Color.Black;
            this.tbN.BorderWidth = 1;
            this.tbN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbN.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbN.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbN.Image = null;
            this.tbN.Location = new System.Drawing.Point(423, 71);
            this.tbN.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbN.Name = "tbN";
            this.tbN.RoundedCorners.BottomLeft = true;
            this.tbN.RoundedCorners.BottomRight = true;
            this.tbN.RoundedCorners.TopLeft = true;
            this.tbN.RoundedCorners.TopRight = true;
            this.tbN.RoundingDiameter = 20;
            this.tbN.Size = new System.Drawing.Size(32, 32);
            this.tbN.TabIndex = 13;
            this.tbN.Text = "N";
            this.tbN.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbJ
            // 
            this.tbJ.BorderColor = System.Drawing.Color.Black;
            this.tbJ.BorderWidth = 1;
            this.tbJ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbJ.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbJ.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbJ.Image = null;
            this.tbJ.Location = new System.Drawing.Point(295, 71);
            this.tbJ.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbJ.Name = "tbJ";
            this.tbJ.RoundedCorners.BottomLeft = true;
            this.tbJ.RoundedCorners.BottomRight = true;
            this.tbJ.RoundedCorners.TopLeft = true;
            this.tbJ.RoundedCorners.TopRight = true;
            this.tbJ.RoundingDiameter = 20;
            this.tbJ.Size = new System.Drawing.Size(32, 32);
            this.tbJ.TabIndex = 12;
            this.tbJ.Text = "J";
            this.tbJ.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbK
            // 
            this.tbK.BorderColor = System.Drawing.Color.Black;
            this.tbK.BorderWidth = 1;
            this.tbK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbK.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbK.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbK.Image = null;
            this.tbK.Location = new System.Drawing.Point(327, 71);
            this.tbK.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbK.Name = "tbK";
            this.tbK.RoundedCorners.BottomLeft = true;
            this.tbK.RoundedCorners.BottomRight = true;
            this.tbK.RoundedCorners.TopLeft = true;
            this.tbK.RoundedCorners.TopRight = true;
            this.tbK.RoundingDiameter = 20;
            this.tbK.Size = new System.Drawing.Size(32, 32);
            this.tbK.TabIndex = 11;
            this.tbK.Text = "K";
            this.tbK.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbG
            // 
            this.tbG.BorderColor = System.Drawing.Color.Black;
            this.tbG.BorderWidth = 1;
            this.tbG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbG.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbG.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbG.Image = null;
            this.tbG.Location = new System.Drawing.Point(199, 71);
            this.tbG.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbG.Name = "tbG";
            this.tbG.RoundedCorners.BottomLeft = true;
            this.tbG.RoundedCorners.BottomRight = true;
            this.tbG.RoundedCorners.TopLeft = true;
            this.tbG.RoundedCorners.TopRight = true;
            this.tbG.RoundingDiameter = 20;
            this.tbG.Size = new System.Drawing.Size(32, 32);
            this.tbG.TabIndex = 10;
            this.tbG.Text = "G";
            this.tbG.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbH
            // 
            this.tbH.BorderColor = System.Drawing.Color.Black;
            this.tbH.BorderWidth = 1;
            this.tbH.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbH.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbH.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbH.Image = null;
            this.tbH.Location = new System.Drawing.Point(231, 71);
            this.tbH.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbH.Name = "tbH";
            this.tbH.RoundedCorners.BottomLeft = true;
            this.tbH.RoundedCorners.BottomRight = true;
            this.tbH.RoundedCorners.TopLeft = true;
            this.tbH.RoundedCorners.TopRight = true;
            this.tbH.RoundingDiameter = 20;
            this.tbH.Size = new System.Drawing.Size(32, 32);
            this.tbH.TabIndex = 9;
            this.tbH.Text = "H";
            this.tbH.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbI
            // 
            this.tbI.BorderColor = System.Drawing.Color.Black;
            this.tbI.BorderWidth = 1;
            this.tbI.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbI.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbI.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbI.Image = null;
            this.tbI.Location = new System.Drawing.Point(263, 71);
            this.tbI.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbI.Name = "tbI";
            this.tbI.RoundedCorners.BottomLeft = true;
            this.tbI.RoundedCorners.BottomRight = true;
            this.tbI.RoundedCorners.TopLeft = true;
            this.tbI.RoundedCorners.TopRight = true;
            this.tbI.RoundingDiameter = 20;
            this.tbI.Size = new System.Drawing.Size(32, 32);
            this.tbI.TabIndex = 8;
            this.tbI.Text = "I";
            this.tbI.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbF
            // 
            this.tbF.BorderColor = System.Drawing.Color.Black;
            this.tbF.BorderWidth = 1;
            this.tbF.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbF.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbF.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbF.Image = null;
            this.tbF.Location = new System.Drawing.Point(167, 71);
            this.tbF.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbF.Name = "tbF";
            this.tbF.RoundedCorners.BottomLeft = true;
            this.tbF.RoundedCorners.BottomRight = true;
            this.tbF.RoundedCorners.TopLeft = true;
            this.tbF.RoundedCorners.TopRight = true;
            this.tbF.RoundingDiameter = 20;
            this.tbF.Size = new System.Drawing.Size(32, 32);
            this.tbF.TabIndex = 7;
            this.tbF.Text = "F";
            this.tbF.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbE
            // 
            this.tbE.BorderColor = System.Drawing.Color.Black;
            this.tbE.BorderWidth = 1;
            this.tbE.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbE.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbE.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbE.Image = null;
            this.tbE.Location = new System.Drawing.Point(135, 71);
            this.tbE.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbE.Name = "tbE";
            this.tbE.RoundedCorners.BottomLeft = true;
            this.tbE.RoundedCorners.BottomRight = true;
            this.tbE.RoundedCorners.TopLeft = true;
            this.tbE.RoundedCorners.TopRight = true;
            this.tbE.RoundingDiameter = 20;
            this.tbE.Size = new System.Drawing.Size(32, 32);
            this.tbE.TabIndex = 6;
            this.tbE.Text = "E";
            this.tbE.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbC
            // 
            this.tbC.BorderColor = System.Drawing.Color.Black;
            this.tbC.BorderWidth = 1;
            this.tbC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbC.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbC.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbC.Image = null;
            this.tbC.Location = new System.Drawing.Point(71, 71);
            this.tbC.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbC.Name = "tbC";
            this.tbC.RoundedCorners.BottomLeft = true;
            this.tbC.RoundedCorners.BottomRight = true;
            this.tbC.RoundedCorners.TopLeft = true;
            this.tbC.RoundedCorners.TopRight = true;
            this.tbC.RoundingDiameter = 20;
            this.tbC.Size = new System.Drawing.Size(32, 32);
            this.tbC.TabIndex = 5;
            this.tbC.Text = "C";
            this.tbC.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbD
            // 
            this.tbD.BorderColor = System.Drawing.Color.Black;
            this.tbD.BorderWidth = 1;
            this.tbD.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbD.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbD.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbD.Image = null;
            this.tbD.Location = new System.Drawing.Point(103, 71);
            this.tbD.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbD.Name = "tbD";
            this.tbD.RoundedCorners.BottomLeft = true;
            this.tbD.RoundedCorners.BottomRight = true;
            this.tbD.RoundedCorners.TopLeft = true;
            this.tbD.RoundedCorners.TopRight = true;
            this.tbD.RoundingDiameter = 20;
            this.tbD.Size = new System.Drawing.Size(32, 32);
            this.tbD.TabIndex = 4;
            this.tbD.Text = "D";
            this.tbD.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbB
            // 
            this.tbB.BorderColor = System.Drawing.Color.Black;
            this.tbB.BorderWidth = 1;
            this.tbB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbB.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbB.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbB.Image = null;
            this.tbB.Location = new System.Drawing.Point(39, 71);
            this.tbB.Margin = new System.Windows.Forms.Padding(24, 23, 24, 23);
            this.tbB.Name = "tbB";
            this.tbB.RoundedCorners.BottomLeft = true;
            this.tbB.RoundedCorners.BottomRight = true;
            this.tbB.RoundedCorners.TopLeft = true;
            this.tbB.RoundedCorners.TopRight = true;
            this.tbB.RoundingDiameter = 20;
            this.tbB.Size = new System.Drawing.Size(32, 32);
            this.tbB.TabIndex = 3;
            this.tbB.Text = "B";
            this.tbB.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbA
            // 
            this.tbA.BorderColor = System.Drawing.Color.Black;
            this.tbA.BorderWidth = 1;
            this.tbA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbA.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbA.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbA.Image = null;
            this.tbA.Location = new System.Drawing.Point(7, 71);
            this.tbA.Margin = new System.Windows.Forms.Padding(12);
            this.tbA.Name = "tbA";
            this.tbA.RoundedCorners.BottomLeft = true;
            this.tbA.RoundedCorners.BottomRight = true;
            this.tbA.RoundedCorners.TopLeft = true;
            this.tbA.RoundedCorners.TopRight = true;
            this.tbA.RoundingDiameter = 20;
            this.tbA.Size = new System.Drawing.Size(32, 32);
            this.tbA.TabIndex = 2;
            this.tbA.Text = "A";
            this.tbA.Click += new System.EventHandler(this.Letter_Click);
            // 
            // tbRight
            // 
            this.tbRight.BorderColor = System.Drawing.Color.Black;
            this.tbRight.BorderWidth = 2;
            this.tbRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbRight.ForeColor = System.Drawing.Color.White;
            this.tbRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.tbRight.Location = new System.Drawing.Point(941, 698);
            this.tbRight.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbRight.Name = "tbRight";
            this.tbRight.RoundedCorners.BottomLeft = true;
            this.tbRight.RoundedCorners.BottomRight = true;
            this.tbRight.RoundedCorners.TopLeft = true;
            this.tbRight.RoundedCorners.TopRight = true;
            this.tbRight.RoundingDiameter = 20;
            this.tbRight.Size = new System.Drawing.Size(66, 57);
            this.tbRight.TabIndex = 63;
            this.tbRight.TabStop = false;
            this.tbRight.Text = "TouchButton";
            this.tbRight.Click += new System.EventHandler(this.tbRight_Click);
            // 
            // tbLeft
            // 
            this.tbLeft.BorderColor = System.Drawing.Color.Black;
            this.tbLeft.BorderWidth = 2;
            this.tbLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLeft.ForeColor = System.Drawing.Color.White;
            this.tbLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.tbLeft.Location = new System.Drawing.Point(859, 698);
            this.tbLeft.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbLeft.Name = "tbLeft";
            this.tbLeft.RoundedCorners.BottomLeft = true;
            this.tbLeft.RoundedCorners.BottomRight = true;
            this.tbLeft.RoundedCorners.TopLeft = true;
            this.tbLeft.RoundedCorners.TopRight = true;
            this.tbLeft.RoundingDiameter = 20;
            this.tbLeft.Size = new System.Drawing.Size(66, 57);
            this.tbLeft.TabIndex = 64;
            this.tbLeft.TabStop = false;
            this.tbLeft.Text = "TouchButton";
            this.tbLeft.Click += new System.EventHandler(this.tbLeft_Click);
            // 
            // CustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.tbLeft);
            this.Controls.Add(this.tbRight);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.gbStoreCard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "CustomerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.gbStoreCard.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbStoreCard;
        private Controls.TouchButton bCancel;
        private System.Windows.Forms.Panel pStoreCard;
        private Controls.TouchButton tbCompany;
        private Controls.TouchButton tbPersons;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Controls.TouchButton tbY;
        private Controls.TouchButton tbZ;
        private Controls.TouchButton tbX;
        private Controls.TouchButton tbW;
        private Controls.TouchButton tbM;
        private Controls.TouchButton tbS;
        private Controls.TouchButton tbO;
        private Controls.TouchButton tbP;
        private Controls.TouchButton tbT;
        private Controls.TouchButton tbL;
        private Controls.TouchButton tbU;
        private Controls.TouchButton tbV;
        private Controls.TouchButton tbR;
        private Controls.TouchButton tbQ;
        private Controls.TouchButton tbN;
        private Controls.TouchButton tbJ;
        private Controls.TouchButton tbK;
        private Controls.TouchButton tbG;
        private Controls.TouchButton tbH;
        private Controls.TouchButton tbI;
        private Controls.TouchButton tbF;
        private Controls.TouchButton tbE;
        private Controls.TouchButton tbC;
        private Controls.TouchButton tbD;
        private Controls.TouchButton tbB;
        private Controls.TouchButton tbA;
        private Controls.TouchButton tbClearFilter;
        private System.Windows.Forms.TextBox tbFilter;
        private Controls.TouchButton tbRight;
        private Controls.TouchButton tbLeft;


    }
}

