﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Transware.Core;
using System.Threading;

namespace Transware.TouchPOS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!AppInfo.Instance.Init())
            {
                Setting s = Setting.Instance;
                AppInfo.Instance.SetDB(s.Server, s.User, s.Password);
            }

            //MessageBox.Show("Tato verze programu slouží jen pro otestování a databázi využívá jen pro čtení, proto nemůže dojít k žádné chybě ve stávajícíc verzí programu Transware.Net.");
            //Setting.Instance.Save();

            Application.ThreadException += ProcessException;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (Setting.Instance.LoginRequired)
                Application.Run(new UserSelectForm());
            else
                Application.Run(new MainForm());            
        }

        static void ProcessException(object sender, ThreadExceptionEventArgs e)
        {
            //MessageBox.Show("Došlo k neočekávané chybě v aplikaci. Může dojít ke ztrátě dat. Doporučujeme aplikaci ukončit a spustit znovu.", "Neočekávaná chyba aplikace", 
            //        MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            new UnhandledExceptionForm(e.Exception).ShowDialog();
        }
    }
}
