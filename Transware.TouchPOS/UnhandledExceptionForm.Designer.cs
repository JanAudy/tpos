﻿namespace Transware.TouchPOS
{
    partial class UnhandledExceptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bSend = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.bContinue = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bShowError = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox1.ForeColor = System.Drawing.Color.Red;
            this.textBox1.Location = new System.Drawing.Point(149, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(463, 42);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Došlo k neočekávané chybě v aplikaci. Může dojít ke ztrátě dat. \r\nDoporučujeme ap" +
                "likaci ukončit a spustit znovu.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Transware.TouchPOS.Properties.Resources.symbol_error128;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(149, 117);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(111, 23);
            this.bSend.TabIndex = 2;
            this.bSend.Text = "Odeslat chybu";
            this.toolTip1.SetToolTip(this.bSend, "Odešle chybu autorovi programu");
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // bClose
            // 
            this.bClose.Location = new System.Drawing.Point(383, 117);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(111, 23);
            this.bClose.TabIndex = 3;
            this.bClose.Text = "Ukončit aplikaci";
            this.toolTip1.SetToolTip(this.bClose, "Ukončí aplikaci");
            this.bClose.UseVisualStyleBackColor = true;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // bContinue
            // 
            this.bContinue.Location = new System.Drawing.Point(500, 117);
            this.bContinue.Name = "bContinue";
            this.bContinue.Size = new System.Drawing.Size(111, 23);
            this.bContinue.TabIndex = 4;
            this.bContinue.Text = "Pokračovat v práci";
            this.toolTip1.SetToolTip(this.bContinue, "Uzavře okno budete moci pokračovat v práci");
            this.bContinue.UseVisualStyleBackColor = true;
            this.bContinue.Click += new System.EventHandler(this.bContinue_Click);
            // 
            // bShowError
            // 
            this.bShowError.Location = new System.Drawing.Point(266, 117);
            this.bShowError.Name = "bShowError";
            this.bShowError.Size = new System.Drawing.Size(111, 23);
            this.bShowError.TabIndex = 5;
            this.bShowError.Text = "Zobrazit chybu";
            this.toolTip1.SetToolTip(this.bShowError, "Ukončí aplikaci");
            this.bShowError.UseVisualStyleBackColor = true;
            this.bShowError.Click += new System.EventHandler(this.bShowError_Click);
            // 
            // UnhandledExceptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 152);
            this.Controls.Add(this.bShowError);
            this.Controls.Add(this.bContinue);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.bSend);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "UnhandledExceptionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Neočekávaná chyba aplikace";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.Button bContinue;
        private System.Windows.Forms.Button bShowError;
    }
}