﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Transware.Data.DataTypes;
using Transware.Data;
using Jadro.Data.DataTypes;
using Transware.Data.DataModel;
using Transware.Data.Enums;
using CustomerSys.Data;
using Jadro.Data.DataModel;
using Jadro.Data;
using Microdata.Util;

namespace Transware.TouchPOS
{
    public partial class UserLoginForm : Form
    {
        private UserShortcut user;

        public UserShortcut User { get { return user; } set { user = value; DrawData(); } }

        public UserLoginForm(UserShortcut user)
        {
            this.user = user;

            InitializeComponent();

            DrawData();
        }


        
        private void NumberButtonClick(object sender, EventArgs e)
        {
            tbPassword.Text += (sender as TouchButton).Tag.ToString();
        }

        private void ClearCodeClick(object sender, EventArgs e)
        {
            tbPassword.Text = "";
        }

        private void DeleteLastClick(object sender, EventArgs e)
        {
            if (tbPassword.Text.Length > 0)
                tbPassword.Text = tbPassword.Text.Substring(0, tbPassword.Text.Length - 1);
        }

        private void DrawData()
        {
            if (user!=null)
                lUser.Text = user.FullName;
        }

        private void Confirm(object sender, EventArgs e)
        {
            string pass = CryptoUtil.RijndaelEncryptorString(tbPassword.Text);
            tbPassword.Text = "";

            if (user.Password == pass)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                lError.Text = "Zadáno nesprávné heslo";
                tbPassword.Text = "";
                return;
            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                Confirm(sender, e);
            }
            else if (e.KeyCode == Keys.Escape || e.KeyCode == Keys.Delete)
            {
                ClearCodeClick(sender, e);
            }
            else if (e.KeyCode == Keys.Back)
            {
                DeleteLastClick(sender, e);
            }
            else if (e.KeyCode == Keys.F4 && e.Alt)
                return;
            else if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            {
                return;
            }
            else
            {
                char c = KeyboardUtil.GetKey(e.KeyCode);
                if (c >= 32)
                {
                    tbPassword.Text += c;

                    e.Handled = true;
                }
            }

        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        
        
        
        
    }
}
