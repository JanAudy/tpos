﻿namespace Transware.TouchPOS
{
    partial class UserLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle1 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle2 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle3 = new BrightIdeasSoftware.HeaderStateStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserLoginForm));
            this.headerFormatStyle1 = new BrightIdeasSoftware.HeaderFormatStyle();
            this.pKeys = new System.Windows.Forms.Panel();
            this.b7 = new Transware.Controls.TouchButton();
            this.b9 = new Transware.Controls.TouchButton();
            this.b8 = new Transware.Controls.TouchButton();
            this.b6 = new Transware.Controls.TouchButton();
            this.b5 = new Transware.Controls.TouchButton();
            this.b4 = new Transware.Controls.TouchButton();
            this.b3 = new Transware.Controls.TouchButton();
            this.b2 = new Transware.Controls.TouchButton();
            this.b1 = new Transware.Controls.TouchButton();
            this.bDot = new Transware.Controls.TouchButton();
            this.b0 = new Transware.Controls.TouchButton();
            this.bTimes = new Transware.Controls.TouchButton();
            this.bDel = new Transware.Controls.TouchButton();
            this.bBackspace = new Transware.Controls.TouchButton();
            this.bConfirm = new Transware.Controls.TouchButton();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lUser = new System.Windows.Forms.Label();
            this.bCancel = new Transware.Controls.TouchButton();
            this.bClose = new Transware.Controls.TouchButton();
            this.lError = new System.Windows.Forms.Label();
            this.pKeys.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerFormatStyle1
            // 
            headerStateStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Hot = headerStateStyle1;
            headerStateStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Normal = headerStateStyle2;
            headerStateStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Pressed = headerStateStyle3;
            // 
            // pKeys
            // 
            this.pKeys.Controls.Add(this.b7);
            this.pKeys.Controls.Add(this.b9);
            this.pKeys.Controls.Add(this.b8);
            this.pKeys.Controls.Add(this.b6);
            this.pKeys.Controls.Add(this.b5);
            this.pKeys.Controls.Add(this.b4);
            this.pKeys.Controls.Add(this.b3);
            this.pKeys.Controls.Add(this.b2);
            this.pKeys.Controls.Add(this.b1);
            this.pKeys.Controls.Add(this.bDot);
            this.pKeys.Controls.Add(this.b0);
            this.pKeys.Controls.Add(this.bTimes);
            this.pKeys.Controls.Add(this.bDel);
            this.pKeys.Controls.Add(this.bBackspace);
            this.pKeys.Controls.Add(this.bConfirm);
            this.pKeys.Location = new System.Drawing.Point(363, 321);
            this.pKeys.Name = "pKeys";
            this.pKeys.Size = new System.Drawing.Size(298, 316);
            this.pKeys.TabIndex = 49;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.SystemColors.Control;
            this.b7.BorderColor = System.Drawing.Color.Black;
            this.b7.BorderWidth = 2;
            this.b7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b7.FillColorBottom = System.Drawing.Color.Blue;
            this.b7.FillColorTop = System.Drawing.Color.Blue;
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b7.ForeColor = System.Drawing.Color.White;
            this.b7.Image = null;
            this.b7.Location = new System.Drawing.Point(0, 0);
            this.b7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b7.Name = "b7";
            this.b7.RoundedCorners.BottomLeft = true;
            this.b7.RoundedCorners.BottomRight = true;
            this.b7.RoundedCorners.TopLeft = true;
            this.b7.RoundedCorners.TopRight = true;
            this.b7.RoundingDiameter = 15;
            this.b7.Size = new System.Drawing.Size(95, 60);
            this.b7.TabIndex = 0;
            this.b7.TabStop = false;
            this.b7.Tag = "7";
            this.b7.Text = "7";
            this.b7.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.SystemColors.Control;
            this.b9.BorderColor = System.Drawing.Color.Black;
            this.b9.BorderWidth = 2;
            this.b9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b9.FillColorBottom = System.Drawing.Color.Blue;
            this.b9.FillColorTop = System.Drawing.Color.Blue;
            this.b9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b9.ForeColor = System.Drawing.Color.White;
            this.b9.Image = null;
            this.b9.Location = new System.Drawing.Point(203, 0);
            this.b9.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b9.Name = "b9";
            this.b9.RoundedCorners.BottomLeft = true;
            this.b9.RoundedCorners.BottomRight = true;
            this.b9.RoundedCorners.TopLeft = true;
            this.b9.RoundedCorners.TopRight = true;
            this.b9.RoundingDiameter = 15;
            this.b9.Size = new System.Drawing.Size(95, 60);
            this.b9.TabIndex = 3;
            this.b9.TabStop = false;
            this.b9.Tag = "9";
            this.b9.Text = "9";
            this.b9.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.SystemColors.Control;
            this.b8.BorderColor = System.Drawing.Color.Black;
            this.b8.BorderWidth = 2;
            this.b8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b8.FillColorBottom = System.Drawing.Color.Blue;
            this.b8.FillColorTop = System.Drawing.Color.Blue;
            this.b8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b8.ForeColor = System.Drawing.Color.White;
            this.b8.Image = null;
            this.b8.Location = new System.Drawing.Point(102, 0);
            this.b8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b8.Name = "b8";
            this.b8.RoundedCorners.BottomLeft = true;
            this.b8.RoundedCorners.BottomRight = true;
            this.b8.RoundedCorners.TopLeft = true;
            this.b8.RoundedCorners.TopRight = true;
            this.b8.RoundingDiameter = 15;
            this.b8.Size = new System.Drawing.Size(95, 60);
            this.b8.TabIndex = 4;
            this.b8.TabStop = false;
            this.b8.Tag = "8";
            this.b8.Text = "8";
            this.b8.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.SystemColors.Control;
            this.b6.BorderColor = System.Drawing.Color.Black;
            this.b6.BorderWidth = 2;
            this.b6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b6.FillColorBottom = System.Drawing.Color.Blue;
            this.b6.FillColorTop = System.Drawing.Color.Blue;
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b6.ForeColor = System.Drawing.Color.White;
            this.b6.Image = null;
            this.b6.Location = new System.Drawing.Point(203, 64);
            this.b6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b6.Name = "b6";
            this.b6.RoundedCorners.BottomLeft = true;
            this.b6.RoundedCorners.BottomRight = true;
            this.b6.RoundedCorners.TopLeft = true;
            this.b6.RoundedCorners.TopRight = true;
            this.b6.RoundingDiameter = 15;
            this.b6.Size = new System.Drawing.Size(95, 60);
            this.b6.TabIndex = 5;
            this.b6.TabStop = false;
            this.b6.Tag = "6";
            this.b6.Text = "6";
            this.b6.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.SystemColors.Control;
            this.b5.BorderColor = System.Drawing.Color.Black;
            this.b5.BorderWidth = 2;
            this.b5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b5.FillColorBottom = System.Drawing.Color.Blue;
            this.b5.FillColorTop = System.Drawing.Color.Blue;
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b5.ForeColor = System.Drawing.Color.White;
            this.b5.Image = null;
            this.b5.Location = new System.Drawing.Point(102, 64);
            this.b5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b5.Name = "b5";
            this.b5.RoundedCorners.BottomLeft = true;
            this.b5.RoundedCorners.BottomRight = true;
            this.b5.RoundedCorners.TopLeft = true;
            this.b5.RoundedCorners.TopRight = true;
            this.b5.RoundingDiameter = 15;
            this.b5.Size = new System.Drawing.Size(95, 60);
            this.b5.TabIndex = 6;
            this.b5.TabStop = false;
            this.b5.Tag = "5";
            this.b5.Text = "5";
            this.b5.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.SystemColors.Control;
            this.b4.BorderColor = System.Drawing.Color.Black;
            this.b4.BorderWidth = 2;
            this.b4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b4.FillColorBottom = System.Drawing.Color.Blue;
            this.b4.FillColorTop = System.Drawing.Color.Blue;
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b4.ForeColor = System.Drawing.Color.White;
            this.b4.Image = null;
            this.b4.Location = new System.Drawing.Point(0, 64);
            this.b4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b4.Name = "b4";
            this.b4.RoundedCorners.BottomLeft = true;
            this.b4.RoundedCorners.BottomRight = true;
            this.b4.RoundedCorners.TopLeft = true;
            this.b4.RoundedCorners.TopRight = true;
            this.b4.RoundingDiameter = 15;
            this.b4.Size = new System.Drawing.Size(95, 60);
            this.b4.TabIndex = 7;
            this.b4.TabStop = false;
            this.b4.Tag = "4";
            this.b4.Text = "4";
            this.b4.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.SystemColors.Control;
            this.b3.BorderColor = System.Drawing.Color.Black;
            this.b3.BorderWidth = 2;
            this.b3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b3.FillColorBottom = System.Drawing.Color.Blue;
            this.b3.FillColorTop = System.Drawing.Color.Blue;
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b3.ForeColor = System.Drawing.Color.White;
            this.b3.Image = null;
            this.b3.Location = new System.Drawing.Point(203, 128);
            this.b3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b3.Name = "b3";
            this.b3.RoundedCorners.BottomLeft = true;
            this.b3.RoundedCorners.BottomRight = true;
            this.b3.RoundedCorners.TopLeft = true;
            this.b3.RoundedCorners.TopRight = true;
            this.b3.RoundingDiameter = 15;
            this.b3.Size = new System.Drawing.Size(95, 60);
            this.b3.TabIndex = 8;
            this.b3.TabStop = false;
            this.b3.Tag = "3";
            this.b3.Text = "3";
            this.b3.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.SystemColors.Control;
            this.b2.BorderColor = System.Drawing.Color.Black;
            this.b2.BorderWidth = 2;
            this.b2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b2.FillColorBottom = System.Drawing.Color.Blue;
            this.b2.FillColorTop = System.Drawing.Color.Blue;
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b2.ForeColor = System.Drawing.Color.White;
            this.b2.Image = null;
            this.b2.Location = new System.Drawing.Point(102, 128);
            this.b2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b2.Name = "b2";
            this.b2.RoundedCorners.BottomLeft = true;
            this.b2.RoundedCorners.BottomRight = true;
            this.b2.RoundedCorners.TopLeft = true;
            this.b2.RoundedCorners.TopRight = true;
            this.b2.RoundingDiameter = 15;
            this.b2.Size = new System.Drawing.Size(95, 60);
            this.b2.TabIndex = 9;
            this.b2.TabStop = false;
            this.b2.Tag = "2";
            this.b2.Text = "2";
            this.b2.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.SystemColors.Control;
            this.b1.BorderColor = System.Drawing.Color.Black;
            this.b1.BorderWidth = 2;
            this.b1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b1.FillColorBottom = System.Drawing.Color.Blue;
            this.b1.FillColorTop = System.Drawing.Color.Blue;
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b1.ForeColor = System.Drawing.Color.White;
            this.b1.Image = null;
            this.b1.Location = new System.Drawing.Point(0, 128);
            this.b1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b1.Name = "b1";
            this.b1.RoundedCorners.BottomLeft = true;
            this.b1.RoundedCorners.BottomRight = true;
            this.b1.RoundedCorners.TopLeft = true;
            this.b1.RoundedCorners.TopRight = true;
            this.b1.RoundingDiameter = 15;
            this.b1.Size = new System.Drawing.Size(95, 60);
            this.b1.TabIndex = 10;
            this.b1.TabStop = false;
            this.b1.Tag = "1";
            this.b1.Text = "1";
            this.b1.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDot
            // 
            this.bDot.BackColor = System.Drawing.SystemColors.Control;
            this.bDot.BorderColor = System.Drawing.Color.Black;
            this.bDot.BorderWidth = 2;
            this.bDot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDot.FillColorBottom = System.Drawing.Color.Blue;
            this.bDot.FillColorTop = System.Drawing.Color.Blue;
            this.bDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDot.ForeColor = System.Drawing.Color.White;
            this.bDot.Image = null;
            this.bDot.Location = new System.Drawing.Point(203, 192);
            this.bDot.Margin = new System.Windows.Forms.Padding(2);
            this.bDot.Name = "bDot";
            this.bDot.RoundedCorners.BottomLeft = true;
            this.bDot.RoundedCorners.BottomRight = true;
            this.bDot.RoundedCorners.TopLeft = true;
            this.bDot.RoundedCorners.TopRight = true;
            this.bDot.RoundingDiameter = 15;
            this.bDot.Size = new System.Drawing.Size(95, 60);
            this.bDot.TabIndex = 11;
            this.bDot.TabStop = false;
            this.bDot.Tag = ",";
            this.bDot.Text = ",";
            this.bDot.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b0
            // 
            this.b0.BackColor = System.Drawing.SystemColors.Control;
            this.b0.BorderColor = System.Drawing.Color.Black;
            this.b0.BorderWidth = 2;
            this.b0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b0.FillColorBottom = System.Drawing.Color.Blue;
            this.b0.FillColorTop = System.Drawing.Color.Blue;
            this.b0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b0.ForeColor = System.Drawing.Color.White;
            this.b0.Image = null;
            this.b0.Location = new System.Drawing.Point(102, 192);
            this.b0.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b0.Name = "b0";
            this.b0.RoundedCorners.BottomLeft = true;
            this.b0.RoundedCorners.BottomRight = true;
            this.b0.RoundedCorners.TopLeft = true;
            this.b0.RoundedCorners.TopRight = true;
            this.b0.RoundingDiameter = 15;
            this.b0.Size = new System.Drawing.Size(95, 60);
            this.b0.TabIndex = 12;
            this.b0.TabStop = false;
            this.b0.Tag = "0";
            this.b0.Text = "0";
            this.b0.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bTimes
            // 
            this.bTimes.BackColor = System.Drawing.SystemColors.Control;
            this.bTimes.BorderColor = System.Drawing.Color.Black;
            this.bTimes.BorderWidth = 2;
            this.bTimes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bTimes.FillColorBottom = System.Drawing.Color.Blue;
            this.bTimes.FillColorTop = System.Drawing.Color.Blue;
            this.bTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bTimes.ForeColor = System.Drawing.Color.White;
            this.bTimes.Image = null;
            this.bTimes.Location = new System.Drawing.Point(0, 192);
            this.bTimes.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bTimes.Name = "bTimes";
            this.bTimes.RoundedCorners.BottomLeft = true;
            this.bTimes.RoundedCorners.BottomRight = true;
            this.bTimes.RoundedCorners.TopLeft = true;
            this.bTimes.RoundedCorners.TopRight = true;
            this.bTimes.RoundingDiameter = 15;
            this.bTimes.Size = new System.Drawing.Size(95, 60);
            this.bTimes.TabIndex = 13;
            this.bTimes.TabStop = false;
            this.bTimes.Text = "X";
            // 
            // bDel
            // 
            this.bDel.BackColor = System.Drawing.SystemColors.Control;
            this.bDel.BorderColor = System.Drawing.Color.Black;
            this.bDel.BorderWidth = 2;
            this.bDel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDel.FillColorBottom = System.Drawing.Color.Red;
            this.bDel.FillColorTop = System.Drawing.Color.Red;
            this.bDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDel.ForeColor = System.Drawing.Color.White;
            this.bDel.Image = null;
            this.bDel.Location = new System.Drawing.Point(-1, 256);
            this.bDel.Margin = new System.Windows.Forms.Padding(2);
            this.bDel.Name = "bDel";
            this.bDel.RoundedCorners.BottomLeft = true;
            this.bDel.RoundedCorners.BottomRight = true;
            this.bDel.RoundedCorners.TopLeft = true;
            this.bDel.RoundedCorners.TopRight = true;
            this.bDel.RoundingDiameter = 15;
            this.bDel.Size = new System.Drawing.Size(95, 60);
            this.bDel.TabIndex = 14;
            this.bDel.TabStop = false;
            this.bDel.Text = "Smazat";
            this.bDel.Click += new System.EventHandler(this.ClearCodeClick);
            // 
            // bBackspace
            // 
            this.bBackspace.BackColor = System.Drawing.SystemColors.Control;
            this.bBackspace.BorderColor = System.Drawing.Color.Black;
            this.bBackspace.BorderWidth = 2;
            this.bBackspace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bBackspace.FillColorBottom = System.Drawing.Color.Red;
            this.bBackspace.FillColorTop = System.Drawing.Color.Red;
            this.bBackspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bBackspace.ForeColor = System.Drawing.Color.White;
            this.bBackspace.Image = ((System.Drawing.Image)(resources.GetObject("bBackspace.Image")));
            this.bBackspace.Location = new System.Drawing.Point(102, 256);
            this.bBackspace.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bBackspace.Name = "bBackspace";
            this.bBackspace.RoundedCorners.BottomLeft = true;
            this.bBackspace.RoundedCorners.BottomRight = true;
            this.bBackspace.RoundedCorners.TopLeft = true;
            this.bBackspace.RoundedCorners.TopRight = true;
            this.bBackspace.RoundingDiameter = 15;
            this.bBackspace.Size = new System.Drawing.Size(95, 60);
            this.bBackspace.TabIndex = 15;
            this.bBackspace.TabStop = false;
            this.bBackspace.Text = "0";
            this.bBackspace.Click += new System.EventHandler(this.DeleteLastClick);
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.bConfirm.BorderColor = System.Drawing.Color.Black;
            this.bConfirm.BorderWidth = 2;
            this.bConfirm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bConfirm.FillColorBottom = System.Drawing.Color.Blue;
            this.bConfirm.FillColorTop = System.Drawing.Color.Blue;
            this.bConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Image = null;
            this.bConfirm.Location = new System.Drawing.Point(203, 256);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.RoundedCorners.BottomLeft = true;
            this.bConfirm.RoundedCorners.BottomRight = true;
            this.bConfirm.RoundedCorners.TopLeft = true;
            this.bConfirm.RoundedCorners.TopRight = true;
            this.bConfirm.RoundingDiameter = 15;
            this.bConfirm.Size = new System.Drawing.Size(95, 60);
            this.bConfirm.TabIndex = 16;
            this.bConfirm.TabStop = false;
            this.bConfirm.Text = "Potvrdit";
            this.bConfirm.Click += new System.EventHandler(this.Confirm);
            // 
            // tbPassword
            // 
            this.tbPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPassword.Location = new System.Drawing.Point(181, 198);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.ReadOnly = true;
            this.tbPassword.Size = new System.Drawing.Size(663, 62);
            this.tbPassword.TabIndex = 82;
            this.tbPassword.TabStop = false;
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lUser
            // 
            this.lUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lUser.Location = new System.Drawing.Point(181, 164);
            this.lUser.Name = "lUser";
            this.lUser.Size = new System.Drawing.Size(663, 31);
            this.lUser.TabIndex = 81;
            this.lUser.Text = "Uživatel:";
            this.lUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(309, 655);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 61;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Click += new System.EventHandler(this.bClose_Click);
            // 
            // bClose
            // 
            this.bClose.BorderColor = System.Drawing.Color.Black;
            this.bClose.BorderWidth = 2;
            this.bClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bClose.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bClose.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bClose.Image = null;
            this.bClose.Location = new System.Drawing.Point(543, 655);
            this.bClose.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bClose.Name = "bClose";
            this.bClose.RoundedCorners.BottomLeft = true;
            this.bClose.RoundedCorners.BottomRight = true;
            this.bClose.RoundedCorners.TopLeft = true;
            this.bClose.RoundedCorners.TopRight = true;
            this.bClose.RoundingDiameter = 20;
            this.bClose.Size = new System.Drawing.Size(203, 57);
            this.bClose.TabIndex = 66;
            this.bClose.TabStop = false;
            this.bClose.Text = "OK";
            this.bClose.Click += new System.EventHandler(this.Confirm);
            // 
            // lError
            // 
            this.lError.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lError.ForeColor = System.Drawing.Color.Red;
            this.lError.Location = new System.Drawing.Point(66, 274);
            this.lError.Name = "lError";
            this.lError.Size = new System.Drawing.Size(888, 31);
            this.lError.TabIndex = 83;
            this.lError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.lError);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.lUser);
            this.Controls.Add(this.pKeys);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "UserLoginForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.pKeys.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.TouchButton b7;
        private Controls.TouchButton b9;
        private Controls.TouchButton b8;
        private Controls.TouchButton b6;
        private Controls.TouchButton b5;
        private Controls.TouchButton b4;
        private Controls.TouchButton b1;
        private Controls.TouchButton b2;
        private Controls.TouchButton b3;
        private Controls.TouchButton bTimes;
        private Controls.TouchButton b0;
        private Controls.TouchButton bDot;
        private Controls.TouchButton bConfirm;
        private Controls.TouchButton bBackspace;
        private Controls.TouchButton bDel;
        private BrightIdeasSoftware.HeaderFormatStyle headerFormatStyle1;
        private System.Windows.Forms.Panel pKeys;
        private Controls.TouchButton bCancel;
        private Controls.TouchButton bClose;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lUser;
        private System.Windows.Forms.Label lError;

    }
}

