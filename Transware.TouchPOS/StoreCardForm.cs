﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Microdata.Util;
using Transware.Data.DataTypes;
using Transware.Data;
using Transware.Data.DataModel;
using Microdata.Data.DataTypes;

namespace Transware.TouchPOS
{
    public partial class StoreCardForm : Form
    {
        private Category root;
        private Category actual;

        private List<TouchButton> buttons = new List<TouchButton>();

        public string Code { get; protected set; }
        public PackageSummary Package { get; protected set; }
        public int startIndex = 0;
        public List<AbstractValidDto> objects = new List<AbstractValidDto>();

        public StoreCardForm()
        {
            InitializeComponent();

            actual = root = CategoryModel.Instance.GetTree();

            BuildButtons();

            PrepareData();

            DrawData();
        }

        
        private void BuildButtons()
        {
            int width = pStoreCard.Width;
            int height = pStoreCard.Height;

            int bw = 138;
            int bh = 65;
            int space = 5;
            for (int y = 6; y < height - bh; y += bh + space)
            {
                for (int x = 10; x < width - bw; x += bw + space)    
                {
                    TouchButton b = new TouchButton();
                    b.Font = new Font(b.Font.FontFamily, 14);
                    b.BorderColor = Color.Black;
                    b.BorderWidth = 1;
                    b.BorderColor = Color.Black;
                    b.RoundingDiameter = 10;
                    b.Left = x;
                    b.Top = y;
                    b.Width = bw;
                    b.Height = bh;
                    b.Visible = true;
                    b.Click += CategoryPackageClick;
                    pStoreCard.Controls.Add(b);
                    buttons.Add(b);
                }
            }
        }

        private void PrepareData()
        {
            objects.Clear();
            objects.AddRange(actual.Subcategories.ToArray());
            List<PackageSummary> packages = PackageSummaryModel.Instance.GetList(actual.ID, Setting.Instance.Store);
            objects.AddRange(packages.ToArray());
        }

        private void DrawData()
        {
            int index = 0;
            for(int i=startIndex;i<objects.Count; i++) 
            {
                if (objects[i] is Category)
                {
                    Category c = objects[i] as Category;
                    buttons[index].Enabled = true;
                    buttons[index].Text = c.Name.ToUpper();
                    buttons[index].FillColorBottom = buttons[index].FillColorTop = Color.LightSteelBlue;
                    buttons[index].Tag = c;
                }
                else if (objects[i] is PackageSummary)
                {
                    PackageSummary p = objects[i] as PackageSummary;
                    buttons[index].Enabled = true;
                    buttons[index].Text = p.Name;
                    buttons[index].FillColorBottom = buttons[index].FillColorTop = Color.LightSkyBlue;
                    buttons[index].Tag = p;
                }
                index++;
                if (index == buttons.Count)
                    break;
            }

            bLeft.Visible = startIndex != 0;
            bRight.Visible = (startIndex+index) < objects.Count;

            while (index < buttons.Count)
            {
                buttons[index].Text = "";
                buttons[index].Enabled = false;
                index++;
            }
        }

        private void CategoryPackageClick(object sender, EventArgs e)
        {
            if ((sender as Control).Tag is Category)
            {
                actual = (sender as Control).Tag as Category;
                PrepareData();
                DrawData();
            }
            else
            if ((sender as Control).Tag is PackageSummary)
            {
                Code = ((sender as Control).Tag as PackageSummary).Ean;
                Package = ((sender as Control).Tag as PackageSummary);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            //if (paymentMode)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        if (e.Control)
            //            bCloseRecord_Click(sender, e);
            //        else
            //            ConfirmProduct(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.Escape || e.KeyCode==Keys.Delete)
            //    {
            //        ClearCodeClick(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.Back)
            //    {
            //        DeleteLastClick(sender, e);
            //    }
            //    else if (e.KeyCode == Keys.F4 && e.Alt)
            //        return;
            //    else
            //    {
            //        char c = KeyboardUtil.GetKey(e.KeyCode);
            //        if (c >= 32)
            //        {
            //            tbPaid.Text += c;

            //            e.Handled = true;
            //        }
            //    }
            //}
            //else
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        ConfirmProduct(sender, e);
            //    }
            //}
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bLeft_Click(object sender, EventArgs e)
        {
            startIndex -= buttons.Count;
            DrawData();
        }

        private void bRight_Click(object sender, EventArgs e)
        {
            startIndex += buttons.Count;
            DrawData();
        }

    }
}
