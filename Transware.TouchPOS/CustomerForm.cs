﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Transware.Data.DataTypes;
using Transware.Data;
using Transware.Data.DataModel;
using Jadro.Data.DataTypes;
using Jadro.Data;
using Jadro.Data.DataModel;
using System.Globalization;
using Microdata.Util;

namespace Transware.TouchPOS
{
    public partial class CustomerForm : Form
    {
        private List<TouchButton> buttons = new List<TouchButton>();
        private List<IDMedia> medias = new List<IDMedia>();
        private List<Osoba> persons = new List<Osoba>();
        private List<Organizace> companies = new List<Organizace>();
        private List<Osoba> filteredPersons = new List<Osoba>();
        private List<Organizace> filteredCompanies = new List<Organizace>();

        public string Code { get; protected set; }
        public IDMedia Media { get; protected set; }
        public Osoba Person { get; protected set; }
        public Organizace Company { get; protected set; }

        private int CustomerType = 0;
        private int startIndex = 0;

        public string CardNo = "";

        public CustomerForm()
        {
            InitializeComponent();

            BuildButtons();

            medias = IDMedialModel.Instance.GetList();
            persons = OsobaModel.Instance.GetOdberatele();
            companies = OrganizaceModel.Instance.GetOdberatele();
            companies.Sort();
            persons.Sort();
            filteredPersons = persons;
            filteredCompanies = companies;
            DrawData();
        }

        private void BuildButtons()
        {
            int width = pStoreCard.Width;
            int height = pStoreCard.Height;

            int bw = 138;
            int bh = 65;
            int space = 5;
            for (int y = 6; y < height - bh; y += bh + space)
            {
                for (int x = 10; x < width - bw; x += bw + space)    
                {
                    TouchButton b = new TouchButton();
                    b.Font = new Font(b.Font.FontFamily, 14);
                    b.BorderColor = Color.Black;
                    b.BorderWidth = 1;
                    b.BorderColor = Color.Black;
                    b.RoundingDiameter = 10;
                    b.Left = x;
                    b.Top = y;
                    b.Width = bw;
                    b.Height = bh;
                    b.Visible = true;
                    b.Click += CategoryClick;
                    pStoreCard.Controls.Add(b);
                    buttons.Add(b);
                }
            }
        }

        private void MakeDisable(TouchButton button)
        {
            button.ForeColor = Color.Black;
            button.FillColorBottom = button.FillColorTop = Color.FromArgb(192, 192, 255);
        }


        private void MakeEnable(TouchButton button)
        {
            button.ForeColor = Color.White;
            button.FillColorBottom = button.FillColorTop = Color.Blue;
        }

        private void DrawData()
        {
            if (CustomerType == 0)
            {
                MakeEnable(tbPersons);
                MakeDisable(tbCompany);

                int index = 0;
                int actualIndex = startIndex;
                while (index < buttons.Count && actualIndex<filteredPersons.Count)
                {
                    Osoba id = filteredPersons[actualIndex];
                    buttons[index].Enabled = true;
                    buttons[index].Text = id.Full_Name ?? (id.Prijmeni + " " + id.Jmeno);
                    buttons[index].FillColorBottom = buttons[index].FillColorTop = Color.LightSkyBlue;
                    buttons[index].Tag = id;
                    index++;
                    actualIndex++;
                }

                tbLeft.Enabled = startIndex > 0;
                tbRight.Enabled = actualIndex < filteredPersons.Count;
                
                while (index < buttons.Count)
                {
                    buttons[index].Text = "";
                    buttons[index].Tag = null;
                    buttons[index].Enabled = false;
                    index++;
                }
            }
            else if (CustomerType == 1)
            {
                MakeDisable(tbPersons);
                MakeEnable(tbCompany);
                int index = 0;
                int actualIndex = startIndex;
                while (index < buttons.Count && actualIndex < filteredCompanies.Count)
                {
                    Organizace id = filteredCompanies[actualIndex];
                    buttons[index].Enabled = true;
                    buttons[index].Text = id.Nazev;
                    buttons[index].FillColorBottom = buttons[index].FillColorTop = Color.LightSkyBlue;
                    buttons[index].Tag = id;
                    index++;
                    actualIndex++;
                }

                tbLeft.Enabled = startIndex > 0;
                tbRight.Enabled = actualIndex < filteredCompanies.Count;
                
                while (index < buttons.Count)
                {
                    buttons[index].Text = "";
                    buttons[index].Tag = null;
                    buttons[index].Enabled = false;
                    index++;
                }
            }
        }

        private void CategoryClick(object sender, EventArgs e)
        {
            if ((sender as TouchButton).Tag is Osoba)
            {
                Person = (sender as TouchButton).Tag as Osoba;
            }
            else if ((sender as TouchButton).Tag is Organizace)
            {
                Company = (sender as TouchButton).Tag as Organizace;
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(CardNo))
            {
                FindCard(sender, e, CardNo);
                CardNo = "";
            }
            else if (e.KeyCode == Keys.Escape || e.KeyCode == Keys.Delete)
            {
                CardNo = "";
            }
            else if (e.KeyCode == Keys.Back)
            {
                if (CardNo.Length > 0)
                    CardNo = CardNo.Substring(0, CardNo.Length - 1);
            }
            else if (e.KeyCode == Keys.F4 && e.Alt)
                return;
            else
            {
                char c = KeyboardUtil.GetKey(e.KeyCode);
                if (c >= 32)
                {
                    CardNo += c;

                    e.Handled = true;
                }
            }
        }


        private void FindCard(object sender, KeyEventArgs e, string cardNo)
        {
            foreach (IDMedia id in medias)
            {
                if (id.Tag == cardNo)
                {
                    Code = id.Tag;
                    Media = id;
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    Close();
                    return;
                }
            }
            MessageBox.Show("Zákaznická karta s číslem '" + cardNo + "' nebyla nalezena.");
        }
        

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CustomerType_Click(object sender, EventArgs e)
        {
            int type = int.Parse((sender as Control).Tag as string);
            CustomerType = type;
            tbFilter.Text = "";
            startIndex = 0;
            DrawData();
        }

        private void tbLeft_Click(object sender, EventArgs e)
        {
            startIndex -= buttons.Count;
            if (startIndex < 0)
                startIndex = 0;
            DrawData();
        }

        private void tbRight_Click(object sender, EventArgs e)
        {
            startIndex += buttons.Count;
            DrawData();
        }

        private void Letter_Click(object sender, EventArgs e)
        {
            tbFilter.Text += (sender as TouchButton).Text as string;
            FilterChanged();
        }

        private void FilterChanged()
        {
            if (CustomerType == 0)
            {
                filteredPersons = new List<Osoba>();
                foreach(Osoba id in persons)
                    if (NormalizeName((id.Full_Name ?? (id.Prijmeni + " " + id.Jmeno))).ToLower().StartsWith(tbFilter.Text.ToLower()))
                        filteredPersons.Add(id);
            }
            else if (CustomerType == 1)
            {
                filteredCompanies = new List<Organizace>();
                foreach (Organizace id in companies)
                    if ((NormalizeName(id.Nazev).ToLower()).StartsWith(tbFilter.Text.ToLower()))
                        filteredCompanies.Add(id);
            }
            DrawData();
        }

        private string NormalizeName(string name)
        {
            string stFormD = name.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }

            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        private void tbClearFilter_Click(object sender, EventArgs e)
        {
            tbFilter.Text = "";
            FilterChanged();
        }

    }
}
