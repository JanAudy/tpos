﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using System.IO;
using Transware.Data.DataModel;
using System.Drawing;

namespace Transware.TouchPOS
{
    public class ShortcutInfo
    {
        public PackageSummary Package;
        public int ShortcutColor;

        public ShortcutInfo()
        {
            Package = null;
            ShortcutColor = Color.White.ToArgb();
        }
    }

    public class ShortcutIO
    {
        public const int ItemsPerPage = 18;
        public const int PageCount = 10;

        static string FileName
        {
            get
            {
                string fileName = "transware.shortcuts.xml";
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                return Path.Combine(path, fileName);
            }
        }

        public static List<ShortcutInfo[]> Load()
        {
            List<ShortcutInfo[]> list = new List<ShortcutInfo[]>();
            if (!File.Exists(FileName))
            {
                for (int i = 0; i < PageCount; i++)
                {
                    ShortcutInfo[] array = new ShortcutInfo[ItemsPerPage];
                    for (int j = 0; j < ItemsPerPage; j++)
                        array[j] = new ShortcutInfo();
                    list.Add(array);
                }
                return list;
            }
            using (StreamReader r = new StreamReader(FileName))
            {
                while (!r.EndOfStream)
                {
                    ShortcutInfo[] array = new ShortcutInfo[ItemsPerPage];
                    string line = r.ReadLine();
                    for (int i = 0; i < ItemsPerPage; i++)
                    {
                        array[i] = new ShortcutInfo();

                        line = r.ReadLine();
                        string[] parts = line.Split(" ".ToCharArray());
                        int id = int.Parse(parts[0]);
                        int color = int.Parse(parts[1]);
                        array[i].Package = (id == 0) ? null : PackageSummaryModel.Instance.Get(id, Setting.Instance.Store);
                        array[i].ShortcutColor = color;
                    }
                    list.Add(array);
                }
            }
            return list;
        }

        public static void Save(List<ShortcutInfo[]> list)
        {
            using (StreamWriter w = new StreamWriter(FileName))
            {
                for(int i=0; i<list.Count; i++)
                {
                    w.WriteLine("Page " + i);
                    ShortcutInfo[] array = list[i];
                    for (int j = 0; j < ItemsPerPage; j++)
                    {
                        w.WriteLine("{0} {1}", ((array[j].Package == null) ? 0 : array[j].Package.ID), array[j].ShortcutColor);
                    }
                }
                w.Close();
            }
        }
    }
}
