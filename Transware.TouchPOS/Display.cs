﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using PRP085Plugin;
using Transware.Plugins;
using System.Xml;
using Transware.Data.DataModel;
using System.IO.Ports;
using Transware.Plugins.Pos;

namespace Transware.TouchPOS
{
    public class Display
    {
        #region Singleton

        private static Display instance = null;

        public static Display Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Display();
                }

                return instance;
            }
        }

        #endregion

        private int LineLength = 20;


        private void SendString(string s)
        {
            if (!Setting.Instance.Display)
                return;

            try
            {
                SerialPort sp = new SerialPort();
                sp.PortName = Setting.Instance.DisplayCom;
                sp.BaudRate = 9600;
                sp.Parity = Parity.None;
                sp.StopBits = StopBits.One;
                sp.DataBits = 8;
                sp.Encoding = Encoding.ASCII;

                sp.Open();
                sp.Write(new byte[] { 12 }, 0, 1);
                sp.Write(DiacriticsConversion.RemoveDiacritics(s));
                sp.Close();
            }
            catch
            {
            }
        }

        public void DrawLines(string line1, string line2)
        {
            string s = "";
            if (line1.Length > LineLength)
                line1 = line1.Substring(0, LineLength);
            s += FillString(line1, LineLength);

            if (line2.Length > LineLength)
                line2 = line2.Substring(0, LineLength);
            s += FillString(line2, LineLength);

            SendString(s);
        }

        public void DrawParts(string line1left, string line1right, string line2left, string line2right)
        {
            string line1 = FillString(line1left, LineLength - line1right.Length) + line1right;
            string line2 = FillString(line2left, LineLength - line2right.Length) + line2right;

            DrawLines(line1, line2);
        }

        

        public string FillString(string x, int size)
        {
            while (x.Length < size)
            {
                x += " ";
            }
            return x;
        }
    }
}
