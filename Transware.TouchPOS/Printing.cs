﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Data.DataTypes;
using PRP085Plugin;
using Transware.Plugins;
using System.Xml;
using Transware.Data.DataModel;

namespace Transware.TouchPOS
{
    public class Printing
    {
        #region Singleton

        private static Printing instance = null;

        public static Printing Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Printing();
                }

                return instance;
            }
        }

        #endregion

        private int LineLength = 48;
        private string _chyba = "";
        public string Chyba
        {
            get { return _chyba; }
        }

        public void Print(XmlDocument xml, int copies = 1)
        {
            if (!Setting.Instance.Printer)
            {
                
                return;
            }
                

            PRP085 printer = new PRP085();
            PrinterConnectionSetting pcs = printer.Setting;
            
            pcs.Method = EPrinterConnectionMethod.Serial;
            pcs.SerialPort.Name = Setting.Instance.PrinterCom;
            pcs.SerialPort.BaudRate = Setting.Instance.PrinterSpeed;
            pcs.SerialPort.DataBits = Setting.Instance.PrinterDataBits;
            pcs.SerialPort.Handshake = System.IO.Ports.Handshake.None;
            pcs.SerialPort.Parity = Setting.Instance.PrinterParity;
            pcs.SerialPort.StopBits = Setting.Instance.PrinterStopBits;
            pcs.SerialPort.Timeout = 500;

            for (int i = 0; i < copies; i++)
                printer.Interpret(xml);
        }

        public string PrintTest()
        {
           

            PRP085 printer = new PRP085();
            PrinterConnectionSetting pcs = printer.Setting;

            pcs.Method = EPrinterConnectionMethod.Serial;
            pcs.SerialPort.Name = Setting.Instance.PrinterCom;
            pcs.SerialPort.BaudRate = Setting.Instance.PrinterSpeed;
            pcs.SerialPort.DataBits = Setting.Instance.PrinterDataBits;
            pcs.SerialPort.Handshake = System.IO.Ports.Handshake.None;
            pcs.SerialPort.Parity = Setting.Instance.PrinterParity;
            pcs.SerialPort.StopBits = Setting.Instance.PrinterStopBits;
            pcs.SerialPort.Timeout = 500;

            printer.PrintTestPage();
            return printer.Chyba;
        }

        public void Print(Bill bill)
        {
            XmlDocument xml = new XmlDocument();
            XmlElement root = xml.CreateElement("posReport");
            xml.AppendChild(root);

            ConfigClass config = ConfigClassModel.Instance.GetConfiguration();

            int copies = 1;

            // start

            
            //if (!string.IsNullOrEmpty(config.GetBillsFirstLine(Database.LoggedStore))) sb.Append(String.Format("<c><d>{0}\n</d>", config.GetBillsFirstLine(Database.LoggedStore)));

            if (!string.IsNullOrEmpty(config.BillsFirstLine)) 
                AddTitle(xml, root, config.BillsFirstLine);
            
            //foreach (string s in config.GetBillsHeader(Database.LoggedStore))
            foreach (string s in config.BillsHeaderStrings)
            {
                AddCenteredLine(xml, root, s);
            }

            AddSeparator(xml, root);

            AddBoldCenteredLine(xml, root, "ÚČTENKA č. " + bill.NumberStr);
            AddCenteredLine(xml, root, "Zjednodušený daňový doklad");
            AddCenteredLine(xml, root, "");

            AddLine(xml, root, FillString("Datum vystavení:", LineLength - 10) + bill.DateTime.ToString("d.M.yyyy"));
            AddLine(xml, root, FillString("Dat.uskut.zdaň.plnění:", LineLength - 10) + bill.DateTime.ToString("d.M.yyyy"));
            AddLine(xml, root, FillString("Měna:", LineLength - 10) + "CZK");
            AddSeparator(xml, root);
            AddBoldLine(xml, root, String.Format("{0,-5} {1}", "ID", "Název"));
            AddBoldLine(xml, root, String.Format("{0,5} {1,-10}{2,10}{3,10}{4,12}", "Množ.", "Mj", "Sazba", "Cena/Mj", "Cena"));
            
            foreach (BillItem row in bill.Items)
            {
                if (!row.Valid) continue;
                
                if (row.IndividualItem)
                {
                    AddLine(xml, root, String.Format("{0,-5} {1}", "", row.Name));
                    AddLine(xml, root, String.Format("{0,-5} {1,-10}{2,10}{3,10:0.00}{4,12:0.00}", (row.Amount).ToString("0.###"),
                        "", row.Vat, row.Price, row.TotalPrice));
                }
                else
                {
                    Package pkg = PackageModel.Instance.Get(row.Package.Value);
                    Product p = ProductModel.Instance.Get(pkg.ProductID.Value);
                    Unit u  = UnitModel.Instance.Get(p.UnitID.Value);
                    Vat v = VatModel.Instance.Get(p.VatID.Value);
                    AddLine(xml, root, String.Format("{0,-5} {1}", p.ID, p.ShortName));
                    AddLine(xml, root, String.Format("{0,-5} {1,-10}{2,10}{3,10:0.00}{4,12:0.00}", (row.TotalAmount * u.Multiply).ToString("0.###"),
                        u.ToString(), string.Format("{0:0.##} %", v.Tax), row.Price, row.TotalPrice));
                }
                if (row.Discount!=0)
                    AddLine(xml, root, String.Format("{0,36}{1,12:0.00}", "Sleva:", row.TotalPriceWithDiscount - row.TotalPrice));
            }

            AddSeparator(xml, root);
            AddLine(xml, root, FillString("Mezisoučet:", LineLength - 20) + String.Format("{0,20:0.00}", bill.TotalPrice));
            if (bill.Discount != 0)
            {
                AddLine(xml, root, FillString("Sleva:", LineLength - 20) + String.Format("{0,20}", bill.Discount.ToString("0.##") + " %"));
                AddLine(xml, root, FillString("Cena po slevě:", LineLength - 20) + String.Format("{0,20:0.00}", bill.TotalPriceWithDiscount));
            }

            if (bill.CustomerDiscount != 0)
            {
                AddLine(xml, root, FillString("Zákaznická sleva:", LineLength - 20) + String.Format("{0,20}", bill.CustomerDiscount.ToString("0.##") + " %"));
                AddLine(xml, root, FillString("Cena po slevě:", LineLength - 20) + String.Format("{0,20:0.00}", bill.TotalPriceWithCustomerDiscount));
            }
            
            if (((int)(100*bill.Rounding))!=0)
                AddLine(xml, root, FillString("Zaokrouhlení:", LineLength - 20) + String.Format("{0,20:0.00}", bill.Rounding));
            
            AddLine(xml, root, FillString("Množství:", LineLength - 20) + String.Format("{0,20}", bill.TotalAmount.ToString("F0")));

            //if (bill.VoucherPrice != 0)
            //    sb.Append(FillString("Poukazy:", LineLength - 20) + String.Format("{0,20:0.00}\n", bill.VoucherPrice));
            
            AddSeparator(xml, root);

            if (bill.PaymentType != null || bill.Payments.Count==1)
            {
                PaymentType pt;
                if (bill.PaymentType != null)
                    pt = PaymentTypeModel.Instance.Get(bill.PaymentType.Value);
                else
                    pt = PaymentTypeModel.Instance.Get(bill.Payments[0].PaymentType);

                copies = Math.Max(copies, pt.NoOfBills);

                AddBoldLine(xml, root, FillString(pt.Name.ToUpper(), LineLength - 20) +                 
                    String.Format("{0,20:0.00}", bill.TotalPriceWithRoundingAndVouchers));
            }
            else
            {
                AddBoldLine(xml, root, FillString("Celkem k úhradě:".ToUpper(), LineLength - 20) +
                              String.Format("{0,20:0.00}", bill.TotalPriceWithRoundingAndVouchers));
                AddSeparator(xml, root);
                foreach (BillPayment bp in bill.Payments)
                {
                    PaymentType pt = PaymentTypeModel.Instance.Get(bp.PaymentType);
                    copies = Math.Max(copies, pt.NoOfBills);
                    AddBoldLine(xml, root, FillString(pt.Name.ToUpper(), LineLength - 20) +
                              String.Format("{0,20:0.00}", bp.Amount));
                }
            }

            //if (bill.Paid != 0)
            //{
            //    AddSeparator(xml, root);
            //    AddLine(xml, root, FillString("Zaplaceno:", LineLength - 20) +
            //                  String.Format("{0,20:0.00}", bill.Paid));
            //    AddLine(xml, root, FillString("Vráceno:", LineLength - 20) +
            //                  String.Format("{0,20:0.00}", bill.Paid - bill.TotalPriceWithRoundingAndVouchers));
            //}

            AddSeparator(xml, root);
            AddLine(xml, root, "Souhrn daně:");
            AddLine(xml, root, String.Format("{0,5}{1,21}{2,11}{3,11}", "Sazba", "Daň", "Základ", "Celkem"));

            foreach (RecordTax tax in bill.Taxes)
            {
                AddLine(xml, root, String.Format("{0,3} %{1,21:0.00}{2,11:0.00}{3,11:0.00}", tax.Percent,
                                        tax.Price-tax.PriceNoVat, tax.PriceNoVat, tax.Price));
            }
            
            AddSeparator(xml, root);

            if (bill.Bkp != null)
            {
                Setting ls = Setting.Instance;

                AddLine(xml, root, string.Format("Provozovna: {0}\n", ls.EetShopName));
                AddLine(xml, root, string.Format("Pokladna: {0}\n", ls.EetDeviceName));
                AddLine(xml, root, string.Format("Datum a čas: {0}\n", bill.CloseDate.ToString("dd.MM.yyyy HH:mm:ss")));
                AddLine(xml, root, string.Format("BKP: {0}\n", bill.Bkp));

                if (bill.Fik != null)
                {
                    AddLine(xml, root, string.Format("FIK: {0}\n", bill.Fik));
                }
                else
                {
                    AddLine(xml, root, string.Format("PKP: {0}\n", bill.Pkp));
                }
                AddLine(xml, root, "Režim: Běžný");
                AddSeparator(xml, root);
            }
            //foreach (string s in config.GetBillsFooter(bill.Store))
            foreach (string s in config.BillsFooterStrings)
            {
                AddCenteredLine(xml, root,  s);
            }

            Print(xml, copies);
        }

        private void AddTitle(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;

            element = xml.CreateElement("title");
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);
        }

        private void AddLine(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;
            
            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);
        }

        private void AddRightAlignedLine(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;
            XmlAttribute attribute;

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("align");
            attribute.Value = "right";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);
        }

        private void AddCenteredLine(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;
            XmlAttribute attribute;

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("align");
            attribute.Value = "center";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);   
        }

        private void AddBoldLine(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;
            XmlAttribute attribute;

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("style");
            attribute.Value = "bold";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);
        }

        private void AddBoldCenteredLine(XmlDocument xml, XmlElement root, string text)
        {
            XmlElement element;
            XmlAttribute attribute;

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("style");
            attribute.Value = "bold";
            element.Attributes.Append(attribute);
            
            attribute = xml.CreateAttribute("align");
            attribute.Value = "center";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode(text));
            root.AppendChild(element);
        }

        private void AddSeparator(XmlDocument xml, XmlElement root)
        {
            XmlElement element;

            element = xml.CreateElement("separator");
            root.AppendChild(element);
        }

        public string FillString(string x, int size)
        {
            while (x.Length < size)
            {
                x += " ";
            }
            return x;
        }
    }
}
