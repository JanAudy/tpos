﻿namespace Transware.TouchPOS
{
    partial class StoreCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbStoreCard = new System.Windows.Forms.GroupBox();
            this.pStoreCard = new System.Windows.Forms.Panel();
            this.bCancel = new Transware.Controls.TouchButton();
            this.bRight = new Transware.Controls.TouchButton();
            this.bLeft = new Transware.Controls.TouchButton();
            this.gbStoreCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbStoreCard
            // 
            this.gbStoreCard.Controls.Add(this.pStoreCard);
            this.gbStoreCard.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStoreCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gbStoreCard.Location = new System.Drawing.Point(0, 0);
            this.gbStoreCard.Name = "gbStoreCard";
            this.gbStoreCard.Size = new System.Drawing.Size(1024, 688);
            this.gbStoreCard.TabIndex = 0;
            this.gbStoreCard.TabStop = false;
            this.gbStoreCard.Text = "Sortiment";
            // 
            // pStoreCard
            // 
            this.pStoreCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pStoreCard.Location = new System.Drawing.Point(3, 27);
            this.pStoreCard.Name = "pStoreCard";
            this.pStoreCard.Size = new System.Drawing.Size(1018, 658);
            this.pStoreCard.TabIndex = 0;
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(17, 698);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 62;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bRight
            // 
            this.bRight.BorderColor = System.Drawing.Color.Black;
            this.bRight.BorderWidth = 1;
            this.bRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.bRight.Location = new System.Drawing.Point(951, 698);
            this.bRight.Name = "bRight";
            this.bRight.RoundedCorners.BottomLeft = true;
            this.bRight.RoundedCorners.BottomRight = true;
            this.bRight.RoundedCorners.TopLeft = true;
            this.bRight.RoundedCorners.TopRight = true;
            this.bRight.RoundingDiameter = 15;
            this.bRight.Size = new System.Drawing.Size(57, 57);
            this.bRight.TabIndex = 64;
            this.bRight.TabStop = false;
            this.bRight.Text = "Položka";
            this.bRight.Click += new System.EventHandler(this.bRight_Click);
            // 
            // bLeft
            // 
            this.bLeft.BorderColor = System.Drawing.Color.Black;
            this.bLeft.BorderWidth = 1;
            this.bLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.bLeft.Location = new System.Drawing.Point(888, 698);
            this.bLeft.Name = "bLeft";
            this.bLeft.RoundedCorners.BottomLeft = true;
            this.bLeft.RoundedCorners.BottomRight = true;
            this.bLeft.RoundedCorners.TopLeft = true;
            this.bLeft.RoundedCorners.TopRight = true;
            this.bLeft.RoundingDiameter = 15;
            this.bLeft.Size = new System.Drawing.Size(57, 57);
            this.bLeft.TabIndex = 63;
            this.bLeft.TabStop = false;
            this.bLeft.Text = "Položka";
            this.bLeft.Click += new System.EventHandler(this.bLeft_Click);
            // 
            // StoreCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.bRight);
            this.Controls.Add(this.bLeft);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.gbStoreCard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "StoreCardForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.gbStoreCard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbStoreCard;
        private Controls.TouchButton bCancel;
        private System.Windows.Forms.Panel pStoreCard;
        private Controls.TouchButton bRight;
        private Controls.TouchButton bLeft;


    }
}

