﻿namespace Transware.TouchPOS
{
    partial class PriceChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle25 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle26 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle27 = new BrightIdeasSoftware.HeaderStateStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PriceChangeForm));
            this.headerFormatStyle1 = new BrightIdeasSoftware.HeaderFormatStyle();
            this.pKeys = new System.Windows.Forms.Panel();
            this.b7 = new Transware.Controls.TouchButton();
            this.b9 = new Transware.Controls.TouchButton();
            this.b8 = new Transware.Controls.TouchButton();
            this.b6 = new Transware.Controls.TouchButton();
            this.b5 = new Transware.Controls.TouchButton();
            this.b4 = new Transware.Controls.TouchButton();
            this.b3 = new Transware.Controls.TouchButton();
            this.b2 = new Transware.Controls.TouchButton();
            this.b1 = new Transware.Controls.TouchButton();
            this.bDot = new Transware.Controls.TouchButton();
            this.b0 = new Transware.Controls.TouchButton();
            this.bTimes = new Transware.Controls.TouchButton();
            this.bDel = new Transware.Controls.TouchButton();
            this.bBackspace = new Transware.Controls.TouchButton();
            this.bConfirm = new Transware.Controls.TouchButton();
            this.tbItem = new System.Windows.Forms.TextBox();
            this.tbAmount = new System.Windows.Forms.TextBox();
            this.tbStorePrice = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bStore = new Transware.Controls.TouchButton();
            this.tbCancel = new Transware.Controls.TouchButton();
            this.label4 = new System.Windows.Forms.Label();
            this.tbItemPrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbItemPriceDiscount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTotalPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbDiscount = new System.Windows.Forms.TextBox();
            this.pShortCuts = new System.Windows.Forms.Panel();
            this.bItem16 = new Transware.Controls.TouchButton();
            this.bItem17 = new Transware.Controls.TouchButton();
            this.bItem18 = new Transware.Controls.TouchButton();
            this.bItem13 = new Transware.Controls.TouchButton();
            this.bItem14 = new Transware.Controls.TouchButton();
            this.bItem15 = new Transware.Controls.TouchButton();
            this.bItem10 = new Transware.Controls.TouchButton();
            this.bItem11 = new Transware.Controls.TouchButton();
            this.bItem12 = new Transware.Controls.TouchButton();
            this.bItem7 = new Transware.Controls.TouchButton();
            this.bItem8 = new Transware.Controls.TouchButton();
            this.bItem9 = new Transware.Controls.TouchButton();
            this.bItem4 = new Transware.Controls.TouchButton();
            this.bItem5 = new Transware.Controls.TouchButton();
            this.bItem6 = new Transware.Controls.TouchButton();
            this.bItem1 = new Transware.Controls.TouchButton();
            this.bItem2 = new Transware.Controls.TouchButton();
            this.bItem3 = new Transware.Controls.TouchButton();
            this.pKeys.SuspendLayout();
            this.pShortCuts.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerFormatStyle1
            // 
            headerStateStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Hot = headerStateStyle25;
            headerStateStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Normal = headerStateStyle26;
            headerStateStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Pressed = headerStateStyle27;
            // 
            // pKeys
            // 
            this.pKeys.Controls.Add(this.b7);
            this.pKeys.Controls.Add(this.b9);
            this.pKeys.Controls.Add(this.b8);
            this.pKeys.Controls.Add(this.b6);
            this.pKeys.Controls.Add(this.b5);
            this.pKeys.Controls.Add(this.b4);
            this.pKeys.Controls.Add(this.b3);
            this.pKeys.Controls.Add(this.b2);
            this.pKeys.Controls.Add(this.b1);
            this.pKeys.Controls.Add(this.bDot);
            this.pKeys.Controls.Add(this.b0);
            this.pKeys.Controls.Add(this.bTimes);
            this.pKeys.Controls.Add(this.bDel);
            this.pKeys.Controls.Add(this.bBackspace);
            this.pKeys.Controls.Add(this.bConfirm);
            this.pKeys.Location = new System.Drawing.Point(709, 321);
            this.pKeys.Name = "pKeys";
            this.pKeys.Size = new System.Drawing.Size(298, 316);
            this.pKeys.TabIndex = 49;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.SystemColors.Control;
            this.b7.BorderColor = System.Drawing.Color.Black;
            this.b7.BorderWidth = 2;
            this.b7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b7.FillColorBottom = System.Drawing.Color.Blue;
            this.b7.FillColorTop = System.Drawing.Color.Blue;
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b7.ForeColor = System.Drawing.Color.White;
            this.b7.Image = null;
            this.b7.Location = new System.Drawing.Point(0, 0);
            this.b7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b7.Name = "b7";
            this.b7.RoundedCorners.BottomLeft = true;
            this.b7.RoundedCorners.BottomRight = true;
            this.b7.RoundedCorners.TopLeft = true;
            this.b7.RoundedCorners.TopRight = true;
            this.b7.RoundingDiameter = 15;
            this.b7.Size = new System.Drawing.Size(95, 60);
            this.b7.TabIndex = 0;
            this.b7.TabStop = false;
            this.b7.Tag = "7";
            this.b7.Text = "7";
            this.b7.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.SystemColors.Control;
            this.b9.BorderColor = System.Drawing.Color.Black;
            this.b9.BorderWidth = 2;
            this.b9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b9.FillColorBottom = System.Drawing.Color.Blue;
            this.b9.FillColorTop = System.Drawing.Color.Blue;
            this.b9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b9.ForeColor = System.Drawing.Color.White;
            this.b9.Image = null;
            this.b9.Location = new System.Drawing.Point(203, 0);
            this.b9.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b9.Name = "b9";
            this.b9.RoundedCorners.BottomLeft = true;
            this.b9.RoundedCorners.BottomRight = true;
            this.b9.RoundedCorners.TopLeft = true;
            this.b9.RoundedCorners.TopRight = true;
            this.b9.RoundingDiameter = 15;
            this.b9.Size = new System.Drawing.Size(95, 60);
            this.b9.TabIndex = 3;
            this.b9.TabStop = false;
            this.b9.Tag = "9";
            this.b9.Text = "9";
            this.b9.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.SystemColors.Control;
            this.b8.BorderColor = System.Drawing.Color.Black;
            this.b8.BorderWidth = 2;
            this.b8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b8.FillColorBottom = System.Drawing.Color.Blue;
            this.b8.FillColorTop = System.Drawing.Color.Blue;
            this.b8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b8.ForeColor = System.Drawing.Color.White;
            this.b8.Image = null;
            this.b8.Location = new System.Drawing.Point(102, 0);
            this.b8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b8.Name = "b8";
            this.b8.RoundedCorners.BottomLeft = true;
            this.b8.RoundedCorners.BottomRight = true;
            this.b8.RoundedCorners.TopLeft = true;
            this.b8.RoundedCorners.TopRight = true;
            this.b8.RoundingDiameter = 15;
            this.b8.Size = new System.Drawing.Size(95, 60);
            this.b8.TabIndex = 4;
            this.b8.TabStop = false;
            this.b8.Tag = "8";
            this.b8.Text = "8";
            this.b8.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.SystemColors.Control;
            this.b6.BorderColor = System.Drawing.Color.Black;
            this.b6.BorderWidth = 2;
            this.b6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b6.FillColorBottom = System.Drawing.Color.Blue;
            this.b6.FillColorTop = System.Drawing.Color.Blue;
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b6.ForeColor = System.Drawing.Color.White;
            this.b6.Image = null;
            this.b6.Location = new System.Drawing.Point(203, 64);
            this.b6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b6.Name = "b6";
            this.b6.RoundedCorners.BottomLeft = true;
            this.b6.RoundedCorners.BottomRight = true;
            this.b6.RoundedCorners.TopLeft = true;
            this.b6.RoundedCorners.TopRight = true;
            this.b6.RoundingDiameter = 15;
            this.b6.Size = new System.Drawing.Size(95, 60);
            this.b6.TabIndex = 5;
            this.b6.TabStop = false;
            this.b6.Tag = "6";
            this.b6.Text = "6";
            this.b6.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.SystemColors.Control;
            this.b5.BorderColor = System.Drawing.Color.Black;
            this.b5.BorderWidth = 2;
            this.b5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b5.FillColorBottom = System.Drawing.Color.Blue;
            this.b5.FillColorTop = System.Drawing.Color.Blue;
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b5.ForeColor = System.Drawing.Color.White;
            this.b5.Image = null;
            this.b5.Location = new System.Drawing.Point(102, 64);
            this.b5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b5.Name = "b5";
            this.b5.RoundedCorners.BottomLeft = true;
            this.b5.RoundedCorners.BottomRight = true;
            this.b5.RoundedCorners.TopLeft = true;
            this.b5.RoundedCorners.TopRight = true;
            this.b5.RoundingDiameter = 15;
            this.b5.Size = new System.Drawing.Size(95, 60);
            this.b5.TabIndex = 6;
            this.b5.TabStop = false;
            this.b5.Tag = "5";
            this.b5.Text = "5";
            this.b5.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.SystemColors.Control;
            this.b4.BorderColor = System.Drawing.Color.Black;
            this.b4.BorderWidth = 2;
            this.b4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b4.FillColorBottom = System.Drawing.Color.Blue;
            this.b4.FillColorTop = System.Drawing.Color.Blue;
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b4.ForeColor = System.Drawing.Color.White;
            this.b4.Image = null;
            this.b4.Location = new System.Drawing.Point(0, 64);
            this.b4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b4.Name = "b4";
            this.b4.RoundedCorners.BottomLeft = true;
            this.b4.RoundedCorners.BottomRight = true;
            this.b4.RoundedCorners.TopLeft = true;
            this.b4.RoundedCorners.TopRight = true;
            this.b4.RoundingDiameter = 15;
            this.b4.Size = new System.Drawing.Size(95, 60);
            this.b4.TabIndex = 7;
            this.b4.TabStop = false;
            this.b4.Tag = "4";
            this.b4.Text = "4";
            this.b4.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.SystemColors.Control;
            this.b3.BorderColor = System.Drawing.Color.Black;
            this.b3.BorderWidth = 2;
            this.b3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b3.FillColorBottom = System.Drawing.Color.Blue;
            this.b3.FillColorTop = System.Drawing.Color.Blue;
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b3.ForeColor = System.Drawing.Color.White;
            this.b3.Image = null;
            this.b3.Location = new System.Drawing.Point(203, 128);
            this.b3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b3.Name = "b3";
            this.b3.RoundedCorners.BottomLeft = true;
            this.b3.RoundedCorners.BottomRight = true;
            this.b3.RoundedCorners.TopLeft = true;
            this.b3.RoundedCorners.TopRight = true;
            this.b3.RoundingDiameter = 15;
            this.b3.Size = new System.Drawing.Size(95, 60);
            this.b3.TabIndex = 8;
            this.b3.TabStop = false;
            this.b3.Tag = "3";
            this.b3.Text = "3";
            this.b3.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.SystemColors.Control;
            this.b2.BorderColor = System.Drawing.Color.Black;
            this.b2.BorderWidth = 2;
            this.b2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b2.FillColorBottom = System.Drawing.Color.Blue;
            this.b2.FillColorTop = System.Drawing.Color.Blue;
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b2.ForeColor = System.Drawing.Color.White;
            this.b2.Image = null;
            this.b2.Location = new System.Drawing.Point(102, 128);
            this.b2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b2.Name = "b2";
            this.b2.RoundedCorners.BottomLeft = true;
            this.b2.RoundedCorners.BottomRight = true;
            this.b2.RoundedCorners.TopLeft = true;
            this.b2.RoundedCorners.TopRight = true;
            this.b2.RoundingDiameter = 15;
            this.b2.Size = new System.Drawing.Size(95, 60);
            this.b2.TabIndex = 9;
            this.b2.TabStop = false;
            this.b2.Tag = "2";
            this.b2.Text = "2";
            this.b2.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.SystemColors.Control;
            this.b1.BorderColor = System.Drawing.Color.Black;
            this.b1.BorderWidth = 2;
            this.b1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b1.FillColorBottom = System.Drawing.Color.Blue;
            this.b1.FillColorTop = System.Drawing.Color.Blue;
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b1.ForeColor = System.Drawing.Color.White;
            this.b1.Image = null;
            this.b1.Location = new System.Drawing.Point(0, 128);
            this.b1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b1.Name = "b1";
            this.b1.RoundedCorners.BottomLeft = true;
            this.b1.RoundedCorners.BottomRight = true;
            this.b1.RoundedCorners.TopLeft = true;
            this.b1.RoundedCorners.TopRight = true;
            this.b1.RoundingDiameter = 15;
            this.b1.Size = new System.Drawing.Size(95, 60);
            this.b1.TabIndex = 10;
            this.b1.TabStop = false;
            this.b1.Tag = "1";
            this.b1.Text = "1";
            this.b1.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDot
            // 
            this.bDot.BackColor = System.Drawing.SystemColors.Control;
            this.bDot.BorderColor = System.Drawing.Color.Black;
            this.bDot.BorderWidth = 2;
            this.bDot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDot.FillColorBottom = System.Drawing.Color.Blue;
            this.bDot.FillColorTop = System.Drawing.Color.Blue;
            this.bDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDot.ForeColor = System.Drawing.Color.White;
            this.bDot.Image = null;
            this.bDot.Location = new System.Drawing.Point(203, 192);
            this.bDot.Margin = new System.Windows.Forms.Padding(2);
            this.bDot.Name = "bDot";
            this.bDot.RoundedCorners.BottomLeft = true;
            this.bDot.RoundedCorners.BottomRight = true;
            this.bDot.RoundedCorners.TopLeft = true;
            this.bDot.RoundedCorners.TopRight = true;
            this.bDot.RoundingDiameter = 15;
            this.bDot.Size = new System.Drawing.Size(95, 60);
            this.bDot.TabIndex = 11;
            this.bDot.TabStop = false;
            this.bDot.Tag = ",";
            this.bDot.Text = ",";
            this.bDot.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b0
            // 
            this.b0.BackColor = System.Drawing.SystemColors.Control;
            this.b0.BorderColor = System.Drawing.Color.Black;
            this.b0.BorderWidth = 2;
            this.b0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b0.FillColorBottom = System.Drawing.Color.Blue;
            this.b0.FillColorTop = System.Drawing.Color.Blue;
            this.b0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b0.ForeColor = System.Drawing.Color.White;
            this.b0.Image = null;
            this.b0.Location = new System.Drawing.Point(102, 192);
            this.b0.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b0.Name = "b0";
            this.b0.RoundedCorners.BottomLeft = true;
            this.b0.RoundedCorners.BottomRight = true;
            this.b0.RoundedCorners.TopLeft = true;
            this.b0.RoundedCorners.TopRight = true;
            this.b0.RoundingDiameter = 15;
            this.b0.Size = new System.Drawing.Size(95, 60);
            this.b0.TabIndex = 12;
            this.b0.TabStop = false;
            this.b0.Tag = "0";
            this.b0.Text = "0";
            this.b0.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bTimes
            // 
            this.bTimes.BackColor = System.Drawing.SystemColors.Control;
            this.bTimes.BorderColor = System.Drawing.Color.Black;
            this.bTimes.BorderWidth = 2;
            this.bTimes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bTimes.FillColorBottom = System.Drawing.Color.Blue;
            this.bTimes.FillColorTop = System.Drawing.Color.Blue;
            this.bTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bTimes.ForeColor = System.Drawing.Color.White;
            this.bTimes.Image = null;
            this.bTimes.Location = new System.Drawing.Point(0, 192);
            this.bTimes.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bTimes.Name = "bTimes";
            this.bTimes.RoundedCorners.BottomLeft = true;
            this.bTimes.RoundedCorners.BottomRight = true;
            this.bTimes.RoundedCorners.TopLeft = true;
            this.bTimes.RoundedCorners.TopRight = true;
            this.bTimes.RoundingDiameter = 15;
            this.bTimes.Size = new System.Drawing.Size(95, 60);
            this.bTimes.TabIndex = 13;
            this.bTimes.TabStop = false;
            this.bTimes.Tag = "";
            this.bTimes.Text = "X";
            this.bTimes.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDel
            // 
            this.bDel.BackColor = System.Drawing.SystemColors.Control;
            this.bDel.BorderColor = System.Drawing.Color.Black;
            this.bDel.BorderWidth = 2;
            this.bDel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDel.FillColorBottom = System.Drawing.Color.Red;
            this.bDel.FillColorTop = System.Drawing.Color.Red;
            this.bDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDel.ForeColor = System.Drawing.Color.White;
            this.bDel.Image = null;
            this.bDel.Location = new System.Drawing.Point(-1, 256);
            this.bDel.Margin = new System.Windows.Forms.Padding(2);
            this.bDel.Name = "bDel";
            this.bDel.RoundedCorners.BottomLeft = true;
            this.bDel.RoundedCorners.BottomRight = true;
            this.bDel.RoundedCorners.TopLeft = true;
            this.bDel.RoundedCorners.TopRight = true;
            this.bDel.RoundingDiameter = 15;
            this.bDel.Size = new System.Drawing.Size(95, 60);
            this.bDel.TabIndex = 14;
            this.bDel.TabStop = false;
            this.bDel.Text = "Smazat";
            this.bDel.Click += new System.EventHandler(this.ClearCodeClick);
            // 
            // bBackspace
            // 
            this.bBackspace.BackColor = System.Drawing.SystemColors.Control;
            this.bBackspace.BorderColor = System.Drawing.Color.Black;
            this.bBackspace.BorderWidth = 2;
            this.bBackspace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bBackspace.FillColorBottom = System.Drawing.Color.Red;
            this.bBackspace.FillColorTop = System.Drawing.Color.Red;
            this.bBackspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bBackspace.ForeColor = System.Drawing.Color.White;
            this.bBackspace.Image = ((System.Drawing.Image)(resources.GetObject("bBackspace.Image")));
            this.bBackspace.Location = new System.Drawing.Point(102, 256);
            this.bBackspace.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bBackspace.Name = "bBackspace";
            this.bBackspace.RoundedCorners.BottomLeft = true;
            this.bBackspace.RoundedCorners.BottomRight = true;
            this.bBackspace.RoundedCorners.TopLeft = true;
            this.bBackspace.RoundedCorners.TopRight = true;
            this.bBackspace.RoundingDiameter = 15;
            this.bBackspace.Size = new System.Drawing.Size(95, 60);
            this.bBackspace.TabIndex = 15;
            this.bBackspace.TabStop = false;
            this.bBackspace.Text = "0";
            this.bBackspace.Click += new System.EventHandler(this.DeleteLastClick);
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.bConfirm.BorderColor = System.Drawing.Color.Black;
            this.bConfirm.BorderWidth = 2;
            this.bConfirm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bConfirm.FillColorBottom = System.Drawing.Color.Blue;
            this.bConfirm.FillColorTop = System.Drawing.Color.Blue;
            this.bConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Image = null;
            this.bConfirm.Location = new System.Drawing.Point(203, 256);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.RoundedCorners.BottomLeft = true;
            this.bConfirm.RoundedCorners.BottomRight = true;
            this.bConfirm.RoundedCorners.TopLeft = true;
            this.bConfirm.RoundedCorners.TopRight = true;
            this.bConfirm.RoundingDiameter = 15;
            this.bConfirm.Size = new System.Drawing.Size(95, 60);
            this.bConfirm.TabIndex = 16;
            this.bConfirm.TabStop = false;
            this.bConfirm.Text = "Potvrdit";
            this.bConfirm.Click += new System.EventHandler(this.ConfirmProduct);
            // 
            // tbItem
            // 
            this.tbItem.BackColor = System.Drawing.Color.White;
            this.tbItem.Enabled = false;
            this.tbItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbItem.Location = new System.Drawing.Point(242, 18);
            this.tbItem.Name = "tbItem";
            this.tbItem.ReadOnly = true;
            this.tbItem.Size = new System.Drawing.Size(327, 38);
            this.tbItem.TabIndex = 50;
            this.tbItem.TabStop = false;
            // 
            // tbAmount
            // 
            this.tbAmount.BackColor = System.Drawing.Color.White;
            this.tbAmount.Enabled = false;
            this.tbAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbAmount.Location = new System.Drawing.Point(242, 62);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.ReadOnly = true;
            this.tbAmount.Size = new System.Drawing.Size(327, 38);
            this.tbAmount.TabIndex = 51;
            this.tbAmount.TabStop = false;
            this.tbAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbStorePrice
            // 
            this.tbStorePrice.BackColor = System.Drawing.Color.White;
            this.tbStorePrice.Enabled = false;
            this.tbStorePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbStorePrice.Location = new System.Drawing.Point(242, 106);
            this.tbStorePrice.Name = "tbStorePrice";
            this.tbStorePrice.ReadOnly = true;
            this.tbStorePrice.Size = new System.Drawing.Size(327, 38);
            this.tbStorePrice.TabIndex = 52;
            this.tbStorePrice.TabStop = false;
            this.tbStorePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 31);
            this.label2.TabIndex = 54;
            this.label2.Text = "Název:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 31);
            this.label1.TabIndex = 55;
            this.label1.Text = "Skladová cena:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 31);
            this.label3.TabIndex = 56;
            this.label3.Text = "Množství:";
            // 
            // bStore
            // 
            this.bStore.BorderColor = System.Drawing.Color.Black;
            this.bStore.BorderWidth = 2;
            this.bStore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bStore.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bStore.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bStore.Image = null;
            this.bStore.Location = new System.Drawing.Point(871, 673);
            this.bStore.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bStore.Name = "bStore";
            this.bStore.RoundedCorners.BottomLeft = true;
            this.bStore.RoundedCorners.BottomRight = true;
            this.bStore.RoundedCorners.TopLeft = true;
            this.bStore.RoundedCorners.TopRight = true;
            this.bStore.RoundingDiameter = 20;
            this.bStore.Size = new System.Drawing.Size(136, 79);
            this.bStore.TabIndex = 57;
            this.bStore.TabStop = false;
            this.bStore.Text = "Uložit";
            this.bStore.Click += new System.EventHandler(this.bStore_Click);
            // 
            // tbCancel
            // 
            this.tbCancel.BorderColor = System.Drawing.Color.Black;
            this.tbCancel.BorderWidth = 2;
            this.tbCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbCancel.FillColorBottom = System.Drawing.Color.Red;
            this.tbCancel.FillColorTop = System.Drawing.Color.Red;
            this.tbCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCancel.ForeColor = System.Drawing.Color.White;
            this.tbCancel.Image = null;
            this.tbCancel.Location = new System.Drawing.Point(18, 673);
            this.tbCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbCancel.Name = "tbCancel";
            this.tbCancel.RoundedCorners.BottomLeft = true;
            this.tbCancel.RoundedCorners.BottomRight = true;
            this.tbCancel.RoundedCorners.TopLeft = true;
            this.tbCancel.RoundedCorners.TopRight = true;
            this.tbCancel.RoundingDiameter = 20;
            this.tbCancel.Size = new System.Drawing.Size(136, 79);
            this.tbCancel.TabIndex = 58;
            this.tbCancel.TabStop = false;
            this.tbCancel.Text = "Storno";
            this.tbCancel.Click += new System.EventHandler(this.tbCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(224, 31);
            this.label4.TabIndex = 60;
            this.label4.Text = "Cena za položku:";
            // 
            // tbItemPrice
            // 
            this.tbItemPrice.BackColor = System.Drawing.Color.White;
            this.tbItemPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbItemPrice.Location = new System.Drawing.Point(242, 232);
            this.tbItemPrice.Name = "tbItemPrice";
            this.tbItemPrice.Size = new System.Drawing.Size(327, 38);
            this.tbItemPrice.TabIndex = 59;
            this.tbItemPrice.TabStop = false;
            this.tbItemPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbItemPrice.TextChanged += new System.EventHandler(this.tbItemPrice_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 31);
            this.label5.TabIndex = 62;
            this.label5.Text = "Cena po slevě:";
            // 
            // tbItemPriceDiscount
            // 
            this.tbItemPriceDiscount.BackColor = System.Drawing.Color.White;
            this.tbItemPriceDiscount.Enabled = false;
            this.tbItemPriceDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbItemPriceDiscount.Location = new System.Drawing.Point(242, 320);
            this.tbItemPriceDiscount.Name = "tbItemPriceDiscount";
            this.tbItemPriceDiscount.ReadOnly = true;
            this.tbItemPriceDiscount.Size = new System.Drawing.Size(327, 38);
            this.tbItemPriceDiscount.TabIndex = 61;
            this.tbItemPriceDiscount.TabStop = false;
            this.tbItemPriceDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(12, 492);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(180, 31);
            this.label6.TabIndex = 64;
            this.label6.Text = "Cena celkem:";
            // 
            // tbTotalPrice
            // 
            this.tbTotalPrice.BackColor = System.Drawing.Color.White;
            this.tbTotalPrice.Enabled = false;
            this.tbTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTotalPrice.Location = new System.Drawing.Point(242, 484);
            this.tbTotalPrice.Name = "tbTotalPrice";
            this.tbTotalPrice.ReadOnly = true;
            this.tbTotalPrice.Size = new System.Drawing.Size(327, 47);
            this.tbTotalPrice.TabIndex = 63;
            this.tbTotalPrice.TabStop = false;
            this.tbTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(12, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 31);
            this.label7.TabIndex = 66;
            this.label7.Text = "Sleva:";
            // 
            // tbDiscount
            // 
            this.tbDiscount.BackColor = System.Drawing.Color.White;
            this.tbDiscount.Enabled = false;
            this.tbDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbDiscount.Location = new System.Drawing.Point(242, 276);
            this.tbDiscount.Name = "tbDiscount";
            this.tbDiscount.ReadOnly = true;
            this.tbDiscount.Size = new System.Drawing.Size(327, 38);
            this.tbDiscount.TabIndex = 65;
            this.tbDiscount.TabStop = false;
            this.tbDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pShortCuts
            // 
            this.pShortCuts.Controls.Add(this.bItem16);
            this.pShortCuts.Controls.Add(this.bItem17);
            this.pShortCuts.Controls.Add(this.bItem18);
            this.pShortCuts.Controls.Add(this.bItem13);
            this.pShortCuts.Controls.Add(this.bItem14);
            this.pShortCuts.Controls.Add(this.bItem15);
            this.pShortCuts.Controls.Add(this.bItem10);
            this.pShortCuts.Controls.Add(this.bItem11);
            this.pShortCuts.Controls.Add(this.bItem12);
            this.pShortCuts.Controls.Add(this.bItem7);
            this.pShortCuts.Controls.Add(this.bItem8);
            this.pShortCuts.Controls.Add(this.bItem9);
            this.pShortCuts.Controls.Add(this.bItem4);
            this.pShortCuts.Controls.Add(this.bItem5);
            this.pShortCuts.Controls.Add(this.bItem6);
            this.pShortCuts.Controls.Add(this.bItem1);
            this.pShortCuts.Controls.Add(this.bItem2);
            this.pShortCuts.Controls.Add(this.bItem3);
            this.pShortCuts.Location = new System.Drawing.Point(603, 18);
            this.pShortCuts.Name = "pShortCuts";
            this.pShortCuts.Size = new System.Drawing.Size(404, 299);
            this.pShortCuts.TabIndex = 67;
            // 
            // bItem16
            // 
            this.bItem16.BorderColor = System.Drawing.Color.Black;
            this.bItem16.BorderWidth = 1;
            this.bItem16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem16.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.Image = null;
            this.bItem16.Location = new System.Drawing.Point(2, 205);
            this.bItem16.Name = "bItem16";
            this.bItem16.RoundedCorners.BottomLeft = true;
            this.bItem16.RoundedCorners.BottomRight = true;
            this.bItem16.RoundedCorners.TopLeft = true;
            this.bItem16.RoundedCorners.TopRight = true;
            this.bItem16.RoundingDiameter = 15;
            this.bItem16.Size = new System.Drawing.Size(130, 35);
            this.bItem16.TabIndex = 32;
            this.bItem16.TabStop = false;
            this.bItem16.Text = "Položka";
            this.bItem16.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem17
            // 
            this.bItem17.BorderColor = System.Drawing.Color.Black;
            this.bItem17.BorderWidth = 1;
            this.bItem17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem17.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.Image = null;
            this.bItem17.Location = new System.Drawing.Point(138, 205);
            this.bItem17.Name = "bItem17";
            this.bItem17.RoundedCorners.BottomLeft = true;
            this.bItem17.RoundedCorners.BottomRight = true;
            this.bItem17.RoundedCorners.TopLeft = true;
            this.bItem17.RoundedCorners.TopRight = true;
            this.bItem17.RoundingDiameter = 15;
            this.bItem17.Size = new System.Drawing.Size(130, 35);
            this.bItem17.TabIndex = 33;
            this.bItem17.TabStop = false;
            this.bItem17.Text = "Položka";
            this.bItem17.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem18
            // 
            this.bItem18.BorderColor = System.Drawing.Color.Black;
            this.bItem18.BorderWidth = 1;
            this.bItem18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem18.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.Image = null;
            this.bItem18.Location = new System.Drawing.Point(274, 205);
            this.bItem18.Name = "bItem18";
            this.bItem18.RoundedCorners.BottomLeft = true;
            this.bItem18.RoundedCorners.BottomRight = true;
            this.bItem18.RoundedCorners.TopLeft = true;
            this.bItem18.RoundedCorners.TopRight = true;
            this.bItem18.RoundingDiameter = 15;
            this.bItem18.Size = new System.Drawing.Size(130, 35);
            this.bItem18.TabIndex = 34;
            this.bItem18.TabStop = false;
            this.bItem18.Text = "Položka";
            this.bItem18.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem13
            // 
            this.bItem13.BorderColor = System.Drawing.Color.Black;
            this.bItem13.BorderWidth = 1;
            this.bItem13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem13.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.Image = null;
            this.bItem13.Location = new System.Drawing.Point(2, 164);
            this.bItem13.Name = "bItem13";
            this.bItem13.RoundedCorners.BottomLeft = true;
            this.bItem13.RoundedCorners.BottomRight = true;
            this.bItem13.RoundedCorners.TopLeft = true;
            this.bItem13.RoundedCorners.TopRight = true;
            this.bItem13.RoundingDiameter = 15;
            this.bItem13.Size = new System.Drawing.Size(130, 35);
            this.bItem13.TabIndex = 29;
            this.bItem13.TabStop = false;
            this.bItem13.Text = "Položka";
            this.bItem13.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem14
            // 
            this.bItem14.BorderColor = System.Drawing.Color.Black;
            this.bItem14.BorderWidth = 1;
            this.bItem14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem14.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.Image = null;
            this.bItem14.Location = new System.Drawing.Point(138, 164);
            this.bItem14.Name = "bItem14";
            this.bItem14.RoundedCorners.BottomLeft = true;
            this.bItem14.RoundedCorners.BottomRight = true;
            this.bItem14.RoundedCorners.TopLeft = true;
            this.bItem14.RoundedCorners.TopRight = true;
            this.bItem14.RoundingDiameter = 15;
            this.bItem14.Size = new System.Drawing.Size(130, 35);
            this.bItem14.TabIndex = 30;
            this.bItem14.TabStop = false;
            this.bItem14.Text = "Položka";
            this.bItem14.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem15
            // 
            this.bItem15.BorderColor = System.Drawing.Color.Black;
            this.bItem15.BorderWidth = 1;
            this.bItem15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem15.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.Image = null;
            this.bItem15.Location = new System.Drawing.Point(274, 164);
            this.bItem15.Name = "bItem15";
            this.bItem15.RoundedCorners.BottomLeft = true;
            this.bItem15.RoundedCorners.BottomRight = true;
            this.bItem15.RoundedCorners.TopLeft = true;
            this.bItem15.RoundedCorners.TopRight = true;
            this.bItem15.RoundingDiameter = 15;
            this.bItem15.Size = new System.Drawing.Size(130, 35);
            this.bItem15.TabIndex = 31;
            this.bItem15.TabStop = false;
            this.bItem15.Text = "Položka";
            this.bItem15.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem10
            // 
            this.bItem10.BorderColor = System.Drawing.Color.Black;
            this.bItem10.BorderWidth = 1;
            this.bItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem10.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.Image = null;
            this.bItem10.Location = new System.Drawing.Point(2, 123);
            this.bItem10.Name = "bItem10";
            this.bItem10.RoundedCorners.BottomLeft = true;
            this.bItem10.RoundedCorners.BottomRight = true;
            this.bItem10.RoundedCorners.TopLeft = true;
            this.bItem10.RoundedCorners.TopRight = true;
            this.bItem10.RoundingDiameter = 15;
            this.bItem10.Size = new System.Drawing.Size(130, 35);
            this.bItem10.TabIndex = 26;
            this.bItem10.TabStop = false;
            this.bItem10.Text = "Položka";
            this.bItem10.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem11
            // 
            this.bItem11.BorderColor = System.Drawing.Color.Black;
            this.bItem11.BorderWidth = 1;
            this.bItem11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem11.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.Image = null;
            this.bItem11.Location = new System.Drawing.Point(138, 123);
            this.bItem11.Name = "bItem11";
            this.bItem11.RoundedCorners.BottomLeft = true;
            this.bItem11.RoundedCorners.BottomRight = true;
            this.bItem11.RoundedCorners.TopLeft = true;
            this.bItem11.RoundedCorners.TopRight = true;
            this.bItem11.RoundingDiameter = 15;
            this.bItem11.Size = new System.Drawing.Size(130, 35);
            this.bItem11.TabIndex = 27;
            this.bItem11.TabStop = false;
            this.bItem11.Text = "Položka";
            this.bItem11.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem12
            // 
            this.bItem12.BorderColor = System.Drawing.Color.Black;
            this.bItem12.BorderWidth = 1;
            this.bItem12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem12.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.Image = null;
            this.bItem12.Location = new System.Drawing.Point(274, 123);
            this.bItem12.Name = "bItem12";
            this.bItem12.RoundedCorners.BottomLeft = true;
            this.bItem12.RoundedCorners.BottomRight = true;
            this.bItem12.RoundedCorners.TopLeft = true;
            this.bItem12.RoundedCorners.TopRight = true;
            this.bItem12.RoundingDiameter = 15;
            this.bItem12.Size = new System.Drawing.Size(130, 35);
            this.bItem12.TabIndex = 28;
            this.bItem12.TabStop = false;
            this.bItem12.Text = "Položka";
            this.bItem12.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem7
            // 
            this.bItem7.BorderColor = System.Drawing.Color.Black;
            this.bItem7.BorderWidth = 1;
            this.bItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem7.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.Image = null;
            this.bItem7.Location = new System.Drawing.Point(2, 82);
            this.bItem7.Name = "bItem7";
            this.bItem7.RoundedCorners.BottomLeft = true;
            this.bItem7.RoundedCorners.BottomRight = true;
            this.bItem7.RoundedCorners.TopLeft = true;
            this.bItem7.RoundedCorners.TopRight = true;
            this.bItem7.RoundingDiameter = 15;
            this.bItem7.Size = new System.Drawing.Size(130, 35);
            this.bItem7.TabIndex = 23;
            this.bItem7.TabStop = false;
            this.bItem7.Text = "Položka";
            this.bItem7.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem8
            // 
            this.bItem8.BorderColor = System.Drawing.Color.Black;
            this.bItem8.BorderWidth = 1;
            this.bItem8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem8.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.Image = null;
            this.bItem8.Location = new System.Drawing.Point(138, 82);
            this.bItem8.Name = "bItem8";
            this.bItem8.RoundedCorners.BottomLeft = true;
            this.bItem8.RoundedCorners.BottomRight = true;
            this.bItem8.RoundedCorners.TopLeft = true;
            this.bItem8.RoundedCorners.TopRight = true;
            this.bItem8.RoundingDiameter = 15;
            this.bItem8.Size = new System.Drawing.Size(130, 35);
            this.bItem8.TabIndex = 24;
            this.bItem8.TabStop = false;
            this.bItem8.Text = "Položka";
            this.bItem8.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem9
            // 
            this.bItem9.BorderColor = System.Drawing.Color.Black;
            this.bItem9.BorderWidth = 1;
            this.bItem9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem9.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.Image = null;
            this.bItem9.Location = new System.Drawing.Point(274, 82);
            this.bItem9.Name = "bItem9";
            this.bItem9.RoundedCorners.BottomLeft = true;
            this.bItem9.RoundedCorners.BottomRight = true;
            this.bItem9.RoundedCorners.TopLeft = true;
            this.bItem9.RoundedCorners.TopRight = true;
            this.bItem9.RoundingDiameter = 15;
            this.bItem9.Size = new System.Drawing.Size(130, 35);
            this.bItem9.TabIndex = 25;
            this.bItem9.TabStop = false;
            this.bItem9.Text = "Položka";
            this.bItem9.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem4
            // 
            this.bItem4.BorderColor = System.Drawing.Color.Black;
            this.bItem4.BorderWidth = 1;
            this.bItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem4.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.Image = null;
            this.bItem4.Location = new System.Drawing.Point(2, 41);
            this.bItem4.Name = "bItem4";
            this.bItem4.RoundedCorners.BottomLeft = true;
            this.bItem4.RoundedCorners.BottomRight = true;
            this.bItem4.RoundedCorners.TopLeft = true;
            this.bItem4.RoundedCorners.TopRight = true;
            this.bItem4.RoundingDiameter = 15;
            this.bItem4.Size = new System.Drawing.Size(130, 35);
            this.bItem4.TabIndex = 20;
            this.bItem4.TabStop = false;
            this.bItem4.Text = "Položka";
            this.bItem4.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem5
            // 
            this.bItem5.BorderColor = System.Drawing.Color.Black;
            this.bItem5.BorderWidth = 1;
            this.bItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem5.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.Image = null;
            this.bItem5.Location = new System.Drawing.Point(138, 41);
            this.bItem5.Name = "bItem5";
            this.bItem5.RoundedCorners.BottomLeft = true;
            this.bItem5.RoundedCorners.BottomRight = true;
            this.bItem5.RoundedCorners.TopLeft = true;
            this.bItem5.RoundedCorners.TopRight = true;
            this.bItem5.RoundingDiameter = 15;
            this.bItem5.Size = new System.Drawing.Size(130, 35);
            this.bItem5.TabIndex = 21;
            this.bItem5.TabStop = false;
            this.bItem5.Text = "Položka";
            this.bItem5.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem6
            // 
            this.bItem6.BorderColor = System.Drawing.Color.Black;
            this.bItem6.BorderWidth = 1;
            this.bItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem6.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.Image = null;
            this.bItem6.Location = new System.Drawing.Point(274, 41);
            this.bItem6.Name = "bItem6";
            this.bItem6.RoundedCorners.BottomLeft = true;
            this.bItem6.RoundedCorners.BottomRight = true;
            this.bItem6.RoundedCorners.TopLeft = true;
            this.bItem6.RoundedCorners.TopRight = true;
            this.bItem6.RoundingDiameter = 15;
            this.bItem6.Size = new System.Drawing.Size(130, 35);
            this.bItem6.TabIndex = 22;
            this.bItem6.TabStop = false;
            this.bItem6.Text = "Položka";
            this.bItem6.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem1
            // 
            this.bItem1.BorderColor = System.Drawing.Color.Black;
            this.bItem1.BorderWidth = 1;
            this.bItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem1.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.Image = null;
            this.bItem1.Location = new System.Drawing.Point(2, 0);
            this.bItem1.Name = "bItem1";
            this.bItem1.RoundedCorners.BottomLeft = true;
            this.bItem1.RoundedCorners.BottomRight = true;
            this.bItem1.RoundedCorners.TopLeft = true;
            this.bItem1.RoundedCorners.TopRight = true;
            this.bItem1.RoundingDiameter = 15;
            this.bItem1.Size = new System.Drawing.Size(130, 35);
            this.bItem1.TabIndex = 17;
            this.bItem1.TabStop = false;
            this.bItem1.Text = "Položka";
            this.bItem1.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem2
            // 
            this.bItem2.BorderColor = System.Drawing.Color.Black;
            this.bItem2.BorderWidth = 1;
            this.bItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem2.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.Image = null;
            this.bItem2.Location = new System.Drawing.Point(138, 0);
            this.bItem2.Name = "bItem2";
            this.bItem2.RoundedCorners.BottomLeft = true;
            this.bItem2.RoundedCorners.BottomRight = true;
            this.bItem2.RoundedCorners.TopLeft = true;
            this.bItem2.RoundedCorners.TopRight = true;
            this.bItem2.RoundingDiameter = 15;
            this.bItem2.Size = new System.Drawing.Size(130, 35);
            this.bItem2.TabIndex = 18;
            this.bItem2.TabStop = false;
            this.bItem2.Text = "Položka";
            this.bItem2.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // bItem3
            // 
            this.bItem3.BorderColor = System.Drawing.Color.Black;
            this.bItem3.BorderWidth = 1;
            this.bItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem3.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.Image = null;
            this.bItem3.Location = new System.Drawing.Point(274, 0);
            this.bItem3.Name = "bItem3";
            this.bItem3.RoundedCorners.BottomLeft = true;
            this.bItem3.RoundedCorners.BottomRight = true;
            this.bItem3.RoundedCorners.TopLeft = true;
            this.bItem3.RoundedCorners.TopRight = true;
            this.bItem3.RoundingDiameter = 15;
            this.bItem3.Size = new System.Drawing.Size(130, 35);
            this.bItem3.TabIndex = 19;
            this.bItem3.TabStop = false;
            this.bItem3.Text = "Položka";
            this.bItem3.Click += new System.EventHandler(this.bItem1_Click);
            // 
            // PriceChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.pShortCuts);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbDiscount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbTotalPrice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbItemPriceDiscount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbItemPrice);
            this.Controls.Add(this.tbCancel);
            this.Controls.Add(this.bStore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbStorePrice);
            this.Controls.Add(this.tbAmount);
            this.Controls.Add(this.tbItem);
            this.Controls.Add(this.pKeys);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "PriceChangeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.PriceChangeForm_Load);
            this.Shown += new System.EventHandler(this.PriceChangeForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.pKeys.ResumeLayout(false);
            this.pShortCuts.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.TouchButton b7;
        private Controls.TouchButton b9;
        private Controls.TouchButton b8;
        private Controls.TouchButton b6;
        private Controls.TouchButton b5;
        private Controls.TouchButton b4;
        private Controls.TouchButton b1;
        private Controls.TouchButton b2;
        private Controls.TouchButton b3;
        private Controls.TouchButton bTimes;
        private Controls.TouchButton b0;
        private Controls.TouchButton bDot;
        private Controls.TouchButton bConfirm;
        private Controls.TouchButton bBackspace;
        private Controls.TouchButton bDel;
        private BrightIdeasSoftware.HeaderFormatStyle headerFormatStyle1;
        private System.Windows.Forms.Panel pKeys;
        private System.Windows.Forms.TextBox tbItem;
        private System.Windows.Forms.TextBox tbAmount;
        private System.Windows.Forms.TextBox tbStorePrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Controls.TouchButton bStore;
        private Controls.TouchButton tbCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbItemPrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbItemPriceDiscount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTotalPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbDiscount;
        private System.Windows.Forms.Panel pShortCuts;
        private Controls.TouchButton bItem16;
        private Controls.TouchButton bItem17;
        private Controls.TouchButton bItem18;
        private Controls.TouchButton bItem13;
        private Controls.TouchButton bItem14;
        private Controls.TouchButton bItem15;
        private Controls.TouchButton bItem10;
        private Controls.TouchButton bItem11;
        private Controls.TouchButton bItem12;
        private Controls.TouchButton bItem7;
        private Controls.TouchButton bItem8;
        private Controls.TouchButton bItem9;
        private Controls.TouchButton bItem4;
        private Controls.TouchButton bItem5;
        private Controls.TouchButton bItem6;
        private Controls.TouchButton bItem1;
        private Controls.TouchButton bItem2;
        private Controls.TouchButton bItem3;

    }
}

