﻿namespace Transware.TouchPOS
{
    partial class UserSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pStoreCard = new System.Windows.Forms.Panel();
            this.lAdmin = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.tbExit = new Transware.Controls.TouchButton();
            this.bRight = new Transware.Controls.TouchButton();
            this.bLeft = new Transware.Controls.TouchButton();
            this.bCancel = new Transware.Controls.TouchButton();
            this.pStoreCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pStoreCard
            // 
            this.pStoreCard.BackgroundImage = global::Transware.TouchPOS.Properties.Resources.background2;
            this.pStoreCard.Controls.Add(this.lAdmin);
            this.pStoreCard.Controls.Add(this.pbLogo);
            this.pStoreCard.Location = new System.Drawing.Point(17, 12);
            this.pStoreCard.Name = "pStoreCard";
            this.pStoreCard.Size = new System.Drawing.Size(991, 658);
            this.pStoreCard.TabIndex = 0;
            // 
            // lAdmin
            // 
            this.lAdmin.AutoSize = true;
            this.lAdmin.BackColor = System.Drawing.Color.Transparent;
            this.lAdmin.Location = new System.Drawing.Point(969, 640);
            this.lAdmin.Name = "lAdmin";
            this.lAdmin.Size = new System.Drawing.Size(13, 13);
            this.lAdmin.TabIndex = 1;
            this.lAdmin.Text = "π";
            this.lAdmin.Click += new System.EventHandler(this.lAdmin_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.BackColor = System.Drawing.Color.Transparent;
            this.pbLogo.BackgroundImage = global::Transware.TouchPOS.Properties.Resources.logo2;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbLogo.Location = new System.Drawing.Point(16, 161);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(391, 336);
            this.pbLogo.TabIndex = 0;
            this.pbLogo.TabStop = false;
            // 
            // tbExit
            // 
            this.tbExit.BorderColor = System.Drawing.Color.Black;
            this.tbExit.BorderWidth = 2;
            this.tbExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbExit.FillColorBottom = System.Drawing.Color.Black;
            this.tbExit.FillColorTop = System.Drawing.Color.Black;
            this.tbExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbExit.ForeColor = System.Drawing.Color.White;
            this.tbExit.Image = null;
            this.tbExit.Location = new System.Drawing.Point(411, 698);
            this.tbExit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbExit.Name = "tbExit";
            this.tbExit.RoundedCorners.BottomLeft = true;
            this.tbExit.RoundedCorners.BottomRight = true;
            this.tbExit.RoundedCorners.TopLeft = true;
            this.tbExit.RoundedCorners.TopRight = true;
            this.tbExit.RoundingDiameter = 20;
            this.tbExit.Size = new System.Drawing.Size(203, 57);
            this.tbExit.TabIndex = 65;
            this.tbExit.TabStop = false;
            this.tbExit.Text = "VYPNOUT";
            this.tbExit.Click += new System.EventHandler(this.tbExit_Click);
            // 
            // bRight
            // 
            this.bRight.BorderColor = System.Drawing.Color.Black;
            this.bRight.BorderWidth = 1;
            this.bRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.bRight.Location = new System.Drawing.Point(951, 698);
            this.bRight.Name = "bRight";
            this.bRight.RoundedCorners.BottomLeft = true;
            this.bRight.RoundedCorners.BottomRight = true;
            this.bRight.RoundedCorners.TopLeft = true;
            this.bRight.RoundedCorners.TopRight = true;
            this.bRight.RoundingDiameter = 15;
            this.bRight.Size = new System.Drawing.Size(57, 57);
            this.bRight.TabIndex = 64;
            this.bRight.TabStop = false;
            this.bRight.Text = "Položka";
            this.bRight.Visible = false;
            this.bRight.Click += new System.EventHandler(this.bRight_Click);
            // 
            // bLeft
            // 
            this.bLeft.BorderColor = System.Drawing.Color.Black;
            this.bLeft.BorderWidth = 1;
            this.bLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.bLeft.Location = new System.Drawing.Point(888, 698);
            this.bLeft.Name = "bLeft";
            this.bLeft.RoundedCorners.BottomLeft = true;
            this.bLeft.RoundedCorners.BottomRight = true;
            this.bLeft.RoundedCorners.TopLeft = true;
            this.bLeft.RoundedCorners.TopRight = true;
            this.bLeft.RoundingDiameter = 15;
            this.bLeft.Size = new System.Drawing.Size(57, 57);
            this.bLeft.TabIndex = 63;
            this.bLeft.TabStop = false;
            this.bLeft.Text = "Položka";
            this.bLeft.Visible = false;
            this.bLeft.Click += new System.EventHandler(this.bLeft_Click);
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(17, 698);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 62;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Visible = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // UserSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.pStoreCard);
            this.Controls.Add(this.tbExit);
            this.Controls.Add(this.bRight);
            this.Controls.Add(this.bLeft);
            this.Controls.Add(this.bCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "UserSelectForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.pStoreCard.ResumeLayout(false);
            this.pStoreCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.TouchButton bCancel;
        private System.Windows.Forms.Panel pStoreCard;
        private Controls.TouchButton bRight;
        private Controls.TouchButton bLeft;
        private Controls.TouchButton tbExit;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lAdmin;


    }
}

