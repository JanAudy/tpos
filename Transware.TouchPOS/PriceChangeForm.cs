﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Transware.Data.DataTypes;
using Transware.Data.DataModel;
using Jadro.Data.DataTypes;
using Jadro.Data;
using Jadro.Data.DataModel;
using Microdata.Util;

namespace Transware.TouchPOS
{
    public partial class PriceChangeForm : Form
    {
        private BillItem billItem;
        private ConfigClass config = null;
        private List<TouchButton> shortcutsButtons = new List<TouchButton>();
        private decimal discount = 0;


        public PriceChangeForm(BillItem item)
        {
            InitializeComponent();

            billItem = item;

            shortcutsButtons.Add(bItem1);
            shortcutsButtons.Add(bItem2);
            shortcutsButtons.Add(bItem3);
            shortcutsButtons.Add(bItem4);
            shortcutsButtons.Add(bItem5);
            shortcutsButtons.Add(bItem6);
            shortcutsButtons.Add(bItem7);
            shortcutsButtons.Add(bItem8);
            shortcutsButtons.Add(bItem9);
            shortcutsButtons.Add(bItem10);
            shortcutsButtons.Add(bItem11);
            shortcutsButtons.Add(bItem12);
            shortcutsButtons.Add(bItem13);
            shortcutsButtons.Add(bItem14);
            shortcutsButtons.Add(bItem15);
            shortcutsButtons.Add(bItem16);
            shortcutsButtons.Add(bItem17);
            shortcutsButtons.Add(bItem18);

            config = ConfigClassModel.Instance.GetConfiguration();;

            discount = billItem.Discount;

            DrawShortcuts();

            DrawData();
        }

        private void DrawShortcuts()
        {
            List<IndividualDiscount> discounts = IndividualDiscountModel.Instance.GetList();
            discounts.Sort();

            shortcutsButtons[0].Text = "Bez slevy";
            shortcutsButtons[0].Tag = 0.0m;
            //shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
            shortcutsButtons[0].Enabled = true;

            for (int i = 1; i < shortcutsButtons.Count; i++)
            {
                if (i>=discounts.Count)
                {
                    shortcutsButtons[i].Text = "";
                    shortcutsButtons[i].Tag = null;
                    //shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = false;
                }
                else
                {
                    shortcutsButtons[i].Text = discounts[i].Discount.ToString("0.#####")+" %";
                    shortcutsButtons[i].Tag = discounts[i].Discount;
                    //shortcutsButtons[i].FillColorBottom = shortcutsButtons[i].FillColorTop = Color.FromArgb(p.ShortcutColor);
                    shortcutsButtons[i].Enabled = true;
                }
            }

        }

        private void DrawData()
        {
            tbItem.Text = billItem.Name;
            tbAmount.Text = billItem.Amount.ToString("0.####");
            PackageSummary sum = PackageSummaryModel.Instance.Get(billItem.Package.Value, Setting.Instance.Store);
            tbStorePrice.Text = sum.Price.ToString("0.00");
            tbItemPrice.Text = billItem.Price.ToString("0.00");
            tbDiscount.Text = billItem.Discount.ToString("0.#####") + " %";
            tbItemPriceDiscount.Text = billItem.PriceWithDiscount.ToString("0.00");
            tbTotalPrice.Text = billItem.TotalPriceWithDiscount.ToString("0.00");
        }

        private void NumberButtonClick(object sender, EventArgs e)
        {
            tbItemPrice.Text += (sender as TouchButton).Tag.ToString();
        }

        private void ClearCodeClick(object sender, EventArgs e)
        {
            tbItemPrice.Text = "";
        }

        private void DeleteLastClick(object sender, EventArgs e)
        {
            if (tbItemPrice.Text.Length > 0)
                tbItemPrice.Text = tbItemPrice.Text.Substring(0, tbItemPrice.Text.Length - 1);
        }

        private void ConfirmProduct(object sender, EventArgs e)
        {
            
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ConfirmProduct(sender, e);
            }
            else if (e.KeyCode == Keys.Escape || e.KeyCode==Keys.Delete)
            {
                ClearCodeClick(sender, e);
            }
            else if (e.KeyCode == Keys.Back)
            {
                DeleteLastClick(sender, e);
            }
            else if (e.KeyCode == Keys.F4 && e.Alt)
                return;
            else if (e.KeyCode==Keys.Left || e.KeyCode==Keys.Right || e.KeyCode==Keys.Up || e.KeyCode==Keys.Down)
            {
                return;
            }
            else
            {
                char c = KeyboardUtil.GetKey(e.KeyCode);
                if (c >= 32)
                {
                    tbItemPrice.Text += c;                    
                    e.Handled = true;
                }
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();

        }

        private void bStore_Click(object sender, EventArgs e)
        {
            decimal d;
            if (decimal.TryParse(tbItemPrice.Text, out d))
            {
                billItem.Price = d;
                billItem.Discount = discount;
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void PriceChangeForm_Load(object sender, EventArgs e)
        {
            tbItemPrice.SelectAll();
        }

        private void PriceChangeForm_Shown(object sender, EventArgs e)
        {
            tbItemPrice.SelectAll();
        }

        private void tbItemPrice_TextChanged(object sender, EventArgs e)
        {
            decimal d;
            if (!decimal.TryParse(tbItemPrice.Text, out d))
            {
                tbItemPriceDiscount.Text = "-- error --";
                tbTotalPrice.Text = "-- error --";
            }
            else
            {
                decimal discounted = d * (100.0m - discount) / 100.0m;
                tbItemPriceDiscount.Text = discounted.ToString("0.00");
                tbTotalPrice.Text = (billItem.Amount * discounted).ToString("0.00");
            }
            tbDiscount.Text = discount.ToString("0.#####") + " %";
        }

        private void bItem1_Click(object sender, EventArgs e)
        {
            discount = (decimal)(sender as TouchButton).Tag;
            tbItemPrice_TextChanged(sender, e);
        }
 

        
    }
}
