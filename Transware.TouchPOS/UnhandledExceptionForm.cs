﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;

namespace Transware.TouchPOS
{
    public partial class UnhandledExceptionForm : Form
    {
        private Exception exception;

        public UnhandledExceptionForm(Exception exception)
        {
            this.exception = exception;

            InitializeComponent();
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Opravdu ukončit aplikaci? Všechny neuložené změny budou ztraceny!", "Ukončení aplikace", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void bContinue_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.To.Add("jan.platos@microdata.cz");
                message.Subject = "Transware Error";
                message.From = new MailAddress("errors@microdata.cz");
                message.Body = exception.ToString();

                SmtpClient client = new SmtpClient("smtp.seznam.cz", 465);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("jan.platos@seznam.cz", "hanssoftprg");
                client.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Odeslání selhalo", "Chyba odeslání", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void bShowError_Click(object sender, EventArgs e)
        {
            TextForm form = new TextForm(exception.ToString());
            form.ShowDialog();
        }
    }
}
