﻿namespace Transware.TouchPOS
{
    partial class PaymentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle7 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle8 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle9 = new BrightIdeasSoftware.HeaderStateStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentForm));
            this.tbCustomer = new System.Windows.Forms.TextBox();
            this.lCustomer = new System.Windows.Forms.Label();
            this.headerFormatStyle1 = new BrightIdeasSoftware.HeaderFormatStyle();
            this.pItems = new System.Windows.Forms.Panel();
            this.gpPayments = new System.Windows.Forms.GroupBox();
            this.lvPayments = new BrightIdeasSoftware.ObjectListView();
            this.olvcName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcAmount = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tbRemains = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pKeys = new System.Windows.Forms.Panel();
            this.b7 = new Transware.Controls.TouchButton();
            this.b9 = new Transware.Controls.TouchButton();
            this.b8 = new Transware.Controls.TouchButton();
            this.b6 = new Transware.Controls.TouchButton();
            this.b5 = new Transware.Controls.TouchButton();
            this.b4 = new Transware.Controls.TouchButton();
            this.b3 = new Transware.Controls.TouchButton();
            this.b2 = new Transware.Controls.TouchButton();
            this.b1 = new Transware.Controls.TouchButton();
            this.bDot = new Transware.Controls.TouchButton();
            this.b0 = new Transware.Controls.TouchButton();
            this.bTimes = new Transware.Controls.TouchButton();
            this.bDel = new Transware.Controls.TouchButton();
            this.bBackspace = new Transware.Controls.TouchButton();
            this.bConfirm = new Transware.Controls.TouchButton();
            this.pPayment = new System.Windows.Forms.Panel();
            this.bSelect2 = new Transware.Controls.TouchButton();
            this.bSelect3 = new Transware.Controls.TouchButton();
            this.bSelect4 = new Transware.Controls.TouchButton();
            this.bSelect1 = new Transware.Controls.TouchButton();
            this.tbTotalWithDiscount = new System.Windows.Forms.TextBox();
            this.lTotalWithDiscount = new System.Windows.Forms.Label();
            this.tbDiscount = new System.Windows.Forms.TextBox();
            this.lDiscount = new System.Windows.Forms.Label();
            this.tbReturn = new System.Windows.Forms.TextBox();
            this.lError = new System.Windows.Forms.Label();
            this.bDiscount = new Transware.Controls.TouchButton();
            this.bCancel = new Transware.Controls.TouchButton();
            this.bSplitPayment = new Transware.Controls.TouchButton();
            this.tbPaid = new System.Windows.Forms.TextBox();
            this.lReturnLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bCard = new Transware.Controls.TouchButton();
            this.bCash = new Transware.Controls.TouchButton();
            this.bCloseRecord = new Transware.Controls.TouchButton();
            this.bClose = new Transware.Controls.TouchButton();
            this.bSelectCustomer = new Transware.Controls.TouchButton();
            this.bAddPayment = new Transware.Controls.TouchButton();
            this.bRight = new Transware.Controls.TouchButton();
            this.bLeft = new Transware.Controls.TouchButton();
            this.pItems.SuspendLayout();
            this.gpPayments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvPayments)).BeginInit();
            this.pKeys.SuspendLayout();
            this.pPayment.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbCustomer
            // 
            this.tbCustomer.BackColor = System.Drawing.Color.White;
            this.tbCustomer.Enabled = false;
            this.tbCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCustomer.Location = new System.Drawing.Point(159, 29);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.ReadOnly = true;
            this.tbCustomer.Size = new System.Drawing.Size(327, 38);
            this.tbCustomer.TabIndex = 33;
            this.tbCustomer.TabStop = false;
            // 
            // lCustomer
            // 
            this.lCustomer.AutoSize = true;
            this.lCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCustomer.Location = new System.Drawing.Point(12, 29);
            this.lCustomer.Name = "lCustomer";
            this.lCustomer.Size = new System.Drawing.Size(141, 31);
            this.lCustomer.TabIndex = 34;
            this.lCustomer.Text = "Odběratel:";
            // 
            // headerFormatStyle1
            // 
            headerStateStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Hot = headerStateStyle7;
            headerStateStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Normal = headerStateStyle8;
            headerStateStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Pressed = headerStateStyle9;
            // 
            // pItems
            // 
            this.pItems.Controls.Add(this.gpPayments);
            this.pItems.Controls.Add(this.tbRemains);
            this.pItems.Controls.Add(this.label4);
            this.pItems.Location = new System.Drawing.Point(603, 16);
            this.pItems.Name = "pItems";
            this.pItems.Size = new System.Drawing.Size(404, 299);
            this.pItems.TabIndex = 47;
            // 
            // gpPayments
            // 
            this.gpPayments.Controls.Add(this.lvPayments);
            this.gpPayments.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gpPayments.Location = new System.Drawing.Point(3, 3);
            this.gpPayments.Name = "gpPayments";
            this.gpPayments.Size = new System.Drawing.Size(398, 222);
            this.gpPayments.TabIndex = 71;
            this.gpPayments.TabStop = false;
            this.gpPayments.Text = "Platby";
            // 
            // lvPayments
            // 
            this.lvPayments.AllColumns.Add(this.olvcName);
            this.lvPayments.AllColumns.Add(this.olvcAmount);
            this.lvPayments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvcName,
            this.olvcAmount});
            this.lvPayments.Location = new System.Drawing.Point(6, 28);
            this.lvPayments.Name = "lvPayments";
            this.lvPayments.ShowGroups = false;
            this.lvPayments.Size = new System.Drawing.Size(384, 188);
            this.lvPayments.TabIndex = 0;
            this.lvPayments.UseCompatibleStateImageBehavior = false;
            this.lvPayments.View = System.Windows.Forms.View.Details;
            // 
            // olvcName
            // 
            this.olvcName.AspectName = "Name";
            this.olvcName.Text = "Platba";
            this.olvcName.Width = 230;
            // 
            // olvcAmount
            // 
            this.olvcAmount.AspectName = "Amount";
            this.olvcAmount.AspectToStringFormat = "{0:0.00}";
            this.olvcAmount.FillsFreeSpace = true;
            this.olvcAmount.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcAmount.Text = "Částka";
            this.olvcAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbRemains
            // 
            this.tbRemains.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbRemains.Location = new System.Drawing.Point(208, 231);
            this.tbRemains.Name = "tbRemains";
            this.tbRemains.ReadOnly = true;
            this.tbRemains.Size = new System.Drawing.Size(185, 62);
            this.tbRemains.TabIndex = 70;
            this.tbRemains.TabStop = false;
            this.tbRemains.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(15, 252);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 31);
            this.label4.TabIndex = 69;
            this.label4.Text = "Zbývá uhradit:";
            // 
            // pKeys
            // 
            this.pKeys.Controls.Add(this.b7);
            this.pKeys.Controls.Add(this.b9);
            this.pKeys.Controls.Add(this.b8);
            this.pKeys.Controls.Add(this.b6);
            this.pKeys.Controls.Add(this.b5);
            this.pKeys.Controls.Add(this.b4);
            this.pKeys.Controls.Add(this.b3);
            this.pKeys.Controls.Add(this.b2);
            this.pKeys.Controls.Add(this.b1);
            this.pKeys.Controls.Add(this.bDot);
            this.pKeys.Controls.Add(this.b0);
            this.pKeys.Controls.Add(this.bTimes);
            this.pKeys.Controls.Add(this.bDel);
            this.pKeys.Controls.Add(this.bBackspace);
            this.pKeys.Controls.Add(this.bConfirm);
            this.pKeys.Location = new System.Drawing.Point(709, 321);
            this.pKeys.Name = "pKeys";
            this.pKeys.Size = new System.Drawing.Size(298, 316);
            this.pKeys.TabIndex = 49;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.SystemColors.Control;
            this.b7.BorderColor = System.Drawing.Color.Black;
            this.b7.BorderWidth = 2;
            this.b7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b7.FillColorBottom = System.Drawing.Color.Blue;
            this.b7.FillColorTop = System.Drawing.Color.Blue;
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b7.ForeColor = System.Drawing.Color.White;
            this.b7.Image = null;
            this.b7.Location = new System.Drawing.Point(0, 0);
            this.b7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b7.Name = "b7";
            this.b7.RoundedCorners.BottomLeft = true;
            this.b7.RoundedCorners.BottomRight = true;
            this.b7.RoundedCorners.TopLeft = true;
            this.b7.RoundedCorners.TopRight = true;
            this.b7.RoundingDiameter = 15;
            this.b7.Size = new System.Drawing.Size(95, 60);
            this.b7.TabIndex = 0;
            this.b7.TabStop = false;
            this.b7.Tag = "7";
            this.b7.Text = "7";
            this.b7.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.SystemColors.Control;
            this.b9.BorderColor = System.Drawing.Color.Black;
            this.b9.BorderWidth = 2;
            this.b9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b9.FillColorBottom = System.Drawing.Color.Blue;
            this.b9.FillColorTop = System.Drawing.Color.Blue;
            this.b9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b9.ForeColor = System.Drawing.Color.White;
            this.b9.Image = null;
            this.b9.Location = new System.Drawing.Point(203, 0);
            this.b9.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b9.Name = "b9";
            this.b9.RoundedCorners.BottomLeft = true;
            this.b9.RoundedCorners.BottomRight = true;
            this.b9.RoundedCorners.TopLeft = true;
            this.b9.RoundedCorners.TopRight = true;
            this.b9.RoundingDiameter = 15;
            this.b9.Size = new System.Drawing.Size(95, 60);
            this.b9.TabIndex = 3;
            this.b9.TabStop = false;
            this.b9.Tag = "9";
            this.b9.Text = "9";
            this.b9.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.SystemColors.Control;
            this.b8.BorderColor = System.Drawing.Color.Black;
            this.b8.BorderWidth = 2;
            this.b8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b8.FillColorBottom = System.Drawing.Color.Blue;
            this.b8.FillColorTop = System.Drawing.Color.Blue;
            this.b8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b8.ForeColor = System.Drawing.Color.White;
            this.b8.Image = null;
            this.b8.Location = new System.Drawing.Point(102, 0);
            this.b8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b8.Name = "b8";
            this.b8.RoundedCorners.BottomLeft = true;
            this.b8.RoundedCorners.BottomRight = true;
            this.b8.RoundedCorners.TopLeft = true;
            this.b8.RoundedCorners.TopRight = true;
            this.b8.RoundingDiameter = 15;
            this.b8.Size = new System.Drawing.Size(95, 60);
            this.b8.TabIndex = 4;
            this.b8.TabStop = false;
            this.b8.Tag = "8";
            this.b8.Text = "8";
            this.b8.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.SystemColors.Control;
            this.b6.BorderColor = System.Drawing.Color.Black;
            this.b6.BorderWidth = 2;
            this.b6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b6.FillColorBottom = System.Drawing.Color.Blue;
            this.b6.FillColorTop = System.Drawing.Color.Blue;
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b6.ForeColor = System.Drawing.Color.White;
            this.b6.Image = null;
            this.b6.Location = new System.Drawing.Point(203, 64);
            this.b6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b6.Name = "b6";
            this.b6.RoundedCorners.BottomLeft = true;
            this.b6.RoundedCorners.BottomRight = true;
            this.b6.RoundedCorners.TopLeft = true;
            this.b6.RoundedCorners.TopRight = true;
            this.b6.RoundingDiameter = 15;
            this.b6.Size = new System.Drawing.Size(95, 60);
            this.b6.TabIndex = 5;
            this.b6.TabStop = false;
            this.b6.Tag = "6";
            this.b6.Text = "6";
            this.b6.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.SystemColors.Control;
            this.b5.BorderColor = System.Drawing.Color.Black;
            this.b5.BorderWidth = 2;
            this.b5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b5.FillColorBottom = System.Drawing.Color.Blue;
            this.b5.FillColorTop = System.Drawing.Color.Blue;
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b5.ForeColor = System.Drawing.Color.White;
            this.b5.Image = null;
            this.b5.Location = new System.Drawing.Point(102, 64);
            this.b5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b5.Name = "b5";
            this.b5.RoundedCorners.BottomLeft = true;
            this.b5.RoundedCorners.BottomRight = true;
            this.b5.RoundedCorners.TopLeft = true;
            this.b5.RoundedCorners.TopRight = true;
            this.b5.RoundingDiameter = 15;
            this.b5.Size = new System.Drawing.Size(95, 60);
            this.b5.TabIndex = 6;
            this.b5.TabStop = false;
            this.b5.Tag = "5";
            this.b5.Text = "5";
            this.b5.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.SystemColors.Control;
            this.b4.BorderColor = System.Drawing.Color.Black;
            this.b4.BorderWidth = 2;
            this.b4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b4.FillColorBottom = System.Drawing.Color.Blue;
            this.b4.FillColorTop = System.Drawing.Color.Blue;
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b4.ForeColor = System.Drawing.Color.White;
            this.b4.Image = null;
            this.b4.Location = new System.Drawing.Point(0, 64);
            this.b4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b4.Name = "b4";
            this.b4.RoundedCorners.BottomLeft = true;
            this.b4.RoundedCorners.BottomRight = true;
            this.b4.RoundedCorners.TopLeft = true;
            this.b4.RoundedCorners.TopRight = true;
            this.b4.RoundingDiameter = 15;
            this.b4.Size = new System.Drawing.Size(95, 60);
            this.b4.TabIndex = 7;
            this.b4.TabStop = false;
            this.b4.Tag = "4";
            this.b4.Text = "4";
            this.b4.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.SystemColors.Control;
            this.b3.BorderColor = System.Drawing.Color.Black;
            this.b3.BorderWidth = 2;
            this.b3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b3.FillColorBottom = System.Drawing.Color.Blue;
            this.b3.FillColorTop = System.Drawing.Color.Blue;
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b3.ForeColor = System.Drawing.Color.White;
            this.b3.Image = null;
            this.b3.Location = new System.Drawing.Point(203, 128);
            this.b3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b3.Name = "b3";
            this.b3.RoundedCorners.BottomLeft = true;
            this.b3.RoundedCorners.BottomRight = true;
            this.b3.RoundedCorners.TopLeft = true;
            this.b3.RoundedCorners.TopRight = true;
            this.b3.RoundingDiameter = 15;
            this.b3.Size = new System.Drawing.Size(95, 60);
            this.b3.TabIndex = 8;
            this.b3.TabStop = false;
            this.b3.Tag = "3";
            this.b3.Text = "3";
            this.b3.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.SystemColors.Control;
            this.b2.BorderColor = System.Drawing.Color.Black;
            this.b2.BorderWidth = 2;
            this.b2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b2.FillColorBottom = System.Drawing.Color.Blue;
            this.b2.FillColorTop = System.Drawing.Color.Blue;
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b2.ForeColor = System.Drawing.Color.White;
            this.b2.Image = null;
            this.b2.Location = new System.Drawing.Point(102, 128);
            this.b2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b2.Name = "b2";
            this.b2.RoundedCorners.BottomLeft = true;
            this.b2.RoundedCorners.BottomRight = true;
            this.b2.RoundedCorners.TopLeft = true;
            this.b2.RoundedCorners.TopRight = true;
            this.b2.RoundingDiameter = 15;
            this.b2.Size = new System.Drawing.Size(95, 60);
            this.b2.TabIndex = 9;
            this.b2.TabStop = false;
            this.b2.Tag = "2";
            this.b2.Text = "2";
            this.b2.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.SystemColors.Control;
            this.b1.BorderColor = System.Drawing.Color.Black;
            this.b1.BorderWidth = 2;
            this.b1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b1.FillColorBottom = System.Drawing.Color.Blue;
            this.b1.FillColorTop = System.Drawing.Color.Blue;
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b1.ForeColor = System.Drawing.Color.White;
            this.b1.Image = null;
            this.b1.Location = new System.Drawing.Point(0, 128);
            this.b1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b1.Name = "b1";
            this.b1.RoundedCorners.BottomLeft = true;
            this.b1.RoundedCorners.BottomRight = true;
            this.b1.RoundedCorners.TopLeft = true;
            this.b1.RoundedCorners.TopRight = true;
            this.b1.RoundingDiameter = 15;
            this.b1.Size = new System.Drawing.Size(95, 60);
            this.b1.TabIndex = 10;
            this.b1.TabStop = false;
            this.b1.Tag = "1";
            this.b1.Text = "1";
            this.b1.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDot
            // 
            this.bDot.BackColor = System.Drawing.SystemColors.Control;
            this.bDot.BorderColor = System.Drawing.Color.Black;
            this.bDot.BorderWidth = 2;
            this.bDot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDot.FillColorBottom = System.Drawing.Color.Blue;
            this.bDot.FillColorTop = System.Drawing.Color.Blue;
            this.bDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDot.ForeColor = System.Drawing.Color.White;
            this.bDot.Image = null;
            this.bDot.Location = new System.Drawing.Point(203, 192);
            this.bDot.Margin = new System.Windows.Forms.Padding(2);
            this.bDot.Name = "bDot";
            this.bDot.RoundedCorners.BottomLeft = true;
            this.bDot.RoundedCorners.BottomRight = true;
            this.bDot.RoundedCorners.TopLeft = true;
            this.bDot.RoundedCorners.TopRight = true;
            this.bDot.RoundingDiameter = 15;
            this.bDot.Size = new System.Drawing.Size(95, 60);
            this.bDot.TabIndex = 11;
            this.bDot.TabStop = false;
            this.bDot.Tag = ",";
            this.bDot.Text = ",";
            this.bDot.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b0
            // 
            this.b0.BackColor = System.Drawing.SystemColors.Control;
            this.b0.BorderColor = System.Drawing.Color.Black;
            this.b0.BorderWidth = 2;
            this.b0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b0.FillColorBottom = System.Drawing.Color.Blue;
            this.b0.FillColorTop = System.Drawing.Color.Blue;
            this.b0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b0.ForeColor = System.Drawing.Color.White;
            this.b0.Image = null;
            this.b0.Location = new System.Drawing.Point(102, 192);
            this.b0.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b0.Name = "b0";
            this.b0.RoundedCorners.BottomLeft = true;
            this.b0.RoundedCorners.BottomRight = true;
            this.b0.RoundedCorners.TopLeft = true;
            this.b0.RoundedCorners.TopRight = true;
            this.b0.RoundingDiameter = 15;
            this.b0.Size = new System.Drawing.Size(95, 60);
            this.b0.TabIndex = 12;
            this.b0.TabStop = false;
            this.b0.Tag = "0";
            this.b0.Text = "0";
            this.b0.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bTimes
            // 
            this.bTimes.BackColor = System.Drawing.SystemColors.Control;
            this.bTimes.BorderColor = System.Drawing.Color.Black;
            this.bTimes.BorderWidth = 2;
            this.bTimes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bTimes.FillColorBottom = System.Drawing.Color.Blue;
            this.bTimes.FillColorTop = System.Drawing.Color.Blue;
            this.bTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bTimes.ForeColor = System.Drawing.Color.White;
            this.bTimes.Image = null;
            this.bTimes.Location = new System.Drawing.Point(0, 192);
            this.bTimes.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bTimes.Name = "bTimes";
            this.bTimes.RoundedCorners.BottomLeft = true;
            this.bTimes.RoundedCorners.BottomRight = true;
            this.bTimes.RoundedCorners.TopLeft = true;
            this.bTimes.RoundedCorners.TopRight = true;
            this.bTimes.RoundingDiameter = 15;
            this.bTimes.Size = new System.Drawing.Size(95, 60);
            this.bTimes.TabIndex = 13;
            this.bTimes.TabStop = false;
            this.bTimes.Text = "X";
            // 
            // bDel
            // 
            this.bDel.BackColor = System.Drawing.SystemColors.Control;
            this.bDel.BorderColor = System.Drawing.Color.Black;
            this.bDel.BorderWidth = 2;
            this.bDel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDel.FillColorBottom = System.Drawing.Color.Red;
            this.bDel.FillColorTop = System.Drawing.Color.Red;
            this.bDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDel.ForeColor = System.Drawing.Color.White;
            this.bDel.Image = null;
            this.bDel.Location = new System.Drawing.Point(-1, 256);
            this.bDel.Margin = new System.Windows.Forms.Padding(2);
            this.bDel.Name = "bDel";
            this.bDel.RoundedCorners.BottomLeft = true;
            this.bDel.RoundedCorners.BottomRight = true;
            this.bDel.RoundedCorners.TopLeft = true;
            this.bDel.RoundedCorners.TopRight = true;
            this.bDel.RoundingDiameter = 15;
            this.bDel.Size = new System.Drawing.Size(95, 60);
            this.bDel.TabIndex = 14;
            this.bDel.TabStop = false;
            this.bDel.Text = "Smazat";
            this.bDel.Click += new System.EventHandler(this.ClearCodeClick);
            // 
            // bBackspace
            // 
            this.bBackspace.BackColor = System.Drawing.SystemColors.Control;
            this.bBackspace.BorderColor = System.Drawing.Color.Black;
            this.bBackspace.BorderWidth = 2;
            this.bBackspace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bBackspace.FillColorBottom = System.Drawing.Color.Red;
            this.bBackspace.FillColorTop = System.Drawing.Color.Red;
            this.bBackspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bBackspace.ForeColor = System.Drawing.Color.White;
            this.bBackspace.Image = ((System.Drawing.Image)(resources.GetObject("bBackspace.Image")));
            this.bBackspace.Location = new System.Drawing.Point(102, 256);
            this.bBackspace.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bBackspace.Name = "bBackspace";
            this.bBackspace.RoundedCorners.BottomLeft = true;
            this.bBackspace.RoundedCorners.BottomRight = true;
            this.bBackspace.RoundedCorners.TopLeft = true;
            this.bBackspace.RoundedCorners.TopRight = true;
            this.bBackspace.RoundingDiameter = 15;
            this.bBackspace.Size = new System.Drawing.Size(95, 60);
            this.bBackspace.TabIndex = 15;
            this.bBackspace.TabStop = false;
            this.bBackspace.Text = "0";
            this.bBackspace.Click += new System.EventHandler(this.DeleteLastClick);
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.bConfirm.BorderColor = System.Drawing.Color.Black;
            this.bConfirm.BorderWidth = 2;
            this.bConfirm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bConfirm.FillColorBottom = System.Drawing.Color.Blue;
            this.bConfirm.FillColorTop = System.Drawing.Color.Blue;
            this.bConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Image = null;
            this.bConfirm.Location = new System.Drawing.Point(203, 256);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.RoundedCorners.BottomLeft = true;
            this.bConfirm.RoundedCorners.BottomRight = true;
            this.bConfirm.RoundedCorners.TopLeft = true;
            this.bConfirm.RoundedCorners.TopRight = true;
            this.bConfirm.RoundingDiameter = 15;
            this.bConfirm.Size = new System.Drawing.Size(95, 60);
            this.bConfirm.TabIndex = 16;
            this.bConfirm.TabStop = false;
            this.bConfirm.Text = "Potvrdit";
            this.bConfirm.Click += new System.EventHandler(this.ConfirmProduct);
            // 
            // pPayment
            // 
            this.pPayment.Controls.Add(this.bRight);
            this.pPayment.Controls.Add(this.bLeft);
            this.pPayment.Controls.Add(this.bSelect2);
            this.pPayment.Controls.Add(this.bSelect3);
            this.pPayment.Controls.Add(this.bSelect4);
            this.pPayment.Controls.Add(this.bSelect1);
            this.pPayment.Controls.Add(this.tbTotalWithDiscount);
            this.pPayment.Controls.Add(this.lTotalWithDiscount);
            this.pPayment.Controls.Add(this.tbDiscount);
            this.pPayment.Controls.Add(this.lDiscount);
            this.pPayment.Controls.Add(this.tbReturn);
            this.pPayment.Controls.Add(this.lError);
            this.pPayment.Controls.Add(this.bDiscount);
            this.pPayment.Controls.Add(this.bCancel);
            this.pPayment.Controls.Add(this.bSplitPayment);
            this.pPayment.Controls.Add(this.tbPaid);
            this.pPayment.Controls.Add(this.lReturnLabel);
            this.pPayment.Controls.Add(this.label2);
            this.pPayment.Controls.Add(this.tbTotal);
            this.pPayment.Controls.Add(this.label3);
            this.pPayment.Controls.Add(this.bCard);
            this.pPayment.Controls.Add(this.bCash);
            this.pPayment.Controls.Add(this.bCloseRecord);
            this.pPayment.Controls.Add(this.bClose);
            this.pPayment.Location = new System.Drawing.Point(12, 80);
            this.pPayment.Name = "pPayment";
            this.pPayment.Size = new System.Drawing.Size(580, 565);
            this.pPayment.TabIndex = 50;
            // 
            // bSelect2
            // 
            this.bSelect2.BorderColor = System.Drawing.Color.Black;
            this.bSelect2.BorderWidth = 2;
            this.bSelect2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelect2.FillColorBottom = System.Drawing.Color.Blue;
            this.bSelect2.FillColorTop = System.Drawing.Color.Blue;
            this.bSelect2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelect2.Image = null;
            this.bSelect2.Location = new System.Drawing.Point(441, 158);
            this.bSelect2.Margin = new System.Windows.Forms.Padding(5);
            this.bSelect2.Name = "bSelect2";
            this.bSelect2.RoundedCorners.BottomLeft = true;
            this.bSelect2.RoundedCorners.BottomRight = true;
            this.bSelect2.RoundedCorners.TopLeft = true;
            this.bSelect2.RoundedCorners.TopRight = true;
            this.bSelect2.RoundingDiameter = 20;
            this.bSelect2.Size = new System.Drawing.Size(135, 62);
            this.bSelect2.TabIndex = 86;
            this.bSelect2.Tag = "1";
            this.bSelect2.Text = "Hotovost";
            this.bSelect2.Click += new System.EventHandler(this.bSelect_Click);
            // 
            // bSelect3
            // 
            this.bSelect3.BorderColor = System.Drawing.Color.Black;
            this.bSelect3.BorderWidth = 2;
            this.bSelect3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelect3.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect3.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelect3.Image = null;
            this.bSelect3.Location = new System.Drawing.Point(441, 226);
            this.bSelect3.Margin = new System.Windows.Forms.Padding(5);
            this.bSelect3.Name = "bSelect3";
            this.bSelect3.RoundedCorners.BottomLeft = true;
            this.bSelect3.RoundedCorners.BottomRight = true;
            this.bSelect3.RoundedCorners.TopLeft = true;
            this.bSelect3.RoundedCorners.TopRight = true;
            this.bSelect3.RoundingDiameter = 20;
            this.bSelect3.Size = new System.Drawing.Size(135, 62);
            this.bSelect3.TabIndex = 85;
            this.bSelect3.Tag = "2";
            this.bSelect3.Text = "Hotovost";
            this.bSelect3.Click += new System.EventHandler(this.bSelect_Click);
            // 
            // bSelect4
            // 
            this.bSelect4.BorderColor = System.Drawing.Color.Black;
            this.bSelect4.BorderWidth = 2;
            this.bSelect4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelect4.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect4.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelect4.Image = null;
            this.bSelect4.Location = new System.Drawing.Point(439, 292);
            this.bSelect4.Margin = new System.Windows.Forms.Padding(5);
            this.bSelect4.Name = "bSelect4";
            this.bSelect4.RoundedCorners.BottomLeft = true;
            this.bSelect4.RoundedCorners.BottomRight = true;
            this.bSelect4.RoundedCorners.TopLeft = true;
            this.bSelect4.RoundedCorners.TopRight = true;
            this.bSelect4.RoundingDiameter = 20;
            this.bSelect4.Size = new System.Drawing.Size(135, 62);
            this.bSelect4.TabIndex = 84;
            this.bSelect4.Tag = "3";
            this.bSelect4.Text = "Hotovost";
            this.bSelect4.Click += new System.EventHandler(this.bSelect_Click);
            // 
            // bSelect1
            // 
            this.bSelect1.BorderColor = System.Drawing.Color.Black;
            this.bSelect1.BorderWidth = 2;
            this.bSelect1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelect1.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect1.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bSelect1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelect1.Image = null;
            this.bSelect1.Location = new System.Drawing.Point(441, 90);
            this.bSelect1.Margin = new System.Windows.Forms.Padding(5);
            this.bSelect1.Name = "bSelect1";
            this.bSelect1.RoundedCorners.BottomLeft = true;
            this.bSelect1.RoundedCorners.BottomRight = true;
            this.bSelect1.RoundedCorners.TopLeft = true;
            this.bSelect1.RoundedCorners.TopRight = true;
            this.bSelect1.RoundingDiameter = 20;
            this.bSelect1.Size = new System.Drawing.Size(135, 62);
            this.bSelect1.TabIndex = 83;
            this.bSelect1.Tag = "0";
            this.bSelect1.Text = "Hotovost";
            this.bSelect1.Click += new System.EventHandler(this.bSelect_Click);
            // 
            // tbTotalWithDiscount
            // 
            this.tbTotalWithDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTotalWithDiscount.Location = new System.Drawing.Point(246, 226);
            this.tbTotalWithDiscount.Name = "tbTotalWithDiscount";
            this.tbTotalWithDiscount.ReadOnly = true;
            this.tbTotalWithDiscount.Size = new System.Drawing.Size(185, 62);
            this.tbTotalWithDiscount.TabIndex = 82;
            this.tbTotalWithDiscount.TabStop = false;
            this.tbTotalWithDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lTotalWithDiscount
            // 
            this.lTotalWithDiscount.AutoSize = true;
            this.lTotalWithDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTotalWithDiscount.Location = new System.Drawing.Point(6, 246);
            this.lTotalWithDiscount.Name = "lTotalWithDiscount";
            this.lTotalWithDiscount.Size = new System.Drawing.Size(222, 31);
            this.lTotalWithDiscount.TabIndex = 81;
            this.lTotalWithDiscount.Text = "Celkem po slevě:";
            // 
            // tbDiscount
            // 
            this.tbDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbDiscount.Location = new System.Drawing.Point(246, 158);
            this.tbDiscount.Name = "tbDiscount";
            this.tbDiscount.ReadOnly = true;
            this.tbDiscount.Size = new System.Drawing.Size(185, 62);
            this.tbDiscount.TabIndex = 80;
            this.tbDiscount.TabStop = false;
            this.tbDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lDiscount
            // 
            this.lDiscount.AutoSize = true;
            this.lDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lDiscount.Location = new System.Drawing.Point(6, 178);
            this.lDiscount.Name = "lDiscount";
            this.lDiscount.Size = new System.Drawing.Size(90, 31);
            this.lDiscount.TabIndex = 79;
            this.lDiscount.Text = "Sleva:";
            // 
            // tbReturn
            // 
            this.tbReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReturn.ForeColor = System.Drawing.Color.Red;
            this.tbReturn.Location = new System.Drawing.Point(246, 360);
            this.tbReturn.Name = "tbReturn";
            this.tbReturn.Size = new System.Drawing.Size(185, 62);
            this.tbReturn.TabIndex = 78;
            this.tbReturn.TabStop = false;
            this.tbReturn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lError
            // 
            this.lError.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lError.ForeColor = System.Drawing.Color.Red;
            this.lError.Location = new System.Drawing.Point(6, 425);
            this.lError.Name = "lError";
            this.lError.Size = new System.Drawing.Size(570, 53);
            this.lError.TabIndex = 76;
            this.lError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bDiscount
            // 
            this.bDiscount.BorderColor = System.Drawing.Color.Black;
            this.bDiscount.BorderWidth = 2;
            this.bDiscount.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDiscount.FillColorBottom = System.Drawing.SystemColors.Control;
            this.bDiscount.FillColorTop = System.Drawing.SystemColors.Control;
            this.bDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDiscount.ForeColor = System.Drawing.Color.Black;
            this.bDiscount.Image = null;
            this.bDiscount.Location = new System.Drawing.Point(441, 5);
            this.bDiscount.Margin = new System.Windows.Forms.Padding(5);
            this.bDiscount.Name = "bDiscount";
            this.bDiscount.RoundedCorners.BottomLeft = true;
            this.bDiscount.RoundedCorners.BottomRight = true;
            this.bDiscount.RoundedCorners.TopLeft = true;
            this.bDiscount.RoundedCorners.TopRight = true;
            this.bDiscount.RoundingDiameter = 20;
            this.bDiscount.Size = new System.Drawing.Size(135, 77);
            this.bDiscount.TabIndex = 75;
            this.bDiscount.Text = "Sleva";
            this.bDiscount.Click += new System.EventHandler(this.bOther1_Click);
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(6, 500);
            this.bCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(203, 57);
            this.bCancel.TabIndex = 61;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Zpět";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bSplitPayment
            // 
            this.bSplitPayment.BorderColor = System.Drawing.Color.Black;
            this.bSplitPayment.BorderWidth = 2;
            this.bSplitPayment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSplitPayment.FillColorBottom = System.Drawing.SystemColors.Control;
            this.bSplitPayment.FillColorTop = System.Drawing.SystemColors.Control;
            this.bSplitPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSplitPayment.ForeColor = System.Drawing.Color.Black;
            this.bSplitPayment.Image = null;
            this.bSplitPayment.Location = new System.Drawing.Point(296, 5);
            this.bSplitPayment.Margin = new System.Windows.Forms.Padding(5);
            this.bSplitPayment.Name = "bSplitPayment";
            this.bSplitPayment.RoundedCorners.BottomLeft = true;
            this.bSplitPayment.RoundedCorners.BottomRight = true;
            this.bSplitPayment.RoundedCorners.TopLeft = true;
            this.bSplitPayment.RoundedCorners.TopRight = true;
            this.bSplitPayment.RoundingDiameter = 20;
            this.bSplitPayment.Size = new System.Drawing.Size(135, 77);
            this.bSplitPayment.TabIndex = 74;
            this.bSplitPayment.Text = "Rozdělit platbu";
            this.bSplitPayment.Click += new System.EventHandler(this.bOther_Click);
            // 
            // tbPaid
            // 
            this.tbPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPaid.Location = new System.Drawing.Point(246, 292);
            this.tbPaid.Name = "tbPaid";
            this.tbPaid.ReadOnly = true;
            this.tbPaid.Size = new System.Drawing.Size(185, 62);
            this.tbPaid.TabIndex = 65;
            this.tbPaid.TabStop = false;
            this.tbPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lReturnLabel
            // 
            this.lReturnLabel.AutoSize = true;
            this.lReturnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReturnLabel.Location = new System.Drawing.Point(6, 380);
            this.lReturnLabel.Name = "lReturnLabel";
            this.lReturnLabel.Size = new System.Drawing.Size(86, 31);
            this.lReturnLabel.TabIndex = 71;
            this.lReturnLabel.Text = "Vrátit:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 312);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 31);
            this.label2.TabIndex = 64;
            this.label2.Text = "Zaplaceno:";
            // 
            // tbTotal
            // 
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTotal.Location = new System.Drawing.Point(246, 90);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(185, 62);
            this.tbTotal.TabIndex = 63;
            this.tbTotal.TabStop = false;
            this.tbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 31);
            this.label3.TabIndex = 62;
            this.label3.Text = "Celkem k úhradě:";
            // 
            // bCard
            // 
            this.bCard.BorderColor = System.Drawing.Color.Black;
            this.bCard.BorderWidth = 2;
            this.bCard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCard.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.bCard.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.bCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCard.ForeColor = System.Drawing.Color.Black;
            this.bCard.Image = null;
            this.bCard.Location = new System.Drawing.Point(151, 5);
            this.bCard.Margin = new System.Windows.Forms.Padding(5);
            this.bCard.Name = "bCard";
            this.bCard.RoundedCorners.BottomLeft = true;
            this.bCard.RoundedCorners.BottomRight = true;
            this.bCard.RoundedCorners.TopLeft = true;
            this.bCard.RoundedCorners.TopRight = true;
            this.bCard.RoundingDiameter = 20;
            this.bCard.Size = new System.Drawing.Size(135, 77);
            this.bCard.TabIndex = 59;
            this.bCard.Text = "Kartou";
            this.bCard.Click += new System.EventHandler(this.bCard_Click);
            // 
            // bCash
            // 
            this.bCash.BorderColor = System.Drawing.Color.Black;
            this.bCash.BorderWidth = 2;
            this.bCash.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCash.FillColorBottom = System.Drawing.Color.Blue;
            this.bCash.FillColorTop = System.Drawing.Color.Blue;
            this.bCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCash.Image = null;
            this.bCash.Location = new System.Drawing.Point(6, 5);
            this.bCash.Margin = new System.Windows.Forms.Padding(5);
            this.bCash.Name = "bCash";
            this.bCash.RoundedCorners.BottomLeft = true;
            this.bCash.RoundedCorners.BottomRight = true;
            this.bCash.RoundedCorners.TopLeft = true;
            this.bCash.RoundedCorners.TopRight = true;
            this.bCash.RoundingDiameter = 20;
            this.bCash.Size = new System.Drawing.Size(135, 77);
            this.bCash.TabIndex = 58;
            this.bCash.Text = "Hotovost";
            this.bCash.Click += new System.EventHandler(this.bCash_Click);
            // 
            // bCloseRecord
            // 
            this.bCloseRecord.BorderColor = System.Drawing.Color.Black;
            this.bCloseRecord.BorderWidth = 2;
            this.bCloseRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCloseRecord.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bCloseRecord.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bCloseRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCloseRecord.Image = null;
            this.bCloseRecord.Location = new System.Drawing.Point(373, 497);
            this.bCloseRecord.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bCloseRecord.Name = "bCloseRecord";
            this.bCloseRecord.RoundedCorners.BottomLeft = true;
            this.bCloseRecord.RoundedCorners.BottomRight = true;
            this.bCloseRecord.RoundedCorners.TopLeft = true;
            this.bCloseRecord.RoundedCorners.TopRight = true;
            this.bCloseRecord.RoundingDiameter = 20;
            this.bCloseRecord.Size = new System.Drawing.Size(203, 57);
            this.bCloseRecord.TabIndex = 60;
            this.bCloseRecord.TabStop = false;
            this.bCloseRecord.Text = "Uzavřít doklad";
            this.bCloseRecord.Click += new System.EventHandler(this.bCloseRecord_Click);
            // 
            // bClose
            // 
            this.bClose.BorderColor = System.Drawing.Color.Black;
            this.bClose.BorderWidth = 2;
            this.bClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bClose.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bClose.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bClose.Image = null;
            this.bClose.Location = new System.Drawing.Point(373, 497);
            this.bClose.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bClose.Name = "bClose";
            this.bClose.RoundedCorners.BottomLeft = true;
            this.bClose.RoundedCorners.BottomRight = true;
            this.bClose.RoundedCorners.TopLeft = true;
            this.bClose.RoundedCorners.TopRight = true;
            this.bClose.RoundingDiameter = 20;
            this.bClose.Size = new System.Drawing.Size(203, 57);
            this.bClose.TabIndex = 66;
            this.bClose.TabStop = false;
            this.bClose.Text = "OK";
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // bSelectCustomer
            // 
            this.bSelectCustomer.BorderColor = System.Drawing.Color.Black;
            this.bSelectCustomer.BorderWidth = 2;
            this.bSelectCustomer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelectCustomer.FillColorBottom = System.Drawing.Color.Magenta;
            this.bSelectCustomer.FillColorTop = System.Drawing.Color.Magenta;
            this.bSelectCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelectCustomer.ForeColor = System.Drawing.Color.Black;
            this.bSelectCustomer.Image = null;
            this.bSelectCustomer.Location = new System.Drawing.Point(497, 16);
            this.bSelectCustomer.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bSelectCustomer.Name = "bSelectCustomer";
            this.bSelectCustomer.RoundedCorners.BottomLeft = true;
            this.bSelectCustomer.RoundedCorners.BottomRight = true;
            this.bSelectCustomer.RoundedCorners.TopLeft = true;
            this.bSelectCustomer.RoundedCorners.TopRight = true;
            this.bSelectCustomer.RoundingDiameter = 20;
            this.bSelectCustomer.Size = new System.Drawing.Size(95, 60);
            this.bSelectCustomer.TabIndex = 35;
            this.bSelectCustomer.TabStop = false;
            this.bSelectCustomer.Text = "Vybrat odběratele";
            this.bSelectCustomer.Click += new System.EventHandler(this.bSelectCustomer_Click);
            // 
            // bAddPayment
            // 
            this.bAddPayment.BackColor = System.Drawing.SystemColors.Control;
            this.bAddPayment.BorderColor = System.Drawing.Color.Black;
            this.bAddPayment.BorderWidth = 2;
            this.bAddPayment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bAddPayment.FillColorBottom = System.Drawing.Color.Blue;
            this.bAddPayment.FillColorTop = System.Drawing.Color.Blue;
            this.bAddPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bAddPayment.ForeColor = System.Drawing.Color.White;
            this.bAddPayment.Image = null;
            this.bAddPayment.Location = new System.Drawing.Point(209, 728);
            this.bAddPayment.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bAddPayment.Name = "bAddPayment";
            this.bAddPayment.RoundedCorners.BottomLeft = true;
            this.bAddPayment.RoundedCorners.BottomRight = true;
            this.bAddPayment.RoundedCorners.TopLeft = true;
            this.bAddPayment.RoundedCorners.TopRight = true;
            this.bAddPayment.RoundingDiameter = 15;
            this.bAddPayment.Size = new System.Drawing.Size(185, 42);
            this.bAddPayment.TabIndex = 73;
            this.bAddPayment.Tag = "7";
            this.bAddPayment.Text = "Přidat platbu";
            this.bAddPayment.Click += new System.EventHandler(this.bAddPayment_Click);
            // 
            // bRight
            // 
            this.bRight.BorderColor = System.Drawing.Color.Black;
            this.bRight.BorderWidth = 1;
            this.bRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.bRight.Location = new System.Drawing.Point(517, 365);
            this.bRight.Name = "bRight";
            this.bRight.RoundedCorners.BottomLeft = true;
            this.bRight.RoundedCorners.BottomRight = true;
            this.bRight.RoundedCorners.TopLeft = true;
            this.bRight.RoundedCorners.TopRight = true;
            this.bRight.RoundingDiameter = 15;
            this.bRight.Size = new System.Drawing.Size(57, 57);
            this.bRight.TabIndex = 75;
            this.bRight.TabStop = false;
            this.bRight.Text = "Položka";
            this.bRight.Click += new System.EventHandler(this.bRight_Click);
            // 
            // bLeft
            // 
            this.bLeft.BorderColor = System.Drawing.Color.Black;
            this.bLeft.BorderWidth = 1;
            this.bLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.bLeft.Location = new System.Drawing.Point(441, 365);
            this.bLeft.Name = "bLeft";
            this.bLeft.RoundedCorners.BottomLeft = true;
            this.bLeft.RoundedCorners.BottomRight = true;
            this.bLeft.RoundedCorners.TopLeft = true;
            this.bLeft.RoundedCorners.TopRight = true;
            this.bLeft.RoundingDiameter = 15;
            this.bLeft.Size = new System.Drawing.Size(57, 57);
            this.bLeft.TabIndex = 74;
            this.bLeft.TabStop = false;
            this.bLeft.Text = "Položka";
            this.bLeft.Click += new System.EventHandler(this.bLeft_Click);
            // 
            // PaymentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.pKeys);
            this.Controls.Add(this.bSelectCustomer);
            this.Controls.Add(this.lCustomer);
            this.Controls.Add(this.tbCustomer);
            this.Controls.Add(this.pItems);
            this.Controls.Add(this.pPayment);
            this.Controls.Add(this.bAddPayment);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "PaymentForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.pItems.ResumeLayout(false);
            this.pItems.PerformLayout();
            this.gpPayments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lvPayments)).EndInit();
            this.pKeys.ResumeLayout(false);
            this.pPayment.ResumeLayout(false);
            this.pPayment.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.TouchButton b7;
        private Controls.TouchButton b9;
        private Controls.TouchButton b8;
        private Controls.TouchButton b6;
        private Controls.TouchButton b5;
        private Controls.TouchButton b4;
        private Controls.TouchButton b1;
        private Controls.TouchButton b2;
        private Controls.TouchButton b3;
        private Controls.TouchButton bTimes;
        private Controls.TouchButton b0;
        private Controls.TouchButton bDot;
        private Controls.TouchButton bConfirm;
        private Controls.TouchButton bBackspace;
        private Controls.TouchButton bDel;
        private System.Windows.Forms.TextBox tbCustomer;
        private System.Windows.Forms.Label lCustomer;
        private Controls.TouchButton bSelectCustomer;
        private BrightIdeasSoftware.HeaderFormatStyle headerFormatStyle1;
        private System.Windows.Forms.Panel pItems;
        private System.Windows.Forms.Panel pKeys;
        private System.Windows.Forms.Panel pPayment;
        private System.Windows.Forms.TextBox tbPaid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label label3;
        private Controls.TouchButton bCancel;
        private Controls.TouchButton bCloseRecord;
        private Controls.TouchButton bCard;
        private Controls.TouchButton bCash;
        private Controls.TouchButton bClose;
        private System.Windows.Forms.Label lReturnLabel;
        private System.Windows.Forms.TextBox tbRemains;
        private System.Windows.Forms.Label label4;
        private Controls.TouchButton bDiscount;
        private Controls.TouchButton bSplitPayment;
        private System.Windows.Forms.Label lError;
        private System.Windows.Forms.GroupBox gpPayments;
        private BrightIdeasSoftware.ObjectListView lvPayments;
        private BrightIdeasSoftware.OLVColumn olvcName;
        private BrightIdeasSoftware.OLVColumn olvcAmount;
        private System.Windows.Forms.TextBox tbReturn;
        private System.Windows.Forms.TextBox tbTotalWithDiscount;
        private System.Windows.Forms.Label lTotalWithDiscount;
        private System.Windows.Forms.TextBox tbDiscount;
        private System.Windows.Forms.Label lDiscount;
        private Controls.TouchButton bAddPayment;
        private Controls.TouchButton bSelect2;
        private Controls.TouchButton bSelect3;
        private Controls.TouchButton bSelect4;
        private Controls.TouchButton bSelect1;
        private Controls.TouchButton bRight;
        private Controls.TouchButton bLeft;

    }
}

