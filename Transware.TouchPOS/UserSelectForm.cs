﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Transware.Controls;
using Microdata.Util;
using Transware.Data.DataTypes;
using Transware.Data;
using Transware.Data.DataModel;
using Microdata.Data.DataTypes;
using Jadro.Data.DataTypes;
using Jadro.Data.DataModel;
using Transware.Core;

namespace Transware.TouchPOS
{
    public partial class UserSelectForm : Form
    {
        private List<TouchButton> buttons = new List<TouchButton>();

        public string Code { get; protected set; }
        public PackageSummary Package { get; protected set; }
        public int startIndex = 0;
        public List<UserShortcut> users = new List<UserShortcut>();
        public UserShortcut User { get; set; }
        private string buffer = "";
        private List<IDMedia> medias = new List<IDMedia>();
        private bool intermediate = false;
        private UserLoginForm loginForm = null;

        public UserSelectForm(bool intermediate = false)
        {
            this.intermediate = intermediate;

            InitializeComponent();

            LoadData();

            loginForm = new UserLoginForm(null);

            BuildButtons();

            DrawData();
        }

        private void LoadData()
        {
            users = UserShortcutModel.Instance.GetActive();
             medias = IDMedialModel.Instance.GetList();
        }

        
        private void BuildButtons()
        {
            int width = pStoreCard.Width;
            int height = pStoreCard.Height;

            int bw = 260;
            int bh = 50;
            int vspace = 20;
            int hspace = 20;
            
            for (int x = 420; x < width - bw; x += bw + hspace)
            {                
                for (int y = 20; y < height - bh; y += bh + vspace)                   
                {
                    TouchButton b = new TouchButton();
                    b.Font = new Font(b.Font.FontFamily, 14);
                    b.BorderColor = Color.Black;
                    b.BorderWidth = 1;
                    b.BorderColor = Color.Black;
                    b.RoundingDiameter = 20;
                    b.Left = x;
                    b.Top = y;
                    b.Width = bw;
                    b.Height = bh;
                    b.Visible = true;
                    b.Click += UserClick;
                    pStoreCard.Controls.Add(b);
                    buttons.Add(b);
                }
            }
        }

        private void DrawData()
        {
            int index = 0;
            int count = users.Count - startIndex;
            if (count < buttons.Count / 2)
            {
                index += (buttons.Count / 2 - count) / 2;
                for (int i = 0; i < index; i++)
                {
                    buttons[i].Text = "";
                    buttons[i].Visible = false;
                }

            }
            for (int i = startIndex; i < users.Count; i++)
            {

                UserShortcut c = users[i] as UserShortcut;
                buttons[index].Visible = true;
                buttons[index].Text = c.FullName ?? "Administrator";
                buttons[index].FillColorBottom = buttons[index].FillColorTop = Color.LightSteelBlue;
                buttons[index].Tag = c;

                index++;
                if (index == buttons.Count)
                    break;
            }

            //bLeft.Visible = startIndex != 0;
            //bRight.Visible = (startIndex+index) < objects.Count;

            while (index < buttons.Count)
            {
                buttons[index].Text = "";
                buttons[index].Visible = false;
                index++;
            }
        }

        private void UserClick(object sender, EventArgs e)
        {
            UserShortcut user = (sender as Control).Tag as UserShortcut;
            User = user;

            loginForm.User = user;
            if (loginForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                AppInfo.Instance.LoggedUser = user.CoreID;

                if (intermediate)
                {
                    DialogResult = DialogResult.OK;
                    Close();
                    return;
                }
                else
                {
                    MainForm mf = new MainForm();
                    mf.ShowDialog();
                }
            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                foreach (IDMedia id in medias)
                {
                    if (id.Tag == buffer)
                    {
                        foreach (TouchButton c in buttons)
                        {
                            if (c.Tag is UserShortcut && (c.Tag as UserShortcut).CoreID == id.OsobaID)
                                UserClick(c, e);
                        }
                    }
                }
                buffer = "";
            }
            else if (e.KeyCode == Keys.Escape || e.KeyCode == Keys.Delete)
            {
                buffer = "";
            }
            else if (e.KeyCode == Keys.Back)
            {
                if (buffer.Length > 0)
                    buffer = buffer.Remove(buffer.Length - 1);
            }
            else
            {
                char c = KeyboardUtil.GetKey(e.KeyCode);
                if (c >= 32)
                {
                    buffer += c;

                    e.Handled = true;
                }
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bLeft_Click(object sender, EventArgs e)
        {
            startIndex -= buttons.Count;
            DrawData();
        }

        private void bRight_Click(object sender, EventArgs e)
        {
            startIndex += buttons.Count;
            DrawData();
        }

        private void tbExit_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Abort;
            Close();
        }

        private void lAdmin_Click(object sender, EventArgs e)
        {
            UserShortcut user = new UserShortcut() { UserName = "sysadministrator" };
            user.Password = CryptoUtil.RijndaelEncryptorString("240611");
            User = user;

            loginForm.User = user;
            if (loginForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                AppInfo.Instance.LoggedUser = 0;

                if (intermediate)
                {
                    DialogResult = DialogResult.OK;
                    Close();
                    return;
                }
                else
                {
                    MainForm mf = new MainForm();
                    mf.ShowDialog();
                }
            }
        }

    }
}
