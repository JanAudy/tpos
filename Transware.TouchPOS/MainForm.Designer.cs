﻿namespace Transware.TouchPOS
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle7 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle8 = new BrightIdeasSoftware.HeaderStateStyle();
            BrightIdeasSoftware.HeaderStateStyle headerStateStyle9 = new BrightIdeasSoftware.HeaderStateStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tbCustomer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbProduct = new System.Windows.Forms.TextBox();
            this.lvItems = new BrightIdeasSoftware.ObjectListView();
            this.olvcCode = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcAmount = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcPrice = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcTotalPrice = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.headerFormatStyle1 = new BrightIdeasSoftware.HeaderFormatStyle();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pShortCuts = new System.Windows.Forms.Panel();
            this.lPage = new System.Windows.Forms.Label();
            this.bRight = new Transware.Controls.TouchButton();
            this.bLeft = new Transware.Controls.TouchButton();
            this.bItem16 = new Transware.Controls.TouchButton();
            this.bItem17 = new Transware.Controls.TouchButton();
            this.bItem18 = new Transware.Controls.TouchButton();
            this.bItem13 = new Transware.Controls.TouchButton();
            this.bItem14 = new Transware.Controls.TouchButton();
            this.bItem15 = new Transware.Controls.TouchButton();
            this.bItem10 = new Transware.Controls.TouchButton();
            this.bItem11 = new Transware.Controls.TouchButton();
            this.bItem12 = new Transware.Controls.TouchButton();
            this.bItem7 = new Transware.Controls.TouchButton();
            this.bItem8 = new Transware.Controls.TouchButton();
            this.bItem9 = new Transware.Controls.TouchButton();
            this.bItem4 = new Transware.Controls.TouchButton();
            this.bItem5 = new Transware.Controls.TouchButton();
            this.bItem6 = new Transware.Controls.TouchButton();
            this.bItem1 = new Transware.Controls.TouchButton();
            this.bItem2 = new Transware.Controls.TouchButton();
            this.bItem3 = new Transware.Controls.TouchButton();
            this.pKeys = new System.Windows.Forms.Panel();
            this.b7 = new Transware.Controls.TouchButton();
            this.b9 = new Transware.Controls.TouchButton();
            this.b8 = new Transware.Controls.TouchButton();
            this.b6 = new Transware.Controls.TouchButton();
            this.b5 = new Transware.Controls.TouchButton();
            this.b4 = new Transware.Controls.TouchButton();
            this.b3 = new Transware.Controls.TouchButton();
            this.b2 = new Transware.Controls.TouchButton();
            this.b1 = new Transware.Controls.TouchButton();
            this.bDot = new Transware.Controls.TouchButton();
            this.b0 = new Transware.Controls.TouchButton();
            this.bTimes = new Transware.Controls.TouchButton();
            this.bDel = new Transware.Controls.TouchButton();
            this.bBackspace = new Transware.Controls.TouchButton();
            this.bConfirm = new Transware.Controls.TouchButton();
            this.bMinus = new Transware.Controls.TouchButton();
            this.tbMenu = new Transware.Controls.TouchButton();
            this.bCancel = new Transware.Controls.TouchButton();
            this.bCancelItem = new Transware.Controls.TouchButton();
            this.bPayment = new Transware.Controls.TouchButton();
            this.bSelectProduct = new Transware.Controls.TouchButton();
            this.bSelectCustomer = new Transware.Controls.TouchButton();
            this.tbPriceChange = new Transware.Controls.TouchButton();
            this.tbExit = new Transware.Controls.TouchButton();
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).BeginInit();
            this.pShortCuts.SuspendLayout();
            this.pKeys.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbCustomer
            // 
            this.tbCustomer.BackColor = System.Drawing.Color.White;
            this.tbCustomer.Enabled = false;
            this.tbCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCustomer.Location = new System.Drawing.Point(159, 29);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.ReadOnly = true;
            this.tbCustomer.Size = new System.Drawing.Size(327, 38);
            this.tbCustomer.TabIndex = 33;
            this.tbCustomer.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 31);
            this.label1.TabIndex = 34;
            this.label1.Text = "Odběratel:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 31);
            this.label2.TabIndex = 37;
            this.label2.Text = "Zboží:";
            // 
            // tbProduct
            // 
            this.tbProduct.BackColor = System.Drawing.SystemColors.Window;
            this.tbProduct.Enabled = false;
            this.tbProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbProduct.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbProduct.Location = new System.Drawing.Point(159, 91);
            this.tbProduct.Name = "tbProduct";
            this.tbProduct.Size = new System.Drawing.Size(327, 38);
            this.tbProduct.TabIndex = 36;
            this.tbProduct.TabStop = false;
            // 
            // lvItems
            // 
            this.lvItems.AllColumns.Add(this.olvcCode);
            this.lvItems.AllColumns.Add(this.olvcName);
            this.lvItems.AllColumns.Add(this.olvcAmount);
            this.lvItems.AllColumns.Add(this.olvcPrice);
            this.lvItems.AllColumns.Add(this.olvcTotalPrice);
            this.lvItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvcCode,
            this.olvcName,
            this.olvcAmount,
            this.olvcPrice,
            this.olvcTotalPrice});
            this.lvItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lvItems.FullRowSelect = true;
            this.lvItems.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lvItems.HeaderFormatStyle = this.headerFormatStyle1;
            this.lvItems.HeaderUsesThemes = false;
            this.lvItems.Location = new System.Drawing.Point(12, 150);
            this.lvItems.MultiSelect = false;
            this.lvItems.Name = "lvItems";
            this.lvItems.RowHeight = 30;
            this.lvItems.ShowGroups = false;
            this.lvItems.Size = new System.Drawing.Size(580, 419);
            this.lvItems.TabIndex = 39;
            this.lvItems.UseCompatibleStateImageBehavior = false;
            this.lvItems.View = System.Windows.Forms.View.Details;
            // 
            // olvcCode
            // 
            this.olvcCode.AspectName = "";
            this.olvcCode.Text = "Kód";
            // 
            // olvcName
            // 
            this.olvcName.AspectName = "Name";
            this.olvcName.Text = "Název";
            this.olvcName.Width = 300;
            // 
            // olvcAmount
            // 
            this.olvcAmount.AspectName = "Amount";
            this.olvcAmount.AspectToStringFormat = "{0:0.#####}";
            this.olvcAmount.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcAmount.Text = "Množ.";
            this.olvcAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcAmount.Width = 45;
            // 
            // olvcPrice
            // 
            this.olvcPrice.AspectName = "PriceWithDiscount";
            this.olvcPrice.AspectToStringFormat = "{0:0.00}";
            this.olvcPrice.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcPrice.Text = "Cena/Mj";
            this.olvcPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcPrice.Width = 70;
            // 
            // olvcTotalPrice
            // 
            this.olvcTotalPrice.AspectName = "TotalPriceWithDiscount";
            this.olvcTotalPrice.AspectToStringFormat = "{0:0.00}";
            this.olvcTotalPrice.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcTotalPrice.Text = "Cena/pol";
            this.olvcTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvcTotalPrice.Width = 70;
            // 
            // headerFormatStyle1
            // 
            headerStateStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Hot = headerStateStyle7;
            headerStateStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Normal = headerStateStyle8;
            headerStateStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerFormatStyle1.Pressed = headerStateStyle9;
            // 
            // tbTotal
            // 
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTotal.Location = new System.Drawing.Point(133, 575);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(459, 62);
            this.tbTotal.TabIndex = 40;
            this.tbTotal.TabStop = false;
            this.tbTotal.Text = "3701";
            this.tbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 591);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 31);
            this.label3.TabIndex = 41;
            this.label3.Text = "Celkem:";
            // 
            // pShortCuts
            // 
            this.pShortCuts.Controls.Add(this.lPage);
            this.pShortCuts.Controls.Add(this.bRight);
            this.pShortCuts.Controls.Add(this.bLeft);
            this.pShortCuts.Controls.Add(this.bItem16);
            this.pShortCuts.Controls.Add(this.bItem17);
            this.pShortCuts.Controls.Add(this.bItem18);
            this.pShortCuts.Controls.Add(this.bItem13);
            this.pShortCuts.Controls.Add(this.bItem14);
            this.pShortCuts.Controls.Add(this.bItem15);
            this.pShortCuts.Controls.Add(this.bItem10);
            this.pShortCuts.Controls.Add(this.bItem11);
            this.pShortCuts.Controls.Add(this.bItem12);
            this.pShortCuts.Controls.Add(this.bItem7);
            this.pShortCuts.Controls.Add(this.bItem8);
            this.pShortCuts.Controls.Add(this.bItem9);
            this.pShortCuts.Controls.Add(this.bItem4);
            this.pShortCuts.Controls.Add(this.bItem5);
            this.pShortCuts.Controls.Add(this.bItem6);
            this.pShortCuts.Controls.Add(this.bItem1);
            this.pShortCuts.Controls.Add(this.bItem2);
            this.pShortCuts.Controls.Add(this.bItem3);
            this.pShortCuts.Location = new System.Drawing.Point(603, 16);
            this.pShortCuts.Name = "pShortCuts";
            this.pShortCuts.Size = new System.Drawing.Size(404, 299);
            this.pShortCuts.TabIndex = 47;
            // 
            // lPage
            // 
            this.lPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPage.Location = new System.Drawing.Point(54, 253);
            this.lPage.Name = "lPage";
            this.lPage.Size = new System.Drawing.Size(296, 42);
            this.lPage.TabIndex = 37;
            this.lPage.Text = "Strana";
            this.lPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bRight
            // 
            this.bRight.BorderColor = System.Drawing.Color.Black;
            this.bRight.BorderWidth = 1;
            this.bRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bRight.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bRight.Image = global::Transware.TouchPOS.Properties.Resources.right;
            this.bRight.Location = new System.Drawing.Point(356, 253);
            this.bRight.Name = "bRight";
            this.bRight.RoundedCorners.BottomLeft = true;
            this.bRight.RoundedCorners.BottomRight = true;
            this.bRight.RoundedCorners.TopLeft = true;
            this.bRight.RoundedCorners.TopRight = true;
            this.bRight.RoundingDiameter = 15;
            this.bRight.Size = new System.Drawing.Size(45, 42);
            this.bRight.TabIndex = 36;
            this.bRight.TabStop = false;
            this.bRight.Text = "Položka";
            this.bRight.Click += new System.EventHandler(this.bRight_Click);
            // 
            // bLeft
            // 
            this.bLeft.BorderColor = System.Drawing.Color.Black;
            this.bLeft.BorderWidth = 1;
            this.bLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bLeft.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bLeft.Image = global::Transware.TouchPOS.Properties.Resources.left;
            this.bLeft.Location = new System.Drawing.Point(3, 253);
            this.bLeft.Name = "bLeft";
            this.bLeft.RoundedCorners.BottomLeft = true;
            this.bLeft.RoundedCorners.BottomRight = true;
            this.bLeft.RoundedCorners.TopLeft = true;
            this.bLeft.RoundedCorners.TopRight = true;
            this.bLeft.RoundingDiameter = 15;
            this.bLeft.Size = new System.Drawing.Size(45, 42);
            this.bLeft.TabIndex = 35;
            this.bLeft.TabStop = false;
            this.bLeft.Text = "Položka";
            this.bLeft.Click += new System.EventHandler(this.bLeft_Click);
            // 
            // bItem16
            // 
            this.bItem16.BorderColor = System.Drawing.Color.Black;
            this.bItem16.BorderWidth = 1;
            this.bItem16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem16.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem16.Image = null;
            this.bItem16.Location = new System.Drawing.Point(2, 205);
            this.bItem16.Name = "bItem16";
            this.bItem16.RoundedCorners.BottomLeft = true;
            this.bItem16.RoundedCorners.BottomRight = true;
            this.bItem16.RoundedCorners.TopLeft = true;
            this.bItem16.RoundedCorners.TopRight = true;
            this.bItem16.RoundingDiameter = 15;
            this.bItem16.Size = new System.Drawing.Size(130, 35);
            this.bItem16.TabIndex = 32;
            this.bItem16.TabStop = false;
            this.bItem16.Text = "Položka";
            this.bItem16.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem17
            // 
            this.bItem17.BorderColor = System.Drawing.Color.Black;
            this.bItem17.BorderWidth = 1;
            this.bItem17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem17.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem17.Image = null;
            this.bItem17.Location = new System.Drawing.Point(138, 205);
            this.bItem17.Name = "bItem17";
            this.bItem17.RoundedCorners.BottomLeft = true;
            this.bItem17.RoundedCorners.BottomRight = true;
            this.bItem17.RoundedCorners.TopLeft = true;
            this.bItem17.RoundedCorners.TopRight = true;
            this.bItem17.RoundingDiameter = 15;
            this.bItem17.Size = new System.Drawing.Size(130, 35);
            this.bItem17.TabIndex = 33;
            this.bItem17.TabStop = false;
            this.bItem17.Text = "Položka";
            this.bItem17.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem18
            // 
            this.bItem18.BorderColor = System.Drawing.Color.Black;
            this.bItem18.BorderWidth = 1;
            this.bItem18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem18.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem18.Image = null;
            this.bItem18.Location = new System.Drawing.Point(274, 205);
            this.bItem18.Name = "bItem18";
            this.bItem18.RoundedCorners.BottomLeft = true;
            this.bItem18.RoundedCorners.BottomRight = true;
            this.bItem18.RoundedCorners.TopLeft = true;
            this.bItem18.RoundedCorners.TopRight = true;
            this.bItem18.RoundingDiameter = 15;
            this.bItem18.Size = new System.Drawing.Size(130, 35);
            this.bItem18.TabIndex = 34;
            this.bItem18.TabStop = false;
            this.bItem18.Text = "Položka";
            this.bItem18.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem13
            // 
            this.bItem13.BorderColor = System.Drawing.Color.Black;
            this.bItem13.BorderWidth = 1;
            this.bItem13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem13.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem13.Image = null;
            this.bItem13.Location = new System.Drawing.Point(2, 164);
            this.bItem13.Name = "bItem13";
            this.bItem13.RoundedCorners.BottomLeft = true;
            this.bItem13.RoundedCorners.BottomRight = true;
            this.bItem13.RoundedCorners.TopLeft = true;
            this.bItem13.RoundedCorners.TopRight = true;
            this.bItem13.RoundingDiameter = 15;
            this.bItem13.Size = new System.Drawing.Size(130, 35);
            this.bItem13.TabIndex = 29;
            this.bItem13.TabStop = false;
            this.bItem13.Text = "Položka";
            this.bItem13.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem14
            // 
            this.bItem14.BorderColor = System.Drawing.Color.Black;
            this.bItem14.BorderWidth = 1;
            this.bItem14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem14.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem14.Image = null;
            this.bItem14.Location = new System.Drawing.Point(138, 164);
            this.bItem14.Name = "bItem14";
            this.bItem14.RoundedCorners.BottomLeft = true;
            this.bItem14.RoundedCorners.BottomRight = true;
            this.bItem14.RoundedCorners.TopLeft = true;
            this.bItem14.RoundedCorners.TopRight = true;
            this.bItem14.RoundingDiameter = 15;
            this.bItem14.Size = new System.Drawing.Size(130, 35);
            this.bItem14.TabIndex = 30;
            this.bItem14.TabStop = false;
            this.bItem14.Text = "Položka";
            this.bItem14.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem15
            // 
            this.bItem15.BorderColor = System.Drawing.Color.Black;
            this.bItem15.BorderWidth = 1;
            this.bItem15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem15.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem15.Image = null;
            this.bItem15.Location = new System.Drawing.Point(274, 164);
            this.bItem15.Name = "bItem15";
            this.bItem15.RoundedCorners.BottomLeft = true;
            this.bItem15.RoundedCorners.BottomRight = true;
            this.bItem15.RoundedCorners.TopLeft = true;
            this.bItem15.RoundedCorners.TopRight = true;
            this.bItem15.RoundingDiameter = 15;
            this.bItem15.Size = new System.Drawing.Size(130, 35);
            this.bItem15.TabIndex = 31;
            this.bItem15.TabStop = false;
            this.bItem15.Text = "Položka";
            this.bItem15.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem10
            // 
            this.bItem10.BorderColor = System.Drawing.Color.Black;
            this.bItem10.BorderWidth = 1;
            this.bItem10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem10.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem10.Image = null;
            this.bItem10.Location = new System.Drawing.Point(2, 123);
            this.bItem10.Name = "bItem10";
            this.bItem10.RoundedCorners.BottomLeft = true;
            this.bItem10.RoundedCorners.BottomRight = true;
            this.bItem10.RoundedCorners.TopLeft = true;
            this.bItem10.RoundedCorners.TopRight = true;
            this.bItem10.RoundingDiameter = 15;
            this.bItem10.Size = new System.Drawing.Size(130, 35);
            this.bItem10.TabIndex = 26;
            this.bItem10.TabStop = false;
            this.bItem10.Text = "Položka";
            this.bItem10.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem11
            // 
            this.bItem11.BorderColor = System.Drawing.Color.Black;
            this.bItem11.BorderWidth = 1;
            this.bItem11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem11.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem11.Image = null;
            this.bItem11.Location = new System.Drawing.Point(138, 123);
            this.bItem11.Name = "bItem11";
            this.bItem11.RoundedCorners.BottomLeft = true;
            this.bItem11.RoundedCorners.BottomRight = true;
            this.bItem11.RoundedCorners.TopLeft = true;
            this.bItem11.RoundedCorners.TopRight = true;
            this.bItem11.RoundingDiameter = 15;
            this.bItem11.Size = new System.Drawing.Size(130, 35);
            this.bItem11.TabIndex = 27;
            this.bItem11.TabStop = false;
            this.bItem11.Text = "Položka";
            this.bItem11.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem12
            // 
            this.bItem12.BorderColor = System.Drawing.Color.Black;
            this.bItem12.BorderWidth = 1;
            this.bItem12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem12.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem12.Image = null;
            this.bItem12.Location = new System.Drawing.Point(274, 123);
            this.bItem12.Name = "bItem12";
            this.bItem12.RoundedCorners.BottomLeft = true;
            this.bItem12.RoundedCorners.BottomRight = true;
            this.bItem12.RoundedCorners.TopLeft = true;
            this.bItem12.RoundedCorners.TopRight = true;
            this.bItem12.RoundingDiameter = 15;
            this.bItem12.Size = new System.Drawing.Size(130, 35);
            this.bItem12.TabIndex = 28;
            this.bItem12.TabStop = false;
            this.bItem12.Text = "Položka";
            this.bItem12.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem7
            // 
            this.bItem7.BorderColor = System.Drawing.Color.Black;
            this.bItem7.BorderWidth = 1;
            this.bItem7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem7.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem7.Image = null;
            this.bItem7.Location = new System.Drawing.Point(2, 82);
            this.bItem7.Name = "bItem7";
            this.bItem7.RoundedCorners.BottomLeft = true;
            this.bItem7.RoundedCorners.BottomRight = true;
            this.bItem7.RoundedCorners.TopLeft = true;
            this.bItem7.RoundedCorners.TopRight = true;
            this.bItem7.RoundingDiameter = 15;
            this.bItem7.Size = new System.Drawing.Size(130, 35);
            this.bItem7.TabIndex = 23;
            this.bItem7.TabStop = false;
            this.bItem7.Text = "Položka";
            this.bItem7.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem8
            // 
            this.bItem8.BorderColor = System.Drawing.Color.Black;
            this.bItem8.BorderWidth = 1;
            this.bItem8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem8.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem8.Image = null;
            this.bItem8.Location = new System.Drawing.Point(138, 82);
            this.bItem8.Name = "bItem8";
            this.bItem8.RoundedCorners.BottomLeft = true;
            this.bItem8.RoundedCorners.BottomRight = true;
            this.bItem8.RoundedCorners.TopLeft = true;
            this.bItem8.RoundedCorners.TopRight = true;
            this.bItem8.RoundingDiameter = 15;
            this.bItem8.Size = new System.Drawing.Size(130, 35);
            this.bItem8.TabIndex = 24;
            this.bItem8.TabStop = false;
            this.bItem8.Text = "Položka";
            this.bItem8.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem9
            // 
            this.bItem9.BorderColor = System.Drawing.Color.Black;
            this.bItem9.BorderWidth = 1;
            this.bItem9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem9.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem9.Image = null;
            this.bItem9.Location = new System.Drawing.Point(274, 82);
            this.bItem9.Name = "bItem9";
            this.bItem9.RoundedCorners.BottomLeft = true;
            this.bItem9.RoundedCorners.BottomRight = true;
            this.bItem9.RoundedCorners.TopLeft = true;
            this.bItem9.RoundedCorners.TopRight = true;
            this.bItem9.RoundingDiameter = 15;
            this.bItem9.Size = new System.Drawing.Size(130, 35);
            this.bItem9.TabIndex = 25;
            this.bItem9.TabStop = false;
            this.bItem9.Text = "Položka";
            this.bItem9.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem4
            // 
            this.bItem4.BorderColor = System.Drawing.Color.Black;
            this.bItem4.BorderWidth = 1;
            this.bItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem4.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem4.Image = null;
            this.bItem4.Location = new System.Drawing.Point(2, 41);
            this.bItem4.Name = "bItem4";
            this.bItem4.RoundedCorners.BottomLeft = true;
            this.bItem4.RoundedCorners.BottomRight = true;
            this.bItem4.RoundedCorners.TopLeft = true;
            this.bItem4.RoundedCorners.TopRight = true;
            this.bItem4.RoundingDiameter = 15;
            this.bItem4.Size = new System.Drawing.Size(130, 35);
            this.bItem4.TabIndex = 20;
            this.bItem4.TabStop = false;
            this.bItem4.Text = "Položka";
            this.bItem4.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem5
            // 
            this.bItem5.BorderColor = System.Drawing.Color.Black;
            this.bItem5.BorderWidth = 1;
            this.bItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem5.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem5.Image = null;
            this.bItem5.Location = new System.Drawing.Point(138, 41);
            this.bItem5.Name = "bItem5";
            this.bItem5.RoundedCorners.BottomLeft = true;
            this.bItem5.RoundedCorners.BottomRight = true;
            this.bItem5.RoundedCorners.TopLeft = true;
            this.bItem5.RoundedCorners.TopRight = true;
            this.bItem5.RoundingDiameter = 15;
            this.bItem5.Size = new System.Drawing.Size(130, 35);
            this.bItem5.TabIndex = 21;
            this.bItem5.TabStop = false;
            this.bItem5.Text = "Položka";
            this.bItem5.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem6
            // 
            this.bItem6.BorderColor = System.Drawing.Color.Black;
            this.bItem6.BorderWidth = 1;
            this.bItem6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem6.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem6.Image = null;
            this.bItem6.Location = new System.Drawing.Point(274, 41);
            this.bItem6.Name = "bItem6";
            this.bItem6.RoundedCorners.BottomLeft = true;
            this.bItem6.RoundedCorners.BottomRight = true;
            this.bItem6.RoundedCorners.TopLeft = true;
            this.bItem6.RoundedCorners.TopRight = true;
            this.bItem6.RoundingDiameter = 15;
            this.bItem6.Size = new System.Drawing.Size(130, 35);
            this.bItem6.TabIndex = 22;
            this.bItem6.TabStop = false;
            this.bItem6.Text = "Položka";
            this.bItem6.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem1
            // 
            this.bItem1.BorderColor = System.Drawing.Color.Black;
            this.bItem1.BorderWidth = 1;
            this.bItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem1.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem1.Image = null;
            this.bItem1.Location = new System.Drawing.Point(2, 0);
            this.bItem1.Name = "bItem1";
            this.bItem1.RoundedCorners.BottomLeft = true;
            this.bItem1.RoundedCorners.BottomRight = true;
            this.bItem1.RoundedCorners.TopLeft = true;
            this.bItem1.RoundedCorners.TopRight = true;
            this.bItem1.RoundingDiameter = 15;
            this.bItem1.Size = new System.Drawing.Size(130, 35);
            this.bItem1.TabIndex = 17;
            this.bItem1.TabStop = false;
            this.bItem1.Text = "Položka";
            this.bItem1.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem2
            // 
            this.bItem2.BorderColor = System.Drawing.Color.Black;
            this.bItem2.BorderWidth = 1;
            this.bItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem2.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem2.Image = null;
            this.bItem2.Location = new System.Drawing.Point(138, 0);
            this.bItem2.Name = "bItem2";
            this.bItem2.RoundedCorners.BottomLeft = true;
            this.bItem2.RoundedCorners.BottomRight = true;
            this.bItem2.RoundedCorners.TopLeft = true;
            this.bItem2.RoundedCorners.TopRight = true;
            this.bItem2.RoundingDiameter = 15;
            this.bItem2.Size = new System.Drawing.Size(130, 35);
            this.bItem2.TabIndex = 18;
            this.bItem2.TabStop = false;
            this.bItem2.Text = "Položka";
            this.bItem2.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // bItem3
            // 
            this.bItem3.BorderColor = System.Drawing.Color.Black;
            this.bItem3.BorderWidth = 1;
            this.bItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bItem3.FillColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.FillColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bItem3.Image = null;
            this.bItem3.Location = new System.Drawing.Point(274, 0);
            this.bItem3.Name = "bItem3";
            this.bItem3.RoundedCorners.BottomLeft = true;
            this.bItem3.RoundedCorners.BottomRight = true;
            this.bItem3.RoundedCorners.TopLeft = true;
            this.bItem3.RoundedCorners.TopRight = true;
            this.bItem3.RoundingDiameter = 15;
            this.bItem3.Size = new System.Drawing.Size(130, 35);
            this.bItem3.TabIndex = 19;
            this.bItem3.TabStop = false;
            this.bItem3.Text = "Položka";
            this.bItem3.Click += new System.EventHandler(this.ShortcutClick);
            // 
            // pKeys
            // 
            this.pKeys.Controls.Add(this.b7);
            this.pKeys.Controls.Add(this.b9);
            this.pKeys.Controls.Add(this.b8);
            this.pKeys.Controls.Add(this.b6);
            this.pKeys.Controls.Add(this.b5);
            this.pKeys.Controls.Add(this.b4);
            this.pKeys.Controls.Add(this.b3);
            this.pKeys.Controls.Add(this.b2);
            this.pKeys.Controls.Add(this.b1);
            this.pKeys.Controls.Add(this.bDot);
            this.pKeys.Controls.Add(this.b0);
            this.pKeys.Controls.Add(this.bTimes);
            this.pKeys.Controls.Add(this.bDel);
            this.pKeys.Controls.Add(this.bBackspace);
            this.pKeys.Controls.Add(this.bConfirm);
            this.pKeys.Location = new System.Drawing.Point(709, 321);
            this.pKeys.Name = "pKeys";
            this.pKeys.Size = new System.Drawing.Size(298, 316);
            this.pKeys.TabIndex = 49;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.SystemColors.Control;
            this.b7.BorderColor = System.Drawing.Color.Black;
            this.b7.BorderWidth = 2;
            this.b7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b7.FillColorBottom = System.Drawing.Color.Blue;
            this.b7.FillColorTop = System.Drawing.Color.Blue;
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b7.ForeColor = System.Drawing.Color.White;
            this.b7.Image = null;
            this.b7.Location = new System.Drawing.Point(0, 0);
            this.b7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b7.Name = "b7";
            this.b7.RoundedCorners.BottomLeft = true;
            this.b7.RoundedCorners.BottomRight = true;
            this.b7.RoundedCorners.TopLeft = true;
            this.b7.RoundedCorners.TopRight = true;
            this.b7.RoundingDiameter = 15;
            this.b7.Size = new System.Drawing.Size(95, 60);
            this.b7.TabIndex = 0;
            this.b7.TabStop = false;
            this.b7.Tag = "7";
            this.b7.Text = "7";
            this.b7.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.SystemColors.Control;
            this.b9.BorderColor = System.Drawing.Color.Black;
            this.b9.BorderWidth = 2;
            this.b9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b9.FillColorBottom = System.Drawing.Color.Blue;
            this.b9.FillColorTop = System.Drawing.Color.Blue;
            this.b9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b9.ForeColor = System.Drawing.Color.White;
            this.b9.Image = null;
            this.b9.Location = new System.Drawing.Point(203, 0);
            this.b9.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b9.Name = "b9";
            this.b9.RoundedCorners.BottomLeft = true;
            this.b9.RoundedCorners.BottomRight = true;
            this.b9.RoundedCorners.TopLeft = true;
            this.b9.RoundedCorners.TopRight = true;
            this.b9.RoundingDiameter = 15;
            this.b9.Size = new System.Drawing.Size(95, 60);
            this.b9.TabIndex = 3;
            this.b9.TabStop = false;
            this.b9.Tag = "9";
            this.b9.Text = "9";
            this.b9.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.SystemColors.Control;
            this.b8.BorderColor = System.Drawing.Color.Black;
            this.b8.BorderWidth = 2;
            this.b8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b8.FillColorBottom = System.Drawing.Color.Blue;
            this.b8.FillColorTop = System.Drawing.Color.Blue;
            this.b8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b8.ForeColor = System.Drawing.Color.White;
            this.b8.Image = null;
            this.b8.Location = new System.Drawing.Point(102, 0);
            this.b8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b8.Name = "b8";
            this.b8.RoundedCorners.BottomLeft = true;
            this.b8.RoundedCorners.BottomRight = true;
            this.b8.RoundedCorners.TopLeft = true;
            this.b8.RoundedCorners.TopRight = true;
            this.b8.RoundingDiameter = 15;
            this.b8.Size = new System.Drawing.Size(95, 60);
            this.b8.TabIndex = 4;
            this.b8.TabStop = false;
            this.b8.Tag = "8";
            this.b8.Text = "8";
            this.b8.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.SystemColors.Control;
            this.b6.BorderColor = System.Drawing.Color.Black;
            this.b6.BorderWidth = 2;
            this.b6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b6.FillColorBottom = System.Drawing.Color.Blue;
            this.b6.FillColorTop = System.Drawing.Color.Blue;
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b6.ForeColor = System.Drawing.Color.White;
            this.b6.Image = null;
            this.b6.Location = new System.Drawing.Point(203, 64);
            this.b6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b6.Name = "b6";
            this.b6.RoundedCorners.BottomLeft = true;
            this.b6.RoundedCorners.BottomRight = true;
            this.b6.RoundedCorners.TopLeft = true;
            this.b6.RoundedCorners.TopRight = true;
            this.b6.RoundingDiameter = 15;
            this.b6.Size = new System.Drawing.Size(95, 60);
            this.b6.TabIndex = 5;
            this.b6.TabStop = false;
            this.b6.Tag = "6";
            this.b6.Text = "6";
            this.b6.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.SystemColors.Control;
            this.b5.BorderColor = System.Drawing.Color.Black;
            this.b5.BorderWidth = 2;
            this.b5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b5.FillColorBottom = System.Drawing.Color.Blue;
            this.b5.FillColorTop = System.Drawing.Color.Blue;
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b5.ForeColor = System.Drawing.Color.White;
            this.b5.Image = null;
            this.b5.Location = new System.Drawing.Point(102, 64);
            this.b5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b5.Name = "b5";
            this.b5.RoundedCorners.BottomLeft = true;
            this.b5.RoundedCorners.BottomRight = true;
            this.b5.RoundedCorners.TopLeft = true;
            this.b5.RoundedCorners.TopRight = true;
            this.b5.RoundingDiameter = 15;
            this.b5.Size = new System.Drawing.Size(95, 60);
            this.b5.TabIndex = 6;
            this.b5.TabStop = false;
            this.b5.Tag = "5";
            this.b5.Text = "5";
            this.b5.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.SystemColors.Control;
            this.b4.BorderColor = System.Drawing.Color.Black;
            this.b4.BorderWidth = 2;
            this.b4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b4.FillColorBottom = System.Drawing.Color.Blue;
            this.b4.FillColorTop = System.Drawing.Color.Blue;
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b4.ForeColor = System.Drawing.Color.White;
            this.b4.Image = null;
            this.b4.Location = new System.Drawing.Point(0, 64);
            this.b4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b4.Name = "b4";
            this.b4.RoundedCorners.BottomLeft = true;
            this.b4.RoundedCorners.BottomRight = true;
            this.b4.RoundedCorners.TopLeft = true;
            this.b4.RoundedCorners.TopRight = true;
            this.b4.RoundingDiameter = 15;
            this.b4.Size = new System.Drawing.Size(95, 60);
            this.b4.TabIndex = 7;
            this.b4.TabStop = false;
            this.b4.Tag = "4";
            this.b4.Text = "4";
            this.b4.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.SystemColors.Control;
            this.b3.BorderColor = System.Drawing.Color.Black;
            this.b3.BorderWidth = 2;
            this.b3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b3.FillColorBottom = System.Drawing.Color.Blue;
            this.b3.FillColorTop = System.Drawing.Color.Blue;
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b3.ForeColor = System.Drawing.Color.White;
            this.b3.Image = null;
            this.b3.Location = new System.Drawing.Point(203, 128);
            this.b3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b3.Name = "b3";
            this.b3.RoundedCorners.BottomLeft = true;
            this.b3.RoundedCorners.BottomRight = true;
            this.b3.RoundedCorners.TopLeft = true;
            this.b3.RoundedCorners.TopRight = true;
            this.b3.RoundingDiameter = 15;
            this.b3.Size = new System.Drawing.Size(95, 60);
            this.b3.TabIndex = 8;
            this.b3.TabStop = false;
            this.b3.Tag = "3";
            this.b3.Text = "3";
            this.b3.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.SystemColors.Control;
            this.b2.BorderColor = System.Drawing.Color.Black;
            this.b2.BorderWidth = 2;
            this.b2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b2.FillColorBottom = System.Drawing.Color.Blue;
            this.b2.FillColorTop = System.Drawing.Color.Blue;
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b2.ForeColor = System.Drawing.Color.White;
            this.b2.Image = null;
            this.b2.Location = new System.Drawing.Point(102, 128);
            this.b2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b2.Name = "b2";
            this.b2.RoundedCorners.BottomLeft = true;
            this.b2.RoundedCorners.BottomRight = true;
            this.b2.RoundedCorners.TopLeft = true;
            this.b2.RoundedCorners.TopRight = true;
            this.b2.RoundingDiameter = 15;
            this.b2.Size = new System.Drawing.Size(95, 60);
            this.b2.TabIndex = 9;
            this.b2.TabStop = false;
            this.b2.Tag = "2";
            this.b2.Text = "2";
            this.b2.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.SystemColors.Control;
            this.b1.BorderColor = System.Drawing.Color.Black;
            this.b1.BorderWidth = 2;
            this.b1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b1.FillColorBottom = System.Drawing.Color.Blue;
            this.b1.FillColorTop = System.Drawing.Color.Blue;
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b1.ForeColor = System.Drawing.Color.White;
            this.b1.Image = null;
            this.b1.Location = new System.Drawing.Point(0, 128);
            this.b1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b1.Name = "b1";
            this.b1.RoundedCorners.BottomLeft = true;
            this.b1.RoundedCorners.BottomRight = true;
            this.b1.RoundedCorners.TopLeft = true;
            this.b1.RoundedCorners.TopRight = true;
            this.b1.RoundingDiameter = 15;
            this.b1.Size = new System.Drawing.Size(95, 60);
            this.b1.TabIndex = 10;
            this.b1.TabStop = false;
            this.b1.Tag = "1";
            this.b1.Text = "1";
            this.b1.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDot
            // 
            this.bDot.BackColor = System.Drawing.SystemColors.Control;
            this.bDot.BorderColor = System.Drawing.Color.Black;
            this.bDot.BorderWidth = 2;
            this.bDot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDot.FillColorBottom = System.Drawing.Color.Blue;
            this.bDot.FillColorTop = System.Drawing.Color.Blue;
            this.bDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDot.ForeColor = System.Drawing.Color.White;
            this.bDot.Image = null;
            this.bDot.Location = new System.Drawing.Point(203, 192);
            this.bDot.Margin = new System.Windows.Forms.Padding(2);
            this.bDot.Name = "bDot";
            this.bDot.RoundedCorners.BottomLeft = true;
            this.bDot.RoundedCorners.BottomRight = true;
            this.bDot.RoundedCorners.TopLeft = true;
            this.bDot.RoundedCorners.TopRight = true;
            this.bDot.RoundingDiameter = 15;
            this.bDot.Size = new System.Drawing.Size(95, 60);
            this.bDot.TabIndex = 11;
            this.bDot.TabStop = false;
            this.bDot.Tag = ",";
            this.bDot.Text = ",";
            this.bDot.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // b0
            // 
            this.b0.BackColor = System.Drawing.SystemColors.Control;
            this.b0.BorderColor = System.Drawing.Color.Black;
            this.b0.BorderWidth = 2;
            this.b0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.b0.FillColorBottom = System.Drawing.Color.Blue;
            this.b0.FillColorTop = System.Drawing.Color.Blue;
            this.b0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b0.ForeColor = System.Drawing.Color.White;
            this.b0.Image = null;
            this.b0.Location = new System.Drawing.Point(102, 192);
            this.b0.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.b0.Name = "b0";
            this.b0.RoundedCorners.BottomLeft = true;
            this.b0.RoundedCorners.BottomRight = true;
            this.b0.RoundedCorners.TopLeft = true;
            this.b0.RoundedCorners.TopRight = true;
            this.b0.RoundingDiameter = 15;
            this.b0.Size = new System.Drawing.Size(95, 60);
            this.b0.TabIndex = 12;
            this.b0.TabStop = false;
            this.b0.Tag = "0";
            this.b0.Text = "0";
            this.b0.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bTimes
            // 
            this.bTimes.BackColor = System.Drawing.SystemColors.Control;
            this.bTimes.BorderColor = System.Drawing.Color.Black;
            this.bTimes.BorderWidth = 2;
            this.bTimes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bTimes.FillColorBottom = System.Drawing.Color.Blue;
            this.bTimes.FillColorTop = System.Drawing.Color.Blue;
            this.bTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bTimes.ForeColor = System.Drawing.Color.White;
            this.bTimes.Image = null;
            this.bTimes.Location = new System.Drawing.Point(0, 192);
            this.bTimes.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bTimes.Name = "bTimes";
            this.bTimes.RoundedCorners.BottomLeft = true;
            this.bTimes.RoundedCorners.BottomRight = true;
            this.bTimes.RoundedCorners.TopLeft = true;
            this.bTimes.RoundedCorners.TopRight = true;
            this.bTimes.RoundingDiameter = 15;
            this.bTimes.Size = new System.Drawing.Size(95, 60);
            this.bTimes.TabIndex = 13;
            this.bTimes.TabStop = false;
            this.bTimes.Tag = " * ";
            this.bTimes.Text = "X";
            this.bTimes.Click += new System.EventHandler(this.NumberButtonClick);
            // 
            // bDel
            // 
            this.bDel.BackColor = System.Drawing.SystemColors.Control;
            this.bDel.BorderColor = System.Drawing.Color.Black;
            this.bDel.BorderWidth = 2;
            this.bDel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bDel.FillColorBottom = System.Drawing.Color.Red;
            this.bDel.FillColorTop = System.Drawing.Color.Red;
            this.bDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDel.ForeColor = System.Drawing.Color.White;
            this.bDel.Image = null;
            this.bDel.Location = new System.Drawing.Point(-1, 256);
            this.bDel.Margin = new System.Windows.Forms.Padding(2);
            this.bDel.Name = "bDel";
            this.bDel.RoundedCorners.BottomLeft = true;
            this.bDel.RoundedCorners.BottomRight = true;
            this.bDel.RoundedCorners.TopLeft = true;
            this.bDel.RoundedCorners.TopRight = true;
            this.bDel.RoundingDiameter = 15;
            this.bDel.Size = new System.Drawing.Size(95, 60);
            this.bDel.TabIndex = 14;
            this.bDel.TabStop = false;
            this.bDel.Text = "Smazat";
            this.bDel.Click += new System.EventHandler(this.ClearCodeClick);
            // 
            // bBackspace
            // 
            this.bBackspace.BackColor = System.Drawing.SystemColors.Control;
            this.bBackspace.BorderColor = System.Drawing.Color.Black;
            this.bBackspace.BorderWidth = 2;
            this.bBackspace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bBackspace.FillColorBottom = System.Drawing.Color.Red;
            this.bBackspace.FillColorTop = System.Drawing.Color.Red;
            this.bBackspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bBackspace.ForeColor = System.Drawing.Color.White;
            this.bBackspace.Image = ((System.Drawing.Image)(resources.GetObject("bBackspace.Image")));
            this.bBackspace.Location = new System.Drawing.Point(102, 256);
            this.bBackspace.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bBackspace.Name = "bBackspace";
            this.bBackspace.RoundedCorners.BottomLeft = true;
            this.bBackspace.RoundedCorners.BottomRight = true;
            this.bBackspace.RoundedCorners.TopLeft = true;
            this.bBackspace.RoundedCorners.TopRight = true;
            this.bBackspace.RoundingDiameter = 15;
            this.bBackspace.Size = new System.Drawing.Size(95, 60);
            this.bBackspace.TabIndex = 15;
            this.bBackspace.TabStop = false;
            this.bBackspace.Text = "0";
            this.bBackspace.Click += new System.EventHandler(this.DeleteLastClick);
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.bConfirm.BorderColor = System.Drawing.Color.Black;
            this.bConfirm.BorderWidth = 2;
            this.bConfirm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bConfirm.FillColorBottom = System.Drawing.Color.Blue;
            this.bConfirm.FillColorTop = System.Drawing.Color.Blue;
            this.bConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Image = null;
            this.bConfirm.Location = new System.Drawing.Point(203, 256);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.RoundedCorners.BottomLeft = true;
            this.bConfirm.RoundedCorners.BottomRight = true;
            this.bConfirm.RoundedCorners.TopLeft = true;
            this.bConfirm.RoundedCorners.TopRight = true;
            this.bConfirm.RoundingDiameter = 15;
            this.bConfirm.Size = new System.Drawing.Size(95, 60);
            this.bConfirm.TabIndex = 16;
            this.bConfirm.TabStop = false;
            this.bConfirm.Text = "Potvrdit";
            this.bConfirm.Click += new System.EventHandler(this.ConfirmProduct);
            // 
            // bMinus
            // 
            this.bMinus.BorderColor = System.Drawing.Color.Black;
            this.bMinus.BorderWidth = 2;
            this.bMinus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bMinus.FillColorBottom = System.Drawing.Color.Yellow;
            this.bMinus.FillColorTop = System.Drawing.Color.Yellow;
            this.bMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bMinus.Image = null;
            this.bMinus.Location = new System.Drawing.Point(601, 449);
            this.bMinus.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.bMinus.Name = "bMinus";
            this.bMinus.RoundedCorners.BottomLeft = true;
            this.bMinus.RoundedCorners.BottomRight = true;
            this.bMinus.RoundedCorners.TopLeft = true;
            this.bMinus.RoundedCorners.TopRight = true;
            this.bMinus.RoundingDiameter = 20;
            this.bMinus.Size = new System.Drawing.Size(101, 60);
            this.bMinus.TabIndex = 53;
            this.bMinus.TabStop = false;
            this.bMinus.Text = "minus";
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // tbMenu
            // 
            this.tbMenu.BorderColor = System.Drawing.Color.Black;
            this.tbMenu.BorderWidth = 2;
            this.tbMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbMenu.FillColorBottom = System.Drawing.Color.PaleGoldenrod;
            this.tbMenu.FillColorTop = System.Drawing.Color.PaleGoldenrod;
            this.tbMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbMenu.ForeColor = System.Drawing.Color.Black;
            this.tbMenu.Image = null;
            this.tbMenu.Location = new System.Drawing.Point(12, 673);
            this.tbMenu.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbMenu.Name = "tbMenu";
            this.tbMenu.RoundedCorners.BottomLeft = true;
            this.tbMenu.RoundedCorners.BottomRight = true;
            this.tbMenu.RoundedCorners.TopLeft = true;
            this.tbMenu.RoundedCorners.TopRight = true;
            this.tbMenu.RoundingDiameter = 20;
            this.tbMenu.Size = new System.Drawing.Size(136, 79);
            this.tbMenu.TabIndex = 52;
            this.tbMenu.TabStop = false;
            this.tbMenu.Text = "Menu";
            this.tbMenu.Click += new System.EventHandler(this.tbMenu_Click);
            // 
            // bCancel
            // 
            this.bCancel.BorderColor = System.Drawing.Color.Black;
            this.bCancel.BorderWidth = 2;
            this.bCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancel.FillColorBottom = System.Drawing.Color.Red;
            this.bCancel.FillColorTop = System.Drawing.Color.Red;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancel.ForeColor = System.Drawing.Color.White;
            this.bCancel.Image = null;
            this.bCancel.Location = new System.Drawing.Point(601, 385);
            this.bCancel.Margin = new System.Windows.Forms.Padding(6);
            this.bCancel.Name = "bCancel";
            this.bCancel.RoundedCorners.BottomLeft = true;
            this.bCancel.RoundedCorners.BottomRight = true;
            this.bCancel.RoundedCorners.TopLeft = true;
            this.bCancel.RoundedCorners.TopRight = true;
            this.bCancel.RoundingDiameter = 20;
            this.bCancel.Size = new System.Drawing.Size(101, 60);
            this.bCancel.TabIndex = 51;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Celkové Storno";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bCancelItem
            // 
            this.bCancelItem.BorderColor = System.Drawing.Color.Black;
            this.bCancelItem.BorderWidth = 2;
            this.bCancelItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bCancelItem.FillColorBottom = System.Drawing.Color.Red;
            this.bCancelItem.FillColorTop = System.Drawing.Color.Red;
            this.bCancelItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCancelItem.ForeColor = System.Drawing.Color.White;
            this.bCancelItem.Image = null;
            this.bCancelItem.Location = new System.Drawing.Point(601, 321);
            this.bCancelItem.Margin = new System.Windows.Forms.Padding(6);
            this.bCancelItem.Name = "bCancelItem";
            this.bCancelItem.RoundedCorners.BottomLeft = true;
            this.bCancelItem.RoundedCorners.BottomRight = true;
            this.bCancelItem.RoundedCorners.TopLeft = true;
            this.bCancelItem.RoundedCorners.TopRight = true;
            this.bCancelItem.RoundingDiameter = 20;
            this.bCancelItem.Size = new System.Drawing.Size(101, 60);
            this.bCancelItem.TabIndex = 50;
            this.bCancelItem.TabStop = false;
            this.bCancelItem.Text = "Storno Položky";
            this.bCancelItem.Click += new System.EventHandler(this.bCancelItem_Click);
            // 
            // bPayment
            // 
            this.bPayment.BorderColor = System.Drawing.Color.Black;
            this.bPayment.BorderWidth = 2;
            this.bPayment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bPayment.FillColorBottom = System.Drawing.Color.Chartreuse;
            this.bPayment.FillColorTop = System.Drawing.Color.Chartreuse;
            this.bPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bPayment.Image = null;
            this.bPayment.Location = new System.Drawing.Point(871, 673);
            this.bPayment.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bPayment.Name = "bPayment";
            this.bPayment.RoundedCorners.BottomLeft = true;
            this.bPayment.RoundedCorners.BottomRight = true;
            this.bPayment.RoundedCorners.TopLeft = true;
            this.bPayment.RoundedCorners.TopRight = true;
            this.bPayment.RoundingDiameter = 20;
            this.bPayment.Size = new System.Drawing.Size(136, 79);
            this.bPayment.TabIndex = 42;
            this.bPayment.TabStop = false;
            this.bPayment.Text = "Total";
            this.bPayment.Click += new System.EventHandler(this.bPayment_Click);
            // 
            // bSelectProduct
            // 
            this.bSelectProduct.BorderColor = System.Drawing.Color.Black;
            this.bSelectProduct.BorderWidth = 2;
            this.bSelectProduct.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelectProduct.FillColorBottom = System.Drawing.Color.Magenta;
            this.bSelectProduct.FillColorTop = System.Drawing.Color.Magenta;
            this.bSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelectProduct.ForeColor = System.Drawing.Color.Black;
            this.bSelectProduct.Image = null;
            this.bSelectProduct.Location = new System.Drawing.Point(497, 80);
            this.bSelectProduct.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bSelectProduct.Name = "bSelectProduct";
            this.bSelectProduct.RoundedCorners.BottomLeft = true;
            this.bSelectProduct.RoundedCorners.BottomRight = true;
            this.bSelectProduct.RoundedCorners.TopLeft = true;
            this.bSelectProduct.RoundedCorners.TopRight = true;
            this.bSelectProduct.RoundingDiameter = 20;
            this.bSelectProduct.Size = new System.Drawing.Size(95, 60);
            this.bSelectProduct.TabIndex = 38;
            this.bSelectProduct.TabStop = false;
            this.bSelectProduct.Text = "Sortiment";
            this.bSelectProduct.Click += new System.EventHandler(this.bSelectProduct_Click);
            // 
            // bSelectCustomer
            // 
            this.bSelectCustomer.BorderColor = System.Drawing.Color.Black;
            this.bSelectCustomer.BorderWidth = 2;
            this.bSelectCustomer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bSelectCustomer.FillColorBottom = System.Drawing.Color.Magenta;
            this.bSelectCustomer.FillColorTop = System.Drawing.Color.Magenta;
            this.bSelectCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSelectCustomer.ForeColor = System.Drawing.Color.Black;
            this.bSelectCustomer.Image = null;
            this.bSelectCustomer.Location = new System.Drawing.Point(497, 16);
            this.bSelectCustomer.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bSelectCustomer.Name = "bSelectCustomer";
            this.bSelectCustomer.RoundedCorners.BottomLeft = true;
            this.bSelectCustomer.RoundedCorners.BottomRight = true;
            this.bSelectCustomer.RoundedCorners.TopLeft = true;
            this.bSelectCustomer.RoundedCorners.TopRight = true;
            this.bSelectCustomer.RoundingDiameter = 20;
            this.bSelectCustomer.Size = new System.Drawing.Size(95, 60);
            this.bSelectCustomer.TabIndex = 35;
            this.bSelectCustomer.TabStop = false;
            this.bSelectCustomer.Text = "Vybrat odběratele";
            this.bSelectCustomer.Click += new System.EventHandler(this.bSelectCustomer_Click);
            // 
            // tbPriceChange
            // 
            this.tbPriceChange.BorderColor = System.Drawing.Color.Black;
            this.tbPriceChange.BorderWidth = 2;
            this.tbPriceChange.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbPriceChange.FillColorBottom = System.Drawing.Color.DarkOliveGreen;
            this.tbPriceChange.FillColorTop = System.Drawing.Color.DarkOliveGreen;
            this.tbPriceChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPriceChange.ForeColor = System.Drawing.Color.White;
            this.tbPriceChange.Image = null;
            this.tbPriceChange.Location = new System.Drawing.Point(601, 513);
            this.tbPriceChange.Margin = new System.Windows.Forms.Padding(6);
            this.tbPriceChange.Name = "tbPriceChange";
            this.tbPriceChange.RoundedCorners.BottomLeft = true;
            this.tbPriceChange.RoundedCorners.BottomRight = true;
            this.tbPriceChange.RoundedCorners.TopLeft = true;
            this.tbPriceChange.RoundedCorners.TopRight = true;
            this.tbPriceChange.RoundingDiameter = 20;
            this.tbPriceChange.Size = new System.Drawing.Size(101, 60);
            this.tbPriceChange.TabIndex = 54;
            this.tbPriceChange.TabStop = false;
            this.tbPriceChange.Text = "Sleva - změna ceny";
            this.tbPriceChange.Click += new System.EventHandler(this.tbPriceChange_Click);
            // 
            // tbExit
            // 
            this.tbExit.BorderColor = System.Drawing.Color.Black;
            this.tbExit.BorderWidth = 2;
            this.tbExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbExit.FillColorBottom = System.Drawing.Color.Black;
            this.tbExit.FillColorTop = System.Drawing.Color.Black;
            this.tbExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbExit.ForeColor = System.Drawing.Color.White;
            this.tbExit.Image = null;
            this.tbExit.Location = new System.Drawing.Point(406, 684);
            this.tbExit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbExit.Name = "tbExit";
            this.tbExit.RoundedCorners.BottomLeft = true;
            this.tbExit.RoundedCorners.BottomRight = true;
            this.tbExit.RoundedCorners.TopLeft = true;
            this.tbExit.RoundedCorners.TopRight = true;
            this.tbExit.RoundingDiameter = 20;
            this.tbExit.Size = new System.Drawing.Size(203, 57);
            this.tbExit.TabIndex = 65;
            this.tbExit.TabStop = false;
            this.tbExit.Text = "ODHLÁSIT";
            this.tbExit.Click += new System.EventHandler(this.tbExit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 786);
            this.Controls.Add(this.tbExit);
            this.Controls.Add(this.tbPriceChange);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.tbMenu);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bCancelItem);
            this.Controls.Add(this.pKeys);
            this.Controls.Add(this.bPayment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.lvItems);
            this.Controls.Add(this.bSelectProduct);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbProduct);
            this.Controls.Add(this.bSelectCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCustomer);
            this.Controls.Add(this.pShortCuts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1024, 786);
            this.MinimumSize = new System.Drawing.Size(1024, 786);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).EndInit();
            this.pShortCuts.ResumeLayout(false);
            this.pKeys.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.TouchButton b7;
        private Controls.TouchButton b9;
        private Controls.TouchButton b8;
        private Controls.TouchButton b6;
        private Controls.TouchButton b5;
        private Controls.TouchButton b4;
        private Controls.TouchButton b1;
        private Controls.TouchButton b2;
        private Controls.TouchButton b3;
        private Controls.TouchButton bTimes;
        private Controls.TouchButton b0;
        private Controls.TouchButton bDot;
        private Controls.TouchButton bConfirm;
        private Controls.TouchButton bBackspace;
        private Controls.TouchButton bDel;
        private Controls.TouchButton bItem1;
        private Controls.TouchButton bItem2;
        private Controls.TouchButton bItem3;
        private System.Windows.Forms.TextBox tbCustomer;
        private System.Windows.Forms.Label label1;
        private Controls.TouchButton bSelectCustomer;
        private Controls.TouchButton bSelectProduct;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbProduct;
        private BrightIdeasSoftware.ObjectListView lvItems;
        private BrightIdeasSoftware.OLVColumn olvcCode;
        private BrightIdeasSoftware.OLVColumn olvcName;
        private BrightIdeasSoftware.OLVColumn olvcAmount;
        private BrightIdeasSoftware.OLVColumn olvcTotalPrice;
        private BrightIdeasSoftware.HeaderFormatStyle headerFormatStyle1;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label label3;
        private Controls.TouchButton bPayment;
        private System.Windows.Forms.Panel pShortCuts;
        private System.Windows.Forms.Panel pKeys;
        private Controls.TouchButton bItem16;
        private Controls.TouchButton bItem17;
        private Controls.TouchButton bItem18;
        private Controls.TouchButton bItem13;
        private Controls.TouchButton bItem14;
        private Controls.TouchButton bItem15;
        private Controls.TouchButton bItem10;
        private Controls.TouchButton bItem11;
        private Controls.TouchButton bItem12;
        private Controls.TouchButton bItem7;
        private Controls.TouchButton bItem8;
        private Controls.TouchButton bItem9;
        private Controls.TouchButton bItem4;
        private Controls.TouchButton bItem5;
        private Controls.TouchButton bItem6;
        private Controls.TouchButton bCancelItem;
        private Controls.TouchButton bCancel;
        private Controls.TouchButton tbMenu;
        private Controls.TouchButton bLeft;
        private Controls.TouchButton bRight;
        private System.Windows.Forms.Label lPage;
        private Controls.TouchButton bMinus;
        private BrightIdeasSoftware.OLVColumn olvcPrice;
        private Controls.TouchButton tbPriceChange;
        private Controls.TouchButton tbExit;

    }
}

