﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Util;
using System.Runtime.InteropServices;
using Microdata.Singletons;

namespace Microdata.Forms
{
    
    public partial class BasicForm : Form
    {
        #region InteropServices fields and extern methods

        private const int SC_CLOSE = 0xF060;
        private const int MF_GRAYED = 0x1;
        private const int MF_ENABLED = 0x0;

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll")]
        private static extern int EnableMenuItem(IntPtr hMenu, int wIDEnableItem, int wEnable);

        #endregion 

        #region Fields

        protected EFormState status = EFormState.Basic;
        protected ToolStripItem menuItem = null;

        #endregion

        #region Properties

        public EFormState Status
        {
            get { return status; }
            set
            {
                status = value;
                FormStatusChanged();
            }
        }

        public bool StatusBasic { get { return status == EFormState.Basic; } }
        public bool StatusEdit { get { return status == EFormState.Edit; } }

        public virtual EFormButtons EnabledButtonsBasicState
        {
            get
            {
                return EFormButtons.New | EFormButtons.Close;
            }
        }

        public virtual EFormButtons EnabledButtonsEditState
        {
            get
            {
                return EFormButtons.Save | EFormButtons.Cancel;
            }
        }

        public virtual string NewText
        {
            get { return "Vytvořit nový záznam"; }
        }

        public virtual string EditText
        {
            get { return "Opravit záznam"; }
        }

        public virtual string DeleteText
        {
            get { return "Smazat záznam"; }
        }

        public virtual string SaveText
        {
            get { return "Uložit záznam"; }
        }

        public virtual string CancelText
        {
            get { return "Stornovat provedené změny"; }
        }

        public virtual string DetailText
        {
            get { return "Zobrazit vybraný záznam"; }
        }

        public virtual string FindText
        {
            get { return "Vyhledat záznamy"; }
        }

        public virtual string FirstText
        {
            get { return "Přejít na první záznam"; }
        }

        public virtual string PreviousText
        {
            get { return "Přejít na předchozí záznam"; }
        }

        public virtual string NextText
        {
            get { return "Přejít na následující záznam"; }
        }

        public virtual string LastText
        {
            get { return "Přejít na poslední záznam"; }
        }

        public virtual string PrintText
        {
            get { return "Tisk"; }
        }

        public virtual string PreviewText
        {
            get { return "Náhled"; }
        }

        public virtual string PrintSettingText
        {
            get { return "Nastavení tisku"; }
        }

        public virtual string SelectText
        {
            get { return "Vybrat aktuální záznam"; }
        }

        public virtual string CloseText
        {
            get { return "Uzavřít okno"; }
        }

        public virtual ToolStripItem MenuItem
        {
            get
            {
                return menuItem;
            }
            set
            {
                menuItem = value;
                if (menuItem != null)
                    menuItem.Enabled = false;
            }
        }

        public virtual bool ForbidSaveOnEnter {get;set;}
        public virtual bool PenalizeSelect {get;set;}
        public virtual bool Initialized { get; set; }
        #endregion

        #region Constructor

        public BasicForm()
        {
            Initialized = false;

            InitializeComponent();

            tsbNew.Text = tsbNew.ToolTipText = NewText;
            tsbEdit.Text = tsbEdit.ToolTipText = EditText;
            tsbDelete.Text = tsbDelete.ToolTipText = DeleteText;
            tsbSave.Text = tsbSave.ToolTipText = SaveText;
            tsbCancel.Text = tsbCancel.ToolTipText = CancelText;
            tsbDetail.Text = tsbDetail.ToolTipText = DetailText;
            tsbFind.Text = tsbFind.ToolTipText = FindText;
            tsbFirst.Text = tsbFirst.ToolTipText = FirstText;
            tsbPrev.Text = tsbPrev.ToolTipText = PreviousText;
            tsbNext.Text = tsbNext.ToolTipText = NextText;
            tsbLast.Text = tsbLast.ToolTipText = LastText;
            tsbPrint.Text = tsbPrint.ToolTipText = PrintText;
            tsbPreview.Text = tsbPreview.ToolTipText = PreviewText;
            tsbPrintSetting.Text = tsbPrintSetting.ToolTipText = PrintSettingText;
            tsbSelect.Text = tsbSelect.ToolTipText = SelectText;
            tsbClose.Text = tsbClose.ToolTipText = CloseText;     
       
            CloseButtonEnabledChanged(this, null);
        }

        #endregion

        #region Using of InteropServices

        private void CloseButtonEnabledChanged(object sender, EventArgs e)
        {
            EnableMenuItem(GetSystemMenu(this.Handle, false), SC_CLOSE, (tsbClose.Enabled) ? MF_ENABLED : MF_GRAYED);
        }

        #endregion

        #region Private methods

        private void MainToolbarClick(object sender, EventArgs e)
        {
            ToolStripItem tsi = (ToolStripItem)sender;
            if (tsi == tsbNew) Create();
            if (tsi == tsbEdit) Edit();
            if (tsi == tsbDelete) Delete();
            if (tsi == tsbSave) Save();
            if (tsi == tsbCancel) Cancel();
            if (tsi == tsbDetail) Detail();
            if (tsi == tsbFind) Find();
            if (tsi == tsbFirst) First();
            if (tsi == tsbPrev) Previous();
            if (tsi == tsbNext) Next();
            if (tsi == tsbLast) Last();
            if (tsi == tsbPrint) Print();
            if (tsi == tsbPreview) Preview();
            if (tsi == tsbPrintSetting) PrintSetting();
            if (tsi == tsbSelect) SelectItem();
            if (tsi == tsbClose) CloseForm();
        }

        private void FormStatusChanged()
        {
            EFormButtons buttons = (Status == EFormState.Basic) ? EnabledButtonsBasicState : EnabledButtonsEditState;

            tsbNew.Enabled = (buttons & EFormButtons.New) == EFormButtons.New;
            tsbEdit.Enabled = (buttons & EFormButtons.Edit) == EFormButtons.Edit;
            tsbDelete.Enabled = (buttons & EFormButtons.Delete) == EFormButtons.Delete;
            tsbSave.Enabled = (buttons & EFormButtons.Save) == EFormButtons.Save;
            tsbCancel.Enabled = (buttons & EFormButtons.Cancel) == EFormButtons.Cancel;
            tsbDetail.Enabled = (buttons & EFormButtons.Detail) == EFormButtons.Detail;
            tsbFind.Enabled = (buttons & EFormButtons.Find) == EFormButtons.Find;
            tsbFirst.Enabled = (buttons & EFormButtons.First) == EFormButtons.First;
            tsbPrev.Enabled = (buttons & EFormButtons.Previous) == EFormButtons.Previous;
            tsbNext.Enabled = (buttons & EFormButtons.Next) == EFormButtons.Next;
            tsbLast.Enabled = (buttons & EFormButtons.Last) == EFormButtons.Last;
            tsbPrint.Enabled = (buttons & EFormButtons.Print) == EFormButtons.Print;
            tsbPreview.Enabled = (buttons & EFormButtons.Preview) == EFormButtons.Preview;
            tsbPrintSetting.Enabled = (buttons & EFormButtons.PrintSetting) == EFormButtons.PrintSetting;
            tsbSelect.Enabled = (buttons & EFormButtons.Select) == EFormButtons.Select;
            tsbClose.Enabled = (buttons & EFormButtons.Close) == EFormButtons.Close;

            FormStatusChanged(status);
        }

        #endregion

        #region Protected virtual methods

        protected virtual void FormStatusChanged(EFormState status)
        {
        }

        protected virtual bool Create()
        {
            return true;
        }

        protected virtual bool Edit()
        {
            return true;
        }

        protected virtual bool Delete()
        {
            if (MessageBox.Show("Opravdu chete odstranit položku?", "Odstranit položku", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                return true;
            return false;
        }

        protected virtual bool Save()
        {
            return true;
        }

        protected virtual bool Cancel()
        {
            if (MessageBox.Show("Opravdu chete stornovat provedené změny?", "Stornovat změny", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                return true;
            return false;
        }

        protected virtual bool Detail()
        {
            return true;
        }

        protected virtual bool Find()
        {
            return true;
        }

        protected virtual bool First()
        {
            return true;
        }

        protected virtual bool Previous()
        {
            return true;
        }

        protected virtual bool Next()
        {
            return true;
        }

        protected virtual bool Last()
        {
            return true;
        }

        protected virtual bool Print()
        {
            return true;
        }

        protected virtual bool Preview()
        {
            return true;
        }

        protected virtual bool PrintSetting()
        {
            return true;
        }

        protected virtual bool SelectItem()
        {
            return true;
        }

        protected virtual bool CloseForm()
        {
            Close();
            return true;
        }

        protected void Initialize()
        {
            LoadSettings();

            FormStatusChanged();

            Initialized = true;
        }
        
        protected virtual void LoadSettings() { }
        protected virtual void SaveSettings() { }

        #endregion

        #region Event for basic window

        private void BasicForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Handled)
                return;

            bool ALT = e.Alt;
            bool CTRL = e.Control;
            bool SHIFT = e.Shift;
            char key = KeyboardUtil.GetKey(e.KeyCode);
            if (e.Control)
            {
                switch (key)
                {
                    case 'N': if (tsbNew.Enabled == true) Create(); break;
                    case 'O': if (tsbEdit.Enabled == true) Edit(); break;
                    case 'D': if (tsbDelete.Enabled == true) Delete(); break;
                    case 'S': if (tsbSave.Enabled == true) Save(); break;
                    case 'P': if (tsbPrint.Enabled == true) Print(); break;
                }
            }
            if (e.KeyCode == Keys.Enter)
            {
                if (Status == EFormState.Edit && tsbSave.Enabled && !ForbidSaveOnEnter)
                {
                    // some components need anter for their specific use
                    if ((ActiveControl is ComboBox && ((ComboBox)ActiveControl).DroppedDown)
                        || (ActiveControl is TextBox && ((TextBox)ActiveControl).Multiline))
                    {
                        // save is done only when Ctrl is pressed
                        if (CTRL)
                        {
                            Save();
                            e.SuppressKeyPress = true;
                        }
                    }
                    else
                    {
                        Save();
                        e.SuppressKeyPress = true;
                    }
                }
                else
                    if (PenalizeSelect)
                    {
                        if (Status == EFormState.Basic && tsbDetail.Enabled)
                            Detail();
                    }
                    else
                    {
                        if (Status == EFormState.Basic && tsbSelect.Enabled)
                            SelectItem();
                        else
                            if (Status == EFormState.Basic && tsbDetail.Enabled)
                                Detail();
                            else if (Status == EFormState.Basic && tsbEdit.Enabled)
                                Edit();
                    }
            }
            if (e.KeyCode == Keys.Escape)
            {
                if (Status == EFormState.Edit && tsbCancel.Enabled)
                    Cancel();
                else
                    if (Status == EFormState.Basic && tsbClose.Enabled)
                        CloseForm();
            }
            if (e.KeyCode == Keys.Delete)
            {
                if (Status == EFormState.Basic && tsbDelete.Enabled)
                    Delete();
            }
        }

        private void BasicForm_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                int L = Left;
                int T = Top;
                int W = Width;
                int H = Height;
                RegistryClass MyRK = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);
                if (MyRK.GetWindowsPosition(ref L, ref T, ref W, ref H))
                {
                    if ((L + T + W + H) > 0) { Left = L; Top = T; Width = W; Height = H; }
                    else { WindowState = FormWindowState.Maximized; }
                    Rectangle sr = Screen.PrimaryScreen.WorkingArea;
                    if (H > sr.Height)
                        WindowState = FormWindowState.Maximized;
                }
                
                if (!Initialized)
                    Initialize();
            }
        }

        private void BasicForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MenuItem != null) MenuItem.Enabled = true;

            if (!DesignMode)
            {
                RegistryClass MyRK = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);
                if (this.WindowState == FormWindowState.Maximized) MyRK.SetWindowsPosition(0, 0, 0, 0);
                else MyRK.SetWindowsPosition(this.Left, this.Top, this.Width, this.Height);
                SaveSettings();
            }
        }        

        #endregion

        #region Public methods

        public void EnableButtons(EFormButtons buttons)
        {
            if ((buttons & EFormButtons.New) == EFormButtons.New)
                tsbNew.Enabled = true;
            if ((buttons & EFormButtons.Edit) == EFormButtons.Edit)
                tsbEdit.Enabled = true;
            if ((buttons & EFormButtons.Delete) == EFormButtons.Delete)
                tsbDelete.Enabled = true;
            if ((buttons & EFormButtons.Save) == EFormButtons.Save)
                tsbSave.Enabled = true;
            if ((buttons & EFormButtons.Cancel) == EFormButtons.Cancel)
                tsbCancel.Enabled = true;
            if ((buttons & EFormButtons.Detail) == EFormButtons.Detail)
                tsbDetail.Enabled = true;
            if ((buttons & EFormButtons.Find) == EFormButtons.Find)
                tsbFind.Enabled = true;
            if ((buttons & EFormButtons.First) == EFormButtons.First)
                tsbFirst.Enabled = true;
            if ((buttons & EFormButtons.Previous) == EFormButtons.Previous)
                tsbPrev.Enabled = true;
            if ((buttons & EFormButtons.Next) == EFormButtons.Next)
                tsbNext.Enabled = true;
            if ((buttons & EFormButtons.Last) == EFormButtons.Last)
                tsbLast.Enabled = true;
            if ((buttons & EFormButtons.Print) == EFormButtons.Print)
                tsbPrint.Enabled = true;
            if ((buttons & EFormButtons.Preview) == EFormButtons.Preview)
                tsbPreview.Enabled = true;
            if ((buttons & EFormButtons.PrintSetting) == EFormButtons.PrintSetting)
                tsbPrintSetting.Enabled = true;
            if ((buttons & EFormButtons.Select) == EFormButtons.Select)
                tsbSelect.Enabled = true;
            if ((buttons & EFormButtons.Close) == EFormButtons.Close)
                tsbClose.Enabled = true;
        }

        public void DisableButtons(EFormButtons buttons)
        {
            if ((buttons & EFormButtons.New) == EFormButtons.New)
                tsbNew.Enabled = false;
            if ((buttons & EFormButtons.Edit) == EFormButtons.Edit)
                tsbEdit.Enabled = false;
            if ((buttons & EFormButtons.Delete) == EFormButtons.Delete)
                tsbDelete.Enabled = false;
            if ((buttons & EFormButtons.Save) == EFormButtons.Save)
                tsbSave.Enabled = false;
            if ((buttons & EFormButtons.Cancel) == EFormButtons.Cancel)
                tsbCancel.Enabled = false;
            if ((buttons & EFormButtons.Detail) == EFormButtons.Detail)
                tsbDetail.Enabled = false;
            if ((buttons & EFormButtons.Find) == EFormButtons.Find)
                tsbFind.Enabled = false;
            if ((buttons & EFormButtons.First) == EFormButtons.First)
                tsbFirst.Enabled = false;
            if ((buttons & EFormButtons.Previous) == EFormButtons.Previous)
                tsbPrev.Enabled = false;
            if ((buttons & EFormButtons.Next) == EFormButtons.Next)
                tsbNext.Enabled = false;
            if ((buttons & EFormButtons.Last) == EFormButtons.Last)
                tsbLast.Enabled = false;
            if ((buttons & EFormButtons.Print) == EFormButtons.Print)
                tsbPrint.Enabled = false;
            if ((buttons & EFormButtons.Preview) == EFormButtons.Preview)
                tsbPreview.Enabled = false;
            if ((buttons & EFormButtons.PrintSetting) == EFormButtons.PrintSetting)
                tsbPrintSetting.Enabled = false;
            if ((buttons & EFormButtons.Select) == EFormButtons.Select)
                tsbSelect.Enabled = false;
            if ((buttons & EFormButtons.Close) == EFormButtons.Close)
                tsbClose.Enabled = false;
        }

        #endregion

        #region Protected Check methods

        protected bool CheckInt(string str, string message)
        {
            int v;

            if (!int.TryParse(str, out v))
            {
                MessageBox.Show(message);
                return false;
            }
            return true;
        }

        protected bool CheckDecimal(string str, string message)
        {
            decimal v;

            if (!decimal.TryParse(str, out v))
            {
                MessageBox.Show(message);
                return false;
            }
            return true;
        }

        protected bool CheckStringEmtpyLength(string str, int length, string emptyMessage, string longerMessage)
        {
            if (string.IsNullOrEmpty(str))
            {
                MessageBox.Show(emptyMessage);
                return false;
            }
            if (str.Length > length)
            {
                MessageBox.Show(longerMessage);
                return false;
            }
            return true;
        }

        protected bool CheckStringEmtpy(string str, string emptyMessage)
        {
            if (string.IsNullOrEmpty(str))
            {
                MessageBox.Show(emptyMessage);
                return false;
            }
            
            return true;
        }

        protected bool CheckStringLength(string str, int length, string longerMessage)
        {
            if (str.Length > length)
            {
                MessageBox.Show(longerMessage);
                return false;
            }
            return true;
        }

        #endregion

    }

    public enum EFormState { Basic, Edit };

    [Flags]
    public enum EFormButtons
    {
        None = 0,
        New = 1,
        Edit = 2,
        Delete = 4,
        Save = 8,
        Cancel = 16,
        Detail = 32,
        Find = 64,
        First = 128,
        Previous = 256,
        Next = 512,
        Last = 1024,
        Print = 2048,
        Preview = 4096,
        PrintSetting = 8192,
        Select = 16384,
        Close = 32768
    };

}
