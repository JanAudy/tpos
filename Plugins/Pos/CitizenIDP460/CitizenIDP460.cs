﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Plugins.Pos;
using System.Xml;

namespace CitizenIDP460Plugin
{
    public class CitizenIDP460 : BasicPosPrinter
    {
        public override string PrinterName
        {
            get { return "Citizen iDP460"; }
        }

        public override int PageWidth
        {
            get
            {
                return 40;
            }
        }

        protected override int SerialPortLineDelay
        {
            get
            {
                return 400;
            }
        }

        protected override int SerialPortFinalDelay
        {
            get
            {
                return 200;
            }
        }

        protected string CenterLine(string data, int lineWidth)
        {
            StringBuilder sb = new StringBuilder();
            int len = lineWidth-data.Length;
            if (len > 0)
            {
                sb.Append(' ', len / 2);
                len -= len / 2;
                sb.Append(data);
                sb.Append(' ', len);
            }
            else
            {
                sb.Append(data);
            }

            return sb.ToString();
        }

        protected string RightAlignLine(string data, int lineWidth)
        {
            StringBuilder sb = new StringBuilder();
            int len = lineWidth - data.Length;
            if (len > 0)
            {
                sb.Append(' ', len);
                sb.Append(data);
            }
            else
            {
                sb.Append(data);
            }

            return sb.ToString();
        }


        public override bool InitializePrint(List<byte[]> bytes)
        {
            // clear print and cancel character attribute
            byte[] buf = new byte[] { 0x18 };
            bytes.Add(buf);

            return true;
        }

        public override bool FinalizePrint(List<byte[]> bytes)
        {
            // eject paper
            byte[] buf = new byte[] { 0x1b, 0x62, 0x35, 0x3b };
            bytes.Add(buf);

            return true;
        }

        public override bool ProcessTitle(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            string data = element.ChildNodes[0].Value;

            // set the double width
            line.Add(0x1e);

            data = CenterLine(data, PageWidth/2);
            data = DiacriticsConversion.RemoveDiacritics(data);
            line.AddRange(Encoding.ASCII.GetBytes(data));

            // set the normal width
            line.Add(0x1f);

            // add new line
            line.Add(0x0a);
            
            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessLine(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            int lineWidth = PageWidth;
            string data = element.ChildNodes[0].Value;
            data = DiacriticsConversion.RemoveDiacritics(data);

            foreach (XmlAttribute attribute in element.Attributes)
            {
                if (attribute.Name == "style")
                {
                    switch (attribute.Value)
                    {
                        case "bold":
                            line.Add(0x1e);
                            lineWidth /= 2;
                            break;
                        case "normal":
                            // reset the font type to plain fontA
                            line.Add(0x1f);
                            break;
                    }
                }

                if (attribute.Name == "align")
                {
                    switch (attribute.Value)
                    {
                        case "left":  break;
                        case "center": data = CenterLine(data, lineWidth); break;
                        case "right": data = RightAlignLine(data, lineWidth); break;
                    }
                }
                
            }

            line.AddRange(Encoding.ASCII.GetBytes(data));

            line.Add(0x1f);
            // add new line
            line.Add(0x0a);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessSeparator(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            for (int i = 0; i < PageWidth; i++)
                line.Add((byte)'-');

            // add new line
            line.Add(0x0a);

            bytes.Add(line.ToArray());
            return true;
        }

    }
}
