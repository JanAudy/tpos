﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Plugins.Pos;
using System.Xml;

namespace StarTSP100
{
    public class StarTSP100 : BasicPosPrinter
    {
        public override string PrinterName
        {
            get { return "Star TSP 100"; }
        }

        protected override int SerialPortLineDelay
        {
            get
            {
                return 100;
            }
        }

        public override bool SupportBarcodes
        {
            get
            {
                return false;
            }
        }

        public override bool SupportDiacritics
        {
            get
            {
                return false;
            }
        }

        //public override bool SupportGraphics
        //{
        //    get
        //    {
        //        return true;
        //    }
        //}

        //public override int ImageMaxWidth
        //{
        //    get { return 576; }
        //}

        //public override int ImageMaxHeight
        //{
        //    get { return 384; }
        //}

        //public override int ImageWidthAligment
        //{
        //    get { return 1; }
        //}

        public override bool InitializePrint(List<byte[]> bytes)
        {
            byte[] buf = new byte[] { }; 
            bytes.Add(buf);
            return true;
        }

        public override bool FinalizePrint(List<byte[]> bytes)
        {
            // cash drawer
            byte[] buf = new byte[] { 0x0a, 0x1b, 0x64, 0x02, 0x7 };
            bytes.Add(buf);
            return true;
        }

        public override bool ProcessTitle(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();
            //double height highlighted font
            line.Add(0x1b);
            line.Add(0x69);
            line.Add(0x1);
            line.Add(0x1);

            // center aligment
            line.Add(0x1b);
            line.Add(0x1d);
            line.Add(0x61);
            line.Add(1);

            line.AddRange(Encoding.ASCII.GetBytes(DiacriticsConversion.RemoveDiacritics(element.ChildNodes[0].Value)));
            //line.AddRange(Encoding.GetEncoding(1250).GetBytes(element.ChildNodes[0].Value));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontA
            line.Add(0x1b);
            line.Add(0x46);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x1d);
            line.Add(0x61);
            line.Add(0);


            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessLine(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            foreach (XmlAttribute attribute in element.Attributes)
            {
                if (attribute.Name == "align")
                {
                    // set aligment
                    line.Add(0x1b);
                    line.Add(0x1d);
                    line.Add(0x61);
                    switch (attribute.Value)
                    {
                        case "left": line.Add(0); break;
                        case "center": line.Add(1); break;
                        case "right": line.Add(2); break;
                    }
                }
                if (attribute.Name == "style")
                {
                    switch (attribute.Value)
                    {
                        case "bold":
                            // set style of font
                            line.Add(0x1b);
                            line.Add(0x45);
                            break;
                        case "normal":
                            // reset the font type to plain fontA
                            line.Add(0x1b);
                            line.Add(0x46);
                            break;
                    }
                }
            }

            line.AddRange(Encoding.ASCII.GetBytes(DiacriticsConversion.RemoveDiacritics(element.ChildNodes[0].Value)));
            //line.AddRange(Encoding.GetEncoding(1250).GetBytes(element.ChildNodes[0].Value));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontA
            line.Add(0x1b);
            line.Add(0x46);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x1d);
            line.Add(0x61);
            line.Add(0);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessSeparator(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            for (int i = 0; i < PageWidth; i++)
                line.Add((byte)'-');

            // add new line
            line.Add(0x0a);

            bytes.Add(line.ToArray());
            return true;
        }

        //public override bool ProcessBarcode(XmlElement element, List<byte[]> bytes)
        //{
        //    //if (element.Attributes.Count == 0 || element.Attributes[0].Name != "type")
        //    //    return false;

        //    //List<byte> line = new List<byte>();

        //    //// set the center aligment
        //    //line.Add(0x1b);
        //    //line.Add(0x61);
        //    //line.Add(1);

        //    //// set position of characters below the line
        //    //line.Add(0x1d);
        //    //line.Add(0x48);
        //    //line.Add(2);

        //    //// set the height of the barcode
        //    //line.Add(0x1d);
        //    //line.Add(0x68);
        //    //line.Add(50);

        //    //// set the barcode type and content
        //    //line.Add(0x1d);
        //    //line.Add(0x6b);
        //    //switch (element.Attributes[0].Value)
        //    //{
        //    //    case "ean8": line.Add(68); break;
        //    //    case "ean13": line.Add(67); break;
        //    //    case "code39": line.Add(69); break;
        //    //    //case "ean128": line.Add(73); break;
        //    //    case "2of5": line.Add(70); break;
        //    //}

        //    //string text = element.ChildNodes[0].Value;
        //    //line.Add((byte)text.Length);
        //    //line.AddRange(Encoding.ASCII.GetBytes(text));

        //    //line.Add(0x0a);

        //    //// set the left aligment
        //    //line.Add(0x1b);
        //    //line.Add(0x61);
        //    //line.Add(0);

        //    //bytes.Add(line.ToArray());
        //    //return true;
        //}

    }
}
