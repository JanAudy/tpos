﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transware.Plugins.Pos;
using System.Xml;

namespace PRP085Plugin
{
    public class PRP085 : BasicPosPrinter
    {
        public override string PrinterName
        {
            get { return "PRP-085"; }
        }

        protected override int SerialPortLineDelay
        {
            get
            {
                return 100;
            }
        }

        public override bool SupportBarcodes
        {
            get
            {
                return true;
            }
        }

        public override bool SupportDiacritics
        {
            get
            {
                return true;
            }
        }

        //public override bool SupportGraphics
        //{
        //    get
        //    {
        //        return true;
        //    }
        //}

        //public override int ImageMaxWidth
        //{
        //    get { return 576; }
        //}

        //public override int ImageMaxHeight
        //{
        //    get { return 384; }
        //}

        //public override int ImageWidthAligment
        //{
        //    get { return 1; }
        //}

        public override bool InitializePrint(List<byte[]> bytes)
        {
            // 0x1b 0x40 initialization of the printer
            // 0x1b 0x4a 10 feed paper by 10 units
            // 0x1b 0x74 6 set code page 852 (Latin2 page)
            byte[] buf = new byte[] { 0x1b, 0x40, 0x1b, 0x4a, 10, 0x1c, 0x70, 1, 0 };
            bytes.Add(buf);
            return true;
        }

        public override bool FinalizePrint(List<byte[]> bytes)
        {
            // 0x0a  blank line
            // 0x1b 0x4a 150 feed paper by 150 units
            // 0x1d 0x56 0 cut the paper
            byte[] buf = new byte[] { 0x0a, 0x1b, 0x4a, 150, 0x1d, 0x56, 0 };
            bytes.Add(buf);
            return true;
        }

        public override bool ProcessTitle(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();
            // set the double height emphazied font A
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x38);

            // set the center aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(1);

            //line.AddRange(Encoding.ASCII.GetBytes(DiacriticsConversion.RemoveDiacritics(element.ChildNodes[0].Value)));
            line.AddRange(Encoding.GetEncoding(1250).GetBytes(element.ChildNodes[0].Value));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontA
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x00);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);


            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessLine(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            foreach (XmlAttribute attribute in element.Attributes)
            {
                if (attribute.Name == "align")
                {
                    // set aligment
                    line.Add(0x1b);
                    line.Add(0x61);
                    switch (attribute.Value)
                    {
                        case "left": line.Add(0); break;
                        case "center": line.Add(1); break;
                        case "right": line.Add(2); break;
                    }
                }
                if (attribute.Name == "style")
                {
                    switch (attribute.Value)
                    {
                        case "bold":
                            // set style of font
                            line.Add(0x1b);
                            line.Add(0x45);
                            line.Add(1);
                            break;
                        case "normal":
                            // reset the font type to plain fontA
                            line.Add(0x1b);
                            line.Add(0x21);
                            line.Add(0x00);
                            break;
                    }
                }
            }

            //line.AddRange(Encoding.ASCII.GetBytes(DiacriticsConversion.RemoveDiacritics(element.ChildNodes[0].Value)));
            line.AddRange(Encoding.GetEncoding(1250).GetBytes(element.ChildNodes[0].Value));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontA
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x00);

            // set aligment to left
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessSeparator(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            for (int i = 0; i < PageWidth; i++)
                line.Add((byte)'-');

            // add new line
            line.Add(0x0a);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessBarcode(XmlElement element, List<byte[]> bytes)
        {
            if (element.Attributes.Count == 0 || element.Attributes[0].Name != "type")
                return false;

            List<byte> line = new List<byte>();

            // set the center aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(1);

            // set position of characters below the line
            line.Add(0x1d);
            line.Add(0x48);
            line.Add(2);

            // set the height of the barcode
            line.Add(0x1d);
            line.Add(0x68);
            line.Add(50);

            // set the barcode type and content
            line.Add(0x1d);
            line.Add(0x6b);
            switch (element.Attributes[0].Value)
            {
                case "ean8": line.Add(68); break;
                case "ean13": line.Add(67); break;
                case "code39": line.Add(69); break;
                //case "ean128": line.Add(73); break;
                case "2of5": line.Add(70); break;
            }

            string text = element.ChildNodes[0].Value;
            line.Add((byte)text.Length);
            line.AddRange(Encoding.ASCII.GetBytes(text));

            line.Add(0x0a);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);

            bytes.Add(line.ToArray());
            return true;
        }


    }
}
