﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Transware.Plugins.Pos;

namespace CitizenCMP10Plugin
{
    public class CitizenCMP10 : BasicPosPrinter
    {
        public override string PrinterName
        {
            get { return "Citizen CMP10"; }
        }

        protected override int SerialPortLineDelay
        {
            get
            {
                return 100;
            }
        }

        public override int PageWidth
        {
            get
            {
                return 42;
            }
        }

        public override bool SupportBarcodes
        {
            get
            {
                return true;
            }
        }

        public override bool SupportDiacritics
        {
            get
            {
                return false;
            }
        }

        public override bool SupportGraphics
        {
            get
            {
                return true;
            }
        }

        public override bool InitializePrint(List<byte[]> bytes)
        {
            // 0x1b 0x40 initialization of the printer
            // 0x1b 0x4a 10 feed paper by 10 units
            // 0x1b 0x21 0x11 set FontB
            byte[] buf = new byte[] { 0x1b, 0x40, 0x1b, 0x4a, 10, 0x1b, 0x21, 0x11 };

            bytes.Add(buf);
            return true;
        }

        public override bool FinalizePrint(List<byte[]> bytes)
        {
            // 0x0a  blank line
            // 0x1b 0x4a 10 feed paper by 10 units
            byte[] buf = new byte[] { 0x0a, 0x1b, 0x4a, 10 };
            bytes.Add(buf);
            return true;
        }

        public override bool ProcessTitle(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();
            //set the double height, double width emphazied font B
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x39);

            // set the center aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(1);

            string data= element.ChildNodes[0].Value;
            data = DiacriticsConversion.RemoveDiacritics(data);
            line.AddRange(Encoding.ASCII.GetBytes(data));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontB
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x01);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);
            
            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessLine(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            foreach (XmlAttribute attribute in element.Attributes)
            {
                if (attribute.Name == "align")
                {
                    // set aligment
                    line.Add(0x1b);
                    line.Add(0x61);
                    switch (attribute.Value)
                    {
                        case "left": line.Add(0); break;
                        case "center": line.Add(1); break;
                        case "right": line.Add(2); break;
                    }
                }
                if (attribute.Name == "style")
                {
                    switch (attribute.Value)
                    {
                        case "bold":
                            // set style of font
                            line.Add(0x1b);
                            line.Add(0x45);
                            line.Add(1);
                            break;
                        case "normal":
                            // reset the font type to plain fontA
                            line.Add(0x1b);
                            line.Add(0x21);
                            line.Add(0x01);
                            break;
                    }
                }
            }

            line.AddRange(Encoding.GetEncoding(852).GetBytes(element.ChildNodes[0].Value));

            // add new line
            line.Add(0x0a);

            // reset the font type to plain fontB
            line.Add(0x1b);
            line.Add(0x21);
            line.Add(0x01);

            // set aligment to left
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessSeparator(XmlElement element, List<byte[]> bytes)
        {
            List<byte> line = new List<byte>();

            for (int i = 0; i < PageWidth; i++)
                line.Add((byte)'-');

            // add new line
            line.Add(0x0a);

            bytes.Add(line.ToArray());
            return true;
        }

        public override bool ProcessBarcode(XmlElement element, List<byte[]> bytes)
        {
            if (element.Attributes.Count == 0 || element.Attributes[0].Name != "type")
                return false;

            List<byte> line = new List<byte>();

            // set the center aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(1);

            // set position of characters below the line
            line.Add(0x1d);
            line.Add(0x48);
            line.Add(2);

            // set the height of the barcode
            line.Add(0x1d);
            line.Add(0x68);
            line.Add(50);

            // set the barcode type and content
            line.Add(0x1d);
            line.Add(0x6b);
            switch (element.Attributes[0].Value)
            {
                case "ean8": line.Add(68); break;
                case "ean13": line.Add(67); break;
                case "code39": line.Add(69); break;
                //case "ean128": line.Add(73); break;
                case "2of5": line.Add(70); break;
            }

            string text = element.ChildNodes[0].Value;
            line.Add((byte)text.Length);
            line.AddRange(Encoding.ASCII.GetBytes(text));

            line.Add(0x0a);

            // set the left aligment
            line.Add(0x1b);
            line.Add(0x61);
            line.Add(0);

            bytes.Add(line.ToArray());
            return true;
        }


        public override bool ProcessGraphics(XmlElement element, List<byte[]> bytes)
        {
            int height = 0, width = 0;

            foreach (XmlAttribute attribute in element.Attributes)
            {
                if (attribute.Name == "height")
                    height = int.Parse(attribute.Value);
                if (attribute.Name == "width")
                    width = int.Parse(attribute.Value);
            }

            if ((width%8)!=0)
                return false;

            string data = element.ChildNodes[0].Value;

            List<byte> line = new List<byte>();

            if (height > 248)
                height = 248;
            // define download image
            line.Add(0x1d);
            line.Add(0x2a);
            line.Add((byte)(width / 8));
            line.Add((byte)height);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x += 8)
                {
                    byte b = 0;
                    for (int i = 0; i < 8; i++)
                        b |= (byte)((data[y * width + x + i] - '0') << i);
                    line.Add(b);
                }
            }

            bytes.Add(line.ToArray());
            line.Clear();

            // print it
            line.Add(0x1d);
            line.Add(0x2f);
            line.Add(0x00);
            
            bytes.Add(line.ToArray());
            return true;
        }

    }
}
