﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class EmailSignList : TranswareBasicForm
    {
        private EmailSign selected = null;

        public EmailSignList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(EmailSignModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbText.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            OLVColumn col = new OLVColumn();
            col.AspectName = "Text";
            col.Text = "Text podpisu";
            col.ToolTipText = "Text podpisu emailu";
            col.LastDisplayIndex = 0;
            
            col.AspectToStringConverter = delegate(object x)
            {
                string value = (string)x;
                return value;
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (EmailSign)selectedObject;
            tbText.Text = selected.Text;

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbText.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (EmailSign)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbText.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }

        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpy(tbText.Text, "Text podpisu musí být zadán."))
            {
                tbText.Focus();
                return false;
            }            

            EmailSign item = selected;
            if (item == null)
                item = EmailSignModel.Instance.Create();
            item.Text = tbText.Text;
            EmailSignModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                EmailSignModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
