﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class PaymentTypeList : TranswareBasicForm
    {
        private PaymentType selected = null;

        public PaymentTypeList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(PaymentTypeModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            tbName.ReadOnly = !StatusEdit;
            cbRounding.Enabled = StatusEdit;
            nudNoOfBills.Enabled = StatusEdit;
            cbRate.Enabled = StatusEdit;
            cbIsCash.Enabled = StatusEdit;
            tbFee.ReadOnly = !StatusEdit;
            Items.Enabled = !StatusEdit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;

            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název položky";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Rounding";
            col.Text = "Zaokrouhlovat";
            col.ToolTipText = "Má se cena dokladu zaokrouhlovat?";
            col.LastDisplayIndex = index++;
            col.AspectToStringConverter = delegate(object x)
            {
                return ((bool)x) ? "Ano" : "Ne";
            };
            Items.ListView.AllColumns.Add(col);


            col = new OLVColumn();
            col.AspectName = "NoOfBills";
            col.Text = "Počet dokladů";
            col.ToolTipText = "Počet dokladů, které s emají vytisknout při platbě tímto způsobem";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Rate";
            col.Text = "Kurz";
            col.ToolTipText = "Který typ kurzu se má používat";
            col.LastDisplayIndex = index++;
            col.AspectToStringConverter = delegate(object x)
            {
                EUsedExchangeRate rate = (EUsedExchangeRate)x;
                switch (rate)
                {
                    case EUsedExchangeRate.CNBMid: return "Střed ČNB";
                    case EUsedExchangeRate.DevizyBuy: return "Devizy nákup";
                    case EUsedExchangeRate.DevizyMid: return "Devizy střed";
                    case EUsedExchangeRate.DevizySell: return "Devizy prodej";
                    case EUsedExchangeRate.ValutyBuy: return "Valuty nákup";
                    case EUsedExchangeRate.ValutyMid: return "Valuty střed";
                    case EUsedExchangeRate.ValutySell: return "Valuty prodej";
                }
                return "N/A";
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "IsCash";
            col.Text = "Hotovost";
            col.ToolTipText = "Jedná se o hotovostní operaci?";
            col.LastDisplayIndex = index++;
            col.AspectToStringConverter = delegate(object x)
            {
                return ((bool)x) ? "Ano" : "Ne";
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Fee";
            col.Text = "Poplatek";
            col.ToolTipText = "Poplatek odváděný poskytovateli služby, např. bance.";
            col.LastDisplayIndex = index++;
            col.AspectToStringConverter = delegate(object x)
            {
                return ((decimal)x).ToString("0.#####") + " %";
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (PaymentType)selectedObject;
            tbName.Text = selected.Name;
            cbRounding.Checked = selected.Rounding;
            nudNoOfBills.Value = selected.NoOfBills;
            cbRate.SelectedIndex = (int)selected.Rate;
            cbIsCash.Checked = selected.IsCash;
            tbFee.Text = selected.Fee.ToString("0.##");
            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            cbRounding.Checked = false;
            nudNoOfBills.Value = 0;
            cbRate.SelectedIndex = 0;
            cbIsCash.Checked = false;
            tbFee.Text = "0";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (PaymentType)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = "";
            cbRounding.Checked = false;
            nudNoOfBills.Value = 0;
            cbRate.SelectedIndex = 0;
            cbIsCash.Checked = false;
            tbFee.Text = "0";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }

        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 50, "Název položky musí být zadán.", "Název položky nesmí být delší než 50 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckDecimal(tbFee.Text,"Poplatek musí být číslo"))
            {
                tbFee.Focus();
                return false;
            }

            PaymentType item = selected;
            if (item == null)
                item = PaymentTypeModel.Instance.Create();
            item.Name = tbName.Text;
            item.Rounding = cbRounding.Checked;
            item.NoOfBills = (int)nudNoOfBills.Value;
            item.Rate = (EUsedExchangeRate)cbRate.SelectedIndex;
            item.IsCash = cbIsCash.Checked;
            item.Fee = decimal.Parse(tbFee.Text);

            PaymentTypeModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                PaymentTypeModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }        
    }
}
