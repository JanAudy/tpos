﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class DifferentialMarkList : TranswareBasicForm
    {
        private DifferentialMark selected = null;

        public DifferentialMarkList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(DifferentialMarkModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbShortcut.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název položky";
            col.LastDisplayIndex = 0;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Shortcut";
            col.Text = "Zkratka";
            col.TextAlign = HorizontalAlignment.Left;
            col.ToolTipText = "Zkratka názvu měrné jednotky";
            col.LastDisplayIndex = 1;
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (DifferentialMark)selectedObject;
            tbName.Text = selected.Name;
            tbShortcut.Text = selected.Shortcut;

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbShortcut.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (DifferentialMark)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbShortcut.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 255, "Název jednotky musí být zadán.", "Název jednotky nesmí být delší než 255 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringEmtpyLength(tbShortcut.Text, 20, "Zkratka názvu jednotky nemůže být prázdná.", "Zkratka názvu jednotky nesmí být delší než 20 znaků"))
            {
                tbName.Focus();
                return false;
            }
            
            DifferentialMark item = selected;
            if (item == null)
                item = DifferentialMarkModel.Instance.Create();
            item.Name = tbName.Text;
            item.Shortcut = tbShortcut.Text;
            DifferentialMarkModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                DifferentialMarkModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
