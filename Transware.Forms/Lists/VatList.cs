﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class VatList : TranswareBasicForm
    {
        private Vat selected = null;

        public VatList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(VatModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbVat.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název položky";
            col.LastDisplayIndex = 0;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Tax";
            col.Text = "DPH";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Hodnota DPH";
            col.LastDisplayIndex = 1;
            col.AspectToStringConverter = delegate(object x)
            {
                decimal value = (decimal)x;
                return value.ToString("0.##") + " %";
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (Vat)selectedObject;
            tbName.Text = selected.Name;
            tbVat.Text = selected.Tax.ToString("0.##");

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbVat.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (Vat)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbVat.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 30, "Název položky musí být zadán.", "Název položky nesmí být delší než 30 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckDecimal(tbVat.Text, "Hodnota DPH musí být číslo"))
            {
                tbVat.Focus();
                return false;
            }

            Vat vat = selected;
            if (vat == null)
                vat = VatModel.Instance.Create();
            vat.Name = tbName.Text;
            vat.Tax = decimal.Parse(tbVat.Text);
            VatModel.Instance.SaveOrUpdate(vat);
            if (selected!=null)
                Items.ListView.RefreshObject(vat);
            else
                Items.ListView.AddObject(vat);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                VatModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
