﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class CurrencyList : TranswareBasicForm
    {
        private Currency selected = null;

        public CurrencyList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(CurrencyModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbShortcut.ReadOnly = !edit;
            cbRecordRounding.Enabled = cbPackageRounding.Enabled = edit;
            nudRecordPlaces.Enabled = nudPackagePlaces.Enabled = edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název měny";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Shortcut";
            col.Text = "Zkratka";
            col.ToolTipText = "Zkratka měny";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.Text = "Zaokrouhlení cen balení";
            col.TextAlign = HorizontalAlignment.Left;
            col.ToolTipText = "Zaokrouhlení cen balení";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                Currency cur = (Currency)x;
                string name = "";
                switch (cur.Rounding)
                {
                    case ERoundingType.Down: name = "Dolů"; break;
                    case ERoundingType.Up: name = "Nahoru"; break;
                    case ERoundingType.Math: name = "Matematicky"; break;
                }
                name += string.Format(" na {0} des. míst", cur.Places);

                return name;
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.Text = "Zaokrouhlení dokladů";
            col.TextAlign = HorizontalAlignment.Left;
            col.ToolTipText = "Zaokrouhlení dokladů";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                Currency cur = (Currency)x;
                string name = "";
                switch (cur.PackageRounding)
                {
                    case ERoundingType.Down: name = "Dolů"; break;
                    case ERoundingType.Up: name = "Nahoru"; break;
                    case ERoundingType.Math: name = "Matematicky"; break;
                }
                name += string.Format(" na {0} des. míst", cur.PackagePlaces);

                return name;
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (Currency)selectedObject;
            tbName.Text = selected.Name;
            tbShortcut.Text = selected.Shortcut;
            cbRecordRounding.SelectedIndex = (int)selected.Rounding;
            nudRecordPlaces.Value = selected.Places;
            cbPackageRounding.SelectedIndex = (int)selected.PackageRounding;
            nudPackagePlaces.Value = selected.PackagePlaces;            

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbShortcut.Text = "";
            cbRecordRounding.SelectedIndex = 0;
            nudRecordPlaces.Value = 0;
            cbPackageRounding.SelectedIndex = 0;
            nudPackagePlaces.Value = 0;
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (Currency)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbShortcut.Text = "";
            cbRecordRounding.SelectedIndex = cbPackageRounding.SelectedIndex = 2;
            nudRecordPlaces.Value = nudPackagePlaces.Value = 0;
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 30, "Název měny musí být zadán.", "Název měny nesmí být delší než 30 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringEmtpyLength(tbShortcut.Text, 3, "Zkratka měny musí být zadán.", "Zkratka měny nesmí být delší než 3 znaků"))
            {
                tbShortcut.Focus();
                return false;
            }
            

            Currency currency = selected;
            if (currency == null)
                currency = CurrencyModel.Instance.Create();
            currency.Name = tbName.Text;
            currency.Shortcut = tbShortcut.Text;
            currency.Rounding = (ERoundingType)cbRecordRounding.SelectedIndex;
            currency.Places = (int)nudRecordPlaces.Value;
            currency.PackageRounding = (ERoundingType)cbPackageRounding.SelectedIndex;
            currency.PackagePlaces = (int)nudPackagePlaces.Value;

            CurrencyModel.Instance.SaveOrUpdate(currency);
            if (selected!=null)
                Items.ListView.RefreshObject(currency);
            else
                Items.ListView.AddObject(currency);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                CurrencyModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
