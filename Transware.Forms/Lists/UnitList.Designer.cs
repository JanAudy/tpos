﻿namespace Transware.Forms
{
    partial class UnitList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.bLang = new System.Windows.Forms.Button();
            this.tbMultiply = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbShortcut = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Items = new Transware.Controls.ListControl();
            this.gbItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.bLang);
            this.gbItem.Controls.Add(this.tbMultiply);
            this.gbItem.Controls.Add(this.label3);
            this.gbItem.Controls.Add(this.tbShortcut);
            this.gbItem.Controls.Add(this.label2);
            this.gbItem.Controls.Add(this.tbName);
            this.gbItem.Controls.Add(this.label1);
            this.gbItem.Location = new System.Drawing.Point(0, 30);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(480, 53);
            this.gbItem.TabIndex = 2;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "Detail položky:";
            // 
            // bLang
            // 
            this.bLang.BackgroundImage = global::Transware.Forms.Properties.Resources.world16;
            this.bLang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bLang.Enabled = false;
            this.bLang.Location = new System.Drawing.Point(239, 22);
            this.bLang.Name = "bLang";
            this.bLang.Size = new System.Drawing.Size(23, 23);
            this.bLang.TabIndex = 6;
            this.bLang.UseVisualStyleBackColor = true;
            this.bLang.Click += new System.EventHandler(this.bLang_Click);
            // 
            // tbMultiply
            // 
            this.tbMultiply.Location = new System.Drawing.Point(425, 24);
            this.tbMultiply.Name = "tbMultiply";
            this.tbMultiply.Size = new System.Drawing.Size(43, 20);
            this.tbMultiply.TabIndex = 5;
            this.tbMultiply.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(372, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Násobek:";
            // 
            // tbShortcut
            // 
            this.tbShortcut.Location = new System.Drawing.Point(325, 24);
            this.tbShortcut.MaxLength = 5;
            this.tbShortcut.Name = "tbShortcut";
            this.tbShortcut.Size = new System.Drawing.Size(41, 20);
            this.tbShortcut.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(268, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Zkratka:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(60, 24);
            this.tbName.MaxLength = 255;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(173, 20);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Název:";
            // 
            // Items
            // 
            this.Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Items.Location = new System.Drawing.Point(0, 89);
            this.Items.Name = "Items";
            this.Items.Size = new System.Drawing.Size(480, 172);
            this.Items.TabIndex = 1;
            this.Items.Title = "Měrné jednotky:";
            this.Items.ItemSelected += new Transware.Controls.ListControlItemSelected(this.Items_ItemSelected);
            this.Items.ItemUnselected += new Transware.Controls.ListControlItemSelected(this.Items_ItemUnselected);
            this.Items.ItemDoubleClicked += new Transware.Controls.ListControlItemSelected(this.Items_ItemDoubleClicked);
            this.Items.CreateColumns += new Transware.Controls.ListControlCreateColumns(this.Items_CreateColumns);
            this.Items.ItemLanguageToolTipShown += new Transware.Controls.LanguageToolTip(this.Items_ItemLanguageToolTipShown);
            // 
            // UnitList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 262);
            this.Controls.Add(this.gbItem);
            this.Controls.Add(this.Items);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "UnitList";
            this.Text = "Seznam měrných jednotek";
            this.Controls.SetChildIndex(this.Items, 0);
            this.Controls.SetChildIndex(this.gbItem, 0);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ListControl Items;
        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.TextBox tbShortcut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMultiply;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bLang;

    }
}