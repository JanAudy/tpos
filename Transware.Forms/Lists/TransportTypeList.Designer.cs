﻿namespace Transware.Forms
{
    partial class TransportTypeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.bLang = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Items = new Transware.Controls.ListControl();
            this.gbItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.bLang);
            this.gbItem.Controls.Add(this.tbDescription);
            this.gbItem.Controls.Add(this.label2);
            this.gbItem.Controls.Add(this.tbName);
            this.gbItem.Controls.Add(this.label1);
            this.gbItem.Location = new System.Drawing.Point(0, 30);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(480, 79);
            this.gbItem.TabIndex = 2;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "Detail položky:";
            // 
            // bLang
            // 
            this.bLang.BackgroundImage = global::Transware.Forms.Properties.Resources.world16;
            this.bLang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bLang.Enabled = false;
            this.bLang.Location = new System.Drawing.Point(445, 22);
            this.bLang.Name = "bLang";
            this.bLang.Size = new System.Drawing.Size(23, 23);
            this.bLang.TabIndex = 7;
            this.bLang.UseVisualStyleBackColor = true;
            this.bLang.Click += new System.EventHandler(this.bLang_Click);
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(60, 50);
            this.tbDescription.MaxLength = 255;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(408, 20);
            this.tbDescription.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Popis:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(60, 24);
            this.tbName.MaxLength = 30;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(380, 20);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Název:";
            // 
            // Items
            // 
            this.Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Items.Location = new System.Drawing.Point(0, 115);
            this.Items.Name = "Items";
            this.Items.Size = new System.Drawing.Size(480, 146);
            this.Items.TabIndex = 1;
            this.Items.Title = "Zoůsoby dopravy:";
            this.Items.ItemSelected += new Transware.Controls.ListControlItemSelected(this.Items_ItemSelected);
            this.Items.ItemUnselected += new Transware.Controls.ListControlItemSelected(this.Items_ItemUnselected);
            this.Items.ItemDoubleClicked += new Transware.Controls.ListControlItemSelected(this.Items_ItemDoubleClicked);
            this.Items.CreateColumns += new Transware.Controls.ListControlCreateColumns(this.Items_CreateColumns);
            this.Items.ItemLanguageToolTipShown += new Transware.Controls.LanguageToolTip(this.Items_ItemLanguageToolTipShown);
            // 
            // TransportTypeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 262);
            this.Controls.Add(this.gbItem);
            this.Controls.Add(this.Items);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "TransportTypeList";
            this.Text = "Seznam způsobů dopravy";
            this.Controls.SetChildIndex(this.Items, 0);
            this.Controls.SetChildIndex(this.gbItem, 0);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ListControl Items;
        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bLang;

    }
}