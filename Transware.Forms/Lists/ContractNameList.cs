﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class ContractNameList : TranswareBasicForm
    {
        private ContractName selected = null;

        public ContractNameList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(ContractNameModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbCode.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {

            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název zakázky";
            col.LastDisplayIndex = 0;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Code";
            col.Text = "Kód";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Kód zakázky";
            col.LastDisplayIndex = 1;
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (ContractName)selectedObject;
            tbName.Text = selected.Name;
            tbCode.Text = selected.Code;

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbCode.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (ContractName)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbCode.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 250, "Název položky musí být zadán.", "Název položky nesmí být delší než 250 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringLength(tbCode.Text, 250, "Popis nesmí být delší než 1000 znaků."))
            {
                tbCode.Focus();
                return false;
            }

            ContractName item = selected;
            if (item == null)
                item = ContractNameModel.Instance.Create();
            item.Name = tbName.Text;
            item.Code = tbCode.Text;
            ContractNameModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                ContractNameModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
