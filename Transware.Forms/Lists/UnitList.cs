﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class UnitList : TranswareBasicForm
    {
        private Unit selected = null;

        public UnitList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(UnitModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbShortcut.ReadOnly = tbMultiply.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název položky";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Shortcut";
            col.Text = "Zkratka";
            col.TextAlign = HorizontalAlignment.Left;
            col.ToolTipText = "Zkratka názvu měrné jednotky";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Multiply";
            col.Text = "Násobek";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Násobek měrné jednotky";
            col.LastDisplayIndex = index++;
            col.AspectToStringConverter = delegate(object x)
            {
                decimal value = (decimal)x;
                return value.ToString("0.#####");
            };
            Items.ListView.AllColumns.Add(col);

            col = Items.LanguageColumn<UnitLang>();
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (Unit)selectedObject;
            tbName.Text = selected.Name;
            tbShortcut.Text = selected.Shortcut;
            tbMultiply.Text = selected.Multiply.ToString("0.#####");
            bLang.Enabled = true;
            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbShortcut.Text = "";
            tbMultiply.Text = "";
            selected = null;
            bLang.Enabled = false;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (Unit)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbShortcut.Text = tbMultiply.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 255, "Název jednotky musí být zadán.", "Název jednotky nesmí být delší než 255 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringEmtpyLength(tbShortcut.Text, 5, "Zkratka názvu jednotky nemůže být prázdná.", "Zkratka názvu jednotky nesmí být delší než 5 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckDecimal(tbMultiply.Text, "Násobek musí být číslo"))
            {
                tbMultiply.Focus();
                return false;
            }

            Unit item = selected;
            if (item == null)
                item = UnitModel.Instance.Create();
            item.Name = tbName.Text;
            item.Shortcut = tbShortcut.Text;
            item.Multiply = decimal.Parse(tbMultiply.Text);
            UnitModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                UnitModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        private string Items_ItemLanguageToolTipShown(object obj)
        {
            Unit unit = (Unit)obj;

            string s = "";
            foreach (ELanguage l in Enum.GetValues(typeof(ELanguage)))
            {
                if (l == ELanguage.Czech) continue;
                if (unit.Languages.ContainsKey(l))
                {
                    string line = unit.Languages[l].Value;
                    string[] parts = line.Split(new char[] { '|' });
                    if (parts.Length == 2)
                        s += "Anglicky: " + parts[0] + "(" + parts[1] + ")\r\n";
                }
            }
            if (!string.IsNullOrEmpty(s))
                return s.TrimEnd();
            else
                return "<není překlad>";
        }

        private void bLang_Click(object sender, EventArgs e)
        {
            if (selected == null) return;
            UnitLangList form = new UnitLangList(selected, selected.Languages);
            form.ShowDialog();
            if (StatusBasic)
            {
                UnitModel.Instance.SaveOrUpdate(selected);
                Items.ListView.RefreshSelectedObjects();
            }
        }

        
    }
}
