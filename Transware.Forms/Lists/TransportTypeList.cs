﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class TransportTypeList : TranswareBasicForm
    {
        private TransportType selected = null;

        public TransportTypeList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(TransportTypeModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbDescription.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název typu dopravy";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Description";
            col.Text = "Popis";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Popis typu dopravy";
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);

            col = Items.LanguageColumn<TransportTypeLang>();
            col.LastDisplayIndex = index++;
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (TransportType)selectedObject;
            tbName.Text = selected.Name;
            tbDescription.Text = selected.Description;
            bLang.Enabled = true;
            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbDescription.Text = "";
            selected = null;
            bLang.Enabled = false;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (TransportType)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbDescription.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 255, "Název položky musí být zadán.", "Název položky nesmí být delší než 255 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringLength(tbDescription.Text, 1000, "Popis nesmí být delší než 1000 znaků."))
            {
                tbDescription.Focus();
                return false;
            }

            TransportType item = selected;
            if (item == null)
                item = TransportTypeModel.Instance.Create();
            item.Name = tbName.Text;
            item.Description = tbDescription.Text;
            TransportTypeModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                TransportTypeModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }


        private string Items_ItemLanguageToolTipShown(object obj)
        {
            TransportType unit = (TransportType)obj;

            string s = "";
            foreach (ELanguage l in Enum.GetValues(typeof(ELanguage)))
            {
                if (l == ELanguage.Czech) continue;
                if (unit.Languages.ContainsKey(l))
                {
                    string line = unit.Languages[l].Value;
                    s += "Anglicky: " + line + "\r\n";
                }
            }
            if (!string.IsNullOrEmpty(s))
                return s.TrimEnd();
            else
                return "<není překlad>";
        }

        private void bLang_Click(object sender, EventArgs e)
        {
            if (selected == null) return;

            TransportTypeLangList form = new TransportTypeLangList(selected, selected.Languages);
            form.ShowDialog();
            if (StatusBasic)
            {
                TransportTypeModel.Instance.SaveOrUpdate(selected);
                Items.ListView.RefreshSelectedObjects();
            }
        }
        
    }
}
