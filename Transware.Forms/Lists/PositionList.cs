﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class PositionList : TranswareBasicForm
    {
        private Position selected = null;

        public PositionList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(PositionModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbDescription.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {

            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název pozice";
            col.LastDisplayIndex = 0;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Description";
            col.Text = "Popis";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Popis pozice";
            col.LastDisplayIndex = 1;
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (Position)selectedObject;
            tbName.Text = selected.Name;
            tbDescription.Text = selected.Description;

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbDescription.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (Position)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbDescription.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 30, "Název pozice musí být zadán.", "Název pozice nesmí být delší než 30 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringLength(tbDescription.Text, 255, "Popis nesmí být delší než 255 znaků."))
            {
                tbDescription.Focus();
                return false;
            }

            Position vat = selected;
            if (vat == null)
                vat = PositionModel.Instance.Create();
            vat.Name = tbName.Text;
            vat.Description = tbDescription.Text;
            PositionModel.Instance.SaveOrUpdate(vat);
            if (selected!=null)
                Items.ListView.RefreshObject(vat);
            else
                Items.ListView.AddObject(vat);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                PositionModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
