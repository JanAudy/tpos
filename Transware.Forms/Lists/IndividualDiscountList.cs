﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;

namespace Transware.Forms
{
    public partial class IndividualDiscountList : TranswareBasicForm
    {
        private IndividualDiscount selected = null;

        public IndividualDiscountList()
        {
            InitializeComponent();

            Items.Initialize();

            Initialize();

            Items.ListView.SetObjects(IndividualDiscountModel.Instance.GetList());
        }
        
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbIndividualDiscount.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            OLVColumn col = new OLVColumn();
            col.AspectName = "Name";
            col.Text = "Název";
            col.ToolTipText = "Název slevy";
            col.LastDisplayIndex = 0;
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.AspectName = "Discount";
            col.Text = "Sleva";
            col.TextAlign = HorizontalAlignment.Right;
            col.ToolTipText = "Sleva zadaná v procentech";
            col.LastDisplayIndex = 1;
            col.AspectToStringConverter = delegate(object x)
            {
                decimal value = (decimal)x;
                return value.ToString("0.#####") + " %";
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (IndividualDiscount)selectedObject;
            tbName.Text = selected.Name;
            tbIndividualDiscount.Text = selected.Discount.ToString("0.#####");

            EnableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbIndividualDiscount.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (IndividualDiscount)selectedObject;
                Edit();
            }
        }

        protected override bool Create()
        {
            tbName.Text = tbIndividualDiscount.Text = "";
            selected = null;
            Status = EFormState.Edit;
            return true;
        }
        protected override bool Edit()
        {
            Status = EFormState.Edit;
            return base.Edit();
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 30, "Název položky musí být zadán.", "Název položky nesmí být delší než 30 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckDecimal(tbIndividualDiscount.Text, "Hodnota slevy musí být číslo"))
            {
                tbIndividualDiscount.Focus();
                return false;
            }

            IndividualDiscount item = selected;
            if (item == null)
                item = IndividualDiscountModel.Instance.Create();
            item.Name = tbName.Text;
            item.Discount = decimal.Parse(tbIndividualDiscount.Text);
            IndividualDiscountModel.Instance.SaveOrUpdate(item);
            if (selected!=null)
                Items.ListView.RefreshObject(item);
            else
                Items.ListView.AddObject(item);

            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool Delete()
        {
            if (selected!=null && base.Delete())
            {
                selected.Valid = false;
                IndividualDiscountModel.Instance.UpdateValid(selected);
                Items.RemoveObject(selected);

                Status = EFormState.Basic;
                Items.Focus();
                Items.RefreshSelectedItem();
                return true;
            }
            return false;
        }

        
    }
}
