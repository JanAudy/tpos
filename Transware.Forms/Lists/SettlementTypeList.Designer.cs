﻿namespace Transware.Forms
{
    partial class SettlementTypeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.cbInstantPayment = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbFee = new System.Windows.Forms.TextBox();
            this.cbIsCash = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbRate = new System.Windows.Forms.ComboBox();
            this.cbRounding = new System.Windows.Forms.CheckBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Items = new Transware.Controls.ListControl();
            this.bLang = new System.Windows.Forms.Button();
            this.gbItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.bLang);
            this.gbItem.Controls.Add(this.cbInstantPayment);
            this.gbItem.Controls.Add(this.label5);
            this.gbItem.Controls.Add(this.label4);
            this.gbItem.Controls.Add(this.tbFee);
            this.gbItem.Controls.Add(this.cbIsCash);
            this.gbItem.Controls.Add(this.label3);
            this.gbItem.Controls.Add(this.cbRate);
            this.gbItem.Controls.Add(this.cbRounding);
            this.gbItem.Controls.Add(this.tbName);
            this.gbItem.Controls.Add(this.label1);
            this.gbItem.Location = new System.Drawing.Point(0, 30);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(548, 80);
            this.gbItem.TabIndex = 2;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "Detail položky:";
            // 
            // cbInstantPayment
            // 
            this.cbInstantPayment.AutoSize = true;
            this.cbInstantPayment.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbInstantPayment.Location = new System.Drawing.Point(434, 26);
            this.cbInstantPayment.Name = "cbInstantPayment";
            this.cbInstantPayment.Size = new System.Drawing.Size(102, 17);
            this.cbInstantPayment.TabIndex = 11;
            this.cbInstantPayment.Text = "Okamžitá platba";
            this.cbInstantPayment.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(521, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(375, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Poplatek:";
            // 
            // tbFee
            // 
            this.tbFee.Location = new System.Drawing.Point(433, 51);
            this.tbFee.Name = "tbFee";
            this.tbFee.Size = new System.Drawing.Size(82, 20);
            this.tbFee.TabIndex = 8;
            // 
            // cbIsCash
            // 
            this.cbIsCash.AutoSize = true;
            this.cbIsCash.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbIsCash.Location = new System.Drawing.Point(239, 53);
            this.cbIsCash.Name = "cbIsCash";
            this.cbIsCash.Size = new System.Drawing.Size(121, 17);
            this.cbIsCash.TabIndex = 7;
            this.cbIsCash.Text = "Hotovostní operace";
            this.cbIsCash.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Převodní kurz:";
            // 
            // cbRate
            // 
            this.cbRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRate.FormattingEnabled = true;
            this.cbRate.Items.AddRange(new object[] {
            "Devizy - nákup",
            "Devizy - prodej",
            "Devizy - střed ",
            "Valuty - nákup ",
            "Valuty - prodej ",
            "Valuty - střed",
            "ČNB - střed"});
            this.cbRate.Location = new System.Drawing.Point(98, 51);
            this.cbRate.Name = "cbRate";
            this.cbRate.Size = new System.Drawing.Size(135, 21);
            this.cbRate.TabIndex = 5;
            // 
            // cbRounding
            // 
            this.cbRounding.AutoSize = true;
            this.cbRounding.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbRounding.Location = new System.Drawing.Point(317, 26);
            this.cbRounding.Name = "cbRounding";
            this.cbRounding.Size = new System.Drawing.Size(95, 17);
            this.cbRounding.TabIndex = 2;
            this.cbRounding.Text = "Zaokrouhlovat";
            this.cbRounding.UseVisualStyleBackColor = true;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(98, 24);
            this.tbName.MaxLength = 255;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(184, 20);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Název:";
            // 
            // Items
            // 
            this.Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Items.Location = new System.Drawing.Point(0, 116);
            this.Items.Name = "Items";
            this.Items.Size = new System.Drawing.Size(548, 233);
            this.Items.TabIndex = 1;
            this.Items.Title = "Typy úhrad faktur:";
            this.Items.ItemSelected += new Transware.Controls.ListControlItemSelected(this.Items_ItemSelected);
            this.Items.ItemUnselected += new Transware.Controls.ListControlItemSelected(this.Items_ItemUnselected);
            this.Items.ItemDoubleClicked += new Transware.Controls.ListControlItemSelected(this.Items_ItemDoubleClicked);
            this.Items.CreateColumns += new Transware.Controls.ListControlCreateColumns(this.Items_CreateColumns);
            this.Items.ItemLanguageToolTipShown += new Transware.Controls.LanguageToolTip(this.Items_ItemLanguageToolTipShown);
            // 
            // bLang
            // 
            this.bLang.BackgroundImage = global::Transware.Forms.Properties.Resources.world16;
            this.bLang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bLang.Enabled = false;
            this.bLang.Location = new System.Drawing.Point(288, 22);
            this.bLang.Name = "bLang";
            this.bLang.Size = new System.Drawing.Size(23, 23);
            this.bLang.TabIndex = 12;
            this.bLang.UseVisualStyleBackColor = true;
            this.bLang.Click += new System.EventHandler(this.bLang_Click);
            // 
            // SettlementTypeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 350);
            this.Controls.Add(this.gbItem);
            this.Controls.Add(this.Items);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SettlementTypeList";
            this.Text = "Seznam typů úhrad faktur";
            this.Controls.SetChildIndex(this.Items, 0);
            this.Controls.SetChildIndex(this.gbItem, 0);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ListControl Items;
        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbRate;
        private System.Windows.Forms.CheckBox cbRounding;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbFee;
        private System.Windows.Forms.CheckBox cbIsCash;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbInstantPayment;
        private System.Windows.Forms.Button bLang;

    }
}