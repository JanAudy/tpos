﻿namespace Transware.Forms
{
    partial class CurrencyList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.tbShortcut = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Items = new Transware.Controls.ListControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbRecordRounding = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudRecordPlaces = new System.Windows.Forms.NumericUpDown();
            this.nudPackagePlaces = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPackageRounding = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gbItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecordPlaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPackagePlaces)).BeginInit();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.nudPackagePlaces);
            this.gbItem.Controls.Add(this.label7);
            this.gbItem.Controls.Add(this.cbPackageRounding);
            this.gbItem.Controls.Add(this.label8);
            this.gbItem.Controls.Add(this.nudRecordPlaces);
            this.gbItem.Controls.Add(this.label6);
            this.gbItem.Controls.Add(this.cbRecordRounding);
            this.gbItem.Controls.Add(this.label5);
            this.gbItem.Controls.Add(this.label4);
            this.gbItem.Controls.Add(this.label3);
            this.gbItem.Controls.Add(this.tbShortcut);
            this.gbItem.Controls.Add(this.label2);
            this.gbItem.Controls.Add(this.tbName);
            this.gbItem.Controls.Add(this.label1);
            this.gbItem.Location = new System.Drawing.Point(0, 30);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(480, 121);
            this.gbItem.TabIndex = 2;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "Detail položky:";
            // 
            // tbShortcut
            // 
            this.tbShortcut.Location = new System.Drawing.Point(405, 24);
            this.tbShortcut.Name = "tbShortcut";
            this.tbShortcut.Size = new System.Drawing.Size(63, 20);
            this.tbShortcut.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(352, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Zkratka:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(69, 24);
            this.tbName.MaxLength = 30;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(264, 20);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Název:";
            // 
            // Items
            // 
            this.Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Items.Location = new System.Drawing.Point(0, 157);
            this.Items.Name = "Items";
            this.Items.Size = new System.Drawing.Size(480, 199);
            this.Items.TabIndex = 1;
            this.Items.Title = "Měny:";
            this.Items.ItemSelected += new Transware.Controls.ListControlItemSelected(this.Items_ItemSelected);
            this.Items.ItemUnselected += new Transware.Controls.ListControlItemSelected(this.Items_ItemUnselected);
            this.Items.ItemDoubleClicked += new Transware.Controls.ListControlItemSelected(this.Items_ItemDoubleClicked);
            this.Items.CreateColumns += new Transware.Controls.ListControlCreateColumns(this.Items_CreateColumns);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Zaokrouhlení cen balení:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(286, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Zaokrouhlení dokladů:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Způsob:";
            // 
            // cbRecordRounding
            // 
            this.cbRecordRounding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRecordRounding.FormattingEnabled = true;
            this.cbRecordRounding.Items.AddRange(new object[] {
            "Dolů",
            "Nahoru",
            "Matematicky"});
            this.cbRecordRounding.Location = new System.Drawing.Point(69, 69);
            this.cbRecordRounding.Name = "cbRecordRounding";
            this.cbRecordRounding.Size = new System.Drawing.Size(121, 21);
            this.cbRecordRounding.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Des. míst:";
            // 
            // nudRecordPlaces
            // 
            this.nudRecordPlaces.Location = new System.Drawing.Point(69, 96);
            this.nudRecordPlaces.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudRecordPlaces.Name = "nudRecordPlaces";
            this.nudRecordPlaces.Size = new System.Drawing.Size(44, 20);
            this.nudRecordPlaces.TabIndex = 9;
            // 
            // nudPackagePlaces
            // 
            this.nudPackagePlaces.Location = new System.Drawing.Point(347, 96);
            this.nudPackagePlaces.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudPackagePlaces.Name = "nudPackagePlaces";
            this.nudPackagePlaces.Size = new System.Drawing.Size(44, 20);
            this.nudPackagePlaces.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(286, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Des. míst:";
            // 
            // cbPackageRounding
            // 
            this.cbPackageRounding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackageRounding.FormattingEnabled = true;
            this.cbPackageRounding.Items.AddRange(new object[] {
            "Dolů",
            "Nahoru",
            "Matematicky"});
            this.cbPackageRounding.Location = new System.Drawing.Point(347, 69);
            this.cbPackageRounding.Name = "cbPackageRounding";
            this.cbPackageRounding.Size = new System.Drawing.Size(121, 21);
            this.cbPackageRounding.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Způsob:";
            // 
            // CurrencyList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 357);
            this.Controls.Add(this.gbItem);
            this.Controls.Add(this.Items);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "CurrencyList";
            this.Text = "Seznam měn";
            this.Controls.SetChildIndex(this.Items, 0);
            this.Controls.SetChildIndex(this.gbItem, 0);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecordPlaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPackagePlaces)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ListControl Items;
        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.TextBox tbShortcut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudRecordPlaces;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbRecordRounding;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudPackagePlaces;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbPackageRounding;
        private System.Windows.Forms.Label label8;

    }
}