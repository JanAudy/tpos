﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class UnitLangList : TranswareBasicForm
    {
        private Dictionary<ELanguage, UnitLang> languages;
        private UnitLang selected = null;

        public UnitLangList(Unit unit, Dictionary<ELanguage, UnitLang> languages)
        {
            this.languages = languages;

            InitializeComponent();

            Items.Initialize();

            Initialize();

            if (!languages.ContainsKey(ELanguage.Czech))
            {
                languages.Add(ELanguage.Czech, new UnitLang() { Language = ELanguage.Czech, Value = unit.Name + "|" + unit.Shortcut });
            }

            if (!languages.ContainsKey(ELanguage.English))
            {
                languages.Add(ELanguage.English, new UnitLang() { UnitID = unit.ID, Language = ELanguage.English, Value = "" });
            }
            
            Items.ListView.SetObjects(languages.Values);
            Items.ListView.Sort();
        }

        public override EFormButtons EnabledButtonsBasicState
        {
            get
            {
                return EFormButtons.Close;
            }
        } 
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = tbShortcut.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;
            OLVColumn col = new OLVColumn();
            col.Text = "Jazyk";
            col.ToolTipText = "Jazyk překladu";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                UnitLang ul = (UnitLang)x;
                switch (ul.Language)
                {
                    case ELanguage.Czech: return "Česky";
                    case ELanguage.English: return "Anglicky";
                }
                return "neznámý";
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.Text = "Název";
            col.ToolTipText = "Název";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                UnitLang ul = (UnitLang)x;
                string[] parts = ul.Value.Split("|".ToCharArray());
                if (parts.Length > 0)
                    return parts[0];
                return "";
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.Text = "Zkratka";
            col.ToolTipText = "Zkratka";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                UnitLang ul = (UnitLang)x;
                string[] parts = ul.Value.Split("|".ToCharArray());
                if (parts.Length > 1)
                    return parts[1];
                return "";
            };
            Items.ListView.AllColumns.Add(col);
           
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (UnitLang)selectedObject;
            string[] parts = selected.Value.Split("|".ToCharArray());
            tbName.Text = (parts.Length > 0) ? parts[0] : "";
            tbShortcut.Text = (parts.Length > 1) ? parts[1] : "";
            
            if (selected.Language!=ELanguage.Czech)
                EnableButtons(EFormButtons.Edit);
            else
                DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            tbShortcut.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (UnitLang)selectedObject;
                Edit();
            }
        }
                
        protected override bool Edit()
        {
            if (selected.Language != ELanguage.Czech)
            {
                Status = EFormState.Edit;
                return base.Edit();
            }
            return true;
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 250, "Název jednotky musí být zadán.", "Název jednotky nesmí být delší než 250 znaků"))
            {
                tbName.Focus();
                return false;
            }
            if (!CheckStringEmtpyLength(tbShortcut.Text, 5, "Zkratka názvu jednotky nemůže být prázdná.", "Zkratka názvu jednotky nesmí být delší než 5 znaků"))
            {
                tbShortcut.Focus();
                return false;
            }            

            UnitLang item = selected;
            item.Value = tbName.Text + "|" + tbShortcut.Text;
            Items.ListView.RefreshObject(item);
            
            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool CloseForm()
        {
            if (languages.ContainsKey(ELanguage.Czech))
                languages.Remove(ELanguage.Czech);
            return base.CloseForm();
        }
        
    }
}
