﻿namespace Transware.Forms
{
    partial class PaymentTypeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Items = new Transware.Controls.ListControl();
            this.cbRounding = new System.Windows.Forms.CheckBox();
            this.nudNoOfBills = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.cbRate = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbIsCash = new System.Windows.Forms.CheckBox();
            this.tbFee = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gbItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNoOfBills)).BeginInit();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.label5);
            this.gbItem.Controls.Add(this.label4);
            this.gbItem.Controls.Add(this.tbFee);
            this.gbItem.Controls.Add(this.cbIsCash);
            this.gbItem.Controls.Add(this.label3);
            this.gbItem.Controls.Add(this.cbRate);
            this.gbItem.Controls.Add(this.label2);
            this.gbItem.Controls.Add(this.nudNoOfBills);
            this.gbItem.Controls.Add(this.cbRounding);
            this.gbItem.Controls.Add(this.tbName);
            this.gbItem.Controls.Add(this.label1);
            this.gbItem.Location = new System.Drawing.Point(0, 30);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(548, 80);
            this.gbItem.TabIndex = 2;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "Detail položky:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(98, 24);
            this.tbName.MaxLength = 200;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(213, 20);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Název:";
            // 
            // Items
            // 
            this.Items.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Items.Location = new System.Drawing.Point(0, 116);
            this.Items.Name = "Items";
            this.Items.Size = new System.Drawing.Size(548, 233);
            this.Items.TabIndex = 1;
            this.Items.Title = "Typy úhrad účtenek:";
            this.Items.ItemSelected += new Transware.Controls.ListControlItemSelected(this.Items_ItemSelected);
            this.Items.ItemUnselected += new Transware.Controls.ListControlItemSelected(this.Items_ItemUnselected);
            this.Items.ItemDoubleClicked += new Transware.Controls.ListControlItemSelected(this.Items_ItemDoubleClicked);
            this.Items.CreateColumns += new Transware.Controls.ListControlCreateColumns(this.Items_CreateColumns);
            // 
            // cbRounding
            // 
            this.cbRounding.AutoSize = true;
            this.cbRounding.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbRounding.Location = new System.Drawing.Point(317, 26);
            this.cbRounding.Name = "cbRounding";
            this.cbRounding.Size = new System.Drawing.Size(95, 17);
            this.cbRounding.TabIndex = 2;
            this.cbRounding.Text = "Zaokrouhlovat";
            this.cbRounding.UseVisualStyleBackColor = true;
            // 
            // nudNoOfBills
            // 
            this.nudNoOfBills.Location = new System.Drawing.Point(499, 25);
            this.nudNoOfBills.Name = "nudNoOfBills";
            this.nudNoOfBills.Size = new System.Drawing.Size(37, 20);
            this.nudNoOfBills.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(414, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Počet dokladů:";
            // 
            // cbRate
            // 
            this.cbRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRate.FormattingEnabled = true;
            this.cbRate.Items.AddRange(new object[] {
            "Devizy - nákup",
            "Devizy - prodej",
            "Devizy - střed ",
            "Valuty - nákup ",
            "Valuty - prodej ",
            "Valuty - střed",
            "ČNB - střed"});
            this.cbRate.Location = new System.Drawing.Point(98, 51);
            this.cbRate.Name = "cbRate";
            this.cbRate.Size = new System.Drawing.Size(135, 21);
            this.cbRate.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Převodní kurz:";
            // 
            // cbIsCash
            // 
            this.cbIsCash.AutoSize = true;
            this.cbIsCash.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbIsCash.Location = new System.Drawing.Point(239, 53);
            this.cbIsCash.Name = "cbIsCash";
            this.cbIsCash.Size = new System.Drawing.Size(121, 17);
            this.cbIsCash.TabIndex = 7;
            this.cbIsCash.Text = "Hotovostní operace";
            this.cbIsCash.UseVisualStyleBackColor = true;
            // 
            // tbFee
            // 
            this.tbFee.Location = new System.Drawing.Point(433, 51);
            this.tbFee.Name = "tbFee";
            this.tbFee.Size = new System.Drawing.Size(82, 20);
            this.tbFee.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(375, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Poplatek:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(521, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "%";
            // 
            // PaymentTypeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 350);
            this.Controls.Add(this.gbItem);
            this.Controls.Add(this.Items);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "PaymentTypeList";
            this.Text = "Seznam typů úhrad účtenek";
            this.Controls.SetChildIndex(this.Items, 0);
            this.Controls.SetChildIndex(this.gbItem, 0);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNoOfBills)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ListControl Items;
        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudNoOfBills;
        private System.Windows.Forms.CheckBox cbRounding;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbFee;
        private System.Windows.Forms.CheckBox cbIsCash;
        private System.Windows.Forms.Label label5;

    }
}