﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microdata.Forms;
using BrightIdeasSoftware;
using Transware.Data.DataTypes;
using Microdata.Util;
using Microdata.Singletons;
using Transware.Data;
using Transware.Data.DataModel;
using Transware.Data.Enums;

namespace Transware.Forms
{
    public partial class OrderTypeLangList : TranswareBasicForm
    {
        private Dictionary<ELanguage, OrderTypeLang> languages;
        private OrderTypeLang selected = null;

        public OrderTypeLangList(OrderType item, Dictionary<ELanguage, OrderTypeLang> languages)
        {
            this.languages = languages;

            InitializeComponent();

            Items.Initialize();

            Initialize();

            if (!languages.ContainsKey(ELanguage.Czech))
            {
                languages.Add(ELanguage.Czech, new OrderTypeLang() { Language = ELanguage.Czech, Value = item.Name });
            }

            if (!languages.ContainsKey(ELanguage.English))
            {
                languages.Add(ELanguage.English, new OrderTypeLang() { OrderTypeID = item.ID, Language = ELanguage.English, Value = "" });
            }
            
            Items.ListView.SetObjects(languages.Values);
            Items.ListView.Sort();
        }

        public override EFormButtons EnabledButtonsBasicState
        {
            get
            {
                return EFormButtons.Close;
            }
        } 
        protected override void LoadSettings()
        {
            base.LoadSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.LoadSetting(reg, "ColumnWidth");
        }

        protected override void SaveSettings()
        {
            base.SaveSettings();

            RegistryClass reg = new RegistryClass(MicrodataStatus.Instance.ApplicationName, MicrodataStatus.Instance.UserName, Name);

            Items.SaveSetting(reg, "ColumnWidth");
        }

        protected override void FormStatusChanged(EFormState status)
        {
            bool edit = status == EFormState.Edit;
            tbName.ReadOnly = !edit;
            Items.Enabled = !edit;
        }

        private void Items_CreateColumns()
        {
            int index = 0;
            OLVColumn col = new OLVColumn();
            col.Text = "Jazyk";
            col.ToolTipText = "Jazyk překladu";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                OrderTypeLang ul = (OrderTypeLang)x;
                switch (ul.Language)
                {
                    case ELanguage.Czech: return "Česky";
                    case ELanguage.English: return "Anglicky";
                }
                return "neznámý";
            };
            Items.ListView.AllColumns.Add(col);

            col = new OLVColumn();
            col.Text = "Název";
            col.ToolTipText = "Název";
            col.LastDisplayIndex = index++;
            col.AspectGetter = delegate(object x)
            {
                OrderTypeLang ul = (OrderTypeLang)x;
                return ul.Value;
            };
            Items.ListView.AllColumns.Add(col);
        }

        private void Items_ItemSelected(object selectedObject)
        {
            selected = (OrderTypeLang)selectedObject;
            tbName.Text = selected.Value;
            
            if (selected.Language!=ELanguage.Czech)
                EnableButtons(EFormButtons.Edit);
            else
                DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemUnselected(object selectedObject)
        {
            tbName.Text = "";
            selected = null;
            DisableButtons(EFormButtons.Edit | EFormButtons.Delete);
        }

        private void Items_ItemDoubleClicked(object selectedObject)
        {
            if (selectedObject != null)
            {
                selected = (OrderTypeLang)selectedObject;
                Edit();
            }
        }
                
        protected override bool Edit()
        {
            if (selected.Language != ELanguage.Czech)
            {
                Status = EFormState.Edit;
                return base.Edit();
            }
            return true;
        }

        protected override bool Cancel()
        {
            if (base.Cancel())
            {
                Status = EFormState.Basic;
                Items.RefreshSelectedItem();
                Items.Focus();
            }
            return true;
        }

        protected override bool Save()
        {
            if (!CheckStringEmtpyLength(tbName.Text, 255, "Název musí být zadán.", "Název nesmí být delší než 255 znaků"))
            {
                tbName.Focus();
                return false;
            }
            
            OrderTypeLang item = selected;
            item.Value = tbName.Text;
            Items.ListView.RefreshObject(item);
            
            Status = EFormState.Basic;
            Items.RefreshSelectedItem();
            Items.Focus();

            return true;
        }

        protected override bool CloseForm()
        {
            if (languages.ContainsKey(ELanguage.Czech))
                languages.Remove(ELanguage.Czech);
            return base.CloseForm();
        }
        
    }
}
