﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Singletons
{
    public class MicrodataStatus
    {
        private static MicrodataStatus instance = null;
        
        public static MicrodataStatus Instance
        {
            get
            {
                if (instance == null)
                    instance = new MicrodataStatus();
                return instance;
            }
        }


        public string ApplicationName { get; set; }
        public string UserName { get; set; }
    }
}
