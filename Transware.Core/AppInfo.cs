﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using Microdata.Util;
using Transware.Data;
using Jadro.Data;
using CustomerSys.Data;

namespace Transware.Core
{
    public class AppInfo
    {
        private static AppInfo appInfo = null;
        public static AppInfo Instance
        {
            get
            {
                if (appInfo == null)
                    appInfo = new AppInfo();
                return appInfo;
            }
        }
        
        public string DatabaseServer { get; set; }
        public string DatabaseServer2 { get; set; }
        public string DatabaseUserName { get; set; }
        public string DatabasePassword { get; set; }
        public long LoggedUser { get; set; }

        public bool Init()
        {
            RegistryKey hklm = Registry.LocalMachine;

            if (hklm.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\DATABAZE") != null)
            {
                hklm = hklm.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\DATABAZE", false);

                DatabaseServer = (string)hklm.GetValue("DB_SERVER", null);
                DatabaseServer2 = (string)hklm.GetValue("DB_SERVER2", null);
                DatabaseUserName = (string)hklm.GetValue("DB_ADMIN", null);
                DatabasePassword = (string)hklm.GetValue("DB_PASSWORD", null);
                if (string.IsNullOrEmpty(DatabaseServer) || string.IsNullOrEmpty(DatabaseUserName) || string.IsNullOrEmpty(DatabasePassword))
                    return false;
                DatabaseUserName = CryptoUtil.RijndaelDecryptorString(DatabaseUserName);
                DatabasePassword = CryptoUtil.RijndaelDecryptorString(DatabasePassword);


                JadroDB.Instance.DB_Server = DatabaseServer;
                JadroDB.Instance.DB_UserName = DatabaseUserName;
                JadroDB.Instance.DB_Password = DatabasePassword;
                
                TranswareDB.Instance.DB_Server = DatabaseServer;
                TranswareDB.Instance.DB_UserName = DatabaseUserName;
                TranswareDB.Instance.DB_Password = DatabasePassword;

                CustomerSysDB.Instance.DB_Server = DatabaseServer;
                CustomerSysDB.Instance.DB_UserName = DatabaseUserName;
                CustomerSysDB.Instance.DB_Password = DatabasePassword;

                return true;
            }
            
            return false;
        }

        public void SetDB(string server, string user, string pass)
        {
            DatabaseServer = server;
            DatabaseServer2 = "";
            DatabaseUserName = user;
            DatabasePassword = pass;

            JadroDB.Instance.DB_Server = DatabaseServer;
            JadroDB.Instance.DB_UserName = DatabaseUserName;
            JadroDB.Instance.DB_Password = DatabasePassword;

            TranswareDB.Instance.DB_Server = DatabaseServer;
            TranswareDB.Instance.DB_UserName = DatabaseUserName;
            TranswareDB.Instance.DB_Password = DatabasePassword;

            CustomerSysDB.Instance.DB_Server = DatabaseServer;
            CustomerSysDB.Instance.DB_UserName = DatabaseUserName;
            CustomerSysDB.Instance.DB_Password = DatabasePassword;
        }
    }
}
