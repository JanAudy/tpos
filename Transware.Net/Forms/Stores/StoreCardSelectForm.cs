﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Transware.Core.DataInterfaces;
using Transware.Net.Code;
using Transware.Core.Domain;
using Transware.Data.DataClasses;
using Transware.Utils;
using Transware.Data;
using BrightIdeasSoftware;
using System.Collections;
using ProductSummary = Transware.Data.DataTypes.ProductSummary;
using Category = Transware.Data.DataTypes.Category;
using Transware.Data.DataModel;
using ProductFilter = Transware.Data.DataModel.ProductSummaryModel.ProductFilter;
using ValueComparison = Transware.Data.DataModel.ProductSummaryModel.ValueComparison;
using Transware.Reports;


namespace Transware.Net.Forms.Stores
{
    public enum SelectScope { Product, Package, Provider, ProductVariant, Multipackage };
    public partial class StoreCardSelectForm : BasicForm
    {
        public bool ShowActual = true;
        private readonly ILog errorLogger = LogManager.GetLogger(typeof(StoreCardSelectForm));
        public bool ForSelection = false;
        public SelectScope SelectScope;
        public Product product = null;
        public Package package = null;
        public Provider provider = null;
        public ProductVariant variant = null;
        public Category searchCat = null;
        public Package[] Packages { get; private set; }
        public List<ProductSummary> searchSummaries = null;
        private Category prevSelected = null;
        
        public long OrganizationID = 0;
        public bool pricesWithVat = false;
        
        private int columnNameWidth = 200;
        private Dictionary<decimal, int> columnPricesWidth = new Dictionary<decimal, int>();
        private int columnNoReservedWidth = 80;
        private int columnReservedWidth = 80;
        private int columnLockedWidth = 80;
        private int columnTotalWidth = 100;
        private int columnOrderMarkWidth = 100;
        private int CategoriesWidth = 200;
        private decimal[] amounts = new decimal[0];
        private ConfigClass config; 

        public StoreCardSelectForm() : this(false, SelectScope.Product, 0) { }

        public StoreCardSelectForm(bool ShowActual) : this(false, SelectScope.Product, 0, ShowActual) { }

        public StoreCardSelectForm(bool ForSelection, SelectScope SelectScope, long OrganizationID)
            : this(ForSelection, SelectScope, OrganizationID, true) { }

        public StoreCardSelectForm(bool ForSelection, SelectScope SelectScope, long OrganizationID, bool ShowActual)
        {
            this.ForSelection = ForSelection;
            this.SelectScope = SelectScope;
            this.OrganizationID = OrganizationID;
            this.ShowActual = ShowActual;

            Mode = FormMode.Basic;
            InitializeComponent();

            if (ForSelection)
                Text = "Vyberte produkt";
            else
                Text = "Evidence skladových karet";
            if (ForSelection)
                lvItems.MultiSelect = false;

            config = Database.Factory.GetConfigClassDao().GetConfigutation();

            lvItems.MultiSelect = SelectScope == Stores.SelectScope.Multipackage;

            ((SimpleDropSink)lvItems.DropSink).CanDropBetween = true;
            ((SimpleDropSink)lvItems.DropSink).CanDropOnItem = config.GetField(ConfigClass.ConfigFields.UseOrderMark) == "1";
            ((SimpleDropSink)lvItems.DropSink).CanDropOnBackground = false;

            lvItems.CanExpandGetter = delegate(object x)
            {
                if (!(x is ProductSummary))
                    return false;
                ProductSummary p = (ProductSummary)x;
                return (p.Variants != null && p.Variants.Count != 0);
            };

            lvItems.ChildrenGetter = delegate(object x)
            {
                if (!(x is ProductSummary))
                    return null;
                ProductSummary p = (ProductSummary)x;
                return p.Variants;
            };

            tvCategory.CanExpandGetter = delegate(object x)
            {
                Category c = (Category)x;
                return (c.Subcategories!=null && c.Subcategories.Count != 0);
            };

            tvCategory.ChildrenGetter = delegate(object x)
            {
                Category c = (Category)x;
                return c.Subcategories;
            };
            olvcCategory.ImageGetter = delegate(Object x)
            {
                Category p = (Category)x;
                if (p.Internet)
                    return 3;
                else
                    return 2;
            };
            ((SimpleDropSink)tvCategory.DropSink).CanDropOnBackground = false;
            tvCategory.CustomSorter = CategoryPositionComparer;

            

            LoadData();
        }

        private void LoadData()
        {
            try
            {
                CategoryModel model = CategoryModel.Instance;

                if (config.GetField(ConfigClass.ConfigFields.JidelnaConnected) != "1")
                {
                    Category c;
                    try
                    {
                        c = model.GetTree();
                    }
                    catch
                    {
                        c = new Category() { Name = "<prázdná položka>" };
                        model.SaveOrUpdate(c);
                    }

                    tvCategory.Roots = new Category[] { c };
                    tvCategory.Expand(c);
                }
                else
                {
                    List<Category> roots;
                    try
                    {
                        roots = model.GetAllTree();
                    }
                    catch
                    {
                        roots = new List<Category>();
                        Category c = new Category() { Name = "<prázdná položka>" };
                        model.SaveOrUpdate(c);
                        roots.Add(c);
                    }

                    tvCategory.Roots = roots;
                    foreach (Category c in roots)
                    {
                        tvCategory.Expand(c);
                    }
                }

                if (ForSelection && OrganizationID != 0)
                {
                    Organizace org = Database.JadroDao.GetOrganizace(OrganizationID);
                    Text = Text + ", Dodavatel: " + org.Name;
                }

                SetIcons(Mode);                
            }
            catch (Exception exc)
            {
                MessageBox.Show("Načítání dat selhalo.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errorLogger.Error("Error when loading data.", exc);
            }
        }

        public void UpdateItems()
        {
            UpdateItems((Category)tvCategory.SelectedObject);
        }

        private void UpdateItems(Category cat)
        {
            try
            {
                // create at least one column
                lvItems.AllColumns.Clear();
                lvItems.ClearCachedInfo();
                OLVColumn ch = new OLVColumn();
                ch.Text = "Název";
                ch.Width = columnNameWidth;
                ch.AspectName = "Name";
                ch.LastDisplayIndex = 0;
                lvItems.AllColumns.Add(ch);
                lvItems.RebuildColumns();
                lvItems.Roots = null;
                lvItems.DiscardAllState();

                if (cat == null)
                {
                    lvItems.Roots = null;
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                lvItems.EmptyListMsg = "Načítám produkty";
                Application.DoEvents();

                List<ProductSummary> products = null;
                if (cat == searchCat)
                {
                    products = searchSummaries;
                }
                else
                    products = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, cat.ID);

                string filter = tbFilter.Text;

                if (products != null && !string.IsNullOrEmpty(filter))
                {
                    List<ProductSummary> tmp = new List<ProductSummary>();
                    foreach (ProductSummary p in products)
                    {
                        if (p.Name.ToLower().StartsWith(filter.ToLower()))
                            tmp.Add(p);
                    }
                    products = tmp;
                }

                // disable non actual or actual based on setting
                if (products != null)
                {
                    List<ProductSummary> tmp = new List<ProductSummary>();
                    foreach (ProductSummary p in products)
                    {
                        if (p.Actual == ShowActual)
                            tmp.Add(p);
                    }
                    products = tmp;
                }

                if (products == null || products.Count == 0)
                {
                    lvItems.Roots = null;
                    return;
                }

                Dictionary<decimal, decimal> amountsD = new Dictionary<decimal, decimal>();
                bool hasVariant = false;
                foreach (ProductSummary p in products)
                {
                    foreach (int am in p.Prices.Keys)
                    {
                        if (!amountsD.ContainsKey(am))
                            amountsD.Add(am, am);
                    }
                    hasVariant |= p.OrderMark != null;
                }
                amounts = new decimal[amountsD.Keys.Count];
                amountsD.Keys.CopyTo(amounts, 0);
                Array.Sort(amounts);

                int displayIndex = 0;
                lvItems.AllColumns.Clear();
                lvItems.RebuildColumns();
                ch = new OLVColumn();
                ch.Text = "Název";
                ch.Width = columnNameWidth;
                ch.AspectName = "Name";
                ch.LastDisplayIndex = displayIndex++;
                ch.ImageGetter = delegate(Object x)
                {
                    if (x is ProductSummary)
                    {
                        ProductSummary p = (ProductSummary)x;
                        if (p.Internet)
                            return 4;
                        else
                            return 1;
                    }
                    else
                        {
                            ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                            if (p.Internet)
                                return 4;
                            else
                                return 1;
                        }
                };
                lvItems.AllColumns.Add(ch);
                foreach (int i in amounts)
                {
                    ch = new OLVColumn();
                    if (pricesWithVat)
                    {
                        ch.Text = "Cena/" + i + " Mj s DPH";
                        ch.AspectGetter = new StoreAspectGetters(i).PackagePrice;
                    }
                    else
                    {
                        ch.Text = "Cena/" + i + " Mj bez DPH";
                        ch.AspectGetter = new StoreAspectGetters(i).PackagePriceNoVat;
                    }

                    if (!columnPricesWidth.ContainsKey(i))
                    {
                        columnPricesWidth.Add(i, 120);
                    }

                    ch.Width = columnPricesWidth[i];
                    ch.TextAlign = HorizontalAlignment.Right;
                    ch.LastDisplayIndex = displayIndex++;
                    ch.Tag = i;
                    lvItems.AllColumns.Add(ch);
                }
                ch = new OLVColumn();
                ch.Text = "Bez rezervace";
                ch.AspectGetter = delegate(Object x)
                {
                    if (x is ProductSummary)
                    {
                        ProductSummary p = (ProductSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealNoReserved.ToString("0.#####");
                    }
                    else
                    {
                        ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealNoReserved.ToString("0.#####");
                    }
                };
                ch.Width = columnNoReservedWidth;
                ch.TextAlign = HorizontalAlignment.Right;
                ch.LastDisplayIndex = displayIndex++;
                lvItems.AllColumns.Add(ch);

                ch = new OLVColumn();
                ch.Text = "Rezervováno";
                ch.AspectGetter = delegate(Object x)
                {
                    if (x is ProductSummary)
                    {
                        ProductSummary p = (ProductSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealReserved.ToString("0.#####");
                    }
                    else
                    {
                        ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealReserved.ToString("0.#####");
                    }
                };
                ch.Width = columnReservedWidth;
                ch.TextAlign = HorizontalAlignment.Right;
                ch.LastDisplayIndex = displayIndex++;
                lvItems.AllColumns.Add(ch);

                ch = new OLVColumn();
                ch.Text = "Neuvolněno";
                ch.AspectGetter = delegate(Object x)
                {
                    if (x is ProductSummary)
                    {
                        ProductSummary p = (ProductSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealLocked.ToString("0.#####");
                    }
                    else
                    {
                        ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealLocked.ToString("0.#####");
                    }
                };
                ch.Width = columnLockedWidth;
                ch.TextAlign = HorizontalAlignment.Right;
                ch.LastDisplayIndex = displayIndex++;
                lvItems.AllColumns.Add(ch);

                ch = new OLVColumn();
                ch.Text = "Celkem na skladě";
                ch.AspectGetter = delegate(Object x)
                {
                    if (x is ProductSummary)
                    {
                        ProductSummary p = (ProductSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealTotal.ToString("0.#####");
                    }
                    else
                    {
                        ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                        if (p.UndefinedAmount)
                            return "nedef. množství";
                        return
                            p.RealTotal.ToString("0.#####");
                    }
                };
                ch.Width = columnTotalWidth;
                ch.TextAlign = HorizontalAlignment.Right;
                ch.LastDisplayIndex = displayIndex++;
                lvItems.AllColumns.Add(ch);

                if (config.GetField(ConfigClass.ConfigFields.UseUserID1) == "1")
                {
                    ch = new OLVColumn();
                    ch.Text = config.GetField(ConfigClass.ConfigFields.UserID1Name) ?? "Uživatelské ID:";
                    ch.AspectGetter = delegate(Object x)
                    {
                        if (x is ProductSummary)
                        {
                            ProductSummary p = (ProductSummary)x;
                            if (p.UserID1 != null)
                                return p.UserID1;
                            return "";
                        }
                        else
                        {
                            ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                            if (p.UserID1 != null)
                                return p.UserID1;
                            return "";
                        }
                    };
                    ch.Width = columnOrderMarkWidth;
                    ch.TextAlign = HorizontalAlignment.Left;
                    ch.LastDisplayIndex = displayIndex++;
                    lvItems.AllColumns.Add(ch);
                }
                //if (config.GetField(ConfigClass.ConfigFields.UseOrderMark) == "1")
                //{
                //    ch = new OLVColumn();
                //    ch.Text = config.GetField(ConfigClass.ConfigFields.OrderMarkName) ?? "Uspořádací znak:";
                //    ch.AspectGetter = delegate(Object x)
                //    {
                //        if (x is ProductSummary)
                //        {
                //            ProductSummary p = (ProductSummary)x;
                //            if (p.OrderMark != null)
                //                return p.OrderMark;
                //            return "";
                //        }
                //        else
                //        {
                //            ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;
                //            if (p.OrderMark != null)
                //                return p.OrderMark;
                //            return "";
                //        }
                //    };
                //    ch.Width = columnOrderMarkWidth;
                //    ch.TextAlign = HorizontalAlignment.Left;
                //    ch.LastDisplayIndex = displayIndex++;
                //    lvItems.AllColumns.Add(ch);
                //}
                lvItems.RebuildColumns();
                lvItems.Roots = products;
                lvItems.Sort();
                lvItems.ExpandAll();
            }
            catch (Exception e)
            {
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                lvItems.EmptyListMsg = "Seznam produktů je prázdný";
            }
        }

        private void SetIcons(FormMode mode)
        {
            TB_DB_NASLEDUJICI.Enabled = false;
            TB_DB_POSLEDNI.Enabled = false;
            TB_DB_PREDESLY.Enabled = false;
            TB_DB_PRVNI.Enabled = false;
            TB_NAJIT.Enabled = true;
            TB_PR_NAHLED.Enabled = true;
            TB_PR_NASTAV.Enabled = false;
            TB_PR_TISK.Enabled = true;
            TB_DETAIL.Enabled = false;
            TB_ULOZIT.Enabled = false;
            TB_STORNOVAT.Enabled = false;
            TB_VYRADIT.Enabled = false;
            TB_VYTVORIT.Enabled = false;
            TB_OPRAVIT.Enabled = false;
            TB_ZAVRIT.Enabled = false;
            
            if (mode == FormMode.Basic)
            {
                TB_VYTVORIT.Enabled = Database.HasPermission(Permissions.ProductsCategoryNew) | Database.HasPermission(Permissions.ProductsProductNew);
                TB_ZAVRIT.Enabled = true;
            }
            else if (mode == FormMode.Edit)
            {
                TB_ULOZIT.Enabled = true;
                TB_STORNOVAT.Enabled = true;
            }
        }

        protected override bool VYTVORIT()
        {
            if (tvCategory.SelectedObject == null)
                return true;

            SaveSettings();
                
            NewCategoryProductDialog f = new NewCategoryProductDialog();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                switch (f.itemType)
                {
                    case CreateItemType.Category:
                        Category selectedCategory = (Category)tvCategory.SelectedObject;
                        Transware.Core.Domain.Category c = Database.Factory.GetCategoryDao().GetById(selectedCategory.ID, false);
                        CategoryForm cform = new CategoryForm(c, null) { DIALOG = true };
                        cform.ShowDialog(this);
                        SaveSettings();
                        selectedCategory.Subcategories = null;
                        tvCategory.Expand(tvCategory.SelectedObject);
                        tvCategory.RefreshObject(tvCategory.SelectedObject);
                        break;
                    case CreateItemType.Product:
                        if (!DIALOG)
                        {
                            ProductForm pform = null;
                            foreach (Form pf in MdiParent.MdiChildren)
                            {
                                if (pf is ProductForm)
                                {
                                    pform = pf as ProductForm;
                                    break;
                                }
                            }
                            int pos = lvItems.GetItemCount();

                            if (config.GetField(ConfigClass.ConfigFields.SetPositionForNewProductToFirst) == "1")
                            {
                                if (lvItems.Objects != null)
                                {
                                    foreach (object o in lvItems.Objects)
                                    {
                                        ProductSummary ps = (ProductSummary)o;
                                        if (ps.PlaceNo < pos)
                                            pos = ps.PlaceNo;
                                    }
                                }
                            }

                            if (pform == null)
                            {
                                Category selectedCat = (Category)tvCategory.SelectedObject;
                                Transware.Core.Domain.Category cat = Database.Factory.GetCategoryDao().GetById(selectedCat.ID, false);
                                pform = new ProductForm(cat, null, true) { MdiParent = this.MdiParent, MenuPolozka = null, placeNo = pos - 1 };
                                pform.ItemsChangedObserver += RedrawItems;
                                pform.Show();
                            }
                            else
                            {
                                if (pform.Mode == FormMode.Basic)
                                {
                                    Category selectedCat = (Category)tvCategory.SelectedObject;
                                    Transware.Core.Domain.Category cat = Database.Factory.GetCategoryDao().GetById(selectedCat.ID, false);
                                    pform.placeNo = pos - 1;
                                    pform.Init(cat, null, true, null);
                                }
                                pform.Focus();
                            }
                        }
                        else
                        {
                            Category selectedCat = (Category)tvCategory.SelectedObject;
                            Transware.Core.Domain.Category cat = Database.Factory.GetCategoryDao().GetById(selectedCat.ID, false);
                            ProductForm pform = new ProductForm(cat, null, true) { DIALOG = true };
                            pform.ItemsChangedObserver += RedrawItems;
                            pform.ShowDialog(this);
                        }
                        //UpdateItems(tvCategory.SelectedNode);
                        break;
                    case CreateItemType.ProductCopy:
                        StoreCardSelectForm form = new StoreCardSelectForm(true, SelectScope.Product, 0) { DIALOG = true };
                        
                        if (form.ShowDialog(this) == DialogResult.OK)
                        {
                            Product p = form.product;
                            Category selectedCat = (Category)tvCategory.SelectedObject;
                            Transware.Core.Domain.Category cat = Database.Factory.GetCategoryDao().GetById(selectedCat.ID, false);
                            p.Category = cat;
                            if (!DIALOG)
                            {
                                ProductForm pfo = null;
                                foreach (Form pf in MdiParent.MdiChildren)
                                {
                                    if (pf is ProductForm)
                                    {
                                        pfo = pf as ProductForm;
                                        break;
                                    }
                                }
                                if (pfo == null)
                                {
                                    pfo = new ProductForm(p.Category, null, true, p) { MdiParent = this.MdiParent, MenuPolozka = null, placeNo = lvItems.Items.Count };//{ DIALOG = true };
                                    pfo.Show();
                                    //form.ShowDialog(this);
                                }
                                else
                                {
                                    if (pfo.Mode == FormMode.Basic)
                                    {
                                        pfo.Init(p.Category, null, true, p);
                                    }
                                    pfo.Focus();
                                }
                            }
                            else
                            {
                                ProductForm pf = new ProductForm(p.Category, p, true) { DIALOG = true };
                                pf.ShowDialog(this);
                            }
                            //UpdateItems(tvCategory.SelectedNode);
                        }
                        break;
                }
            }
            
            return true;
        }

        protected override bool OPRAVIT()
        {
            if (tvCategory.Focused && tvCategory.SelectedObject != null)
            {
                Category selectedCat = (Category)tvCategory.SelectedObject;
                Transware.Core.Domain.Category c = Database.Factory.GetCategoryDao().GetById(selectedCat.ID, false);
                CategoryForm form = new CategoryForm(c.Parent, c) { DIALOG = true };
                bool status = c.Internet;

                if (form.ShowDialog(this) == DialogResult.OK)
                {
                    selectedCat.Name = form.category.Name;
                    selectedCat.Description = form.category.Description;
                    selectedCat.Internet = form.category.Internet;
                    if (status != selectedCat.Internet)
                    {
                        if (MessageBox.Show("Nastavení pro zobrazení na Internetu se změnilo. Změnit je i u všech podkategorií a karet sortimentu?", "Změna nastavení", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            status = selectedCat.Internet;

                            ChangeInternetCategory(selectedCat, status);

                            UpdateItems();
                        }
                        else
                        {
                            tvCategory.RefreshObject(selectedCat);
                        }
                    }
                    tvCategory.RefreshObject(selectedCat);
                }
            }
            return true;
        }

        protected override void DETAIL()
        {
            if (lvItems.SelectedObjects.Count == 0)
                return;

            if (lvItems.SelectedObjects[0] is ProductSummary.ProductVariantSummary)
            {
                ProductSummary.ProductVariantSummary ps = (ProductSummary.ProductVariantSummary)lvItems.SelectedObjects[0];
                ProductVariant p = Database.Factory.GetProductVariantDao().GetById(ps.ID, false);
                if (DIALOG)
                {
                    ProductVariantForm form = null;
                    
                    if (form == null)
                    {
                        form = new ProductVariantForm(p.Product, p, false) { DIALOG = true, placeNo = lvItems.Items.Count };
                        //form.ItemsChangedObserver += RedrawItems;
                        form.ShowDialog(this);
                    }
                }
                else
                {
                    ProductVariantForm form = null;
                    //Product p = (Product)item.Tag;
                    foreach (Form f in MdiParent.MdiChildren)
                    {
                        if (f is ProductVariantForm)
                        {
                            form = f as ProductVariantForm;
                            break;
                        }
                    }
                    if (form == null)
                    {
                        form = new ProductVariantForm(p.Product, p, false) { MdiParent = this.MdiParent, MenuPolozka = null, placeNo = lvItems.Items.Count };//{ DIALOG = true };
                        //form.ItemsChangedObserver += RedrawItems;
                        form.Show();
                        //form.ShowDialog(this);
                    }
                    else
                    {
                        if (form.Mode == FormMode.Basic)
                        {
                            form.Init(p.Product, p, false, null);
                        }
                        form.Focus();
                    }
                }
            }
            else
            {
                ProductSummary ps = (ProductSummary)lvItems.SelectedObjects[0];
                Product p = Database.Factory.GetProductDao().GetById(ps.ID, false);
                
                if (DIALOG)
                {
                    ProductForm form = null;
                    
                    if (form == null)
                    {
                        form = new ProductForm(p.Category, p, false) { DIALOG = true, placeNo = lvItems.Items.Count };
                        form.ItemsChangedObserver += RedrawItems;
                        form.ShowDialog(this);
                    }
                }
                else
                {
                    ProductForm form = null;
                    foreach (Form f in MdiParent.MdiChildren)
                    {
                        if (f is ProductForm)
                        {
                            form = f as ProductForm;
                            break;
                        }
                    }
                    if (form == null)
                    {
                        form = new ProductForm(p.Category, p, false) { MdiParent = this.MdiParent, MenuPolozka = null, placeNo = lvItems.Items.Count };//{ DIALOG = true };
                        form.ItemsChangedObserver += RedrawItems;
                        form.Show();
                    }
                    else
                    {
                        if (form.Mode == FormMode.Basic)
                        {
                            form.Init(p.Category, p, false, null);
                        }
                        form.Focus();
                    }
                }
            }      
        }

        protected override bool VYBRAT()
        {
            if (lvItems.SelectedObjects.Count == 0)
                return true;

            if (SelectScope == Stores.SelectScope.Multipackage) 
            {
                if (lvItems.SelectedObjects.Count == 0)
                    return false;

                List<Package> pkgs = new List<Package>();
                foreach (object o in lvItems.SelectedObjects)
                {
                    if (o is ProductSummary)
                    {
                        ProductSummary ps = o as ProductSummary;
                        product = Database.Factory.GetProductDao().GetById(ps.ID, false);
                        if (product.Variants.Count != 0)
                            continue;
                        if (product.Packages.Count > 0)
                            pkgs.Add(product.Packages[0]);
                    }
                    if (o is ProductSummary.ProductVariantSummary)
                    {
                        ProductSummary.ProductVariantSummary ps = o as ProductSummary.ProductVariantSummary;
                        product = Database.Factory.GetProductDao().GetById(ps.PID, false);
                        variant = Database.Factory.GetProductVariantDao().GetById(ps.ID, false);
                        if (variant.Packages.Count > 0)
                            pkgs.Add(variant.Packages[0]);
                    }
                }
                Packages = pkgs.ToArray();
                
                DialogResult = DialogResult.OK;
                ZAVRIT(); 
                return true;
            }

            object obj = lvItems.SelectedObjects[0];
            if (obj is ProductSummary)
            {
                ProductSummary ps = (ProductSummary)lvItems.SelectedObjects[0];
                product = Database.Factory.GetProductDao().GetById(ps.ID, false);

                if (SelectScope == SelectScope.Product)
                {
                    DialogResult = DialogResult.OK;
                    ZAVRIT();
                    return true;
                }

                if (SelectScope == SelectScope.Package)
                {
                    //if (product.Providers.Count == 0)
                    //{
                    //    if (MessageBox.Show("Tento produkt nemá definovaného žádného dodavatele. Chcete jej přidat k tomuto produktu?", "Chybí dodavatel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                    //        return false;
                    //    if (!AddProvider(product, OrganizationID))
                    //        return false;
                    //}

                    if (OrganizationID != 0)
                    {
                        Provider org = product.GetProvider(OrganizationID);

                        if (org == null)
                        {
                            if (MessageBox.Show("Tento produkt nedodává zvolený dodavatel. Chcete jej přidat k tomuto produktu?", "Chyba dodavatele", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                return false;
                            if (!AddProvider(product, OrganizationID))
                                return false;
                        }
                        if (product.Packages.Count == 1)
                        {
                            package = product.Packages[0];
                            DialogResult = DialogResult.OK;
                            ZAVRIT();
                            return true;
                        }
                        else
                        {
                            PackageSelectForm pform = new PackageSelectForm(product) { DIALOG = true };
                            if (pform.ShowDialog() == DialogResult.OK)
                            {
                                package = pform.package;
                                DialogResult = DialogResult.OK;
                                ZAVRIT();
                                return true;
                            }
                            else
                                return false;
                        }
                    }

                    if (product.Packages.Count == 1)
                    {
                        package = product.Packages[0];
                        DialogResult = DialogResult.OK;
                        ZAVRIT();
                        return true;
                    }
                    PackageSelectForm form = new PackageSelectForm(product) { DIALOG = true };
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        package = form.package;
                    }
                    else
                        return false;
                }

                if (SelectScope == SelectScope.Provider)
                {
                    if (product.Providers.Count == 0)
                    {
                        if (MessageBox.Show("Tento produkt nemá definovaného žádného dodavatele. Chcete jej přidat k tomuto produktu?", "Chybí dodavatel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                            return false;
                        if (!AddProvider(product, OrganizationID))
                            return false;
                    }

                    if (OrganizationID != 0)
                    {
                        provider = product.GetProvider(OrganizationID);

                        if (provider == null)
                        {
                            if (MessageBox.Show("Tento produkt nedodává zvolený dodavatel. Chcete jej přidat k tomuto produktu?", "Chyba dodavatele", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                return false;
                            if (!AddProvider(product, OrganizationID))
                                return false;
                            provider = product.GetProvider(OrganizationID);
                        }
                    }
                    else
                    {
                        if (product.Providers.Count == 1)
                            provider = product.Providers[0];
                        else
                        {
                            ProviderList form = new ProviderList(product, false) { DIALOG = true };
                            if (form.ShowDialog(this) == DialogResult.OK)
                                provider = form.provider;
                        }
                    }
                }

                DialogResult = DialogResult.OK;
                ZAVRIT();
            }

            if (obj is ProductSummary.ProductVariantSummary)
            {
                ProductSummary.ProductVariantSummary ps = (ProductSummary.ProductVariantSummary)lvItems.SelectedObjects[0];
                ProductVariant variant = Database.Factory.GetProductVariantDao().GetById(ps.ID, false);

                if (SelectScope == SelectScope.Product)
                {
                    DialogResult = DialogResult.OK;
                    ZAVRIT();
                    return true;
                }

                if (SelectScope == SelectScope.Package)
                {
                    //if (variant.Providers.Count == 0)
                    //{
                    //    if (MessageBox.Show("Tento produkt nemá definovaného žádného dodavatele. Chcete jej přidat k tomuto produktu?", "Chybí dodavatel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                    //        return false;
                    //    if (!AddProvider(variant, OrganizationID))
                    //        return false;
                    //}

                    if (OrganizationID != 0)
                    {
                        Provider org = variant.GetProvider(OrganizationID);

                        if (org == null)
                        {
                            if (MessageBox.Show("Tento produkt nedodává zvolený dodavatel. Chcete jej přidat k tomuto produktu?", "Chyba dodavatele", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                return false;
                            if (!AddProvider(variant, OrganizationID))
                                return false;
                        }
                        if (variant.Packages.Count == 1)
                        {
                            package = variant.Packages[0];
                            DialogResult = DialogResult.OK;
                            ZAVRIT();
                            return true;
                        }
                        else
                        {
                            PackageSelectForm pform = new PackageSelectForm(variant.Packages) { DIALOG = true };
                            if (pform.ShowDialog() == DialogResult.OK)
                            {
                                package = pform.package;
                                DialogResult = DialogResult.OK;
                                ZAVRIT();
                                return true;
                            }
                            else
                                return false;
                        }
                    }

                    if (variant.Packages.Count == 1)
                    {
                        package = variant.Packages[0];
                        DialogResult = DialogResult.OK;
                        ZAVRIT();
                        return true;
                    }
                    PackageSelectForm form = new PackageSelectForm(variant.Packages) { DIALOG = true };
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        package = form.package;
                    }
                    else
                        return false;
                }

                if (SelectScope == SelectScope.Provider)
                {
                    if (variant.Providers.Count == 0)
                    {
                        if (MessageBox.Show("Tento produkt nemá definovaného žádného dodavatele. Chcete jej přidat k tomuto produktu?", "Chybí dodavatel", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                            return false;
                        if (!AddProvider(variant, OrganizationID))
                            return false;
                    }

                    if (OrganizationID != 0)
                    {
                        provider = variant.GetProvider(OrganizationID);

                        if (provider == null)
                        {
                            if (MessageBox.Show("Tento produkt nedodává zvolený dodavatel. Chcete jej přidat k tomuto produktu?", "Chyba dodavatele", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                return false;
                            if (!AddProvider(variant, OrganizationID))
                                return false;
                            provider = variant.GetProvider(OrganizationID);
                        }
                    }
                    else
                    {
                        if (variant.Providers.Count == 1)
                            provider = variant.Providers[0];
                        else
                        {
                            ProviderList form = new ProviderList(variant, false) { DIALOG = true };
                            if (form.ShowDialog(this) == DialogResult.OK)
                                provider = form.provider;
                        }
                    }
                }

                DialogResult = DialogResult.OK;
                ZAVRIT();
            }

            return true;
        }

        private bool AddProvider(Product product, long orgId)
        {
            Provider p = new Provider();
            p.OrganizationID = orgId;
            ProviderForm pf = new ProviderForm(p) { DIALOG = true };
            if (pf.ShowDialog(this) == DialogResult.OK)
            {
                p.Product = product;
                product.Providers.Add(p);
                product.Provider = p;

                IProductDao dao = Database.Factory.GetProductDao();
                try
                {
                    dao.SaveOrUpdate(product);
                    return true;
                }
                catch { }                
            }
            return false;
        }

        private bool AddProvider(ProductVariant product, long orgId)
        {
            Provider p = new Provider();
            p.OrganizationID = orgId;
            ProviderForm pf = new ProviderForm(p) { DIALOG = true };
            if (pf.ShowDialog(this) == DialogResult.OK)
            {
                p.Variant = product;
                product.Providers.Add(p);
                product.Provider = p;

                IProductDao dao = Database.Factory.GetProductDao();
                try
                {
                    dao.SaveOrUpdate(product.Product);
                    return true;
                }
                catch { }
            }
            return false;
        }

        private void DeleteCategory(Category c)
        {
            foreach (Category sc in c.Subcategories)
            {
                if (sc.Parent != c)
                    continue;
                
                DeleteCategory(sc);
            }
            List<ProductSummary> products = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, c.ID);
            ProductSummaryModel.Instance.SetInvalid(products);

            c.Valid = false;
            CategoryModel.Instance.UpdateValid(c);
            tvCategory.RemoveObject(c);
        }

        private void ChangeInternetCategory(Category c, bool state)
        {
            foreach (Category sc in c.Subcategories)
            {
                if (sc.Parent != c)
                    continue;
                sc.Internet = state;
                CategoryModel.Instance.SaveOrUpdate(sc);
                ChangeInternetCategory(sc, state);
            }

            List<ProductSummary> summaries = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, c.ID);
            ProductSummaryModel.Instance.SetInternet(summaries, state);

            CategoryModel.Instance.SaveOrUpdate(c);
        }

        protected override bool VYRADIT()
        {
            if (ForSelection || !TB_VYRADIT.Enabled)
                return false;

            if (tvCategory.Focused)
            {
                if (tvCategory.SelectedObject == null)
                    return false;

                Category cat = (Category)tvCategory.SelectedObject;
                if (cat.Parent == null && cat.ID == 0)
                {
                    if (MessageBox.Show("Opravdu odstranit výsledky hledání?",
                    "Odstranit výsledky hledání", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                    DialogResult.Yes)
                    {
                        tvCategory.RemoveObject(cat);
                        searchCat = null;
                        tvCategory_SelectionChanged(this, null);
                    }
                    return true;
                }

                if (MessageBox.Show("Opravdu odstranit tento typ sortimentu včetně případných podkategorií a produktů?",
                    "Odstranit typ sortimentu", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                    DialogResult.Yes)
                {
                    Category c = cat;

                    try
                    {
                        DeleteCategory(c);

                        c.Parent.Subcategories.Remove(c);
                        tvCategory.RefreshObject(c.Parent);
                        tvCategory_SelectionChanged(this, null);
                    }
                    catch (Exception e)
                    {
                        Database.Factory.GetCategoryDao().RollbackChanges();
                        MessageBox.Show("Mazání selhalo.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        errorLogger.Error("Error when deleting data.", e);
                    }
                }
            }
            else
            {
                if (lvItems.SelectedObjects.Count == 0)
                    return true;

                if (lvItems.SelectedObjects.Count == 1)
                {
                    object o = lvItems.SelectedObjects[0];
                    if (o is ProductSummary)
                    {
                        ProductSummary psp = (ProductSummary)lvItems.SelectedObjects[0];
                        if (psp.RealTotal != 0)
                        {
                            MessageBox.Show("Tento produkt nemá nulový stav, nelze jej proto odstranit!", "Odstranit product", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                            return false;
                        }
                        if (MessageBox.Show("Opravdu odstranit tento produkt?",
                            "Odstranit product", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                            DialogResult.Yes)
                        {
                            ProductSummary p = (ProductSummary)lvItems.SelectedObjects[0];

                            try
                            {
                                List<ProductSummary> ps = new List<ProductSummary>();
                                ps.Add(p);
                                ProductSummaryModel.Instance.SetInvalid(ps);

                                int index = lvItems.SelectedIndices[0];
                                lvItems.RemoveObject(lvItems.SelectedObject);

                                if (index >= lvItems.Items.Count)
                                    index = lvItems.Items.Count - 1;
                                if (index < 0)
                                    index = 0;
                                if (index >= 0 && index < lvItems.Items.Count)
                                    lvItems.Items[index].Selected = true;
                            }
                            catch (Exception e)
                            {
                                Database.Factory.GetProductDao().RollbackChanges();
                                MessageBox.Show("Mazání selhalo.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                errorLogger.Error("Error when deleting data.", e);
                            }
                        }
                    }
                    else
                    {
                        ProductSummary.ProductVariantSummary psp = (ProductSummary.ProductVariantSummary)lvItems.SelectedObjects[0];
                        if (psp.RealTotal != 0)
                        {
                            MessageBox.Show("Tato varianta nemá nulový stav, nelze ji proto odstranit!", "Odstranit variantu", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                            return false;
                        }
                        if (MessageBox.Show("Opravdu odstranit tuto variantu?",
                            "Odstranit variantu", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                            DialogResult.Yes)
                        {
                            ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)lvItems.SelectedObjects[0];

                            try
                            {
                                List<ProductSummary.ProductVariantSummary> ps = new List<ProductSummary.ProductVariantSummary>();
                                ps.Add(p);
                                ProductSummaryModel.Instance.SetInvalid(ps);
                            }
                            catch (Exception e)
                            {
                                //Database.Factory.GetProductDao().RollbackChanges();
                                MessageBox.Show("Mazání selhalo.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                errorLogger.Error("Error when deleting data.", e);
                            }
                        }
                    }
                }
                else
                {
                    if (MessageBox.Show("Opravdu odstranit tyto produkty?",
                        "Odstranit producty", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                        DialogResult.Yes)
                    {
                        try
                        {
                            List<ProductSummary> ps = new List<ProductSummary>(); 
                            foreach (Object item in lvItems.SelectedObjects)
                            {
                                if (!(item is ProductSummary))
                                {
                                    MessageBox.Show("Nelze zároveň smazat produkty a varianty.", "Smazat produkty", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                    return false;
                                }
                                ps.Add((ProductSummary)item);
                            }
                            ProductSummaryModel.Instance.SetInvalid(ps);

                            int index = lvItems.SelectedIndices[0];

                            lvItems.RemoveObjects(lvItems.SelectedObjects);
                            
                            if (index >= lvItems.Items.Count)
                                index = lvItems.Items.Count - 1;
                            if (index < 0)
                                index = 0;
                            if (index >= 0 && index < lvItems.Items.Count)
                                lvItems.Items[index].Selected = true;
                        }
                        catch (Exception e)
                        {
                            Database.Factory.GetProductDao().RollbackChanges();
                            MessageBox.Show("Mazání selhalo.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            errorLogger.Error("Error when deleting data.", e);
                        }
                    }
                }
                UpdateItems();
            }
            return true;
        }

        private void lvItems_DoubleClick(object sender, EventArgs e)
        {
            if (lvItems.SelectedObjects.Count > 0)
            {
                if (ForSelection)
                    VYBRAT();
                else
                    DETAIL();
            }
        }             

        private void lvItems_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (ForSelection)
            {
                if (lvItems.SelectedObjects.Count == 0)
                {
                    TB_VYBRAT.Enabled = false;
                    TB_DETAIL.Enabled = false;
                    return;
                }

                TB_VYBRAT.Enabled = true;
                TB_DETAIL.Enabled = Database.HasPermission(Permissions.ProductsProductShow);
            }
            else
            {
                if (lvItems.SelectedObjects.Count == 0)
                {
                    TB_VYRADIT.Enabled = false;
                    TB_OPRAVIT.Enabled = false;
                    TB_DETAIL.Enabled = false;
                }
                else
                {
                    TB_VYRADIT.Enabled = Database.HasPermission(Permissions.ProductsProductDel);
                    TB_DETAIL.Enabled = Database.HasPermission(Permissions.ProductsProductShow);
                    TB_OPRAVIT.Enabled = false;
                }
            }            
        }

        private void tbFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateItems();
        }

        private void lvItems_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                if (tvCategory.SelectedObject == null || tvCategory.SelectedObject == searchCat)
                    return;

                if (MessageBox.Show("Opravdu seřadit položky podle abecedy?", "Seřadit položky", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Category cat = (Category)tvCategory.SelectedObject;
                    List<ProductSummary> products = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, cat.ID);

                    if (products.Count == 0)
                    {
                        return;
                    }

                    products.Sort(new ProductSummary.NameComparer());

                    for (int i = 0; i < products.Count; i++)
                        products[i].PlaceNo = i;

                    ProductSummaryModel.Instance.UpdatePlaceNo(products);

                    UpdateItems(cat);
                }                
            }

            if (lvItems.AllColumns[e.Column].Tag != null)
            {
                pricesWithVat = !pricesWithVat;
                foreach (OLVColumn col in lvItems.AllColumns)
                {
                    if (col.Tag == null) continue;
                    int amount = (int)col.Tag;
                    if (pricesWithVat)
                    {
                        col.Text = "Cena/" + amount + " Mj s DPH";
                        col.AspectGetter = new StoreAspectGetters(amount).PackagePrice;
                    }
                    else
                    {
                        col.Text = "Cena/" + amount + " Mj bez DPH";
                        col.AspectGetter = new StoreAspectGetters(amount).PackagePriceNoVat;
                    }
                }
                lvItems.Refresh();
            }
        }

        
        protected override void SaveSettings()
        {
            RegistryClass MyRK = new RegistryClass(Constants.APP_REGNAME, new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name);
            
            MyRK.SetWinPARAM("PRICES_WITH_VAT", ((pricesWithVat)?1:0).ToString());

            MyRK.SetWinPARAM("ColumnNameWidth", columnNameWidth.ToString());
            MyRK.SetWinPARAM("ColumnNoReservedWidth", columnNoReservedWidth.ToString());
            MyRK.SetWinPARAM("ColumnReservedWidth", columnReservedWidth.ToString());
            MyRK.SetWinPARAM("ColumnLockedWidth", columnLockedWidth.ToString());
            MyRK.SetWinPARAM("ColumnTotalWidth", columnTotalWidth.ToString());
            MyRK.SetWinPARAM("CategoriesWidth", splitContainer1.SplitterDistance.ToString());
            string amount = "";
            foreach (int i in columnPricesWidth.Keys)
                amount += i + ";";
            MyRK.SetWinPARAM("Amounts", amount);
            foreach (int i in columnPricesWidth.Keys)
            {
                MyRK.SetWinPARAM("ColumnPricesWidth"+i, columnPricesWidth[i].ToString());
            }

            string listofItems = "";
            GetListOfExpadedItem(tvCategory.Objects, ref listofItems);
            if (listofItems.Length > 0)
                listofItems = listofItems.Remove(listofItems.Length - 1);
            MyRK.SetWinPARAM("ListOfExpandedItems", listofItems);

            if (tvCategory.SelectedObject!=searchCat)
                MyRK.SetWinPARAM("SelectedNode", ((Category)tvCategory.SelectedObject).ID.ToString());
        }

        private void GetListOfExpadedItem(IEnumerable objects, ref string s)
        {
            foreach(object x in objects)
            {
                if (x == searchCat) continue;
                Category c = (Category)x;
                if (tvCategory.IsExpanded(c))
                    s += c.ID + ";";
                GetListOfExpadedItem(tvCategory.GetChildren(c), ref s);
            }            
        }

        protected override void LoadSettings()
        {
            RegistryClass MyRK = new RegistryClass(Constants.APP_REGNAME, new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name);

            pricesWithVat = MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName),
                this.Name, "PRICES_WITH_VAT", "0")=="1";

            columnNameWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name, 
                "ColumnNameWidth", columnNameWidth.ToString()));
            columnNoReservedWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name, 
                "ColumnNoReservedWidth", columnNoReservedWidth.ToString()));
            columnReservedWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name, 
                "ColumnReservedWidth", columnReservedWidth.ToString()));
            columnLockedWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name,
                "ColumnLockedWidth", columnLockedWidth.ToString()));
            columnTotalWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name, 
                "ColumnTotalWidth", columnTotalWidth.ToString()));
            CategoriesWidth = int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name,
                "CategoriesWidth", CategoriesWidth.ToString()));
            string amount = MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name, "Amounts", "");
            string[] str = amount.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in str)
            {
                columnPricesWidth.Add(int.Parse(s), int.Parse(MyRK.GetWinPARAM(new MyCrypto().HashEncryptorString(Database.LoggedUser.UserName), this.Name,
                    "ColumnPricesWidth" + s, "120")));
            }

            string listOfItems = MyRK.GetWinPARAM("ListOfExpandedItems", "");
            if (!string.IsNullOrEmpty(listOfItems))
            {
                string[] items = listOfItems.Split(new char[] { ';' });
                Dictionary<long, long> expandedItems = new Dictionary<long, long>();
                foreach (string item in items)
                {
                    long id = long.Parse(item);
                    expandedItems.Add(id, id);
                }
                tvCategory.BeginUpdate();
                ExpandItems(tvCategory.Objects, expandedItems);
                tvCategory.EndUpdate();
            }
            long SelectedNode = 0;
            long.TryParse(MyRK.GetWinPARAM("SelectedNode", ""), out SelectedNode);
            if (SelectedNode != 0)
            {
                SelectNode(tvCategory.Objects, SelectedNode);
            }
            if (tvCategory.SelectedObject == null && tvCategory.Items.Count > 0)
                tvCategory.SelectedObject = ((ArrayList)tvCategory.Objects)[0];
            tvCategory.SelectedItem.EnsureVisible();
        }

        private void SelectNode(IEnumerable objects, long SelectedNode)
        {
            foreach (Object o in objects)
            {
                if (o == searchCat) continue;
                if (tvCategory.SelectedObject != null)
                    return;

                Category c = (Category)o;
                if (c.ID == SelectedNode)
                    tvCategory.SelectObject(c, true);

                SelectNode(tvCategory.GetChildren(c), SelectedNode);
            }
        }

        private void ExpandItems(IEnumerable objects, Dictionary<long, long> expandedItems)
        {
            foreach (Object o in objects)
            {
                if (o == searchCat) continue;

                Category c = (Category)o;
                if (expandedItems.ContainsKey(c.ID))
                    tvCategory.Expand(c);
                ExpandItems(tvCategory.GetChildren(c), expandedItems);
            }
            
        }

        private void lvItems_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            int columnCount = lvItems.AllColumns.Count;
            if (lvItems.AllColumns[e.ColumnIndex].Width < 40)
                lvItems.AllColumns[e.ColumnIndex].Width = 40;
            if (e.ColumnIndex == 0)
            {
                columnNameWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex == (1 + amounts.Length))
            {
                columnNoReservedWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex == (1 + amounts.Length + 1))
            {
                columnReservedWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex == (1 + amounts.Length + 2))
            {
                columnLockedWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex == (1 + amounts.Length + 3))
            {
                columnTotalWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex == (1 + amounts.Length + 4))
            {
                columnOrderMarkWidth = lvItems.AllColumns[e.ColumnIndex].Width;
            }
            if (e.ColumnIndex >= 1 && e.ColumnIndex < columnCount - 6)
            {
                columnPricesWidth[amounts[e.ColumnIndex - 1]] = lvItems.AllColumns[e.ColumnIndex].Width;
            }
        }

        private void lvItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && lvItems.SelectedObjects.Count > 0)
            {
                e.Handled = true;
                if (ForSelection)
                    VYBRAT();
                else
                    DETAIL();
            }
            if (e.KeyCode == Keys.Delete && lvItems.SelectedObjects.Count > 0)
            {
                if (!ForSelection)
                {
                    e.Handled = true;
                    VYRADIT();
                }
            }
        }

        private void tvCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && tvCategory.SelectedObject != null)
            {
                tvCategory.ToggleExpansion(tvCategory.SelectedObject);
              
            }
            if (e.KeyCode == Keys.Delete && tvCategory.SelectedObject != null)
            {
                VYRADIT();
            }
        }

        private void StoreCardSelectForm_Activated(object sender, EventArgs e)
        {
            UpdateItems();
        }

        private void StoreCardSelectForm_Shown(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = CategoriesWidth;
        }

        private string FormateAmount(decimal value)
        {
            if (value == decimal.Floor(value))
                return value.ToString("F0");
            else
                return value.ToString("F3");
        }

        protected override bool NAJIT()
        {
            SearchProductForm form = new SearchProductForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;

                ProductFilter filter = form.Filter;
                if (searchCat == null)
                {
                    searchCat = new Category() { Name="Výsledky hledání", Description="Obsahuje výsledky posledního hledání" };
                    searchCat.Position = -1;

                    ArrayList a = (ArrayList)tvCategory.Objects;
                    a.Insert(0, searchCat);
                    tvCategory.SetObjects(a);
                }
                
                searchSummaries = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, filter);
                
                tvCategory.RefreshObject(searchCat);
                prevSelected = null;
                tvCategory.SelectObject(searchCat, true);

                Cursor.Current = Cursors.Default;

                return true;
            }
            return false;
        }

        private void RedrawItems(Transware.Core.Domain.Category cat)
        {
            if (tvCategory == null || tvCategory.SelectedObject == null)
                return;
            if (((Category)tvCategory.SelectedObject).ID == cat.ID)
            {
                Category c = (Category)tvCategory.SelectedObject;
                UpdateItems(c);
            }
        }

        private void lvItems_BeforeSorting(object sender, BeforeSortingEventArgs e)
        {
            if (lvItems.AllColumns.Count == 0)
            {
                e.Canceled = true;
                return;
            }
            e.Canceled = true;
            //e.ColumnToSort = lvItems.AllColumns[0];
        }

        private void lvItems_ModelCanDrop(object sender, ModelDropEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            if (tvCategory.SelectedObject == null || tvCategory.SelectedObject == searchCat)
                return;

            if (e.SourceModels.Count == 0 || (!(e.SourceModels[0] is ProductSummary)) || e.SourceModels.Count > 1)
                return;

            if (!(e.TargetModel is ProductSummary))
                return;

            ProductSummary source = (ProductSummary)e.SourceModels[0];
            if (e.DropTargetItem != null)
            {
                ProductSummary target = (ProductSummary)e.DropTargetItem.RowObject;
                if (source.ID == target.ID)
                {
                    e.InfoMessage = "Nelze přesouvat produkt sám na sebe";
                    return;
                }
                e.Effect = DragDropEffects.Move;            
            }
            
        }

        private void lvItems_ModelDropped(object sender, ModelDropEventArgs e)
        {
            if (e.SourceModels.Count == 0 || (e.SourceModels[0] is ProductSummary && e.SourceModels.Count > 1))
                return;
            ProductSummary source = (ProductSummary)e.SourceModels[0];
            if (e.DropTargetLocation == DropTargetLocation.Item)
            {
                if (e.DropTargetItem != null)
                {
                    ProductSummary target = (ProductSummary)e.DropTargetItem.RowObject;
                    if (MessageBox.Show("Opravdu přidat produkt jako variantu skladové karty?", "Vytvoření varianty", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                        return;

                    IProductDao dao = Database.Factory.GetProductDao();
                    List<Store> stores = Database.Factory.GetStoreDao().GetAllValid();
                    List<Product> products = new List<Product>();
                    ProductVariant pv = null;
                    try
                    {
                        dao.BeginTransaction();
                        
                        products.Add(dao.GetById(source.ID, false));

                        Product product = dao.GetById(target.ID, false);
                        
                        foreach (Product p in products)
                        {
                            pv = new ProductVariant();
                            pv.Product = product;
                            pv.OrderMark = p.Name;
                            pv.Specification = p.Specification;
                            pv.DetailedSpecification = p.DetailedSpecification;
                            pv.Setting = p.Setting.Clone();
                            pv.UserID1 = p.UserID1;
                            pv.UserID2 = p.UserID2;
                            pv.Provider = p.Provider;
                            foreach (StoreProduct sp in p.StoreProducts.Values)
                            {
                                sp.Product = null;
                                sp.Variant = pv;
                                pv.StoreProducts.Add(sp.Store, sp);
                            }
                            foreach (Package pkg in p.Packages)
                            {
                                pkg.Variant = pv;
                                pkg.Product = product;
                                pv.Packages.Add(pkg);
                            }
                            p.Packages.Clear();
                            foreach (Provider pr in p.Providers)
                            {
                                pr.Variant = pv;
                                pr.Product = null;
                                pv.Providers.Add(pr);
                            }
                            p.Providers.Clear();
                            foreach (StoreProduct spv in pv.StoreProducts.Values)
                            {
                                StoreProduct sp = (StoreProduct)product.StoreProducts[spv.Store];
                                if (sp == null)
                                {
                                    sp = new StoreProduct() { Product = product, Store = spv.Store };
                                    product.StoreProducts.Add(sp.Store, sp);
                                }
                                sp.RealLocked += spv.RealLocked;
                                sp.RealNoReserved += spv.RealNoReserved;
                                sp.RealReserved += spv.RealReserved;
                                sp.RealTotal += spv.RealTotal;
                            }
                            
                            p.Valid = false;
                            product.Variants.Add(pv);
                            dao.SaveOrUpdate(p);
                        }

                        dao.SaveOrUpdate(product);

                        dao.CommitChanges();

                        ProductSummary ps = source;
                        List<Transware.Data.DataTypes.ProductImage> images = ProductImageModel.Instance.GetByProduct(ps.ID);
                        foreach (Transware.Data.DataTypes.ProductImage pi in images)
                        {
                            pi.Product = 0;
                            pi.Variant = pv.ID;
                            ProductImageModel.Instance.SaveOrUpdate(pi);
                        }
                    }
                    catch (Exception ex)
                    {
                        dao.RollbackChanges();
                    }
                    UpdateItems();
                    return;
                }
            }
            else
            {
                if (e.DropTargetItem != null)
                {
                    ProductSummary target = (ProductSummary)e.DropTargetItem.RowObject;
                    int index = e.DropTargetIndex;

                    if (e.DropTargetLocation == DropTargetLocation.BelowItem)
                        index++;

                    IProductDao dao = Database.Factory.GetProductDao();
                    try
                    {
                        List<ProductSummary> products = new List<ProductSummary>();

                        foreach (object o in lvItems.Roots)
                            products.Add((ProductSummary)o);

                        int i = 0;
                        foreach (ProductSummary ps in products)
                        {
                            if (ps == source)
                                ps.PlaceNo = index;
                            else
                                if (i < index)
                                    ps.PlaceNo = i;
                                else
                                    ps.PlaceNo = i + 1;
                            if (lvItems.IsExpanded(ps))
                                i += ps.Variants.Count;
                            i++;
                        }
                        ProductSummaryModel.Instance.UpdatePlaceNo(products);

                        products.Sort(new ProductSummaryPlaceNoComparer());
                        lvItems.SetObjects(products);
                    }
                    catch (Exception ex)
                    {
                        errorLogger.Error(ex.Message);
                        UpdateItems();
                    }
                }
            }
        }

        private class ProductSummaryPlaceNoComparer : IComparer<ProductSummary>
        {
            #region IComparer<ProductSummary> Members

            public int Compare(ProductSummary x, ProductSummary y)
            {
                return x.PlaceNo - y.PlaceNo;
            }

            #endregion
        }

        private void CategoryPositionComparer(OLVColumn column, SortOrder order)
        {
            this.tvCategory.ListViewItemSorter = new CategoryComparer();
        }

        private class CategoryComparer : IComparer
        {
            private IComparer<Category> cmp = new Category.PositionComparer();

            #region IComparer Members

            public int Compare(object x, object y)
            {
                if (!(x is OLVListItem) || !(y is OLVListItem))
                    return 0;
                OLVListItem ix = (OLVListItem)x;
                OLVListItem iy = (OLVListItem)y;
                return cmp.Compare((Category)ix.RowObject, (Category)iy.RowObject);
            }

            #endregion
        }

        private class StoreAspectGetters
        {
            int amount = 0;

            public StoreAspectGetters(int amount)
            {
                this.amount = amount;
            }

            public object PackagePriceNoVat(object x)
            {
                if (x is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)x;

                    if (p.Prices.ContainsKey(amount))
                        if (p.IndividualPrice)
                            return "indiv. cena";
                        else
                            return p.Prices[amount].ToString("F2");
                    else
                        return "";
                }
                else
                {
                    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;

                    if (p.Prices.ContainsKey(amount))
                        if (p.IndividualPrice)
                            return "indiv. cena";
                        else
                            return p.Prices[amount].ToString("F2");
                    else
                        return "";
                }
                //return pkg.PriceNoVatWithDiscount(Database.LoggedStore).ToString("0.00");
            }

            public object PackagePrice(object x)
            {
                if (x is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)x;

                    if (p.Prices.ContainsKey(amount))
                        if (p.IndividualPrice)
                            return "indiv. cena";
                        else
                            return Vat.GetPrice(p.Tax, p.Prices[amount]).ToString("F2");
                    else
                        return "";
                }
                else
                {
                    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)x;

                    if (p.Prices.ContainsKey(amount))
                        if (p.IndividualPrice)
                            return "indiv. cena";
                        else
                            return Vat.GetPrice(p.Tax, p.Prices[amount]).ToString("F2");
                    else
                        return "";
                }
                //return pkg.PriceWithDiscount(Database.LoggedStore).ToString("0.00");
            }
        }

        private void tvCategory_BeforeSorting(object sender, BeforeSortingEventArgs e)
        {
            //e.Canceled = true;
            e.ColumnToSort = olvcCategory;
        }

        private void tvCategory_CellToolTipShowing(object sender, ToolTipShowingEventArgs e)
        {
            Category c = (Category)e.Model;
            e.Text = c.Description;
        }

        private void tvCategory_SelectionChanged(object sender, EventArgs e)
        {
            if (!ForSelection)
            {
                if (tvCategory.SelectedObject != null)
                {
                    if (prevSelected != tvCategory.SelectedObject)
                    {
                        prevSelected = (Category)tvCategory.SelectedObject;
                        UpdateItems();
                    }
                    tbFilter.Text = "";
                    TB_DETAIL.Enabled = false;
                    TB_OPRAVIT.Enabled = Database.HasPermission(Permissions.ProductsCategoryEdit);
                    TB_VYRADIT.Enabled = (((Category)tvCategory.SelectedObject).ParentID == 0) ? false : Database.HasPermission(Permissions.ProductsCategoryDel);
                }
                else
                {
                    prevSelected = null;
                    tbFilter.Text = "";
                    UpdateItems();
                    TB_OPRAVIT.Enabled = false;
                    TB_VYRADIT.Enabled = false;
                    TB_DETAIL.Enabled = false;
                }
            }
            else
            {
                //if (tvCategory.SelectedObject != null)
                //{
                    tbFilter.Text = "";
                    UpdateItems();
                //}
            }
        }

        private void lvItems_Enter(object sender, EventArgs e)
        {
            if (ForSelection)
            {
                if (lvItems.SelectedObjects.Count != 1)
                {
                    TB_VYBRAT.Enabled = false;
                    TB_DETAIL.Enabled = false;
                    return;
                }

                TB_VYBRAT.Enabled = true;
                TB_DETAIL.Enabled = Database.HasPermission(Permissions.ProductsProductShow);
            }
            else
            {
                if (lvItems.SelectedObjects.Count == 0)
                {
                    TB_VYRADIT.Enabled = false;
                    TB_OPRAVIT.Enabled = false;
                    TB_DETAIL.Enabled = false;
                }
                else
                {
                    TB_VYRADIT.Enabled = Database.HasPermission(Permissions.ProductsProductDel);
                    TB_DETAIL.Enabled = Database.HasPermission(Permissions.ProductsProductShow);
                    TB_OPRAVIT.Enabled = false;
                }
            } 
        }

        private void tvCategory_ModelCanDrop(object sender, ModelDropEventArgs e)
        {
            e.Effect=DragDropEffects.None;
            if (e.TargetModel == null || e.TargetModel==searchCat)
                return;

            if (e.SourceListView == lvItems || e.SourceListView == tvCategory)
            {
                e.Effect = DragDropEffects.Move;
                if (e.SourceListView == tvCategory)
                {                 
                    e.DropSink.CanDropBetween = true;
                    e.DropSink.CanDropOnItem = true;
                    
                    //e.InfoMessage = "Sortiment bude vložen na zvolenou pozici";
                    Category source = (Category)e.SourceModels[0];
                    Category target = (Category)e.TargetModel;
                    if (target.Parent == null && e.DropTargetLocation != DropTargetLocation.Item)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Sortiment nelze umístit na tuto uroveň";
                        return;
                    }
                    if (source.Parent == null)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Tento sortiment nelze přemisťovat";
                        return;
                    }
                    if (source.Parent == target && e.DropTargetLocation==DropTargetLocation.Item)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Sortiment již v této složce je";
                        return;
                    }
                    if (source == target)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Sortiment nelze vložit sám do sebe";
                        return;
                    }
                    if (source == searchCat)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Výsledky hledání nelze přesouvat";
                        return;
                    }
                }
                else
                {
                    if (tvCategory.SelectedObject == searchCat)
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Vyhledané produkty nelze přesouvat";
                        return;
                    }
                    if (!(e.SourceModels[0] is ProductSummary))
                    {
                        e.Effect = DragDropEffects.None;
                        e.InfoMessage = "Varianty nelze přeřazovat do kategorií.";
                        return;
                    }
                    Category c = (Category)e.TargetModel;
                    ProductSummary p = (ProductSummary)e.SourceModels[0];
                    if (c.ID == p.Category)
                    {
                        if (e.SourceModels.Count == 1)
                            e.InfoMessage = "Produkt nelze vložit do sortimentu kde již je";
                        else
                            e.InfoMessage = "Produkty nelze vložit do sortimentu kde již jsou";
                        e.Effect = DragDropEffects.None;
                        return;
                    }
                    e.DropSink.CanDropBetween = false;
                    e.DropSink.CanDropOnItem = true;
                    
                }
            }
        }

        private void tvCategory_ModelDropped(object sender, ModelDropEventArgs e)
        {
            if (e.SourceListView == lvItems)
            {
                try
                {
                    Category c = (Category)e.TargetModel;
                    
                    List<ProductSummary> cp = ProductSummaryModel.Instance.GetList(Database.LoggedStore.ID, c.ID);
                    List<ProductSummary> products = new List<ProductSummary>();
                    
                    foreach (Object o in e.SourceModels)
                    {
                        products.Add((ProductSummary)o);
                    }
                    
                    int pos = cp.Count;
                    foreach (ProductSummary p in products)
                    {
                        p.PlaceNo = pos++;
                        p.Category = c.ID;
                    }
                    ProductSummaryModel.Instance.UpdateCategoryAndPlaceNo(products);

                    foreach (Object o in e.SourceModels)
                        e.SourceListView.RemoveObject(o);

                    tvCategory.RefreshObject(c);
                    if (c.Parent != null)
                        tvCategory.RefreshObject(c.Parent);
                }
                catch (Exception ex)
                {

                }
            }
            if (e.SourceListView == tvCategory)
            {
                Category target = (Category)e.TargetModel;
                Category source = (Category)e.SourceModels[0];
              
                #region Same parent for source and target, Location is between items - reorder items
                if (source.Parent == target.Parent && e.DropTargetLocation != DropTargetLocation.Item)
                {
                    List<Category> categories = target.Parent.Subcategories;
                    categories.Sort(new Category.PositionComparer());
                    int index = categories.IndexOf(target);

                    if (e.DropTargetLocation == DropTargetLocation.BelowItem)
                        index++;

                    try
                    {
                        for (int i = 0; i < categories.Count; i++)
                        {
                            if (categories[i] == source)
                                categories[i].Position = index;
                            else
                                if (i < index)
                                    categories[i].Position = i;
                                else
                                    categories[i].Position = i + 1;
                            CategoryModel.Instance.SaveOrUpdate(categories[i]);
                        }
                        categories.Sort(new Category.PositionComparer());
                        tvCategory.RefreshObject(target.Parent);

                    }
                    catch (Exception ex)
                    {
                        errorLogger.Error(ex.Message);
                        UpdateItems();
                    }
                    return;
                }
                #endregion // same parents

                #region Location is between items, parents are different
                if (e.DropTargetLocation != DropTargetLocation.Item)
                {
                    List<Category> categories = target.Parent.Subcategories;
                    categories.Sort(new Category.PositionComparer());
                    int index = categories.IndexOf(target);

                    if (e.DropTargetLocation == DropTargetLocation.BelowItem)
                        index++;

                    try
                    {
                        for (int i = 0; i < categories.Count; i++)
                        {
                            if (i < index)
                                categories[i].Position = i;
                            else
                                categories[i].Position = i + 1;
                            CategoryModel.Instance.SaveOrUpdate(categories[i]);
                        }
                        Category sourceParent = source.Parent;

                        source.Parent.Subcategories.Remove(source);
                        source.Parent = target.Parent;
                        target.Parent.Subcategories.Add(source);
                        source.Position = index;
                        CategoryModel.Instance.SaveOrUpdate(source);
                        categories.Sort(new Category.PositionComparer());
                        tvCategory.RefreshObject(source.Parent);
                        tvCategory.RefreshObject(sourceParent);
                    }
                    catch (Exception ex)
                    {
                        errorLogger.Error(ex.Message);
                        UpdateItems();
                    }
                    return;
                }
                #endregion

                if (e.DropTargetLocation == DropTargetLocation.Item)
                {
                    List<Category> categories = target.Subcategories;
                    categories.Sort(new Category.PositionComparer());
                    int index = 0;

                    try
                    {
                        for (int i = 0; i < categories.Count; i++)
                        {
                            categories[i].Position = index++;
                            CategoryModel.Instance.SaveOrUpdate(categories[i]);
                        }
                        Category sourceParent = source.Parent;

                        source.Parent.Subcategories.Remove(source);
                        source.Parent = target;
                        source.Parent.Subcategories.Add(source);
                        source.Position = index;
                        CategoryModel.Instance.SaveOrUpdate(source);
                        categories.Sort(new Category.PositionComparer());
                        tvCategory.RefreshObject(source.Parent);
                        tvCategory.RefreshObject(sourceParent);
                    }
                    catch (Exception ex)
                    {
                        errorLogger.Error(ex.Message);
                        UpdateItems();
                    }
                    return;
                }
            }
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            //Text = splitContainer1.SplitterDistance.ToString();
        }

        private void StoreCardSelectForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //NHibernateSessionManager.Instance.GetSession().SessionFactory.Evict(typeof(Product));
        }

        private void lvItems_CellRightClick(object sender, CellRightClickEventArgs e)
        {
            ContextMenuStrip cms = new ContextMenuStrip();
            e.MenuStrip = cms;

            if (config.GetField(ConfigClass.ConfigFields.UseOrderMark) == "1")
            {
                int productVariantCount = 0;
                foreach (object o in lvItems.SelectedObjects)
                {
                    if (o is ProductSummary && (o as ProductSummary).Variants.Count!=0)
                        productVariantCount++;
                    if (o is ProductSummary.ProductVariantSummary)
                        productVariantCount++;
                }
                if (productVariantCount == 0)
                {
                    if (lvItems.SelectedObjects.Count > 1)
                    {
                        ToolStripMenuItem tsi = new ToolStripMenuItem();
                        tsi.Text = "Sloučit skladové karty jako varianty nového produktu?";
                        tsi.Click += delegate(object xsender, EventArgs xe)
                        {
                            if (MessageBox.Show("Opravdu sloučit produkty jako varianty nového produktu?", "Sloučení produktů", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                return;

                            List<ProductSummary> summaries = new List<ProductSummary>();
                            foreach (object o in lvItems.SelectedObjects)
                            {
                                summaries.Add(o as ProductSummary);
                            }
                            MergeProducts(summaries);
                            UpdateItems();
                        };
                        cms.Items.Add(tsi);
                    }
                    else
                    {
                        ToolStripMenuItem tsi = new ToolStripMenuItem();
                        tsi.Text = "Převést skladovou kartu na variantu nového produktu?";
                        tsi.Click += delegate(object xsender, EventArgs xe)
                        {
                            if (MessageBox.Show("Opravdu převést skladovou kartu na variantu nového produktu?", "Převod produktu", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                return;

                            List<ProductSummary> summaries = new List<ProductSummary>();
                            foreach (object o in lvItems.SelectedObjects)
                            {
                                summaries.Add(o as ProductSummary);
                            }
                            MergeProducts(summaries);
                            UpdateItems();
                        };
                        cms.Items.Add(tsi);
                    }
                }
            }
            
            if (e.Model is ProductSummary && config.GetField(ConfigClass.ConfigFields.UseOrderMark)=="1")
            {
                ProductSummary ps = e.Model as ProductSummary;
                ToolStripMenuItem tsi = new ToolStripMenuItem();
                tsi.Text = "Vytvořit variantu skladové karty";
                tsi.Click += delegate(object xsender, EventArgs xe) {
                    Product p = Database.Factory.GetProductDao().GetById(ps.ID, false);
                    ProductVariantForm form = new ProductVariantForm(p, null, true);
                    form.ShowDialog();
                    UpdateItems();
                };
                cms.Items.Add(tsi);
            }

            if (cms.Items.Count == 0)
                e.MenuStrip = null;
        }

        private void MergeProducts(List<ProductSummary> summaries)
        {
            List<Product> products = new List<Product>();

            IProductDao dao = Database.Factory.GetProductDao();
            List<Store> stores = Database.Factory.GetStoreDao().GetAllValid();
                
            try
            {
                dao.BeginTransaction();
                foreach (ProductSummary ps in summaries)
                    products.Add(dao.GetById(ps.ID, false));

                Product copyFrom = products[0];
                Product product = new Product();
                product.Category = copyFrom.Category;
                product.Name = copyFrom.Name;
                product.ShortName = copyFrom.ShortName; ;
                product.Specification = copyFrom.Specification; ;
                product.Vat = copyFrom.Vat;
                product.CTax = copyFrom.CTax;
                product.Unit = copyFrom.Unit;
                product.TimePeriod = copyFrom.TimePeriod;
                product.PlaceNo = copyFrom.PlaceNo;
                foreach (Object o in copyFrom.NameLang.Keys)
                {
                    int key = (int)o;
                    product.NameLang.Add(key, copyFrom.NameLang[key]);
                }
                product.Setting.Action = copyFrom.Setting.Action;
                product.Setting.Actual = copyFrom.Setting.Actual;
                product.Setting.Catalog = copyFrom.Setting.Catalog;
                product.Setting.Commissional = copyFrom.Setting.Commissional;
                product.Setting.IndividualPrice = copyFrom.Setting.IndividualPrice;
                product.Setting.Internet = copyFrom.Setting.Internet;
                product.Setting.OverHead = copyFrom.Setting.OverHead;
                product.Setting.Prescription = copyFrom.Setting.Prescription;
                product.Setting.ShowOnTitlePage = copyFrom.Setting.ShowOnTitlePage;
                product.Setting.ShowPricesOnWeb = copyFrom.Setting.ShowPricesOnWeb;
                product.Setting.Tandem = copyFrom.Setting.Tandem;
                product.Setting.UndefinedAmount = copyFrom.Setting.UndefinedAmount;
                product.Setting.VariableLength = copyFrom.Setting.VariableLength;
                product.Setting.Voucher = copyFrom.Setting.Voucher;
                
                foreach (Product p in products)
                {
                    ProductVariant pv = new ProductVariant();
                    pv.Product = product;
                    pv.OrderMark = p.Name;
                    pv.Specification = p.Specification;
                    pv.DetailedSpecification = p.DetailedSpecification;
                    pv.Setting = p.Setting.Clone();
                    pv.UserID1 = p.UserID1;
                    pv.UserID2 = p.UserID2;
                    pv.Provider = p.Provider;
                    foreach (StoreProduct sp in p.StoreProducts.Values)
                    {
                        sp.Product = null;
                        sp.Variant = pv;
                        pv.StoreProducts.Add(sp.Store, sp);
                    }
                    foreach (Package pkg in p.Packages)
                    {
                        pkg.Variant = pv;
                        pkg.Product = product;
                        pv.Packages.Add(pkg);
                    }
                    p.Packages.Clear();
                    foreach (Provider pr in p.Providers)
                    {
                        pr.Variant = pv;
                        pr.Product = null;
                        pv.Providers.Add(pr);
                    }
                    p.Providers.Clear();
                    p.Valid = false;
                    product.Variants.Add(pv);
                    dao.SaveOrUpdate(p);
                }

                foreach (Store s in stores)
                {
                    StoreProduct sp = new StoreProduct() { Product = product, Store=s };
                    foreach (ProductVariant pv in product.Variants)
                    {
                        StoreProduct spv = (StoreProduct)pv.StoreProducts[s];
                        if (spv == null)
                        {
                            spv = new StoreProduct() { Variant = pv, Store = s };
                        }
                        sp.RealLocked += spv.RealLocked;
                        sp.RealNoReserved += spv.RealNoReserved;
                        sp.RealReserved += spv.RealReserved;
                        sp.RealTotal += spv.RealTotal;
                    }
                    product.StoreProducts.Add(s, sp);
                }

                dao.SaveOrUpdate(product);

                dao.CommitChanges();

                for(int i=0; i<product.Variants.Count; i++)
                {
                    ProductSummary ps = summaries[i];
                    List<Transware.Data.DataTypes.ProductImage> images = ProductImageModel.Instance.GetByProduct(ps.ID);
                    foreach (Transware.Data.DataTypes.ProductImage pi in images)
                    {
                        pi.Product = 0;
                        pi.Variant = product.Variants[i].ID;
                        ProductImageModel.Instance.SaveOrUpdate(pi);
                    }
                }
            }
            catch (Exception e)
            {
                dao.RollbackChanges();
            }
        }


        protected override bool NAHLED()
        {
            List<ProductSummary> products = new List<ProductSummary>();
            if (lvItems.Roots != null)
                foreach (object o in lvItems.Roots)
                    products.Add(o as ProductSummary);

            new PrintUtil().Preview<ProductSummary>(products, new ProductSummaryFormatter(amounts, pricesWithVat));
            return true;
        }

        protected override bool TISK()
        {
            List<ProductSummary> products = new List<ProductSummary>();
            if (lvItems.Roots != null)
                foreach (object o in lvItems.Roots)
                    products.Add(o as ProductSummary);

            new PrintUtil().Print<ProductSummary>(products, new ProductSummaryFormatter(amounts, pricesWithVat));
            return true;
        }

        class ProductSummaryFormatter : AbstractListReportFormatter<ProductSummary>
        {
            delegate object ValueGetter(object x);
            private bool useUserId = false;
            
            private List<ValueGetter> getters = new List<ValueGetter>();
            public ProductSummaryFormatter(decimal[] amounts, bool pricewWithVat)
                : base("Sortiment", "Sortiment", 6, new float[] { 20, 10, 10, 10, 10, 10 },
                    new string[] { "Název", "Zkratka", "Zaokrouhlení dokladů", "Des. míst", "Zaokrouhlení balení", "Des. míst" },
                new CellAligment[] { CellAligment.Left, CellAligment.Left, CellAligment.Left, CellAligment.Right, CellAligment.Left, CellAligment.Right })
            {
                ConfigClass config = Database.Factory.GetConfigClassDao().GetConfigutation();

                List<float> widths = new List<float>();
                List<string> names = new List<string>();
                List<CellAligment> alligments = new List<CellAligment>();
                names.Add("Název");
                widths.Add(20);
                alligments.Add(CellAligment.Left);
                foreach (decimal amount in amounts)
                {
                    if (pricewWithVat)
                    {
                        names.Add("Cena/" + amount + " Mj s DPH");
                        widths.Add(12);
                        alligments.Add(CellAligment.Right);
                        getters.Add(new StoreAspectGetters((int)amount).PackagePrice);
                    }
                    else
                    {
                        names.Add("Cena/" + amount + " Mj bez DPH");
                        widths.Add(12);
                        alligments.Add(CellAligment.Right);
                        getters.Add(new StoreAspectGetters((int)amount).PackagePriceNoVat);
                    }
                }
                names.Add("Bez rezervace");
                widths.Add(12);
                alligments.Add(CellAligment.Right);

                names.Add("Rezervováno");
                widths.Add(12);
                alligments.Add(CellAligment.Right);

                names.Add("Neuvolněno");
                widths.Add(12);
                alligments.Add(CellAligment.Right);

                names.Add("Celkem na skladě");
                widths.Add(12);
                alligments.Add(CellAligment.Right);

                if (config.GetField(ConfigClass.ConfigFields.UseUserID1) == "1")
                {
                    names.Add(config.GetField(ConfigClass.ConfigFields.UserID1Name) ?? "Uživatelské ID:");
                    widths.Add(12);
                    alligments.Add(CellAligment.Left);
                    useUserId = true;
                }

                this.Columns = widths.ToArray();
                this.ColumnNames = names.ToArray();
                this.ColumnCount = widths.Count;
                this.Aligments = alligments.ToArray();
            }

            public override string[] Values(ProductSummary o)
            {
                List<string> vals = new List<string>();
                vals.Add(o.Name);

                foreach (ValueGetter getter in getters)
                {
                    vals.Add(getter(o).ToString());
                }

                //if (o is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)o;
                    if (p.UndefinedAmount)
                        vals.Add("nedef. množství");
                    else
                        vals.Add(p.RealNoReserved.ToString("0.#####"));
                }
                //else
                //{
                //    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)o;
                //    if (p.UndefinedAmount)
                //        vals.Add("nedef. množství");
                //    else
                //        vals.Add(p.RealNoReserved.ToString("0.#####"));
                //}

                //if (o is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)o;
                    if (p.UndefinedAmount)
                        vals.Add("nedef. množství");
                    else
                        vals.Add(p.RealReserved.ToString("0.#####"));
                }
                //else
                //{
                //    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)o;
                //    if (p.UndefinedAmount)
                //        vals.Add("nedef. množství");
                //    else
                //        vals.Add(p.RealReserved.ToString("0.#####"));
                //}

                //if (o is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)o;
                    if (p.UndefinedAmount)
                        vals.Add("nedef. množství");
                    else
                        vals.Add(p.RealLocked.ToString("0.#####"));
                }
                //else
                //{
                //    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)o;
                //    if (p.UndefinedAmount)
                //        vals.Add("nedef. množství");
                //    else
                //        vals.Add(p.RealLocked.ToString("0.#####"));
                //}

                //if (o is ProductSummary)
                {
                    ProductSummary p = (ProductSummary)o;
                    if (p.UndefinedAmount)
                        vals.Add("nedef. množství");
                    else
                        vals.Add(p.RealTotal.ToString("0.#####"));
                }
                //else
                //{
                //    ProductSummary.ProductVariantSummary p = (ProductSummary.ProductVariantSummary)o;
                //    if (p.UndefinedAmount)
                //        vals.Add("nedef. množství");
                //    else
                //        vals.Add(p.RealTotal.ToString("0.#####"));
                //}

                if (useUserId)
                {
                    vals.Add((o as ProductSummary).UserID1);
                }
                return vals.ToArray();
            }
        }
    }
}
