﻿using BrightIdeasSoftware;
namespace Transware.Net.Forms.Stores
{
    partial class StoreCardSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreCardSelectForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tvCategory = new BrightIdeasSoftware.TreeListView();
            this.olvcCategory = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.lvItems = new BrightIdeasSoftware.TreeListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tvCategory);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lvItems);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(838, 493);
            this.splitContainer1.SplitterDistance = 215;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // tvCategory
            // 
            this.tvCategory.AllColumns.Add(this.olvcCategory);
            this.tvCategory.AllowDrop = true;
            this.tvCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvCategory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvcCategory});
            this.tvCategory.FullRowSelect = true;
            this.tvCategory.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.tvCategory.HideSelection = false;
            this.tvCategory.IsSimpleDragSource = true;
            this.tvCategory.IsSimpleDropSink = true;
            this.tvCategory.Location = new System.Drawing.Point(0, 0);
            this.tvCategory.MultiSelect = false;
            this.tvCategory.Name = "tvCategory";
            this.tvCategory.OwnerDraw = true;
            this.tvCategory.ShowGroups = false;
            this.tvCategory.ShowItemToolTips = true;
            this.tvCategory.Size = new System.Drawing.Size(215, 493);
            this.tvCategory.SmallImageList = this.imageList;
            this.tvCategory.StateImageList = this.imageList;
            this.tvCategory.TabIndex = 0;
            this.tvCategory.UseCompatibleStateImageBehavior = false;
            this.tvCategory.View = System.Windows.Forms.View.Details;
            this.tvCategory.VirtualMode = true;
            this.tvCategory.BeforeSorting += new System.EventHandler<BrightIdeasSoftware.BeforeSortingEventArgs>(this.tvCategory_BeforeSorting);
            this.tvCategory.CellToolTipShowing += new System.EventHandler<BrightIdeasSoftware.ToolTipShowingEventArgs>(this.tvCategory_CellToolTipShowing);
            this.tvCategory.ModelCanDrop += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.tvCategory_ModelCanDrop);
            this.tvCategory.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.tvCategory_ModelDropped);
            this.tvCategory.SelectionChanged += new System.EventHandler(this.tvCategory_SelectionChanged);
            this.tvCategory.Enter += new System.EventHandler(this.tvCategory_SelectionChanged);
            this.tvCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvCategory_KeyDown);
            // 
            // olvcCategory
            // 
            this.olvcCategory.AspectName = "Name";
            this.olvcCategory.FillsFreeSpace = true;
            this.olvcCategory.Text = "Název";
            this.olvcCategory.Width = 100;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Folder.png");
            this.imageList.Images.SetKeyName(1, "document.bmp");
            this.imageList.Images.SetKeyName(2, "FolderY.png");
            this.imageList.Images.SetKeyName(3, "FolderYInternet.png");
            this.imageList.Images.SetKeyName(4, "Internet.png");
            this.imageList.Images.SetKeyName(5, "InternetNo.png");
            // 
            // lvItems
            // 
            this.lvItems.AllowDrop = true;
            this.lvItems.AlternateRowBackColor = System.Drawing.Color.Gainsboro;
            this.lvItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvItems.EmptyListMsg = "Seznam produktů je prázdný";
            this.lvItems.FullRowSelect = true;
            this.lvItems.HideSelection = false;
            this.lvItems.IsSearchOnSortColumn = false;
            this.lvItems.IsSimpleDragSource = true;
            this.lvItems.IsSimpleDropSink = true;
            this.lvItems.Location = new System.Drawing.Point(2, 31);
            this.lvItems.Name = "lvItems";
            this.lvItems.OwnerDraw = true;
            this.lvItems.SelectColumnsOnRightClick = false;
            this.lvItems.ShowGroups = false;
            this.lvItems.ShowSortIndicators = false;
            this.lvItems.Size = new System.Drawing.Size(614, 459);
            this.lvItems.SmallImageList = this.imageList;
            this.lvItems.SortGroupItemsByPrimaryColumn = false;
            this.lvItems.TabIndex = 2;
            this.lvItems.UseAlternatingBackColors = true;
            this.lvItems.UseCompatibleStateImageBehavior = false;
            this.lvItems.View = System.Windows.Forms.View.Details;
            this.lvItems.VirtualMode = true;
            this.lvItems.BeforeSorting += new System.EventHandler<BrightIdeasSoftware.BeforeSortingEventArgs>(this.lvItems_BeforeSorting);
            this.lvItems.CellRightClick += new System.EventHandler<BrightIdeasSoftware.CellRightClickEventArgs>(this.lvItems_CellRightClick);
            this.lvItems.ModelCanDrop += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.lvItems_ModelCanDrop);
            this.lvItems.ModelDropped += new System.EventHandler<BrightIdeasSoftware.ModelDropEventArgs>(this.lvItems_ModelDropped);
            this.lvItems.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvItems_ColumnClick);
            this.lvItems.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.lvItems_ColumnWidthChanged);
            this.lvItems.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvItems_ItemSelectionChanged);
            this.lvItems.DoubleClick += new System.EventHandler(this.lvItems_DoubleClick);
            this.lvItems.Enter += new System.EventHandler(this.lvItems_Enter);
            this.lvItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvItems_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbFilter);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(619, 29);
            this.panel1.TabIndex = 1;
            // 
            // tbFilter
            // 
            this.tbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilter.Location = new System.Drawing.Point(38, 5);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(578, 20);
            this.tbFilter.TabIndex = 1;
            this.tbFilter.TextChanged += new System.EventHandler(this.tbFilter_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filtr:";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // StoreCardSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 520);
            this.Controls.Add(this.splitContainer1);
            this.Name = "StoreCardSelectForm";
            this.Text = "Katalog skladových karet";
            this.Activated += new System.EventHandler(this.StoreCardSelectForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StoreCardSelectForm_FormClosed);
            this.Shown += new System.EventHandler(this.StoreCardSelectForm_Shown);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private TreeListView tvCategory;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.Label label1;
        private TreeListView lvItems;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private OLVColumn olvcCategory;
    }
}