﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Transware.Plugins;

namespace Transware.Net
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void bFindPlugins_Click(object sender, EventArgs e)
        {
            string pluginDir = Application.StartupPath + @"\Plugins\Pos";
            List<IPrinterPlugin> plugins = new List<IPrinterPlugin>();

            if (Directory.Exists(pluginDir))
            {
                string[] files = Directory.GetFiles(pluginDir, "*.dll");

                for (int i = 0; i < files.Length; i++)
                {
                    try
                    {
                        Assembly asm = Assembly.LoadFile(files[i]);

                        Type[] exportedTypes = asm.GetExportedTypes();

                        foreach (Type exportedType in exportedTypes)
                        {
                            if (typeof(IPrinterPlugin).IsAssignableFrom(exportedType))
                            {
                                IPrinterPlugin plugin = (IPrinterPlugin)asm.CreateInstance(exportedType.FullName);

                                if (plugin != null)
                                    plugins.Add(plugin);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Chyba při načítání pluginu " + files[i] + ": " + ex.Message, "PluginApp", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
