﻿namespace Transware.Net
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bFindPlugins = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bFindPlugins
            // 
            this.bFindPlugins.Location = new System.Drawing.Point(35, 61);
            this.bFindPlugins.Name = "bFindPlugins";
            this.bFindPlugins.Size = new System.Drawing.Size(75, 23);
            this.bFindPlugins.TabIndex = 0;
            this.bFindPlugins.Text = "FindPlugins";
            this.bFindPlugins.UseVisualStyleBackColor = true;
            this.bFindPlugins.Click += new System.EventHandler(this.bFindPlugins_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.bFindPlugins);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Transware.iNet";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bFindPlugins;
    }
}

