﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace Transware.Plugins.Managers
{
    /// <summary>   Abstract class for all plugin managers.  </summary>
    ///
    /// <remarks>   Jan Platos, 03.08.2011. </remarks>
    ///
    /// <typeparam name="T">    Generic type parameter.</typeparam>
    public abstract class PluginManager<T>
    {
        #region Inner fields

        /// <summary> The plugins managed by this plugin manager </summary>
        protected List<T> plugins = new List<T>();

        /// <summary> Name of the registry key </summary>
        protected string registryKeyName = "";

        #endregion


        #region Properties

        /// <summary>   Gets the plugins. </summary>
        ///
        /// <value> The plugins. </value>
        public List<T> Plugins { get { return plugins; } }

        #endregion


        #region Public methods

        /// <summary>   Loads the plugins. </summary>
        ///
        /// <param name="folder">   Pathname of the folder.</param>
        public void LoadPlugins(string folder)
        {
            if (Directory.Exists(folder))
            {
                string[] files = Directory.GetFiles(folder, "*.dll");

                for (int i = 0; i < files.Length; i++)
                {
                    try
                    {
                        Assembly asm = Assembly.LoadFile(files[i]);

                        Type[] exportedTypes = asm.GetExportedTypes();

                        foreach (Type exportedType in exportedTypes)
                        {
                            if (typeof(T).IsAssignableFrom(exportedType))
                            {
                                T plugin = (T)asm.CreateInstance(exportedType.FullName);

                                if (plugin != null)
                                    plugins.Add(plugin);
                            }
                        }
                    }
                    catch 
                    {
                        
                    }
                }
            }
        }

        #endregion


    }
}
