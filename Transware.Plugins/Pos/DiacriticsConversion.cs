﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transware.Plugins.Pos
{
    /// <summary>
    /// 
    /// </summary>
    public class DiacriticsConversion
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static byte RemoveDiacritics(char c)
        {
            switch (c)
            {
                case 'á': return (byte)'a';
                case 'Á': return (byte)'A';
                case 'č': return (byte)'c';
                case 'Č': return (byte)'C';
                case 'ď': return (byte)'d';
                case 'Ď': return (byte)'D';
                case 'é': return (byte)'e';
                case 'ě': return (byte)'e';
                case 'É': return (byte)'E';
                case 'Ě': return (byte)'E';
                case 'í': return (byte)'i';
                case 'Í': return (byte)'I';
                case 'ň': return (byte)'n';
                case 'Ň': return (byte)'N';
                case 'ó': return (byte)'o';
                case 'Ó': return (byte)'O';
                case 'ř': return (byte)'r';
                case 'Ř': return (byte)'R';
                case 'š': return (byte)'s';
                case 'Š': return (byte)'S';
                case 'ť': return (byte)'t';
                case 'Ť': return (byte)'T';
                case 'ú': return (byte)'u';
                case 'ů': return (byte)'u';
                case 'Ú': return (byte)'U';
                case 'Ů': return (byte)'U';
                case 'ý': return (byte)'y';
                case 'Ý': return (byte)'Y';
                case 'ž': return (byte)'z';
                case 'Ž': return (byte)'Z';
                default: return (byte)c;
            }

        }

        public static string RemoveDiacritics(string p)
        {
            string s = "";
            for (int i = 0; i < p.Length; i++)
            {
                s += RemoveDiacriticsChar(p[i]);
            }
            return s;
        }

        public static char RemoveDiacriticsChar(char c)
        {
            switch (c)
            {
                case 'á': return 'a';
                case 'Á': return 'A';
                case 'č': return 'c';
                case 'Č': return 'C';
                case 'ď': return 'd';
                case 'Ď': return 'D';
                case 'é': return 'e';
                case 'ě': return 'e';
                case 'É': return 'E';
                case 'Ě': return 'E';
                case 'í': return 'i';
                case 'Í': return 'I';
                case 'ň': return 'n';
                case 'Ň': return 'N';
                case 'ó': return 'o';
                case 'Ó': return 'O';
                case 'ř': return 'r';
                case 'Ř': return 'R';
                case 'š': return 's';
                case 'Š': return 'S';
                case 'ť': return 't';
                case 'Ť': return 'T';
                case 'ú': return 'u';
                case 'ů': return 'u';
                case 'Ú': return 'U';
                case 'Ů': return 'U';
                case 'ý': return 'y';
                case 'Ý': return 'Y';
                case 'ž': return 'z';
                case 'Ž': return 'Z';
                default: return c;
            }
        }
    }
}
 