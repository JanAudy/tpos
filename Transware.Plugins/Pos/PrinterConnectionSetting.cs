﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.IO;
using System.Xml;

namespace Transware.Plugins
{
    /// <summary>
    /// Printer Connection setting represents all possible connection setting
    /// </summary>
    public class PrinterConnectionSetting
    {
        #region Properties

        /// <summary>
        /// Refines selected connection methods
        /// </summary>
        public EPrinterConnectionMethod Method { get; set; }

        /// <summary>
        /// Defines setting of serial port
        /// </summary>
        public SerialPortSetting SerialPort { get; set; }

        /// <summary>
        /// Defines setting of parallel port
        /// </summary>
        public ParalelPortSetting ParalelPort { get; set; }

        /// <summary>
        /// Defines setting of connection through the printer
        /// </summary>
        public PrinterSetting Printer { get; set; }

        /// <summary>
        /// Defines setting using network
        /// </summary>
        public NetworkSetting Network { get; set; }

        /// <summary>
        /// Defines connection through the bluetooth
        /// </summary>
        public BluetoothSetting Bluetooth { get; set; }

        #endregion

        #region Constructor

        public PrinterConnectionSetting()
        {
            Method = EPrinterConnectionMethod.None;
            SerialPort = new SerialPortSetting() { Name = "COM1", BaudRate = 9600, Handshake = Handshake.None, DataBits = 8, Parity = Parity.None, StopBits = StopBits.One, Timeout = 500 };
            ParalelPort = new ParalelPortSetting() { Name = "LPT1" };
            Printer = new PrinterSetting() { Name = "" };
            Network = new NetworkSetting() { Address = "127.0.0.1", Port = 80 };
            Bluetooth = new BluetoothSetting() { Name = "" };
        }

        #endregion

        #region Load and store methods for string data

        public void Load(string data)
        {
            MemoryStream ms = new MemoryStream();
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            ms.Write(bytes, 0, bytes.Length);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(data);

            XmlElement root = xml.DocumentElement;
            if (root.Attributes.Count == 1 && root.Attributes[0].Name == "Method")
                Method = (EPrinterConnectionMethod)int.Parse(root.Attributes[0].Value);

            foreach (XmlElement element in root.ChildNodes)
            {
                switch (element.Name)
                {
                    case "SerialPortSetting": SerialPort.Load(element); break;
                    case "ParalelPortSetting": ParalelPort.Load(element); break;
                    case "PrinterSetting": Printer.Load(element); break;
                    case "NetworkSetting": Network.Load(element); break;
                    case "BluetoothSetting": Bluetooth.Load(element); break;
                }
            }
        }

        public string Save()
        {
            XmlDocument xml = new XmlDocument();
            XmlElement root = xml.CreateElement("PrinterConnectionSetting");
            xml.AppendChild(root);

            XmlAttribute attr = xml.CreateAttribute("Method");
            attr.Value = ((int)Method).ToString();
            root.Attributes.Append(attr);

            root.AppendChild(SerialPort.Save(xml));
            root.AppendChild(ParalelPort.Save(xml));
            root.AppendChild(Printer.Save(xml));
            root.AppendChild(Network.Save(xml));
            root.AppendChild(Bluetooth.Save(xml));

            MemoryStream ms = new MemoryStream();
            xml.Save(ms);
            return Encoding.UTF8.GetString(ms.GetBuffer());
        }

        #endregion
    }
}
