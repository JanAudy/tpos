﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using System.IO.Ports;
using System.IO;
using System.Net.Sockets;
using System.Drawing;
using System.Drawing.Imaging;

namespace Transware.Plugins.Pos
{
    public abstract class BasicPosPrinter : IPrinterPlugin
    {
        protected PrinterConnectionSetting setting = new PrinterConnectionSetting();

        protected virtual int SerialPortLineDelay { get { return 50; } }
        protected virtual int SerialPortFinalDelay { get { return 300; } }
        protected virtual int NetworkLineDelay { get { return 20; } }
        protected virtual int NetworkFinalDelay { get { return 300; } }

        #region IPrinterPlugin Members

        public abstract string PrinterName
        {
            get;
        }

        public virtual int PageWidth
        {
            get { return 48; }
        }

        public virtual bool TitleReduceLineWidth
        {
            get { return true; }
        }

        public virtual bool SupportBarcodes
        {
            get { return false; }
        }

        public virtual bool SupportDiacritics
        {
            get { return false; }
        }

        public virtual bool SupportGraphics
        {
            get { return false; }
        }

        public virtual int ImageMaxWidth
        {
            get { return 384; }
        }

        public virtual int ImageMaxHeight
        {
            get { return 248; }
        }

        public virtual int ImageWidthAligment
        {
            get { return 8; }
        }

        private string _chyba = "";
        public virtual string Chyba
        {
            get { return _chyba; }
        }

        public virtual EPrinterConnectionMethod AllowedConnectionMethods
        {
            get { return EPrinterConnectionMethod.Serial | EPrinterConnectionMethod.Printer | EPrinterConnectionMethod.LPT; }
        }

        public PrinterConnectionSetting Setting
        {
            get
            {
                return setting;
            }
        }

        public virtual bool Interpret(XmlDocument xml)
        {
            XmlElement root = xml.DocumentElement;
            List<byte[]> bytes = new List<byte[]>();

            bool res = true;

            res = InitializePrint(bytes);

            if (!res) return false;

            foreach (XmlElement element in root.ChildNodes)
            {
                if (element.NodeType != XmlNodeType.Element) continue;

                switch (element.Name)
                {
                    case "title": res = ProcessTitle(element, bytes); break;
                    case "line": res = ProcessLine(element, bytes); break;
                    case "separator": res = ProcessSeparator(element, bytes); break;
                    case "barcode": res = ProcessBarcode(element, bytes); break;
                    case "graphics": res = ProcessGraphics(element, bytes); break;
                }

                if (!res) return false;
            }

            res = FinalizePrint(bytes);

            if (!res) return res;

            return Print(bytes);
        }

        #endregion

        #region Printing methods specific for each printer

        public virtual bool InitializePrint(List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool ProcessTitle(XmlElement element, List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool ProcessLine(XmlElement element, List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool ProcessSeparator(XmlElement element, List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool ProcessBarcode(XmlElement element, List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool ProcessGraphics(XmlElement element, List<byte[]> bytes)
        {
            return true;
        }

        public virtual bool FinalizePrint(List<byte[]> bytes)
        {
            return true;
        }

        #endregion


        #region Printing methods

        public virtual bool Print(List<byte[]> bytes)
        {
            switch (setting.Method)
            {
                case EPrinterConnectionMethod.None:  break;
                case EPrinterConnectionMethod.Serial: return PrintSerial(bytes);
                case EPrinterConnectionMethod.LPT: return PrintParalel(bytes);
                case EPrinterConnectionMethod.Printer: return PrintPrinter(bytes);
                case EPrinterConnectionMethod.Network: return PrintNetwork(bytes);
            }


            return true;
        }

        protected virtual bool PrintSerial(List<byte[]> bytes)
        {
            try
            {
                SerialPort serialPort = new SerialPort();
                serialPort.PortName = setting.SerialPort.Name;
                serialPort.BaudRate = setting.SerialPort.BaudRate;
                serialPort.DataBits = setting.SerialPort.DataBits;
                serialPort.StopBits = setting.SerialPort.StopBits;
                serialPort.Parity = setting.SerialPort.Parity;
                serialPort.Handshake = setting.SerialPort.Handshake;
                serialPort.WriteTimeout = setting.SerialPort.Timeout;

                if (!serialPort.IsOpen)
                    serialPort.Open();

                if (!serialPort.IsOpen) {
                    _chyba += "Serial port not open!!!";
                    return false;
                }
                 

                foreach (byte[] array in bytes)
                {
                    serialPort.Write(array, 0, array.Length);
                    Thread.Sleep(SerialPortLineDelay);
                }
                Thread.Sleep(SerialPortFinalDelay);
                serialPort.Close();

                return true;
            }
            catch (Exception ex)
            {
                _chyba += ex.Message.ToString();
                _chyba += setting.SerialPort.Name;
                return false;
            }
        }

        protected virtual bool PrintParalel(List<byte[]> bytes)
        {
            try
            {
                LPTControl lpt = new LPTControl();
                lpt.LptName = setting.ParalelPort.Name;

                lpt.Open();

                foreach (byte[] array in bytes)
                {
                    lpt.Write(array);
                }

                lpt.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        protected virtual bool PrintPrinter(List<byte[]> bytes)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                foreach (byte[] line in bytes)
                {
                    ms.Write(line, 0, line.Length);
                }
                byte[] array = new byte[ms.Length];
                Array.Copy(ms.GetBuffer(), array, array.Length);

                RawPrinterControl.SendBytesToPrinter(setting.Printer.Name, array);

                ms.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        protected virtual bool PrintNetwork(List<byte[]> bytes)
        {
            try
            {
                TcpClient tcp = new TcpClient(setting.Network.Address, setting.Network.Port);

                if (!tcp.Connected) 
                    return false;

                NetworkStream stream = tcp.GetStream();

                foreach (byte[] array in bytes)
                {
                    stream.Write(array, 0, array.Length);
                    Thread.Sleep(NetworkLineDelay);
                }
                Thread.Sleep(NetworkFinalDelay);
                stream.Close();
                tcp.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Test page definition and printing

        public virtual void PrintTestPage()
        {
            XmlDocument xml = new XmlDocument();
            XmlElement root = xml.CreateElement("posReport");
            xml.AppendChild(root);

            XmlElement element;
            XmlAttribute attribute;

            element = xml.CreateElement("title");
            element.AppendChild(xml.CreateTextNode("This is title"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("Plain left-aligned line"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("align");
            attribute.Value = "center";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode("Plain centered line"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("align");
            attribute.Value = "right";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode("Plain right-aligned line"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("style");
            attribute.Value = "bold";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode("Bold line"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            attribute = xml.CreateAttribute("style");
            attribute.Value = "bold";
            element.Attributes.Append(attribute);
            attribute = xml.CreateAttribute("align");
            attribute.Value = "center";
            element.Attributes.Append(attribute);
            element.AppendChild(xml.CreateTextNode("Centered bold line"));
            root.AppendChild(element);

            element = xml.CreateElement("separator");
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("Alphabet:"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("abcdefghijklmnopqrstuvwxyz"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("0123456789,.%"));
            root.AppendChild(element);


            if (SupportDiacritics)
            {
                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("Diacritics:"));
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("áčďéěíňóřšťúůýž"));
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ"));
                root.AppendChild(element);
            }

            if (SupportBarcodes)
            {
                element = xml.CreateElement("separator");
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("EAN-8"));
                root.AppendChild(element);

                element = xml.CreateElement("barcode");
                attribute = xml.CreateAttribute("type");
                attribute.Value = "ean8";
                element.Attributes.Append(attribute);
                element.AppendChild(xml.CreateTextNode("12345670"));
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("EAN-13"));
                root.AppendChild(element);

                element = xml.CreateElement("barcode");
                attribute = xml.CreateAttribute("type");
                attribute.Value = "ean13";
                element.Attributes.Append(attribute);
                element.AppendChild(xml.CreateTextNode("1234567890128"));
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("2 of 5"));
                root.AppendChild(element);

                element = xml.CreateElement("barcode");
                attribute = xml.CreateAttribute("type");
                attribute.Value = "2of5";
                element.Attributes.Append(attribute);
                element.AppendChild(xml.CreateTextNode("1234567890"));
                root.AppendChild(element);

                element = xml.CreateElement("line");
                element.AppendChild(xml.CreateTextNode("Code 39"));
                root.AppendChild(element);

                element = xml.CreateElement("barcode");
                attribute = xml.CreateAttribute("type");
                attribute.Value = "code39";
                element.Attributes.Append(attribute);
                element.AppendChild(xml.CreateTextNode("CODE 39"));
                root.AppendChild(element);
            }

            if (SupportGraphics)
            {
                element = xml.CreateElement("separator");
                root.AppendChild(element);

                Bitmap img = Resources.logoBW288;
                element = xml.CreateElement("graphics");

                attribute = xml.CreateAttribute("height");
                attribute.Value = img.Height.ToString();
                element.Attributes.Append(attribute);

                int emptyWidth = (ImageMaxWidth - img.Width);
                
                attribute = xml.CreateAttribute("width");
                attribute.Value = (img.Width+emptyWidth).ToString();
                element.Attributes.Append(attribute);

                StringBuilder sb = new StringBuilder();

                for (int y = 0; y < img.Height; y++)
                {
                    for (int i = 0; i < emptyWidth/2; i++) sb.Append(0);

                    for (int x = 0; x < img.Width; x++)
                    {
                        sb.Append(img.GetPixel(x, y).GetBrightness() > 0.5 ? 0 : 1);                        
                    }
                    
                    for (int i = 0; i < emptyWidth - (emptyWidth / 2); i++) sb.Append(0);
                }
                element.AppendChild(xml.CreateTextNode(sb.ToString()));

                root.AppendChild(element);
            }

            element = xml.CreateElement("separator");
            root.AppendChild(element);

            element = xml.CreateElement("line");
            element.AppendChild(xml.CreateTextNode("Test completed"));
            root.AppendChild(element);

            Interpret(xml);
        }

        #endregion
    }


}
