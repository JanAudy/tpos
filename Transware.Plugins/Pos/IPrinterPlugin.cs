﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Transware.Plugins
{
    /// <summary>
    /// Interface for all POS printers
    /// </summary>
    public interface IPrinterPlugin
    {
        /// <summary>
        /// Get the Name of the printer, it must be unique in the plugin collection
        /// </summary>
        string PrinterName { get; }
        
        /// <summary>
        /// Get the width of page in characters
        /// </summary>
        int PageWidth { get; }

        /// <summary>
        /// Identifies, is title line reduce the line wirh to half
        /// </summary>
        bool TitleReduceLineWidth { get; }

        /// <summary>
        /// True, if printer support printing of barcodes
        /// </summary>
        bool SupportBarcodes { get; }

        /// <summary>
        /// True if printer support czech diacritics
        /// </summary>
        bool SupportDiacritics { get; }

        /// <summary>
        /// Get the available connection methods for this printer
        /// </summary>
        EPrinterConnectionMethod AllowedConnectionMethods { get; }

        /// <summary>
        /// Printer setting derived from registry
        /// </summary>
        PrinterConnectionSetting Setting { get; }

        /// <summary>
        /// Interpreting of report writen using PosReportLanguages
        /// </summary>
        /// <param name="xml">Xml document with content of the report</param>
        /// <returns>True if interpretation was succesfull or not</returns>
        bool Interpret(XmlDocument xml);

        /// <summary>
        /// Prints test page with all type of font and aligment, including barcodes if supported
        /// </summary>
        void PrintTestPage();

    }
}
