﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transware.Plugins
{
    [Flags]
    public enum EPrinterConnectionMethod
    {
        None = 0,
        Serial = 1,
        LPT = 2,
        Printer = 4,
        Network = 8,
        Bluetooth = 16
    };
}
