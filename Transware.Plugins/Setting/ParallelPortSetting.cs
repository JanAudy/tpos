﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Transware.Plugins
{
    public class ParalelPortSetting : ISettingSerializer
    {
        public string Name { get; set; }

        #region ISettingSerializer Members

        public void Load(XmlElement element)
        {
            foreach (XmlAttribute attr in element.Attributes)
            {
                switch (attr.Name)
                {
                    case "Name": Name = attr.Value; break;
                }
            }
        }

        public XmlElement Save(XmlDocument xml)
        {
            XmlElement element = xml.CreateElement("ParalelPortSetting");
            XmlAttribute attrib = xml.CreateAttribute("Name");
            attrib.Value = Name;
            element.Attributes.Append(attrib);

            return element;
        }

        #endregion
    }
}
