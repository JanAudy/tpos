﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Xml;

namespace Transware.Plugins
{
    public class SerialPortSetting : ISettingSerializer
    {
        public string Name { get; set; }
        public int BaudRate { get; set; }
        public int DataBits { get; set; }
        public StopBits StopBits { get; set; }
        public Parity Parity { get; set; }
        public Handshake Handshake { get; set; }
        public int Timeout { get; set; }


        #region ISettingSerializer Members

        public void Load(XmlElement element)
        {
            foreach (XmlAttribute attr in element.Attributes)
            {
                switch (attr.Name)
                {
                    case "Name": Name = attr.Value; break;
                    case "BaudRate": BaudRate = int.Parse(attr.Value); break;
                    case "DataBits": DataBits = int.Parse(attr.Value); break;
                    case "StopBits": StopBits = (StopBits)int.Parse(attr.Value); break;
                    case "Parity": Parity = (Parity)int.Parse(attr.Value); break;
                    case "Handshake": Handshake = (Handshake)int.Parse(attr.Value); break;
                    case "Timeout": Timeout = int.Parse(attr.Value); break;
                }
            }
        }

        public XmlElement Save(XmlDocument xml)
        {
            XmlElement element = xml.CreateElement("SerialPortSetting");
            XmlAttribute attrib = xml.CreateAttribute("Name");
            attrib.Value = Name;
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("BaudRate");
            attrib.Value = BaudRate.ToString();
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("DataBits");
            attrib.Value = DataBits.ToString();
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("StopBits");
            attrib.Value = ((int)StopBits).ToString();
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("Parity");
            attrib.Value = ((int)Parity).ToString();
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("Handshake");
            attrib.Value = ((int)Handshake).ToString();
            element.Attributes.Append(attrib);

            attrib = xml.CreateAttribute("Timeout");
            attrib.Value = Timeout.ToString();
            element.Attributes.Append(attrib);

            return element;
        }

        #endregion
    }

}
