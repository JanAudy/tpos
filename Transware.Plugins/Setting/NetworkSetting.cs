﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Transware.Plugins
{
    public class NetworkSetting : ISettingSerializer
    {
        public string Address { get; set; }
        public int Port { get; set; }

        #region ISettingSerializer Members

        public void Load(XmlElement element)
        {
            foreach (XmlAttribute attr in element.Attributes)
            {
                switch (attr.Name)
                {
                    case "Address": Address = attr.Value; break;
                    case "Port": Port = int.Parse(attr.Value); break;
                }
            }
        }

        public XmlElement Save(XmlDocument xml)
        {
            XmlElement element = xml.CreateElement("NetworkSetting");
            XmlAttribute attrib = xml.CreateAttribute("Address");
            attrib.Value = Address;
            element.Attributes.Append(attrib);
            attrib = xml.CreateAttribute("Port");
            attrib.Value = Port.ToString();
            element.Attributes.Append(attrib);

            return element;
        }

        #endregion
    }
}
