﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Transware.Plugins
{
    public interface ISettingSerializer
    {
        void Load(XmlElement element);
        XmlElement Save(XmlDocument xml);
    }
}
