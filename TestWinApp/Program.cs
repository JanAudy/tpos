﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Jadro.Data;
using Transware.Data;
using Microdata.Singletons;

namespace TestWinApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            JadroDB.Instance.DB_Server = @"PLA06-NB\SQLEXPRESS";
            JadroDB.Instance.DB_UserName = "sa";
            JadroDB.Instance.DB_Password = "cdlist";

            TranswareDB.Instance.DB_Server = @"PLA06-NB\SQLEXPRESS";
            TranswareDB.Instance.DB_UserName = "sa";
            TranswareDB.Instance.DB_Password = "cdlist";

            MicrodataStatus.Instance.ApplicationName = "TestWindowsApplication";
            MicrodataStatus.Instance.UserName = "Test";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
