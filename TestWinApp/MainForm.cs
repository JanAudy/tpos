﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transware.Forms;

namespace TestWinApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void bVat_Click(object sender, EventArgs e)
        {
            VatList form = new VatList();
            form.ShowDialog();
        }

        private void bCTax_Click(object sender, EventArgs e)
        {
            CTaxList form = new CTaxList();
            form.ShowDialog();
        }

        private void bCurrency_Click(object sender, EventArgs e)
        {
            CurrencyList form = new CurrencyList();
            form.ShowDialog();
        }

        private void bEmailSign_Click(object sender, EventArgs e)
        {
            EmailSignList form = new EmailSignList();
            form.ShowDialog();
        }

        private void bEmailText_Click(object sender, EventArgs e)
        {
            EmailTextList form = new EmailTextList(Transware.Data.Enums.ERecordType.Invoice);
            form.ShowDialog();
        }

        private void bIndividualDiscount_Click(object sender, EventArgs e)
        {
            IndividualDiscountList form = new IndividualDiscountList();
            form.ShowDialog();
        }

        private void bInterestGroup_Click(object sender, EventArgs e)
        {
            InterestGroupList form = new InterestGroupList();
            form.ShowDialog();
        }

        private void bPackageType_Click(object sender, EventArgs e)
        {
            PackageTypeList form = new PackageTypeList();
            form.ShowDialog();
        }

        private void bPaymentTitle_Click(object sender, EventArgs e)
        {
            PaymentTitleList form = new PaymentTitleList();
            form.ShowDialog();
        }

        private void bPaymentType_Click(object sender, EventArgs e)
        {
            PaymentTypeList form = new PaymentTypeList();
            form.ShowDialog();
        }

        private void bPosition_Click(object sender, EventArgs e)
        {
            PositionList form = new PositionList();
            form.ShowDialog();
        }

        private void bRecordFooter_Click(object sender, EventArgs e)
        {
            RecordFooterList form = new RecordFooterList();
            form.ShowDialog();
        }

        private void bRecordHeader_Click(object sender, EventArgs e)
        {
            RecordHeaderList form = new RecordHeaderList();
            form.ShowDialog();
        }

        private void bSettlementType_Click(object sender, EventArgs e)
        {
            SettlementTypeList form = new SettlementTypeList();
            form.ShowDialog();
        }

        private void bTimePeriod_Click(object sender, EventArgs e)
        {
            TimePeriodList form = new TimePeriodList();
            form.ShowDialog();
        }

        private void bUnit_Click(object sender, EventArgs e)
        {
            UnitList form = new UnitList();
            form.ShowDialog();
        }

        private void bDifferentialMark_Click(object sender, EventArgs e)
        {
            DifferentialMarkList form = new DifferentialMarkList();
            form.ShowDialog();
        }

        private void bTransportType_Click(object sender, EventArgs e)
        {
            TransportTypeList form = new TransportTypeList();
            form.ShowDialog();
        }

        private void bContractName_Click(object sender, EventArgs e)
        {
            ContractNameList form = new ContractNameList();
            form.ShowDialog();
        }

        private void bContractReturnedItemMark_Click(object sender, EventArgs e)
        {
            ContractReturnedItemNameList form = new ContractReturnedItemNameList();
            form.ShowDialog();
        }

        private void bDepartments_Click(object sender, EventArgs e)
        {
            DepartmentList form = new DepartmentList();
            form.ShowDialog();
        }

        private void bOrderType_Click(object sender, EventArgs e)
        {
            OrderTypeList form = new OrderTypeList();
            form.ShowDialog();
        }
    }
}
