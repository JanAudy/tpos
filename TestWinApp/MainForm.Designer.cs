﻿namespace TestWinApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bVat = new System.Windows.Forms.Button();
            this.bCTax = new System.Windows.Forms.Button();
            this.bCurrency = new System.Windows.Forms.Button();
            this.bEmailSign = new System.Windows.Forms.Button();
            this.bEmailText = new System.Windows.Forms.Button();
            this.bIndividualDiscount = new System.Windows.Forms.Button();
            this.bInterestGroup = new System.Windows.Forms.Button();
            this.bPackageType = new System.Windows.Forms.Button();
            this.bPaymentTitle = new System.Windows.Forms.Button();
            this.bPaymentType = new System.Windows.Forms.Button();
            this.bPosition = new System.Windows.Forms.Button();
            this.bRecordFooter = new System.Windows.Forms.Button();
            this.bRecordHeader = new System.Windows.Forms.Button();
            this.bSettlementType = new System.Windows.Forms.Button();
            this.bTimePeriod = new System.Windows.Forms.Button();
            this.bUnit = new System.Windows.Forms.Button();
            this.bDifferentialMark = new System.Windows.Forms.Button();
            this.bTransportType = new System.Windows.Forms.Button();
            this.bContractName = new System.Windows.Forms.Button();
            this.bContractReturnedItemMark = new System.Windows.Forms.Button();
            this.bDepartments = new System.Windows.Forms.Button();
            this.bOrderType = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bVat
            // 
            this.bVat.Location = new System.Drawing.Point(36, 37);
            this.bVat.Name = "bVat";
            this.bVat.Size = new System.Drawing.Size(75, 23);
            this.bVat.TabIndex = 0;
            this.bVat.Text = "Vats";
            this.bVat.UseVisualStyleBackColor = true;
            this.bVat.Click += new System.EventHandler(this.bVat_Click);
            // 
            // bCTax
            // 
            this.bCTax.Location = new System.Drawing.Point(36, 66);
            this.bCTax.Name = "bCTax";
            this.bCTax.Size = new System.Drawing.Size(75, 23);
            this.bCTax.TabIndex = 1;
            this.bCTax.Text = "CTax";
            this.bCTax.UseVisualStyleBackColor = true;
            this.bCTax.Click += new System.EventHandler(this.bCTax_Click);
            // 
            // bCurrency
            // 
            this.bCurrency.Location = new System.Drawing.Point(36, 95);
            this.bCurrency.Name = "bCurrency";
            this.bCurrency.Size = new System.Drawing.Size(75, 23);
            this.bCurrency.TabIndex = 2;
            this.bCurrency.Text = "Currency";
            this.bCurrency.UseVisualStyleBackColor = true;
            this.bCurrency.Click += new System.EventHandler(this.bCurrency_Click);
            // 
            // bEmailSign
            // 
            this.bEmailSign.Location = new System.Drawing.Point(36, 124);
            this.bEmailSign.Name = "bEmailSign";
            this.bEmailSign.Size = new System.Drawing.Size(75, 23);
            this.bEmailSign.TabIndex = 3;
            this.bEmailSign.Text = "EmailSign";
            this.bEmailSign.UseVisualStyleBackColor = true;
            this.bEmailSign.Click += new System.EventHandler(this.bEmailSign_Click);
            // 
            // bEmailText
            // 
            this.bEmailText.Location = new System.Drawing.Point(36, 153);
            this.bEmailText.Name = "bEmailText";
            this.bEmailText.Size = new System.Drawing.Size(75, 23);
            this.bEmailText.TabIndex = 4;
            this.bEmailText.Text = "EmailText";
            this.bEmailText.UseVisualStyleBackColor = true;
            this.bEmailText.Click += new System.EventHandler(this.bEmailText_Click);
            // 
            // bIndividualDiscount
            // 
            this.bIndividualDiscount.Location = new System.Drawing.Point(36, 182);
            this.bIndividualDiscount.Name = "bIndividualDiscount";
            this.bIndividualDiscount.Size = new System.Drawing.Size(75, 23);
            this.bIndividualDiscount.TabIndex = 5;
            this.bIndividualDiscount.Text = "IndividualDiscount";
            this.bIndividualDiscount.UseVisualStyleBackColor = true;
            this.bIndividualDiscount.Click += new System.EventHandler(this.bIndividualDiscount_Click);
            // 
            // bInterestGroup
            // 
            this.bInterestGroup.Location = new System.Drawing.Point(36, 211);
            this.bInterestGroup.Name = "bInterestGroup";
            this.bInterestGroup.Size = new System.Drawing.Size(75, 23);
            this.bInterestGroup.TabIndex = 6;
            this.bInterestGroup.Text = "InterestGroup";
            this.bInterestGroup.UseVisualStyleBackColor = true;
            this.bInterestGroup.Click += new System.EventHandler(this.bInterestGroup_Click);
            // 
            // bPackageType
            // 
            this.bPackageType.Location = new System.Drawing.Point(117, 37);
            this.bPackageType.Name = "bPackageType";
            this.bPackageType.Size = new System.Drawing.Size(75, 23);
            this.bPackageType.TabIndex = 7;
            this.bPackageType.Text = "PackageType";
            this.bPackageType.UseVisualStyleBackColor = true;
            this.bPackageType.Click += new System.EventHandler(this.bPackageType_Click);
            // 
            // bPaymentTitle
            // 
            this.bPaymentTitle.Location = new System.Drawing.Point(117, 66);
            this.bPaymentTitle.Name = "bPaymentTitle";
            this.bPaymentTitle.Size = new System.Drawing.Size(75, 23);
            this.bPaymentTitle.TabIndex = 8;
            this.bPaymentTitle.Text = "PaymentTitle";
            this.bPaymentTitle.UseVisualStyleBackColor = true;
            this.bPaymentTitle.Click += new System.EventHandler(this.bPaymentTitle_Click);
            // 
            // bPaymentType
            // 
            this.bPaymentType.Location = new System.Drawing.Point(117, 95);
            this.bPaymentType.Name = "bPaymentType";
            this.bPaymentType.Size = new System.Drawing.Size(75, 23);
            this.bPaymentType.TabIndex = 9;
            this.bPaymentType.Text = "PaymentType";
            this.bPaymentType.UseVisualStyleBackColor = true;
            this.bPaymentType.Click += new System.EventHandler(this.bPaymentType_Click);
            // 
            // bPosition
            // 
            this.bPosition.Location = new System.Drawing.Point(117, 124);
            this.bPosition.Name = "bPosition";
            this.bPosition.Size = new System.Drawing.Size(75, 23);
            this.bPosition.TabIndex = 10;
            this.bPosition.Text = "Position";
            this.bPosition.UseVisualStyleBackColor = true;
            this.bPosition.Click += new System.EventHandler(this.bPosition_Click);
            // 
            // bRecordFooter
            // 
            this.bRecordFooter.Location = new System.Drawing.Point(117, 153);
            this.bRecordFooter.Name = "bRecordFooter";
            this.bRecordFooter.Size = new System.Drawing.Size(75, 23);
            this.bRecordFooter.TabIndex = 11;
            this.bRecordFooter.Text = "RecordFooter";
            this.bRecordFooter.UseVisualStyleBackColor = true;
            this.bRecordFooter.Click += new System.EventHandler(this.bRecordFooter_Click);
            // 
            // bRecordHeader
            // 
            this.bRecordHeader.Location = new System.Drawing.Point(117, 182);
            this.bRecordHeader.Name = "bRecordHeader";
            this.bRecordHeader.Size = new System.Drawing.Size(75, 23);
            this.bRecordHeader.TabIndex = 12;
            this.bRecordHeader.Text = "RecordHeader";
            this.bRecordHeader.UseVisualStyleBackColor = true;
            this.bRecordHeader.Click += new System.EventHandler(this.bRecordHeader_Click);
            // 
            // bSettlementType
            // 
            this.bSettlementType.Location = new System.Drawing.Point(117, 211);
            this.bSettlementType.Name = "bSettlementType";
            this.bSettlementType.Size = new System.Drawing.Size(75, 23);
            this.bSettlementType.TabIndex = 13;
            this.bSettlementType.Text = "SettlementType";
            this.bSettlementType.UseVisualStyleBackColor = true;
            this.bSettlementType.Click += new System.EventHandler(this.bSettlementType_Click);
            // 
            // bTimePeriod
            // 
            this.bTimePeriod.Location = new System.Drawing.Point(36, 240);
            this.bTimePeriod.Name = "bTimePeriod";
            this.bTimePeriod.Size = new System.Drawing.Size(75, 23);
            this.bTimePeriod.TabIndex = 14;
            this.bTimePeriod.Text = "TimePeriod";
            this.bTimePeriod.UseVisualStyleBackColor = true;
            this.bTimePeriod.Click += new System.EventHandler(this.bTimePeriod_Click);
            // 
            // bUnit
            // 
            this.bUnit.Location = new System.Drawing.Point(117, 240);
            this.bUnit.Name = "bUnit";
            this.bUnit.Size = new System.Drawing.Size(75, 23);
            this.bUnit.TabIndex = 15;
            this.bUnit.Text = "Unit";
            this.bUnit.UseVisualStyleBackColor = true;
            this.bUnit.Click += new System.EventHandler(this.bUnit_Click);
            // 
            // bDifferentialMark
            // 
            this.bDifferentialMark.Location = new System.Drawing.Point(36, 269);
            this.bDifferentialMark.Name = "bDifferentialMark";
            this.bDifferentialMark.Size = new System.Drawing.Size(75, 23);
            this.bDifferentialMark.TabIndex = 16;
            this.bDifferentialMark.Text = "DifferentialMark";
            this.bDifferentialMark.UseVisualStyleBackColor = true;
            this.bDifferentialMark.Click += new System.EventHandler(this.bDifferentialMark_Click);
            // 
            // bTransportType
            // 
            this.bTransportType.Location = new System.Drawing.Point(117, 269);
            this.bTransportType.Name = "bTransportType";
            this.bTransportType.Size = new System.Drawing.Size(75, 23);
            this.bTransportType.TabIndex = 17;
            this.bTransportType.Text = "TransportType";
            this.bTransportType.UseVisualStyleBackColor = true;
            this.bTransportType.Click += new System.EventHandler(this.bTransportType_Click);
            // 
            // bContractName
            // 
            this.bContractName.Location = new System.Drawing.Point(36, 298);
            this.bContractName.Name = "bContractName";
            this.bContractName.Size = new System.Drawing.Size(75, 23);
            this.bContractName.TabIndex = 18;
            this.bContractName.Text = "ContractName";
            this.bContractName.UseVisualStyleBackColor = true;
            this.bContractName.Click += new System.EventHandler(this.bContractName_Click);
            // 
            // bContractReturnedItemMark
            // 
            this.bContractReturnedItemMark.Location = new System.Drawing.Point(117, 298);
            this.bContractReturnedItemMark.Name = "bContractReturnedItemMark";
            this.bContractReturnedItemMark.Size = new System.Drawing.Size(75, 23);
            this.bContractReturnedItemMark.TabIndex = 19;
            this.bContractReturnedItemMark.Text = "ContractReturnedItemName";
            this.bContractReturnedItemMark.UseVisualStyleBackColor = true;
            this.bContractReturnedItemMark.Click += new System.EventHandler(this.bContractReturnedItemMark_Click);
            // 
            // bDepartments
            // 
            this.bDepartments.Location = new System.Drawing.Point(36, 327);
            this.bDepartments.Name = "bDepartments";
            this.bDepartments.Size = new System.Drawing.Size(75, 23);
            this.bDepartments.TabIndex = 20;
            this.bDepartments.Text = "Department";
            this.bDepartments.UseVisualStyleBackColor = true;
            this.bDepartments.Click += new System.EventHandler(this.bDepartments_Click);
            // 
            // bOrderType
            // 
            this.bOrderType.Location = new System.Drawing.Point(117, 327);
            this.bOrderType.Name = "bOrderType";
            this.bOrderType.Size = new System.Drawing.Size(75, 23);
            this.bOrderType.TabIndex = 21;
            this.bOrderType.Text = "OrderType";
            this.bOrderType.UseVisualStyleBackColor = true;
            this.bOrderType.Click += new System.EventHandler(this.bOrderType_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 396);
            this.Controls.Add(this.bOrderType);
            this.Controls.Add(this.bDepartments);
            this.Controls.Add(this.bContractReturnedItemMark);
            this.Controls.Add(this.bContractName);
            this.Controls.Add(this.bTransportType);
            this.Controls.Add(this.bDifferentialMark);
            this.Controls.Add(this.bUnit);
            this.Controls.Add(this.bTimePeriod);
            this.Controls.Add(this.bSettlementType);
            this.Controls.Add(this.bRecordHeader);
            this.Controls.Add(this.bRecordFooter);
            this.Controls.Add(this.bPosition);
            this.Controls.Add(this.bPaymentType);
            this.Controls.Add(this.bPaymentTitle);
            this.Controls.Add(this.bPackageType);
            this.Controls.Add(this.bInterestGroup);
            this.Controls.Add(this.bIndividualDiscount);
            this.Controls.Add(this.bEmailText);
            this.Controls.Add(this.bEmailSign);
            this.Controls.Add(this.bCurrency);
            this.Controls.Add(this.bCTax);
            this.Controls.Add(this.bVat);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bVat;
        private System.Windows.Forms.Button bCTax;
        private System.Windows.Forms.Button bCurrency;
        private System.Windows.Forms.Button bEmailSign;
        private System.Windows.Forms.Button bEmailText;
        private System.Windows.Forms.Button bIndividualDiscount;
        private System.Windows.Forms.Button bInterestGroup;
        private System.Windows.Forms.Button bPackageType;
        private System.Windows.Forms.Button bPaymentTitle;
        private System.Windows.Forms.Button bPaymentType;
        private System.Windows.Forms.Button bPosition;
        private System.Windows.Forms.Button bRecordFooter;
        private System.Windows.Forms.Button bRecordHeader;
        private System.Windows.Forms.Button bSettlementType;
        private System.Windows.Forms.Button bTimePeriod;
        private System.Windows.Forms.Button bUnit;
        private System.Windows.Forms.Button bDifferentialMark;
        private System.Windows.Forms.Button bTransportType;
        private System.Windows.Forms.Button bContractName;
        private System.Windows.Forms.Button bContractReturnedItemMark;
        private System.Windows.Forms.Button bDepartments;
        private System.Windows.Forms.Button bOrderType;
    }
}

