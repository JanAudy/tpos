﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Cis_Bank : AbstractJadroDto
    {
        public string Nazev { get; set; }
        public string Kod { get; set; }
        public string Swift { get; set; }
        public decimal StatID { get; set; }
        public short Platnost { get; set; }

        private Cis_Statu stat = null;
        internal event LazyLoadObjectDecimal<Cis_Statu> LoadStat = null;
        public Cis_Statu Stat
        {
            get
            {
                if (stat == null)
                    if (LoadStat != null)
                        LoadStat(this, StatID, ref stat);

                return stat;
            }
            set { stat = value; StatID = value.ID; }
        }
    }
}
