﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jadro.Data.DataTypes
{
    public class Ciselnik : AbstractJadroDto
    {
        public string Popis { get; set; }
        public short Platnost { get; set; }
    }
}
