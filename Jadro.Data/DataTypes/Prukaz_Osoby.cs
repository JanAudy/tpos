﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Prukaz_Osoby : AbstractJadroDto
    {
        public decimal ID_OSOBA { get; set; }
        public decimal TypID { get; set; }
        public string CisPruk { get; set; }
        public DateTime DatVyd { get; set; }
        public DateTime PlatnostDo { get; set; }
        public string MistVyd { get; set; }

        public Osoba osoba = null;
        internal event LazyLoadObjectDecimal<Osoba> LoadOsoba = null;
        public Osoba Osoba
        {
            get
            {
                if (osoba == null)
                    if (LoadOsoba != null)
                        LoadOsoba(this, ID_OSOBA, ref osoba);

                return osoba;
            }
            set { osoba = value; ID_OSOBA = value.ID; }
        }
    }
}
