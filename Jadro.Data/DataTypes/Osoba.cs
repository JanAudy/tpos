﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Osoba : AbstractJadroDto, IComparable<Osoba>
    {
        public string Kod { get; set; }
        public decimal Prislusnost { get; set; }
        public string Rodne_C { get; set; }
        public DateTime Datum_Narozeni { get; set; }
        public decimal Titul_Pred { get; set; }
        public char Pohlavi { get; set; }
        public short Blokace { get; set; }
        public string Jmeno { get; set; }
        public string Jmeno2 { get; set; }
        public string Prijmeni { get; set; }
        public decimal Titul_Za { get; set; }
        public string Full_Name { get; set; }
        public byte[] Foto { get; set; }
        public decimal ID_ORGANIZACE { get; set; }
        public decimal ID_FUNKCE { get; set; }
        public decimal ID_ODDELENI { get; set; }
        public decimal ID_SKUPINA { get; set; }
        public string Poznamka { get; set; }
        public decimal Adr_Row { get; set; }
        public decimal Tel1_Row { get; set; }
        public decimal Tel2_Row { get; set; }
        public decimal Email_Row { get; set; }
        public decimal WWW_Row { get; set; }
        public decimal Prukaz_Row { get; set; }
        public short Platnost { get; set; }

        public decimal Credit { get; set; }
        public bool Dodavatel { get; set; }
        public bool Odberatel { get; set; }
        public bool Zahranicni { get; set; }

        private List<Adr_Osoby> adresy = null;
        internal event LazyLoadListDecimal<Adr_Osoby> LoadAdresy;
        public List<Adr_Osoby> Adresy
        {
            get
            {
                if (adresy == null)
                    if (LoadAdresy != null)
                        LoadAdresy(this, this.ID, ref adresy);
                    else
                        adresy = new List<Adr_Osoby>();

                return adresy;
            }
            set { adresy = value; }
        }

        private List<Email_Osoby> emaily = null;
        internal event LazyLoadListDecimal<Email_Osoby> LoadEmaily;
        public List<Email_Osoby> Emaily
        {
            get
            {
                if (emaily == null)
                    if (LoadEmaily != null)
                        LoadEmaily(this, this.ID, ref emaily);
                    else
                        emaily = new List<Email_Osoby>();

                return emaily;
            }
            set { emaily = value; }
        }

        private List<Tel_Osoby> telefony = null;
        internal event LazyLoadListDecimal<Tel_Osoby> LoadTelefony;
        public List<Tel_Osoby> Telefony
        {
            get
            {
                if (telefony == null)
                    if (LoadTelefony != null)
                        LoadTelefony(this, this.ID, ref telefony);
                    else
                        telefony = new List<Tel_Osoby>();

                return telefony;
            }
            set { telefony = value; }
        }

        private List<WWW_Osoby> www = null;
        internal event LazyLoadListDecimal<WWW_Osoby> LoadWWW;
        public List<WWW_Osoby> WWW
        {
            get
            {
                if (www == null)
                    if (LoadWWW != null)
                        LoadWWW(this, this.ID, ref www);
                    else
                        www = new List<WWW_Osoby>();

                return www;
            }
            set { www = value; }
        }

        public Organizace organizace = null;
        internal event LazyLoadObjectDecimal<Organizace> LoadOrganizace = null;
        public Organizace Organizace
        {
            get
            {
                if (organizace == null && ID_ORGANIZACE != 0)
                    if (LoadOrganizace != null)
                        LoadOrganizace(this, ID_ORGANIZACE, ref organizace);

                return organizace;
            }
            set { organizace = value; ID_ORGANIZACE = value.ID; }
        }

        public Oddeleni oddeleni = null;
        internal event LazyLoadObjectDecimal<Oddeleni> LoadOddeleni = null;
        public Oddeleni Oddeleni
        {
            get
            {
                if (oddeleni == null && ID_ODDELENI != 0)
                    if (LoadOddeleni != null)
                        LoadOddeleni(this, ID_ODDELENI, ref oddeleni);

                return oddeleni;
            }
            set { oddeleni = value; ID_ODDELENI = value.ID; }
        }

        #region IComparable<Osoba> Members

        public int CompareTo(Osoba other)
        {
            return (Full_Name ?? (Prijmeni + " " + Jmeno)).CompareTo(other.Full_Name ?? (other.Prijmeni + " " + other.Jmeno));
        }

        #endregion
    }
}
