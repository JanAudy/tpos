﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public abstract class AbstractJadroDto : AbstractIDDecimalDto
    {
        public string AUTOR { get; set; }
        public DateTime DT_AKCE { get; set; }
        public char AKCE { get; set; }
        
        public AbstractJadroDto()
        {
            AUTOR = "";
            DT_AKCE = ZeroDateTime;
            AKCE = 'I';
        }

    }
}
