﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class WWW_Oddeleni : WWW
    {
        public decimal ID_Oddeleni { get; set; }

        public Oddeleni oddeleni = null;
        internal event LazyLoadObjectDecimal<Oddeleni> LoadOddeleni = null;
        public Oddeleni Oddeleni
        {
            get
            {
                if (oddeleni == null)
                    if (LoadOddeleni != null)
                        LoadOddeleni(this, ID_Oddeleni, ref oddeleni);

                return oddeleni;
            }
            set { oddeleni = value; ID_Oddeleni = value.ID; }
        }
    }
}
