﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class IDMedia : AbstractJadroDto
    {
        public short Platnost { get; set; }
        public short Blokace { get; set; }
        public string Nazev { get; set; }
        public decimal Typ_Media { get; set; }
        public string Tag { get; set; }
        public decimal OsobaID { get; set; }
        public decimal SkupinaID { get; set; }
        public decimal OddeleniID { get; set; }
        public decimal OrganizaceID { get; set; }

        private Cis_IDMedii typ = null;
        internal event LazyLoadObjectDecimal<Cis_IDMedii> LoadTyp = null;
        public Cis_IDMedii Typ
        {
            get
            {
                if (typ == null)
                    if (LoadTyp != null)
                        LoadTyp(this, Typ_Media, ref typ);

                return typ;
            }
            set { typ = value; Typ_Media = value.ID; }
        }

        public Organizace organizace = null;
        internal event LazyLoadObjectDecimal<Organizace> LoadOrganizace = null;
        public Organizace Organizace
        {
            get
            {
                if (organizace == null && OrganizaceID!=0)
                    if (LoadOrganizace != null)
                        LoadOrganizace(this, OrganizaceID, ref organizace);

                return organizace;
            }
            set { organizace = value; OrganizaceID = value.ID; }
        }

        public Oddeleni oddeleni = null;
        internal event LazyLoadObjectDecimal<Oddeleni> LoadOddeleni = null;
        public Oddeleni Oddeleni
        {
            get
            {
                if (oddeleni == null && OddeleniID!=0)
                    if (LoadOddeleni != null)
                        LoadOddeleni(this, OddeleniID, ref oddeleni);

                return oddeleni;
            }
            set { oddeleni = value; OddeleniID = value.ID; }
        }

        public Osoba osoba = null;
        internal event LazyLoadObjectDecimal<Osoba> LoadOsoba = null;
        public Osoba Osoba
        {
            get
            {
                if (osoba == null && OsobaID!=0)
                    if (LoadOsoba != null)
                        LoadOsoba(this, OsobaID, ref osoba);

                return osoba;
            }
            set { osoba = value; OsobaID = value.ID; }
        }

        public Skupina skupina = null;
        internal event LazyLoadObjectDecimal<Skupina> LoadSkupina = null;
        public Skupina Skupina
        {
            get
            {
                if (skupina == null && SkupinaID != 0)
                    if (LoadSkupina != null)
                        LoadSkupina(this, SkupinaID, ref skupina);

                return skupina;
            }
            set { skupina = value; SkupinaID = value.ID; }
        }

        public string Owner
        {
            get
            {
                if (OsobaID != 0)
                    return Osoba.Full_Name ?? Osoba.Jmeno + " " + Osoba.Prijmeni;
                if (OrganizaceID != 0)
                    return Organizace.Nazev;
                return "";
            }
        }
    }
}
