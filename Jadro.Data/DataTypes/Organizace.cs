﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Organizace : AbstractJadroDto, IComparable<Organizace>
    {
        public decimal Kat_Row { get; set; }
        public string Kod { get; set; }
        public string Nazev { get; set; }
        public string ICO { get; set; }
        public string DIC { get; set; }
        public short Odberatel { get; set; }
        public short Dodavatel { get; set; }
        public short Ostatni { get; set; }
        public short Platce_Dph { get; set; }
        public short Je_Organizace { get; set; }
        public short Je_Instituce { get; set; }
        public short Zahranicni { get; set; }
        public short Neplatic { get; set; }
        public bool Aktivni { get; set; }
        public byte[] Logo { get; set; }
        public string Poznamka { get; set; }
        public decimal Adr_Row { get; set; }
        public decimal Tel1_Row { get; set; }
        public decimal Tel2_Row { get; set; }
        public decimal Email_Row { get; set; }
        public decimal WWW_Row { get; set; }
        public decimal Banka_Row { get; set; }
        public short Platnost { get; set; }

        public Organizace()
        {            
            Platnost = 1;
        }


        private List<Adr_Organizace> adresy = null;
        internal event LazyLoadListDecimal<Adr_Organizace> LoadAdresy;
        public List<Adr_Organizace> Adresy
        {
            get
            {
                if (adresy == null)
                    if (LoadAdresy != null)
                        LoadAdresy(this, this.ID, ref adresy);
                    else
                        adresy = new List<Adr_Organizace>();

                return adresy;
            }
            set { adresy = value; }
        }

        private List<Email_Organizace> emaily = null;
        internal event LazyLoadListDecimal<Email_Organizace> LoadEmaily;
        public List<Email_Organizace> Emaily
        {
            get
            {
                if (emaily == null)
                    if (LoadEmaily != null)
                        LoadEmaily(this, this.ID, ref emaily);
                    else
                        emaily = new List<Email_Organizace>();

                return emaily;
            }
            set { emaily = value; }
        }

        private List<Tel_Organizace> telefony = null;
        internal event LazyLoadListDecimal<Tel_Organizace> LoadTelefony;
        public List<Tel_Organizace> Telefony
        {
            get
            {
                if (telefony == null)
                    if (LoadTelefony != null)
                        LoadTelefony(this, this.ID, ref telefony);
                    else
                        telefony = new List<Tel_Organizace>();

                return telefony;
            }
            set { telefony = value; }
        }

        private List<WWW_Organizace> www = null;
        internal event LazyLoadListDecimal<WWW_Organizace> LoadWWW;
        public List<WWW_Organizace> WWW
        {
            get
            {
                if (www == null)
                    if (LoadWWW != null)
                        LoadWWW(this, this.ID, ref www);
                    else
                        www = new List<WWW_Organizace>();

                return www;
            }
            set { www = value; }
        }

        private List<Ucty_Organizace> ucty_Organizace = null;
        internal event LazyLoadListDecimal<Ucty_Organizace> LoadUcty_Organizace;
        public List<Ucty_Organizace> Ucty_Organizace
        {
            get
            {
                if (ucty_Organizace == null)
                    if (LoadUcty_Organizace != null)
                        LoadUcty_Organizace(this, this.ID, ref ucty_Organizace);
                    else
                        ucty_Organizace = new List<Ucty_Organizace>();

                return ucty_Organizace;
            }
            set { ucty_Organizace = value; }
        }

        public override void AssignIDToChildren()
        {
            if (Adresy != null)
            {
                foreach (Adr_Organizace o in Adresy)
                    o.Organizace = this;
            }

            if (Emaily != null)
            {
                foreach (Email_Organizace o in Emaily)
                    o.Organizace = this;
            }

            if (Telefony != null)
            {
                foreach (Tel_Organizace o in Telefony)
                    o.Organizace = this;
            }

            if (WWW != null)
            {
                foreach (WWW_Organizace o in WWW)
                    o.Organizace = this;
            }

            if (Ucty_Organizace != null)
            {
                foreach (Ucty_Organizace o in Ucty_Organizace)
                    o.Organizace = this;
            }
        }

        #region IComparable<Organizace> Members

        public int CompareTo(Organizace other)
        {
            return (Nazev ?? "").CompareTo(other.Nazev);
        }

        #endregion
    }
}
