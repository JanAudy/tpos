﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Oddeleni : AbstractJadroDto
    {
        public string Kod { get; set; }
        public string Nazev { get; set; }
        public string Misto { get; set; }
        public decimal Vedouci { get; set; }
        public decimal Zastupce { get; set; }
        public decimal ID_Organizace { get; set; }
        public string Text { get; set; }
        public decimal Tel1_Row { get; set; }
        public decimal Tel2_Row { get; set; }
        public decimal Email_Row { get; set; }
        public decimal WWW_Row { get; set; }
        public short Platnost { get; set; }

        private List<Email_Oddeleni> emaily = null;
        internal event LazyLoadListDecimal<Email_Oddeleni> LoadEmaily;
        public List<Email_Oddeleni> Emaily
        {
            get
            {
                if (emaily == null)
                    if (LoadEmaily != null)
                        LoadEmaily(this, this.ID, ref emaily);
                    else
                        emaily = new List<Email_Oddeleni>();

                return emaily;
            }
            set { emaily = value; }
        }

        private List<Tel_Oddeleni> telefony = null;
        internal event LazyLoadListDecimal<Tel_Oddeleni> LoadTelefony;
        public List<Tel_Oddeleni> Telefony
        {
            get
            {
                if (telefony == null)
                    if (LoadTelefony != null)
                        LoadTelefony(this, this.ID, ref telefony);
                    else
                        telefony = new List<Tel_Oddeleni>();

                return telefony;
            }
            set { telefony = value; }
        }

        private List<WWW_Oddeleni> www = null;
        internal event LazyLoadListDecimal<WWW_Oddeleni> LoadWWW;
        public List<WWW_Oddeleni> WWW
        {
            get
            {
                if (www == null)
                    if (LoadWWW != null)
                        LoadWWW(this, this.ID, ref www);
                    else
                        www = new List<WWW_Oddeleni>();

                return www;
            }
            set { www = value; }
        }
    }
}
