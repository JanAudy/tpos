﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jadro.Data.DataTypes
{
    public class IDMediaSummary
    {
        public decimal ID { get; set; }
        public string CardNo { get; set; }

        public decimal CardTypeID { get; set; }
        public string CardTypeName { get; set; }

        public long Person_ID { get; set; }
        public string Person_FullName { get; set; }
        public string Person_Name { get; set; }
        public string Person_Surname { get; set; }

    }
}
