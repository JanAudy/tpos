﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jadro.Data.DataTypes
{
    public class Cis_PSC : AbstractJadroDto
    {
        public string Obec { get; set; }
        public string PSC { get; set; }
        public string Posta { get; set; }
        public string Okres { get; set; }
        public short Kod_Okresu { get; set; }
        public short Platnost { get; set; }
    }
}
