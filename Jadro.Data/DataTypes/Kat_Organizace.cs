﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Kat_Organizace : AbstractJadroDto
    {
        public decimal ID_Organizace { get; set; }
        public decimal TypID { get; set; }

        public Organizace organizace = null;
        internal event LazyLoadObjectDecimal<Organizace> LoadOrganizace = null;
        public Organizace Organizace
        {
            get
            {
                if (organizace == null)
                    if (LoadOrganizace != null)
                        LoadOrganizace(this, ID_Organizace, ref organizace);

                return organizace;
            }
            set { organizace = value; ID_Organizace = value.ID; }
        }

        public Cis_Typu_Kategorii typ = null;
        internal event LazyLoadObjectDecimal<Cis_Typu_Kategorii> LoadTyp = null;
        public Cis_Typu_Kategorii Typ
        {
            get
            {
                if (typ == null)
                    if (LoadTyp != null)
                        LoadTyp(this, TypID, ref typ);

                return typ;
            }
            set { typ = value; TypID = value.ID; }
        }
    }
}
