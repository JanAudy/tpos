﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jadro.Data.DataTypes
{
    public class Skupina : AbstractJadroDto
    {
        public string Kod { get; set; }
        public string Nazev { get; set; }
        public string Popis { get; set; }
        public short Platnost { get; set; }
    }
}
