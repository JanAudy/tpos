﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class WWW_Organizace : WWW
    {
        public decimal ID_Organizace { get; set; }

        public Organizace organizace = null;
        internal event LazyLoadObjectDecimal<Organizace> LoadOrganizace = null;
        public Organizace Organizace
        {
            get
            {
                if (organizace == null)
                    if (LoadOrganizace != null)
                        LoadOrganizace(this, ID_Organizace, ref organizace);

                return organizace;
            }
            set { organizace = value; ID_Organizace = value.ID; }
        }
    }
}
