﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class WWW : AbstractJadroDto
    {
        public decimal TypID { get; set; }
        public string Adresa { get; set; }

        public Cis_Typu_WWW typ = null;
        internal event LazyLoadObjectDecimal<Cis_Typu_WWW> LoadTyp = null;
        public Cis_Typu_WWW Typ
        {
            get
            {
                if (typ == null)
                    if (LoadTyp != null)
                        LoadTyp(this, TypID, ref typ);

                return typ;
            }
            set { typ = value; TypID = value.ID; }
        }
    }
}
