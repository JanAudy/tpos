﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Tel_Osoby : Telefon
    {
        public decimal ID_Osoba { get; set; }

        public Osoba osoba = null;
        internal event LazyLoadObjectDecimal<Osoba> LoadOsoba = null;
        public Osoba Osoba
        {
            get
            {
                if (osoba == null)
                    if (LoadOsoba != null)
                        LoadOsoba(this, ID_Osoba, ref osoba);

                return osoba;
            }
            set { osoba = value; ID_Osoba = value.ID; }
        }
    }
}
