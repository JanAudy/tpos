﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Adresa : AbstractJadroDto
    {
        public decimal TypID { get; set; }
        public string CP { get; set; }
        public string Ulice { get; set; }
        public string Mesto { get; set; }
        public string PSC { get; set; }
        public decimal StatID { get; set; }

        public Cis_Typu_Adres typ = null;
        internal event LazyLoadObjectDecimal<Cis_Typu_Adres> LoadTyp = null;
        public Cis_Typu_Adres Typ
        {
            get
            {
                if (typ == null)
                    if (LoadTyp != null)
                        LoadTyp(this, TypID, ref typ);                    

                return typ;
            }
            set { typ = value; TypID = value.ID; }
        }

        public Cis_Statu stat = null;
        internal event LazyLoadObjectDecimal<Cis_Statu> LoadStat = null;
        public Cis_Statu Stat
        {
            get
            {
                if (stat == null)
                    if (LoadStat != null)
                        LoadStat(this, StatID, ref stat);

                return stat;
            }
            set { stat = value; StatID = value.ID; }
        }
    }
}
