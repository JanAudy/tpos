﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jadro.Data.DataTypes
{
    public class Cis_JMSvatky : AbstractJadroDto
    {
        public short Mesic { get; set; }
        public short Den { get; set; }
        public string Popis { get; set; }
        public short Platnost { get; set; }
    }
}
