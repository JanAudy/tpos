﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Jadro.Data.DataTypes
{
    public class Ucty_Organizace : AbstractJadroDto
    {
        public decimal ID_Organizace { get; set; }
        public string Ucet { get; set; }
        public string IBAN { get; set; }
        public decimal BankaID { get; set; }

        public Cis_Bank banka = null;
        internal event LazyLoadObjectDecimal<Cis_Bank> LoadBanka = null;
        public Cis_Bank Banka
        {
            get
            {
                if (banka == null)
                    if (LoadBanka != null)
                        LoadBanka(this, BankaID, ref banka);

                return banka;
            }
            set { banka = value; BankaID = value.ID; }
        }

        public Organizace organizace = null;
        internal event LazyLoadObjectDecimal<Organizace> LoadOrganizace = null;
        public Organizace Organizace
        {
            get
            {
                if (organizace == null)
                    if (LoadOrganizace != null)
                        LoadOrganizace(this, ID_Organizace, ref organizace);

                return organizace;
            }
            set { organizace = value; ID_Organizace = value.ID; }
        }
    }
}
