﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataAccess
{
    public abstract class AbstractJadroDtoDao<T> : AbstractIDDecimalDao<T> where T:AbstractJadroDto, new()
    {
        public AbstractJadroDtoDao(SqlConnection connection, string tableName) : base(connection, tableName) { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref T obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.AUTOR = Interprete(reader, cols, "AUTOR", obj.AUTOR);
            obj.DT_AKCE = Interprete(reader, cols, "DT_AKCE", obj.DT_AKCE);
            obj.AKCE = Interprete(reader, cols, "AKCE", obj.AKCE);
        }
        
        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "AUTOR", obj.AUTOR);
            obj.DT_AKCE = DateTime.Now;
            AddParam(parameters, "DT_AKCE", obj.DT_AKCE);
            AddParam(parameters, "AKCE", (insert)?'I':'U');
        }
    }
}
