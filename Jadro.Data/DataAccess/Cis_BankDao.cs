﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class Cis_BankDao : AbstractJadroDtoDao<Cis_Bank>
    {
        public Cis_BankDao(SqlConnection connection) : base(connection, "CIS_BANK") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Cis_Bank obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Nazev = Interprete(reader, cols, "Nazev", obj.Nazev);
            obj.Kod = Interprete(reader, cols, "Kod", obj.Kod);
            obj.Swift = Interprete(reader, cols, "Swift", obj.Swift);
            obj.StatID = Interprete(reader, cols, "Stat", obj.StatID);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Cis_Bank obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Nazev", obj.Nazev);
            AddParam(parameters, "Kod", obj.Kod);
            AddParam(parameters, "Swift", obj.Swift);
            AddParam(parameters, "Stat", obj.StatID);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
