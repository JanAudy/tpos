﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Typu_AdresDao : CiselnikDao<Cis_Typu_Adres>
    {
        public Cis_Typu_AdresDao(SqlConnection connection) : base(connection, "Cis_Typu_Adres") { }
    }
}
