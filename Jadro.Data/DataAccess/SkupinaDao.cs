﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class SkupinaDao : AbstractJadroDtoDao<Skupina>
    {
        public SkupinaDao(SqlConnection connection) : base(connection, "Oddeleni") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Skupina obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Kod = Interprete(reader, cols, "Kod", obj.Kod);
            obj.Nazev = Interprete(reader, cols, "Nazev", obj.Nazev);
            obj.Popis = Interprete(reader, cols, "Popis", obj.Popis);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Skupina obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Kod", obj.Kod);
            AddParam(parameters, "Nazev", obj.Nazev);
            AddParam(parameters, "Popis", obj.Popis);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
