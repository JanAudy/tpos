﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class CiselnikDao<T> : AbstractJadroDtoDao<T> where T:Ciselnik, new()
    {
        public CiselnikDao(SqlConnection connection, string tableName) : base(connection, tableName) { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref T obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Popis = Interprete(reader, cols, "Popis", obj.Popis);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Popis", obj.Popis);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
