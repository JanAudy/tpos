﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Tel_OsobyDao : TelefonDao<Tel_Osoby>
    {
        public Tel_OsobyDao(SqlConnection connection) : base(connection, "Tel_Osoby") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Tel_Osoby obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Osoba = Interprete(reader, cols, "ID_Osoba", obj.TypID);
        }

        public override void FillParams(Tel_Osoby obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Osoba", obj.ID_Osoba);
        }
    }
}
