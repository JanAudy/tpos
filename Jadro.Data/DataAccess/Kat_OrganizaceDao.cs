﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class Kat_OrganizaceDao : AbstractJadroDtoDao<Kat_Organizace>
    {
        public Kat_OrganizaceDao(SqlConnection connection) : base(connection, "Kat_Organizace") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Kat_Organizace obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Organizace = Interprete(reader, cols, "ID_Organizace", obj.ID_Organizace);
            obj.TypID = Interprete(reader, cols, "Typ", obj.TypID);
        }

        public override void FillParams(Kat_Organizace obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Organizace", obj.ID_Organizace);
            AddParam(parameters, "Typ", obj.TypID);
        }
    }
}
