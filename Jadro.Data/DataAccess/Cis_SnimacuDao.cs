﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_SnimacuDao : CiselnikDao<Cis_Snimacu>
    {
        public Cis_SnimacuDao(SqlConnection connection) : base(connection, "Cis_Snimacu") { }
    }
}
