﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_MesiceDao : CiselnikDao<Cis_Mesice>
    {
        public Cis_MesiceDao(SqlConnection connection) : base(connection, "Cis_Mesice") { }
    }
}
