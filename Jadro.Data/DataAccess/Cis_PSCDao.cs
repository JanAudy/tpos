﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class Cis_PSCDao : AbstractJadroDtoDao<Cis_PSC>
    {
        public Cis_PSCDao(SqlConnection connection) : base(connection, "CIS_PSC") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Cis_PSC obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Obec = Interprete(reader, cols, "Obec", obj.Obec);
            obj.PSC = Interprete(reader, cols, "PSC", obj.PSC);
            obj.Posta = Interprete(reader, cols, "Posta", obj.Posta);
            obj.Okres = Interprete(reader, cols, "Okres", obj.Okres);
            obj.Kod_Okresu = Interprete(reader, cols, "Kod_Okresu", obj.Kod_Okresu);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Cis_PSC obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Obec", obj.Obec);
            AddParam(parameters, "PSC", obj.PSC);
            AddParam(parameters, "Posta", obj.Posta);
            AddParam(parameters, "Okres", obj.Okres);
            AddParam(parameters, "Kod_Okresu", obj.Kod_Okresu);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
