﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Titulu_ZaDao : CiselnikDao<Cis_Titulu_Za>
    {
        public Cis_Titulu_ZaDao(SqlConnection connection) : base(connection, "Cis_Titulu_Za") { }
    }
}
