﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_IDMediiDao : CiselnikDao<Cis_IDMedii>
    {
        public Cis_IDMediiDao(SqlConnection connection) : base(connection, "Cis_IDMedii") { }
    }
}
