﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Adr_OrganizaceDao : AdresaDao<Adr_Organizace>
    {
        public Adr_OrganizaceDao(SqlConnection connection) : base(connection, "ADR_Organizace") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Adr_Organizace obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Organizace = Interprete(reader, cols, "ID_Organizace", obj.ID_Organizace);
        }

        public override void FillParams(Adr_Organizace obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Organizace", obj.ID_Organizace);
        }
    }
}
