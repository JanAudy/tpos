﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_PrukazuDao : CiselnikDao<Cis_Prukazu>
    {
        public Cis_PrukazuDao(SqlConnection connection) : base(connection, "Cis_Prukazu") { }
    }
}
