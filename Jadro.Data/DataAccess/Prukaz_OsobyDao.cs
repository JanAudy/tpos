﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class Prukaz_OsobyDao : AbstractJadroDtoDao<Prukaz_Osoby>
    {
        public Prukaz_OsobyDao(SqlConnection connection) : base(connection, "PRUKAZ_OSOBY") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Prukaz_Osoby obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_OSOBA = Interprete(reader, cols, "ID_OSOBA", obj.ID_OSOBA);
            obj.TypID = Interprete(reader, cols, "Typ", obj.TypID);
            obj.CisPruk = Interprete(reader, cols, "CisPruk", obj.CisPruk);
            obj.DatVyd = Interprete(reader, cols, "DatVyd", obj.DatVyd);
            obj.PlatnostDo = Interprete(reader, cols, "PlatnostDo", obj.PlatnostDo);
            obj.MistVyd = Interprete(reader, cols, "MistVyd", obj.MistVyd);

        }

        public override void FillParams(Prukaz_Osoby obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_OSOBA", obj.ID_OSOBA);
            AddParam(parameters, "Typ", obj.TypID);
            AddParam(parameters, "CisPruk", obj.CisPruk);
            AddParam(parameters, "DatVyd", obj.DatVyd);
            AddParam(parameters, "PlatnostDo", obj.PlatnostDo);
            AddParam(parameters, "MistVyd", obj.MistVyd);

        }
    }
}
