﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Typu_EmailuDao : CiselnikDao<Cis_Typu_Emailu>
    {
        public Cis_Typu_EmailuDao(SqlConnection connection) : base(connection, "Cis_Typu_Emailu") { }
    }
}
