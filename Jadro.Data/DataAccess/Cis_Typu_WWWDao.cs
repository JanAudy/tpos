﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Typu_WWWDao : CiselnikDao<Cis_Typu_WWW>
    {
        public Cis_Typu_WWWDao(SqlConnection connection) : base(connection, "Cis_Typu_WWW") { }
    }
}
