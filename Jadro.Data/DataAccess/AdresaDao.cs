﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class AdresaDao<T> : AbstractJadroDtoDao<T> where T:Adresa, new()
    {
        public AdresaDao(SqlConnection connection, string tableName) : base(connection, tableName) { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref T obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.TypID = Interprete(reader, cols, "Typ", obj.TypID);
            obj.CP = Interprete(reader, cols, "CP", obj.CP);
            obj.Ulice = Interprete(reader, cols, "Ulice", obj.Ulice);
            obj.Mesto = Interprete(reader, cols, "Mesto", obj.Mesto);
            obj.PSC = Interprete(reader, cols, "PSC", obj.PSC);
            obj.StatID = Interprete(reader, cols, "Stat", obj.StatID);
        }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Typ", obj.TypID);
            AddParam(parameters, "CP", obj.CP);
            AddParam(parameters, "Ulice", obj.Ulice);
            AddParam(parameters, "Mesto", obj.Mesto);
            AddParam(parameters, "PSC", obj.PSC);
            AddParam(parameters, "Stat", obj.StatID);
        }
    }
}
