﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_StatuDao : CiselnikDao<Cis_Statu>
    {
        public Cis_StatuDao(SqlConnection connection) : base(connection, "Cis_Statu") { }
    }
}
