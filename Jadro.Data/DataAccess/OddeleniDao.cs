﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class OddeleniDao : AbstractJadroDtoDao<Oddeleni>
    {
        public OddeleniDao(SqlConnection connection) : base(connection, "Oddeleni") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Oddeleni obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Kod = Interprete(reader, cols, "Kod", obj.Kod);
            obj.Nazev = Interprete(reader, cols, "Nazev", obj.Nazev);
            obj.Misto = Interprete(reader, cols, "Misto", obj.Misto);
            obj.Vedouci = Interprete(reader, cols, "Vedouci", obj.Vedouci);
            obj.Zastupce = Interprete(reader, cols, "Odberatel", obj.Zastupce);
            obj.ID_Organizace = Interprete(reader, cols, "ID_Organizace", obj.ID_Organizace);
            obj.Text = Interprete(reader, cols, "Text", obj.Text);
            obj.Tel1_Row = Interprete(reader, cols, "Tel1_Row", obj.Tel1_Row);
            obj.Tel2_Row = Interprete(reader, cols, "Tel2_Row", obj.Tel2_Row);
            obj.Email_Row = Interprete(reader, cols, "Email_Row", obj.Email_Row);
            obj.WWW_Row = Interprete(reader, cols, "WWW_Row", obj.WWW_Row);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Oddeleni obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Kod", obj.Kod);
            AddParam(parameters, "Nazev", obj.Nazev);
            AddParam(parameters, "Misto", obj.Misto);
            AddParam(parameters, "Vedouci", obj.Vedouci);
            AddParam(parameters, "Zastupce", obj.Zastupce);
            AddParam(parameters, "ID_Organizace", obj.ID_Organizace);
            AddParam(parameters, "Text", obj.Text);
            AddParam(parameters, "Tel1_Row", obj.Tel1_Row);
            AddParam(parameters, "Tel2_Row", obj.Tel2_Row);
            AddParam(parameters, "Email_Row", obj.Email_Row);
            AddParam(parameters, "WWW_Row", obj.WWW_Row);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
