﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Adr_OsobyDao : AdresaDao<Adr_Osoby>
    {
        public Adr_OsobyDao(SqlConnection connection) : base(connection, "ADR_Osoby") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Adr_Osoby obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Osoba = Interprete(reader, cols, "ID_Osoba", obj.ID_Osoba);
        }

        public override void FillParams(Adr_Osoby obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Osoba", obj.ID_Osoba);
        }
    }
}
