﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class OrganizaceDao : AbstractJadroDtoDao<Organizace>
    {
        public OrganizaceDao(SqlConnection connection) : base(connection, "ORGANIZACE") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Organizace obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Kat_Row = Interprete(reader, cols, "Kat_Row", obj.Kat_Row);
            obj.Kod = Interprete(reader, cols, "Kod", obj.Kod);
            obj.Nazev = Interprete(reader, cols, "Nazev", obj.Nazev);
            obj.ICO = Interprete(reader, cols, "ICO", obj.ICO);
            obj.DIC = Interprete(reader, cols, "DIC", obj.DIC);
            obj.Odberatel = Interprete(reader, cols, "Odberatel", obj.Odberatel);
            obj.Dodavatel = Interprete(reader, cols, "Dodavatel", obj.Dodavatel);
            obj.Ostatni = Interprete(reader, cols, "Ostatni", obj.Ostatni);
            obj.Platce_Dph = Interprete(reader, cols, "Platce_Dph", obj.Platce_Dph);
            obj.Je_Organizace = Interprete(reader, cols, "Je_Organizace", obj.Je_Organizace);
            obj.Je_Instituce = Interprete(reader, cols, "Je_Instituce", obj.Je_Instituce);
            obj.Zahranicni = Interprete(reader, cols, "Zahranicni", obj.Zahranicni);
            obj.Neplatic = Interprete(reader, cols, "Neplatic", obj.Neplatic);
            obj.Aktivni = Interprete(reader, cols, "Aktivni", obj.Aktivni);
            obj.Logo = Interprete(reader, cols, "Logo", obj.Logo);
            obj.Poznamka = Interprete(reader, cols, "Poznamka", obj.Poznamka);
            obj.Adr_Row = Interprete(reader, cols, "Adr_Row", obj.Adr_Row);
            obj.Tel1_Row = Interprete(reader, cols, "Tel1_Row", obj.Tel1_Row);
            obj.Tel2_Row = Interprete(reader, cols, "Tel2_Row", obj.Tel2_Row);
            obj.Email_Row = Interprete(reader, cols, "Email_Row", obj.Email_Row);
            obj.WWW_Row = Interprete(reader, cols, "WWW_Row", obj.WWW_Row);
            obj.Banka_Row = Interprete(reader, cols, "Banka_Row", obj.Banka_Row);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Organizace obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Kat_Row", obj.Kat_Row);
            AddParam(parameters, "Kod", obj.Kod);
            AddParam(parameters, "Nazev", obj.Nazev);
            AddParam(parameters, "ICO", obj.ICO);
            AddParam(parameters, "DIC", obj.DIC);
            AddParam(parameters, "Odberatel", obj.Odberatel);
            AddParam(parameters, "Dodavatel", obj.Dodavatel);
            AddParam(parameters, "Ostatni", obj.Ostatni);
            AddParam(parameters, "Platce_Dph", obj.Platce_Dph);
            AddParam(parameters, "Je_Organizace", obj.Je_Organizace);
            AddParam(parameters, "Je_Instituce", obj.Je_Instituce);
            AddParam(parameters, "Zahranicni", obj.Zahranicni);
            AddParam(parameters, "Neplatic", obj.Neplatic);
            AddParam(parameters, "Aktivni", obj.Aktivni);
            AddParam(parameters, "Logo", obj.Logo);
            AddParam(parameters, "Poznamka", obj.Poznamka);
            AddParam(parameters, "Adr_Row", obj.Adr_Row);
            AddParam(parameters, "Tel1_Row", obj.Tel1_Row);
            AddParam(parameters, "Tel2_Row", obj.Tel2_Row);
            AddParam(parameters, "Email_Row", obj.Email_Row);
            AddParam(parameters, "WWW_Row", obj.WWW_Row);
            AddParam(parameters, "Banka_Row", obj.Banka_Row);
            AddParam(parameters, "Platnost", obj.Platnost);
        }

    }
}
