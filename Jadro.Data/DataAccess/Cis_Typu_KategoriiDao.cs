﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Typu_KategoriiDao : CiselnikDao<Cis_Typu_Kategorii>
    {
        public Cis_Typu_KategoriiDao(SqlConnection connection) : base(connection, "Cis_Typu_Kategorii") { }
    }
}
