﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_FunkciDao : CiselnikDao<Cis_Funkci>
    {
        public Cis_FunkciDao(SqlConnection connection) : base(connection, "Cis_Funkci") { }
    }
}
