﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Titulu_PredDao : CiselnikDao<Cis_Titulu_Pred>
    {
        public Cis_Titulu_PredDao(SqlConnection connection) : base(connection, "Cis_Titulu_Pred") { }
    }
}
