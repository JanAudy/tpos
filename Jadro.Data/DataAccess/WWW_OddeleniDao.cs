﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class WWW_OddeleniDao : WWWDao<WWW_Oddeleni>
    {
        public WWW_OddeleniDao(SqlConnection connection) : base(connection, "WWW_Oddeleni") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref WWW_Oddeleni obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Oddeleni = Interprete(reader, cols, "ID_Oddeleni", obj.TypID);
        }

        public override void FillParams(WWW_Oddeleni obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Oddeleni", obj.ID_Oddeleni);
        }
    }
    
}
