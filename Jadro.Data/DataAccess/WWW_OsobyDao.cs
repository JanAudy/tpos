﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class WWW_OsobyDao : WWWDao<WWW_Osoby>
    {
        public WWW_OsobyDao(SqlConnection connection) : base(connection, "WWW_Osoby") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref WWW_Osoby obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Osoba = Interprete(reader, cols, "ID_Osoba", obj.TypID);
        }

        public override void FillParams(WWW_Osoby obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Osoba", obj.ID_Osoba);
        }
    }
}
