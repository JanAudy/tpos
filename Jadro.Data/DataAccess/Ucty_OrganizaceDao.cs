﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Ucty_OrganizaceDao : AbstractJadroDtoDao<Ucty_Organizace>
    {
        public Ucty_OrganizaceDao(SqlConnection connection) : base(connection, "UCTY_ORGANIZACE") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Ucty_Organizace obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.ID_Organizace = Interprete(reader, cols, "ID_Organizace", obj.ID_Organizace);
            obj.Ucet = Interprete(reader, cols, "Ucet", obj.Ucet);
            obj.IBAN = Interprete(reader, cols, "IBAN", obj.IBAN);
            obj.BankaID = Interprete(reader, cols, "Banka", obj.BankaID);
        }

        public override void FillParams(Ucty_Organizace obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "ID_Organizace", obj.ID_Organizace);
            AddParam(parameters, "Ucet", obj.Ucet);
            AddParam(parameters, "IBAN", obj.IBAN);
            AddParam(parameters, "Banka", obj.BankaID);
        }
    }
}
