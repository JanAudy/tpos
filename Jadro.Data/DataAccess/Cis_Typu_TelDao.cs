﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class Cis_Typu_TelDao : CiselnikDao<Cis_Typu_Tel>
    {
        public Cis_Typu_TelDao(SqlConnection connection) : base(connection, "Cis_Typu_Tel") { }
    }
}
