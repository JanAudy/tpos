﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class Cis_JMSvatkyDao : AbstractJadroDtoDao<Cis_JMSvatky>
    {
        public Cis_JMSvatkyDao(SqlConnection connection) : base(connection, "CIS_JMSVATKY") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Cis_JMSvatky obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Mesic = Interprete(reader, cols, "Mesic", obj.Mesic);
            obj.Den = Interprete(reader, cols, "Den", obj.Den);
            obj.Popis = Interprete(reader, cols, "Popis", obj.Popis);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
        }

        public override void FillParams(Cis_JMSvatky obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Mesic", obj.Mesic);
            AddParam(parameters, "Den", obj.Den);
            AddParam(parameters, "Popis", obj.Popis);
            AddParam(parameters, "Platnost", obj.Platnost);
        }
    }
}
