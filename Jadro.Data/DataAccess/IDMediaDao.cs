﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class IDMediaDao : AbstractJadroDtoDao<IDMedia>
    {
        public IDMediaDao(SqlConnection connection) : base(connection, "IDMedia") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref IDMedia obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
            obj.Blokace = Interprete(reader, cols, "Blokace", obj.Blokace);
            obj.Nazev = Interprete(reader, cols, "Nazev", obj.Nazev);
            obj.Typ_Media = Interprete(reader, cols, "Typ_Media", obj.Typ_Media);
            obj.Tag = Interprete(reader, cols, "Tag", obj.Tag);
            obj.OsobaID = Interprete(reader, cols, "Osoba", obj.OsobaID);
            obj.SkupinaID = Interprete(reader, cols, "Skupina", obj.SkupinaID);
            obj.OddeleniID = Interprete(reader, cols, "Oddeleni", obj.OddeleniID);
            obj.OrganizaceID = Interprete(reader, cols, "Organizace", obj.OrganizaceID);
        }

        public override void FillParams(IDMedia obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Platnost", obj.Platnost);
            AddParam(parameters, "Blokace", obj.Blokace);
            AddParam(parameters, "Nazev", obj.Nazev);
            AddParam(parameters, "Typ_Media", obj.Typ_Media);
            AddParam(parameters, "Tag", obj.Tag);
            AddParam(parameters, "Osoba", obj.OsobaID);
            AddParam(parameters, "Skupina", obj.SkupinaID);
            AddParam(parameters, "Oddeleni", obj.OddeleniID);
            AddParam(parameters, "Organizace", obj.OrganizaceID);
        }
    }
}
