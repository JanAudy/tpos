﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataAccess
{
    public class TelefonDao<T> : AbstractJadroDtoDao<T> where T:Telefon, new()
    {
        public TelefonDao(SqlConnection connection, string tableName) : base(connection, tableName) { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref T obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.TypID = Interprete(reader, cols, "Typ", obj.TypID);
            obj.Tel = Interprete(reader, cols, "Tel", obj.Tel);
        }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Typ", obj.TypID);
            AddParam(parameters, "Tel", obj.Tel);
        }
    }
}
