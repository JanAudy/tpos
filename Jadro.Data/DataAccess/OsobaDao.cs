﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using System.Data.SqlClient;

namespace Jadro.Data.DataAccess
{
    public class OsobaDao : AbstractJadroDtoDao<Osoba>
    {
        public OsobaDao(SqlConnection connection) : base(connection, "OSOBA") { }

        public override void Interprete(System.Data.SqlClient.SqlDataReader reader, Microdata.Data.PresentColumnsCollection cols, ref Osoba obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Kod = Interprete(reader, cols, "Kod", obj.Kod);
            obj.Prislusnost = Interprete(reader, cols, "Prislusnost", obj.Prislusnost);
            obj.Rodne_C = Interprete(reader, cols, "Rodne_C", obj.Rodne_C);
            obj.Datum_Narozeni = Interprete(reader, cols, "Datum_Narozeni", obj.Datum_Narozeni);
            obj.Titul_Pred = Interprete(reader, cols, "Titul_Pred", obj.Titul_Pred);
            obj.Pohlavi = Interprete(reader, cols, "Pohlavi", obj.Pohlavi);
            obj.Blokace = Interprete(reader, cols, "Blokace", obj.Blokace);
            obj.Jmeno = Interprete(reader, cols, "Jmeno", obj.Jmeno);
            obj.Jmeno2 = Interprete(reader, cols, "Jmeno2", obj.Jmeno2);
            obj.Prijmeni = Interprete(reader, cols, "Prijmeni", obj.Prijmeni);
            obj.Titul_Za = Interprete(reader, cols, "Titul_Za", obj.Titul_Za);
            obj.Full_Name = Interprete(reader, cols, "Full_Name", obj.Full_Name);
            obj.Foto = Interprete(reader, cols, "Foto", obj.Foto);
            obj.ID_ORGANIZACE = Interprete(reader, cols, "ID_ORGANIZACE", obj.ID_ORGANIZACE);
            obj.ID_FUNKCE = Interprete(reader, cols, "ID_FUNKCE", obj.ID_FUNKCE);
            obj.ID_ODDELENI = Interprete(reader, cols, "ID_ODDELENI", obj.ID_ODDELENI);
            obj.ID_SKUPINA = Interprete(reader, cols, "ID_SKUPINA", obj.ID_SKUPINA);
            obj.Poznamka = Interprete(reader, cols, "Poznamka", obj.Poznamka);
            obj.Adr_Row = Interprete(reader, cols, "Adr_Row", obj.Adr_Row);
            obj.Tel1_Row = Interprete(reader, cols, "Tel1_Row", obj.Tel1_Row);
            obj.Tel2_Row = Interprete(reader, cols, "Tel2_Row", obj.Tel2_Row);
            obj.Email_Row = Interprete(reader, cols, "Email_Row", obj.Email_Row);
            obj.WWW_Row = Interprete(reader, cols, "WWW_Row", obj.WWW_Row);
            obj.Prukaz_Row = Interprete(reader, cols, "Prukaz_Row", obj.Prukaz_Row);
            obj.Platnost = Interprete(reader, cols, "Platnost", obj.Platnost);
            obj.Credit = Interprete(reader, cols, "Credit", obj.Credit);
            obj.Dodavatel = Interprete(reader, cols, "Dodavatel", obj.Dodavatel);
            obj.Odberatel = Interprete(reader, cols, "Odberatel", obj.Odberatel);
            obj.Zahranicni = Interprete(reader, cols, "Zahranicni", obj.Zahranicni);
        }

        public override void FillParams(Osoba obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);

            AddParam(parameters, "Kod", obj.Kod);
            AddParam(parameters, "Prislusnost", obj.Prislusnost);
            AddParam(parameters, "Rodne_C", obj.Rodne_C);
            AddParam(parameters, "Datum_Narozeni", obj.Datum_Narozeni);
            AddParam(parameters, "Titul_Pred", obj.Titul_Pred);
            AddParam(parameters, "Pohlavi", obj.Pohlavi);
            AddParam(parameters, "Blokace", obj.Blokace);
            AddParam(parameters, "Jmeno", obj.Jmeno);
            AddParam(parameters, "Jmeno2", obj.Jmeno2);
            AddParam(parameters, "Prijmeni", obj.Prijmeni);
            AddParam(parameters, "Titul_Za", obj.Titul_Za);
            AddParam(parameters, "Full_Name", obj.Full_Name);
            AddParam(parameters, "Foto", obj.Foto);
            AddParam(parameters, "ID_ORGANIZACE", obj.ID_ORGANIZACE);
            AddParam(parameters, "ID_FUNKCE", obj.ID_FUNKCE);
            AddParam(parameters, "ID_ODDELENI", obj.ID_ODDELENI);
            AddParam(parameters, "ID_SKUPINA", obj.ID_SKUPINA);
            AddParam(parameters, "Poznamka", obj.Poznamka);
            AddParam(parameters, "Adr_Row", obj.Adr_Row);
            AddParam(parameters, "Tel1_Row", obj.Tel1_Row);
            AddParam(parameters, "Tel2_Row", obj.Tel2_Row);
            AddParam(parameters, "Email_Row", obj.Email_Row);            
            AddParam(parameters, "WWW_Row", obj.WWW_Row);
            AddParam(parameters, "Prukaz_Row", obj.Prukaz_Row);
            AddParam(parameters, "Platnost", obj.Platnost);
            AddParam(parameters, "Credit", obj.Credit);
            AddParam(parameters, "Dodavatel", obj.Dodavatel);
            AddParam(parameters, "Odberatel", obj.Odberatel);
            AddParam(parameters, "Zahranicni", obj.Zahranicni);
        }
    }
}
