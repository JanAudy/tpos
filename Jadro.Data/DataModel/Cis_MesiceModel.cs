﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_MesiceModel : BaseJadroModel<Cis_Mesice>
    {
        #region Singleton

        private static Cis_MesiceModel instance;

        public static Cis_MesiceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_MesiceModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_MesiceModel() : base(JadroDB.Instance.CisMesiceDao)
        {

        }
    }
}
