﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Email_OrganizaceModel : EmailModel<Email_Organizace>
    {
        #region Singleton

        private static Email_OrganizaceModel instance;

        public static Email_OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Email_OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected Email_OrganizaceModel() : base(JadroDB.Instance.EmailOrganizaceDao) { }

        public override void InitLazy(Email_Organizace res)
        {
            base.InitLazy(res);
            res.LoadOrganizace += LoadOrganizace;
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }

        public List<Email_Organizace> GetByOrganizace(decimal id)
        {
            return GetByParent("ID_ORGANIZACE", id);
        }
    }
}
