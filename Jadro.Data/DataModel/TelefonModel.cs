﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Jadro.Data.DataAccess;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class TelefonModel<T> : BaseJadroModel<T> where T:Telefon, new()
    {
        protected TelefonModel(TelefonDao<T> dao)
            : base(dao)
        {

        }

        public override void InitLazy(T res)
        {
            base.InitLazy(res);
            res.LoadTyp += LoadTyp;
        }

        public void LoadTyp(object sender, decimal typID, ref Cis_Typu_Tel typ)
        {
            typ = Cis_Typu_TelModel.Instance.Get(typID);
        }

        public virtual List<T> GetByParent(string attrib, decimal id)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint(attrib, ConstraintOperation.Equal, id));
            return GetList(constraints);
        }

    }
}
