﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_IDMediiModel : BaseJadroModel<Cis_IDMedii>
    {
        #region Singleton

        private static Cis_IDMediiModel instance;

        public static Cis_IDMediiModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_IDMediiModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_IDMediiModel() : base(JadroDB.Instance.CisIDMediiDao)
        {

        }
    }
}
