﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Typu_KategoriiModel : BaseJadroModel<Cis_Typu_Kategorii>
    {
        #region Singleton

        private static Cis_Typu_KategoriiModel instance;

        public static Cis_Typu_KategoriiModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Typu_KategoriiModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Typu_KategoriiModel() : base(JadroDB.Instance.CisTypu_KategoriiDao)
        {

        }
    }
}
