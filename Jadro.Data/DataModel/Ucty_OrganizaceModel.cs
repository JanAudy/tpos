﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Ucty_OrganizaceModel : BaseJadroModel<Ucty_Organizace>
    {
        #region Singleton

        private static Ucty_OrganizaceModel instance;

        public static Ucty_OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Ucty_OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected Ucty_OrganizaceModel()
            : base(JadroDB.Instance.UctyOrganizaceDao)
        {

        }

        public override void InitLazy(Ucty_Organizace res)
        {
            res.LoadBanka += LoadBanka;
        }

        public void LoadBanka(object sender, decimal bankID, ref Cis_Bank typ)
        {
            typ = Cis_BankModel.Instance.Get(bankID);
        }

        public virtual List<Ucty_Organizace> GetByOrganizace(decimal id)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("ID_ORGANIZACE", ConstraintOperation.Equal, id));
            return GetList(constraints);
        }
    }
}
