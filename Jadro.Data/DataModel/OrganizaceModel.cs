﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class OrganizaceModel : BaseJadroModel<Organizace>
    {
        #region Singleton

        private static OrganizaceModel instance;

        public static OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected OrganizaceModel()
            : base(JadroDB.Instance.OrganizaceDao)
        {

        }

        public List<Organizace> GetOdberatele()
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint("Odberatel", ConstraintOperation.Equal, "1"));
            return GetList(cons);
        }

        public override Organizace Create()
        {
            Organizace o  = base.Create();

            o.Adresy = new List<Adr_Organizace>();
            o.Emaily = new List<Email_Organizace>();
            o.Telefony = new List<Tel_Organizace>();
            o.WWW = new List<WWW_Organizace>();
            o.Ucty_Organizace = new List<Ucty_Organizace>();
            return o;
        }
        public override Organizace Load(decimal id)
        {
            Organizace res = base.Load(id);
            res.LoadAdresy += LoadAdresy;
            res.LoadEmaily += LoadEmaily;
            res.LoadTelefony += LoadTelefony;
            res.LoadWWW += LoadWWW;
            res.LoadUcty_Organizace += LoadUcty;
            return res;
        }

        public void LoadAdresy(object sender, decimal organizaceID, ref List<Adr_Organizace> adresy)
        {
            adresy = Adr_OrganizaceModel.Instance.GetByOrganizace(organizaceID);
        }

        public void LoadEmaily(object sender, decimal organizaceID, ref List<Email_Organizace> emaily)
        {
            emaily = Email_OrganizaceModel.Instance.GetByOrganizace(organizaceID);
        }

        public void LoadTelefony(object sender, decimal organizaceID, ref List<Tel_Organizace> telefony)
        {
            telefony = Tel_OrganizaceModel.Instance.GetByOrganizace(organizaceID);
        }

        public void LoadWWW(object sender, decimal organizaceID, ref List<WWW_Organizace> www)
        {
            www = WWW_OrganizaceModel.Instance.GetByOrganizace(organizaceID);
        }

        public void LoadUcty(object sender, decimal organizaceID, ref List<Ucty_Organizace> ucty)
        {
            ucty = Ucty_OrganizaceModel.Instance.GetByOrganizace(organizaceID);
        }

        protected override bool SaveOrUpdateChildren(Organizace obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.Adresy != null)
            {
                foreach (Adr_Organizace o in obj.Adresy)
                    res &= Adr_OrganizaceModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Emaily != null)
            {
                foreach (Email_Organizace o in obj.Emaily)
                    res &= Email_OrganizaceModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Telefony != null)
            {
                foreach (Tel_Organizace o in obj.Telefony)
                    res &= Tel_OrganizaceModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.WWW != null)
            {
                foreach (WWW_Organizace o in obj.WWW)
                    res &= WWW_OrganizaceModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Ucty_Organizace != null)
            {
                foreach (Ucty_Organizace o in obj.Ucty_Organizace)
                    res &= Ucty_OrganizaceModel.Instance.SaveOrUpdate(o);
            }

            return res;
        }

        public Organizace GetByNazev(string nazev)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Nazev", ConstraintOperation.Equal, nazev));
            return dao.Get(constraints);
        }
    }
}
