﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Typu_EmailuModel : BaseJadroModel<Cis_Typu_Emailu>
    {
        #region Singleton

        private static Cis_Typu_EmailuModel instance;

        public static Cis_Typu_EmailuModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Typu_EmailuModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Typu_EmailuModel() : base(JadroDB.Instance.CisTypu_EmailuDao)
        {

        }
    }
}
