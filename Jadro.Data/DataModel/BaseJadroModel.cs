﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataModel;
using Microdata.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class BaseJadroModel<T> : AbstractDecimalModel<T> where T:AbstractIDDecimalDto, new()
    {
        protected AbstractIDDecimalDao<T> dao = null;
        
        public BaseJadroModel(AbstractIDDecimalDao<T> dao)
        {
            this.dao = dao;
        }

        public override T Create()
        {
            T obj = new T();
            return obj;
        }

        protected virtual bool SaveOrUpdateChildren(T obj) { return true; }

        public override bool SaveOrUpdate(T obj)
        {
            bool res = dao.SaveOrUpdate(obj);
            if (res)
            {
                res &= SaveOrUpdateChildren(obj);
            }

            return res;
        }

        public virtual void InitLazy(T obj) {}

        public override T Load(decimal id)
        {
            T t = dao.Get(id);
            InitLazy(t);
            return t;
        }

        public override List<T> GetList(List<Constraint> constraints)
        {
            List<T> result = null;

            result = dao.GetList(constraints);
            foreach (T r in result)
            {
                InitLazy(r);
            }

            return result;
        }
    }
}
