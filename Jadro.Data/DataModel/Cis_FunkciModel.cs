﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_FunkciModel : BaseJadroModel<Cis_Funkci>
    {
        #region Singleton

        private static Cis_FunkciModel instance;

        public static Cis_FunkciModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_FunkciModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_FunkciModel() : base(JadroDB.Instance.CisFunkciDao)
        {

        }
    }
}
