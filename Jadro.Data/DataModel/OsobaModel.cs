﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class OsobaModel : BaseJadroModel<Osoba>
    {
        #region Singleton

        private static OsobaModel instance;

        public static OsobaModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OsobaModel();
                }
                return instance;
            }
        }

        #endregion

        protected OsobaModel()
            : base(JadroDB.Instance.OsobaDao)
        {

        }

        public List<Osoba> GetOdberatele()
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint("Odberatel", ConstraintOperation.Equal, "1"));
            return GetList(cons);
        }

        public override Osoba Create()
        {
            Osoba o  = base.Create();
            o.Adresy = new List<Adr_Osoby>();
            o.Emaily = new List<Email_Osoby>();
            o.Telefony = new List<Tel_Osoby>();
            o.WWW = new List<WWW_Osoby>();
            return o;
        }
        public override Osoba Load(decimal id)
        {
            Osoba res = base.Load(id);
            res.LoadAdresy += LoadAdresy;
            res.LoadEmaily += LoadEmaily;
            res.LoadTelefony += LoadTelefony;
            res.LoadWWW += LoadWWW;
            res.LoadOddeleni += LoadOddeleni;
            res.LoadOrganizace += LoadOrganizace;
            return res;
        }

        public void LoadAdresy(object sender, decimal osobaID, ref List<Adr_Osoby> adresy)
        {
            adresy = Adr_OsobyModel.Instance.GetByOsoba(osobaID);
        }

        public void LoadEmaily(object sender, decimal osobaID, ref List<Email_Osoby> emaily)
        {
            emaily = Email_OsobyModel.Instance.GetByOsoba(osobaID);
        }

        public void LoadTelefony(object sender, decimal osobaID, ref List<Tel_Osoby> telefony)
        {
            telefony = Tel_OsobyModel.Instance.GetByOsoba(osobaID);
        }

        public void LoadWWW(object sender, decimal osobaID, ref List<WWW_Osoby> www)
        {
            www = WWW_OsobyModel.Instance.GetByOsoba(osobaID);
        }

        public void LoadOddeleni(object sender, decimal oddeleniID, ref Oddeleni oddeleni)
        {
            oddeleni = OddeleniModel.Instance.Get(oddeleniID);
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }

        protected override bool SaveOrUpdateChildren(Osoba obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.Adresy != null)
            {
                foreach (Adr_Osoby o in obj.Adresy)
                    res &= Adr_OsobyModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Emaily != null)
            {
                foreach (Email_Osoby o in obj.Emaily)
                    res &= Email_OsobyModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Telefony != null)
            {
                foreach (Tel_Osoby o in obj.Telefony)
                    res &= Tel_OsobyModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.WWW != null)
            {
                foreach (WWW_Osoby o in obj.WWW)
                    res &= WWW_OsobyModel.Instance.SaveOrUpdate(o);
            }

            return res;
        }

    }
}
