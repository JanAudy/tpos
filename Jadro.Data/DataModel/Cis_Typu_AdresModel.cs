﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Typu_AdresModel : BaseJadroModel<Cis_Typu_Adres>
    {
        #region Singleton

        private static Cis_Typu_AdresModel instance;

        public static Cis_Typu_AdresModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Typu_AdresModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Typu_AdresModel() : base(JadroDB.Instance.CisTypu_AdresDao)
        {

        }
    }
}
