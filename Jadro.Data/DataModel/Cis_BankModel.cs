﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_BankModel : BaseJadroModel<Cis_Bank>
    {
        #region Singleton

        private static Cis_BankModel instance;

        public static Cis_BankModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_BankModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_BankModel()
            : base(JadroDB.Instance.CisBankDao)
        {

        }

        public override Cis_Bank Load(decimal id)
        {
            Cis_Bank res = base.Load(id);
            res.LoadStat += LoadStat;
            return res;
        }


        public void LoadStat(object banka, decimal stateID, ref Cis_Statu stat)
        {
            stat = Cis_StatuModel.Instance.Get(stateID);
        }
    }
}
