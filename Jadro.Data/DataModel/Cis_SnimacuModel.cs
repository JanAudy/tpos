﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_SnimacuModel : BaseJadroModel<Cis_Snimacu>
    {
        #region Singleton

        private static Cis_SnimacuModel instance;

        public static Cis_SnimacuModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_SnimacuModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_SnimacuModel() : base(JadroDB.Instance.CisSnimacuDao)
        {

        }
    }
}
