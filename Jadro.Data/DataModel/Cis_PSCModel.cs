﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_PSCModel : BaseJadroModel<Cis_PSC>
    {
        #region Singleton

        private static Cis_PSCModel instance;

        public static Cis_PSCModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_PSCModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_PSCModel() : base(JadroDB.Instance.CisPSCDao)
        {

        }
    }
}
