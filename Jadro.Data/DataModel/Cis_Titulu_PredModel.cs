﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Titulu_PredModel : BaseJadroModel<Cis_Titulu_Pred>
    {
        #region Singleton

        private static Cis_Titulu_PredModel instance;

        public static Cis_Titulu_PredModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Titulu_PredModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Titulu_PredModel() : base(JadroDB.Instance.CisTitulu_PredDao)
        {

        }
    }
}
