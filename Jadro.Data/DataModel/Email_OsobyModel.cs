﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Email_OsobyModel : EmailModel<Email_Osoby>
    {
        #region Singleton

        private static Email_OsobyModel instance;

        public static Email_OsobyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Email_OsobyModel();
                }
                return instance;
            }
        }

        #endregion

        protected Email_OsobyModel() : base(JadroDB.Instance.EmailOsobyDao) { }

        public override void InitLazy(Email_Osoby res)
        {
            base.InitLazy(res);
            res.LoadOsoba += LoadOsoba;
        }

        public void LoadOsoba(object sender, decimal osobaID, ref Osoba osoba)
        {
            osoba = OsobaModel.Instance.Get(osobaID);
        }

        public List<Email_Osoby> GetByOsoba(decimal id)
        {
            return GetByParent("ID_OSOBA", id);
        }
    }
}
