﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Email_OddeleniModel : EmailModel<Email_Oddeleni>
    {
        #region Singleton

        private static Email_OddeleniModel instance;

        public static Email_OddeleniModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Email_OddeleniModel();
                }
                return instance;
            }
        }

        #endregion

        protected Email_OddeleniModel() : base(JadroDB.Instance.EmailOddeleniDao) { }

        public override void InitLazy(Email_Oddeleni res)
        {
            base.InitLazy(res);
            res.LoadOddeleni += LoadOddeleni;
        }

        public void LoadOddeleni(object sender, decimal oddeleniID, ref Oddeleni oddeleni)
        {
            oddeleni = OddeleniModel.Instance.Get(oddeleniID);
        }

        public List<Email_Oddeleni> GetByOddeleni(decimal id)
        {
            return GetByParent("ID_ODDELENI", id);
        }
    }
}
