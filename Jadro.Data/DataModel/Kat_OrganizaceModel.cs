﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Jadro.Data.DataAccess;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Kat_OrganizaceModel : BaseJadroModel<Kat_Organizace>
    {
        protected Kat_OrganizaceModel()
            : base(JadroDB.Instance.Kat_OrganizaceDao)
        {

        }

        public override void InitLazy(Kat_Organizace obj)
        {
            base.InitLazy(obj);
            obj.LoadOrganizace += LoadOrganizace;
            obj.LoadTyp += LoadTyp;
        }

        public void LoadTyp(object sender, decimal typID, ref Cis_Typu_Kategorii typ)
        {
            typ = Cis_Typu_KategoriiModel.Instance.Get(typID);
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }


    }
}
