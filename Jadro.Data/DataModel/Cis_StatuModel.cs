﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_StatuModel : BaseJadroModel<Cis_Statu>
    {
        #region Singleton

        private static Cis_StatuModel instance;

        public static Cis_StatuModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_StatuModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_StatuModel()
            : base(JadroDB.Instance.CisStatuDao)
        {

        }

    }
}
