﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Tel_OrganizaceModel : TelefonModel<Tel_Organizace>
    {
        #region Singleton

        private static Tel_OrganizaceModel instance;

        public static Tel_OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Tel_OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected Tel_OrganizaceModel() : base(JadroDB.Instance.TelOrganizaceDao) { }

        public override void InitLazy(Tel_Organizace res)
        {
            base.InitLazy(res);
            res.LoadOrganizace += LoadOrganizace;
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }

        public List<Tel_Organizace> GetByOrganizace(decimal id)
        {
            return GetByParent("ID_ORGANIZACE", id);
        }
    }
}
