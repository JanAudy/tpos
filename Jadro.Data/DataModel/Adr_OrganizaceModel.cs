﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Adr_OrganizaceModel : AdresaModel<Adr_Organizace>
    {
        #region Singleton

        private static Adr_OrganizaceModel instance;

        public static Adr_OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Adr_OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected Adr_OrganizaceModel() : base(JadroDB.Instance.AdrOrganizaceDao) { }

        public override void InitLazy(Adr_Organizace res)
        {
            base.InitLazy(res);
            res.LoadOrganizace += LoadOrganizace;
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }

        public List<Adr_Organizace> GetByOrganizace(decimal id)
        {
            return GetByParent("ID_ORGANIZACE", id);
        }


    }
}
