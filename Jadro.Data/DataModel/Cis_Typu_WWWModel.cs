﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Typu_WWWModel : BaseJadroModel<Cis_Typu_WWW>
    {
        #region Singleton

        private static Cis_Typu_WWWModel instance;

        public static Cis_Typu_WWWModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Typu_WWWModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Typu_WWWModel() : base(JadroDB.Instance.CisTypu_WWWDao)
        {

        }
    }
}
