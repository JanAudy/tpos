﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Titulu_ZaModel : BaseJadroModel<Cis_Titulu_Za>
    {
        #region Singleton

        private static Cis_Titulu_ZaModel instance;

        public static Cis_Titulu_ZaModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Titulu_ZaModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Titulu_ZaModel() : base(JadroDB.Instance.CisTitulu_ZaDao)
        {

        }
    }
}
