﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Adr_OsobyModel : AdresaModel<Adr_Osoby>
    {
        #region Singleton

        private static Adr_OsobyModel instance;

        public static Adr_OsobyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Adr_OsobyModel();
                }
                return instance;
            }
        }

        #endregion

        protected Adr_OsobyModel() : base(JadroDB.Instance.AdrOsobyDao) { }

        public override void InitLazy(Adr_Osoby res)
        {
            base.InitLazy(res);
            res.LoadOsoba += LoadOsoba;
        }

        public void LoadOsoba(object sender, decimal osobaID, ref Osoba osoba)
        {
            osoba = OsobaModel.Instance.Get(osobaID);
        }

        public List<Adr_Osoby> GetByOsoba(decimal id)
        {
            return GetByParent("ID_OSOBA", id);
        }

    }
}
