﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Jadro.Data.DataAccess;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class IDMedialModel : BaseJadroModel<IDMedia>
    {
        #region Singleton

        private static IDMedialModel instance;

        public static IDMedialModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IDMedialModel();
                }
                return instance;
            }
        }

        #endregion

        protected IDMedialModel()
            : base(JadroDB.Instance.IDMediaDao)
        {

        }

        public override void InitLazy(IDMedia obj)
        {
            base.InitLazy(obj);
            obj.LoadTyp += LoadTyp;
            obj.LoadOsoba += LoadOsoba;
            obj.LoadOrganizace += LoadOrganizace;
            obj.LoadOddeleni += LoadOddeleni;
            obj.LoadSkupina += LoadSkupina;
        }

        public void LoadTyp(object sender, decimal typID, ref Cis_IDMedii typ)
        {
            typ = Cis_IDMediiModel.Instance.Get(typID);
        }

        public void LoadOddeleni(object sender, decimal oddeleniID, ref Oddeleni oddeleni)
        {
            oddeleni = OddeleniModel.Instance.Get(oddeleniID);
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }

        public void LoadOsoba(object sender, decimal osobaID, ref Osoba osoba)
        {
            osoba = OsobaModel.Instance.Get(osobaID);
        }

        public void LoadSkupina(object sender, decimal skupinaID, ref Skupina skupina)
        {
            skupina = SkupinaModel.Instance.Get(skupinaID);
        }


        public List<IDMedia> GetList(decimal osoba)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint("Osoba", ConstraintOperation.Equal, osoba));

            return dao.GetList(constraints);
        }
    }
}
