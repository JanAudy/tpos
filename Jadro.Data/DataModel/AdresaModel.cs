﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Jadro.Data.DataAccess;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class AdresaModel<T> : BaseJadroModel<T> where T:Adresa, new()
    {
        protected AdresaModel(AdresaDao<T> dao)
            : base(dao)
        {

        }

        public override void InitLazy(T res)
        {
            base.InitLazy(res);
            res.LoadStat += LoadStat;
            res.LoadTyp += LoadTyp;
        }

        public void LoadStat(object sender, decimal stateID, ref Cis_Statu stat)
        {
            stat = Cis_StatuModel.Instance.Get(stateID);
        }

        public void LoadTyp(object sender, decimal typID, ref Cis_Typu_Adres typ)
        {
            typ = Cis_Typu_AdresModel.Instance.Get(typID);
        }

        public virtual List<T> GetByParent(string attrib, decimal id)
        {
            List<Constraint> constraints = new List<Constraint>();
            constraints.Add(new Constraint(attrib, ConstraintOperation.Equal, id));
            return GetList(constraints);
        }

    }
}
