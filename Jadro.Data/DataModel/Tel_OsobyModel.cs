﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Tel_OsobyModel : TelefonModel<Tel_Osoby>
    {
        #region Singleton

        private static Tel_OsobyModel instance;

        public static Tel_OsobyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Tel_OsobyModel();
                }
                return instance;
            }
        }

        #endregion

        protected Tel_OsobyModel() : base(JadroDB.Instance.TelOsobyDao) { }

        public override void InitLazy(Tel_Osoby res)
        {
            base.InitLazy(res);
            res.LoadOsoba += LoadOsoba;
        }

        public void LoadOsoba(object sender, decimal osobaID, ref Osoba osoba)
        {
            osoba = OsobaModel.Instance.Get(osobaID);
        }

        public List<Tel_Osoby> GetByOsoba(decimal id)
        {
            return GetByParent("ID_OSOBA", id);
        }
    }
}
