﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_PrukazuModel : BaseJadroModel<Cis_Prukazu>
    {
        #region Singleton

        private static Cis_PrukazuModel instance;

        public static Cis_PrukazuModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_PrukazuModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_PrukazuModel() : base(JadroDB.Instance.CisPrukazuDao)
        {

        }
    }
}
