﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class SkupinaModel : BaseJadroModel<Skupina>
    {
        #region Singleton

        private static SkupinaModel instance;

        public static SkupinaModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SkupinaModel();
                }
                return instance;
            }
        }

        #endregion

        protected SkupinaModel()
            : base(JadroDB.Instance.SkupinaDao)
        {

        }
    }
}
