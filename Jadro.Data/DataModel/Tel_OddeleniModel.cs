﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class Tel_OddeleniModel : TelefonModel<Tel_Oddeleni>
    {
        #region Singleton

        private static Tel_OddeleniModel instance;

        public static Tel_OddeleniModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Tel_OddeleniModel();
                }
                return instance;
            }
        }

        #endregion

        protected Tel_OddeleniModel() : base(JadroDB.Instance.TelOddeleniDao) { }

        public override void InitLazy(Tel_Oddeleni res)
        {
            base.InitLazy(res);
            res.LoadOddeleni += LoadOddeleni;
        }

        public void LoadOddeleni(object sender, decimal oddeleniID, ref Oddeleni oddeleni)
        {
            oddeleni = OddeleniModel.Instance.Get(oddeleniID);
        }

        public List<Tel_Oddeleni> GetByOddeleni(decimal id)
        {
            return GetByParent("ID_ODDELENI", id);
        }
    }
}
