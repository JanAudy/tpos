﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_JMSvatkyModel : BaseJadroModel<Cis_JMSvatky>
    {
        #region Singleton

        private static Cis_JMSvatkyModel instance;

        public static Cis_JMSvatkyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_JMSvatkyModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_JMSvatkyModel() : base(JadroDB.Instance.CisJMSvatkyDao)
        {

        }
    }
}
