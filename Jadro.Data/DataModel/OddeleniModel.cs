﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class OddeleniModel : BaseJadroModel<Oddeleni>
    {
        #region Singleton

        private static OddeleniModel instance;

        public static OddeleniModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OddeleniModel();
                }
                return instance;
            }
        }

        #endregion

        protected OddeleniModel()
            : base(JadroDB.Instance.OddeleniDao)
        {

        }

        public override Oddeleni Create()
        {
            Oddeleni o  = base.Create();
            o.Emaily = new List<Email_Oddeleni>();
            o.Telefony = new List<Tel_Oddeleni>();
            o.WWW = new List<WWW_Oddeleni>();
            return o;
        }
        public override Oddeleni Load(decimal id)
        {
            Oddeleni res = base.Load(id);
            res.LoadEmaily += LoadEmaily;
            res.LoadTelefony += LoadTelefony;
            res.LoadWWW += LoadWWW;
            return res;
        }

        public void LoadEmaily(object sender, decimal organizaceID, ref List<Email_Oddeleni> emaily)
        {
            emaily = Email_OddeleniModel.Instance.GetByOddeleni(organizaceID);
        }

        public void LoadTelefony(object sender, decimal organizaceID, ref List<Tel_Oddeleni> telefony)
        {
            telefony = Tel_OddeleniModel.Instance.GetByOddeleni(organizaceID);
        }

        public void LoadWWW(object sender, decimal organizaceID, ref List<WWW_Oddeleni> www)
        {
            www = WWW_OddeleniModel.Instance.GetByOddeleni(organizaceID);
        }

        protected override bool SaveOrUpdateChildren(Oddeleni obj)
        {
            bool res = base.SaveOrUpdateChildren(obj);

            if (res && obj.Emaily != null)
            {
                foreach (Email_Oddeleni o in obj.Emaily)
                    res &= Email_OddeleniModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.Telefony != null)
            {
                foreach (Tel_Oddeleni o in obj.Telefony)
                    res &= Tel_OddeleniModel.Instance.SaveOrUpdate(o);
            }

            if (res && obj.WWW != null)
            {
                foreach (WWW_Oddeleni o in obj.WWW)
                    res &= WWW_OddeleniModel.Instance.SaveOrUpdate(o);
            }

            return res;
        }
    }
}
