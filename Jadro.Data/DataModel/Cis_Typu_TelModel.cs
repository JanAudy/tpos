﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_Typu_TelModel : BaseJadroModel<Cis_Typu_Tel>
    {
        #region Singleton

        private static Cis_Typu_TelModel instance;

        public static Cis_Typu_TelModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_Typu_TelModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_Typu_TelModel() : base(JadroDB.Instance.CisTypu_TelDao)
        {

        }
    }
}
