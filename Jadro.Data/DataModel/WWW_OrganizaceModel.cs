﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class WWW_OrganizaceModel : WWWModel<WWW_Organizace>
    {
        #region Singleton

        private static WWW_OrganizaceModel instance;

        public static WWW_OrganizaceModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WWW_OrganizaceModel();
                }
                return instance;
            }
        }

        #endregion

        protected WWW_OrganizaceModel() : base(JadroDB.Instance.WwwOrganizaceDao) { }

        public override void InitLazy(WWW_Organizace res)
        {
            base.InitLazy(res);
            res.LoadOrganizace += LoadOrganizace;
        }

        public void LoadOrganizace(object sender, decimal organizaceID, ref Organizace organizace)
        {
            organizace = OrganizaceModel.Instance.Get(organizaceID);
        }


        public List<WWW_Organizace> GetByOrganizace(decimal id)
        {
            return GetByParent("ID_ORGANIZACE", id);
        }
    }
}
