﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;

namespace Jadro.Data.DataModel
{
    public class Cis_DnyModel : BaseJadroModel<Cis_Dny>
    {
        #region Singleton

        private static Cis_DnyModel instance;

        public static Cis_DnyModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cis_DnyModel();
                }
                return instance;
            }
        }

        #endregion

        protected Cis_DnyModel() : base(JadroDB.Instance.CisDnyDao)
        {

        }
    }
}
