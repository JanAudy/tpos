﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jadro.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Jadro.Data.DataModel
{
    public class WWW_OddeleniModel : WWWModel<WWW_Oddeleni>
    {
        #region Singleton

        private static WWW_OddeleniModel instance;

        public static WWW_OddeleniModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WWW_OddeleniModel();
                }
                return instance;
            }
        }

        #endregion

        protected WWW_OddeleniModel() : base(JadroDB.Instance.WwwOddeleniDao) { }

        public override void InitLazy(WWW_Oddeleni res)
        {
            base.InitLazy(res);
            res.LoadOddeleni += LoadOddeleni;
        }

        public void LoadOddeleni(object sender, decimal oddeleniID, ref Oddeleni oddeleni)
        {
            oddeleni = OddeleniModel.Instance.Get(oddeleniID);
        }

        public List<WWW_Oddeleni> GetByOddeleni(decimal id)
        {
            return GetByParent("ID_ODDELENI", id);
        }
    }
}
