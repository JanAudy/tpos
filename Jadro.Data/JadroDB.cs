﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data;
using Jadro.Data.DataTypes;
using Jadro.Data.DataAccess;

namespace Jadro.Data
{
    public class JadroDB : AbstractDB<JadroDB>
    {
        public override string DB_Name
        {
            get { return "MDATA_JADRO"; }
        }

        private Adr_OrganizaceDao adrOrganizaceDao = null;
        public Adr_OrganizaceDao AdrOrganizaceDao
        {
            get
            {
                if (adrOrganizaceDao == null)
                    adrOrganizaceDao = new Adr_OrganizaceDao(Connection);
                return adrOrganizaceDao;
            }
        }

        private Adr_OsobyDao adrOsobyDao = null;
        public Adr_OsobyDao AdrOsobyDao
        {
            get
            {
                if (adrOsobyDao == null)
                    adrOsobyDao = new Adr_OsobyDao(Connection);
                return adrOsobyDao;
            }
        }

        private Cis_BankDao cisBankDao = null;
        public Cis_BankDao CisBankDao
        {
            get
            {
                if (cisBankDao == null)
                    cisBankDao = new Cis_BankDao(Connection);
                return cisBankDao;
            }
        }

        private Cis_DnyDao cisDnyDao = null;
        public Cis_DnyDao CisDnyDao
        {
            get
            {
                if (cisDnyDao == null)
                    cisDnyDao = new Cis_DnyDao(Connection);
                return cisDnyDao;
            }
        }

        private Cis_FunkciDao cisFunkciDao = null;
        public Cis_FunkciDao CisFunkciDao
        {
            get
            {
                if (cisFunkciDao == null)
                    cisFunkciDao = new Cis_FunkciDao(Connection);
                return cisFunkciDao;
            }
        }

        private Cis_IDMediiDao cisIDMediiDao = null;
        public Cis_IDMediiDao CisIDMediiDao
        {
            get
            {
                if (cisIDMediiDao == null)
                    cisIDMediiDao = new Cis_IDMediiDao(Connection);
                return cisIDMediiDao;
            }
        }

        private Cis_JMSvatkyDao cisJMSvatkyDao = null;
        public Cis_JMSvatkyDao CisJMSvatkyDao
        {
            get
            {
                if (cisJMSvatkyDao == null)
                    cisJMSvatkyDao = new Cis_JMSvatkyDao(Connection);
                return cisJMSvatkyDao;
            }
        }

        private Cis_MesiceDao cisMesiceDao = null;
        public Cis_MesiceDao CisMesiceDao
        {
            get
            {
                if (cisMesiceDao == null)
                    cisMesiceDao = new Cis_MesiceDao(Connection);
                return cisMesiceDao;
            }
        }

        private Cis_PrukazuDao cisPrukazuDao = null;
        public Cis_PrukazuDao CisPrukazuDao
        {
            get
            {
                if (cisPrukazuDao == null)
                    cisPrukazuDao = new Cis_PrukazuDao(Connection);
                return cisPrukazuDao;
            }
        }

        private Cis_PSCDao cisPSCDao = null;
        public Cis_PSCDao CisPSCDao
        {
            get
            {
                if (cisPSCDao == null)
                    cisPSCDao = new Cis_PSCDao(Connection);
                return cisPSCDao;
            }
        }

        private Cis_SnimacuDao cisSnimacuDao = null;
        public Cis_SnimacuDao CisSnimacuDao
        {
            get
            {
                if (cisSnimacuDao == null)
                    cisSnimacuDao = new Cis_SnimacuDao(Connection);
                return cisSnimacuDao;
            }
        }

        private Cis_StatuDao cisStatuDao = null;
        public Cis_StatuDao CisStatuDao
        {
            get
            {
                if (cisStatuDao == null)
                    cisStatuDao = new Cis_StatuDao(Connection);
                return cisStatuDao;
            }
        }

        private Cis_Titulu_PredDao cisTitulu_PredDao = null;
        public Cis_Titulu_PredDao CisTitulu_PredDao
        {
            get
            {
                if (cisTitulu_PredDao == null)
                    cisTitulu_PredDao = new Cis_Titulu_PredDao(Connection);
                return cisTitulu_PredDao;
            }
        }

        private Cis_Titulu_ZaDao cisTitulu_ZaDao = null;
        public Cis_Titulu_ZaDao CisTitulu_ZaDao
        {
            get
            {
                if (cisTitulu_ZaDao == null)
                    cisTitulu_ZaDao = new Cis_Titulu_ZaDao(Connection);
                return cisTitulu_ZaDao;
            }
        }

        private Cis_Typu_AdresDao cisTypu_AdresDao = null;
        public Cis_Typu_AdresDao CisTypu_AdresDao
        {
            get
            {
                if (cisTypu_AdresDao == null)
                    cisTypu_AdresDao = new Cis_Typu_AdresDao(Connection);
                return cisTypu_AdresDao;
            }
        }

        private Cis_Typu_EmailuDao cisTypu_EmailuDao = null;
        public Cis_Typu_EmailuDao CisTypu_EmailuDao
        {
            get
            {
                if (cisTypu_EmailuDao == null)
                    cisTypu_EmailuDao = new Cis_Typu_EmailuDao(Connection);
                return cisTypu_EmailuDao;
            }
        }

        private Cis_Typu_KategoriiDao cisTypu_KategoriiDao = null;
        public Cis_Typu_KategoriiDao CisTypu_KategoriiDao
        {
            get
            {
                if (cisTypu_KategoriiDao == null)
                    cisTypu_KategoriiDao = new Cis_Typu_KategoriiDao(Connection);
                return cisTypu_KategoriiDao;
            }
        }

        private Cis_Typu_TelDao cisTypu_TelDao = null;
        public Cis_Typu_TelDao CisTypu_TelDao
        {
            get
            {
                if (cisTypu_TelDao == null)
                    cisTypu_TelDao = new Cis_Typu_TelDao(Connection);
                return cisTypu_TelDao;
            }
        }

        private Cis_Typu_WWWDao cisTypu_WWWDao = null;
        public Cis_Typu_WWWDao CisTypu_WWWDao
        {
            get
            {
                if (cisTypu_WWWDao == null)
                    cisTypu_WWWDao = new Cis_Typu_WWWDao(Connection);
                return cisTypu_WWWDao;
            }
        }

        private Email_OrganizaceDao emailOrganizaceDao = null;
        public Email_OrganizaceDao EmailOrganizaceDao
        {
            get
            {
                if (emailOrganizaceDao == null)
                    emailOrganizaceDao = new Email_OrganizaceDao(Connection);
                return emailOrganizaceDao;
            }
        }

        private Email_OddeleniDao emailOddeleniDao = null;
        public Email_OddeleniDao EmailOddeleniDao
        {
            get
            {
                if (emailOddeleniDao == null)
                    emailOddeleniDao = new Email_OddeleniDao(Connection);
                return emailOddeleniDao;
            }
        }

        private Email_OsobyDao emailOsobyDao = null;
        public Email_OsobyDao EmailOsobyDao
        {
            get
            {
                if (emailOsobyDao == null)
                    emailOsobyDao = new Email_OsobyDao(Connection);
                return emailOsobyDao;
            }
        }

        private IDMediaDao idMediaDao = null;
        public IDMediaDao IDMediaDao
        {
            get
            {
                if (idMediaDao == null)
                    idMediaDao = new IDMediaDao(Connection);
                return idMediaDao;
            }
        }

        //private IDMediaSummaryDao idMediaSummaryDao = null;
        //public IDMediaSummaryDao IDMediaSummaryDao
        //{
        //    get
        //    {
        //        if (idMediaSummaryDao == null)
        //            idMediaSummaryDao = new IDMediaSummaryDao(Connection);
        //        return idMediaSummaryDao;
        //    }
        //}

        private Kat_OrganizaceDao kat_OrganizaceDao = null;
        public Kat_OrganizaceDao Kat_OrganizaceDao
        {
            get
            {
                if (kat_OrganizaceDao == null)
                    kat_OrganizaceDao = new Kat_OrganizaceDao(Connection);
                return kat_OrganizaceDao;
            }
        }

        private OrganizaceDao organizaceDao = null;
        public OrganizaceDao OrganizaceDao
        {
            get
            {
                if (organizaceDao == null)
                    organizaceDao = new OrganizaceDao(Connection);
                return organizaceDao;
            }
        }

        private OddeleniDao oddeleniDao = null;
        public OddeleniDao OddeleniDao
        {
            get
            {
                if (oddeleniDao == null)
                    oddeleniDao = new OddeleniDao(Connection);
                return oddeleniDao;
            }
        }

        private OsobaDao osobaDao = null;
        public OsobaDao OsobaDao
        {
            get
            {
                if (osobaDao == null)
                    osobaDao = new OsobaDao(Connection);
                return osobaDao;
            }
        }

        private Prukaz_OsobyDao prukazOsobyDao = null;
        public Prukaz_OsobyDao PrukazOsobyDao
        {
            get
            {
                if (prukazOsobyDao == null)
                    prukazOsobyDao = new Prukaz_OsobyDao(Connection);
                return prukazOsobyDao;
            }
        }

        private SkupinaDao skupinaDao = null;
        public SkupinaDao SkupinaDao
        {
            get
            {
                if (skupinaDao == null)
                    skupinaDao = new SkupinaDao(Connection);
                return skupinaDao;
            }
        }

        private Tel_OrganizaceDao telOrganizaceDao = null;
        public Tel_OrganizaceDao TelOrganizaceDao
        {
            get
            {
                if (telOrganizaceDao == null)
                    telOrganizaceDao = new Tel_OrganizaceDao(Connection);
                return telOrganizaceDao;
            }
        }

        private Tel_OddeleniDao telOddeleniDao = null;
        public Tel_OddeleniDao TelOddeleniDao
        {
            get
            {
                if (telOddeleniDao == null)
                    telOddeleniDao = new Tel_OddeleniDao(Connection);
                return telOddeleniDao;
            }
        }

        private Tel_OsobyDao telOsobyDao = null;
        public Tel_OsobyDao TelOsobyDao
        {
            get
            {
                if (telOsobyDao == null)
                    telOsobyDao = new Tel_OsobyDao(Connection);
                return telOsobyDao;
            }
        }

        private Ucty_OrganizaceDao uctyOrganizaceDao = null;
        public Ucty_OrganizaceDao UctyOrganizaceDao
        {
            get
            {
                if (uctyOrganizaceDao == null)
                    uctyOrganizaceDao = new Ucty_OrganizaceDao(Connection);
                return uctyOrganizaceDao;
            }
        }

        private WWW_OrganizaceDao wwwOrganizaceDao = null;
        public WWW_OrganizaceDao WwwOrganizaceDao
        {
            get
            {
                if (wwwOrganizaceDao == null)
                    wwwOrganizaceDao = new WWW_OrganizaceDao(Connection);
                return wwwOrganizaceDao;
            }
        }

        private WWW_OddeleniDao wwwOddeleniDao = null;
        public WWW_OddeleniDao WwwOddeleniDao
        {
            get
            {
                if (wwwOddeleniDao == null)
                    wwwOddeleniDao = new WWW_OddeleniDao(Connection);
                return wwwOddeleniDao;
            }
        }

        private WWW_OsobyDao wwwOsobyDao = null;
        public WWW_OsobyDao WwwOsobyDao
        {
            get
            {
                if (wwwOsobyDao == null)
                    wwwOsobyDao = new WWW_OsobyDao(Connection);
                return wwwOsobyDao;
            }
        }
    }
}
