﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace TestApp
{
    public class ImageUtil
    {
        private static ImageCodecInfo jpegCodec = null;
        private static EncoderParameters jpegParameters = null;

        public static Image GetSmallerImage(Image image, int maxDim)
        {
            if (image.Width < maxDim && image.Height < maxDim)
                return image;

            Image smallImage;
            if (image.Width >= image.Height)
            {
                int newHeight = image.Height * maxDim / image.Width;
                Bitmap bm = new Bitmap(maxDim, newHeight, PixelFormat.Format24bppRgb);
                bm.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                Graphics gr = Graphics.FromImage(bm);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(image, new Rectangle(0, 0, bm.Width, bm.Height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                gr.Dispose();
                smallImage = bm;
            }
            else
            {
                int newWidth = image.Width * maxDim / image.Height;
                Bitmap bm = new Bitmap(newWidth, maxDim, PixelFormat.Format24bppRgb);
                bm.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                Graphics gr = Graphics.FromImage(bm);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(image, new Rectangle(0, 0, bm.Width, bm.Height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                gr.Dispose();
                smallImage = bm;
            }
            return smallImage;
        }

        public static Image GetSmallerImageJPEG(Image image, int maxDim)
        {
            Image sm = GetSmallerImage(image, maxDim);
            MemoryStream str = new MemoryStream();
            sm.Save(str, ImageUtil.JpegCodec, ImageUtil.JpegParameters);
            sm.Dispose();
            return Image.FromStream(str);
        }

        public static byte[] GetSmallerImageJPEGByte(Image image, int maxDim)
        {
            Image sm = GetSmallerImage(image, maxDim);
            MemoryStream str = new MemoryStream();
            sm.Save(str, ImageUtil.JpegCodec, ImageUtil.JpegParameters);
            byte[] bytes = str.ToArray();
            str.Close();
            return bytes;
        }


        public static Image AsImage(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            Image img = null;
            try
            {
                img = Image.FromStream(ms);
            }
            catch
            {
            }
            if (img == null)
            {
                try
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    img = (Image)bf.Deserialize(ms);
                }
                catch { }
            }
            return img;
        }

        public static ImageCodecInfo JpegCodec
        {
            get
            {
                if (jpegCodec == null)
                {
                    ImageCodecInfo[] iciCodecs = ImageCodecInfo.GetImageEncoders();
                    foreach (ImageCodecInfo ic in iciCodecs)
                    {
                        if (ic.MimeType == "image/jpeg")
                        {
                            jpegCodec = ic;
                            break;
                        }
                    }
                }
                return jpegCodec;
            }
        }

        public static EncoderParameters JpegParameters
        {
            get
            {
                if (jpegParameters == null)
                {
                    EncoderParameter epQuality = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)85);
                    jpegParameters = new EncoderParameters(1);
                    jpegParameters.Param[0] = epQuality;
                }
                return jpegParameters;
            }
        }
    }
}
