﻿namespace Transware.Controls
{
    partial class TouchButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TouchButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "TouchButton";
            this.Size = new System.Drawing.Size(75, 23);
            this.EnabledChanged += new System.EventHandler(this.TouchButton_EnabledChanged);
            this.FontChanged += new System.EventHandler(this.TouchButton_FontChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.TouchButton_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TouchButton_MouseDown);
            this.MouseLeave += new System.EventHandler(this.TouchButton_MouseLeave);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TouchButton_MouseUp);
            this.Resize += new System.EventHandler(this.TouchButton_Resize);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
