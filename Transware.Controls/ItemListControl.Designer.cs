﻿namespace Transware.Controls
{
    partial class ItemListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvItems = new BrightIdeasSoftware.ObjectListView();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // lvItems
            // 
            this.lvItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvItems.Location = new System.Drawing.Point(0, 0);
            this.lvItems.Name = "lvItems";
            this.lvItems.OverlayText.BackColor = System.Drawing.SystemColors.Info;
            this.lvItems.OverlayText.BorderColor = System.Drawing.SystemColors.InfoText;
            this.lvItems.OverlayText.BorderWidth = 1F;
            this.lvItems.OverlayText.CornerRounding = 0F;
            this.lvItems.OverlayText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lvItems.OverlayText.InsetX = 1;
            this.lvItems.OverlayText.InsetY = 0;
            this.lvItems.OverlayText.Text = "";
            this.lvItems.OverlayText.TextColor = System.Drawing.SystemColors.InfoText;
            this.lvItems.OwnerDraw = true;
            this.lvItems.ShowGroups = false;
            this.lvItems.Size = new System.Drawing.Size(150, 150);
            this.lvItems.TabIndex = 0;
            this.lvItems.UseCompatibleStateImageBehavior = false;
            this.lvItems.View = System.Windows.Forms.View.Details;
            this.lvItems.ColumnRightClick += new BrightIdeasSoftware.ColumnRightClickEventHandler(this.lvItems_ColumnRightClick);
            this.lvItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvItems_KeyDown);
            // 
            // contextMenu
            // 
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // ItemListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvItems);
            this.Name = "ItemListControl";
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView lvItems;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
    }
}
