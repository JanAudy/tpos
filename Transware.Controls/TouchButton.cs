﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Transware.Controls
{
    [DefaultEvent("Click")]
    public partial class TouchButton : UserControl
    {
        private Bitmap img = null;
        private Color borderColor = SystemColors.ActiveBorder;
        private int borderWidth = 1; 
        
        private Color fillColorTop = SystemColors.Control;
        private Color fillColorBottom = SystemColors.Control;
        
        private string text = "TouchButton";

        private int roundingDiameter = 20;

        private Image image = null;

        private ToolStripItemDisplayStyle displayStyle = ToolStripItemDisplayStyle.Text;

        private bool mouseDown = false;

        private RoundedCornersStyle roundedCorners = new RoundedCornersStyle();

        #region Properties

        [Description("Defines color of the Border"), Category("Appearance")]
        public Color BorderColor { get { return borderColor; } set { borderColor = value; GenerateImage(); Invalidate(); } }
        [Description("Defines width of the Border"), Category("Appearance")]
        public int BorderWidth { get { return borderWidth; } set { borderWidth = value; GenerateImage(); Invalidate(); } }

        [Description("Defines top color of the gradient fill"), Category("Appearance")]
        public Color FillColorTop { get { return fillColorTop; } set { fillColorTop = value; GenerateImage(); Invalidate(); } }
        [Description("Defines bottom color of the gradient fill"), Category("Appearance")]
        public Color FillColorBottom { get { return fillColorBottom; } set { fillColorBottom = value; GenerateImage(); Invalidate(); } }

        [Description("Defines text of te button"), Category("Appearance"), EditorBrowsable(EditorBrowsableState.Always), Browsable(true),DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), Bindable(true)]
        public override string Text { get { return text; } set { text = value; GenerateImage(); Invalidate(); } }

        [Description("Defines diameter of the rounding"), Category("Appearance")]
        public int RoundingDiameter { get { return roundingDiameter; } set { roundingDiameter = value; GenerateImage(); Invalidate(); } }

        [Description("Defines image on the button"), Category("Appearance")]
        public Image Image { get { return image; } set { image = value; GenerateImage(); Invalidate(); } }

        [Description("Defines display style of image and text on the button"), Category("Appearance")]
        public ToolStripItemDisplayStyle DisplayStyle { get { return displayStyle; } set { displayStyle = value; GenerateImage(); Invalidate(); } }

        [Description("Defines which corners are rounded"), Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RoundedCornersStyle RoundedCorners { get { return roundedCorners; } set { roundedCorners = value; GenerateImage(); Invalidate(); } }


        
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public override Image BackgroundImage
        {
            get
            {
                return base.BackgroundImage;
            }
            set
            {
                base.BackgroundImage = value;
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public override ImageLayout BackgroundImageLayout
        {
            get
            {
                return base.BackgroundImageLayout;
            }
            set
            {
                base.BackgroundImageLayout = value;
            }
        }



        #endregion


        #region Constructor

        public TouchButton()
        {
            InitializeComponent();

            GenerateImage();
            text = Name;

            roundedCorners.Changed += TouchButton_Resize;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x00000020;//WS_EX_TRANSPARENT
                return cp;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Color bk = Color.Transparent;
            e.Graphics.FillRectangle(new SolidBrush(bk), e.ClipRectangle);
        }

        #endregion

        #region Private methods

        private void GenerateImage()
        {
            img = new Bitmap(Width, Height);

            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            
            g.Clear(Color.Transparent);

            Rectangle rect = new Rectangle(0, 0, img.Width-1, img.Height-1);

            Pen borderPen = new Pen((Enabled)?borderColor: SystemColors.InactiveBorder) { Alignment = PenAlignment.Center, Width = borderWidth };
            Brush fillBrush = null;
            if (Enabled)
                if (mouseDown)
                    fillBrush = new LinearGradientBrush(rect, ControlPaint.Dark(fillColorTop, 0.05f), ControlPaint.Dark(fillColorBottom, 0.05f), LinearGradientMode.Vertical);
                else
                    fillBrush = new LinearGradientBrush(rect, fillColorTop, fillColorBottom, LinearGradientMode.Vertical);
            else
                fillBrush = new SolidBrush(SystemColors.InactiveCaption);

            Brush textBrush = new SolidBrush((Enabled) ? ForeColor : SystemColors.InactiveCaptionText);

            if (borderWidth > 1)
                rect.Inflate(-(borderWidth - 1), -(borderWidth - 1));

            GraphicsPath path = RoundedRectangle(rect, roundingDiameter, roundedCorners);

            g.FillPath(fillBrush, path);
            if (borderWidth>0)
                g.DrawPath(borderPen, path);

            if (displayStyle == ToolStripItemDisplayStyle.Text || displayStyle == ToolStripItemDisplayStyle.ImageAndText)
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;

                DrawString(g, textBrush);
            }
            if (image!=null && (displayStyle==ToolStripItemDisplayStyle.Image || displayStyle==ToolStripItemDisplayStyle.ImageAndText))
            {
                Point p = new Point(img.Width/2, img.Height/2);
                p.X-= image.Width/2;
                p.Y-= image.Height/2;
                g.DrawImage(image, p.X, p.Y, image.Width, image.Height);
            }
                        
        }

        private void DrawString(Graphics g, Brush textBrush)
        {
            SizeF s = g.MeasureString(Text, Font);
            if (s.Width <= img.Width)
                g.DrawString(Text, Font, textBrush, 0 + (img.Width - s.Width) / 2.0f, 0 + (img.Height - s.Height) / 2.0f);
            else
            {
                string[] parts = Text.Split(" _".ToCharArray());
                string line = "";
                List<string> lines = new List<string>();
                Font f = Font;

                while (f.Size > 8)
                {
                    bool change = false; 
                    
                    lines.Clear();
                    line = "";
                    for (int i = 0; i < parts.Length; i++)
                    {
                        string tmp = (line + " " + parts[i]).Trim();
                        if (g.MeasureString(tmp, f).Width < img.Width || string.IsNullOrEmpty(line.Trim()))
                            line = tmp;
                        else
                        {
                            lines.Add(line);
                            line = "";
                            i--;
                        }
                    }

                    if (!string.IsNullOrEmpty(line.Trim()))
                        lines.Add(line.Trim());

                    float totalHeight = -2;
                    foreach (string l in lines)
                    {
                        if (g.MeasureString(l, f).Width >= img.Width)
                        {
                            change = true;
                            break;
                        }
                        totalHeight += g.MeasureString(l, f).Height + 2;
                    }
                    if (totalHeight > Height)
                        change = true;

                    if (change)
                    {
                        f = new Font(f.FontFamily, f.Size - 1, f.Style);
                    }
                    else
                        break;
                }
                
                int count = 0;
                float height = 0;
                foreach (string l in lines)
                {
                    if ((height + 2 + g.MeasureString(l, f).Height) < img.Height)
                    {
                        count++;
                        height += g.MeasureString(l, f).Height + 2;
                    }
                }
                height -= 2;

                height = (img.Height - height) / 2.0f;
                for (int i = 0; i < count; i++)
                {
                    s = g.MeasureString(lines[i], f);
                    g.DrawString(lines[i], f, textBrush, 0 + (img.Width - s.Width) / 2.0f, height);
                    height += s.Height + 2;
                }
            }
        }

        private GraphicsPath RoundedRectangle(Rectangle rect, int diameter, RoundedCornersStyle roundedCorners)
        {
            GraphicsPath path = new GraphicsPath();

            RectangleF arc = new RectangleF(rect.X, rect.Y, diameter, diameter);

            if (roundedCorners.TopLeft)
                path.AddArc(arc, 180, 90);
            else
                path.AddLine(arc.X, arc.Y, arc.X, arc.Y);

            arc.X = rect.Right - diameter;
            if (roundedCorners.TopRight)
                path.AddArc(arc, 270, 90);
            else
                path.AddLine(arc.X + diameter, arc.Y, arc.X + diameter, arc.Y);

            arc.Y = rect.Bottom - diameter;
            if (roundedCorners.BottomRight)
                path.AddArc(arc, 0, 90);
            else
                path.AddLine(arc.X + diameter, arc.Y+diameter, arc.X + diameter, arc.Y+diameter);

            arc.X = rect.Left;
            if (roundedCorners.BottomLeft)
                path.AddArc(arc, 90, 90);
            else
                path.AddLine(arc.X, arc.Y + diameter, arc.X, arc.Y + diameter);

            path.CloseFigure();
            
            //if (roundedCorners.IsNone)
            //{
            //    path.AddRectangle(rect);
            //}

            return path;
        }

        #endregion

        #region Events

        private void TouchButton_Paint(object sender, PaintEventArgs e)
        {
            if (img == null)
                GenerateImage();
            SmoothingMode s = e.Graphics.SmoothingMode;
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.DrawImageUnscaledAndClipped(img, e.ClipRectangle);
            e.Graphics.SmoothingMode = s;
        }

        private void TouchButton_Resize(object sender, EventArgs e)
        {
            GenerateImage();
            Invalidate();
        }

        private void TouchButton_FontChanged(object sender, EventArgs e)
        {
            GenerateImage();
            Invalidate();
        }

        private void TouchButton_EnabledChanged(object sender, EventArgs e)
        {
            GenerateImage();
            Invalidate();
        }

        private void TouchButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                mouseDown = true;
                GenerateImage();
                Invalidate();
            }
        }

        private void TouchButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseDown = false;
                GenerateImage();
                Invalidate();
            }
        }

        private void TouchButton_MouseLeave(object sender, EventArgs e)
        {
            if (mouseDown)
            {
                mouseDown = false;
                GenerateImage();
                Invalidate();
            }
        }

        public override void Refresh()
        {
            GenerateImage();
            base.Refresh();
        }

        #endregion

        [TypeConverterAttribute(typeof(System.ComponentModel.ExpandableObjectConverter))]
        public class RoundedCornersStyle
        {
            public bool topLeft = true;
            public bool topRight = true;
            public bool bottomRight = true;
            public bool bottomLeft = true;

            public void SetAll()
            {
                topLeft = topRight = bottomLeft = bottomRight = true;
            }

            public void SetNone()
            {
                topLeft = topRight = bottomLeft = bottomRight = false;
            }

            [Browsable(false)]
            public EventHandler Changed = null;

            [Browsable(false)]
            public  bool IsAll { get { return topLeft && topRight && bottomRight && bottomLeft; } }
            [Browsable(false)]
            public bool IsNone { get { return !topLeft && !topRight && !bottomRight && !bottomLeft; } }

            public bool TopLeft { get { return topLeft; } set { topLeft = value; IsChanged(); } }
            public bool TopRight { get { return topRight; } set { topRight = value; IsChanged(); } }
            public bool BottomRight { get { return bottomRight; } set { bottomRight = value; IsChanged(); } }
            public bool BottomLeft { get { return bottomLeft; } set { bottomLeft = value; IsChanged(); } }


            private void IsChanged() { if (Changed != null) Changed(this, new EventArgs()); }

            public override string ToString()
            {
                if (IsAll)
                    return "All";
                if (IsNone)
                    return "None";

                string s = "";
                if (topLeft)
                    s += ", TopLeft";
                if (topRight)
                    s += ", TopRight";
                if (bottomRight)
                    s += ", BottomRight";
                if (bottomLeft)
                    s += ", BottomLeft";

                
                return s.Substring(2);
            }
        }
    }
}
