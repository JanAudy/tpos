﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using Microdata.Util;

namespace Transware.Controls
{
    public partial class ItemListControl : UserControl
    {
        #region Private fields

        private bool trackBarcodes = true;
        private string keyBuffer = "";
        #endregion


        #region Properties

        [Description("Occurs when an item is selected inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemSelected = null;
        [Description("Occurs when an item is unselected inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemUnselected = null;
        [Description("Occurs when a double click is made on item inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemDoubleClicked = null;
        [Description("Occurs when colums are created"), Category("Behavior")]
        public event ItemListControlBarcodeApproved BarcodeApproved = null;
        
        [Description("Occurs when colums are created"), Category("Appearance")]
        public event ListControlCreateColumns CreateColumns = null;
        [Description("Occurs when an tooltip is shown over a language column"), Category("Appearance")]
        public event LanguageToolTip ItemLanguageToolTipShown = null;

        [Description("Track pressed keys and react when barcode is confirmed by enter"), Category("Behavior"), DefaultValue(true), Browsable(true)]
        public bool TrackBarcodes { get { return trackBarcodes; } set { trackBarcodes = value; } }


        [Browsable(false)]
        public ObjectListView ListView { get { return lvItems; } }

        #endregion

        #region Constructor

        public ItemListControl()
        {
            InitializeComponent();
        }

        #endregion

        public void Initialize()
        {
            lvItems.AllColumns.Clear();
            if (CreateColumns != null)
                CreateColumns();
            lvItems.RebuildColumns();
        }

        private void lvItems_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lvItems.Enabled && lvItems.SelectedObject != null)
            {
                if (ItemSelected != null)
                    ItemSelected(lvItems.SelectedObject);
            }
            else
                if (ItemUnselected != null)
                    ItemUnselected(null);
        }

        public void RefreshSelectedItem()
        {
            lvItems_ItemSelectionChanged(this, null);
        }

        public void SaveSetting(RegistryClass registry, string prefix)
        {
            for (int i = 0; i < lvItems.AllColumns.Count; i++)
            {
                registry.SetWinPARAM(prefix + i.ToString(), lvItems.AllColumns[i].Width.ToString());
            }
        }

        public void LoadSetting(RegistryClass registry, string prefix)
        {
            for (int i = 0; i < lvItems.AllColumns.Count; i++)
            {
                lvItems.AllColumns[i].Width = int.Parse(registry.GetWinPARAM(prefix + i, "90"));
            }
        }

        public void RemoveObject(object obj)
        {
            int index = -1;
            if (lvItems.SelectedObject == obj)
            {
                index = lvItems.SelectedIndex;
            }
            lvItems.RemoveObject(obj);
            if (lvItems.GetItemCount() > 0 && index >= 0)
            {
                if (index >= lvItems.GetItemCount())
                    index--;
                lvItems.SelectedIndex = index;
            }
        }

        private void lvItems_DoubleClick(object sender, EventArgs e)
        {
            if (lvItems.SelectedObject != null)
                if (ItemDoubleClicked != null)
                    ItemDoubleClicked(lvItems.SelectedObject);
        }

        private void lvItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (!trackBarcodes) return;

            if (e.KeyValue < 32 && e.KeyValue != 13 && e.KeyValue != 27)
                return;

            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
                return;

            char ch = KeyboardUtil.GetKey(e.KeyCode);

            if (e.KeyCode == Keys.Escape)
                keyBuffer = "";
            else
                if (e.KeyCode == Keys.Enter || ch == '\r' || ch == '\n')
                {
                    if (BarcodeApproved != null)
                        BarcodeApproved(keyBuffer);
                    keyBuffer = "";
                }
                else
                    keyBuffer += ch;

            lvItems.OverlayText.Text += keyBuffer;
            lvItems.RefreshOverlay(lvItems.OverlayText);
        }

        private void lvItems_ColumnRightClick(object sender, ColumnClickEventArgs e)
        {
            contextMenu.Items.Clear();

            if (lvItems.AllColumns[e.Column] is ItemListTwoStateColumn)
            {
                (lvItems.AllColumns[e.Column] as ItemListTwoStateColumn).FillContextMenu(contextMenu);
                contextMenu.Show(Cursor.Position);
            }
        }


        public ItemListTwoStateColumn PriceColumn(string state0columnName, string state1columnName, string state0Text, string state1Text)
        {
            ItemListTwoStateColumn col = new ItemListTwoStateColumn();
            col.Aspect0Name = state0columnName;
            col.Aspect1Name = state1columnName;
            col.State0Text = state0Text;
            col.State1Text = state1Text;
            col.AspectToStringConverter = delegate(object x)
            {
                decimal value = (decimal)x;
                return value.ToString("F2");
            };

            return col;
        }

       
    }

    public delegate void ItemListControlBarcodeApproved(string buffer);
    public delegate void ColumnStatusChanged(ItemListTwoStateColumn column);

    public class ItemListTwoStateColumn : OLVColumn
    {
        public string Aspect0Name { get; set; }
        public string Aspect1Name { get; set; }
        
        public string State0Text { get; set; }
        public string State1Text { get; set; }
        public int Status { get; set; }

        public event ColumnStatusChanged StatusChanged = null;

        public virtual void ChangeState()
        {
            Status = (Status + 1) % 2;
            ProcessStatusChange();
        }

        public virtual void ChangeState(int status)
        {
            Status = status;
            ProcessStatusChange();
        }

        protected virtual void ProcessStatusChange()
        {
            Text = (Status == 0) ? State0Text : State1Text;
            AspectName = (Status == 0) ? Aspect0Name : Aspect1Name;
            if (StatusChanged != null)
                StatusChanged(this);
        }

        public virtual void FillContextMenu(ContextMenuStrip menu)
        {
            menu.Items.Add(new ToolStripMenuItem(State0Text, null, (delegate(object s, EventArgs ex)
            {
                ChangeState(0);
            })));
            menu.Items.Add(new ToolStripMenuItem(State1Text, null, (delegate(object s, EventArgs ex)
            {
                ChangeState(1);
            })));
        }
    }

}
