﻿namespace Transware.Controls
{
    partial class ListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListControl));
            this.gbItems = new System.Windows.Forms.GroupBox();
            this.lvItems = new BrightIdeasSoftware.ObjectListView();
            this.flags = new System.Windows.Forms.ImageList(this.components);
            this.gbItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // gbItems
            // 
            this.gbItems.Controls.Add(this.lvItems);
            this.gbItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbItems.Location = new System.Drawing.Point(0, 0);
            this.gbItems.Name = "gbItems";
            this.gbItems.Size = new System.Drawing.Size(296, 208);
            this.gbItems.TabIndex = 3;
            this.gbItems.TabStop = false;
            // 
            // lvItems
            // 
            this.lvItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvItems.FullRowSelect = true;
            this.lvItems.HideSelection = false;
            this.lvItems.Location = new System.Drawing.Point(3, 16);
            this.lvItems.Name = "lvItems";
            this.lvItems.OwnerDraw = true;
            this.lvItems.ShowGroups = false;
            this.lvItems.Size = new System.Drawing.Size(290, 189);
            this.lvItems.TabIndex = 1;
            this.lvItems.UseCompatibleStateImageBehavior = false;
            this.lvItems.View = System.Windows.Forms.View.Details;
            this.lvItems.CellToolTipShowing += new System.EventHandler<BrightIdeasSoftware.ToolTipShowingEventArgs>(this.lvItems_CellToolTipShowing);
            this.lvItems.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvItems_ItemSelectionChanged);
            this.lvItems.DoubleClick += new System.EventHandler(this.lvItems_DoubleClick);
            // 
            // flags
            // 
            this.flags.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("flags.ImageStream")));
            this.flags.TransparentColor = System.Drawing.Color.Transparent;
            this.flags.Images.SetKeyName(0, "cz");
            this.flags.Images.SetKeyName(1, "us");
            this.flags.Images.SetKeyName(2, "gb");
            // 
            // ListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbItems);
            this.Name = "ListControl";
            this.Size = new System.Drawing.Size(296, 208);
            this.gbItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lvItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.GroupBox gbItems;
        protected BrightIdeasSoftware.ObjectListView lvItems;
        private System.Windows.Forms.ImageList flags;
    }
}
