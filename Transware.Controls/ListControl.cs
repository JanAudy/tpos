﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using Microdata.Util;
using System.Reflection;
using Transware.Data.Enums;
using Transware.Data.DataTypes;

namespace Transware.Controls
{
    public partial class ListControl : UserControl
    {
        private string title = "";
        private long LanguageColumnTag = 1234567890L;

        #region Properties

        [Description("Occurs when an item is selected inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemSelected = null;
        [Description("Occurs when an item is unselected inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemUnselected = null;
        [Description("Occurs when a double click is made on item inside ListView"), Category("Behavior")]
        public event ListControlItemSelected ItemDoubleClicked = null;
        
        [Description("Occurs when colums are created"), Category("Appearance")]
        public event ListControlCreateColumns CreateColumns = null;
        [Description("Occurs when an tooltip is shown over a language column"), Category("Appearance")]
        public event LanguageToolTip ItemLanguageToolTipShown = null;

        [Description("Title of the list"), Category("Appearance"), DefaultValue(""), Browsable(true)]
        public string Title { get { return title; } set { title = value; gbItems.Text = title; } }

        
        [Browsable(false)]
        public ObjectListView ListView { get { return lvItems; } }

        #endregion

        public ListControl()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
            lvItems.AllColumns.Clear();
            if (CreateColumns != null)
                CreateColumns();
            lvItems.RebuildColumns();
        }

        private void lvItems_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lvItems.Enabled && lvItems.SelectedObject != null)
            {
                if (ItemSelected != null)
                    ItemSelected(lvItems.SelectedObject);
            }
            else
                if (ItemUnselected != null)
                    ItemUnselected(null);                
        }

        public void RefreshSelectedItem()
        {
            lvItems_ItemSelectionChanged(this, null);
        }

        public void SaveSetting(RegistryClass registry, string prefix)
        {
            for (int i = 0; i < lvItems.AllColumns.Count; i++)
            {
                registry.SetWinPARAM(prefix + i.ToString(), lvItems.AllColumns[i].Width.ToString());
            }
        }

        public void LoadSetting(RegistryClass registry, string prefix)
        {
            for (int i = 0; i < lvItems.AllColumns.Count; i++)
            {
                lvItems.AllColumns[i].Width = int.Parse(registry.GetWinPARAM(prefix + i, "90"));
            }
        }

        public void RemoveObject(object obj)
        {
            int index = -1;
            if (lvItems.SelectedObject == obj)
            {
                index = lvItems.SelectedIndex;
            }
            lvItems.RemoveObject(obj);
            if (lvItems.GetItemCount()>0 && index >= 0)
            {
                if (index >= lvItems.GetItemCount())
                    index--;
                lvItems.SelectedIndex = index;
            }
        }

        private void lvItems_DoubleClick(object sender, EventArgs e)
        {
            if (lvItems.SelectedObject != null)
                if (ItemDoubleClicked != null)
                    ItemDoubleClicked(lvItems.SelectedObject);
        }


        #region Public methods

        public OLVColumn LanguageColumn<T>()
        {
            OLVColumn col = new OLVColumn();
            col.Text = "Překlad";
            col.Renderer = new ImageRenderer();
            col.AspectGetter = delegate(object x)
            {
                PropertyInfo prop = x.GetType().GetProperty("Languages");
                List<Image> images = new List<Image>();
                
                if (prop!=null)
                {
                    Dictionary<ELanguage, T> languages = (prop.GetValue(x, null) as Dictionary<ELanguage, T>);
                    
                    foreach (ELanguage l in Enum.GetValues(typeof(ELanguage)))
                    {
                        if (l == ELanguage.Czech) continue;
                        if (languages.ContainsKey(l))
                            images.Add(flags.Images["gb"]);
                    }
                }
                return images.ToArray();
            };
            col.Tag = LanguageColumnTag;
            return col;

        }
        #endregion

        private void lvItems_CellToolTipShowing(object sender, ToolTipShowingEventArgs e)
        {
            if (e.Column.Tag!=null && (long)e.Column.Tag == LanguageColumnTag)
            {
                if (ItemLanguageToolTipShown != null)
                    e.Text = ItemLanguageToolTipShown(e.Model);
                e.Handled = true;
            }            
        }

    }

    public delegate void ListControlItemSelected(object selectedObject);
    public delegate void ListControlCreateColumns();
    public delegate string LanguageToolTip(object obj);
}
