﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace Microdata.Data
{
    public abstract class AbstractDB<T> : IDisposable where T:new()
    {
        #region Singleton

        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }

        #endregion


        #region IDisposable Members

        public void Dispose()
        {
            if (connection == null)
            {
                connection.StateChange -= ConnectionStateChanged;
                connection.Close();
            }
        }

        #endregion

        #region Database access

        public abstract string DB_Name { get; }

        public string DB_UserName { get; set; }
        public string DB_Password { get; set; }
        public string DB_Server { get; set; }

        #endregion

        private DbProviderFactory provider = DbProviderFactories.GetFactory("System.Data.SqlClient");
        private SqlConnection connection;

        public SqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new SqlConnection();
                    connection.ConnectionString = "Server=" + DB_Server + ";uid=" + DB_UserName + ";pwd=" + DB_Password + ";database=" + DB_Name;
                    connection.StateChange += ConnectionStateChanged;
                    connection.Open();
                }

                return connection;
            }
        }

        public void ConnectionStateChanged(object sneder, StateChangeEventArgs args)
        {
            if (args.OriginalState == ConnectionState.Open && args.CurrentState == ConnectionState.Closed)
            {
                Connection.Open();
            }
        }


        
    }
}
