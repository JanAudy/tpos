﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microdata.Data.DataTypes;

namespace Microdata.Data.DataAccess
{
    public abstract class AbstractIDDao<T, Tid> where T : AbstractIDDto<Tid>, new()
    {
        protected virtual string IDColumnName { get { return "ID"; } }

        protected SqlConnection connection = null;
        protected string tableName = "";

        protected AbstractIDDao(SqlConnection connection, string tableName)
        {
            this.connection = connection;
            this.tableName = tableName;
        }
                

        #region Get methods

        public T Get(List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandTimeout = 600;
                comm.CommandText = "SELECT * FROM " + tableName;
                if (constraints != null && constraints.Count > 0)
                    comm.CommandText += " WHERE " + ConstraintsToString(constraints, comm.Parameters);

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                T result = null;
                if (reader.Read())
                {
                    result = Interprete(reader, cols);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        protected string ConstraintsToString(List<Constraint> constraints, SqlParameterCollection param)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Constraint c in constraints)
            {
                if (c is CompositeConstraint)
                {
                    CompositeConstraint cc = c as CompositeConstraint;
                    if (cc.Operation == ConstraintOperation.Not)
                    {
                        sb.Append("NOT(");
                        sb.Append(ConstraintsToString(cc.Constraints, param));
                        sb.Append(")");
                    }
                    if (cc.Operation == ConstraintOperation.Or)
                    {
                        sb.Append("(");
                        foreach (Constraint ic in cc.Constraints)
                        {
                            List<Constraint> cl = new List<Constraint>();
                            cl.Add(ic);
                            sb.Append(ConstraintsToString(cl, param));
                            sb.Append(" OR ");
                        }
                        sb = sb.Remove(sb.Length - 4, 4);
                        sb.Append(")");
                    }
                }
                else
                {
                    if (c.ParamName.Contains('.'))
                        sb.Append(c.ParamName);
                    else
                        sb.Append("[" + c.ParamName + "]");

                    switch (c.Operation)
                    {
                        case ConstraintOperation.Equal: sb.Append("="); break;
                        case ConstraintOperation.NotEqual: sb.Append("!="); break;
                        case ConstraintOperation.Less: sb.Append("<"); break;
                        case ConstraintOperation.LessOrEqual: sb.Append("<="); break;
                        case ConstraintOperation.Greater: sb.Append(">"); break;
                        case ConstraintOperation.GreaterOrEqual: sb.Append(">="); break;
                        case ConstraintOperation.Like: sb.Append(" LIKE "); break;
                        case ConstraintOperation.IsNull: sb.Append(" IS NULL "); break;
                        case ConstraintOperation.IsNotNull: sb.Append(" IS NOT NULL "); break;
                    }
                    if (c.Operation != ConstraintOperation.IsNull && c.Operation != ConstraintOperation.IsNotNull && !(c is InnerConstraint))
                    {
                        sb.Append(GetConstraintParamName(c.ParamName));

                        param.Add(new SqlParameter(GetConstraintParamName(c.ParamName), c.Value));
                    }
                    if (c is InnerConstraint)
                        sb.Append(c.Value.ToString());
                }
                sb.Append(" AND ");                
            }
            return sb.Remove(sb.Length - 5, 5).ToString();
        }

        private string GetConstraintParamName(string name)
        {
            string pn = "@" + name.Replace('.', '_');
            return pn;
        }

        public virtual List<T> GetList()
        {
            return GetList(null);
        }

        public virtual List<T> GetList(List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;
                if (constraints != null && constraints.Count > 0)
                    comm.CommandText += " WHERE " + ConstraintsToString(constraints, comm.Parameters);

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<T> result = new List<T>();
                while (reader.Read())
                {
                    result.Add(Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<T>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public virtual List<Tid> GetAllId() { return GetAllId(null); }

        public abstract List<Tid> GetAllId(List<Constraint> constraints);
        

        #endregion

        #region Save and Update

        protected abstract bool GetID(T obj);

        public abstract void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert);

        public abstract bool SaveOrUpdate(T obj);

        public virtual bool Save(T obj)
        {
            SqlCommand comm = null;
            try
            {
                Dictionary<string, SqlParameter> parameters = new Dictionary<string, SqlParameter>();

                // call abstract class, which must be override in all child
                FillParams(obj, parameters, true);

                string[] paramNames = parameters.Keys.ToArray();

                comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO " + tableName + " (";

                for (int i = 0; i < paramNames.Length; i++)
                {
                    comm.CommandText += paramNames[i];
                    if (i < paramNames.Length - 1)
                        comm.CommandText += ",";
                }
                comm.CommandText += ") Values (";

                for (int i = 0; i < paramNames.Length; i++)
                {
                    comm.CommandText += parameters[paramNames[i]].ParameterName;
                    if (i < paramNames.Length - 1)
                        comm.CommandText += ",";
                    comm.Parameters.Add(parameters[paramNames[i]]);
                }

                comm.CommandText += ")";

                bool result = comm.ExecuteNonQuery() == 1;

                bool hasID = GetID(obj);

                comm.Dispose();
                comm = null;

                return result && hasID;
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
                return false;
            }
            finally
            {
                if (comm != null)
                {
                    comm.Dispose();
                    comm = null;
                }
            }
        }

        public virtual bool Update(T obj)
        {
            SqlCommand comm = null;
            try
            {
                Dictionary<string, SqlParameter> parameters = new Dictionary<string, SqlParameter>();

                // call abstract class, which must be override in all child
                FillParams(obj, parameters, false);

                string[] paramNames = parameters.Keys.ToArray();

                comm = connection.CreateCommand();
                comm.CommandText = "UPDATE " + tableName + " SET ";

                for (int i = 0; i < paramNames.Length; i++)
                {
                    if (paramNames[i].ToUpper() != IDColumnName.ToUpper())
                    {
                        comm.CommandText += paramNames[i];
                        comm.CommandText += "=";
                        comm.CommandText += parameters[paramNames[i]].ParameterName;
                        if (i < paramNames.Length - 1)
                            comm.CommandText += ",";
                    }
                    comm.Parameters.Add(parameters[paramNames[i]]);
                }
                comm.CommandText += " WHERE " + IDColumnName + "=" + parameters[IDColumnName].ParameterName;

                bool result = comm.ExecuteNonQuery() == 1;

                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
                return false;
            }
            finally
            {
                if (comm != null)
                {
                    comm.Dispose();
                    comm = null;
                }
            }
        }

        #endregion

        #region Abstract interpretation methods

        public T Interprete(SqlDataReader reader) { return Interprete(reader, new PresentColumnsCollection(reader)); }

        public abstract void Interprete(SqlDataReader reader, PresentColumnsCollection cols, ref T obj);
        
        public virtual T Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            T obj = new T();
            Interprete(reader, cols, ref obj);
            return obj;
        }       

        #endregion

        #region Safe methods

        protected object SafeString(string value)
        {
            if (value == null)
                return SqlString.Null;
            return value;
        }

        protected object SafeDateTime(DateTime value)
        {
            if (value == null || value < AbstractIDDto<T>.ZeroDateTime)
                return SqlDateTime.Null;
            return value;
        }

        protected object SafeDateTime(DateTime? value)
        {
            if (value == null || !value.HasValue || value.Value < AbstractIDDto<T>.ZeroDateTime)
                return SqlDateTime.Null;
            return value.Value;
        }

        protected object SafeByte(byte[] value)
        {
            if (value == null)
                return SqlBinary.Null;
            return value;
        }

        protected object SafeDecimal(decimal? value)
        {
            if (value == null)
                return SqlDecimal.Null;
            return value;
        }

        protected object SafeLong(long? value)
        {
            if (value == null)
                return SqlInt64.Null;
            return value;
        }

        #endregion


        #region Intepretation methods for data types

        protected decimal Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, decimal def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (decimal)reader[name];
            return def;
        }

        protected decimal? Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, decimal? def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (decimal?)reader[name];
            return def;
        }

        protected long Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, long def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (long)reader[name];
            return def;
        }

        protected long? Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, long? def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (long)reader[name];
            return def;
        }

        protected int Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, int def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (int)reader[name];
            return def;
        }

        protected string Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, string def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (string)reader[name];
            return def;
        }

        protected DateTime Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, DateTime def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (DateTime)reader[name];
            return def;
        }

        protected char Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, char def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return ((string)reader[name])[0];
            return def;
        }

        protected short Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, short def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (short)reader[name];
            return def;
        }

        protected bool Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, bool def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (bool)reader[name];
            return def;
        }

        protected byte[] Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, byte[] def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (byte[])reader[name];
            return def;
        }

        protected double Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, double def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (double)reader[name];
            return def;
        }

        protected float Interprete(SqlDataReader reader, PresentColumnsCollection cols, string name, float def)
        {
            if (cols.Contains(name) && !(reader[name] is DBNull))
                return (float)(double)(reader[name]);
            return def;
        }

        #endregion

        #region Adding Params

        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, decimal value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, string value)
        {
            parameters.Add(name, new SqlParameter("@" + name, SafeString(value)));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, short value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, bool value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, long value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, char value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, byte[] value)
        {
            parameters.Add(name, new SqlParameter("@" + name, SafeByte(value)));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, DateTime value)
        {
            parameters.Add(name, new SqlParameter("@" + name, SafeDateTime(value)));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, double value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, float value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, int value)
        {
            parameters.Add(name, new SqlParameter("@" + name, value));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, long? value)
        {
            parameters.Add("["+name+"]", new SqlParameter("@" + name, SafeLong(value)));
        }
        protected void AddParam(Dictionary<string, SqlParameter> parameters, string name, decimal? value)
        {
            parameters.Add(name, new SqlParameter("@" + name, SafeDecimal(value)));
        }

        #endregion
    }
}
