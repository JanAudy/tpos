﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataAccess
{
    public class InnerConstraint : Constraint
    {
        public InnerConstraint(string param, ConstraintOperation operation, string value) : base(param, operation, value) { }
        
    }
}
