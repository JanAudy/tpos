﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using System.Data.SqlClient;

namespace Microdata.Data.DataAccess
{
    public class AbstractValidDao<T> : AbstractIDLongDao<T> where T:AbstractValidDto, new()
    {
        public AbstractValidDao(SqlConnection connection, string tableName) : base(connection, tableName) { }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            base.FillParams(obj, parameters, insert);
            AddParam(parameters, "Valid", obj.Valid);
        }

        public override void Interprete(SqlDataReader reader, PresentColumnsCollection cols, ref T obj)
        {
            base.Interprete(reader, cols, ref obj);

            obj.Valid = Interprete(reader, cols, "Valid", obj.Valid);
        }

        public override List<T> GetList(List<Constraint> constraints)
        {
            if (constraints == null)
                constraints = new List<Constraint>();
            Constraint c = new Constraint("Valid", ConstraintOperation.Equal, true);
            if (!constraints.Contains(c))
                constraints.Add(c);
            
            return base.GetList(constraints);
        }

        public override List<long> GetAllId(List<Constraint> constraints)
        {
            if (constraints == null)
                constraints = new List<Constraint>();
            Constraint c = new Constraint("Valid", ConstraintOperation.Equal, true);
            if (!constraints.Contains(c))
                constraints.Add(c);

            return base.GetAllId(constraints);
        }

        public virtual bool UpdateValid(T obj)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "UPDATE " + tableName + " SET Valid=@valid WHERE ID=@id";

                comm.Parameters.Add(new SqlParameter("valid", obj.Valid));
                comm.Parameters.Add(new SqlParameter("id", obj.ID));

                int rows = comm.ExecuteNonQuery();

                comm.Dispose();
                comm = null;

                return rows == 1;
            }
            catch (Exception e)
            {
                string text = e.ToString();
                return false;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
