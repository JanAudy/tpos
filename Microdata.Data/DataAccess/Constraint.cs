﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataAccess
{
    public enum ConstraintOperation { Equal, NotEqual, Less, Greater, LessOrEqual, GreaterOrEqual, IsNull, IsNotNull, Like, Or, Not };
    public class Constraint
    {
        public string ParamName {get;set;}
        public ConstraintOperation Operation { get; set; }
        public object Value { get; set; }


        public Constraint(string param, ConstraintOperation operation, object value)
        {
            ParamName = param;
            Operation = operation;
            Value = value;
        }

        public Constraint(string param, ConstraintOperation operation) : this(param, operation, null) { }

        public override bool Equals(object obj)
        {
            if (obj is Constraint)
                return ParamName == (obj as Constraint).ParamName;
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
