﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataAccess
{
    public class CompositeConstraint : Constraint
    {
        public List<Constraint> Constraints = new List<Constraint>();

        public CompositeConstraint(ConstraintOperation operation) : base("", operation)
        {
            
        }
    }
}
