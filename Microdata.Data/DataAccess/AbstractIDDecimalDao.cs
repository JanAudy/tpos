﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using System.Data.SqlClient;

namespace Microdata.Data.DataAccess
{
    public class AbstractIDDecimalDao<T> : AbstractIDDao<T, decimal> where T:AbstractIDDecimalDto, new()
    {
        public AbstractIDDecimalDao(SqlConnection connection, string tableName) : base(connection, tableName) { }


        protected override bool GetID(T obj)
        {
            SqlCommand comm = connection.CreateCommand();
            try
            {
                comm.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                obj.ID = Convert.ToDecimal(comm.ExecuteScalar());

                obj.AssignIDToChildren();

                return obj.ID != 0;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                comm.Dispose();
            }
        }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            if (!insert)
                AddParam(parameters, IDColumnName, obj.ID);
        }

        public override void Interprete(SqlDataReader reader, PresentColumnsCollection cols, ref T obj)
        {
            obj.ID = Interprete(reader, cols, IDColumnName, obj.ID);
        }

        public override bool SaveOrUpdate(T obj)
        {
            bool res = false;
            if (obj.ID == 0)
                res = Save(obj);
            else
                res = Update(obj);

            return res;
        }

        public virtual T Get(decimal id)
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint(IDColumnName, ConstraintOperation.Equal, id));
            return Get(cons);
        }

        public override List<decimal> GetAllId(List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT "+IDColumnName+" FROM " + tableName;
                if (constraints != null && constraints.Count > 0)
                    comm.CommandText += " WHERE " + ConstraintsToString(constraints, comm.Parameters);

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<decimal> result = new List<decimal>();
                while (reader.Read())
                {
                    result.Add((decimal)reader[0]);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<decimal>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
