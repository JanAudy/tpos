﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using System.Data.SqlClient;

namespace Microdata.Data.DataAccess
{
    public class AbstractIDLongDao<T> : AbstractIDDao<T, long> where T:AbstractIDLongDto, new()
    {
        public AbstractIDLongDao(SqlConnection connection, string tableName) : base(connection, tableName) { }


        protected override bool GetID(T obj)
        {
            SqlCommand comm = connection.CreateCommand();
            try
            {
                comm.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                obj.ID = Convert.ToInt64(comm.ExecuteScalar());

                obj.AssignIDToChildren();

                return obj.ID != 0;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                comm.Dispose();
            }
        }

        public override void FillParams(T obj, Dictionary<string, SqlParameter> parameters, bool insert)
        {
            if (!insert)
                AddParam(parameters, IDColumnName, obj.ID);
        }

        public override void Interprete(SqlDataReader reader, PresentColumnsCollection cols, ref T obj)
        {
            obj.ID = Interprete(reader, cols, IDColumnName, obj.ID);
        }

        public override bool SaveOrUpdate(T obj)
        {
            if (obj.ID == 0)
                return Save(obj);
            else
                return Update(obj);
        }

        public virtual T Get(long id)
        {
            List<Constraint> cons = new List<Constraint>();
            cons.Add(new Constraint(IDColumnName, ConstraintOperation.Equal, id));
            return Get(cons);
        }
        
        public override List<long> GetAllId(List<Constraint> constraints)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT "+IDColumnName+" FROM " + tableName;
                if (constraints != null && constraints.Count > 0)
                    comm.CommandText += " WHERE " + ConstraintsToString(constraints, comm.Parameters);

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<long> result = new List<long>();
                while (reader.Read())
                {
                    result.Add((long)reader[0]);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<long>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
