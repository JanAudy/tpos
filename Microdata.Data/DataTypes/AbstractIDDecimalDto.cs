﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataTypes
{
    public abstract class AbstractIDDecimalDto : AbstractIDDto<decimal>
    {
        public AbstractIDDecimalDto()
        {
            ID = 0;
        }

        public override int GetHashCode()
        {
            return (GetType().ToString() + ID.ToString()).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is AbstractIDLongDto)
            {
                AbstractIDLongDto g = (AbstractIDLongDto)obj;
                return ID == g.ID;
            }
            return base.Equals(obj);
        }

    }
}
