﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataTypes
{
    public abstract class AbstractValidDto : AbstractIDLongDto
    {
        public bool Valid { get; set; }

        public AbstractValidDto()
        {
            Valid = true;
        }
    }
}
