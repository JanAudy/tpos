﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataTypes
{
    public delegate void LazyLoadListDecimal<T>(object sender, decimal id, ref List<T> items);
    public delegate void LazyLoadObjectDecimal<T>(object sender, decimal id, ref T item);
    public delegate void LazyLoadListLong<T>(object sender, long id, ref List<T> items);
    public delegate void LazyLoadObjectLong<T>(object sender, long id, ref T item);
    public delegate void LazyLoadDictionary<Tkey, Tvalue>(object sender, long id, ref Dictionary<Tkey, Tvalue> items);
    public delegate void LazyLoadHashSet<Tkey>(object sender, long id, ref HashSet<Tkey> items);
 
    public abstract class AbstractIDDto<T>
    {
        public T ID { get; internal protected set; }

        public AbstractIDDto()
        {
            
        }

        public virtual void AssignIDToChildren() { }

        public static DateTime ZeroDateTime = new DateTime(1900, 1, 1);
    }
}
