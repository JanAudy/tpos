﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Microdata.Data.DataModel
{
    public abstract class AbstractLongModel<T> : AbstractModel<T,long> where T:AbstractIDLongDto
    {
        
    }
}
