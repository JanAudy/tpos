﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;
using Microdata.Data.DataAccess;

namespace Microdata.Data.DataModel
{
    // T is decimal/long U is object
    public abstract class AbstractModel<Tobj, Tid> where Tobj:AbstractIDDto<Tid>
    {
        public abstract Tobj Create();

        public abstract bool SaveOrUpdate(Tobj obj);

        public abstract Tobj Load(Tid id);

        public virtual Tobj Get(Tid id)
        {
            return Load(id);
        }

        public virtual List<Tobj> GetList() { return GetList(null); }

        public abstract List<Tobj> GetList(List<Constraint> constraints);

    }
}
