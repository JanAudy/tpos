﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Microdata.Data.DataModel
{
    public class Cache<Tobj, Tid> where Tobj:AbstractIDDto<Tid>
    {
        private Dictionary<Tid, Tobj> cache = new Dictionary<Tid, Tobj>();
        
        public bool Contains(Tid id)
        {
            return cache.ContainsKey(id);
        }

        public void Add(Tobj obj)
        {
            cache.Add(obj.ID, obj);
        }

        public Tobj Get(Tid id)
        {
            if (!cache.ContainsKey(id))
                return null;
            return cache[id];
        }
    }
}
