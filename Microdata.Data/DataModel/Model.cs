﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microdata.Data.DataModel
{
    public class Model
    {
        #region Singleton

        private static Model instance;

        public static Model Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Model();
                }
                return instance;
            }
        }

        #endregion

        private Dictionary<Type, object> models = new Dictionary<Type, object>();
    }
}
