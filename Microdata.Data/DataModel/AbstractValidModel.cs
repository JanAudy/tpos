﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microdata.Data.DataTypes;

namespace Microdata.Data.DataModel
{
    public abstract class AbstractValidModel<T> : AbstractLongModel<T> where T:AbstractValidDto
    {
        public abstract bool UpdateValid(T obj);
    }
}
