﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerSys.Data
{
    public class Transaction : AbstractDataType
    {
        public enum ERecordType { None, Bill, Invoice };
        
        public long Card { get; set; }
        public long Person { get; set; }
        public long Department { get; set; }
        public long Organization { get; set; }
        public DateTime DateTime { get; set; }
        public ERecordType RecordType { get; set; }
        public long RecordID { get; set; }
        public long RecordNumber { get; set; }
        public string RecordNumberStr { get; set; }
        public decimal Amount { get; set; }
        public decimal Discount { get; set; }
        public decimal AmountWithDiscount { get; set; }
        public long Points { get; set; }
        public decimal Certificate { get; set; }
        public long Gift { get; set; }

        public Transaction()
            : base()
        {
            DateTime = new DateTime(1900, 1, 1); ;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is Gift)
            {
                Transaction g = (Transaction)obj;
                return base.Equals(obj);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
