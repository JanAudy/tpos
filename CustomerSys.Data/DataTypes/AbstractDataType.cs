﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerSys.Data
{
    public abstract class AbstractDataType
    {
        public long ID { get; internal set; }
        public bool Valid { get; set; }

        public AbstractDataType()
        {
            ID = 0;
            Valid = true;
        }

        public override int GetHashCode()
        {
            return (GetType().ToString() + ID.ToString()).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is AbstractDataType)
            {
                AbstractDataType g = (AbstractDataType)obj;
                return ID == g.ID && Valid == g.Valid;
            }
            return base.Equals(obj);
        }

        public static DateTime ZeroDateTime = new DateTime(1900, 1, 1);
    }
}
