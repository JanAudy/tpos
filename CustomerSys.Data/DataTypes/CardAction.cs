﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerSys.Data
{
    public class CardAction : AbstractDataType
    {
        public enum ArrivalType {  Every, Birthday, Nameday, Nth, Minimal, EveryNth};
        public enum PeriodType { Anytime, InDates, Until, AtDay };
        public enum ExpenseType { None, Actual, TotalPerCard, TotalPerOwner };
        public enum PointsType { None, PerCard, PerOwner };
        public enum RewardType { Value, Points }
        public enum RewardValueType { None, Discount, Gift, Certificate };
        public enum RewardPointType { None, OnePointPer, Points }

        public long CardType { get; set; }
        public long Sale { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public ArrivalType Arrival { get; set; }
        public decimal ArrivalData { get; set; }
        public PeriodType Period { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public decimal PeriodData { get; set; }
        public ExpenseType Expense { get; set; }
        public decimal ExpenseFrom { get; set; }
        public decimal? ExpenseTo { get; set; }
        public DateTime? ExpensePeriodFrom { get; set; }
        public DateTime? ExpensePeriodTo { get; set; }
        public PointsType Points { get; set; }
        public int PointsData { get; set; }
        public RewardType Reward { get; set; }
        public RewardValueType RewardValue { get; set; }
        public decimal RewardValueData { get; set; }
        public int RewardValueDataParam { get; set; }
        public RewardPointType RewardPoint { get; set; }
        public decimal RewardPointData { get; set; }

        public CardAction() : base()
        {
            DateTime = DateTime.Now;
            PeriodFrom = PeriodTo = ZeroDateTime;
            ValidFrom = new DateTime(1900, 1, 1);
            ValidTo = new DateTime(2100, 12, 31);
        }

        public CardAction(long sale) : this()
        {
            Sale = sale;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is CardAction)
            {
                CardAction g = (CardAction)obj;
                return base.Equals(obj);// && Name == g.Name && Description == g.Description && Image == g.Image;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
