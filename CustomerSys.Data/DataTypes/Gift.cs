﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace CustomerSys.Data
{
    public class Gift : AbstractDataType
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Image Image { get; set; }
       
        public Gift() : base()
        {
            
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is Gift)
            {
                Gift g = (Gift)obj;
                return base.Equals(obj) && Name == g.Name && Description == g.Description && Image == g.Image;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
