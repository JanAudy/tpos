﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerSys.Data
{
    public class Sale : AbstractDataType
    {
        public string Name { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        public Sale()
        {
            ValidFrom = null;
            ValidTo = null;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj is Sale)
            {
                Sale g = (Sale)obj;
                return base.Equals(obj) && Name == g.Name && ValidFrom == g.ValidFrom && ValidTo == g.ValidTo;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
