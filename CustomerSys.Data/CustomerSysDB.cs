﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace CustomerSys.Data
{
    public class CustomerSysDB : IDisposable
    {
        #region IDisposable Members

        public void Dispose()
        {
            if (connection == null)
            {
                connection.StateChange -= ConnectionStateChanged;
                connection.Close();
            }
        }

        #endregion

        #region Singleton

        private static CustomerSysDB instance;

        public static CustomerSysDB Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CustomerSysDB();
                }
                return instance;
            }
        }

        #endregion

        #region Database access

        public string DB_Name { get { return "MDATA_CUSTOMERSYS"; } }

        public string DB_UserName { get; set; }
        public string DB_Password { get; set; }
        public string DB_Server { get; set; }

        #endregion

        private DbProviderFactory provider = DbProviderFactories.GetFactory("System.Data.SqlClient");
        private SqlConnection connection;

        public SqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new SqlConnection();
                    connection.ConnectionString = "Server=" + DB_Server + ";uid=" + DB_UserName + ";pwd=" + DB_Password + ";database=" + DB_Name;
                    connection.StateChange += ConnectionStateChanged;
                    connection.Open();
                }
                return connection;
            }
        }

        public void ConnectionStateChanged(object sneder, StateChangeEventArgs args)
        {
            if (args.OriginalState == ConnectionState.Open && args.CurrentState == ConnectionState.Closed)
            {
                Connection.Open();
            }
        }


        private GiftDao giftDao = null;
        public GiftDao GiftDao
        {
            get
            {
                if (giftDao == null)
                    giftDao = new GiftDao(Connection);
                return giftDao;
            }
        }

        private CardActionDao cardActionDao = null;
        public CardActionDao CardActionDao
        {
            get
            {
                if (cardActionDao == null)
                    cardActionDao = new CardActionDao(Connection);
                return cardActionDao;
            }
        }

        private TransactionDao transactionDao = null;
        public TransactionDao TransactionDao
        {
            get
            {
                if (transactionDao == null)
                    transactionDao = new TransactionDao(Connection);
                return transactionDao;
            }
        }

        private SaleDao saleDao = null;
        public SaleDao SaleDao
        {
            get
            {
                if (saleDao == null)
                    saleDao = new SaleDao(Connection);
                return saleDao;
            }
        }

    }
}
