﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerSys.Data
{
    public class Evaluator
    {
        public struct CustomerDiscountType
        {
            public decimal Discount;
            public bool OnlyNonDiscounted;
        }

        #region Singleton

        private static Evaluator instance = null;
        public static Evaluator Instance
        {
            get
            {
                if (instance == null)
                    instance = new Evaluator();
                return instance;
            }
        }

        #endregion

        public CustomerDiscountType GetDiscount(long cardId, long cardType, long person, long organization, DateTime dateTime, decimal actualAmount)
        {
            List<CardAction> actions = CustomerSysDB.Instance.CardActionDao.GetByCardType(cardType, dateTime);

            CustomerDiscountType discount = new CustomerDiscountType();
            discount.Discount = 0;
            discount.OnlyNonDiscounted = false;

            foreach (CardAction ca in actions)
            {
                if (ca.Reward != CardAction.RewardType.Value) continue;
                if (ca.RewardValue != CardAction.RewardValueType.Discount) continue;

                if (ca.Arrival!=CardAction.ArrivalType.Every)
                {
                    if (ca.Arrival == CardAction.ArrivalType.Nth || ca.Arrival==CardAction.ArrivalType.Minimal || ca.Arrival==CardAction.ArrivalType.EveryNth)
                    {
                        int arrivalCount = CustomerSysDB.Instance.TransactionDao.GetCountByCard(cardId);
                    }
                }

                if (ca.Period != CardAction.PeriodType.Anytime)
                {
                    if (ca.Period == CardAction.PeriodType.AtDay)
                    {
                        int day = (int)ca.PeriodData;
                        switch (dateTime.DayOfWeek)
                        {
                            case DayOfWeek.Monday: if (day == 0) continue; break;
                            case DayOfWeek.Tuesday: if (day == 1) continue; break;
                            case DayOfWeek.Wednesday: if (day == 2) continue; break;
                            case DayOfWeek.Thursday: if (day == 3) continue; break;
                            case DayOfWeek.Friday: if (day == 4) continue; break;
                            case DayOfWeek.Saturday: if (day == 5) continue; break;
                            case DayOfWeek.Sunday: if (day == 6) continue; break;
                        }
                    }
                    if (ca.Period == CardAction.PeriodType.InDates)
                    {
                        if (dateTime.Date < ca.PeriodFrom || dateTime.Date > ca.PeriodTo)
                            continue;
                    }
                }

                if (ca.Expense != CardAction.ExpenseType.None)
                {
                    if (ca.Expense == CardAction.ExpenseType.Actual)
                    {
                        if (actualAmount < ca.ExpenseFrom)
                            continue;
                        if (ca.ExpenseTo.HasValue && actualAmount > ca.ExpenseTo.Value)
                            continue;
                    }
                    if (ca.Expense == CardAction.ExpenseType.TotalPerCard || ca.Expense==CardAction.ExpenseType.TotalPerOwner)
                    {
                        decimal totalAmount = 0;
                        if (ca.ExpensePeriodFrom.HasValue && ca.ExpensePeriodTo.HasValue)
                            totalAmount = CustomerSysDB.Instance.TransactionDao.GetTotalExpensePerCard(cardId, ca.ExpensePeriodFrom.Value, ca.ExpensePeriodTo.Value);
                        else
                            totalAmount = CustomerSysDB.Instance.TransactionDao.GetTotalExpensePerCard(cardId);
                        
                        if (totalAmount < ca.ExpenseFrom)
                            continue;
                        if (ca.ExpenseTo.HasValue && totalAmount > ca.ExpenseTo.Value)
                            continue;
                    }
                }

                //// points
                //if (ca.Points != CardAction.PointsType.None)
                //{
                //    if (ca.Points == CardAction.PointsType.PerCard || ca.Points == CardAction.PointsType.PerOwner)
                //    {
                //        decimal totalAmount = CustomerSysDB.Instance.TransactionDao.GetTotalExpensePerCard(cardId);
                //        if (totalAmount < ca.ExpenseFrom)
                //            continue;
                //        if (ca.ExpenseTo.HasValue && totalAmount > ca.ExpenseTo.Value)
                //            continue;
                //    }
                //}

                if (discount.Discount < ca.RewardValueData)
                {
                    discount.Discount = ca.RewardValueData;
                    discount.OnlyNonDiscounted = ca.RewardValueDataParam == 1;
                }
            }
            return discount;
        }
    }
}
