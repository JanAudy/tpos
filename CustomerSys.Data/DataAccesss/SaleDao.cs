﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace CustomerSys.Data
{
    public class SaleDao : AbstractDao<Sale>
    {
        public SaleDao(SqlConnection connection)
            : base(connection, "Sales")
        {
            
        }

        public override Sale Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            Sale g = new Sale();
            if (cols.Contains("ID"))
                g.ID = (long)reader["ID"];
            if (cols.Contains("Name") && !(reader["Name"] is DBNull))
                g.Name = (string)reader["Name"];
            
            if (cols.Contains("ValidFrom"))
                if (reader["ValidFrom"] is DBNull)
                    g.ValidFrom = null;
                else
                    g.ValidFrom = (DateTime?)reader["ValidFrom"];
            if (cols.Contains("ValidTo"))
                if (reader["ValidTo"] is DBNull)
                    g.ValidTo = null;
                else
                    g.ValidTo = (DateTime?)reader["ValidTo"];

            if (cols.Contains("Valid"))
                g.Valid = (bool)reader["Valid"];

            return g;
        }

        public override bool Save(Sale obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO [MDATA_CUSTOMERSYS].[dbo].[Sales] ([Name],[ValidFrom],[ValidTo],[Valid]) " +
                    "Values (@Name, @ValidFrom, @ValidTo, @Valid)";

                comm.Parameters.Add(new SqlParameter("@Name", SafeString(obj.Name)));
                comm.Parameters.Add(new SqlParameter("@ValidFrom", SafeDateTime(obj.ValidFrom)));
                comm.Parameters.Add(new SqlParameter("@ValidTo", SafeDateTime(obj.ValidTo)));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                obj.ID = GetID();

                comm.Dispose();

                return result && obj.ID != 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override bool Update(Sale obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "UPDATE [MDATA_CUSTOMERSYS].[dbo].[Sales] SET [Name]=@Name, [ValidFrom] = @ValidFrom, [ValidTo] = @ValidTo, [Valid] = @Valid " +
                    "WHERE ID = @Id";

                comm.Parameters.Add(new SqlParameter("@Id", obj.ID));
                comm.Parameters.Add(new SqlParameter("@Name", obj.Name));
                comm.Parameters.Add(new SqlParameter("@ValidFrom", SafeDateTime(obj.ValidFrom)));
                comm.Parameters.Add(new SqlParameter("@ValidTo", SafeDateTime(obj.ValidTo)));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                comm.Dispose();

                return result && obj.ID != 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
