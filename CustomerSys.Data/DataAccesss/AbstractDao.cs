﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace CustomerSys.Data
{
    public abstract class AbstractDao<T> where T:AbstractDataType
    {
        protected SqlConnection connection = null;
        protected string tableName = "";

        protected AbstractDao(SqlConnection connection, string tableName)
        {
            this.connection = connection;
            this.tableName = tableName;
        }


        protected long GetID()
        {
            SqlCommand comm = connection.CreateCommand();
            try
            {
                comm.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                return Convert.ToInt64(comm.ExecuteScalar());
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                comm.Dispose();
            }
        }

        protected object SafeString(string value)
        {
            if (value == null)
                return SqlString.Null;
            return value;
        }

        protected object SafeDateTime(DateTime value)
        {
            if (value == null || value<AbstractDataType.ZeroDateTime)
                return SqlDateTime.Null;
            return value;
        }

        protected object SafeDateTime(DateTime? value)
        {
            if (value == null || !value.HasValue || value.Value < AbstractDataType.ZeroDateTime)
                return SqlDateTime.Null;
            return value.Value;
        }

        protected object SafeDecimal(decimal? value)
        {
            if (value == null)
                return SqlDecimal.Null;
            return value;
        }

        public T Get(long id)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM "+tableName+" WHERE ID=@id";
                comm.Parameters.Add(new SqlParameter("@id", id));

                reader = comm.ExecuteReader();

                T result = null;
                if (reader.Read())
                {
                    result = Interprete(reader);
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public List<T> GetAll()
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName;

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<T> result = new List<T>();
                while (reader.Read())
                {
                    result.Add(Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<T>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public List<T> GetAllValid()
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM " + tableName + " WHERE Valid=@valid";
                comm.Parameters.Add(new SqlParameter("@valid", true));

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<T> result = new List<T>();
                while (reader.Read())
                {
                    result.Add(Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<T>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }

        public T Interprete(SqlDataReader reader) { return Interprete(reader, new PresentColumnsCollection(reader)); }
        public abstract T Interprete(SqlDataReader reader, PresentColumnsCollection cols);

        public abstract bool Save(T obj);
        public abstract bool Update(T obj);

        public bool SaveOrUpdate(T obj)
        {
            if (obj.ID == 0)
                return Save(obj);
            else
                return Update(obj);
        }
    }
}
