﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Drawing;
using System.Data;
using System.Data.SqlTypes;

namespace CustomerSys.Data
{
    public class GiftDao : AbstractDao<Gift>
    {
        public GiftDao(SqlConnection connection) : base(connection, "Gifts")
        {
            
        }

        public override bool Save(Gift obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO Gifts(Name, Description, Image, Valid) Values (@name, @desc, @image, @valid)";
                comm.Parameters.Add(new SqlParameter("@name", SafeString(obj.Name)));
                comm.Parameters.Add(new SqlParameter("@desc", SafeString(obj.Description)));
                comm.Parameters.Add(new SqlParameter("@image", SqlBinary.Null));
                comm.Parameters.Add(new SqlParameter("@valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                obj.ID = GetID();

                comm.Dispose();

                return result && obj.ID!=0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override bool Update(Gift obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "UPDATE Gifts SET Name=@name, Description=@desc, Image=@image, Valid=@valid WHERE ID=@id";
                comm.Parameters.Add(new SqlParameter("@id", obj.ID)); 
                comm.Parameters.Add(new SqlParameter("@name", SafeString(obj.Name)));
                comm.Parameters.Add(new SqlParameter("@desc", SafeString(obj.Description)));
                comm.Parameters.Add(new SqlParameter("@image", SqlBinary.Null));
                comm.Parameters.Add(new SqlParameter("@valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                comm.Dispose();

                return result && obj.ID != 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public override Gift Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            Gift g = new Gift();
            if (cols.Contains("ID"))
                g.ID = (long)reader["ID"];
            if (cols.Contains("Name") && !(reader["Name"] is DBNull))
                g.Name = (string)reader["Name"];
            if (cols.Contains("Description") && !(reader["Description"] is DBNull))
                g.Description = (string)reader["Description"];
            //if (cols.Contains("Image") && !(reader["Image"] is DBNull))
            //    g.Description = (string)reader["Image"];
            if (cols.Contains("Valid"))
                g.Valid = (bool)reader["Valid"];
            
            return g;
        }

        
        
    }
}
