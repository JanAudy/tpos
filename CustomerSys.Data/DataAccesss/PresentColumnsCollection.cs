﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace CustomerSys.Data
{
    public class PresentColumnsCollection
    {
        private Dictionary<string, int> columns = new Dictionary<string, int>();

        public PresentColumnsCollection(SqlDataReader reader)
        {
            columns.Clear();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                columns.Add(reader.GetName(i), i);
            }
        }

        public bool Contains(string name)
        {
            return columns.ContainsKey(name);
        }
    }
}
