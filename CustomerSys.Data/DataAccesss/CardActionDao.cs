﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CustomerSys.Data
{
    public class CardActionDao : AbstractDao<CardAction>
    {
        public CardActionDao(SqlConnection connection)
            : base(connection, "CardActions")
        {
            
        }

        public override CardAction Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            CardAction g = new CardAction();
            if (cols.Contains("ID"))
                g.ID = (long)reader["ID"];
            if (cols.Contains("Sale") && !(reader["Sale"] is DBNull))
                g.Sale = (long)reader["Sale"];
            if (cols.Contains("CardType") && !(reader["CardType"] is DBNull))
                g.CardType = (long)reader["CardType"];
            if (cols.Contains("DateTime") && !(reader["DateTime"] is DBNull))
                g.DateTime = (DateTime)reader["DateTime"];
            
            if (cols.Contains("ValidFrom"))
                if (reader["ValidFrom"] is DBNull)
                    g.ValidFrom = null;
                else
                    g.ValidFrom = (DateTime?)reader["ValidFrom"];
            if (cols.Contains("ValidTo"))
                if (reader["ValidTo"] is DBNull)
                    g.ValidTo = null;
                else
                    g.ValidTo = (DateTime?)reader["ValidTo"];

            if (cols.Contains("Arrival") && !(reader["Arrival"] is DBNull))
                g.Arrival = (CardAction.ArrivalType)reader["Arrival"];
            if (cols.Contains("ArrivalData") && !(reader["ArrivalData"] is DBNull))
                g.ArrivalData = (decimal)reader["ArrivalData"];
            if (cols.Contains("Period") && !(reader["Period"] is DBNull))
                g.Period = (CardAction.PeriodType)reader["Period"];
            if (cols.Contains("PeriodFrom") && !(reader["PeriodFrom"] is DBNull))
                g.PeriodFrom = (DateTime)reader["PeriodFrom"];
            if (cols.Contains("PeriodTo") && !(reader["PeriodTo"] is DBNull))
                g.PeriodTo = (DateTime)reader["PeriodTo"];
            if (cols.Contains("PeriodData") && !(reader["PeriodData"] is DBNull))
                g.PeriodData = (decimal)reader["PeriodData"];
            if (cols.Contains("Expense") && !(reader["Expense"] is DBNull))
                g.Expense = (CardAction.ExpenseType)reader["Expense"];
            if (cols.Contains("ExpenseFrom") && !(reader["ExpenseFrom"] is DBNull))
                g.ExpenseFrom = (decimal)reader["ExpenseFrom"];
            if (cols.Contains("ExpenseTo"))
                if (reader["ExpenseTo"] is DBNull)
                    g.ExpenseTo = null;
                else
                    g.ExpenseTo = (decimal?)reader["ExpenseTo"];
            
            if (cols.Contains("ExpensePeriodFrom"))
                if (reader["ExpensePeriodFrom"] is DBNull)
                    g.ExpensePeriodFrom = null;
                else
                    g.ExpensePeriodFrom = (DateTime?)reader["ExpensePeriodFrom"];
            if (cols.Contains("ExpensePeriodTo"))
                if (reader["ExpensePeriodTo"] is DBNull)
                    g.ExpensePeriodTo = null;
                else
                    g.ExpensePeriodTo = (DateTime?)reader["ExpensePeriodTo"];
            
            if (cols.Contains("Points") && !(reader["Points"] is DBNull))
                g.Points = (CardAction.PointsType)reader["Points"];
            if (cols.Contains("PointsData") && !(reader["PointsData"] is DBNull))
                g.PointsData = (int)reader["PointsData"];
            if (cols.Contains("Reward") && !(reader["Reward"] is DBNull))
                g.Reward = (CardAction.RewardType)reader["Reward"];
            if (cols.Contains("RewardValue") && !(reader["RewardValue"] is DBNull))
                g.RewardValue = (CardAction.RewardValueType)reader["RewardValue"];
            if (cols.Contains("RewardValueData") && !(reader["RewardValueData"] is DBNull))
                g.RewardValueData = (decimal)reader["RewardValueData"];
            if (cols.Contains("RewardValueDataParam") && !(reader["RewardValueDataParam"] is DBNull))
                g.RewardValueDataParam = (int)reader["RewardValueDataParam"];
            if (cols.Contains("RewardPoint") && !(reader["RewardPoint"] is DBNull))
                g.RewardPoint = (CardAction.RewardPointType)reader["RewardPoint"];
            if (cols.Contains("RewardPointData") && !(reader["RewardPointData"] is DBNull))
                g.RewardPointData = (decimal)reader["RewardPointData"];
            if (cols.Contains("Valid"))
                g.Valid = (bool)reader["Valid"];

            return g;
        }

        public override bool Save(CardAction obj)
        {
            try
            {

                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO [MDATA_CUSTOMERSYS].[dbo].[CardActions] ([Sale],[CardType],[DateTime],[ValidFrom],[ValidTo],[Arrival],[ArrivalData],[Period],[PeriodFrom],[PeriodTo],[PeriodData],"+
                    "[Expense],[ExpenseFrom],[ExpenseTo],[ExpensePeriodFrom],[ExpensePeriodTo],[Points],[PointsData],[Reward],[RewardValue],[RewardValueData],[RewardValueDataParam],[RewardPoint],[RewardPointData],[Valid]) " +
                    "Values (@Sale, @CardType, @DateTime, @ValidFrom, @ValidTo, @Arrival, @ArrivalData, @Period, @PeriodFrom, @PeriodTo, @PeriodData, @Expense, @ExpenseFrom, @ExpenseTo, @ExpensePeriodFrom, @ExpensePeriodTo, " +
                    "@Points, @PointsData, @Reward, @RewardValue, @RewardValueData, @RewardValueDataParam, @RewardPoint, @RewardPointData, @Valid)";

                comm.Parameters.Add(new SqlParameter("@Sale", obj.Sale));
                comm.Parameters.Add(new SqlParameter("@CardType", obj.CardType));
                comm.Parameters.Add(new SqlParameter("@DateTime", SafeDateTime(obj.DateTime)));
                comm.Parameters.Add(new SqlParameter("@ValidFrom", SafeDateTime(obj.ValidFrom)));
                comm.Parameters.Add(new SqlParameter("@ValidTo", SafeDateTime(obj.ValidTo)));
                comm.Parameters.Add(new SqlParameter("@Arrival", obj.Arrival));
                comm.Parameters.Add(new SqlParameter("@ArrivalData", SafeDecimal(obj.ArrivalData)));
                comm.Parameters.Add(new SqlParameter("@Period", obj.Period));
                comm.Parameters.Add(new SqlParameter("@PeriodFrom", SafeDateTime(obj.PeriodFrom)));
                comm.Parameters.Add(new SqlParameter("@PeriodTo", SafeDateTime(obj.PeriodTo)));
                comm.Parameters.Add(new SqlParameter("@PeriodData", SafeDecimal(obj.PeriodData)));
                comm.Parameters.Add(new SqlParameter("@Expense", obj.Expense));
                comm.Parameters.Add(new SqlParameter("@ExpenseFrom", SafeDecimal(obj.ExpenseFrom)));
                comm.Parameters.Add(new SqlParameter("@ExpenseTo", SafeDecimal(obj.ExpenseTo)));
                comm.Parameters.Add(new SqlParameter("@ExpensePeriodFrom", SafeDateTime(obj.ExpensePeriodFrom)));
                comm.Parameters.Add(new SqlParameter("@ExpensePeriodTo", SafeDateTime(obj.ExpensePeriodTo)));
                comm.Parameters.Add(new SqlParameter("@Points", obj.Points));
                comm.Parameters.Add(new SqlParameter("@PointsData", SafeDecimal(obj.PointsData)));
                comm.Parameters.Add(new SqlParameter("@Reward", obj.Reward));
                comm.Parameters.Add(new SqlParameter("@RewardValue", obj.RewardValue));
                comm.Parameters.Add(new SqlParameter("@RewardValueData", SafeDecimal(obj.RewardValueData)));
                comm.Parameters.Add(new SqlParameter("@RewardValueDataParam", obj.RewardValueDataParam));
                comm.Parameters.Add(new SqlParameter("@RewardPoint", obj.RewardPoint));
                comm.Parameters.Add(new SqlParameter("@RewardPointData", SafeDecimal(obj.RewardPointData)));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));
                
                bool result = comm.ExecuteNonQuery() == 1;

                obj.ID = GetID();

                comm.Dispose();

                return result && obj.ID != 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override bool Update(CardAction obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "UPDATE [MDATA_CUSTOMERSYS].[dbo].[CardActions] SET [Sale] = @Sale, [CardType] = @CardType, [DateTime] = @DateTime, [ValidFrom] = @ValidFrom, [ValidTo] = @ValidTo, [Arrival] = @Arrival, "+
                    "[ArrivalData] = @ArrivalData, [Period] = @Period, [PeriodFrom] = @PeriodFrom, [PeriodTo] = @PeriodTo, [PeriodData] = @PeriodData, [Expense] = @Expense, "+
                    "[ExpenseFrom] = @ExpenseFrom, [ExpenseTo] = @ExpenseTo, [ExpensePeriodFrom] = @ExpensePeriodFrom, [ExpensePeriodTo] = @ExpensePeriodTo, [Points] = @Points, [PointsData] = @PointsData, [Reward] = @Reward, [RewardValue] = @RewardValue, " +
                    "[RewardValueData] = @RewardValueData, [RewardValueDataParam] = @RewardValueDataParam, [RewardPoint] = @RewardPoint, [RewardPointData] = @RewardPointData, [Valid] = @Valid " +
                    "WHERE ID=@Id";
                comm.Parameters.Add(new SqlParameter("@Id", obj.ID));
                comm.Parameters.Add(new SqlParameter("@Sale", obj.Sale));
                comm.Parameters.Add(new SqlParameter("@CardType", obj.CardType));
                comm.Parameters.Add(new SqlParameter("@DateTime", SafeDateTime(obj.DateTime)));
                comm.Parameters.Add(new SqlParameter("@ValidFrom", SafeDateTime(obj.ValidFrom)));
                comm.Parameters.Add(new SqlParameter("@ValidTo", SafeDateTime(obj.ValidTo)));
                comm.Parameters.Add(new SqlParameter("@Arrival", obj.Arrival));
                comm.Parameters.Add(new SqlParameter("@ArrivalData", SafeDecimal(obj.ArrivalData)));
                comm.Parameters.Add(new SqlParameter("@Period", obj.Period));
                comm.Parameters.Add(new SqlParameter("@PeriodFrom", SafeDateTime(obj.PeriodFrom)));
                comm.Parameters.Add(new SqlParameter("@PeriodTo", SafeDateTime(obj.PeriodTo)));
                comm.Parameters.Add(new SqlParameter("@PeriodData", SafeDecimal(obj.PeriodData)));
                comm.Parameters.Add(new SqlParameter("@Expense", obj.Expense));
                comm.Parameters.Add(new SqlParameter("@ExpenseFrom", SafeDecimal(obj.ExpenseFrom)));
                comm.Parameters.Add(new SqlParameter("@ExpenseTo", SafeDecimal(obj.ExpenseTo)));
                comm.Parameters.Add(new SqlParameter("@ExpensePeriodFrom", SafeDateTime(obj.ExpensePeriodFrom)));
                comm.Parameters.Add(new SqlParameter("@ExpensePeriodTo", SafeDateTime(obj.ExpensePeriodTo)));
                comm.Parameters.Add(new SqlParameter("@Points", obj.Points));
                comm.Parameters.Add(new SqlParameter("@PointsData", SafeDecimal(obj.PointsData)));
                comm.Parameters.Add(new SqlParameter("@Reward", obj.Reward));
                comm.Parameters.Add(new SqlParameter("@RewardValue", obj.RewardValue));
                comm.Parameters.Add(new SqlParameter("@RewardValueData", SafeDecimal(obj.RewardValueData)));
                comm.Parameters.Add(new SqlParameter("@RewardValueDataParam", obj.RewardValueDataParam));
                comm.Parameters.Add(new SqlParameter("@RewardPoint", obj.RewardPoint));
                comm.Parameters.Add(new SqlParameter("@RewardPointData", SafeDecimal(obj.RewardPointData)));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                comm.Dispose();

                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal List<CardAction> GetByCardType(long cardType, DateTime dateTime)
        {
            SqlDataReader reader = null;
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM "+tableName+" WHERE CardType=@CardType AND ((ValidFrom is NULL) OR (ValidFrom<=@from)) AND ((ValidTo is NULL) OR (ValidTo>=@to)) AND Valid=1";
                comm.Parameters.Add(new SqlParameter("@CardType", cardType));
                comm.Parameters.Add(new SqlParameter("@from", dateTime));
                comm.Parameters.Add(new SqlParameter("@to", dateTime));

                reader = comm.ExecuteReader();

                PresentColumnsCollection cols = new PresentColumnsCollection(reader);
                List<CardAction> result = new List<CardAction>();
                while (reader.Read())
                {
                    result.Add(Interprete(reader, cols));
                }
                reader.Close();
                comm.Dispose();
                comm = null;

                return result;
            }
            catch (Exception e)
            {
                return new List<CardAction>();
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
