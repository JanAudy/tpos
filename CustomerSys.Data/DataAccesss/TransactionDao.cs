﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomerSys.Data;
using System.Data.SqlClient;

namespace CustomerSys.Data
{
    public class TransactionDao : AbstractDao<Transaction>
    {

        public TransactionDao(SqlConnection connection)
            : base(connection, "Transactions")
        {
            
        }

        public override bool Save(Transaction obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "INSERT INTO [MDATA_CUSTOMERSYS].[dbo].[Transactions]([Card],[Person],[Department],[Organization],[DateTime],"+
                    "[RecordType],[RecordID],[RecordNumber],[RecordNumberStr],[Amount],[Discount],[AmountWithDiscount],[Points],[Certificate],[Gift],[Valid]) Values" +
                    "(@Card,@Person,@Department,@Organization,@DateTime,@RecordType,@RecordID,@RecordNumber,@RecordNumberStr,@Amount,@Discount,@AmountWithDiscount,@Points,"+
                    "@Certificate,@Gift,@Valid)";

                comm.Parameters.Add(new SqlParameter("@Card", obj.Card));
                comm.Parameters.Add(new SqlParameter("@Person", obj.Person));
                comm.Parameters.Add(new SqlParameter("@Department", obj.Department));
                comm.Parameters.Add(new SqlParameter("@Organization", obj.Organization));
                comm.Parameters.Add(new SqlParameter("@DateTime", obj.DateTime));
                comm.Parameters.Add(new SqlParameter("@RecordType", obj.RecordType));
                comm.Parameters.Add(new SqlParameter("@RecordID", obj.RecordID));
                comm.Parameters.Add(new SqlParameter("@RecordNumber", obj.RecordNumber));
                comm.Parameters.Add(new SqlParameter("@RecordNumberStr", SafeString(obj.RecordNumberStr)));
                comm.Parameters.Add(new SqlParameter("@Amount", obj.Amount));
                comm.Parameters.Add(new SqlParameter("@Discount", obj.Discount));
                comm.Parameters.Add(new SqlParameter("@AmountWithDiscount", obj.AmountWithDiscount));
                comm.Parameters.Add(new SqlParameter("@Points", obj.Points));
                comm.Parameters.Add(new SqlParameter("@Certificate", obj.Certificate));
                comm.Parameters.Add(new SqlParameter("@Gift", obj.Gift));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                obj.ID = GetID();

                comm.Dispose();

                return result && obj.ID!=0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override bool Update(Transaction obj)
        {
            try
            {
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "UPDATE [MDATA_CUSTOMERSYS].[dbo].[Transactions] SET "+
                    "[Card] = @Card, [Person] = @Person, [Department] = @Department, [Organization] = @Organization, [DateTime] = @DateTime, "+
                    "[RecordType] = @RecordType, [RecordID] = @RecordID, [RecordNumber] = @RecordNumber, [RecordNumberStr] = @RecordNumberStr, "+
                    "[Amount] = @Amount, [Discount] = @Discount, [AmountWithDiscount] = @AmountWithDiscount, [Points] = @Points, [Certificate] = @Certificate, "+
                    "[Gift] = @Gift, Valid=@valid WHERE ID=@id";
                comm.Parameters.Add(new SqlParameter("@id", obj.ID)); 
                comm.Parameters.Add(new SqlParameter("@Card", obj.Card));
                comm.Parameters.Add(new SqlParameter("@Person", obj.Person));
                comm.Parameters.Add(new SqlParameter("@Department", obj.Department));
                comm.Parameters.Add(new SqlParameter("@Organization", obj.Organization));
                comm.Parameters.Add(new SqlParameter("@DateTime", obj.DateTime));
                comm.Parameters.Add(new SqlParameter("@RecordType", obj.RecordType));
                comm.Parameters.Add(new SqlParameter("@RecordID", obj.RecordID));
                comm.Parameters.Add(new SqlParameter("@RecordNumber", obj.RecordNumber));
                comm.Parameters.Add(new SqlParameter("@RecordNumberStr", SafeString(obj.RecordNumberStr)));
                comm.Parameters.Add(new SqlParameter("@Amount", obj.Amount));
                comm.Parameters.Add(new SqlParameter("@Discount", obj.Discount));
                comm.Parameters.Add(new SqlParameter("@AmountWithDiscount", obj.AmountWithDiscount));
                comm.Parameters.Add(new SqlParameter("@Points", obj.Points));
                comm.Parameters.Add(new SqlParameter("@Certificate", obj.Certificate));
                comm.Parameters.Add(new SqlParameter("@Gift", obj.Gift));
                comm.Parameters.Add(new SqlParameter("@Valid", obj.Valid));

                bool result = comm.ExecuteNonQuery() == 1;

                comm.Dispose();

                return result && obj.ID != 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public override Transaction Interprete(SqlDataReader reader, PresentColumnsCollection cols)
        {
            Transaction g = new Transaction();
            if (cols.Contains("ID"))
                g.ID = (long)reader["ID"];
           if (cols.Contains("Card"))
               g.Card = (long)reader["Card"];
            if (cols.Contains("Person"))
               g.Person = (long)reader["Person"];
            if (cols.Contains("Department"))
               g.Department = (long)reader["Department"];
            if (cols.Contains("Organization"))
               g.Organization = (long)reader["Organization"];
            if (cols.Contains("DateTime"))
               g.DateTime = (DateTime)reader["DateTime"];
            if (cols.Contains("RecordType"))
               g.RecordType = (Transaction.ERecordType)reader["RecordType"];
            if (cols.Contains("RecordID"))
               g.RecordID = (long)reader["RecordID"];
            if (cols.Contains("RecordNumber"))
               g.RecordNumber = (long)reader["RecordNumber"];
            if (cols.Contains("RecordNumberStr") && !(reader["RecordNumberStr"] is DBNull))
               g.RecordNumberStr = (string)reader["RecordNumberStr"];
            if (cols.Contains("Amount"))
               g.Amount = (decimal)reader["Amount"];
            if (cols.Contains("Discount"))
               g.Discount = (decimal)reader["Discount"];
            if (cols.Contains("AmountWithDiscount"))
               g.AmountWithDiscount = (decimal)reader["AmountWithDiscount"];
            if (cols.Contains("Points"))
               g.Points = (long)reader["Points"];
            if (cols.Contains("Certificate"))
               g.Certificate = (decimal)reader["Certificate"];
            if (cols.Contains("Gift"))
               g.Gift = (long)reader["Gift"];
            if (cols.Contains("Valid"))
                g.Valid = (bool)reader["Valid"];
            
            return g;
        }

        internal int GetCountByCard(long cardId)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT Count(Id) FROM " + tableName + "WHERE Card="+cardId;

                int count  = (int)comm.ExecuteScalar();

                comm.Dispose();
                comm = null;

                return count;
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        internal decimal GetTotalExpensePerCard(long cardId)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT Sum(AmountWithDiscount) FROM " + tableName + " WHERE Card=" + cardId+" AND Valid=1";

                decimal expense = (decimal)comm.ExecuteScalar();

                comm.Dispose();
                comm = null;

                return expense;
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }

        internal decimal GetTotalExpensePerCard(long cardId, DateTime dtFrom, DateTime dtTo)
        {
            SqlCommand comm = null;
            try
            {
                comm = connection.CreateCommand();
                comm.CommandText = "SELECT Sum(AmountWithDiscount) FROM @Tablename WHERE Card=@CardId AND DateTime>=@from AND DateTime<=@to AND Valid=1";
                comm.Parameters.Add(new SqlParameter("@TableName", tableName));
                comm.Parameters.Add(new SqlParameter("@CardId", cardId));
                comm.Parameters.Add(new SqlParameter("@from", dtFrom));
                comm.Parameters.Add(new SqlParameter("@to", dtTo));

                decimal expense = (decimal)comm.ExecuteScalar();

                comm.Dispose();
                comm = null;

                return expense;
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                if (comm != null)
                    comm.Dispose();
            }
        }
    }
}
