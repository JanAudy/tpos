﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Microdata.Util
{
    public class CryptoUtil
    {
        const string CRYPTO_SEED = "bgJJHBJBjbjhbHBJHJH";
        
        public static string RijndaelEncryptorString(string InputText)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(InputText);
            byte[] Salt = Encoding.ASCII.GetBytes(CRYPTO_SEED.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(CRYPTO_SEED, Salt);
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(PlainText, 0, PlainText.Length);
            cryptoStream.FlushFinalBlock();
            byte[] CipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string EncryptedData = Convert.ToBase64String(CipherBytes);
            return EncryptedData;

        }
        
        public static string RijndaelDecryptorString(string InputText)
        {
            string DecryptedData;
            try
            {
                RijndaelManaged RijndaelCipher = new RijndaelManaged();
                byte[] EncryptedData = Convert.FromBase64String(InputText);
                byte[] Salt = Encoding.ASCII.GetBytes(CRYPTO_SEED.Length.ToString());
                PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(CRYPTO_SEED, Salt);
                ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
                MemoryStream memoryStream = new MemoryStream(EncryptedData);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);
                byte[] PlainText = new byte[EncryptedData.Length];
                int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);
                memoryStream.Close();
                cryptoStream.Close();
                DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);
            }
            catch
            {
                DecryptedData = "";
            }

            return DecryptedData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InputText"></param>
        /// <returns></returns>
        public static string HashEncryptorString(string InputText)
        {
            // string HashData;
            byte[] byteOutput; // hashovaci funkce SHA-1 je ziskana ze tridy SHA1CryptoServiceProvider 
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] MessageBytes = UE.GetBytes(InputText);
            SHA256Managed SHhash = new SHA256Managed();
            byteOutput = SHhash.ComputeHash(MessageBytes);
            //    HashData = Encoding.Unicode.GetString(byteOutput, 0, byteOutput.Length);
            return Convert.ToBase64String(byteOutput);
        }


    }
}
