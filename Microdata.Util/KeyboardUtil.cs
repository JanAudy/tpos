﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Microdata.Util
{
    public class KeyboardUtil
    {
        public static char GetKey(Keys code)
        {
            switch (code)
            {
                case Keys.D0:
                case Keys.NumPad0: return '0';
                case Keys.D1:
                case Keys.NumPad1: return '1';
                case Keys.D2:
                case Keys.NumPad2: return '2';
                case Keys.D3:
                case Keys.NumPad3: return '3';
                case Keys.D4:
                case Keys.NumPad4: return '4';
                case Keys.D5:
                case Keys.NumPad5: return '5';
                case Keys.D6:
                case Keys.NumPad6: return '6';
                case Keys.D7:
                case Keys.NumPad7: return '7';
                case Keys.D8:
                case Keys.NumPad8: return '8';
                case Keys.D9:
                case Keys.NumPad9: return '9';
                case Keys.Enter: return '\n';
                case Keys.Back: return '\b';
                case Keys.OemMinus: return '-';
                case Keys.Subtract: return '-';
                case Keys.Oemcomma: return ',';
            }
            return (char)(code & Keys.KeyCode);
        }

    }
}
