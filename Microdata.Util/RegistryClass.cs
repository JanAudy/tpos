﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Microdata.Util
{
    public class RegistryClass
    {
        private string pAPP = "";
        private string pUSER = "";
        private string pWINDOW = "";

        public RegistryClass(string applicationName, string userName, string windowsName)
        {
            pAPP = applicationName;
            pUSER = CryptoUtil.HashEncryptorString(userName);
            pWINDOW = windowsName;
        }
        
        public void SetWindowsPosition(int left, int top, int width, int height)
        {
            string oldPosition, newPosition;
            oldPosition = "";
            RegistryKey HLKEY = Registry.CurrentUser;

            HLKEY = HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW\" + pUSER);
            if (HLKEY == null) return;
            if (HLKEY.GetValue(pWINDOW) != null) oldPosition = (string)HLKEY.GetValue(pWINDOW);
            newPosition = Convert.ToString(left) + ";" + Convert.ToString(top) + ";" + Convert.ToString(width) + ";" + Convert.ToString(height);
            if (oldPosition != newPosition) HLKEY.SetValue(pWINDOW, newPosition);
        }

        public bool GetWindowsPosition(ref int left, ref int top, ref int width, ref int height)
        {
            string position;
            char[] splitter = { ';' };
            bool VYS = false;
            RegistryKey HLKEY = Registry.CurrentUser;
            try
            {
                HLKEY = HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW\" + pUSER, false);
                if (HLKEY != null)
                {
                    if (HLKEY.GetValue(pWINDOW) != null)
                    {
                        position = (string)HLKEY.GetValue(pWINDOW);
                        string[] VELIKOST = position.Split(splitter);
                        left = Convert.ToInt32(VELIKOST[0]);
                        top = Convert.ToInt32(VELIKOST[1]);
                        width = Convert.ToInt32(VELIKOST[2]);
                        height = Convert.ToInt32(VELIKOST[3]);
                        VYS = true;
                    }
                }
            }
            catch
            {
                VYS = false;
            }


            return VYS;
        }

        public void SetWinPARAM(string name, string value)
        {
            string GetPARAM;
            GetPARAM = "";
            RegistryKey HLKEY = Registry.CurrentUser;

            HLKEY = HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM\" + pUSER + @"\" + pWINDOW);
            if (HLKEY == null) return;
            if (HLKEY.GetValue(name) != null) GetPARAM = (string)HLKEY.GetValue(name);
            if (GetPARAM != value) HLKEY.SetValue(name, value);
        }

        public string GetWinPARAM(string name)
        {
            RegistryKey HLKEY = Registry.CurrentUser;

            HLKEY = HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM\" + pUSER + @"\" + pWINDOW, true);
            if (HLKEY == null) return "";
            if (HLKEY.GetValue(name) != null) return (string)HLKEY.GetValue(name);
            return "";
        }

        public string GetWinPARAM(string name, string defParam)
        {

            RegistryKey HLKEY = Registry.CurrentUser;

            HLKEY = HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM\" + pUSER + @"\" + pWINDOW, true);
            if (HLKEY == null) return defParam;
            if (HLKEY.GetValue(name) != null) return (string)HLKEY.GetValue(name);
            return defParam;
        }

        public void CreateUserWinKey()
        {
            RegistryKey HLKEY = Registry.CurrentUser;
            if (HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP, false) == null)
            {
                HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP);
            }
            if (HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW", false) == null)
            {
                HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW");
            }
            if (HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW\" + pUSER, true) == null)
            {
                HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\WINDOW\" + pUSER);
            }
            if (HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM", false) == null)
            {
                HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM");
            }
            if (HLKEY.OpenSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM\" + pUSER, true) == null)
            {
                HLKEY.CreateSubKey(@"SOFTWARE\MICRODATA\DOTNET\" + pAPP + @"\PARAM\" + pUSER);
            }
        }
    }
}
